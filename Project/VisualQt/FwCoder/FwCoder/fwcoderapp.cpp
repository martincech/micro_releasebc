#include "fwcoderapp.h"
#include <QFileDialog>



FwCoderApp::FwCoderApp(QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent, flags)
{
   ui.setupUi(this);
   Coder = new TFwCoder();
}

FwCoderApp::~FwCoderApp()
{
   delete Coder;
}

void FwCoderApp::on_OpenConfigPushButton_clicked( void) {
   ConfigFileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.cfg)"));
}

void FwCoderApp::on_OpenFilePushButton_clicked( void) {
   
}


void FwCoderApp::on_EncryptPushButton_clicked( void) {
   InputFileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.hex)"));
   OutputFileName = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("Files (*.fwc)"));

   Coder->Encode(InputFileName.toAscii().data(), OutputFileName.toAscii().data());
}


void FwCoderApp::on_DecryptPushButton_clicked( void) {
   InputFileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.fwc)"));
   OutputFileName = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("Files (*.fwe)"));

   Coder->Decode(InputFileName.toAscii().data(), OutputFileName.toAscii().data());
}

void FwCoderApp::on_InterpretPushButton_clicked( void) {
   InputFileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.fwc)"));
   OutputFileName = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("Files (*.txt)"));

   Coder->Interpret(InputFileName.toAscii().data(), OutputFileName.toAscii().data());
}