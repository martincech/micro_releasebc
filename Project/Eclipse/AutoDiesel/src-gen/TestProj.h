
#ifndef TESTPROJ_H_
#define TESTPROJ_H_

#include "sc_types.h"

#ifdef __cplusplus
extern "C" { 
#endif 

/*! \file Header of the state machine 'TestProj'.
*/

//! enumeration of all states 
typedef enum {
	TestProj_Start_logic_Start_logic,
	TestProj_Start_logic_Start_logic_r1_Init,
	TestProj_Start_logic_Start_logic_r1_User_start_requested,
	TestProj_Motor_control_Diesel_start_sequence,
	TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence,
	TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart,
	TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing,
	TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start,
	TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm,
	TestProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start,
	TestProj_Motor_control_Diesel_start_sequence_inner_region_Starting,
	TestProj_Motor_control_Diesel_start_sequence_inner_region_Running_check,
	TestProj_Motor_control_Diesel_shutdown_sequence,
	TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Turn_off_electronics,
	TestProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown,
	TestProj_Motor_control_Motor_priority_logic,
	TestProj_Motor_control_Elmot_start_sequence,
	TestProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run,
	TestProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot,
	TestProj_Motor_control_Hydro_start_sequence,
	TestProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run,
	TestProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro,
	TestProj_Motor_control_Cooling_logic,
	TestProj_Motor_control_Cooling_logic_r1_Coutner,
	TestProj_Diesel_events_to_internal_events_Event_watch,
	TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch,
	TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch,
	TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_STOP_Watch_STOP,
	TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r1_Start_Stop_watch_START_watch_START,
	TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch,
	TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_RUN_Watch_RUN,
	TestProj_Diesel_events_to_internal_events_Event_watch_Outputs_Output_watch_r2_RUN_GLOW_watch_GLOW_watch_GLOW,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_TRUE,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_EXT_Watch_EXT_FALSE,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_TRUE,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_AIR_Watch_AIR_FALSE,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_TRUE,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_TEMP_Watch_TEMP_FALSE,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_TRUE,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Diesel_Motor_signals_watch_OIL_Watch_OIL_FALSE,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_TRUE,
	TestProj_Diesel_events_to_internal_events_Event_watch_Inputs_Input_watch_Keyboard_Keyboard_watch_AutoType_solder_Switch_Watch_SWITCH_AUTOTYPE_FALSE,
	TestProj_last_state
} TestProjStates;

//! Type definition of the data structure for the TestProjIfaceDiesel1 interface scope.
typedef struct {
	sc_boolean RUN_raised;
	sc_boolean RUN_value;
	sc_boolean START_raised;
	sc_boolean START_value;
	sc_boolean GLOW_raised;
	sc_boolean GLOW_value;
	sc_boolean STOP_raised;
	sc_boolean STOP_value;
	sc_boolean EXT_ON_raised;
	sc_boolean TEMP_HI_raised;
	sc_boolean OIL_LOW_raised;
	sc_boolean AIR_ERR_raised;
} TestProjIfaceDiesel1;

//! Type definition of the data structure for the TestProjIfaceDiesel2 interface scope.
typedef struct {
	sc_boolean RUN_raised;
	sc_boolean RUN_value;
	sc_boolean START_raised;
	sc_boolean START_value;
	sc_boolean GLOW_raised;
	sc_boolean GLOW_value;
	sc_boolean STOP_raised;
	sc_boolean STOP_value;
	sc_boolean EXT_ON_raised;
	sc_boolean TEMP_HI_raised;
	sc_boolean OIL_LOW_raised;
	sc_boolean AIR_ERR_raised;
} TestProjIfaceDiesel2;

//! Type definition of the data structure for the TestProjIfaceElmot interface scope.
typedef struct {
	sc_boolean WALL_AVAILABLE_raised;
	sc_boolean WALL_AVAILABLE_value;
	sc_boolean RUNNING_raised;
	sc_boolean RUNNING_value;
	sc_boolean RUN_raised;
	sc_boolean RUN_value;
} TestProjIfaceElmot;

//! Type definition of the data structure for the TestProjIfaceAxima interface scope.
typedef struct {
	sc_boolean RUNNING_raised;
	sc_boolean RUNNING_value;
} TestProjIfaceAxima;

//! Type definition of the data structure for the TestProjIfaceHydro interface scope.
typedef struct {
	sc_boolean HYDRO_AVAILABLE_raised;
	sc_boolean HYDRO_AVAILABLE_value;
	sc_boolean RUNNING_raised;
	sc_boolean RUNNING_value;
	sc_boolean RUN_raised;
	sc_boolean RUN_value;
} TestProjIfaceHydro;

//! Type definition of the data structure for the TestProjIfaceKeyboard interface scope.
typedef struct {
	sc_boolean START_STOP_HIGH_raised;
	sc_boolean START_STOP_LOW_raised;
	sc_boolean SWITCH_DIESEL_raised;
	sc_boolean SWITCH_DIESEL_value;
	sc_boolean SWITCH_HYDRO_raised;
	sc_boolean SWITCH_HYDRO_value;
	sc_boolean SWITCH_ELMOT_raised;
	sc_boolean SWITCH_ELMOT_value;
	sc_boolean SWITCH_AUTOTYPE_raised;
	sc_boolean SWITCH_AUTOTYPE_value;
} TestProjIfaceKeyboard;

//! Type definition of the data structure for the TestProjIfaceSensors interface scope.
typedef struct {
	sc_boolean SIRENA_EN_raised;
	sc_boolean SIRENA_EN_value;
	sc_boolean LED_GLOW_raised;
	sc_boolean LED_GLOW_value;
	sc_boolean LED_EXT_raised;
	sc_boolean LED_EXT_value;
	sc_boolean LED_OIL_raised;
	sc_boolean LED_OIL_value;
	sc_boolean LED_TEMP_raised;
	sc_boolean LED_TEMP_value;
	sc_boolean LED_AIR_raised;
	sc_boolean LED_AIR_value;
} TestProjIfaceSensors;

//! Type definition of the data structure for the TestProjIfaceCooling interface scope.
typedef struct {
	sc_boolean COOL_TMC_raised;
	sc_boolean COOL_TMC_value;
	sc_boolean COOL_EN_raised;
	sc_boolean COOL_EN_value;
} TestProjIfaceCooling;

//! Type definition of the data structure for the TestProjIfaceVentilation interface scope.
typedef struct {
	sc_boolean ON_raised;
	sc_boolean ON_value;
	sc_boolean U_LOW_raised;
	sc_boolean U_LOW_value;
	sc_boolean BATTERY_FULL_raised;
	sc_boolean BATTERY_FULL_value;
} TestProjIfaceVentilation;

//! Type definition of the data structure for the TestProjInternal interface scope.
typedef struct {
	sc_integer ActiveMotor;
	sc_integer DieselStartTries;
	sc_integer StartedBy;
	sc_boolean RunMotor;
	sc_integer CoolPause;
	sc_integer CoolPauseLength;
	sc_boolean StopDiesel;
	sc_boolean RUN_raised;
	sc_boolean RUN_value;
	sc_boolean START_raised;
	sc_boolean START_value;
	sc_boolean GLOW_raised;
	sc_boolean GLOW_value;
	sc_boolean STOP_raised;
	sc_boolean STOP_value;
	sc_boolean EXT_ON;
	sc_boolean TEMP_HI;
	sc_boolean OIL_LOW;
	sc_boolean AIR_ERR;
	sc_string AUTOTYPE;
	sc_boolean Init_raised;
} TestProjInternal;

//! Type definition of the data structure for the TestProjTimeEvents interface scope.
typedef struct {
	sc_boolean testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Restart_tev0_raised;
	sc_boolean testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Glowing_tev0_raised;
	sc_boolean testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel_2_start_tev0_raised;
	sc_boolean testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Alarm_tev0_raised;
	sc_boolean testProj_Motor_control_Diesel_start_sequence_inner_region_Start_sequence_inner_region_Diesel1_start_tev0_raised;
	sc_boolean testProj_Motor_control_Diesel_start_sequence_inner_region_Starting_tev0_raised;
	sc_boolean testProj_Motor_control_Diesel_shutdown_sequence_Shutdown_sequence_Shutdown_tev0_raised;
	sc_boolean testProj_Motor_control_Elmot_start_sequence_inner_region_Elmot_wait_run_tev0_raised;
	sc_boolean testProj_Motor_control_Elmot_start_sequence_inner_region_Shut_elmot_tev0_raised;
	sc_boolean testProj_Motor_control_Hydro_start_sequence_inner_region_hydro_wait_run_tev0_raised;
	sc_boolean testProj_Motor_control_Hydro_start_sequence_inner_region_Shut_hydro_tev0_raised;
	sc_boolean testProj_Motor_control_Cooling_logic_r1_Coutner_tev0_raised;
} TestProjTimeEvents;


//! the maximum number of orthogonal states defines the dimension of the state configuration vector.
#define TESTPROJ_MAX_ORTHOGONAL_STATES 11

/*! Type definition of the data structure for the TestProj state machine.
This data structure has to be allocated by the client code. */
typedef struct {
	TestProjStates stateConfVector[TESTPROJ_MAX_ORTHOGONAL_STATES];
	sc_ushort stateConfVectorPosition; 
	
	TestProjIfaceDiesel1 ifaceDiesel1;
	TestProjIfaceDiesel2 ifaceDiesel2;
	TestProjIfaceElmot ifaceElmot;
	TestProjIfaceAxima ifaceAxima;
	TestProjIfaceHydro ifaceHydro;
	TestProjIfaceKeyboard ifaceKeyboard;
	TestProjIfaceSensors ifaceSensors;
	TestProjIfaceCooling ifaceCooling;
	TestProjIfaceVentilation ifaceVentilation;
	TestProjInternal internal;
	TestProjTimeEvents timeEvents;
} TestProj;

/*! Initializes the TestProj state machine data structures. Must be called before first usage.*/
extern void testProj_init(TestProj* handle);

/*! Activates the state machine */
extern void testProj_enter(TestProj* handle);

/*! Deactivates the state machine */
extern void testProj_exit(TestProj* handle);

/*! Performs a 'run to completion' step. */
extern void testProj_runCycle(TestProj* handle);

/*! Raises a time event. */
extern void testProj_raiseTimeEvent(TestProj* handle, sc_eventid evid);

/*! Checks if the out event 'RUN' that is defined in the interface scope 'Diesel1' has been raised. */ 
extern sc_boolean testProjIfaceDiesel1_israised_rUN(TestProj* handle);

/*! Gets the value of the out event 'RUN' that is defined in the interface scope 'Diesel1'. */ 
extern sc_boolean testProjIfaceDiesel1_get_rUN_value(TestProj* handle);

/*! Checks if the out event 'START' that is defined in the interface scope 'Diesel1' has been raised. */ 
extern sc_boolean testProjIfaceDiesel1_israised_sTART(TestProj* handle);

/*! Gets the value of the out event 'START' that is defined in the interface scope 'Diesel1'. */ 
extern sc_boolean testProjIfaceDiesel1_get_sTART_value(TestProj* handle);

/*! Checks if the out event 'GLOW' that is defined in the interface scope 'Diesel1' has been raised. */ 
extern sc_boolean testProjIfaceDiesel1_israised_gLOW(TestProj* handle);

/*! Gets the value of the out event 'GLOW' that is defined in the interface scope 'Diesel1'. */ 
extern sc_boolean testProjIfaceDiesel1_get_gLOW_value(TestProj* handle);

/*! Checks if the out event 'STOP' that is defined in the interface scope 'Diesel1' has been raised. */ 
extern sc_boolean testProjIfaceDiesel1_israised_sTOP(TestProj* handle);

/*! Gets the value of the out event 'STOP' that is defined in the interface scope 'Diesel1'. */ 
extern sc_boolean testProjIfaceDiesel1_get_sTOP_value(TestProj* handle);

/*! Raises the in event 'EXT_ON' that is defined in the interface scope 'Diesel1'. */ 
extern void testProjIfaceDiesel1_raise_eXT_ON(TestProj* handle);

/*! Raises the in event 'TEMP_HI' that is defined in the interface scope 'Diesel1'. */ 
extern void testProjIfaceDiesel1_raise_tEMP_HI(TestProj* handle);

/*! Raises the in event 'OIL_LOW' that is defined in the interface scope 'Diesel1'. */ 
extern void testProjIfaceDiesel1_raise_oIL_LOW(TestProj* handle);

/*! Raises the in event 'AIR_ERR' that is defined in the interface scope 'Diesel1'. */ 
extern void testProjIfaceDiesel1_raise_aIR_ERR(TestProj* handle);


/*! Checks if the out event 'RUN' that is defined in the interface scope 'Diesel2' has been raised. */ 
extern sc_boolean testProjIfaceDiesel2_israised_rUN(TestProj* handle);

/*! Gets the value of the out event 'RUN' that is defined in the interface scope 'Diesel2'. */ 
extern sc_boolean testProjIfaceDiesel2_get_rUN_value(TestProj* handle);

/*! Checks if the out event 'START' that is defined in the interface scope 'Diesel2' has been raised. */ 
extern sc_boolean testProjIfaceDiesel2_israised_sTART(TestProj* handle);

/*! Gets the value of the out event 'START' that is defined in the interface scope 'Diesel2'. */ 
extern sc_boolean testProjIfaceDiesel2_get_sTART_value(TestProj* handle);

/*! Checks if the out event 'GLOW' that is defined in the interface scope 'Diesel2' has been raised. */ 
extern sc_boolean testProjIfaceDiesel2_israised_gLOW(TestProj* handle);

/*! Gets the value of the out event 'GLOW' that is defined in the interface scope 'Diesel2'. */ 
extern sc_boolean testProjIfaceDiesel2_get_gLOW_value(TestProj* handle);

/*! Checks if the out event 'STOP' that is defined in the interface scope 'Diesel2' has been raised. */ 
extern sc_boolean testProjIfaceDiesel2_israised_sTOP(TestProj* handle);

/*! Gets the value of the out event 'STOP' that is defined in the interface scope 'Diesel2'. */ 
extern sc_boolean testProjIfaceDiesel2_get_sTOP_value(TestProj* handle);

/*! Raises the in event 'EXT_ON' that is defined in the interface scope 'Diesel2'. */ 
extern void testProjIfaceDiesel2_raise_eXT_ON(TestProj* handle);

/*! Raises the in event 'TEMP_HI' that is defined in the interface scope 'Diesel2'. */ 
extern void testProjIfaceDiesel2_raise_tEMP_HI(TestProj* handle);

/*! Raises the in event 'OIL_LOW' that is defined in the interface scope 'Diesel2'. */ 
extern void testProjIfaceDiesel2_raise_oIL_LOW(TestProj* handle);

/*! Raises the in event 'AIR_ERR' that is defined in the interface scope 'Diesel2'. */ 
extern void testProjIfaceDiesel2_raise_aIR_ERR(TestProj* handle);


/*! Raises the in event 'WALL_AVAILABLE' that is defined in the interface scope 'Elmot'. */ 
extern void testProjIfaceElmot_raise_wALL_AVAILABLE(TestProj* handle, sc_boolean value);

/*! Raises the in event 'RUNNING' that is defined in the interface scope 'Elmot'. */ 
extern void testProjIfaceElmot_raise_rUNNING(TestProj* handle, sc_boolean value);

/*! Checks if the out event 'RUN' that is defined in the interface scope 'Elmot' has been raised. */ 
extern sc_boolean testProjIfaceElmot_israised_rUN(TestProj* handle);

/*! Gets the value of the out event 'RUN' that is defined in the interface scope 'Elmot'. */ 
extern sc_boolean testProjIfaceElmot_get_rUN_value(TestProj* handle);


/*! Raises the in event 'RUNNING' that is defined in the interface scope 'Axima'. */ 
extern void testProjIfaceAxima_raise_rUNNING(TestProj* handle, sc_boolean value);


/*! Raises the in event 'HYDRO_AVAILABLE' that is defined in the interface scope 'Hydro'. */ 
extern void testProjIfaceHydro_raise_hYDRO_AVAILABLE(TestProj* handle, sc_boolean value);

/*! Raises the in event 'RUNNING' that is defined in the interface scope 'Hydro'. */ 
extern void testProjIfaceHydro_raise_rUNNING(TestProj* handle, sc_boolean value);

/*! Checks if the out event 'RUN' that is defined in the interface scope 'Hydro' has been raised. */ 
extern sc_boolean testProjIfaceHydro_israised_rUN(TestProj* handle);

/*! Gets the value of the out event 'RUN' that is defined in the interface scope 'Hydro'. */ 
extern sc_boolean testProjIfaceHydro_get_rUN_value(TestProj* handle);


/*! Raises the in event 'START_STOP_HIGH' that is defined in the interface scope 'Keyboard'. */ 
extern void testProjIfaceKeyboard_raise_sTART_STOP_HIGH(TestProj* handle);

/*! Raises the in event 'START_STOP_LOW' that is defined in the interface scope 'Keyboard'. */ 
extern void testProjIfaceKeyboard_raise_sTART_STOP_LOW(TestProj* handle);

/*! Raises the in event 'SWITCH_DIESEL' that is defined in the interface scope 'Keyboard'. */ 
extern void testProjIfaceKeyboard_raise_sWITCH_DIESEL(TestProj* handle, sc_boolean value);

/*! Raises the in event 'SWITCH_HYDRO' that is defined in the interface scope 'Keyboard'. */ 
extern void testProjIfaceKeyboard_raise_sWITCH_HYDRO(TestProj* handle, sc_boolean value);

/*! Raises the in event 'SWITCH_ELMOT' that is defined in the interface scope 'Keyboard'. */ 
extern void testProjIfaceKeyboard_raise_sWITCH_ELMOT(TestProj* handle, sc_boolean value);

/*! Raises the in event 'SWITCH_AUTOTYPE' that is defined in the interface scope 'Keyboard'. */ 
extern void testProjIfaceKeyboard_raise_sWITCH_AUTOTYPE(TestProj* handle, sc_boolean value);


/*! Checks if the out event 'SIRENA_EN' that is defined in the interface scope 'Sensors' has been raised. */ 
extern sc_boolean testProjIfaceSensors_israised_sIRENA_EN(TestProj* handle);

/*! Gets the value of the out event 'SIRENA_EN' that is defined in the interface scope 'Sensors'. */ 
extern sc_boolean testProjIfaceSensors_get_sIRENA_EN_value(TestProj* handle);

/*! Checks if the out event 'LED_GLOW' that is defined in the interface scope 'Sensors' has been raised. */ 
extern sc_boolean testProjIfaceSensors_israised_lED_GLOW(TestProj* handle);

/*! Gets the value of the out event 'LED_GLOW' that is defined in the interface scope 'Sensors'. */ 
extern sc_boolean testProjIfaceSensors_get_lED_GLOW_value(TestProj* handle);

/*! Checks if the out event 'LED_EXT' that is defined in the interface scope 'Sensors' has been raised. */ 
extern sc_boolean testProjIfaceSensors_israised_lED_EXT(TestProj* handle);

/*! Gets the value of the out event 'LED_EXT' that is defined in the interface scope 'Sensors'. */ 
extern sc_boolean testProjIfaceSensors_get_lED_EXT_value(TestProj* handle);

/*! Checks if the out event 'LED_OIL' that is defined in the interface scope 'Sensors' has been raised. */ 
extern sc_boolean testProjIfaceSensors_israised_lED_OIL(TestProj* handle);

/*! Gets the value of the out event 'LED_OIL' that is defined in the interface scope 'Sensors'. */ 
extern sc_boolean testProjIfaceSensors_get_lED_OIL_value(TestProj* handle);

/*! Checks if the out event 'LED_TEMP' that is defined in the interface scope 'Sensors' has been raised. */ 
extern sc_boolean testProjIfaceSensors_israised_lED_TEMP(TestProj* handle);

/*! Gets the value of the out event 'LED_TEMP' that is defined in the interface scope 'Sensors'. */ 
extern sc_boolean testProjIfaceSensors_get_lED_TEMP_value(TestProj* handle);

/*! Checks if the out event 'LED_AIR' that is defined in the interface scope 'Sensors' has been raised. */ 
extern sc_boolean testProjIfaceSensors_israised_lED_AIR(TestProj* handle);

/*! Gets the value of the out event 'LED_AIR' that is defined in the interface scope 'Sensors'. */ 
extern sc_boolean testProjIfaceSensors_get_lED_AIR_value(TestProj* handle);


/*! Raises the in event 'COOL_TMC' that is defined in the interface scope 'Cooling'. */ 
extern void testProjIfaceCooling_raise_cOOL_TMC(TestProj* handle, sc_boolean value);

/*! Checks if the out event 'COOL_EN' that is defined in the interface scope 'Cooling' has been raised. */ 
extern sc_boolean testProjIfaceCooling_israised_cOOL_EN(TestProj* handle);

/*! Gets the value of the out event 'COOL_EN' that is defined in the interface scope 'Cooling'. */ 
extern sc_boolean testProjIfaceCooling_get_cOOL_EN_value(TestProj* handle);


/*! Raises the in event 'ON' that is defined in the interface scope 'Ventilation'. */ 
extern void testProjIfaceVentilation_raise_oN(TestProj* handle, sc_boolean value);

/*! Raises the in event 'U_LOW' that is defined in the interface scope 'Ventilation'. */ 
extern void testProjIfaceVentilation_raise_u_LOW(TestProj* handle, sc_boolean value);

/*! Raises the in event 'BATTERY_FULL' that is defined in the interface scope 'Ventilation'. */ 
extern void testProjIfaceVentilation_raise_bATTERY_FULL(TestProj* handle, sc_boolean value);



/*! Checks if the specified state is active. */
extern sc_boolean testProj_isActive(TestProj* handle, TestProjStates state);

#ifdef __cplusplus
}
#endif 

#endif /* TESTPROJ_H_ */
