#-------------------------------------------------
#
# Project created by QtCreator 2013-07-25T08:50:02
#
#-------------------------------------------------

QT       += core gui

TARGET = Bat2Log
TEMPLATE = app

INCLUDEPATH  = ../../../Library/Qt
INCLUDEPATH += ../../../Library/Bat2
INCLUDEPATH += ../../../Library/Bat
INCLUDEPATH += ../../../Library/Micro
INCLUDEPATH += ../../../Library/Device
INCLUDEPATH += ../../../Library/Win32

SOURCES += main.cpp\
        mainwindow.cpp \
    ../../../Library/Bat2/Log/Log.c \
    ../../../Library/Qt/uSimulator/Nvm.cpp \
    ../../../Library/Micro/Time/uTime.c \
    ../../../Library/Micro/Time/uDateTime.c \
    ../../../Library/Micro/Time/uDate.c \
    ../../../Library/Micro/Time/uClock.c \
    ../../../Library/Micro/Data/uFifo.c \
    ../../../Library/Qt/uSimulator/SysClock.c

HEADERS  += mainwindow.h \
    Hardware.h

FORMS    += mainwindow.ui
