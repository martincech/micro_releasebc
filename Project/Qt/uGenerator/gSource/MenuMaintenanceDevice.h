//******************************************************************************
//
//   MenuMaintenanceDevice.h  Maintenance device menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuMaintenanceDevice_H__
   #define __MenuMaintenanceDevice_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuMaintenanceDevice( void);
// Menu maintenance device

#endif
