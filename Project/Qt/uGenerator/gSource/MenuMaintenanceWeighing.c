//******************************************************************************
//
//   MenuMaintenanceWeighing.c  Maintenance weighing menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuMaintenanceWeighing.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "Platform.h"
#include "Weighing.h"
#include "Weighing.h"
#include "Weighing.h"
#include "Weighing.h"


static DefMenu( MaintenanceWeighingMenu)
   STR_START,
   STR_STOP,
   STR_PAUSE,
   STR_RESUME,
EndMenu()

typedef enum {
   MI_START,
   MI_STOP,
   MI_PAUSE,
   MI_RESUME
} EMaintenanceWeighingMenu;

//------------------------------------------------------------------------------
//  Menu MaintenanceWeighing
//------------------------------------------------------------------------------

void MenuMaintenanceWeighing( void)
// Menu maintenance weighing
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_WEIGHING, MaintenanceWeighingMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_START :
            WeighingStart();
            break;

         case MI_STOP :
            WeighingStop();
            break;

         case MI_PAUSE :
            WeighingPause();
            break;

         case MI_RESUME :
            WeighingResume();
            break;

      }
   }
} // MenuMaintenanceWeighing
