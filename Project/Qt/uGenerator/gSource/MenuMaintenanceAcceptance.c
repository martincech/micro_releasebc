//******************************************************************************
//
//   MenuMaintenanceAcceptance.c  Maintenance acceptance menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuMaintenanceAcceptance.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config.h"               // Project configuration

#include "Platform.h"


static DefMenu( MaintenanceAcceptanceMenu)
   STR_MODE,
   STR_GENDER,
   STR_LOW_LIMIT,
   STR_LOW_LIMIT_MALE,
   STR_HIGH_LIMIT,
   STR_HIGH_LIMIT_MALE,
   STR_LOW_LIMIT_FEMALE,
   STR_HIGH_LIMIT_FEMALE,
   STR_STEP,
EndMenu()

typedef enum {
   MI_MODE,
   MI_GENDER,
   MI_LOW_LIMIT,
   MI_LOW_LIMIT_MALE,
   MI_HIGH_LIMIT,
   MI_HIGH_LIMIT_MALE,
   MI_LOW_LIMIT_FEMALE,
   MI_HIGH_LIMIT_FEMALE,
   MI_STEP
} EMaintenanceAcceptanceMenu;

// Local functions :

static void PlatformAcceptanceParameters( int Index, int y, TPlatformAcceptance *Parameters);
// Draw maintenance acceptance parameters

//------------------------------------------------------------------------------
//  Menu MaintenanceAcceptance
//------------------------------------------------------------------------------

void MenuMaintenanceAcceptance( void)
// Edit maintenance acceptance parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_ACCEPTANCE, MaintenanceAcceptanceMenu, (TMenuItemCb *)PlatformAcceptanceParameters, &PlatformAcceptance, &MData)){
         ConfigPlatformAcceptanceSave();
         return;
      }
      switch( MData.Item){
         case MI_MODE :
            i = PlatformAcceptance.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_ACCEPTANCE_MODE, _ACCEPTANCE_MODE_LAST)){
               break;
            }
            PlatformAcceptance.Mode = (byte)i;
            break;

         case MI_GENDER :
            i = PlatformAcceptance.Gender;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_GENDER, _GENDER_LAST)){
               break;
            }
            PlatformAcceptance.Gender = (byte)i;
            break;

         case MI_LOW_LIMIT :
            i = PlatformAcceptance.LowLimit;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformAcceptance.LowLimit = (TWeight)i;
            break;

         case MI_LOW_LIMIT_MALE :
         case MI_HIGH_LIMIT :
            i = PlatformAcceptance.HighLimit;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformAcceptance.HighLimit = (TWeight)i;
            break;

         case MI_HIGH_LIMIT_MALE :
         case MI_LOW_LIMIT_FEMALE :
            i = PlatformAcceptance.LowLimitFemale;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformAcceptance.LowLimitFemale = (TWeight)i;
            break;

         case MI_HIGH_LIMIT_FEMALE :
            i = PlatformAcceptance.HighLimitFemale;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformAcceptance.HighLimitFemale = (TWeight)i;
            break;

         case MI_STEP :
            i = PlatformAcceptance.Step;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_ACCEPTANCE_STEP, _ACCEPTANCE_STEP_LAST)){
               break;
            }
            PlatformAcceptance.Step = (byte)i;
            break;

      }
   }
} // MenuMaintenanceAcceptance

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformAcceptanceParameters( int Index, int y, TPlatformAcceptance *Parameters)
// Draw maintenance acceptance parameters
{
   switch( Index){
      case MI_MODE :
         DLabelEnum( Parameters->Mode, ENUM_ACCEPTANCE_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_GENDER :
         DLabelEnum( Parameters->Gender, ENUM_GENDER, DMENU_PARAMETERS_X, y);
         break;

      case MI_LOW_LIMIT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->LowLimit);
         break;

      case MI_LOW_LIMIT_MALE :
      case MI_HIGH_LIMIT :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->HighLimit);
         break;

      case MI_HIGH_LIMIT_MALE :
      case MI_LOW_LIMIT_FEMALE :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->LowLimitFemale);
         break;

      case MI_HIGH_LIMIT_FEMALE :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->HighLimitFemale);
         break;

      case MI_STEP :
         DLabelEnum( Parameters->Step, ENUM_ACCEPTANCE_STEP, DMENU_PARAMETERS_X, y);
         break;

   }
} // PlatformAcceptanceParameters
