Pripominky ke generovani
------------------------

//1. Neumistovat interaktivni reakce do skeletu menu.
   Napr. MenuWeighing.c ma vlozeny potvrzovaci
   dotazy pro start/stop. Misto toho vytvorit
   nove funkce MenuWeighingStart/Stop, kde tyto
   dotazy budou ukryty. V hlavnim formulari potom
   nebude method ale menu. Pri zmene struktury menu
   neni nutne preeditovavat skelet menu

2. Podle bodu 1 umoznit ve formulari zadani prazdneho
   menu. Vygeneruje se jen header a zdrojovy text
   prazdne funkce podle template MenuEmpty.c
   
3. Sjednotit nazvy, napr. Weighing/Start vola
   MenuWeighingConfiguration()
   
4. Krome automatickeho generovani include ve
   skeletu menu doplnit do formulare prikaz
   include (plati pro bezprostredne nasledujici
   definici dat) u dalsi definice neplati (?)   
   
5. Upravit automaticke generovani include pro
   menu doplnit adresar "Menu/MenuXxx.h"
   
6. V hlavicce menu je mozna zbytecne include
   zakladniho souboru (nejspis staci Hardware.h).
   Stejne tak v skeletu menu.
   
7. Pokud je editovani Textu, Date, Time, Clock pridat
   prislusne #include "Gadget/DInput....h" (DTime.h
   je-li pouzito Date, Time, Clock)
   s definicemi input boxu. Taktez doplnit generovani
   stringu STR_ENTER_xxx. Doplnit lokalni promennou
   UDateTime DateTime.

7. V definici menu umoznit jednorazove spusteni
   funkce/metody. Napr. klicova slova trmenu/trmethod
   podle vzoru trigger. Ve switch bude misto
   break return (v template EditXxx.c misto
   break vlozit $BREAK$).
   
8. K souboru Defaults generovat header, kde budou
   definice extern zakladnich struktur a extern const
   default zakladnich struktur.
   
//9. V souboru Default chybi modifikator const pred
   deklaraci TxxxDefault