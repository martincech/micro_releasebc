/********************************************************************************
** Form generated from reading UI file 'devicepanel.ui'
**
** Created: Sun 11. Nov 09:50:41 2012
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEVICEPANEL_H
#define UI_DEVICEPANEL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "uSimulator/lcddisplay.h"

QT_BEGIN_NAMESPACE

class Ui_DevicePanel
{
public:
    QVBoxLayout *verticalLayout;
    QCheckBox *powerOn;
    LcdDisplay *lcdDisplay;
    QGridLayout *gridLayout;
    QPushButton *buttonEsc;
    QSpacerItem *horizontalSpacer_1;
    QPushButton *buttonUp;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *buttonEnter;
    QSpacerItem *verticalSpacer_1;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *verticalSpacer_3;
    QPushButton *buttonLeft;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *buttonDown;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *buttonRight;

    void setupUi(QWidget *DevicePanel)
    {
        if (DevicePanel->objectName().isEmpty())
            DevicePanel->setObjectName(QString::fromUtf8("DevicePanel"));
        DevicePanel->resize(500, 500);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DevicePanel->sizePolicy().hasHeightForWidth());
        DevicePanel->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(DevicePanel);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        powerOn = new QCheckBox(DevicePanel);
        powerOn->setObjectName(QString::fromUtf8("powerOn"));
        powerOn->setFocusPolicy(Qt::NoFocus);

        verticalLayout->addWidget(powerOn);

        lcdDisplay = new LcdDisplay(DevicePanel);
        lcdDisplay->setObjectName(QString::fromUtf8("lcdDisplay"));
        lcdDisplay->setMinimumSize(QSize(480, 320));
        lcdDisplay->setMaximumSize(QSize(480, 320));
        lcdDisplay->setFocusPolicy(Qt::NoFocus);

        verticalLayout->addWidget(lcdDisplay);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        buttonEsc = new QPushButton(DevicePanel);
        buttonEsc->setObjectName(QString::fromUtf8("buttonEsc"));
        buttonEsc->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(buttonEsc, 0, 0, 2, 2);

        horizontalSpacer_1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_1, 0, 2, 2, 1);

        buttonUp = new QPushButton(DevicePanel);
        buttonUp->setObjectName(QString::fromUtf8("buttonUp"));
        buttonUp->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(buttonUp, 0, 3, 2, 2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 5, 2, 1);

        buttonEnter = new QPushButton(DevicePanel);
        buttonEnter->setObjectName(QString::fromUtf8("buttonEnter"));
        buttonEnter->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(buttonEnter, 0, 6, 2, 2);

        verticalSpacer_1 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_1, 1, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 1, 4, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_3, 1, 7, 1, 1);

        buttonLeft = new QPushButton(DevicePanel);
        buttonLeft->setObjectName(QString::fromUtf8("buttonLeft"));
        buttonLeft->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(buttonLeft, 2, 0, 1, 2);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 2, 2, 1, 1);

        buttonDown = new QPushButton(DevicePanel);
        buttonDown->setObjectName(QString::fromUtf8("buttonDown"));
        buttonDown->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(buttonDown, 2, 3, 1, 2);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_4, 2, 5, 1, 1);

        buttonRight = new QPushButton(DevicePanel);
        buttonRight->setObjectName(QString::fromUtf8("buttonRight"));
        buttonRight->setFocusPolicy(Qt::NoFocus);

        gridLayout->addWidget(buttonRight, 2, 6, 1, 2);


        verticalLayout->addLayout(gridLayout);


        retranslateUi(DevicePanel);

        QMetaObject::connectSlotsByName(DevicePanel);
    } // setupUi

    void retranslateUi(QWidget *DevicePanel)
    {
        DevicePanel->setWindowTitle(QApplication::translate("DevicePanel", "Form", 0, QApplication::UnicodeUTF8));
        powerOn->setText(QApplication::translate("DevicePanel", "Power On", 0, QApplication::UnicodeUTF8));
        buttonEsc->setText(QApplication::translate("DevicePanel", "Esc", 0, QApplication::UnicodeUTF8));
        buttonUp->setText(QApplication::translate("DevicePanel", "Up", 0, QApplication::UnicodeUTF8));
        buttonEnter->setText(QApplication::translate("DevicePanel", "Enter", 0, QApplication::UnicodeUTF8));
        buttonLeft->setText(QApplication::translate("DevicePanel", "Left", 0, QApplication::UnicodeUTF8));
        buttonDown->setText(QApplication::translate("DevicePanel", "Down", 0, QApplication::UnicodeUTF8));
        buttonRight->setText(QApplication::translate("DevicePanel", "Right", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DevicePanel: public Ui_DevicePanel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEVICEPANEL_H
