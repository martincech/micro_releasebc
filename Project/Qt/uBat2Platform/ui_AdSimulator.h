/********************************************************************************
** Form generated from reading UI file 'PlatformSimulator.ui'
**
** Created: Wed 17. Apr 10:10:09 2013
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PlatformSimulator_H
#define UI_PlatformSimulator_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PlatformSimulator
{
public:
    QAction *actionOpen;
    QAction *actionPlay;
    QAction *actionPause;
    QAction *actionStop;
    QAction *actionFastBackward;
    QAction *actionBackward;
    QAction *actionForward;
    QAction *actionFastForward;
    QAction *actionFixed;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QSlider *samplesGauge;
    QHBoxLayout *horizontalLayout_2;
    QLabel *currentTimeLabel;
    QLabel *currentTime;
    QSpacerItem *horizontalSpacer_2;
    QLabel *labelValue;
    QLineEdit *editValue;
    QLabel *labelKg;
    QPushButton *buttonSet;
    QHBoxLayout *horizontalLayout_3;
    QLabel *totalTimeLabel;
    QLabel *totalTime;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout;
    QLabel *playbackSpeedLabel;
    QComboBox *playbackSpeed;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *PlatformSimulator)
    {
        if (PlatformSimulator->objectName().isEmpty())
            PlatformSimulator->setObjectName(QString::fromUtf8("PlatformSimulator"));
        PlatformSimulator->resize(639, 184);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/image/platform.png"), QSize(), QIcon::Normal, QIcon::Off);
        PlatformSimulator->setWindowIcon(icon);
        actionOpen = new QAction(PlatformSimulator);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/image/open.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon1);
        actionPlay = new QAction(PlatformSimulator);
        actionPlay->setObjectName(QString::fromUtf8("actionPlay"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/image/play.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPlay->setIcon(icon2);
        actionPause = new QAction(PlatformSimulator);
        actionPause->setObjectName(QString::fromUtf8("actionPause"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/image/pause.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPause->setIcon(icon3);
        actionStop = new QAction(PlatformSimulator);
        actionStop->setObjectName(QString::fromUtf8("actionStop"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/image/stop.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionStop->setIcon(icon4);
        actionFastBackward = new QAction(PlatformSimulator);
        actionFastBackward->setObjectName(QString::fromUtf8("actionFastBackward"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/image/skip-backward.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionFastBackward->setIcon(icon5);
        actionBackward = new QAction(PlatformSimulator);
        actionBackward->setObjectName(QString::fromUtf8("actionBackward"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/image/backward.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionBackward->setIcon(icon6);
        actionForward = new QAction(PlatformSimulator);
        actionForward->setObjectName(QString::fromUtf8("actionForward"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/image/forward.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionForward->setIcon(icon7);
        actionFastForward = new QAction(PlatformSimulator);
        actionFastForward->setObjectName(QString::fromUtf8("actionFastForward"));
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/image/skip-forward.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionFastForward->setIcon(icon8);
        actionFixed = new QAction(PlatformSimulator);
        actionFixed->setObjectName(QString::fromUtf8("actionFixed"));
        actionFixed->setCheckable(true);
        centralWidget = new QWidget(PlatformSimulator);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        samplesGauge = new QSlider(centralWidget);
        samplesGauge->setObjectName(QString::fromUtf8("samplesGauge"));
        samplesGauge->setMaximum(0);
        samplesGauge->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(samplesGauge);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        currentTimeLabel = new QLabel(centralWidget);
        currentTimeLabel->setObjectName(QString::fromUtf8("currentTimeLabel"));
        currentTimeLabel->setMinimumSize(QSize(80, 0));

        horizontalLayout_2->addWidget(currentTimeLabel);

        currentTime = new QLabel(centralWidget);
        currentTime->setObjectName(QString::fromUtf8("currentTime"));
        QFont font;
        font.setFamily(QString::fromUtf8("Courier New"));
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        currentTime->setFont(font);

        horizontalLayout_2->addWidget(currentTime);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        labelValue = new QLabel(centralWidget);
        labelValue->setObjectName(QString::fromUtf8("labelValue"));
        labelValue->setEnabled(false);

        horizontalLayout_2->addWidget(labelValue);

        editValue = new QLineEdit(centralWidget);
        editValue->setObjectName(QString::fromUtf8("editValue"));
        editValue->setEnabled(false);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(editValue->sizePolicy().hasHeightForWidth());
        editValue->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(editValue);

        labelKg = new QLabel(centralWidget);
        labelKg->setObjectName(QString::fromUtf8("labelKg"));

        horizontalLayout_2->addWidget(labelKg);

        buttonSet = new QPushButton(centralWidget);
        buttonSet->setObjectName(QString::fromUtf8("buttonSet"));
        buttonSet->setEnabled(false);
        buttonSet->setMinimumSize(QSize(60, 0));
        buttonSet->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_2->addWidget(buttonSet);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        totalTimeLabel = new QLabel(centralWidget);
        totalTimeLabel->setObjectName(QString::fromUtf8("totalTimeLabel"));
        totalTimeLabel->setMinimumSize(QSize(80, 0));

        horizontalLayout_3->addWidget(totalTimeLabel);

        totalTime = new QLabel(centralWidget);
        totalTime->setObjectName(QString::fromUtf8("totalTime"));
        totalTime->setFont(font);

        horizontalLayout_3->addWidget(totalTime);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        playbackSpeedLabel = new QLabel(centralWidget);
        playbackSpeedLabel->setObjectName(QString::fromUtf8("playbackSpeedLabel"));

        horizontalLayout->addWidget(playbackSpeedLabel);

        playbackSpeed = new QComboBox(centralWidget);
        playbackSpeed->setObjectName(QString::fromUtf8("playbackSpeed"));

        horizontalLayout->addWidget(playbackSpeed);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        PlatformSimulator->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(PlatformSimulator);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        PlatformSimulator->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(PlatformSimulator);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        PlatformSimulator->setStatusBar(statusBar);

        mainToolBar->addAction(actionOpen);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionPlay);
        mainToolBar->addAction(actionPause);
        mainToolBar->addAction(actionStop);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionFastBackward);
        mainToolBar->addAction(actionBackward);
        mainToolBar->addAction(actionForward);
        mainToolBar->addAction(actionFastForward);
        mainToolBar->addSeparator();
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionFixed);

        retranslateUi(PlatformSimulator);

        playbackSpeed->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(PlatformSimulator);
    } // setupUi

    void retranslateUi(QMainWindow *PlatformSimulator)
    {
        PlatformSimulator->setWindowTitle(QApplication::translate("PlatformSimulator", "Bat2 Platform Simulator", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("PlatformSimulator", "Open", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionOpen->setToolTip(QApplication::translate("PlatformSimulator", "Open simulator file", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionPlay->setText(QApplication::translate("PlatformSimulator", "Play", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionPlay->setToolTip(QApplication::translate("PlatformSimulator", "Start playback", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionPause->setText(QApplication::translate("PlatformSimulator", "Pause", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionPause->setToolTip(QApplication::translate("PlatformSimulator", "Pause playback", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionStop->setText(QApplication::translate("PlatformSimulator", "Stop", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionStop->setToolTip(QApplication::translate("PlatformSimulator", "Stop playback", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionFastBackward->setText(QApplication::translate("PlatformSimulator", "FastBackward", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionFastBackward->setToolTip(QApplication::translate("PlatformSimulator", "Move fast backward", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionBackward->setText(QApplication::translate("PlatformSimulator", "Backward", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionBackward->setToolTip(QApplication::translate("PlatformSimulator", "Move backward", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionForward->setText(QApplication::translate("PlatformSimulator", "Forward", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionForward->setToolTip(QApplication::translate("PlatformSimulator", "Move forward", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionFastForward->setText(QApplication::translate("PlatformSimulator", "FastForward", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionFastForward->setToolTip(QApplication::translate("PlatformSimulator", "Move fast forward", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionFixed->setText(QApplication::translate("PlatformSimulator", "Fixed", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionFixed->setToolTip(QApplication::translate("PlatformSimulator", "Fixed value", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        currentTimeLabel->setText(QApplication::translate("PlatformSimulator", "Current time :", 0, QApplication::UnicodeUTF8));
        currentTime->setText(QApplication::translate("PlatformSimulator", "00:00:00", 0, QApplication::UnicodeUTF8));
        labelValue->setText(QApplication::translate("PlatformSimulator", "Value :", 0, QApplication::UnicodeUTF8));
        editValue->setText(QApplication::translate("PlatformSimulator", "0", 0, QApplication::UnicodeUTF8));
        labelKg->setText(QApplication::translate("PlatformSimulator", "kg", 0, QApplication::UnicodeUTF8));
        buttonSet->setText(QApplication::translate("PlatformSimulator", "Set", 0, QApplication::UnicodeUTF8));
        totalTimeLabel->setText(QApplication::translate("PlatformSimulator", "Total time :", 0, QApplication::UnicodeUTF8));
        totalTime->setText(QApplication::translate("PlatformSimulator", "00:00:00", 0, QApplication::UnicodeUTF8));
        playbackSpeedLabel->setText(QApplication::translate("PlatformSimulator", "Playback speed :", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class PlatformSimulator: public Ui_PlatformSimulator {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PlatformSimulator_H
