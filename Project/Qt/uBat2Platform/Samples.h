//******************************************************************************
//
//   Samples.h   Weighing samples loader
//   Version 1.0 (c) VEIT Electronics
//
//******************************************************************************

#ifndef SAMPLES_H
#define SAMPLES_H

#include <QString>

#ifndef __WeighingSimulation_H__
   #include "Bat2Platform/WeighingSimulation.h"
#endif

//------------------------------------------------------------------------------
//   Samples
//------------------------------------------------------------------------------

class Samples
{
public:
   Samples();
   ~Samples();

   bool load( QString fileName);
   bool loadBinary(QString fileName);

   int            count();
   TWeightSample *samples();

private:
   TWeightSample *_samples;
   int            _samplesCount;
}; // Samples

//------------------------------------------------------------------------------

#endif // MAINWINDOW_H
