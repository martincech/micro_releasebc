#include "MainClass.h"
#include "Memory/FileRemote.h"
#include "Action/ActionRemote.h"

#include "Scheduler/WeighingPlanList.h"
#include "Message/ContactList.h"
#include "Archive/Archive.h"
#include "Storage/Sample.h"
#include "Config/Config.h"
#include "Curve/CorrectionList.h"
#include "Curve/CurveList.h"
#include "Predefined/PredefinedList.h"

MainClass::MainClass(QObject *parent) :
   QObject(parent)
{
}

void MainClass::connectClicked(TcpClient &rem)
{
   Socket = new SocketIfEthernet((SocketIfEthernet &)rem);
   w = new MainWindow();
   w->setWindowTitle( "Bat2 Simulator");
   w->setAttribute(Qt::WA_QuitOnClose);
   // nonvolatile memory initialization :
   w->show();


   FileRemoteSetup(Socket);
   ActionRemoteSetup(Socket);

}
