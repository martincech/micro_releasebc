#include <QtCore/QCoreApplication>
#include <QString>
#include <QFile>
#include <QTextStream>

static bool IsValidPinName(QString PinName);
static QString PinNameToPortName(QString PinName);
static int PinNameToPin(QString PinName);

int main(int argc, char *argv[])
{
   QCoreApplication a(argc, argv);
   
   if(argc < 3) {
      return 0;
   }

   QString InputFileName = QString(argv[1]);
   QString OutputFileName = QString(argv[2]);

   QFile OutputFile(OutputFileName);
   OutputFile.open(QIODevice::WriteOnly | QIODevice::Text);

   QTextStream OutputStream(&OutputFile);

   QFile InputFile(InputFileName);
   InputFile.open(QIODevice::ReadOnly | QIODevice::Text);

   QTextStream InputStream(&InputFile);

   while (!InputStream.atEnd()) {
      QString Line = InputStream.readLine();
      QTextStream LineStream(&Line);

      QString PinNumber;
      QString Dummy;
      QString PinName;
      QString Default;
      QString Alt[8];

      LineStream >> PinNumber;

      if(!PinNumber.toInt()) {
         continue;
      }

      LineStream >> Dummy;

      LineStream >> PinName;

      if(!IsValidPinName(PinName)) {
         continue;
      }

      LineStream >> Default;
      LineStream >> Alt[0];
      LineStream >> Alt[1];
      LineStream >> Alt[2];
      LineStream >> Alt[3];
      LineStream >> Alt[4];
      LineStream >> Alt[5];
      LineStream >> Alt[6];
      LineStream >> Alt[7];

      QString PortName = PinNameToPortName(PinName);
      int Pin = PinNameToPin(PinName);

      OutputStream << QString().sprintf("/* %s%02d */", PortName.toAscii().data(), Pin) << endl;
      OutputStream << "#define " << QString().sprintf("KINETIS_PIN_%s%02d", PortName.toAscii().data(), Pin) << " " << QString("").sprintf("PIN_NUMBER(KINETIS_%s, %d)", PortName.toAscii().data(), Pin) << endl;

      for(int i = 0 ; i < 7 ; i++) {
         if(Alt[i].length() == 0) {
            break;
         } else if(Alt[i] == "*") {
            continue;
         }

         QString AltName = Alt[i].replace("/", "_");

         OutputStream << "#define " << QString().sprintf("KINETIS_PIN_%s_%s%02d", AltName.toAscii().data(), PortName.toAscii().data(), Pin) << " " << QString().sprintf("KINETIS_PIN_%s%02d", PortName.toAscii().data(), Pin) << endl;
         OutputStream << "#define " << QString().sprintf("KINETIS_PIN_%s_%s%02d_FUNCTION", AltName.toAscii().data(), PortName.toAscii().data(), Pin) << " " << i << endl;
      }

      OutputStream << endl;

      int a = 8;
   }

   OutputFile.close();

   return a.exec();
}

static bool IsValidPinName(QString PinName) {
   if(PinName.startsWith("PT")) {
      return true;
   }

   return false;
}

static QString PinNameToPortName(QString PinName) {
   return "PORT" + PinName[2];
}

static int PinNameToPin(QString PinName) {
   int Pin = PinName[3].digitValue();

   if(PinName[4].isDigit()) {
      Pin = Pin * 10 + PinName[4].digitValue();
   }

   return Pin;
}
