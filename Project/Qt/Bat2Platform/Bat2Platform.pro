#-------------------------------------------------
#
#  Bat2Platform project
#
#-------------------------------------------------

QT       += core gui network

TARGET = Bat2Platform
TEMPLATE = app

INCLUDEPATH  = ../../../Library/Qt/Bat2Platform
INCLUDEPATH  = ../../../Library/Qt
INCLUDEPATH += ../../../Library/Bat2Platform
INCLUDEPATH += ../../../Library/Bat2
INCLUDEPATH += ../../../Library/Bat
INCLUDEPATH += ../../../Library/Micro
INCLUDEPATH += ../../../Library/Device
INCLUDEPATH += ../../../Library/Win32

DEFINES += OPTION_SIGMA_DELTA
DEFINES += OPTION_PICOSTRAIN
DEFINES += OPTION_SIMULATION

RC_FILE   = Bat2Platform.rc

SOURCES += main.cpp\
        mainwindow.cpp \
    ../../../Library/Qt/Crt/crtdump.cpp \
    ../../../Library/Qt/Crt/crt.cpp \
    ../../../Library/Qt/Socket/localserver.cpp \
    ../../../Library/Qt/Bat2Platform/Rcs.cpp \
    ../../../Library/Qt/Bat2Platform/SysClock.c \
    ../../../Library/Micro/Time/uDateTime.c \
    ../../../Library/Micro/Time/uDate.c \
    ../../../Library/Micro/Time/uClock.c \
    ../../../Library/Micro/Time/uTime.c \
    ../../../Library/Bat/Filter/FilterRelative.c \
    ../../../Library/Bat2Platform/Fifo/Fifo.c \
    ../../../Library/Bat2Platform/Platform/Acceptance.c \
    ../../../Library/Bat2Platform/Platform/Platform.c \
    ../../../Library/Bat2Platform/Remote/RcsCommand.c \
    ../../../Library/Qt/Bat2Platform/Samples.cpp \
    ../../../Library/Bat2Platform/Platform/Wepl.c \
    ../../../Library/Qt/Bat2Platform/pWeighing.c \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.cpp

HEADERS  += mainwindow.h \
    ../../../Library/Qt/Crt/crt.h \
    ../../../Library/Qt/Crt/crtdump.h \
    ../../../Library/Qt/Socket/localserver.h \
    ../../../Library/Qt/Bat2Platform/Rcs.h \
    ../../../Library/Qt/Bat2Platform/WeighingSimulation.h \
    ../../../Library/Bat/Filter/FilterRelative.h \
    ../../../Library/Bat/Device/VersionDef.h \
    ../../../Library/Bat2Platform/Fifo/Fifo.h \
    ../../../Library/Bat2Platform/Config/Bat2WireDef.h \
    ../../../Library/Bat2Platform/Platform/PlatformRpc.h \
    ../../../Library/Bat2Platform/Platform/PlatformDef.h \
    ../../../Library/Bat2Platform/Platform/Platform.h \
    ../../../Library/Bat2Platform/Platform/Wepl.h \
    ../../../Library/Bat2Platform/Platform/Acceptance.h \
    ../../../Library/Bat2Platform/Remote/RcsCommand.h \
    ../../../Library/Bat2Platform/Weighing/Weighing.h \
    Hardware.h \
    Samples.h \
    ../../../Library/Qt/Bat2Platform/Samples.h \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.h

FORMS    += mainwindow.ui \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.ui

RESOURCES += \
    Bat2Platform.qrc \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.qrc
