//******************************************************************************
//
//   main.cpp     Bat2Platform simulator main
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include <QtGui/QApplication>
#include "mainwindow.h"

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
   QApplication a(argc, argv);
   MainWindow w;
   w.show();
   
   return a.exec();
} // main

