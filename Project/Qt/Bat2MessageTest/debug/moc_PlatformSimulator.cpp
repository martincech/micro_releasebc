/****************************************************************************
** Meta object code from reading C++ file 'PlatformSimulator.h'
**
** Created: Fri 9. Aug 12:30:34 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PlatformSimulator.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PlatformSimulator[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x08,
      45,   18,   18,   18, 0x08,
      78,   72,   18,   18, 0x08,
     129,  120,   18,   18, 0x08,
     162,   18,   18,   18, 0x08,
     195,   18,   18,   18, 0x08,
     227,   18,   18,   18, 0x08,
     253,   18,   18,   18, 0x08,
     280,   18,   18,   18, 0x08,
     306,   18,   18,   18, 0x08,
     340,   18,   18,   18, 0x08,
     370,   18,   18,   18, 0x08,
     399,   18,   18,   18, 0x08,
     432,   18,   18,   18, 0x08,
     455,   18,   18,   18, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_PlatformSimulator[] = {
    "PlatformSimulator\0\0on_actionOpen_triggered()\0"
    "on_actionFixed_triggered()\0index\0"
    "on_playbackSpeed_currentIndexChanged(int)\0"
    "position\0on_samplesGauge_sliderMoved(int)\0"
    "on_samplesGauge_sliderReleased()\0"
    "on_samplesGauge_sliderPressed()\0"
    "on_actionPlay_triggered()\0"
    "on_actionPause_triggered()\0"
    "on_actionStop_triggered()\0"
    "on_actionFastBackward_triggered()\0"
    "on_actionBackward_triggered()\0"
    "on_actionForward_triggered()\0"
    "on_actionFastForward_triggered()\0"
    "on_buttonSet_clicked()\0timerSlowTimeout()\0"
};

void PlatformSimulator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PlatformSimulator *_t = static_cast<PlatformSimulator *>(_o);
        switch (_id) {
        case 0: _t->on_actionOpen_triggered(); break;
        case 1: _t->on_actionFixed_triggered(); break;
        case 2: _t->on_playbackSpeed_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_samplesGauge_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_samplesGauge_sliderReleased(); break;
        case 5: _t->on_samplesGauge_sliderPressed(); break;
        case 6: _t->on_actionPlay_triggered(); break;
        case 7: _t->on_actionPause_triggered(); break;
        case 8: _t->on_actionStop_triggered(); break;
        case 9: _t->on_actionFastBackward_triggered(); break;
        case 10: _t->on_actionBackward_triggered(); break;
        case 11: _t->on_actionForward_triggered(); break;
        case 12: _t->on_actionFastForward_triggered(); break;
        case 13: _t->on_buttonSet_clicked(); break;
        case 14: _t->timerSlowTimeout(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PlatformSimulator::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PlatformSimulator::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_PlatformSimulator,
      qt_meta_data_PlatformSimulator, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PlatformSimulator::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PlatformSimulator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PlatformSimulator::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PlatformSimulator))
        return static_cast<void*>(const_cast< PlatformSimulator*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int PlatformSimulator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
