//******************************************************************************
//
//    main.cpp     Bat2compact simulator main
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include <QtGui/QApplication>
#include <QDir>

#include "uSimulator/mainwindow.h"
#include "PlatformSimulator/PlatformSimulator.h"
#include "Memory/Nvm.h"
#include "File/Efs.h"
#include "Uart/WinUart.h"
#include "Uart/UartParameters.h"
#include "Uart/UartModem.h"
#include "Gsm/Gsm.h"
#include "Log/Log.h"
#include "Log/Logger.h"
#define NVM_FILE_NAME   "NvmMemory.bin"
#define EFS_NAME        "Root"

int main(int argc, char *argv[])
{
   QApplication a(argc, argv);
   MainWindow w;
   w.setWindowTitle( "Bat2 Simulator");
   w.setAttribute(Qt::WA_QuitOnClose);
   w.show();
  //QObject::connect( qApp, SIGNAL(lastWindowClosed()), qApp, SLOT(quit()) );
   PlatformSimulator sim;
   sim.show();

   // nonvolatile memory initialization :
   NvmInit();
   QString nvmPath;
   nvmPath  = QDir::currentPath();         // get working directory
   nvmPath += QString( "/" NVM_FILE_NAME);
   NvmSetup( (char *)nvmPath.toStdString().c_str());

   // external file system :
   QString efsPath;
   efsPath  = QDir::currentPath();         // get working directory
   efsPath += QString( "/" EFS_NAME);
   EfsRoot( efsPath.toAscii());

/*
   // GSM modem :
   const char *GsmName = "COM10";
   if( argc > 2){
      GsmName = argv[ 2];
   }
   WinUart *uart = new WinUart;
   uart->setPort( GsmName);
   UartModemSetup( uart, 0);
*/
   a.exec();         // Qt main loop

   NvmShutdown();
   return( 0);
} // main
