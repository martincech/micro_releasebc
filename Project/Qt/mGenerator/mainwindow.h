#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Parse/Csv.h"
#include "mGenerator/mGenerator.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_actionRegisters_triggered();

    void on_actionOpen_triggered();

    void on_actionInputFile_triggered();

    void on_actionPrevious_triggered();

    void on_actionNext_triggered();

    void on_actionGlobalHeaders_triggered();

    void on_actionGlobalSources_triggered();

    void on_actionRegisterGroupsC_triggered();

    void on_actionRegisterGroupsH_triggered();

    void on_actionSave_triggered();

    void on_actionOpenDefault_triggered();

    void on_actionSave_Stack_triggered();

private:
    Ui::MainWindow *ui;

    QString     fileName;
    mGenerator *generator;
    Csv        *csvRegisters;

    int         pageRegistersIndex;
    QStringList genFileGlobalHValues;
    QStringList genFileGlobalHFileNames;
    QStringList genFileGlobalCValues;
    QStringList genFileGlobalCFileNames;
    int         genFileGlobalIndex;

    QStringList genFileGroupHValues;
    QStringList genFileGroupHFileNames;
    QStringList genFileGroupCValues;
    QStringList genFileGroupCFileNames;
    int         genFileGroupIndex;

    void openFile( QString fName);
};

#endif // MAINWINDOW_H
