#-------------------------------------------------
#
# Project created by QtCreator 2013-08-07T14:38:44
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Bat2UsbTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH  = ../../../Library/Bat2/Compact
INCLUDEPATH += ../../../Library/Bat2
INCLUDEPATH += ../../../Library/Bat
INCLUDEPATH += ../../../Library/Qt
INCLUDEPATH += ../../../Library/Kinetis
INCLUDEPATH += ../../../Library/Device
INCLUDEPATH += ../../../Library/Micro
INCLUDEPATH += ../../../Library/Micro/Unity
INCLUDEPATH += ../../../Library/Micro/CMock
INCLUDEPATH += ../../../Library/Micro/CException
INCLUDEPATH += .

DEFINES += UNITY_OUTPUT_CHAR=dbgPutchar

SOURCES += \
    ../../../Library/Bat2/Usb/Usb.c \
    ../../../Library/Bat2/Usb/TestUsb.c \
    ../../../Library/Micro/Unity/unity_fixture.c \
    ../../../Library/Micro/Unity/unity.c \
    ../../../Library/Qt/System/SysClock.c \
    ../../../Library/Micro/Time/uTime.c \
    ../../../Library/Micro/Time/uDateTime.c \
    ../../../Library/Micro/Time/uDate.c \
    ../../../Library/Micro/Time/uClock.c \
    ../../../Library/Bat/Country/Country.c \
    ../../../Library/Bat/Console/xprintf.c \
    ../../../Library/Bat/Console/xprint.c \
    ../../../Library/Bat/Console/sputchar.c \
    ../../../Library/Bat/Console/conio.c \
    ../../../Library/Micro/Convert/uEndian.c \
    ../../../Library/Micro/Convert/uBcd.c \
    ../../../Library/Bat2/Compact/MockPower.c \
    ../../../Library/Bat2/Usb/MockUsbDevice.c \
    ../../../Library/Device/Iadc/MockIAdc.c \
    ../../../Library/Kinetis/Usb/MockUsbModule.c \
    ../../../Library/Micro/CMock/cmock.c \
    ../../../Library/Bat2/Usb/MockUsbHardware.c \
    ../../../Library/Micro/CException/CException.c \
    ../../../Library/Qt/UnityAddOn/TestApp.cpp \
    ../../../Library/Kinetis/Usb/MockUsbCharger.c

HEADERS += \
    Hardware.h \
    ../../../Library/Micro/Test/TestRunner.h \
    ../../../Library/Qt/UnityAddOn/TestApp.h
