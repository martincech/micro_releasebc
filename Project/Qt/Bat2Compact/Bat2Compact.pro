#-------------------------------------------------
#
#  Bat2Compact project
#
#-------------------------------------------------

QT       += core gui network

TARGET    = Bat2Compact
#CONFIG   += console
CONFIG   += warn_off
QMAKE_CFLAGS_DEBUG     += -Wall -std=c99
QMAKE_CFLAGS_RELEASE   += -Wall -std=c99
QMAKE_CXXFLAGS_DEBUG   += -Wall
QMAKE_CXXFLAGS_RELEASE += -Wall
TEMPLATE  = app

RC_FILE   = Bat2Compact.rc


INCLUDEPATH  = ../../../Library/Bat2/Compact
INCLUDEPATH += ../../../Library/Qt
INCLUDEPATH += ../../../Library/
INCLUDEPATH += ../../../Library/Qt/Bat2Platform
INCLUDEPATH += ../../../Library/Qt/fnet
INCLUDEPATH += ../../../Library/Qt/Modbus
INCLUDEPATH += ../../../Library/Bat2
INCLUDEPATH += ../../../Library/Bat2Platform/Compact
INCLUDEPATH += ../../../Library/Bat2Platform
INCLUDEPATH += ../../../Library/Bat
INCLUDEPATH += ../../../Library/Device
INCLUDEPATH += ../../../Library/Device/Ethernet
INCLUDEPATH += ../../../Library/Device/Ethernet/dhcp
INCLUDEPATH += ../../../Library/Device/Ethernet/dns
INCLUDEPATH += ../../../Library/Device/Ethernet/poll
INCLUDEPATH += ../../../Library/Device/Ethernet/serial
INCLUDEPATH += ../../../Library/Device/Modbus
INCLUDEPATH += ../../../Library/Bat2/Modbus
INCLUDEPATH += ../../../Library/Micro
INCLUDEPATH += ../../../Library/Win32
INCLUDEPATH += ../../../Library/Kinetis/Ethernet
INCLUDEPATH += ../../../Library/Win32/WinPcap/Include

LIBS += -lws2_32
LIBS += -L../../../Library/Win32/WinPcap/Lib -lwpcap

DEFINES += OPTION_BAT2_COMPACT
DEFINES += OPTION_SIMULATION
DEFINES += OPTION_SIGMA_DELTA
DEFINES += _EXPORTING
DEFINES += _WIN32
#DEFINES += OPTION_PICOSTRAIN
#DEFINES += KBD_NUMERIC

#define same macros for DEBUG & RELEASE as in Crossworks project
CONFIG(debug, debug|release) {
   DEFINES += DEBUG
   DEFINES += __UDEBUG__
#   DEFINES += _HTTP_DEBUGGING_
   DEFINES += __GSM_SMS_STATISTICS__
   #DEFINES += STORAGE_ALLWAYS_DEFAULT
}
CONFIG(release, debug|release) {
   DEFINES += NDEBUG
}


SOURCES += main.cpp \
    ../../../Library/Bat2Platform/Platform/Acceptance.c \
    ../../../Library/Bat2Platform/Fifo/Fifo.c \
    ../../../Library/Bat/Filter/FilterRelative.c \
    ../../../Library/Bat2/Compact/Config/Config.c \
    ../../../Library/Bat2Platform/Compact/Platform/Platform.c \
    ../../../Library/Bat2Platform/Device/Status.c \
    ../../../Library/Bat2/Compact/Menu/MenuMaintenanceCalibration.c \
    ../../../Library/Qt/Bat2Platform/Samples.cpp \
    ../../../Library/Bat2Platform/Platform/Wepl.c \
    ../../../Library/Bat2/Weighing/Weighing.c \
    ../../../Library/Qt/Bat2Platform/pWeighing.c \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.cpp \
    ../../../Library/Bat2Platform/Calibration/Sigma/Calibration.c \
    ../../../Library/Device/Ad7192/Ad7192fDummy.c \
    ../../../Library/Bat2/Diagnostic/DiagnosticFifo.c \
    ../../../Library/Bat2/Diagnostic/Diagnostic.c \
    ../../../Library/Bat2/Dummy/Usb.c \
    ../../../Library/Qt/uSimulator/Power.cpp \
    ../../../Library/Bat2/Log/Log.c \
    ../../../Library/Bat2/Menu/MenuDateTime.c \
    ../../../Library/Bat2/Dummy/Sleep.c \
    ../../../Library/Bat2/Dummy/Accu.c \
    ../../../Library/Bat2/Dummy/MenuPower.c \
    ../../../Library/Bat/Gadget/DGraph.c \
    ../../../Library/Bat2/Graph/MenuGraph.c \
    ../../../Library/Bat2/Megavi/MegaviTask.c \
    ../../../Library/Bat2/Megavi/MegaviConfig.c \
    ../../../Library/Bat2/Message/MGsm.c \
    ../../../Library/Bat/Menu/MenuExit.c \
    ../../../Library/Bat2/Dummy/Remote.c \
    ../../../Library/Bat2/Config/Context.c \
    ../../../Library/Bat2/Menu/MenuRs485.c \
    ../../../Library/Bat2/Menu/MenuRs485Interface.c \
    ../../../Library/Device/Rs485/Rs485Config.c \
    ../../../Library/Bat2/Dummy/Megavi.cpp \
    ../../../Library/Bat2/Scheduler/WeighingPlanList.c \
    ../../../Library/Qt/System/SysClock.c \
    ../../../Library/Bat2/Ethernet/MenuEthernetInfo.c \
    ../../../Library/Bat2/Scheduler/MenuWeighingPlans.c \
    ../../../Library/Device/Remote/Frame.c \
    ../../../Library/Bat2/Dummy/SocketIfUart.c \
    ../../../Library/Bat2/Megavi/MessageMegavi.c \
    ../../../Library/Bat2/Dacs/DacsTask.c \
    ../../../Library/Bat2/Dacs/DacsConfig.c \
    ../../../Library/Bat2/Dummy/Dacs.cpp \
    ../../../Library/Device/Rs485/Rs485.c \
    ../../../Library/Qt/uSimulator/Nvm.cpp \
    ../../../Library/Device/Memory/File.c \
    ../../../Library/Bat2/Usb/MenuUsbDummy.c \
    ../../../Library/Device/Memory/FileRemoteDummy.c \
    ../../../Library/Qt/File/Efs.cpp \
    ../../../Library/Bat2/Menu/MenuWeighingTargetAdjust.c \
    ../../../Library/Bat2/Menu/MenuSmsStatistics.c \
    ../../../Library/Bat/Gadget/DTimeRange.c \
    ../../../Library/Bat2/Message/MenuGsmPowerOptionsTimes.c \
    ../../../Library/Device/Memory/FileNvm.c \
    ../../../Library/Bat2/Menu/MenuConfig.c \
    ../../../Library/Bat2/Platform/ZoneConfig.c \
    ../../../Library/Bat2/Memory/FileDef.c \
    ../../../Library/Bat2/Config/State.c \
    ../../../Library/Device/Memory/Ram.c \
    ../../../Library/Bat2/Compact/Sys.c \
    ../../../Library/Bat2/Compact/Gui.c \
    ../../../Library/Bat2/Communication/Communication.c \
    ../../../Library/Bat2/Communication/MenuCellularData.c \
    ../../../Library/Bat2/Communication/MenuDataPublication.c \
    ../../../Library/Bat2/Communication/MenuEthernet.c \
    ../../../Library/Bat2/Communication/MenuInternetConnection.c \
    ../../../Library/Bat2/Communication/MenuWifi.c \
    ../../../Library/Bat/Gadget/DIp.c \
    ../../../Library/Bat/Internet/xIp.c \
    ../../../Library/Bat2/Menu/MenuCommunication.c \
    ../../../Library/Bat2/Compact/Config/FactoryDefault.c \
    ../../../Library/Bat/Gadget/DEnterIp.c \
    ../../../Library/Device/Ethernet/http/client/HTTPClient.c \
    ../../../Library/Device/Ethernet/http/client/HTTPClientAuth.c \
    ../../../Library/Device/Ethernet/http/client/HTTPClientString.c \
    ../../../Library/Device/Ethernet/http/client/HTTPClientWrapper.c \
    ../../../Library/Bat2/SmsGate/SmsGate.c \
    ../../../Library/Bat2/Communication/InternetPublication.c \
    ../../../Library/Device/Multitasking/Tasks.c \
    ../../../Library/Qt/Multitasking/Multitasking.cpp \
    ../../../Library/Bat2/Compact/Tasks/TasksDef.c \
    ../../../Library/Bat2/Dummy/MenuPowerFailure.c \
    ../../../Library/Bat2/Dummy/MenuRemote.c \
    ../../../Library/Qt/Multitasking/Worker.cpp \
    ../../../Library/Device/Gsm/Gsm.c \
    ../../../Library/Device/Gsm/GsmDefPrv.c \
    ../../../Library/Device/Memory/Mempool.c \
    ../../../Library/Win32/Uart/UartModem.cpp \
    ../../../Library/Win32/Uart/WinUart.cpp \
    ../../../Library/Qt/Uart/uartparameters.cpp \
    ../../../Library/Qt/Bat2/Compact/AppMain.cpp \
    ../../../Library/Bat2/SmsGate/SmsGateConfig.c \
    ../../../Library/Device/Ethernet/fnet_arp.c \
    ../../../Library/Device/Ethernet/fnet_error.c \
    ../../../Library/Device/Ethernet/fnet_eth.c \
    ../../../Library/Device/Ethernet/fnet_checksum.c \
    ../../../Library/Device/Ethernet/fnet_icmp.c \
    ../../../Library/Device/Ethernet/fnet_icmp6.c \
    ../../../Library/Device/Ethernet/fnet_igmp.c \
    ../../../Library/Device/Ethernet/fnet_inet.c \
    ../../../Library/Device/Ethernet/fnet_ip.c \
    ../../../Library/Device/Ethernet/fnet_ip6.c \
    ../../../Library/Device/Ethernet/fnet_isr.c \
    ../../../Library/Device/Ethernet/fnet_loop.c \
    ../../../Library/Device/Ethernet/fnet_mempool.c \
    ../../../Library/Device/Ethernet/fnet_mld.c \
    ../../../Library/Device/Ethernet/fnet_nd6.c \
    ../../../Library/Device/Ethernet/fnet_netbuf.c \
    ../../../Library/Device/Ethernet/fnet_netif.c \
    ../../../Library/Device/Ethernet/fnet_prot.c \
    ../../../Library/Device/Ethernet/fnet_raw.c \
    ../../../Library/Device/Ethernet/fnet_socket.c \
    ../../../Library/Device/Ethernet/fnet_stack.c \
    ../../../Library/Device/Ethernet/fnet_stdlib.c \
    ../../../Library/Device/Ethernet/fnet_tcp.c \
    ../../../Library/Device/Ethernet/fnet_tcp_gsm.c \
    ../../../Library/Device/Ethernet/fnet_timer.c \
    ../../../Library/Device/Ethernet/fnet_udp.c \
    ../../../Library/Device/Ethernet/fnet_udp_gsm.c \
    ../../../Library/Device/Ethernet/dhcp/fnet_dhcp.c \
    ../../../Library/Device/Ethernet/dns/fnet_dns.c \
    ../../../Library/Device/Ethernet/poll/fnet_poll.c \
    ../../../Library/Device/Ethernet/serial/fnet_serial.c \
    ../../../Library/Kinetis/Ethernet/fnet_cpu.c \
    ../../../Library/Qt/fnet/fnet_win32.cpp \
    ../../../Library/Qt/fnet/fnet_win32_prv.cpp \
    ../../../Library/Qt/fnet/cputimer.cpp \
    ../../../Library/Device/Ethernet/EthernetLibrary.c \
    ../../../Library/Qt/fnet/FNetLogWindow.cpp \
    ../../../Library/Qt/Bootloader/BootloaderTrigger.c \
    ../../../Library/Bat2/Modbus/ModbusCoils.c \
    ../../../Library/Bat2/Modbus/ModbusConfig.c \
    ../../../Library/Bat2/Modbus/ModbusDiscreteInputs.c \
    ../../../Library/Bat2/Modbus/ModbusHoldingRegisters.c \
    ../../../Library/Bat2/Modbus/ModbusInputRegisters.c \
    ../../../Library/Bat2/Modbus/ModbusSlaveId.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusCalibrationGroup.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusConfigurationGroup.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusGsmContactsGroup.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusCharacterBufferGroup.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusPredefinedCorrectionCurvesGroup.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusPredefinedGrowthCurvesGroup.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusPredefinedWeighingPlansGroup.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusPredefinedWeighingsGroup.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusReg.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusRegRangeCheck.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusSmsGateGroup.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusStatisticsGroup.c \
    ../../../Library/Bat2/Modbus/Slave/ModbusWeighingGroup.c \
    ../../../Library/Device/Modbus/mb.c \
    ../../../Library/Device/Modbus/mbascii.c \
    ../../../Library/Device/Modbus/mbcrc.c \
    ../../../Library/Device/Modbus/mbfunccoils.c \
    ../../../Library/Device/Modbus/mbfuncdiag.c \
    ../../../Library/Device/Modbus/mbfuncdisc.c \
    ../../../Library/Device/Modbus/mbfuncinput.c \
    ../../../Library/Device/Modbus/mbfuncother.c \
    ../../../Library/Device/Modbus/mbfuncholding.c \
    ../../../Library/Device/Modbus/mbrtu.c \
    ../../../Library/Device/Modbus/mbtcp.c \
    ../../../Library/Device/Modbus/mbutils.c \
    ../../../Library/Qt/Modbus/portevent.c \
    ../../../Library/Qt/Modbus/portother.c \
    ../../../Library/Qt/Modbus/portserial.c \
    ../../../Library/Qt/Modbus/porttimer.c \
    ../../../Library/Micro/Crc/Crc.c \
    ../../../Library/Qt/SensorPack/Sensors.c \
    ../../../Library/Bat2/Modbus/ModbusHoldingRegistersMaster.c

SOURCES +=
SOURCES += ../../../Library/Bat2/Compact/Str.c
SOURCES += ../../../Library/Bat2/Compact/Bitmap.c
SOURCES += ../../../Library/Bat2/Compact/Fonts.c
SOURCES +=
SOURCES +=
#SOURCES += ../../../Library/Bat2/Dummy/Power.c
SOURCES += ../../../Library/Bat2/Archive/Archive.c
SOURCES += ../../../Library/Bat2/Archive/MenuArchive.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Curve/GrowthCurve.c
SOURCES += ../../../Library/Bat2/Curve/CurveList.c
SOURCES += ../../../Library/Bat2/Curve/MenuGrowthCurve.c
SOURCES += ../../../Library/Bat2/Curve/CorrectionCurve.c
SOURCES += ../../../Library/Bat2/Curve/CorrectionList.c
SOURCES += ../../../Library/Bat2/Curve/MenuCorrectionCurve.c
SOURCES += ../../../Library/Bat2/Display/DisplayConfiguration.c
SOURCES += ../../../Library/Bat2/Display/MenuContrast.c
SOURCES += ../../../Library/Bat2/Display/MenuBacklightIntensity.c
SOURCES += ../../../Library/Bat2/Display/MenuDisplay.c
SOURCES += ../../../Library/Bat2/Display/MenuBacklight.c
SOURCES += ../../../Library/Bat2/Menu/Menu.c
SOURCES += ../../../Library/Bat2/Menu/MenuMain.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighing.c
SOURCES += ../../../Library/Bat2/Menu/MenuStatistics.c
SOURCES += ../../../Library/Bat2/Menu/MenuUserSettings.c
SOURCES += ../../../Library/Bat2/Menu/MenuConfiguration.c
SOURCES += ../../../Library/Bat2/Menu/MenuMaintenance.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingStart.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingStartLater.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingStop.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingSuspend.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingRelease.c
SOURCES += ../../../Library/Bat2/Menu/MenuConfigurationWeighing.c
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingConfiguration.c
SOURCES += ../../../Library/Bat2/Menu/MenuInitialWeights.c
SOURCES += ../../../Library/Bat2/Menu/MenuGrowthCurves.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Menu/MenuWeighingMenuSelection.c
SOURCES += ../../../Library/Bat2/Menu/MenuTargetWeights.c
SOURCES += ../../../Library/Bat2/Menu/MenuDetection.c
SOURCES += ../../../Library/Bat2/Menu/MenuAcceptance.c
SOURCES += ../../../Library/Bat2/Menu/MenuAcceptanceData.c
SOURCES += ../../../Library/Bat2/Menu/MenuPlatformInfo.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Menu/MenuMaintenanceSimulation.c
SOURCES += ../../../Library/Bat2/Menu/Password.c
SOURCES += ../../../Library/Bat2/Menu/Screen.c
SOURCES += ../../../Library/Bat2/Menu/ScreenStatistic.c
SOURCES += ../../../Library/Bat2/Message/MenuGsm.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmContacts.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmContact.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Message/MenuGsmCommands.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmEvents.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmPowerOptions.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmInfo.c
SOURCES += ../../../Library/Bat2/Message/MenuGsmPinCode.c
SOURCES += ../../../Library/Bat2/Message/GsmMessage.c
SOURCES += ../../../Library/Bat2/Message/ContactList.c
SOURCES += ../../../Library/Bat2/Message/Message.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Platform/Zone.c
SOURCES += ../../../Library/Bat2/Predefined/PredefinedList.c
SOURCES += ../../../Library/Bat2/Predefined/MenuPredefinedWeighing.c
SOURCES += ../../../Library/Bat2/Scheduler/WeighingScheduler.c
SOURCES += ../../../Library/Bat2/Scheduler/WeighingPlan.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Scheduler/MenuWeighingPlan.c
SOURCES += ../../../Library/Bat2/Scheduler/MenuWeighingDays.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Scheduler/MenuWeighingTimes.c
SOURCES += ../../../Library/Bat2/Scheduler/MenuWeekDays.c
SOURCES += ../../../Library/Bat2/Statistic/Calculate.c
SOURCES += ../../../Library/Bat2/Statistic/DStatistic.c
SOURCES += ../../../Library/Bat2/Statistic/MenuStatisticParameters.c
SOURCES += ../../../Library/Bat2/Storage/Sample.c
SOURCES += ../../../Library/Bat2/Storage/MenuSample.c
SOURCES += ../../../Library/Bat2/Weighing/WeighingConfiguration.c
SOURCES += ../../../Library/Bat2/Weighing/Statistics.c
SOURCES += ../../../Library/Bat2/Weighing/Prediction.c
SOURCES += ../../../Library/Bat2/Weighing/Memory.c
SOURCES += ../../../Library/Bat2/Weighing/Curve.c
SOURCES += ../../../Library/Bat2/Weighing/Correction.c
SOURCES += ../../../Library/Bat2/Dummy/Contrast.c
SOURCES += ../../../Library/Bat2/Dummy/Backlight.c
SOURCES += ../../../Library/Bat2/Dummy/Beep.c
SOURCES +=
SOURCES += ../../../Library/Bat2/Dummy/Iadc.c
SOURCES += ../../../Library/Bat/Console/conio.c
SOURCES += ../../../Library/Bat/Console/xprint.c
SOURCES += ../../../Library/Bat/Console/xprintf.c
SOURCES += ../../../Library/Bat/Console/sputchar.c
SOURCES += ../../../Library/Bat/Country/Country.c
SOURCES += ../../../Library/Bat/Country/xTime.c
SOURCES += ../../../Library/Bat/Country/bTime.c
SOURCES += ../../../Library/Bat/Country/MenuCountry.c
SOURCES += ../../../Library/Bat/Graphic/Graphic.c
SOURCES += ../../../Library/Bat/Graphic/Text.c
SOURCES += ../../../Library/Bat/Gadget/DCursor.c
SOURCES += ../../../Library/Bat/Gadget/DList.c
SOURCES += ../../../Library/Bat/Gadget/DDirectory.c
SOURCES += ../../../Library/Bat/Gadget/DNamedList.c
SOURCES += ../../../Library/Bat/Gadget/DMsg.c
SOURCES += ../../../Library/Bat/Gadget/DMenu.c
SOURCES += ../../../Library/Bat/Gadget/DCheckList.c
SOURCES += ../../../Library/Bat/Gadget/DLayout.c
SOURCES += ../../../Library/Bat/Gadget/DLabel.c
SOURCES += ../../../Library/Bat/Gadget/DInputText.c
SOURCES += ../../../Library/Bat/Gadget/DInput.c
SOURCES += ../../../Library/Bat/Gadget/DFormat.c
SOURCES += ../../../Library/Bat/Gadget/DEvent.c
SOURCES += ../../../Library/Bat/Gadget/DEnterText.c
SOURCES += ../../../Library/Bat/Gadget/DEnterList.c
SOURCES += ../../../Library/Bat/Gadget/DEnter.c
SOURCES += ../../../Library/Bat/Gadget/DEdit.c
SOURCES += ../../../Library/Bat/Gadget/DEnterDate.c
SOURCES += ../../../Library/Bat/Gadget/DEnterTime.c
SOURCES += ../../../Library/Bat/Gadget/DProgress.c
SOURCES += ../../../Library/Bat/Gadget/DTime.c
SOURCES += ../../../Library/Bat/Statistic/Statistic.c
SOURCES += ../../../Library/Bat/Statistic/Histogram.c
SOURCES += ../../../Library/Bat/Statistic/DHistogram.c
SOURCES += ../../../Library/Bat/String/StrUtil.c
SOURCES += ../../../Library/Bat/Weight/Weight.c
SOURCES += ../../../Library/Bat/Weight/xWeight.c
SOURCES += ../../../Library/Bat/Weight/DWeight.c
SOURCES += ../../../Library/Bat/Weight/MenuWeightUnits.c
SOURCES += ../../../Library/Micro/Convert/uBcd.c
SOURCES += ../../../Library/Micro/Data/uStorage.c
SOURCES += ../../../Library/Micro/Data/uFifo.c
SOURCES += ../../../Library/Micro/Data/uDirectory.c
SOURCES += ../../../Library/Micro/Data/uNamedList.c
SOURCES += ../../../Library/Micro/Data/uList.c
SOURCES += ../../../Library/Micro/Time/uClock.c
SOURCES += ../../../Library/Micro/Time/uDate.c
SOURCES += ../../../Library/Micro/Time/uTime.c
SOURCES += ../../../Library/Micro/Time/uDateTime.c
SOURCES += ../../../Library/Micro/Math/uGeometry.c
SOURCES += ../../../Library/Micro/Parse/uParse.c
SOURCES += ../../../Library/Qt/uSimulator/mainwindow.cpp
SOURCES += ../../../Library/Qt/uSimulator/lcddisplay.cpp
SOURCES += ../../../Library/Qt/uSimulator/devicepanel.cpp
SOURCES +=
SOURCES += ../../../Library/Qt/uSimulator/uDebug.cpp
SOURCES += ../../../Library/Qt/Crt/crt.cpp
SOURCES += ../../../Library/Qt/Crt/crtdump.cpp
SOURCES += ../../../Library/Win32/Utility/Performance.c
#SOURCES += ../../../Library/Win32/Uart/WinUart.cpp
#SOURCES += ../../../Library/Win32/Uart/UartModem.cpp
#SOURCES += ../../../Library/Device/Gsm/Gsm.c
#--- connectivity :
SOURCES += ../../../Library/Qt/Socket/LocalClient.cpp

HEADERS += ../uBat2Compact/Hardware.h \
    ../../../Library/Bat2/Compact/Config/ConfigDef.h \
    ../../../Library/Bat2/Compact/Config/Config.h \
    Hardware.h \
    ../../../Library/Bat2Platform/Compact/Weighing/pWeighing.h \
    ../../../Library/Qt/Bat2Platform/Samples.h \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.h \
    ../../../Library/Bat/Gadget/DGraph.h \
    ../../../Library/Bat2/Graph/MenuGraph.h \
    ../../../Library/Bat2/Graph/GraphDef.h \
    ../../../Library/Bat2/Megavi/MenuMegavi.h \
    ../../../Library/Bat2/Megavi/MegaviTask.h \
    ../../../Library/Device/Megavi/MegaviDef.h \
    ../../../Library/Device/Megavi/Megavi.h \
    ../../../Library/Bat2/Megavi/MegaviConfig.h \
    ../../../Library/Bat2/Megavi/MegaviConfigDef.h \
    ../../../Library/Bat2/Multitasking/pt-sem.h \
    ../../../Library/Bat2/Multitasking/pt.h \
    ../../../Library/Bat2/Multitasking/lc-switch.h \
    ../../../Library/Bat2/Multitasking/lc.h \
    ../../../Library/Bat2/Menu/MenuRs485Def.h \
    ../../../Library/Bat2/Menu/MenuRs485.h \
    ../../../Library/Bat2/Menu/MenuRs485Interface.h \
    ../../../Library/Device/Rs485/Rs485ConfigDef.h \
    ../../../Library/Device/Rs485/Rs485Config.h \
    ../../../Library/Bat2/Multitasking/lc.h \
    ../../../Library/Bat2/Scheduler/WeighingPlanList.h \
    ../../../Library/Bat2/Scheduler/MenuWeighingPlans.h \
    ../../../Library/Bat2/Ethernet/MenuEthernetInfo.h \
    ../../../Library/Bat2/Ethernet/EthernetLibrary.h \
    ../../../Library/Device/Ethernet/fnet_user_config.h \
    ../../../Library/Device/Rs485/Rs485.h \
    ../../../Library/Device/Remote/Frame.h \
    ../../../Library/Device/Remote/SocketIfUsb.h \
    ../../../Library/Device/Remote/SocketIfUart.h \
    ../../../Library/Device/Remote/SocketIfEthernet.h \
    ../../../Library/Device/Remote/Socket.h \
    ../../../Library/Bat2/Megavi/MessageMegavi.h \
    ../../../Library/Bat2/Dacs/DacsTask.h \
    ../../../Library/Device/Dacs/DacsDef.h \
    ../../../Library/Device/Dacs/Dacs.h \
    ../../../Library/Bat2/Dacs/DacsConfigDef.h \
    ../../../Library/Bat2/Dacs/DacsConfig.h \
    ../../../Library/Bat2/Ethernet/EthernetLibrary.h \
    ../../../Library/Bat2/Memory/FileDef.h \
    ../../../Library/Device/Remote/SocketIfSms.h \
    ../../../Library/Bat2/Menu/MenuWeighingTargetAdjust.h \
    ../../../Library/Bat2/Menu/MenuSmsStatistics.h \
    ../../../Library/Bat2/Message/MenuGsmRemoteControl.h \
    ../../../Library/Bat/Gadget/DTimeRange.h \
    ../../../Library/Bat2/Message/MenuGsmPowerOptionsTimes.h \
    ../../../Library/Bat2/Log/Logger.h \
    ../../../Library/Bat2/Log/LogDef.h \
    ../../../Library/Bat2/Log/Log.h \
    ../../../Library/Device/Memory/FileNvm.h \
    ../../../Library/Device/Memory/FileNvm.h \
    ../../../Library/Device/Memory/FileRemote.h \
    ../../../Library/Bat2/Communication/Communication.h \
    ../../../Library/Bat2/Communication/CommunicationDef.h \
    ../../../Library/Bat2/Communication/MenuCellularData.h \
    ../../../Library/Bat2/Communication/MenuDataPublication.h \
    ../../../Library/Bat2/Communication/MenuEthernet.h \
    ../../../Library/Bat2/Communication/MenuInternetConnection.h \
    ../../../Library/Bat2/Communication/MenuWifi.h \
    ../../../Library/Bat/Gadget/DIp.h \
    ../../../Library/Bat/Internet/xIp.h \
    ../../../Library/Bat2/Menu/MenuCommunication.h \
    ../../../Library/Bat2/Compact/Config/FactoryDefault.h \
    ../../../Library/Qt/Cpu/Cpu.h \
    ../../../Library/Device/Ethernet/http/client/HTTPClient.h \
    ../../../Library/Device/Ethernet/http/client/HTTPClientAuth.h \
    ../../../Library/Device/Ethernet/http/client/HTTPClientCommon.h \
    ../../../Library/Device/Ethernet/http/client/HTTPClientString.h \
    ../../../Library/Device/Ethernet/http/client/HTTPClientWrapper.h \
    ../../../Library/Bat2/SmsGate/SmsGate.h \
    ../../../Library/Bat2/SmsGate/SmsGateDef.h \
    ../../../Library/Bat2/Communication/InternetPublication.h \
    ../../../Library/Bat2/Communication/InternetPublicationDef.h \
    ../../../Library/Device/Ethernet/http/client/HTTPStatusCodes.h \
    ../../../Library/Device/Multitasking/Tasks.h \
    ../../../Library/Device/Multitasking/Multitasking.h \
    ../../../Library/Qt/Multitasking/MultitaskingDef.h \
    ../../../Library/Bat2/Menu/MenuPowerFailure.h \
    ../../../Library/Bat2/Remote/MenuRemote.h \
    ../../../Library/Qt/Multitasking/Worker.h \
    ../../../Library/Bat2/Compact/Gui.h \
    ../../../Library/Device/Gsm/GsmDefPrv.h \
    ../../../Library/Device/Memory/mempool.h \
    ../../../Library/Win32/Uart/WinUart.h \
    ../../../Library/Qt/Uart/UartParameters.h \
    ../../../Library/Qt/sleepthread.h \
    ../../../Library/Bat2/SmsGate/SmsGateConfig.h \
    ../../../Library/Device/Ethernet/fnet.h \
    ../../../Library/Device/Ethernet/fnet_arp.h \
    ../../../Library/Device/Ethernet/fnet_config.h \
    ../../../Library/Device/Ethernet/fnet_debug.h \
    ../../../Library/Device/Ethernet/fnet_error.h \
    ../../../Library/Device/Ethernet/fnet_eth.h \
    ../../../Library/Device/Ethernet/fnet_eth_prv.h \
    ../../../Library/Device/Ethernet/fnet_checksum.h \
    ../../../Library/Device/Ethernet/fnet_icmp.h \
    ../../../Library/Device/Ethernet/fnet_icmp6.h \
    ../../../Library/Device/Ethernet/fnet_igmp.h \
    ../../../Library/Device/Ethernet/fnet_inet.h \
    ../../../Library/Device/Ethernet/fnet_ip.h \
    ../../../Library/Device/Ethernet/fnet_ip_prv.h \
    ../../../Library/Device/Ethernet/fnet_ip6.h \
    ../../../Library/Device/Ethernet/fnet_ip6_prv.h \
    ../../../Library/Device/Ethernet/fnet_isr.h \
    ../../../Library/Device/Ethernet/fnet_loop.h \
    ../../../Library/Device/Ethernet/fnet_mempool.h \
    ../../../Library/Device/Ethernet/fnet_mld.h \
    ../../../Library/Device/Ethernet/fnet_nd6.h \
    ../../../Library/Device/Ethernet/fnet_netbuf.h \
    ../../../Library/Device/Ethernet/fnet_netif.h \
    ../../../Library/Device/Ethernet/fnet_netif_prv.h \
    ../../../Library/Device/Ethernet/fnet_prot.h \
    ../../../Library/Device/Ethernet/fnet_raw.h \
    ../../../Library/Device/Ethernet/fnet_services.h \
    ../../../Library/Device/Ethernet/fnet_services_config.h \
    ../../../Library/Device/Ethernet/fnet_socket.h \
    ../../../Library/Device/Ethernet/fnet_socket_prv.h \
    ../../../Library/Device/Ethernet/fnet_stack.h \
    ../../../Library/Device/Ethernet/fnet_stack_config.h \
    ../../../Library/Device/Ethernet/fnet_stdlib.h \
    ../../../Library/Device/Ethernet/fnet_tcp.h \
    ../../../Library/Device/Ethernet/fnet_tcp_gsm.h \
    ../../../Library/Device/Ethernet/fnet_timer.h \
    ../../../Library/Device/Ethernet/fnet_timer_prv.h \
    ../../../Library/Device/Ethernet/fnet_udp.h \
    ../../../Library/Device/Ethernet/fnet_udp_gsm.h \
    ../../../Library/Device/Ethernet/fnet_user_config.h \
    ../../../Library/Device/Ethernet/dhcp/fnet_dhcp.h \
    ../../../Library/Device/Ethernet/dhcp/fnet_dhcp_config.h \
    ../../../Library/Device/Ethernet/dns/fnet_dns.h \
    ../../../Library/Device/Ethernet/dns/fnet_dns_config.h \
    ../../../Library/Device/Ethernet/dns/fnet_dns_prv.h \
    ../../../Library/Device/Ethernet/poll/fnet_poll.h \
    ../../../Library/Device/Ethernet/serial/fnet_serial.h \
    ../../../Library/Device/Ethernet/serial/fnet_serial_config.h \
    ../../../Library/Kinetis/Ethernet/EthPhy.h \
    ../../../Library/Kinetis/Ethernet/fnet_comp.h \
    ../../../Library/Kinetis/Ethernet/fnet_comp_config.h \
    ../../../Library/Kinetis/Ethernet/fnet_cpu.h \
    ../../../Library/Kinetis/Ethernet/fnet_cpu_config.h \
    ../../../Library/Kinetis/Ethernet/fnet_os.h \
    ../../../Library/Kinetis/Ethernet/fnet_os_config.h \
    ../../../Library/Win32/WinPcap/Include/bittypes.h \
    ../../../Library/Win32/WinPcap/Include/ip6_misc.h \
    ../../../Library/Win32/WinPcap/Include/Packet32.h \
    ../../../Library/Win32/WinPcap/Include/pcap.h \
    ../../../Library/Win32/WinPcap/Include/pcap-bpf.h \
    ../../../Library/Win32/WinPcap/Include/pcap-namedb.h \
    ../../../Library/Win32/WinPcap/Include/pcap-stdinc.h \
    ../../../Library/Win32/WinPcap/Include/remote-ext.h \
    ../../../Library/Win32/WinPcap/Include/Win32-Extensions.h \
    ../../../Library/Win32/WinPcap/Include/pcap/bluetooth.h \
    ../../../Library/Win32/WinPcap/Include/pcap/bpf.h \
    ../../../Library/Win32/WinPcap/Include/pcap/namedb.h \
    ../../../Library/Win32/WinPcap/Include/pcap/pcap.h \
    ../../../Library/Win32/WinPcap/Include/pcap/sll.h \
    ../../../Library/Win32/WinPcap/Include/pcap/usb.h \
    ../../../Library/Win32/WinPcap/Include/pcap/vlan.h \
    ../../../Library/Qt/fnet/fnet_win32_prv.h \
    ../../../Library/Qt/fnet/cputimer.h \
    ../../../Library/Qt/fnet/fnet_win32.h \
    ../../../Library/Qt/fnet/FNetLogWindow.h \
    ../../../Library/Bootloader/Bootloader.h \
    ../../../Library/Micro/Debug/uDebug.h \
    ../../../Library/Bat2/Modbus/mbconfig.h \
    ../../../Library/Bat2/Modbus/ModbusConfig.h \
    ../../../Library/Bat2/Modbus/ModbusConfigDef.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusCalibrationGroup.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusConfigurationGroup.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusGsmContactsGroup.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusCharacterBufferGroup.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusPredefinedCorrectionCurvesGroup.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusPredefinedGrowthCurvesGroup.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusPredefinedWeighingPlansGroup.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusPredefinedWeighingsGroup.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusReg.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusRegRangeCheck.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusSmsGateGroup.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusStatisticsGroup.h \
    ../../../Library/Bat2/Modbus/Slave/ModbusWeighingGroup.h \
    ../../../Library/Device/Modbus/mb.h \
    ../../../Library/Device/Modbus/mbascii.h \
    ../../../Library/Device/Modbus/mbcrc.h \
    ../../../Library/Device/Modbus/mbframe.h \
    ../../../Library/Device/Modbus/mbfunc.h \
    ../../../Library/Device/Modbus/mbport.h \
    ../../../Library/Device/Modbus/mbproto.h \
    ../../../Library/Device/Modbus/mbrtu.h \
    ../../../Library/Device/Modbus/mbtcp.h \
    ../../../Library/Device/Modbus/mbutils.h \
    ../../../Library/Qt/Modbus/mbconfig.h \
    ../../../Library/Qt/Modbus/port.h \
    ../../../Library/Micro/Crc/Crc.h \
    ../../../Library/SensorPack/Sensors/Sensors.h \
    ../../../Library/Micro/PreprocLoop/loop.h \
    ../../../Library/Micro/PreprocLoop/loopstart.h \
    ../../../Library/Bat2/Modbus/ModbusHoldingRegistersMaster.h
HEADERS += ../uBat2Compact/Storage.h
HEADERS += ../../../Library/Win32/Utility/Performance.h
HEADERS += ../uBat2Compact/Persistent.h
HEADERS += ../uBat2Compact/Bitmap.h
HEADERS += ../uBat2Compact/Str.h
HEADERS += ../../../Library/Qt/uSimulator/mainwindow.h
HEADERS += ../../../Library/Qt/uSimulator/devicepanel.h
HEADERS += ../../../Library/Qt/uSimulator/lcddisplay.h
HEADERS += ../../../Library/Qt/Crt/crt.h
HEADERS += ../../../Library/Qt/Crt/crtdump.h
HEADERS += ../../../Library/Bat2/Archive/ArchiveDef.h
HEADERS += ../../../Library/Bat2/Archive/Archive.h
HEADERS += ../../../Library/Bat2/Archive/MenuArchive.h
HEADERS += ../../../Library/Bat2/Compact/Power.h
HEADERS += ../../../Library/Bat2/Config/Bat2Def.h
HEADERS += ../../../Library/Bat2/Config/ConfigDef.h
HEADERS += ../../../Library/Bat2/Config/Config.h
HEADERS += ../../../Library/Bat2/Config/DeviceDef.h
HEADERS += ../../../Library/Bat2/Curve/GrowthCurveDef.h
HEADERS += ../../../Library/Bat2/Curve/GrowthCurve.h
HEADERS += ../../../Library/Bat2/Curve/CurveList.h
HEADERS += ../../../Library/Bat2/Curve/MenuGrowthCurve.h
HEADERS += ../../../Library/Bat2/Curve/CorrectionCurveDef.h
HEADERS += ../../../Library/Bat2/Curve/CorrectionCurve.h
HEADERS += ../../../Library/Bat2/Curve/CorrectionList.h
HEADERS += ../../../Library/Bat2/Curve/MenuCorrectionCurve.h
HEADERS += ../../../Library/Bat2/Display/DisplayConfigurationDef.h
HEADERS += ../../../Library/Bat2/Display/DisplayConfiguration.h
HEADERS += ../../../Library/Bat2/Display/MenuBacklightIntensity.h
HEADERS += ../../../Library/Bat2/Display/MenuContrast.h
HEADERS += ../../../Library/Bat2/Display/MenuDisplay.h
HEADERS += ../../../Library/Bat2/Display/MenuBacklight.h
HEADERS += ../../../Library/Bat2/Memory/NvmLayout.h
HEADERS += ../../../Library/Bat2/Menu/Menu.h
HEADERS += ../../../Library/Bat2/Menu/MenuMain.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighing.h
HEADERS += ../../../Library/Bat2/Menu/MenuStatistics.h
HEADERS += ../../../Library/Bat2/Menu/MenuUserSettings.h
HEADERS += ../../../Library/Bat2/Menu/MenuConfiguration.h
HEADERS += ../../../Library/Bat2/Menu/MenuMaintenance.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingStart.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingStartLater.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingStop.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingSuspend.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingRelease.h
HEADERS += ../../../Library/Bat2/Menu/MenuConfigurationWeighing.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingConfiguration.h
HEADERS += ../../../Library/Bat2/Menu/MenuInitialWeights.h
HEADERS += ../../../Library/Bat2/Menu/MenuGrowthCurves.h
HEADERS += ../../../Library/Bat2/Menu/MenuWeighingMenuSelection.h
HEADERS += ../../../Library/Bat2/Menu/MenuTargetWeights.h
HEADERS += ../../../Library/Bat2/Menu/MenuDetection.h
HEADERS += ../../../Library/Bat2/Menu/MenuAcceptance.h
HEADERS += ../../../Library/Bat2/Menu/MenuAcceptanceData.h
HEADERS += ../../../Library/Bat2/Menu/MenuPlatformInfo.h
HEADERS += ../../../Library/Bat2/Menu/MenuMaintenanceCalibration.h
HEADERS += ../../../Library/Bat2/Menu/MenuMaintenanceSimulation.h
HEADERS += ../../../Library/Bat2/Menu/Password.h
HEADERS += ../../../Library/Bat2/Menu/Screen.h
HEADERS += ../../../Library/Bat2/Menu/ScreenStatistic.h
HEADERS += ../../../Library/Bat2/Message/GsmMessageDef.h
HEADERS += ../../../Library/Bat2/Message/GsmMessage.h
HEADERS += ../../../Library/Bat2/Message/ContactList.h
HEADERS += ../../../Library/Bat2/Message/MenuGsm.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmContacts.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmContact.h
HEADERS +=
HEADERS += ../../../Library/Bat2/Message/MenuGsmCommands.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmPowerOptions.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmInfo.h
HEADERS += ../../../Library/Bat2/Message/MenuGsmPinCode.h
HEADERS += ../../../Library/Bat2/Message/Message.h
HEADERS += ../../../Library/Bat2/Message/MGsm.h
HEADERS += ../../../Library/Bat2Platform/Platform/PlatformDef.h
HEADERS += ../../../Library/Bat2Platform/Platform/Wepl.h
HEADERS += ../../../Library/Bat2/Platform/Zone.h
HEADERS += ../../../Library/Bat2/Predefined/PredefinedWeighingDef.h
HEADERS += ../../../Library/Bat2/Predefined/PredefinedList.h
HEADERS += ../../../Library/Bat2/Predefined/MenuPredefinedWeighing.h
HEADERS += ../../../Library/Bat2/Scheduler/WeighingSchedulerDef.h
HEADERS += ../../../Library/Bat2/Scheduler/WeighingScheduler.h
HEADERS += ../../../Library/Bat2/Scheduler/WeighingPlan.h
HEADERS +=
HEADERS += ../../../Library/Bat2/Scheduler/MenuWeighingPlan.h
HEADERS += ../../../Library/Bat2/Scheduler/MenuWeighingDays.h
HEADERS +=
HEADERS += ../../../Library/Bat2/Scheduler/MenuWeighingTimes.h
HEADERS += ../../../Library/Bat2/Scheduler/MenuWeekDays.h
HEADERS += ../../../Library/Bat2/Statistic/StatisticDef.h
HEADERS += ../../../Library/Bat2/Statistic/Calculate.h
HEADERS += ../../../Library/Bat2/Statistic/DStatistic.h
HEADERS += ../../../Library/Bat2/Statistic/MenuStatisticParameters.h
HEADERS += ../../../Library/Bat2/Storage/Sample.h
HEADERS += ../../../Library/Bat2/Storage/SampleDef.h
HEADERS += ../../../Library/Bat2/Storage/MenuSample.h
HEADERS += ../../../Library/Bat2/Weighing/Weighing.h
HEADERS += ../../../Library/Bat2/Weighing/WeighingConfigurationDef.h
HEADERS += ../../../Library/Bat2/Weighing/WeighingConfiguration.h
HEADERS += ../../../Library/Bat2/Weighing/Statistics.h
HEADERS += ../../../Library/Bat2/Weighing/Prediction.h
HEADERS += ../../../Library/Bat2/Weighing/Memory.h
HEADERS += ../../../Library/Bat2/Weighing/Curve.h
HEADERS += ../../../Library/Bat2/Weighing/Correction.h
HEADERS += ../../../Library/Bat/Country/CountryDef.h
HEADERS += ../../../Library/Bat/Country/Country.h
HEADERS += ../../../Library/Bat/Country/CountrySet.h
HEADERS += ../../../Library/Bat/Country/xTime.h
HEADERS += ../../../Library/Bat/Country/bTime.h
HEADERS += ../../../Library/Bat/Country/MenuCountry.h
HEADERS += ../../../Library/Bat/Device/VersionDef.h
HEADERS += ../../../Library/Bat/Gadget/DCursor.h
HEADERS += ../../../Library/Bat/Gadget/DList.h
HEADERS += ../../../Library/Bat/Gadget/DDirectory.h
HEADERS += ../../../Library/Bat/Gadget/DNamedList.h
HEADERS += ../../../Library/Bat/Gadget/DMsg.h
HEADERS += ../../../Library/Bat/Gadget/DMenu.h
HEADERS += ../../../Library/Bat/Gadget/DCheckList.h
HEADERS += ../../../Library/Bat/Gadget/DLayout.h
HEADERS += ../../../Library/Bat/Gadget/DLabel.h
HEADERS += ../../../Library/Bat/Gadget/DInput.h
HEADERS += ../../../Library/Bat/Gadget/DFormat.h
HEADERS += ../../../Library/Bat/Gadget/DEvent.h
HEADERS += ../../../Library/Bat/Gadget/DEnterList.h
HEADERS += ../../../Library/Bat/Gadget/DEnter.h
HEADERS += ../../../Library/Bat/Gadget/DEdit.h
HEADERS += ../../../Library/Bat/Gadget/DProgress.h
HEADERS += ../../../Library/Bat/Gadget/DTime.h
HEADERS += ../../../Library/Bat/Sound/Beep.h
HEADERS += ../../../Library/Bat/Statistic/StatisticGaugeDef.h
HEADERS += ../../../Library/Bat/Statistic/HistogramDef.h
HEADERS += ../../../Library/Bat/Statistic/Statistic.h
HEADERS += ../../../Library/Bat/Statistic/Histogram.h
HEADERS += ../../../Library/Bat/Statistic/DHistogram.h
HEADERS += ../../../Library/Bat/System/System.h
HEADERS += ../../../Library/Bat/Weight/Weight.h
HEADERS += ../../../Library/Bat/Weight/xWeight.h
HEADERS += ../../../Library/Bat/Weight/DWeight.h
HEADERS += ../../../Library/Bat/Weight/WeightDef.h
HEADERS += ../../../Library/Bat/Weight/SexDef.h
HEADERS += ../../../Library/Bat/Weight/WeightFlagDef.h
HEADERS += ../../../Library/Bat/Weight/MenuWeightUnits.h
HEADERS += ../../../Library/Device/File/Efs.h
HEADERS += ../../../Library/Device/Memory/Nvm.h
HEADERS += ../../../Library/Device/System/System.h
HEADERS += ../../../Library/Device/Gsm/GsmDef.h
HEADERS += ../../../Library/Device/Gsm/Gsm.h
HEADERS += ../../../Library/Micro/Convert/uBcd.h
HEADERS += ../../../Library/Micro/Data/uDirectory.h
HEADERS += ../../../Library/Micro/Data/uDirectoryDef.h
HEADERS += ../../../Library/Micro/Data/uNamedList.h
HEADERS += ../../../Library/Micro/Data/uNamedListDef.h
HEADERS += ../../../Library/Micro/Data/uStorage.h
HEADERS += ../../../Library/Micro/Data/uStorageDef.h
HEADERS += ../../../Library/Micro/Data/uFifo.h
HEADERS += ../../../Library/Micro/Data/uFifoDef.h
HEADERS += ../../../Library/Micro/Data/uListDef.h
HEADERS += ../../../Library/Micro/Data/uList.h
HEADERS += ../../../Library/Micro/Time/uTime.h
HEADERS += ../../../Library/Micro/Time/uDate.h
HEADERS += ../../../Library/Micro/Time/uDateTime.h
HEADERS += ../../../Library/Micro/Time/uClock.h
HEADERS += ../../../Library/Micro/Math/uGeometry.h
HEADERS += ../../../Library/Micro/Parse/uParse.h
HEADERS += ../../../Library/Bat2/Compact/Tasks/TasksDef.h

#--- connectivity :
HEADERS += ../../../Library/Qt/Socket/LocalClient.h
HEADERS += ../../../Library/Qt/Bat2/WeplClient.h

FORMS   += ../../../Library/Qt/uSimulator/mainwindow.ui \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.ui \
    ../../../Library/Qt/Uart/UartParameters.ui \
    ../../../Library/Qt/fnet/FNetLogWindow.ui
FORMS   += ../../../Library/Qt/uSimulator/devicepanel.ui

RESOURCES += Bat2Compact.qrc \
    ../Bat2Platform/Bat2Platform.qrc \
    ../../../Library/Qt/Bat2Platform/PlatformSimulator/PlatformSimulator.qrc

OTHER_FILES += ../../../Library/Bat2/Compact/Str.txt \
    ../../../Library/Bat2/Ethernet/MenuEthernet.str
OTHER_FILES += ../../../Library/Bat2/Compact/Str.str
