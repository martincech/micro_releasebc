//******************************************************************************
//
//   MainWindow.h   String Glue main window
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
   Q_OBJECT
   
public:
   explicit MainWindow(QWidget *parent = 0);
   ~MainWindow();
   
private slots:
   void on_actionOpen_triggered();
   void on_actionSave_triggered();

   void on_actionStrings_triggered();
   void on_actionHeader_triggered();
   void on_actionSource_triggered();
   void on_actionCsv_triggered();

   void on_actionAvr32_triggered();

private:
   Ui::MainWindow *ui;
   QString         fileName;
};

#endif // MAINWINDOW_H
