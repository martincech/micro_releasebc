/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Mon 14. May 10:23:21 2012
**      by: Qt User Interface Compiler version 4.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QStackedWidget>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionStrings;
    QAction *actionHeader;
    QAction *actionSource;
    QAction *actionCsv;
    QAction *actionAvr32;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QStackedWidget *pageSelector;
    QWidget *pageStrings;
    QVBoxLayout *verticalLayout;
    QLabel *labelStrings;
    QPlainTextEdit *textStrings;
    QWidget *pageHeader;
    QVBoxLayout *verticalLayout_2;
    QLabel *labelHeader;
    QPlainTextEdit *textHeader;
    QWidget *pageSource;
    QVBoxLayout *verticalLayout_3;
    QLabel *labelSource;
    QPlainTextEdit *textSource;
    QWidget *pageCsv;
    QVBoxLayout *verticalLayout_4;
    QLabel *labelCsv;
    QPlainTextEdit *textCsv;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(711, 591);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/image/glue.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/image/open.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon1);
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/image/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSave->setIcon(icon2);
        actionStrings = new QAction(MainWindow);
        actionStrings->setObjectName(QString::fromUtf8("actionStrings"));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        actionStrings->setFont(font);
        actionHeader = new QAction(MainWindow);
        actionHeader->setObjectName(QString::fromUtf8("actionHeader"));
        actionHeader->setFont(font);
        actionSource = new QAction(MainWindow);
        actionSource->setObjectName(QString::fromUtf8("actionSource"));
        actionSource->setFont(font);
        actionCsv = new QAction(MainWindow);
        actionCsv->setObjectName(QString::fromUtf8("actionCsv"));
        actionCsv->setFont(font);
        actionAvr32 = new QAction(MainWindow);
        actionAvr32->setObjectName(QString::fromUtf8("actionAvr32"));
        actionAvr32->setCheckable(true);
        QFont font1;
        font1.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
        actionAvr32->setFont(font1);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pageSelector = new QStackedWidget(centralWidget);
        pageSelector->setObjectName(QString::fromUtf8("pageSelector"));
        pageStrings = new QWidget();
        pageStrings->setObjectName(QString::fromUtf8("pageStrings"));
        verticalLayout = new QVBoxLayout(pageStrings);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        labelStrings = new QLabel(pageStrings);
        labelStrings->setObjectName(QString::fromUtf8("labelStrings"));
        QFont font2;
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        labelStrings->setFont(font2);

        verticalLayout->addWidget(labelStrings);

        textStrings = new QPlainTextEdit(pageStrings);
        textStrings->setObjectName(QString::fromUtf8("textStrings"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Courier New"));
        font3.setPointSize(8);
        textStrings->setFont(font3);

        verticalLayout->addWidget(textStrings);

        pageSelector->addWidget(pageStrings);
        pageHeader = new QWidget();
        pageHeader->setObjectName(QString::fromUtf8("pageHeader"));
        verticalLayout_2 = new QVBoxLayout(pageHeader);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        labelHeader = new QLabel(pageHeader);
        labelHeader->setObjectName(QString::fromUtf8("labelHeader"));
        labelHeader->setFont(font2);

        verticalLayout_2->addWidget(labelHeader);

        textHeader = new QPlainTextEdit(pageHeader);
        textHeader->setObjectName(QString::fromUtf8("textHeader"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Courier New"));
        textHeader->setFont(font4);

        verticalLayout_2->addWidget(textHeader);

        pageSelector->addWidget(pageHeader);
        pageSource = new QWidget();
        pageSource->setObjectName(QString::fromUtf8("pageSource"));
        verticalLayout_3 = new QVBoxLayout(pageSource);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        labelSource = new QLabel(pageSource);
        labelSource->setObjectName(QString::fromUtf8("labelSource"));
        labelSource->setFont(font2);

        verticalLayout_3->addWidget(labelSource);

        textSource = new QPlainTextEdit(pageSource);
        textSource->setObjectName(QString::fromUtf8("textSource"));
        textSource->setFont(font4);

        verticalLayout_3->addWidget(textSource);

        pageSelector->addWidget(pageSource);
        pageCsv = new QWidget();
        pageCsv->setObjectName(QString::fromUtf8("pageCsv"));
        verticalLayout_4 = new QVBoxLayout(pageCsv);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        labelCsv = new QLabel(pageCsv);
        labelCsv->setObjectName(QString::fromUtf8("labelCsv"));
        labelCsv->setFont(font2);

        verticalLayout_4->addWidget(labelCsv);

        textCsv = new QPlainTextEdit(pageCsv);
        textCsv->setObjectName(QString::fromUtf8("textCsv"));
        textCsv->setFont(font4);

        verticalLayout_4->addWidget(textCsv);

        pageSelector->addWidget(pageCsv);

        horizontalLayout->addWidget(pageSelector);

        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        QFont font5;
        font5.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font5.setBold(true);
        font5.setWeight(75);
        mainToolBar->setFont(font5);
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        QFont font6;
        font6.setFamily(QString::fromUtf8("Arial"));
        font6.setPointSize(10);
        font6.setBold(true);
        font6.setWeight(75);
        statusBar->setFont(font6);
        MainWindow->setStatusBar(statusBar);

        mainToolBar->addAction(actionOpen);
        mainToolBar->addAction(actionSave);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionStrings);
        mainToolBar->addAction(actionHeader);
        mainToolBar->addAction(actionSource);
        mainToolBar->addAction(actionCsv);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionAvr32);

        retranslateUi(MainWindow);

        pageSelector->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "String Glue", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("MainWindow", "Open", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionOpen->setToolTip(QApplication::translate("MainWindow", "Open file", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionSave->setText(QApplication::translate("MainWindow", "Save", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionSave->setToolTip(QApplication::translate("MainWindow", "Save file", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionStrings->setText(QApplication::translate("MainWindow", "Strings", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionStrings->setToolTip(QApplication::translate("MainWindow", "Show strings", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionHeader->setText(QApplication::translate("MainWindow", "Header", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionHeader->setToolTip(QApplication::translate("MainWindow", "Show header file", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionSource->setText(QApplication::translate("MainWindow", "Source", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionSource->setToolTip(QApplication::translate("MainWindow", "Show source file", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionCsv->setText(QApplication::translate("MainWindow", "CSV", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionCsv->setToolTip(QApplication::translate("MainWindow", "Show CSV file", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        actionAvr32->setText(QApplication::translate("MainWindow", "AVR32", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionAvr32->setToolTip(QApplication::translate("MainWindow", "AVR32 code generation", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        labelStrings->setText(QApplication::translate("MainWindow", "Strings", 0, QApplication::UnicodeUTF8));
        labelHeader->setText(QApplication::translate("MainWindow", "Header", 0, QApplication::UnicodeUTF8));
        labelSource->setText(QApplication::translate("MainWindow", "Source", 0, QApplication::UnicodeUTF8));
        labelCsv->setText(QApplication::translate("MainWindow", "CSV", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
