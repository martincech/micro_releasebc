//******************************************************************************
//
//   StringParser.cpp  String list parser
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#include "StringParser.h"
#include "Parse/TextFile.h"
#include <QFile>

#define KEYWORD_ENUM        "ENUM_"
#define KEYWORD_ENUM_SIZE   (sizeof( KEYWORD_ENUM) - 1)
#define KEYWORD_INCLUDE     "include"

//------------------------------------------------------------------------------
//   Constructor
//------------------------------------------------------------------------------

StringParser::StringParser() : _lexer()
{
   _index   = 0;
   _unit    = 0;
} // StringParser

//------------------------------------------------------------------------------
//   Parse
//------------------------------------------------------------------------------

void StringParser::parse( QString text)
// Returns parsed <text>
{
   _lexer.setText( text);
   _lexer.parse();
   int count  = _lexer.count();
   if( count == 0){
      return;                          // empty file
   }
   _index     = 0;
   _unit      = _lexer.at( _index);
   while( _index < count){
      parseStatement();
   }
} // parse

//------------------------------------------------------------------------------
//   Parse file
//------------------------------------------------------------------------------

void StringParser::parseFile( QString fileName)
// Returns parsed <fileName>
{
   TextFile file( fileName);
   QString text = file.load();
   parse( text);
} // parseFile

//------------------------------------------------------------------------------
//   Generate
//------------------------------------------------------------------------------

QList<QString> StringParser::_duplicationList;

void StringParser::generate( QString fileName)
// Generate string files of <fileName> source
{
   // clear generated files :
   _strings.clear();
   _header.clear();
   _source.clear();
   _csv.clear();
   _duplicationList.clear();
   generateProlog();
   parseFile( fileName);
   generateEpilog();
} // generate

//------------------------------------------------------------------------------
//   Strings
//------------------------------------------------------------------------------

QString StringParser::strings()
// Returns generated strings
{
   return( _strings);
} // strings

//------------------------------------------------------------------------------
//   Header
//------------------------------------------------------------------------------

QString StringParser::header()
// Returns generated header
{
   return( _header);
} // header

//------------------------------------------------------------------------------
//   Source
//------------------------------------------------------------------------------

QString StringParser::source()
// Returns generated source
{
   return( _source);
} // source

//------------------------------------------------------------------------------
//   CSV
//------------------------------------------------------------------------------

QString StringParser::csv()
// Returns generated CSV
{
   return( _csv);
} // csv

//------------------------------------------------------------------------------
//   String Index
//------------------------------------------------------------------------------

int StringParser::stringIndex()
// Returns current string index
{
   return( _stringIndex);
} // stringIndex

void StringParser::setStringIndex( int index)
// Set string base as <index>
{
   _stringIndex = index;
} // setStringIndex

//******************************************************************************

//------------------------------------------------------------------------------
//   Line
//------------------------------------------------------------------------------

void StringParser::parseStatement()
// Returns code for line starting at <_index> position
{
   switch( _unit->type){
      case LEX_SYMBOL :
         parseInclude();
         break;

      case LEX_WORD :
         // check for enum definition :
         if( _unit->value.left( KEYWORD_ENUM_SIZE) == QString( KEYWORD_ENUM)){
            parseEnum();
            _insideEnum = true;
            break;
         }
         parseDefinition( _insideEnum ? false : true);
         break;

      case LEX_COMMENT :
         generateComment( _unit->value);
         skipLine();
         break;

      case LEX_NEWLINE :
         generateNewLine();
         nextSymbol();
         _insideEnum = false;
         break;

      default :
         error();
         skipLine();                         // skip up to end of line
         break;
   }
}  // parserStatement

//------------------------------------------------------------------------------
//   Include
//------------------------------------------------------------------------------

void StringParser::parseInclude()
// Returns code for include statement
{
   // include statement LEX_SYMBOL :
   if( _unit->value != QString( "#")){
      error();
      return;
   }
   nextSymbol();
   // include keyword :
   if( _unit->type != LEX_WORD){
      error();
      return;
   }
   if( _unit->value != QString( KEYWORD_INCLUDE)){
      error();
      return;
   }
   // space :
   skipSpace();
   // include file :
   if( _unit->type != LEX_TEXT){
      error();
      return;
   }
   // parse included file :
   generateComment( QString( ">>> #include '%1'").arg( _unit->value));
   if( !QFile::exists( _unit->value)){
      fileError( _unit->value);
      nextSymbol();
      return;
   }
   StringParser parser;
   parser.setStringIndex( _stringIndex);    // set current index as base
   parser.parseFile( _unit->value);
   _strings    += parser.strings();
   _header     += parser.header();
   _source     += parser.source();
   _csv        += parser.csv();
   _stringIndex = parser.stringIndex();     // move current index
   generateComment( QString( "<<< '%1'").arg( _unit->value));
   // skip file name :
   skipLine();
} // parseInclude

//------------------------------------------------------------------------------
//   Definition
//------------------------------------------------------------------------------

void StringParser::parseDefinition( bool ignoreDuplication)
// Returns code for string definition
{
   // string definition LEX_WORD :
   QString name = _unit->value;        // string name
   // check for space :
   skipSpace();
   // get string :
   if( _unit->type != LEX_TEXT){
      error();
      return;
   }
   QString value = _unit->value;
   // optional comment :
   skipSpace();
   QString comment;
   if( _unit->type == LEX_NEWLINE){
      skipLine();
   } else if( _unit->type == LEX_COMMENT){
      comment = _unit->value;
      skipLine();
   } else {
      error();
      skipLine();
      return;
   }
   if(ignoreDuplication) {
      if(_duplicationList.contains(name)) {
         return;
      }
   }
   _duplicationList.append(name);
   generateString( name, value, comment);
} // parseDefinition

//------------------------------------------------------------------------------
//   Enum
//------------------------------------------------------------------------------

void StringParser::parseEnum()
// Returns code for enum definition
{
   // string definition LEX_WORD :
   generateEnum( _unit->value);        // enum name
   nextSymbol();
   skipLine();
   // enum constants :
   int count = _lexer.count();
   while( _index < count){
      if( _unit->type != LEX_SPACE){
         return;
      }
      nextSymbol();                    // skip indentation space
      parseDefinition( false);            // get constant definition
   }
} // parseEnum

//------------------------------------------------------------------------------
//   Next symbol
//------------------------------------------------------------------------------

void StringParser::nextSymbol()
// Move to next symbol
{
   _index++;
   _unit = _lexer.at( _index);
} // nextSymbol

//------------------------------------------------------------------------------
//   Skip space
//------------------------------------------------------------------------------

void StringParser::skipSpace()
// Skip optional space
{
   nextSymbol();
   if( _unit->type != LEX_SPACE){
      return;
   }
   nextSymbol();
} // skipSpace

//------------------------------------------------------------------------------
//   Skip line
//------------------------------------------------------------------------------

void StringParser::skipLine()
// Skip up to end of line
{
   int count = _lexer.count();
   while( _index < count){
      if( _unit->type == LEX_NEWLINE){
         nextSymbol();
         return;
      }
      nextSymbol();
   }
} // skipLine

//------------------------------------------------------------------------------
//   Error
//------------------------------------------------------------------------------

void StringParser::error()
// Returns error message for current unit
{
   _strings += QString( "** ERROR : line %1 column %2\n").arg( _unit->line).arg( _unit->column);
   _header  += QString( "** ERROR : line %1 column %2\n").arg( _unit->line).arg( _unit->column);
   _source  += QString( "** ERROR : line %1 column %2\n").arg( _unit->line).arg( _unit->column);
   _csv     += QString( "\"** ERROR : line %1 column %2\"\n").arg( _unit->line).arg( _unit->column);
} // error

//------------------------------------------------------------------------------
//   File error
//------------------------------------------------------------------------------

void StringParser::fileError( QString fileName)
// Error message for <fileName> not found
{
   _strings += QString( "** ERROR file not found (line %2)\n").arg( _unit->line);
   _header  += QString( "** ERROR file not found (line %2)\n").arg( _unit->line);
   _source  += QString( "** ERROR file not found (line %2)\n").arg( _unit->line);
   _csv     += QString( "\"** ERROR file not found (line %2)\"\n").arg( _unit->line);
} // fileError;

//------------------------------------------------------------------------------
//   Prolog
//------------------------------------------------------------------------------

void StringParser::generateProlog()
// Generate output files prolog
{
   // Strings
   // Header
   _header += QString( "//******************************************************************************\n");
   _header += QString( "//\n");
   _header += QString( "//  Str.h          String codes\n");
   _header += QString( "//  Version 1.0    (c) Robot\n");
   _header += QString( "//\n");
   _header += QString( "//******************************************************************************\n");
   _header += QChar( '\n');
   _header += QString( "#ifndef __Str_H__\n");
   _header += QString( "   #define __Str_H__\n");
   _header += QChar( '\n');
   _header += QString( "#ifndef __StrDef_H__\n");
   _header += QString( "   #include \"String/StrDef.h\"\n");
   _header += QString( "#endif\n");
   _header += QChar( '\n');
   _header += QString( "#define STR_NULL (char *)%1 // undefined string\n").arg( _stringIndex++);
   // Source
   _source += QString( "//******************************************************************************\n");
   _source += QString( "//\n");
   _source += QString( "//  Str.c          Strings translations\n");
   _source += QString( "//  Version 1.0    (c) Robot\n");
   _source += QString( "//\n");
   _source += QString( "//******************************************************************************\n");
   _source += QChar( '\n');
   _source += QString( "#include \"Str.h\"\n");
   _source += QChar( '\n');
   _source += QString( "//------------------------------------------------------------------------------\n");
   _source += QString( "//   String definitions\n");
   _source += QString( "//------------------------------------------------------------------------------\n");
   _source += QChar( '\n');
   _source += QString( "TAllStrings AllStrings = {\n");
   _source += QString( "// system :\n");
   _source += QString( "/* STR_NULL */   \"\",\n");
   _source += QChar( '\n');
   // CSV
   _csv    += QString( "\"LANGUAGE\",\"LNG_ENGLISH\"\n");
   _csv    += QString( "\"CODEPAGE\",\"NORMAL\"\n");
   _csv    += QString( "\"\"\n");
} // generateProlog

//------------------------------------------------------------------------------
//   Epilog
//------------------------------------------------------------------------------

void StringParser::generateEpilog()
// Generate output files epilog
{
   // Strings
   // Header
   _header += QChar( '\n');
   _header += QString( "// system :\n");
   _header += QString( "#define _STR_LAST (char *)%1 // strings count\n").arg( _stringIndex++);
   _header += QChar( '\n');
   _header += QString( "#endif\n");
   // Source
   _source += QChar( '\n');
   _source += QString( "// system :\n");
   _source += QString( "/* _STR_LAST */ \"?\"\n");
   _source += QString( "};\n");
   // CSV
} // generateEpilog

//------------------------------------------------------------------------------
//   Enum
//------------------------------------------------------------------------------

void StringParser::generateEnum( QString name)
// Generate enum <name>
{
   _strings += QString( "%1\n").arg( name);
   _header  += QString( "#define %1 (char *)%2\n").arg( name, -20).arg( _stringIndex);
   // dont' change _stringIndex
} // generateEnum

//------------------------------------------------------------------------------
//   String / enum item
//------------------------------------------------------------------------------

void StringParser::generateString( QString name, QString value, QString comment)
// Generate string with <name>,<value> and <comment>
{
   if( comment.isEmpty()){
      _strings += QString( "%1 \"%2\"\n").arg( name).arg( value);      
      _header  += QString( "#define %1 (char *)%2\n").arg( name, -20).arg( _stringIndex);
      _source  += QString( "/* %1 */ \"%2\",\n").arg( name, -20).arg( value);
      _csv     += QString( "\"%1\",\"%2\"\n").arg( name).arg( value);
   } else {
      _strings += QString( "%1 \"%2\" //%3\n").arg( name).arg( value).arg( comment);
      _header  += QString( "#define %1 (char *)%2\t//%3\n").arg( name, -20).arg( _stringIndex).arg( comment);
      _source  += QString( "/* %1 */ \"%2\",\t//%3\n").arg( name, -20).arg( value).arg( comment);
      _csv     += QString( "\"%1\",\"%2\",\"//%3\"\n").arg( name).arg( value).arg( comment);
   }
   _stringIndex++;
} // generateString

//------------------------------------------------------------------------------
//   Comment
//------------------------------------------------------------------------------

void StringParser::generateComment( QString text)
// Generate comment with <text>
{
   _strings += QString( "//%1\n").arg( text);
   _header  += QString( "//%1\n").arg( text);
   _source  += QString( "//%1\n").arg( text);
   _csv     += QString( "\"//%1\"\n").arg( text);
} // generateComment

//------------------------------------------------------------------------------
//   New line
//------------------------------------------------------------------------------

void StringParser::generateNewLine()
// Generate new line
{
   _strings += QChar( '\n');
   _header  += QChar( '\n');
   _source  += QChar( '\n');
   _csv     += QString( "\"\"\n");
} // generateNewLine
