﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataModel;
using Microsoft.AspNet.Identity;
using Claim = System.Security.Claims.Claim;

namespace DataContext
{
   /// <summary>
   ///     EntityFramework based user store implementation that supports IUserStore, IUserLoginStore, IUserClaimStore and
   ///     IUserRoleStore
   /// </summary>
   public class UserStore :
      IUserLoginStore<User, Guid>,
      IUserClaimStore<User, Guid>,
      IUserRoleStore<User, Guid>,
      IUserPasswordStore<User, Guid>,
      IUserSecurityStampStore<User, Guid>,
      IQueryableUserStore<User, Guid>,
      IUserEmailStore<User, Guid>,
      IUserPhoneNumberStore<User, Guid>,
      IUserTwoFactorStore<User, Guid>,
      IUserLockoutStore<User, Guid>
   {
      private readonly IDbSet<Login> logins;
      private readonly EntityStore<Role> roleStore;
      private readonly IDbSet<DataModel.Claim> userClaims;
      private bool disposed;
      private EntityStore<User> userStore;

      /// <summary>
      /// Constructor which creates a new instance of <see cref="BatModelContainer"/> context.
      /// </summary>
      public UserStore()
         : this(new BatModelContainer())
      {
      }

      /// <summary>
      ///     Constructor which takes a db context and wires up the stores with default instances using the context
      /// </summary>
      /// <param name="context"></param>
      public UserStore(BatModelContainer context)
      {
         if (context == null)
         {
            throw new ArgumentNullException("context");
         }
         Context = context;
         AutoSaveChanges = true;
         userStore = new EntityStore<User>(context);
         roleStore = new EntityStore<Role>(context);
         logins = Context.Set<Login>();
         userClaims = Context.Set<DataModel.Claim>();
      }

      /// <summary>
      ///     Context for the store
      /// </summary>
      public BatModelContainer Context { get; private set; }

      /// <summary>
      ///     If true will call dispose on the BatModelContainer during Dispose
      /// </summary>
      public bool DisposeContext { get; set; }

      /// <summary>
      ///     If true will call SaveChanges after Create/Update/Delete
      /// </summary>
      public bool AutoSaveChanges { get; set; }

      /// <summary>
      ///     Returns an IQueryable of users
      /// </summary>
      public IQueryable<User> Users
      {
         get { return userStore.EntitySet; }
      }

      /// <summary>
      ///     Return the claims for a user
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public virtual Task<IList<Claim>> GetClaimsAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         IList<Claim> result = user.Claims.Select(c => new Claim(c.ClaimType, c.ClaimValue)).ToList();
         return Task.FromResult(result);
      }

      /// <summary>
      ///     Add a claim to a user
      /// </summary>
      /// <param name="user"></param>
      /// <param name="claim"></param>
      /// <returns></returns>
      public virtual Task AddClaimAsync(User user, Claim claim)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         if (claim == null)
         {
            throw new ArgumentNullException("claim");
         }
         user.Claims.Add(new DataModel.Claim {UserId = user.Id, ClaimType = claim.Type, ClaimValue = claim.Value});
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Remove a claim from a user
      /// </summary>
      /// <param name="user"></param>
      /// <param name="claim"></param>
      /// <returns></returns>
      public virtual Task RemoveClaimAsync(User user, Claim claim)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         if (claim == null)
         {
            throw new ArgumentNullException("claim");
         }
         var claims =
            user.Claims.Where(uc => uc.ClaimValue == claim.Value && uc.ClaimType == claim.Type).ToList();
         foreach (var c in claims)
         {
            user.Claims.Remove(c);
         }
         // Note: these claims might not exist in the dbset
         var query =
            userClaims.Where(
               uc => uc.UserId.Equals(user.Id) && uc.ClaimValue == claim.Value && uc.ClaimType == claim.Type);
         foreach (var c in query)
         {
            userClaims.Remove(c);
         }
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Returns whether the user email is confirmed
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<bool> GetEmailConfirmedAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         return Task.FromResult(user.EmailConfirmed);
      }

      /// <summary>
      ///     Set IsConfirmed on the user
      /// </summary>
      /// <param name="user"></param>
      /// <param name="confirmed"></param>
      /// <returns></returns>
      public Task SetEmailConfirmedAsync(User user, bool confirmed)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         user.EmailConfirmed = confirmed;
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Set the user email
      /// </summary>
      /// <param name="user"></param>
      /// <param name="email"></param>
      /// <returns></returns>
      public Task SetEmailAsync(User user, string email)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         user.Email = email;
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Get the user's email
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<string> GetEmailAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         return Task.FromResult(user.Email);
      }

      /// <summary>
      ///     Find a user by email
      /// </summary>
      /// <param name="email"></param>
      /// <returns></returns>
      public Task<User> FindByEmailAsync(string email)
      {
         ThrowIfDisposed();
         return GetUserAggregateAsync(u => u.Email.ToUpper() == email.ToUpper());
      }

      /// <summary>
      ///     Returns the DateTimeOffset that represents the end of a user's lockout, any time in the past should be considered
      ///     not locked out.
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<DateTimeOffset> GetLockoutEndDateAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         return
            Task.FromResult(user.LockoutEndDateUtc.HasValue
               ? new DateTimeOffset(DateTime.SpecifyKind(user.LockoutEndDateUtc.Value, DateTimeKind.Utc))
               : new DateTimeOffset());
      }

      /// <summary>
      ///     Locks a user out until the specified end date (set to a past date, to unlock a user)
      /// </summary>
      /// <param name="user"></param>
      /// <param name="lockoutEnd"></param>
      /// <returns></returns>
      public Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         user.LockoutEndDateUtc = lockoutEnd == DateTimeOffset.MinValue ? (DateTime?) null : lockoutEnd.UtcDateTime;
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Used to record when an attempt to access the user has failed
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<int> IncrementAccessFailedCountAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         user.AccessFailedCount++;
         return Task.FromResult(user.AccessFailedCount);
      }

      /// <summary>
      ///     Used to reset the account access count, typically after the account is successfully accessed
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task ResetAccessFailedCountAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         user.AccessFailedCount = 0;
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Returns the current number of failed access attempts.  This number usually will be reset whenever the password is
      ///     verified or the account is locked out.
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<int> GetAccessFailedCountAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         return Task.FromResult(user.AccessFailedCount);
      }

      /// <summary>
      ///     Returns whether the user can be locked out.
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<bool> GetLockoutEnabledAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         return Task.FromResult(user.LockoutEnabled);
      }

      /// <summary>
      ///     Sets whether the user can be locked out.
      /// </summary>
      /// <param name="user"></param>
      /// <param name="enabled"></param>
      /// <returns></returns>
      public Task SetLockoutEnabledAsync(User user, bool enabled)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         user.LockoutEnabled = enabled;
         return Task.FromResult(0);
      }

      // Only call save changes if AutoSaveChanges is true

      /// <summary>
      ///     Find a user by id
      /// </summary>
      /// <param name="userId"></param>
      /// <returns></returns>
      public virtual Task<User> FindByIdAsync(Guid userId)
      {
         ThrowIfDisposed();
         return GetUserAggregateAsync(u => u.Id.Equals(userId));
      }

      /// <summary>
      ///     Find a user by name
      /// </summary>
      /// <param name="userName"></param>
      /// <returns></returns>
      public virtual Task<User> FindByNameAsync(string userName)
      {
         ThrowIfDisposed();
         return GetUserAggregateAsync(u => u.UserName.ToUpper() == userName.ToUpper());
      }

      /// <summary>
      ///     Insert an entity
      /// </summary>
      /// <param name="user"></param>
      public virtual async Task CreateAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         userStore.Create(user);
         await SaveChanges();
      }

      /// <summary>
      ///     Mark an entity for deletion
      /// </summary>
      /// <param name="user"></param>
      public virtual async Task DeleteAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         userStore.Delete(user);
         await SaveChanges();
      }

      /// <summary>
      ///     Update an entity
      /// </summary>
      /// <param name="user"></param>
      public virtual async Task UpdateAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         userStore.Update(user);
         await SaveChanges();
      }

      /// <summary>
      ///     Dispose the store
      /// </summary>
      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
      }

      // IUserLogin implementation

      /// <summary>
      ///     Returns the user associated with this login
      /// </summary>
      /// <returns></returns>
      public virtual async Task<User> FindAsync(UserLoginInfo login)
      {
         ThrowIfDisposed();
         if (login == null)
         {
            throw new ArgumentNullException("login");
         }
         var provider = login.LoginProvider;
         var key = login.ProviderKey;
         var userLogin =
            await logins.FirstOrDefaultAsync(l => l.LoginProvider == provider && l.ProviderKey == key);
         if (userLogin != null)
         {
            return await GetUserAggregateAsync(u => u.Id.Equals(userLogin.UserId));
         }
         return null;
      }

      /// <summary>
      ///     Add a login to the user
      /// </summary>
      /// <param name="user"></param>
      /// <param name="login"></param>
      /// <returns></returns>
      public virtual Task AddLoginAsync(User user, UserLoginInfo login)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         if (login == null)
         {
            throw new ArgumentNullException("login");
         }
         user.Logins.Add(new Login
         {
            UserId = user.Id,
            ProviderKey = login.ProviderKey,
            LoginProvider = login.LoginProvider
         });
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Remove a login from a user
      /// </summary>
      /// <param name="user"></param>
      /// <param name="login"></param>
      /// <returns></returns>
      public virtual Task RemoveLoginAsync(User user, UserLoginInfo login)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         if (login == null)
         {
            throw new ArgumentNullException("login");
         }
         var provider = login.LoginProvider;
         var key = login.ProviderKey;
         var entry = user.Logins.SingleOrDefault(l => l.LoginProvider == provider && l.ProviderKey == key);
         if (entry != null)
         {
            user.Logins.Remove(entry);
         }
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Get the logins for a user
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public virtual Task<IList<UserLoginInfo>> GetLoginsAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         IList<UserLoginInfo> result =
            user.Logins.Select(l => new UserLoginInfo(l.LoginProvider, l.ProviderKey)).ToList();
         return Task.FromResult(result);
      }

      /// <summary>
      ///     Set the password hash for a user
      /// </summary>
      /// <param name="user"></param>
      /// <param name="passwordHash"></param>
      /// <returns></returns>
      public Task SetPasswordHashAsync(User user, string passwordHash)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         user.PasswordHash = passwordHash;
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Get the password hash for a user
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<string> GetPasswordHashAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         return Task.FromResult(user.PasswordHash);
      }

      /// <summary>
      ///     Returns true if the user has a password set
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<bool> HasPasswordAsync(User user)
      {
         return Task.FromResult(user.PasswordHash != null);
      }

      /// <summary>
      ///     Set the user's phone number
      /// </summary>
      /// <param name="user"></param>
      /// <param name="phoneNumber"></param>
      /// <returns></returns>
      public Task SetPhoneNumberAsync(User user, string phoneNumber)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         user.PhoneNumber = phoneNumber;
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Get a user's phone number
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<string> GetPhoneNumberAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         return Task.FromResult(user.PhoneNumber);
      }

      /// <summary>
      ///     Returns whether the user phoneNumber is confirmed
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<bool> GetPhoneNumberConfirmedAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         return Task.FromResult(user.PhoneNumberConfirmed);
      }

      /// <summary>
      ///     Set PhoneNumberConfirmed on the user
      /// </summary>
      /// <param name="user"></param>
      /// <param name="confirmed"></param>
      /// <returns></returns>
      public Task SetPhoneNumberConfirmedAsync(User user, bool confirmed)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         user.PhoneNumberConfirmed = confirmed;
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Add a user to a role
      /// </summary>
      /// <param name="user"></param>
      /// <param name="roleName"></param>
      /// <returns></returns>
      public virtual Task AddToRoleAsync(User user, string roleName)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         if (String.IsNullOrWhiteSpace(roleName))
         {
            throw new ArgumentException(Resources.Resources.ValueCannotBeNullOrEmpty, "roleName");
         }
         var roleEntity = roleStore.DbEntitySet.SingleOrDefault(r => r.Name.ToUpper() == roleName.ToUpper());
         if (roleEntity == null)
         {
            throw new InvalidOperationException(String.Format(CultureInfo.CurrentCulture,
               Resources.Resources.RoleNotFound, roleName));
         }
         roleEntity.Users.Add(user);
         //var ur = new UserRole {UserId = user.Id, RoleId = roleEntity.Id};
         //userRoles.Add(ur);
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Remove a user from a role
      /// </summary>
      /// <param name="user"></param>
      /// <param name="roleName"></param>
      /// <returns></returns>
      public virtual Task RemoveFromRoleAsync(User user, string roleName)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         if (String.IsNullOrWhiteSpace(roleName))
         {
            throw new ArgumentException(Resources.Resources.ValueCannotBeNullOrEmpty, "roleName");
         }
         var roleEntity = roleStore.DbEntitySet.SingleOrDefault(r => r.Name.ToUpper() == roleName.ToUpper());
         if (roleEntity != null)
         {
            if (roleEntity.Users.Contains(user))
            {
               roleEntity.Users.Remove(user);
            }
            //var userRole = userRoles.FirstOrDefault(r => roleId.Equals(r.RoleId) && r.UserId.Equals(userId));
            //if (userRole != null)
            //{
            //    userRoles.Remove(userRole);
            //}
         }
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Get the names of the roles a user is a member of
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public virtual Task<IList<string>> GetRolesAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         //var query = from userRoles in user.Roles
         //   join roles in roleStore.DbEntitySet
         //      on userRoles.RoleId equals roles.Id
         //   select roles.Name;
         var query = from userRoles in user.Roles
            select userRoles.Name;
         return Task.FromResult<IList<string>>(query.ToList());
      }

      /// <summary>
      ///     Returns true if the user is in the named role
      /// </summary>
      /// <param name="user"></param>
      /// <param name="roleName"></param>
      /// <returns></returns>
      public virtual Task<bool> IsInRoleAsync(User user, string roleName)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         if (String.IsNullOrWhiteSpace(roleName))
         {
            throw new ArgumentException(Resources.Resources.ValueCannotBeNullOrEmpty, "roleName");
         }

         //var any = false;
         //var role = roleStore.DbEntitySet.SingleOrDefault(r => r.Name.ToUpper() == roleName.ToUpper());
         //if (role != null)
         //{
         //   any = role.Users.Any(ur => ur.RoleId.Equals(role.Id) && ur.UserId.Equals(user.Id));
         //   //_roleStore.DbEntitySet.Where(r => r.Name.ToUpper() == roleName.ToUpper())
         //   //    .Where(r => r.Users.Any(ur => ur.RoleId.Equals(r.Id) && ur.UserId.Equals(user.Id)))
         //   //    .Count() > 0;
         //}
         return Task.FromResult(user.Roles.Any(role => role.Name == roleName));
      }

      /// <summary>
      ///     Set the security stamp for the user
      /// </summary>
      /// <param name="user"></param>
      /// <param name="stamp"></param>
      /// <returns></returns>
      public Task SetSecurityStampAsync(User user, string stamp)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         user.SecurityStamp = stamp;
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Get the security stamp for a user
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<string> GetSecurityStampAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         return Task.FromResult(user.SecurityStamp);
      }

      /// <summary>
      ///     Set whether two factor authentication is enabled for the user
      /// </summary>
      /// <param name="user"></param>
      /// <param name="enabled"></param>
      /// <returns></returns>
      public Task SetTwoFactorEnabledAsync(User user, bool enabled)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         user.TwoFactorEnabled = enabled;
         return Task.FromResult(0);
      }

      /// <summary>
      ///     Gets whether two factor authentication is enabled for the user
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public Task<bool> GetTwoFactorEnabledAsync(User user)
      {
         ThrowIfDisposed();
         if (user == null)
         {
            throw new ArgumentNullException("user");
         }
         return Task.FromResult(user.TwoFactorEnabled);
      }

      private async Task SaveChanges()
      {
         if (AutoSaveChanges)
         {
            await Context.SaveChangesAsync();
         }
      }

      /// <summary>
      /// Used to attach child entities to the User aggregate, i.e. Roles, Logins, and Claims
      /// </summary>
      /// <param name="filter"></param>
      /// <returns></returns>
      protected virtual Task<User> GetUserAggregateAsync(Expression<Func<User, bool>> filter)
      {
         return Users.Include(u => u.Roles)
            .Include(u => u.Claims)
            .Include(u => u.Logins)
            .FirstOrDefaultAsync(filter);
      }

      private void ThrowIfDisposed()
      {
         if (disposed)
         {
            throw new ObjectDisposedException(GetType().Name);
         }
      }

      /// <summary>
      ///     If disposing, calls dispose on the Context.  Always nulls out the Context
      /// </summary>
      /// <param name="disposing"></param>
      protected virtual void Dispose(bool disposing)
      {
         if (DisposeContext && disposing && Context != null)
         {
            Context.Dispose();
         }
         disposed = true;
         Context = null;
         userStore = null;
      }
   }
}