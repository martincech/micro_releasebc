﻿using System.Linq;
using System.Windows;

namespace BatUploader
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            if (e.Args != null && e.Args.Any())
            {
                string first = ((string) e.Args[0]);

                if (first.Equals("-d"))
                {
                    this.Properties["DeleteFile"] = true;

                    if (e.Args.Count() > 1)
                    {
                        this.Properties["FileName"] = ((string)e.Args[1]);
                    }
                    
                } else
                {
                    this.Properties["FileName"] = ((string)e.Args[0]);
                }

            }

            base.OnStartup(e);
        }
    }
}