﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace BatApp
{
   public class RouteConfig
   {
      public static void RegisterRoutes(RouteCollection routes)
      {
         routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
         routes.Add("JsActionRoute", JsAction.JsActionRouteHandlerInstance.JsActionRoute);

         routes.MapRoute(
            name: "Scales",
            url: "Scales",
            defaults: new { controller = "Scales", action = "Index" });

         //Web Api routes  
         routes.MapHttpRoute(
            name: "DefaultApi",
            routeTemplate: "api/{apiVersion}/{controller}/{id}",
            defaults: new { id = RouteParameter.Optional }
            );

         routes.MapHttpRoute(
            name: "Stats2",
            routeTemplate: "api/{apiVersion}/Bat2s/{key}/{controller}/{id}",
            defaults: new { controller = "Stats", id = RouteParameter.Optional },
            constraints: new { controller = @".*Stats" }
            );

         routes.MapHttpRoute(
            name: "Bat2",
            routeTemplate: "api/{apiVersion}/Bat2s/{controller}/{id}",
            defaults: new { id = RouteParameter.Optional },
            constraints: new { controller = @".*(ConfigurationsV2|ConfigurationV2Updates)" }
            );

         

         routes.MapRoute(
            "Default",
            "{controller}/{action}/{id}",
            new { controller = "Weighings", action = "WeighingsPage", id = UrlParameter.Optional }
            );
      }
   }
}