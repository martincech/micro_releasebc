﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BatApp.Infrastructure;
using DataModel;
using DataModel.Reference_Data;
using Ninject;
using Utilities.IOC;

namespace BatApp
{
   public class MvcApplication : System.Web.HttpApplication
   {
      protected void Application_Start()
      {
         GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
         GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);

         IocContainer.Container = new NinjectIocContainer(new StandardKernel());
         DependencyResolver.SetResolver(new NinjectDependencyResolver(IocContainer.Container as IKernel));

         using (var dataContext = new BatModelContainer())
         {
            if (!dataContext.Database.Exists())
            {
               dataContext.Database.Create();
               var roles = dataContext.Roles.AddRange(InitalData.GetRoles());
               dataContext.SaveChanges();
               var user = InitalData.GetUser();
               user.Roles = new List<Role>() { roles.First(x => x.Name == Constants.ADMIN) };
               dataContext.Users.Add(user);
               dataContext.SaveChanges();
            }
         }

         AreaRegistration.RegisterAllAreas();
         GlobalConfiguration.Configure(WebApiConfig.Register);

         FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
         RouteConfig.RegisterRoutes(RouteTable.Routes);
         BundleConfig.RegisterBundles(BundleTable.Bundles);
         BindersConfig.Bind(ModelBinders.Binders);
      }
   }
}