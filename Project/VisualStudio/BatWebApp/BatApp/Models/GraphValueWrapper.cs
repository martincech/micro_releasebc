﻿namespace BatApp.Models
{
    public class GraphValueWrapper
    {
        public string Name { get; set; }
        public double Value { get; set; }
        public int Day { get; set; }
    }
}
