﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using DataModel;
// ReSharper disable InconsistentNaming

namespace BatApp.Models.DTO
{
   /// <summary>
   ///  Data transfer object for Curve
   /// </summary>
   public class CurveDTO: Curve
   {
      #region Overrides of Curve
      /// <summary>
      /// Overrides JsonIgnore for CurveValues
      /// </summary>
     
      public new ICollection<CurveValue> CurveValues
      {
         get; set;
      }
      #endregion
   }
}
