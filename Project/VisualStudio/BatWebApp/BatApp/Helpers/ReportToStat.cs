﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bat2Library;
using Bat2Library.Utilities;
using BatLibrary;

namespace BatApp.Helpers
{
   public class ReportToStat
   {
      public static List<Stat> GetStats(IEnumerable<Report> reports, Units unit)
      {
         return reports.Select(x=> GetStat(x,unit)).ToList(); 
      }

      public static Stat GetStat(Report report, Units unit)
      {
         if (report == null)
         {
            return null;
         }
         
         return new Stat()
         {
            Average = ConvertWeight.Convert(report.Average,unit,Units.KG),
            Count = report.Count,
            Cv = report.Cv,
            Date = report.Date,
            Day = report.Day,
            Gain =  ConvertWeight.Convert(report.Gain,unit,Units.KG),
            Sex = report.Sex,
            Sigma = ConvertWeight.Convert(report.Sigma, unit, Units.KG),
            Uni =  report.Uniformity
         };
      }

   }
}