﻿using System.Linq;
using System.Web.Mvc;

namespace BatApp.Helpers.ModelBinders
{
   public class IntArrayModelBinder : DefaultModelBinder
   {
      public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
      {
         var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName + "[]");
         if (value == null || string.IsNullOrEmpty(value.AttemptedValue))
         {
            return base.BindModel(controllerContext, bindingContext);
         }

         return value
            .AttemptedValue
            .Split(',')
            .Select(int.Parse)
            .ToArray();
      }
   }
}