﻿

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Web;
using System.Web.Mvc;
namespace BatApp.Helpers.ModelBindersOwn
{
   public class JQueryDataTablesRequestBinder : CustomDefaultModelBinder
   {
      public override object BindModel(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext)
      {
         return base.BindModel(controllerContext, bindingContext);
      }
   }


   public class CustomDefaultModelBinder : IModelBinder
   {
      private static string _resourceClassKey;
      private ModelBinderDictionary _binders;

      /// <summary>
      /// Gets or sets the model binders for the application.
      /// </summary>
      /// 
      /// <returns>
      /// The model binders for the application.
      /// </returns>
      protected internal ModelBinderDictionary Binders
      {
         get
         {
            if (this._binders == null)
               this._binders = System.Web.Mvc.ModelBinders.Binders;
            return this._binders;
         }
         set
         {
            this._binders = value;
         }
      }

      /// <summary>
      /// Gets or sets the name of the resource file (class key) that contains localized string values.
      /// </summary>
      /// 
      /// <returns>
      /// The name of the resource file (class key).
      /// </returns>
      public static string ResourceClassKey
      {
         get
         {
            return CustomDefaultModelBinder._resourceClassKey ?? string.Empty;
         }
         set
         {
            CustomDefaultModelBinder._resourceClassKey = value;
         }
      }

      private static void AddValueRequiredMessageToModelState(ControllerContext controllerContext, ModelStateDictionary modelState, string modelStateKey, Type elementType, object value)
      {
         if (value != null || TypeHelpers.TypeAllowsNullValue(elementType) || !modelState.IsValidField(modelStateKey))
            return;
         modelState.AddModelError(modelStateKey, CustomDefaultModelBinder.GetValueRequiredResource(controllerContext));
      }

      internal void BindComplexElementalModel(ControllerContext controllerContext, ModelBindingContext bindingContext, object model)
      {
         ModelBindingContext modelBindingContext = this.CreateComplexElementalModelBindingContext(controllerContext, bindingContext, model);
         if (!this.OnModelUpdating(controllerContext, modelBindingContext))
            return;
         this.BindProperties(controllerContext, modelBindingContext);
         this.OnModelUpdated(controllerContext, modelBindingContext);
      }

      internal object BindComplexModel(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext)
      {
         object model = bindingContext.Model;
         Type modelType1 = bindingContext.ModelType;
         if (model == null && modelType1.IsArray)
         {
            Type elementType = modelType1.GetElementType();
            Type modelType2 = typeof(List<>).MakeGenericType(elementType);
            object collection = this.CreateModel(controllerContext, bindingContext, modelType2);
            ModelBindingContext bindingContext1 = new ModelBindingContext()
            {
               ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType((Func<object>)(() => collection), modelType2),
               ModelName = bindingContext.ModelName,
               ModelState = bindingContext.ModelState,
               PropertyFilter = bindingContext.PropertyFilter,
               ValueProvider = bindingContext.ValueProvider
            };
            IList list = (IList)this.UpdateCollection(controllerContext, bindingContext1, elementType);
            if (list == null)
               return (object)null;
            Array instance = Array.CreateInstance(elementType, list.Count);
            list.CopyTo(instance, 0);
            return (object)instance;
         }
         if (model == null)
            model = this.CreateModel(controllerContext, bindingContext, modelType1);
         Type genericInterface1 = TypeHelpers.ExtractGenericInterface(modelType1, typeof(IDictionary<,>));
         if (genericInterface1 != (Type)null)
         {
            Type[] genericArguments = genericInterface1.GetGenericArguments();
            Type keyType = genericArguments[0];
            Type valueType = genericArguments[1];
            ModelBindingContext bindingContext1 = new ModelBindingContext()
            {
               ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType((Func<object>)(() => model), modelType1),
               ModelName = bindingContext.ModelName,
               ModelState = bindingContext.ModelState,
               PropertyFilter = bindingContext.PropertyFilter,
               ValueProvider = bindingContext.ValueProvider
            };
            return this.UpdateDictionary(controllerContext, bindingContext1, keyType, valueType);
         }
         Type genericInterface2 = TypeHelpers.ExtractGenericInterface(modelType1, typeof(IEnumerable<>));
         if (genericInterface2 != (Type)null)
         {
            Type elementType = genericInterface2.GetGenericArguments()[0];
            if (typeof(ICollection<>).MakeGenericType(elementType).IsInstanceOfType(model))
            {
               ModelBindingContext bindingContext1 = new ModelBindingContext()
               {
                  ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType((Func<object>)(() => model), modelType1),
                  ModelName = bindingContext.ModelName,
                  ModelState = bindingContext.ModelState,
                  PropertyFilter = bindingContext.PropertyFilter,
                  ValueProvider = bindingContext.ValueProvider
               };
               return this.UpdateCollection(controllerContext, bindingContext1, elementType);
            }
         }
         this.BindComplexElementalModel(controllerContext, bindingContext, model);
         return model;
      }

      /// <summary>
      /// Binds the model by using the specified controller context and binding context.
      /// </summary>
      /// 
      /// <returns>
      /// The bound object.
      /// </returns>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param><exception cref="T:System.ArgumentNullException">The <paramref name="bindingContext "/>parameter is null.</exception>
      public virtual object BindModel(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext)
      {
         RuntimeHelpers.EnsureSufficientExecutionStack();
         if (bindingContext == null)
            throw new ArgumentNullException("bindingContext");
         bool flag1 = false;
         if (!string.IsNullOrEmpty(bindingContext.ModelName) && !bindingContext.ValueProvider.ContainsPrefix(bindingContext.ModelName))
         {
            if (!bindingContext.FallbackToEmptyPrefix)
               return (object)null;
            bindingContext = new System.Web.Mvc.ModelBindingContext()
            {
               ModelMetadata = bindingContext.ModelMetadata,
               ModelState = bindingContext.ModelState,
               PropertyFilter = bindingContext.PropertyFilter,
               ValueProvider = bindingContext.ValueProvider
            };
            flag1 = true;
         }
         if (!flag1)
         {
            bool flag2 = CustomDefaultModelBinder.ShouldPerformRequestValidation(controllerContext, bindingContext);
            ValueProviderResult valueProviderResult = null;// bindingContext.UnvalidatedValueProvider.GetValue(bindingContext.ModelName, !flag2);
            if (valueProviderResult != null)
               return this.BindSimpleModel(controllerContext, bindingContext, valueProviderResult);
         }
         if (!bindingContext.ModelMetadata.IsComplexType)
            return (object)null;
         return this.BindComplexModel(controllerContext, bindingContext);
      }

      private void BindProperties(ControllerContext controllerContext, ModelBindingContext bindingContext)
      {
         PropertyDescriptorCollection modelProperties = this.GetModelProperties(controllerContext, bindingContext);
         Predicate<string> propertyFilter = bindingContext.PropertyFilter;
         for (int index = 0; index < modelProperties.Count; ++index)
         {
            PropertyDescriptor propertyDescriptor = modelProperties[index];
            if (CustomDefaultModelBinder.ShouldUpdateProperty(propertyDescriptor, propertyFilter))
               this.BindProperty(controllerContext, bindingContext, propertyDescriptor);
         }
      }

      /// <summary>
      /// Binds the specified property by using the specified controller context and binding context and the specified property descriptor.
      /// </summary>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param><param name="propertyDescriptor">Describes a property to be bound. The descriptor provides information such as the component type, property type, and property value. It also provides methods to get or set the property value.</param>
      protected virtual void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor)
      {
         string subPropertyName = CustomDefaultModelBinder.CreateSubPropertyName(bindingContext.ModelName, propertyDescriptor.Name);
         if (!bindingContext.ValueProvider.ContainsPrefix(subPropertyName))
            return;
         IModelBinder binder = this.Binders.GetBinder(propertyDescriptor.PropertyType);
         object obj = propertyDescriptor.GetValue(bindingContext.Model);
         ModelMetadata modelMetadata = bindingContext.PropertyMetadata[propertyDescriptor.Name];
         modelMetadata.Model = obj;
         System.Web.Mvc.ModelBindingContext bindingContext1 = new System.Web.Mvc.ModelBindingContext()
         {
            ModelMetadata = modelMetadata,
            ModelName = subPropertyName,
            ModelState = bindingContext.ModelState,
            ValueProvider = bindingContext.ValueProvider
         };
         object propertyValue = this.GetPropertyValue(controllerContext, bindingContext1, propertyDescriptor, binder);
         modelMetadata.Model = propertyValue;
         ModelState modelState = bindingContext.ModelState[subPropertyName];
         if (modelState == null || modelState.Errors.Count == 0)
         {
            if (!this.OnPropertyValidating(controllerContext, bindingContext, propertyDescriptor, propertyValue))
               return;
            this.SetProperty(controllerContext, bindingContext, propertyDescriptor, propertyValue);
            this.OnPropertyValidated(controllerContext, bindingContext, propertyDescriptor, propertyValue);
         }
         else
         {
            this.SetProperty(controllerContext, bindingContext, propertyDescriptor, propertyValue);
            foreach (ModelError modelError in Enumerable.ToList<ModelError>(Enumerable.Where<ModelError>((IEnumerable<ModelError>)modelState.Errors, (Func<ModelError, bool>)(err =>
            {
               if (string.IsNullOrEmpty(err.ErrorMessage))
                  return err.Exception != null;
               return false;
            }))))
            {
               for (Exception exception = modelError.Exception; exception != null; exception = exception.InnerException)
               {
                  if (exception is FormatException || exception is OverflowException)
                  {
                     string displayName = modelMetadata.GetDisplayName();
                     string errorMessage = string.Format((IFormatProvider)CultureInfo.CurrentCulture, CustomDefaultModelBinder.GetValueInvalidResource(controllerContext), new object[2]
              {
                (object) modelState.Value.AttemptedValue,
                (object) displayName
              });
                     modelState.Errors.Remove(modelError);
                     modelState.Errors.Add(errorMessage);
                     break;
                  }
               }
            }
         }
      }

      internal object BindSimpleModel(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext, ValueProviderResult valueProviderResult)
      {
         bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueProviderResult);
         if (bindingContext.ModelType.IsInstanceOfType(valueProviderResult.RawValue))
            return valueProviderResult.RawValue;
         if (bindingContext.ModelType != typeof(string) && !bindingContext.ModelType.IsArray)
         {
            Type genericInterface = TypeHelpers.ExtractGenericInterface(bindingContext.ModelType, typeof(IEnumerable<>));
            if (genericInterface != (Type)null)
            {
               object model = this.CreateModel(controllerContext, bindingContext, bindingContext.ModelType);
               Type collectionType = genericInterface.GetGenericArguments()[0];
               Type destinationType = collectionType.MakeArrayType();
               object newContents = CustomDefaultModelBinder.ConvertProviderResult(bindingContext.ModelState, bindingContext.ModelName, valueProviderResult, destinationType);
               if (typeof(ICollection<>).MakeGenericType(collectionType).IsInstanceOfType(model))
                  CustomDefaultModelBinder.CollectionHelpers.ReplaceCollection(collectionType, model, newContents);
               return model;
            }
         }
         return CustomDefaultModelBinder.ConvertProviderResult(bindingContext.ModelState, bindingContext.ModelName, valueProviderResult, bindingContext.ModelType);
      }

      private static bool CanUpdateReadonlyTypedReference(Type type)
      {
         return !type.IsValueType && !type.IsArray && !(type == typeof(string));
      }

      private static object ConvertProviderResult(ModelStateDictionary modelState, string modelStateKey, ValueProviderResult valueProviderResult, Type destinationType)
      {
         try
         {
            return valueProviderResult.ConvertTo(destinationType);
         }
         catch (Exception ex)
         {
            modelState.AddModelError(modelStateKey, ex);
            return (object)null;
         }
      }

      internal ModelBindingContext CreateComplexElementalModelBindingContext(ControllerContext controllerContext, ModelBindingContext bindingContext, object model)
      {
         BindAttribute bindAttr = (BindAttribute)this.GetTypeDescriptor(controllerContext, bindingContext).GetAttributes()[typeof(BindAttribute)];
         Predicate<string> predicate = bindAttr != null ? (Predicate<string>)(propertyName =>
         {
            if (bindAttr.IsPropertyAllowed(propertyName))
               return bindingContext.PropertyFilter(propertyName);
            return false;
         }) : bindingContext.PropertyFilter;
         return new ModelBindingContext()
         {
            ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType((Func<object>)(() => model), bindingContext.ModelType),
            ModelName = bindingContext.ModelName,
            ModelState = bindingContext.ModelState,
            PropertyFilter = predicate,
            ValueProvider = bindingContext.ValueProvider
         };
      }

      /// <summary>
      /// Creates the specified model type by using the specified controller context and binding context.
      /// </summary>
      /// 
      /// <returns>
      /// A data object of the specified type.
      /// </returns>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param><param name="modelType">The type of the model object to return.</param>
      protected virtual object CreateModel(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext, Type modelType)
      {
         Type type = modelType;
         if (modelType.IsGenericType)
         {
            Type genericTypeDefinition = modelType.GetGenericTypeDefinition();
            if (genericTypeDefinition == typeof(IDictionary<,>))
            {
               type = typeof(Dictionary<,>).MakeGenericType(modelType.GetGenericArguments());
            }
            else
            {
               if (!(genericTypeDefinition == typeof(IEnumerable<>)) && !(genericTypeDefinition == typeof(ICollection<>)))
               {
                  if (!(genericTypeDefinition == typeof(IList<>)))
                     goto label_6;
               }
               type = typeof(List<>).MakeGenericType(modelType.GetGenericArguments());
            }
         }
      label_6:
         try
         {
            return Activator.CreateInstance(type);
         }
         catch (MissingMethodException ex)
         {
            MissingMethodException missingMethodException = TypeHelpers.EnsureDebuggableException(ex, type.FullName);
            if (missingMethodException != null)
               throw missingMethodException;
            throw;
         }
      }

      /// <summary>
      /// Creates an index (a subindex) based on a category of components that make up a larger index, where the specified index value is an integer.
      /// </summary>
      /// 
      /// <returns>
      /// The name of the subindex.
      /// </returns>
      /// <param name="prefix">The prefix for the subindex.</param><param name="index">The index value.</param>
      protected static string CreateSubIndexName(string prefix, int index)
      {
         return string.Format((IFormatProvider)CultureInfo.InvariantCulture, "{0}[{1}]", new object[2]
      {
        (object) prefix,
        (object) index
      });
      }

      /// <summary>
      /// Creates an index (a subindex) based on a category of components that make up a larger index, where the specified index value is a string.
      /// </summary>
      /// 
      /// <returns>
      /// The name of the subindex.
      /// </returns>
      /// <param name="prefix">The prefix for the subindex.</param><param name="index">The index value.</param>
      protected static string CreateSubIndexName(string prefix, string index)
      {
         return string.Format((IFormatProvider)CultureInfo.InvariantCulture, "{0}[{1}]", new object[2]
      {
        (object) prefix,
        (object) index
      });
      }

      /// <summary>
      /// Creates the name of the subproperty by using the specified prefix and property name.
      /// </summary>
      /// 
      /// <returns>
      /// The name of the subproperty.
      /// </returns>
      /// <param name="prefix">The prefix for the subproperty.</param><param name="propertyName">The name of the property.</param>
      protected internal static string CreateSubPropertyName(string prefix, string propertyName)
      {
         if (string.IsNullOrEmpty(prefix))
            return propertyName;
         if (string.IsNullOrEmpty(propertyName))
            return prefix;
         return prefix + "." + propertyName;
      }

      /// <summary>
      /// Returns a set of properties that match the property filter restrictions that are established by the specified <paramref name="binding context"/>.
      /// </summary>
      /// 
      /// <returns>
      /// An enumerable set of property descriptors.
      /// </returns>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param>
      protected IEnumerable<PropertyDescriptor> GetFilteredModelProperties(ControllerContext controllerContext, ModelBindingContext bindingContext)
      {
         PropertyDescriptorCollection modelProperties = this.GetModelProperties(controllerContext, bindingContext);
         Predicate<string> propertyFilter = bindingContext.PropertyFilter;
         return Enumerable.Where<PropertyDescriptor>(Enumerable.Cast<PropertyDescriptor>((IEnumerable)modelProperties), (Func<PropertyDescriptor, bool>)(property => CustomDefaultModelBinder.ShouldUpdateProperty(property, propertyFilter)));
      }

      private static void GetIndexes(ModelBindingContext bindingContext, out bool stopOnIndexNotFound, out IEnumerable<string> indexes)
      {
         string subPropertyName = CustomDefaultModelBinder.CreateSubPropertyName(bindingContext.ModelName, "index");
         ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(subPropertyName);
         if (valueProviderResult != null)
         {
            string[] strArray = valueProviderResult.ConvertTo(typeof(string[])) as string[];
            if (strArray != null)
            {
               stopOnIndexNotFound = false;
               indexes = (IEnumerable<string>)strArray;
               return;
            }
         }
         stopOnIndexNotFound = true;
         indexes = CustomDefaultModelBinder.GetZeroBasedIndexes();
      }

      /// <summary>
      /// Returns the properties of the model by using the specified controller context and binding context.
      /// </summary>
      /// 
      /// <returns>
      /// A collection of property descriptors.
      /// </returns>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param>
      protected virtual PropertyDescriptorCollection GetModelProperties(ControllerContext controllerContext, ModelBindingContext bindingContext)
      {
         return this.GetTypeDescriptor(controllerContext, bindingContext).GetProperties();
      }

      /// <summary>
      /// Returns the value of a property using the specified controller context, binding context, property descriptor, and property binder.
      /// </summary>
      /// 
      /// <returns>
      /// An object that represents the property value.
      /// </returns>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param><param name="propertyDescriptor">The descriptor for the property to access. The descriptor provides information such as the component type, property type, and property value. It also provides methods to get or set the property value.</param><param name="propertyBinder">An object that provides a way to bind the property.</param>
      protected virtual object GetPropertyValue(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, IModelBinder propertyBinder)
      {
         object objA = propertyBinder.BindModel(controllerContext, bindingContext);
         if (bindingContext.ModelMetadata.ConvertEmptyStringToNull && object.Equals(objA, (object)string.Empty))
            return (object)null;
         return objA;
      }

      /// <summary>
      /// Returns the descriptor object for a type that is specified by its controller context and binding context.
      /// </summary>
      /// 
      /// <returns>
      /// A custom type descriptor object.
      /// </returns>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param>
      protected virtual ICustomTypeDescriptor GetTypeDescriptor(ControllerContext controllerContext, ModelBindingContext bindingContext)
      {
         return TypeDescriptorHelper.Get(bindingContext.ModelType);
      }

      private static string GetUserResourceString(ControllerContext controllerContext, string resourceName)
      {
         string str = (string)null;
         if (!string.IsNullOrEmpty(CustomDefaultModelBinder.ResourceClassKey) && controllerContext != null && controllerContext.HttpContext != null)
            str = controllerContext.HttpContext.GetGlobalResourceObject(CustomDefaultModelBinder.ResourceClassKey, resourceName, CultureInfo.CurrentUICulture) as string;
         return str;
      }

      private static string GetValueInvalidResource(ControllerContext controllerContext)
      {
         return "";// CustomDefaultModelBinder.GetUserResourceString(controllerContext, "PropertyValueInvalid") ?? MvcResources.DefaultModelBinder_ValueInvalid;
      }

      private static string GetValueRequiredResource(ControllerContext controllerContext)
      {
         return "";// CustomDefaultModelBinder.GetUserResourceString(controllerContext, "PropertyValueRequired") ?? MvcResources.DefaultModelBinder_ValueRequired;
      }

      private static IEnumerable<string> GetZeroBasedIndexes()
      {
         int i = 0;
         while (true)
         {
            yield return i.ToString((IFormatProvider)CultureInfo.InvariantCulture);
            ++i;
         }
      }

      /// <summary>
      /// Determines whether a data model is valid for the specified binding context.
      /// </summary>
      /// 
      /// <returns>
      /// true if the model is valid; otherwise, false.
      /// </returns>
      /// <param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param><exception cref="T:System.ArgumentNullException">The <paramref name="bindingContext"/> parameter is null.</exception>
      protected static bool IsModelValid(ModelBindingContext bindingContext)
      {
         if (bindingContext == null)
            throw new ArgumentNullException("bindingContext");
         if (string.IsNullOrEmpty(bindingContext.ModelName))
            return bindingContext.ModelState.IsValid;
         return bindingContext.ModelState.IsValidField(bindingContext.ModelName);
      }

      /// <summary>
      /// Called when the model is updated.
      /// </summary>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param>
      protected virtual void OnModelUpdated(ControllerContext controllerContext, ModelBindingContext bindingContext)
      {
         Dictionary<string, bool> dictionary = new Dictionary<string, bool>((IEqualityComparer<string>)StringComparer.OrdinalIgnoreCase);
         foreach (ModelValidationResult validationResult in ModelValidator.GetModelValidator(bindingContext.ModelMetadata, controllerContext).Validate((object)null))
         {
            string subPropertyName = CustomDefaultModelBinder.CreateSubPropertyName(bindingContext.ModelName, validationResult.MemberName);
            if (!dictionary.ContainsKey(subPropertyName))
               dictionary[subPropertyName] = bindingContext.ModelState.IsValidField(subPropertyName);
            if (dictionary[subPropertyName])
               bindingContext.ModelState.AddModelError(subPropertyName, validationResult.Message);
         }
      }

      /// <summary>
      /// Called when the model is updating.
      /// </summary>
      /// 
      /// <returns>
      /// true if the model is updating; otherwise, false.
      /// </returns>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param>
      protected virtual bool OnModelUpdating(ControllerContext controllerContext, ModelBindingContext bindingContext)
      {
         return true;
      }

      /// <summary>
      /// Called when the specified property is validated.
      /// </summary>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param><param name="propertyDescriptor">Describes a property to be validated. The descriptor provides information such as the component type, property type, and property value. It also provides methods to get or set the property value.</param><param name="value">The value to set for the property.</param>
      protected virtual void OnPropertyValidated(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, object value)
      {
      }

      /// <summary>
      /// Called when the specified property is validating.
      /// </summary>
      /// 
      /// <returns>
      /// true if the property is validating; otherwise, false.
      /// </returns>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param><param name="propertyDescriptor">Describes a property being validated. The descriptor provides information such as component type, property type, and property value. It also provides methods to get or set the property value.</param><param name="value">The value to set for the property.</param>
      protected virtual bool OnPropertyValidating(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, object value)
      {
         return true;
      }

      /// <summary>
      /// Sets the specified property by using the specified controller context, binding context, and property value.
      /// </summary>
      /// <param name="controllerContext">The context within which the controller operates. The context information includes the controller, HTTP content, request context, and route data.</param><param name="bindingContext">The context within which the model is bound. The context includes information such as the model object, model name, model type, property filter, and value provider.</param><param name="propertyDescriptor">Describes a property to be set. The descriptor provides information such as the component type, property type, and property value. It also provides methods to get or set the property value.</param><param name="value">The value to set for the property.</param>
      protected virtual void SetProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, object value)
      {
         ModelMetadata metadata = bindingContext.PropertyMetadata[propertyDescriptor.Name];
         metadata.Model = value;
         string subPropertyName = CustomDefaultModelBinder.CreateSubPropertyName(bindingContext.ModelName, metadata.PropertyName);
         if (value == null && bindingContext.ModelState.IsValidField(subPropertyName))
         {
            ModelValidator modelValidator = Enumerable.FirstOrDefault<ModelValidator>(Enumerable.Where<ModelValidator>(ModelValidatorProviders.Providers.GetValidators(metadata, controllerContext), (Func<ModelValidator, bool>)(v => v.IsRequired)));
            if (modelValidator != null)
            {
               foreach (ModelValidationResult validationResult in modelValidator.Validate(bindingContext.Model))
                  bindingContext.ModelState.AddModelError(subPropertyName, validationResult.Message);
            }
         }
         bool flag = value == null && !TypeHelpers.TypeAllowsNullValue(propertyDescriptor.PropertyType);
         if (!propertyDescriptor.IsReadOnly)
         {
            if (!flag)
            {
               try
               {
                  propertyDescriptor.SetValue(bindingContext.Model, value);
               }
               catch (Exception ex)
               {
                  if (bindingContext.ModelState.IsValidField(subPropertyName))
                     bindingContext.ModelState.AddModelError(subPropertyName, ex);
               }
            }
         }
         if (!flag || !bindingContext.ModelState.IsValidField(subPropertyName))
            return;
         bindingContext.ModelState.AddModelError(subPropertyName, CustomDefaultModelBinder.GetValueRequiredResource(controllerContext));
      }

      private static bool ShouldPerformRequestValidation(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext)
      {
         if (controllerContext == null || controllerContext.Controller == null || (bindingContext == null || bindingContext.ModelMetadata == null))
            return true;
         if (controllerContext.Controller.ValidateRequest)
            return bindingContext.ModelMetadata.RequestValidationEnabled;
         return false;
      }

      private static bool ShouldUpdateProperty(PropertyDescriptor property, Predicate<string> propertyFilter)
      {
         return (!property.IsReadOnly || CustomDefaultModelBinder.CanUpdateReadonlyTypedReference(property.PropertyType)) && propertyFilter(property.Name);
      }

      internal object UpdateCollection(ControllerContext controllerContext, ModelBindingContext bindingContext, Type elementType)
      {
         bool stopOnIndexNotFound;
         IEnumerable<string> indexes;
         CustomDefaultModelBinder.GetIndexes(bindingContext, out stopOnIndexNotFound, out indexes);
         IModelBinder binder = this.Binders.GetBinder(elementType);
         List<object> list = new List<object>();
         foreach (string index in indexes)
         {
            string subIndexName = CustomDefaultModelBinder.CreateSubIndexName(bindingContext.ModelName, index);
            if (!bindingContext.ValueProvider.ContainsPrefix(subIndexName))
            {
               if (stopOnIndexNotFound)
                  break;
            }
            else
            {
               System.Web.Mvc.ModelBindingContext bindingContext1 = new System.Web.Mvc.ModelBindingContext()
               {
                  ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType((Func<object>)null, elementType),
                  ModelName = subIndexName,
                  ModelState = bindingContext.ModelState,
                  PropertyFilter = bindingContext.PropertyFilter,
                  ValueProvider = bindingContext.ValueProvider
               };
               object obj = binder.BindModel(controllerContext, bindingContext1);
               CustomDefaultModelBinder.AddValueRequiredMessageToModelState(controllerContext, bindingContext.ModelState, subIndexName, elementType, obj);
               list.Add(obj);
            }
         }
         if (list.Count == 0)
            return (object)null;
         object model = bindingContext.Model;
         CustomDefaultModelBinder.CollectionHelpers.ReplaceCollection(elementType, model, (object)list);
         return model;
      }

      internal object UpdateDictionary(ControllerContext controllerContext, ModelBindingContext bindingContext, Type keyType, Type valueType)
      {
         bool stopOnIndexNotFound;
         IEnumerable<string> indexes;
         CustomDefaultModelBinder.GetIndexes(bindingContext, out stopOnIndexNotFound, out indexes);
         IModelBinder binder1 = this.Binders.GetBinder(keyType);
         IModelBinder binder2 = this.Binders.GetBinder(valueType);
         List<KeyValuePair<object, object>> list = new List<KeyValuePair<object, object>>();
         foreach (string index in indexes)
         {
            string subIndexName = CustomDefaultModelBinder.CreateSubIndexName(bindingContext.ModelName, index);
            string subPropertyName1 = CustomDefaultModelBinder.CreateSubPropertyName(subIndexName, "key");
            string subPropertyName2 = CustomDefaultModelBinder.CreateSubPropertyName(subIndexName, "value");
            if (!bindingContext.ValueProvider.ContainsPrefix(subPropertyName1) || !bindingContext.ValueProvider.ContainsPrefix(subPropertyName2))
            {
               if (stopOnIndexNotFound)
                  break;
            }
            else
            {
               System.Web.Mvc.ModelBindingContext bindingContext1 = new System.Web.Mvc.ModelBindingContext()
               {
                  ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType((Func<object>)null, keyType),
                  ModelName = subPropertyName1,
                  ModelState = bindingContext.ModelState,
                  ValueProvider = bindingContext.ValueProvider
               };
               object obj = binder1.BindModel(controllerContext, bindingContext1);
               CustomDefaultModelBinder.AddValueRequiredMessageToModelState(controllerContext, bindingContext.ModelState, subPropertyName1, keyType, obj);
               if (keyType.IsInstanceOfType(obj))
                  list.Add(CustomDefaultModelBinder.CreateEntryForModel(controllerContext, bindingContext, valueType, binder2, subPropertyName2, obj));
            }
         }
         if (list.Count == 0)
         {
            IEnumerableValueProvider enumerableValueProvider = bindingContext.ValueProvider as IEnumerableValueProvider;
            if (enumerableValueProvider != null)
            {
               foreach (KeyValuePair<string, string> keyValuePair in (IEnumerable<KeyValuePair<string, string>>)enumerableValueProvider.GetKeysFromPrefix(bindingContext.ModelName))
                  list.Add(CustomDefaultModelBinder.CreateEntryForModel(controllerContext, bindingContext, valueType, binder2, keyValuePair.Value, (object)keyValuePair.Key));
            }
         }
         object model = bindingContext.Model;
         CustomDefaultModelBinder.CollectionHelpers.ReplaceDictionary(keyType, valueType, model, (object)list);
         return model;
      }

      private static KeyValuePair<object, object> CreateEntryForModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type valueType, IModelBinder valueBinder, string modelName, object modelKey)
      {
         System.Web.Mvc.ModelBindingContext bindingContext1 = new System.Web.Mvc.ModelBindingContext()
         {
            ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType((Func<object>)null, valueType),
            ModelName = modelName,
            ModelState = bindingContext.ModelState,
            PropertyFilter = bindingContext.PropertyFilter,
            ValueProvider = bindingContext.ValueProvider
         };
         object obj = valueBinder.BindModel(controllerContext, bindingContext1);
         CustomDefaultModelBinder.AddValueRequiredMessageToModelState(controllerContext, bindingContext.ModelState, modelName, valueType, obj);
         return new KeyValuePair<object, object>(modelKey, obj);
      }

      private static class CollectionHelpers
      {
         private static readonly MethodInfo _replaceCollectionMethod = typeof(CustomDefaultModelBinder.CollectionHelpers).GetMethod("ReplaceCollectionImpl", BindingFlags.Static | BindingFlags.NonPublic);
         private static readonly MethodInfo _replaceDictionaryMethod = typeof(CustomDefaultModelBinder.CollectionHelpers).GetMethod("ReplaceDictionaryImpl", BindingFlags.Static | BindingFlags.NonPublic);

         [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
         public static void ReplaceCollection(Type collectionType, object collection, object newContents)
         {
            CustomDefaultModelBinder.CollectionHelpers._replaceCollectionMethod.MakeGenericMethod(collectionType).Invoke((object)null, new object[2]
        {
          collection,
          newContents
        });
         }

         private static void ReplaceCollectionImpl<T>(ICollection<T> collection, IEnumerable newContents)
         {
            collection.Clear();
            if (newContents == null)
               return;
            foreach (object obj1 in newContents)
            {
               T obj2 = obj1 is T ? (T)obj1 : default(T);
               collection.Add(obj2);
            }
         }

         [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
         public static void ReplaceDictionary(Type keyType, Type valueType, object dictionary, object newContents)
         {
            CustomDefaultModelBinder.CollectionHelpers._replaceDictionaryMethod.MakeGenericMethod(keyType, valueType).Invoke((object)null, new object[2]
        {
          dictionary,
          newContents
        });
         }

         private static void ReplaceDictionaryImpl<TKey, TValue>(IDictionary<TKey, TValue> dictionary, IEnumerable<KeyValuePair<object, object>> newContents)
         {
            dictionary.Clear();
            foreach (KeyValuePair<object, object> keyValuePair in newContents)
            {
               if (keyValuePair.Key is TKey)
               {
                  TKey index = (TKey)keyValuePair.Key;
                  TValue obj = keyValuePair.Value is TValue ? (TValue)keyValuePair.Value : default(TValue);
                  dictionary[index] = obj;
               }
            }
         }
      }
   }

   internal static class TypeDescriptorHelper
   {
      public static ICustomTypeDescriptor Get(Type type)
      {
         return new AssociatedMetadataTypeTypeDescriptionProvider(type).GetTypeDescriptor(type);
      }
   }

   internal static class TypeHelpers
   {
      private static readonly Dictionary<Type, TryGetValueDelegate> _tryGetValueDelegateCache = new Dictionary<Type, TryGetValueDelegate>();
      private static readonly ReaderWriterLockSlim _tryGetValueDelegateCacheLock = new ReaderWriterLockSlim();
      private static readonly MethodInfo _strongTryGetValueImplInfo = typeof(TypeHelpers).GetMethod("StrongTryGetValueImpl", BindingFlags.Static | BindingFlags.NonPublic);
      public static readonly Assembly MsCorLibAssembly = typeof(string).Assembly;
      public static readonly Assembly MvcAssembly = typeof(Controller).Assembly;
      public static readonly Assembly SystemWebAssembly = typeof(HttpContext).Assembly;

      public static TDelegate CreateDelegate<TDelegate>(Assembly assembly, string typeName, string methodName, object thisParameter) where TDelegate : class
      {
         Type type = assembly.GetType(typeName, false);
         if (type == (Type)null)
            return default(TDelegate);
         return TypeHelpers.CreateDelegate<TDelegate>(type, methodName, thisParameter);
      }

      public static TDelegate CreateDelegate<TDelegate>(Type targetType, string methodName, object thisParameter) where TDelegate : class
      {
         Type[] types = Array.ConvertAll<ParameterInfo, Type>(typeof(TDelegate).GetMethod("Invoke").GetParameters(), (Converter<ParameterInfo, Type>)(pInfo => pInfo.ParameterType));
         MethodInfo method = targetType.GetMethod(methodName, types);
         if (method == (MethodInfo)null)
            return default(TDelegate);
         return Delegate.CreateDelegate(typeof(TDelegate), thisParameter, method, false) as TDelegate;
      }

      public static TryGetValueDelegate CreateTryGetValueDelegate(Type targetType)
      {
         TypeHelpers._tryGetValueDelegateCacheLock.EnterReadLock();
         TryGetValueDelegate getValueDelegate;
         try
         {
            if (TypeHelpers._tryGetValueDelegateCache.TryGetValue(targetType, out getValueDelegate))
               return getValueDelegate;
         }
         finally
         {
            TypeHelpers._tryGetValueDelegateCacheLock.ExitReadLock();
         }
         Type genericInterface = TypeHelpers.ExtractGenericInterface(targetType, typeof(IDictionary<,>));
         if (genericInterface != (Type)null)
         {
            Type[] genericArguments = genericInterface.GetGenericArguments();
            Type type1 = genericArguments[0];
            Type type2 = genericArguments[1];
            if (type1.IsAssignableFrom(typeof(string)))
               getValueDelegate = (TryGetValueDelegate)Delegate.CreateDelegate(typeof(TryGetValueDelegate), TypeHelpers._strongTryGetValueImplInfo.MakeGenericMethod(type1, type2));
         }
         if (getValueDelegate == null && typeof(IDictionary).IsAssignableFrom(targetType))
            getValueDelegate = new TryGetValueDelegate(TypeHelpers.TryGetValueFromNonGenericDictionary);
         TypeHelpers._tryGetValueDelegateCacheLock.EnterWriteLock();
         try
         {
            TypeHelpers._tryGetValueDelegateCache[targetType] = getValueDelegate;
         }
         finally
         {
            TypeHelpers._tryGetValueDelegateCacheLock.ExitWriteLock();
         }
         return getValueDelegate;
      }

      public static Type ExtractGenericInterface(Type queryType, Type interfaceType)
      {
         if (TypeHelpers.MatchesGenericType(queryType, interfaceType))
            return queryType;
         return TypeHelpers.MatchGenericTypeFirstOrDefault(queryType.GetInterfaces(), interfaceType);
      }

      public static object GetDefaultValue(Type type)
      {
         if (!TypeHelpers.TypeAllowsNullValue(type))
            return Activator.CreateInstance(type);
         return (object)null;
      }

      public static bool IsCompatibleObject<T>(object value)
      {
         if (value is T)
            return true;
         if (value == null)
            return TypeHelpers.TypeAllowsNullValue(typeof(T));
         return false;
      }

      public static bool IsNullableValueType(Type type)
      {
         return Nullable.GetUnderlyingType(type) != (Type)null;
      }

      public static MissingMethodException EnsureDebuggableException(MissingMethodException originalException, string fullTypeName)
      {
         MissingMethodException missingMethodException = (MissingMethodException)null;
        /* if (!originalException.Message.Contains(fullTypeName))
            missingMethodException = new MissingMethodException(string.Format((IFormatProvider)CultureInfo.CurrentCulture, MvcResources.TypeHelpers_CannotCreateInstance, new object[2]
        {
          (object) originalException.Message,
          (object) fullTypeName
        }), (Exception)originalException);*/
         return missingMethodException;
      }

      private static bool MatchesGenericType(Type type, Type matchType)
      {
         if (type.IsGenericType)
            return type.GetGenericTypeDefinition() == matchType;
         return false;
      }

      private static Type MatchGenericTypeFirstOrDefault(Type[] types, Type matchType)
      {
         for (int index = 0; index < types.Length; ++index)
         {
            Type type = types[index];
            if (TypeHelpers.MatchesGenericType(type, matchType))
               return type;
         }
         return (Type)null;
      }

      private static bool StrongTryGetValueImpl<TKey, TValue>(object dictionary, string key, out object value)
      {
         TValue obj;
         bool flag = false;// ((IDictionary<TKey, TValue>)dictionary).TryGetValue((TKey)key, out obj);
         value = null;// (object)obj;
         return flag;
      }

      private static bool TryGetValueFromNonGenericDictionary(object dictionary, string key, out object value)
      {
         IDictionary dictionary1 = (IDictionary)dictionary;
         bool flag = dictionary1.Contains((object)key);
         value = flag ? dictionary1[(object)key] : (object)null;
         return flag;
      }

      public static bool TypeAllowsNullValue(Type type)
      {
         if (type.IsValueType)
            return TypeHelpers.IsNullableValueType(type);
         return true;
      }
   }

   internal delegate bool TryGetValueDelegate(object dictionary, string key, out object value);

   internal static class CollectionExtensions
   {
      public static T[] AppendAndReallocate<T>(this T[] array, T value)
      {
         int length = array.Length;
         T[] objArray = new T[length + 1];
         array.CopyTo((Array)objArray, 0);
         objArray[length] = value;
         return objArray;
      }

      public static T[] AsArray<T>(this IEnumerable<T> values)
      {
         return values as T[] ?? Enumerable.ToArray<T>(values);
      }

      public static Collection<T> AsCollection<T>(this IEnumerable<T> enumerable)
      {
         return enumerable as Collection<T> ?? new Collection<T>(enumerable as IList<T> ?? (IList<T>)new List<T>(enumerable));
      }

      public static IList<T> AsIList<T>(this IEnumerable<T> enumerable)
      {
         return enumerable as IList<T> ?? (IList<T>)new List<T>(enumerable);
      }

      public static List<T> AsList<T>(this IEnumerable<T> enumerable)
      {
         List<T> list = enumerable as List<T>;
         if (list != null)
            return list;
         ListWrapperCollection<T> wrapperCollection = enumerable as ListWrapperCollection<T>;
         if (wrapperCollection != null)
            return wrapperCollection.ItemsList;
         return new List<T>(enumerable);
      }

      public static void RemoveFrom<T>(this List<T> list, int start)
      {
         list.RemoveRange(start, list.Count - start);
      }

      public static T SingleDefaultOrError<T, TArg1>(this IList<T> list, Action<TArg1> errorAction, TArg1 errorArg1)
      {
         switch (list.Count)
         {
            case 0:
               return default(T);
            case 1:
               return list[0];
            default:
               errorAction(errorArg1);
               return default(T);
         }
      }

      public static TMatch SingleOfTypeDefaultOrError<TInput, TMatch, TArg1>(this IList<TInput> list, Action<TArg1> errorAction, TArg1 errorArg1) where TMatch : class
      {
         TMatch match1 = default(TMatch);
         for (int index = 0; index < list.Count; ++index)
         {
            TMatch match2 = (object)list[index] as TMatch;
            if ((object)match2 != null)
            {
               if ((object)match1 == null)
               {
                  match1 = match2;
               }
               else
               {
                  errorAction(errorArg1);
                  return default(TMatch);
               }
            }
         }
         return match1;
      }

      public static T[] ToArrayWithoutNulls<T>(this ICollection<T> collection) where T : class
      {
         T[] objArray1 = new T[collection.Count];
         int length = 0;
         foreach (T obj in (IEnumerable<T>)collection)
         {
            if ((object)obj != null)
            {
               objArray1[length] = obj;
               ++length;
            }
         }
         if (length == collection.Count)
            return objArray1;
         T[] objArray2 = new T[length];
         Array.Copy((Array)objArray1, (Array)objArray2, length);
         return objArray2;
      }

      public static Dictionary<TKey, TValue> ToDictionaryFast<TKey, TValue>(this TValue[] array, Func<TValue, TKey> keySelector, IEqualityComparer<TKey> comparer)
      {
         Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>(array.Length, comparer);
         for (int index = 0; index < array.Length; ++index)
         {
            TValue obj = array[index];
            dictionary.Add(keySelector(obj), obj);
         }
         return dictionary;
      }

      public static Dictionary<TKey, TValue> ToDictionaryFast<TKey, TValue>(this IList<TValue> list, Func<TValue, TKey> keySelector, IEqualityComparer<TKey> comparer)
      {
         TValue[] array = list as TValue[];
         if (array != null)
            return CollectionExtensions.ToDictionaryFast<TKey, TValue>(array, keySelector, comparer);
         return CollectionExtensions.ToDictionaryFastNoCheck<TKey, TValue>(list, keySelector, comparer);
      }

      public static Dictionary<TKey, TValue> ToDictionaryFast<TKey, TValue>(this IEnumerable<TValue> enumerable, Func<TValue, TKey> keySelector, IEqualityComparer<TKey> comparer)
      {
         TValue[] array = enumerable as TValue[];
         if (array != null)
            return CollectionExtensions.ToDictionaryFast<TKey, TValue>(array, keySelector, comparer);
         IList<TValue> list = enumerable as IList<TValue>;
         if (list != null)
            return CollectionExtensions.ToDictionaryFastNoCheck<TKey, TValue>(list, keySelector, comparer);
         Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>(comparer);
         foreach (TValue obj in enumerable)
            dictionary.Add(keySelector(obj), obj);
         return dictionary;
      }

      private static Dictionary<TKey, TValue> ToDictionaryFastNoCheck<TKey, TValue>(IList<TValue> list, Func<TValue, TKey> keySelector, IEqualityComparer<TKey> comparer)
      {
         int count = list.Count;
         Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>(count, comparer);
         for (int index = 0; index < count; ++index)
         {
            TValue obj = list[index];
            dictionary.Add(keySelector(obj), obj);
         }
         return dictionary;
      }
   }

   internal sealed class ListWrapperCollection<T> : Collection<T>
   {
      private readonly List<T> _items;

      internal List<T> ItemsList
      {
         get
         {
            return this._items;
         }
      }

      internal ListWrapperCollection()
         : this(new List<T>())
      {
      }

      internal ListWrapperCollection(List<T> list)
         : base((IList<T>)list)
      {
         this._items = list;
      }
   }


}
