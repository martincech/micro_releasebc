﻿using System;
using System.Web.Mvc.Filters;

namespace BatApp.Infrastructure.Filters
{
   public static class AuthenticationChallengeContextExtensions
   {
      public static void ChallengeWith(this AuthenticationChallengeContext filterContext, string challenge)
      {
         if (filterContext == null)
         {
            throw new ArgumentNullException("filterContext");
         }

         filterContext.Result = new ChallengeUnauthorizedResult(challenge, filterContext.Result);
      }
   }
}