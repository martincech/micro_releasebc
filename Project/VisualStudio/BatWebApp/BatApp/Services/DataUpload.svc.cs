﻿using System;
using System.IO;
using System.Xml;
using Services.FileUpload;
using DataContext;
using Microsoft.AspNet.Identity;
using Constants = DataModel.Constants;


namespace BatApp.Services
{
   public class ProtectedDataUpload : IProtectedDataUpload
   {
      public StatusInfo LoadFile(ProtectedRemoteFileInfo fileInfo)
      {
         var info = new StatusInfo();
         using (var userManager = new UserManager())
         {
            var user = userManager.Find(fileInfo.UserName, fileInfo.Password);

            if (user != null)
            {
               if (userManager.IsInRole(user.Id, Constants.USER) && !userManager.IsInRole(user.Id, Constants.FLOCK_MANAGEMENT))
               {
                  info.Status = string.Format(Resources.Resources.UserNoImportRights, fileInfo.UserName);
                  return info;
               }

               var document = new XmlDocument();
               document.Load(fileInfo.InputStream);
               
               if (!userManager.IsInRole(user.Id, Constants.ADMIN) &&
                   !userManager.IsInRole(user.Id, Constants.COMPANY_ADMIN))
               {
                  user = user.Parent;
               }

               try
               {
                  var importCount = DataFileImport.Import(user, document);
                  info.Status = string.Format(Resources.Resources.ReportsImported, importCount, fileInfo.UserName);
               }
               catch (InvalidDataException)
               {
                  info.Status = Resources.Resources.InvalidFileFormat;
               }
               catch (Exception)
               {
                  info.Status = Resources.Resources.UnknowDataUploadError;
               }
            }
            else
            {
               if (userManager.FindByName(fileInfo.UserName) != null)
               {
                  info.Status = string.Format(Resources.Resources.UserIncorectPassword, fileInfo.UserName);
               }
               else
               {
                  info.Status = string.Format(Resources.Resources.UserDoesntExist, fileInfo.UserName);
               }
            }
         }
         return info;
      }
   }
}