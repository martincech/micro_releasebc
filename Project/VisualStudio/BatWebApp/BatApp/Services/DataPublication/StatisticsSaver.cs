﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Bat2Library;
using DataContext;
using DataModel;

namespace BatApp.Services.DataPublication
{
   public class StatisticsSaver
   {
      #region Private fields

      private BatContext db;

      #endregion

      #region Public interfaces

      #region Constructors

      public StatisticsSaver()
         : this(null)
      {
      }

      public StatisticsSaver(BatContext container)
      {
         db = container;
      }

      #endregion

      /// <summary>
      /// Parse data and save them in database
      /// </summary>
      /// <param name="parser">parser to parse sended data</param>
      /// <param name="data">data to parse</param>
      /// <returns>true if saving to database succeed</returns>
      public bool ParseAndSave(IDataParser parser, object data)
      {
         if (!parser.ParseData(data))
         {
            return false;
         }
         return UpdateAndSave(parser);

      }


      #endregion

      #region Private helpers

      private bool UpdateAndSave(IDataParser parser)
      {

         if (parser.Male != null)
         {
            SaveOrUpdateStat(parser.Male, parser);
         }
         if (parser.Female != null)
         {
            SaveOrUpdateStat(parser.Female, parser);
         }
         return true;
      }

      private Scale CreateScale(Stat stat, string scaleName, string phoneNumber, int scaleNumber = 0)
      {
         return db.CreateScale(scaleNumber, scaleName, phoneNumber, "", stat);
      }

      /// <summary>
      ///  Save or update statistic in database.
      ///  </summary>
      /// <param name="stat">incoming statistic</param>
      /// <returns>Scale assigned this statistic</returns>
      private void SaveOrUpdateStat(Stat stat, IDataParser parser)
      {

         var user = db.Users.First(x => x.Id == db.UserId);
         bool isAdmin = Thread.CurrentPrincipal.IsInRole(Constants.ADMIN);

         //Get scale by tel
         //Todo: GET ALL SCALES
         var scale = db.Scales.FirstOrDefault(x => x.PhoneNumber == parser.PhoneNumber && parser.PhoneNumber != null);
         if (scale != null)
         {
            // + Check user company
            // + Check scale name  or scale number
            if ((scale.CompanyId == user.CompanyId || isAdmin) && string.IsNullOrEmpty(parser.ScaleName) ? scale.SerialNumber == 0 || parser.ScaleNumber == scale.SerialNumber : parser.ScaleName == scale.Name)
            {
               // + Asign data
               db.AddUpdateStat(stat, scale.Id);
            }
            else
            {
               // - Create scale
               CreateScale(stat, parser.ScaleName, parser.PhoneNumber, parser.ScaleNumber);
            }
         }
         else
         {

            // - Check user company + scale name or scale number
            scale =
               db.Scales.FirstOrDefault(
                  x =>
                     (x.CompanyId == user.CompanyId || isAdmin) && string.IsNullOrEmpty(parser.ScaleName) ? parser.ScaleNumber == x.SerialNumber : parser.ScaleName == x.Name);

            if (scale != null)
            {
               // + Update phone number
               scale.PhoneNumber = parser.PhoneNumber;
               scale.SerialNumber = parser.ScaleNumber;
               scale.Name = parser.ScaleName;
               db.UpdateScale(scale, scale.Id);
               // + Asign data
               db.AddUpdateStat(stat, scale.Id);
            }
            else
            {
               CreateScale(stat, parser.ScaleName, parser.PhoneNumber, parser.ScaleNumber);
            }
         }
      }
      #endregion
   }
}