﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BatApp.Startup))]
namespace BatApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
