﻿/* VIEWMODELS */
var closedTable;
/* VM for closed flock */
var ClosedFlocksVM = {
   closedSelected: ko.observableArray(),
   closedSelectedCount: ko.observable(0),
   totalClosedRows: ko.observable(0)
};

function renderDate(date) {
   var x = new Date(date);
   return $.datepicker.formatDate('dd.mm.yy', x);
}

var calcDataTableHeight = function () {
   
    var height = $(window).height() - 303;
    if (height < 427) {
        height = 427;
    }
    return height;
};

/* FUNCTIONS */
var createClosedTable = function() {
   if (typeof closedTable == 'undefined') {
      closedTable = $('#closedFlocksTable').dataTable({
         "language": {
             "url": baseURL + "Content/DataTables/Internationalisation/" + $.cookie("_culture") + ".txt"
         },
         "sScrollY": calcDataTableHeight(),
         "sScrollX": "100%",
         "sScrollXInner": "99.99%",
         "bServerSide": true,
         "sAjaxSource": baseURL + "Flocks/ClosedFlocksHandler",
         "bProcessing": true,
         "sDom": "frtiS",
         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            if (jQuery.inArray(aData[0], ClosedFlocksVM.closedSelected()) != -1) {
               $(nRow).addClass('row_selected');
            }
            return nRow;
         },
         "fnInitComplete": function(oSettings, json) {
            ClosedFlocksVM.totalClosedRows(closedTable.fnGetData().length);
            $("#closedRowsCount").text("[" + ClosedFlocksVM.totalClosedRows() + "]");
         },


         "fnDrawCallback": function() {
            ClosedFlocksVM.totalClosedRows(closedTable.fnGetData().length);
            $("#closedRowsCount").text("[" + ClosedFlocksVM.totalClosedRows() + "]");
         },

         "aoColumns": [
            {
               "bVisible": false,
               "bSearchable": false,
               "bSortable": false,
               "mDataProp": "Id"

            },
            { "mDataProp": "Name" },
            { "mDataProp": "Scale.Name" },
            {
               "render": function(data) { return renderDate(data); },
               "data": "StartDate"
            },
            {
               "render": function(data) { return renderDate(data); },
               "data": "EndDate"
            }
         ]
      });
   } else {
      $(window).trigger('resize');
      closedTable.fnDraw();
      $(window).trigger('resize');
   }
}; // delete flocks from database

ClosedFlocksVM.deleteFlocks = function() {
   if (ClosedFlocksVM.closedSelected().length > 0) {
      JsActions.Flocks.DeleteFlocks(
         ClosedFlocksVM.closedSelected(),
         {
            success: function() {
               closedTable.fnDraw();
               ClosedFlocksVM.closedSelectedCount(0);
               ClosedFlocksVM.closedSelected.removeAll();
               $('.modal').modal('hide');
            }
         });
   }
};

// reopen flocks from database
ClosedFlocksVM.reopenFlocks = function () {
   if (ClosedFlocksVM.closedSelected().length > 0) {
      JsActions.Flocks.ReopenFlocks(
         ClosedFlocksVM.closedSelected(),
         {
            success: function() {
               closedTable.fnDraw();
               ClosedFlocksVM.closedSelectedCount(0);
               ClosedFlocksVM.closedSelected.removeAll();
               $('.modal').modal('hide');
            }
         });
   }
};


/* OnDOM ready */

$(window).resize(function () {
    $('.dataTables_scrollBody').css('height', calcDataTableHeight());
});

$(document).ready(function () {
   var lastChecked;
   $('#closedFlocksTable tbody').on("click", "tr", function(event) {

      if (!lastChecked) {
         lastChecked = this;
      }
      var aData;
      var iId;
      if (event.shiftKey) {
         var start = $('#closedFlocksTable tbody tr').index(this);
         var end = $('#closedFlocksTable tbody tr').index(lastChecked);

         for (var i = Math.min(start, end); i <= Math.max(start, end); i++) {
            if (!$('#closedFlocksTable tbody tr').eq(i).hasClass('row_selected')) {
               aData = closedTable.fnGetData(i);
               iId = aData.Id;
               if (jQuery.inArray(iId, ClosedFlocksVM.closedSelected()) == -1) {
                  ClosedFlocksVM.closedSelected()[ClosedFlocksVM.closedSelected().length++] = iId;
                  ClosedFlocksVM.closedSelectedCount(ClosedFlocksVM.closedSelectedCount() + 1);
               }

               $('#closedFlocksTable tbody tr').eq(i).addClass("row_selected");
            }
         }

         // Clear browser text selection mask
         if (window.getSelection) {
            if (window.getSelection().empty) { // Chrome
               window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) { // Firefox
               window.getSelection().removeAllRanges();
            }
         } else if (document.selection) { // IE?
            document.selection.empty();
         }
      } else {
         aData = closedTable.fnGetData(this);
         iId = aData.Id;
         $(this).toggleClass('row_selected');

         if (jQuery.inArray(iId, ClosedFlocksVM.closedSelected()) == -1) {
            ClosedFlocksVM.closedSelected()[ClosedFlocksVM.closedSelected().length++] = iId;
            ClosedFlocksVM.closedSelectedCount(ClosedFlocksVM.closedSelectedCount() + 1);
         } else {

            ClosedFlocksVM.closedSelectedCount(ClosedFlocksVM.closedSelectedCount() - 1);
            ClosedFlocksVM.closedSelected(jQuery.grep(ClosedFlocksVM.closedSelected(), function(value) {
               return value != iId;
            }));
         }
      }
      lastChecked = this;
   });

   // tab with closed flocks loader
   $('#closedFlocks a').click(function(e) {
      e.preventDefault();
      $(this).tab('show');
      $("#actualRowsCount").text("");
      createClosedTable();
   });
});


ko.applyBindings(ClosedFlocksVM, document.getElementById("closedFlocksForm"));