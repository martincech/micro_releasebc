﻿using System;
using System.Web.Http;
using BatApp.Areas.UserAccounts.Helpers;
using BatApp.Infrastructure.Filters;
using DataContext;

namespace BatApp.Controllers
{
   [BasicAuthenticationFilter]
   public class BaseApiController : ApiController
   {
      private BatContext context;

      protected BatContext Context
      {
         get { return context ?? (context = new BatContext(CurrentUserId)); }
      }

      protected Guid CurrentUserId
      {
         get { return IdentityExtensions.GetUserId(User.Identity); }
      }
   }
}
