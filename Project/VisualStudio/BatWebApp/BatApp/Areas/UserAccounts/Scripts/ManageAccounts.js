﻿var selectedScales;

$("#company").change(function () {
    $.ajax({
        url: "/UserAccounts/Manage/GetCompanyScales/" + $(this).val(),
        method: "POST",
        success: function (data) {
            $("#SelectedScales").find('option').remove();
            $(data).each(function () {
                $("#SelectedScales").append($("<option />").val(this.Value).text(this.Text));
            });
            $("#SelectedScales").val(selectedScales);
        }
    });
});

//$('form').submit(function() {
   
//});


$(".radioCheckBox").change(function (e) {
    $(".radioCheckBox").not(this).attr('checked', false);
    $(".normalCheckBox").attr("checked", false);

    $(".normalCheckBox").parent().hide();
    $("#scalesList").hide();
    if ($(this).hasClass("User")) {
        $(".normalCheckBox").parent().show();
        $("#scalesList").show();
    }

    if ($(this).hasClass("Admin")) {
        $("#companyRow").hide();
    } else {
        $("#companyRow").show();
    }

    $(this).attr("checked", true);
});

$('.radioCheckBox').click(
    function (e) {
        if (!$(this).is(":checked")) {
            e.preventDefault();
        }
    });


$(document).ready(function () {
    $("#roleRow").appendTo("#roleTable");
    $("#roleFooter").appendTo("#roleTable");
    //.css("display","none");
    if (!($("input:checked").length > 0)) {
        $("input:checkbox").first().attr("checked", true);
        $("input:checkbox").first().trigger("change");
    }

    if (!$("input:checked").hasClass("User")) {
        $("#scalesList").hide();
        $("#roleFooter").hide();
    }

    if ($("input:checked").hasClass("Admin")) {
        $("#companyRow").hide();
    }

   

    
    selectedScales = $('#SelectedScales').val();
    $("#company").trigger("change");
});

