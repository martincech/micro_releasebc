﻿using System.Collections.Generic;
using DataModel;
using PagedList;
using System.Web.Mvc;

namespace BatApp.Areas.UserAccounts.Models.Manage
{
   public class IndexViewModel
   {
      public string Search { get; set; }
      public IPagedList<User> Users { get; set; }
      public IEnumerable<int> SelectedCompanies { get; set; }
      public IEnumerable<SelectListItem> Companies { get; set; }

   }
}