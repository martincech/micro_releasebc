﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BatApp.Areas.UserAccounts.Models.Manage
{
   public class DetailViewModel
   {
      [Required]
      [HiddenInput(DisplayValue = false)]
      public Guid UserGuid { get; set; }

   }
}