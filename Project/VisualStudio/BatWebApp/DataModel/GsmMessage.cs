//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
    
   
   using System;
   using System.Collections.Generic;
   using Newtonsoft.Json;
   
   public partial class GsmMessage
   {
      #region Constructors
   
      
      
      public GsmMessage()
      {
         this.Commands = new Commands();
         AditionalConstructor();
      }
   
      partial void AditionalConstructor();
   
      #endregion
   
      #region Primary keys
   
      /// <summary>
      /// This is primary key property!
      /// </summary>
      public virtual int Id { get; set; }
   
      #endregion
      public virtual Bat2Library.GsmEventMaskE EventMask { get; set; }
      public virtual Bat2Library.GsmPowerModeE Mode { get; set; }
      public virtual byte SwitchOnDuration { get; set; }
      public virtual byte SwitchOnPeriod { get; set; }
   
      public virtual Commands Commands { get; set; }
   
      [JsonIgnore]
      public virtual ICollection<SwitchOnTimes> SwitchOnTimes { get; set; }
      public virtual Configuration Configuration { get; set; }
   }
}
