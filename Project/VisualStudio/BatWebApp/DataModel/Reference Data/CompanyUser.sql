﻿USE [BatApp];
GO
SET IDENTITY_INSERT [dbo].[Companies] ON;
GO

MERGE INTO [dbo].[Companies] AS Target

USING (VALUES 
    (1, N'Veit Electronics')
) AS Source (
    [Id],
    [Name]
)
ON Target.[Id] = Source.[Id]
-- Update matched rows
WHEN MATCHED THEN
UPDATE SET
    [Name] = Source.[Name]
-- Insert new rows
WHEN NOT MATCHED BY TARGET THEN
INSERT (
    [Id],
    [Name]
)
VALUES (
    [Id],
    [Name]
);
GO

SET IDENTITY_INSERT [dbo].[Companies] OFF;
GO