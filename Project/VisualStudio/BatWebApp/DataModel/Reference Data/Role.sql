﻿USE [BatApp];
GO
MERGE INTO [dbo].[Roles] AS Target
USING (VALUES
    (N'7fd171ea-ccea-470d-9422-a83cd827ee22', N'Admin'),
    (N'edfe60b6-3b31-4f2f-945f-bd423a770910', N'FarmerAdmin'),
    (N'7e397278-7b19-468d-a6d2-e810d17469ce', N'CanDeleteWeighings'),
    (N'949e8a4b-99ca-40e6-924f-3448cf656b47', N'CanImportData'),
    (N'535e2360-45b1-4d41-9abc-0d965aec90a3', N'FlockManagement')
) AS Source ([Id], [Name])
ON Target.[Id] = Source.[Id]
-- Update matched rows
WHEN MATCHED THEN
UPDATE SET [Name] = Source.[Name]
-- Insert new rows
WHEN NOT MATCHED BY TARGET THEN
INSERT ([Id], [Name])
VALUES ([Id], [Name])
-- Delete rows that are in the target but not the source
WHEN NOT MATCHED BY SOURCE THEN
DELETE;
GO