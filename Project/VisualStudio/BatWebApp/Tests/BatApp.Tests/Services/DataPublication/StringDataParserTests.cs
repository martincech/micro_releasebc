﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BatApp.Services.DataPublication;

namespace BatApp.Tests.Services.DataPublication
{
   [TestClass]
   public class StringDataParserTests
   {
      private const string MSG_SHORT_CORRECT = "124123456 SCALE ef DAY 001 01.01.15 Cnt 0002 Avg 6,30 Gain 28,000 Sig 52,00 Cv 003 Uni 004";
      private const string MSG_SHORT_INCORRECT = "124123456 LOGIN pet panr PASSWORD Petr 123 SCALE ef DAY 1 01.01.15 Count 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";
      private const string MSG_SHORT_INCORRECT2 = "124123456 SCALE ef DAY -2 01.01.15 Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";

      private const string MSG_SHORT_CORRECT_DATE_FORMAT1 = "124123456 SCALE ef DAY 1 10.01.2015 Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";
      private const string MSG_SHORT_CORRECT_DATE_FORMAT2 = "124123456 SCALE ef DAY 1 10/01/2015 Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";
      private const string MSG_SHORT_CORRECT_DATE_FORMAT3 = "124123456 SCALE ef DAY 1 2015-02-10 Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";

      private const string MSG_LONG_INCORRECT_SCALE = "124123456 SCALE x a -1 DAY 2 03.03.15 FEMALES: Cnt 4 Avg 12,6 Gain 56 Sig 104 Cv 6 Uni 8 MALES: Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";
      private const string MSG_LONG_INCORRECT_DAY = "124123456 SCALE 1 DAY -1 03.03.15 FEMALES: Cnt 4 Avg 12,6 Gain 56 Sig 104 Cv 6 Uni 8 MALES: Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";
      private const string MSG_LONG_INCORRECT_CNT = "124123456 SCALE 1 DAY 2 03.03.15 FEMALES: Cnt -4 Avg 12,6 Gain 56 Sig 104 Cv 6 Uni 8 MALES: Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";
      private const string MSG_LONG_INCORRECT_AVG = "124123456 SCALE 1 DAY 2 03.03.15 FEMALES: Cnt 4 Avg -12,6 Gain 56 Sig 104 Cv 6 Uni 8 MALES: Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";
      private const string MSG_LONG_INCORRECT_SIG = "124123456 SCALE 1 DAY 2 03.03.15 FEMALES: Cnt 4 Avg 12,6 Gain 56 Sig -104 Cv 6 Uni 8 MALES: Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";
      private const string MSG_LONG_INCORRECT_CV = "124123456 SCALE 1 DAY 2 03.03.15 FEMALES: Cnt 4 Avg 12,6 Gain 56 Sig 104 Cv -6 Uni 8 MALES: Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";
      private const string MSG_LONG_INCORRECT_UNI = "124123456 SCALE 1 DAY 2 03.03.15 FEMALES: Cnt 4 Avg 12,6 Gain 56 Sig 104 Cv 6 Uni -8 MALES: Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";

      private const string MSG_SHORT_CORRECT_ID1 = "124123456 SCALE 7 DAY 1 10.01.2015 Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";
      private const string MSG_SHORT_CORRECT_ID2 = "124123456 SCALE ab DAY 1 10.01.2015 Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";
      private const string MSG_SHORT_CORRECT_ID3 = "124123456 SCALE c2a1 DAY 1 10.01.2015 Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";

      private const string MSG_LONG_CORRECT = "124123456 SCALE 1 DAY 2 03.03.15 FEMALES: Cnt 4 Avg 12,6 Gain 56 Sig 104 Cv 6 Uni 8 MALES: Cnt 2 Avg 6,3 Gain 28 Sig 52 Cv 3 Uni 4";

      private const int EXPECTED_ID = 239;
       
      [TestMethod]
      public void ShortMessage_Correct()
      {
         var parser = new StringDataParser();
         parser.ParseData(MSG_SHORT_CORRECT);
         var expectedDate = new DateTime(2015, 1, 1);
        
         Assert.AreEqual(1, parser.Female.Day);
         Assert.AreEqual(expectedDate, parser.Female.Date);
         Assert.AreEqual(Bat2Library.SexE.SEX_UNDEFINED, parser.Female.Sex);
         Assert.AreEqual(2, parser.Female.Count);
         Assert.AreEqual(6.3, parser.Female.Average);
         Assert.AreEqual(28, parser.Female.Gain);
         Assert.AreEqual(52, parser.Female.Sigma);
         Assert.AreEqual(3, parser.Female.Cv);
         Assert.AreEqual(4, parser.Female.Uni);
      }

      [TestMethod]
      public void LongMessage_Correct()
      {
         var parser = new StringDataParser();
         parser.ParseData(MSG_LONG_CORRECT);
         var expectedDate = new DateTime(2015, 3, 3);

         Assert.AreEqual(2, parser.Male.Day);
         Assert.AreEqual(expectedDate, parser.Male.Date);

         Assert.AreEqual(Bat2Library.SexE.SEX_FEMALE, parser.Female.Sex);
         Assert.AreEqual(4, parser.Female.Count);
         Assert.AreEqual(12.6, parser.Female.Average);
         Assert.AreEqual(56, parser.Female.Gain);
         Assert.AreEqual(104, parser.Female.Sigma);
         Assert.AreEqual(6, parser.Female.Cv);
         Assert.AreEqual(8, parser.Female.Uni);

         Assert.AreEqual(Bat2Library.SexE.SEX_MALE, parser.Male.Sex);
         Assert.AreEqual(2, parser.Male.Count);
         Assert.AreEqual(6.3, parser.Male.Average);
         Assert.AreEqual(28, parser.Male.Gain);
         Assert.AreEqual(52, parser.Male.Sigma);
         Assert.AreEqual(3, parser.Male.Cv);
         Assert.AreEqual(4, parser.Male.Uni);
      }

      [TestMethod]
      public void ShortMessage_Incorrect_MessageSize()
      {
         var parser = new StringDataParser();
         Assert.IsFalse(parser.ParseData(MSG_SHORT_CORRECT + " Day 23"));
      }

      [TestMethod]
      public void ShortMessage_Incorrect_WrongMessageFormat()
      {
         var parser = new StringDataParser();
         Assert.IsFalse(parser.ParseData(MSG_SHORT_INCORRECT));

      }

      [TestMethod]
      public void LongMessage_Incorrect_WrongMessageFormat_Scale()
      {
          var parser = new StringDataParser();
          Assert.IsFalse(parser.ParseData(MSG_LONG_INCORRECT_SCALE));

      }

      [TestMethod]
      public void LongMessage_Incorrect_WrongMessageFormat_Day()
      {
          var parser = new StringDataParser();
          Assert.IsFalse(parser.ParseData(MSG_LONG_INCORRECT_DAY));

      }

      [TestMethod]
      public void LongMessage_Incorrect_WrongMessageFormat_Cnt()
      {
          var parser = new StringDataParser();
          Assert.IsFalse(parser.ParseData(MSG_LONG_INCORRECT_CNT));

      }

      [TestMethod]
      public void LongMessage_Incorrect_WrongMessageFormat_Avg()
      {
          var parser = new StringDataParser();
          Assert.IsFalse(parser.ParseData(MSG_LONG_INCORRECT_AVG));

      }

      [TestMethod]
      public void LongMessage_Incorrect_WrongMessageFormat_Sig()
      {
          var parser = new StringDataParser();
          Assert.IsFalse(parser.ParseData(MSG_LONG_INCORRECT_SIG));

      }

      [TestMethod]
      public void LongMessage_Incorrect_WrongMessageFormat_Cv()
      {
          var parser = new StringDataParser();
          Assert.IsFalse(parser.ParseData(MSG_LONG_INCORRECT_CV));

      }

      [TestMethod]
      public void LongMessage_Incorrect_WrongMessageFormat_Uni()
      {
          var parser = new StringDataParser();
          Assert.IsFalse(parser.ParseData(MSG_LONG_INCORRECT_UNI));

      }

      [TestMethod]
      public void ShortMessage_Incorrect_ConvertValueFail()
      {
         var parser = new StringDataParser();
         Assert.IsFalse(parser.ParseData(MSG_SHORT_INCORRECT2));
      }

      [TestMethod]
      public void Parser_IncorectType()
      {
         var parser = new StringDataParser();
         Assert.IsFalse(parser.ParseData(new object()));
      }
       
   }
}
