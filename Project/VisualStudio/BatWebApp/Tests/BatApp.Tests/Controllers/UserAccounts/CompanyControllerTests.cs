﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using DataModel;
using System.Web.Mvc;
using DataContext;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using BatApp.Areas.UserAccounts.Controllers;
using BatApp.Areas.UserAccounts.Models.Company;

namespace BatApp.Tests.Controllers.UserAccounts
{
   [TestClass]
   public class CompanyControllerTests
   {
      private MockCompanyController controller;
      private ICollection<User> users;
      private User user;

      [TestInitialize]
      public void Init()
      {
         users = new Collection<User>();
         controller = new MockCompanyController();
         users = SampleData.GetCreatedUsers();
         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();
            }
            dataContext.Database.Create();
            dataContext.Users.AddRange(users).ToList();
            dataContext.SaveChanges();
            user = dataContext.Roles.First(x => x.Name == "Admin").Users.First();
            controller.SetExecutiveUser(user);
         }
      }

      [TestMethod]
      public void Index()
      {
         var result = controller.Index(1,"");
         Assert.IsTrue(result.Model is IndexViewModel);
        // Assert.IsTrue((result.Model as IndexViewModel).Companies.Select(s => s.Id).Contains(user.CompanyId));
      }

      [TestMethod]
      public void Create_Get()
      {
         var result = controller.Create();
          Assert.IsTrue((result as  ViewResult).Model is CompanyViewModel);
          Assert.AreEqual("Edit",(result as ViewResult).ViewName);
      }

      [TestMethod]
      public void Create_Post()
      {
         var result = controller.Create(new CompanyViewModel() 
         {
             Name = "NewTestCompany"
         });
         Assert.IsTrue(result is RedirectToRouteResult);
         Assert.IsTrue(controller.BatContext().Companies.Select(s => s.Name).Contains("NewTestCompany"));
      }

      [TestMethod]
      public void CheckIfCompanyExists()
      {
         var result = controller.CheckIfCompanyExists(user.Company.Name);
         Assert.IsTrue(result.Data.ToString().Contains("error"));

         result = controller.CheckIfCompanyExists("NotExistingCompanyName");
         Assert.IsFalse(result.Data.ToString().Contains("error"));
      }

      [TestMethod]
      public void Edit_Post()
      {
         var result = controller.Edit(new CompanyViewModel() { Id = user.Company.Id, Name = "NewName" });
         Assert.IsTrue(result is RedirectToRouteResult);
         Assert.AreEqual("NewName", controller.BatContext().Companies.First(f=>f.Id == user.CompanyId).Name);
      }

      [TestMethod]
      public void Delete()
      {
         var result = controller.Delete(user.Company.Id) as ViewResult;
         Assert.IsTrue(result.Model is CompanyViewModel);
         Assert.AreEqual(user.Company.Name, (result.Model as CompanyViewModel).Name);
         
      }

      [TestMethod]
      public void DeleteConfirmed()
      {
         var delUser = users.ElementAt(5);
         var result = controller.DeleteConfirmed(delUser.Company.Id);
         Assert.IsTrue(result is RedirectToRouteResult);
         Assert.IsNull(controller.BatContext().Companies.FirstOrDefault(f => f.Id == delUser.Company.Id));
      }



   }

   internal class MockCompanyController : CompanyController
   {
      public Guid UserGuid()
      {
         return CurrentUserId;
      }

      public BatContext BatContext()
      {
         return Context;
      }

      public User GetCurrentUser()
      {
         return CurrentUser;
      }
   }
}
