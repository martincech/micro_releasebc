﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BatApp.Controllers;
using DataContext;
using DataModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using BatApp.Helpers;
using BatApp.Models;
using System.Web.Mvc;

namespace BatApp.Tests.Controllers
{
   [TestClass]
   public class FlocksControllerTests
   {
      private MockFlocksController controller;
      private ICollection<User> users;
      private User user;
      private ICollection<Flock> flocks;

      [TestInitialize]
      public void Init()
      {
         users = new Collection<User>();
         controller = new MockFlocksController();
         users = SampleData.GetCreatedUsers();
         var scaleCompany1 = SampleData.CreateScales(users.First().Company, 1).First();
         var scaleCompany2 = SampleData.CreateScales(users.Last().Company, 1).First();

         var f1 = SampleData.CreateFlocks(scaleCompany1, 5);
         var f2 = SampleData.CreateFlocks(scaleCompany2, 5);

         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();

            }
            dataContext.Database.Create();
            dataContext.Users.AddRange(users).ToList();
            dataContext.Flocks.AddRange(f1);
            dataContext.Flocks.AddRange(f2);
            dataContext.SaveChanges();
            flocks = dataContext.Flocks.ToList();
            user = users.FirstOrDefault();
            controller.SetExecutiveUser(user);
         }
      }

      [TestMethod]
      public void Create()
      {
         var newFlock = new Flock()
         {
            Name = "TestFlock",
            InitialAge = 1,
            StartDate = DateTime.Now,
            ScaleId = flocks.First().ScaleId
         };
         var result = controller.Create(newFlock) as JsonNetResult;
         Assert.IsFalse(result.Data.ToString().Contains("error"));

         Assert.IsTrue(controller.BatContext().Flocks.Where(p => p.Name == "TestFlock").Any());

         newFlock = new Flock()
         {
            Name = "TestFlock",
            InitialAge = 1,
            StartDate = DateTime.Now,
            ScaleId = flocks.First().ScaleId
         };
         result = controller.Create(newFlock) as JsonNetResult;
         Assert.IsTrue(result.Data.ToString().Contains("error"));

         newFlock = new Flock() {Name = "", InitialAge = 1, StartDate = DateTime.Now, ScaleId = flocks.First().ScaleId};
         result = controller.Create(newFlock) as JsonNetResult;
         Assert.IsTrue(result.Data.ToString().Contains("error"));

         result = controller.Create(null) as JsonNetResult;
         Assert.IsTrue(result.Data.ToString().Contains("error"));

         newFlock = new Flock() {Name = "TestFlock", InitialAge = 1, StartDate = DateTime.Now, NewScale = "NewScale"};
         result = controller.Create(newFlock) as JsonNetResult;
         Assert.IsTrue(result.Data.ToString().Contains("error"));

      }

      [TestMethod]
      public void Edit()
      {
         var flock = controller.BatContext().Flocks.First();
         flock.Name = "ChangedName";
         flock.InitialAge = 3;
         flock.MaxDay = 5;
         flock.Note = "Note";
         flock.StartDate = DateTime.Now;
         var result = controller.Edit(flock);
         Assert.IsNotNull(result.Data);
         var editedFlock = controller.BatContext().Flocks.Where(p => p.Id == flock.Id).First();
         Assert.AreEqual("ChangedName", editedFlock.Name);
         Assert.AreEqual(3, editedFlock.InitialAge);
         Assert.AreEqual(5, editedFlock.MaxDay);
         Assert.AreEqual("Note", editedFlock.Note);
      }

      [TestMethod]
      public void DeleteFlocks()
      {
         int flockId = controller.BatContext().Flocks.First().Id;
         Assert.IsTrue(controller.BatContext().Flocks.Where(p => p.Id == flockId).Any());
         var result = controller.DeleteFlocks(new int[] {flockId});
         Assert.IsNotNull(result);
         Assert.IsFalse(controller.BatContext().Flocks.Where(p => p.Id == flockId).Any());
      }

      [TestMethod]
      public void ReopenFlocks()
      {
         using (var dataContext = new BatModelContainer())
         {
            var flock = dataContext.Flocks.First();
            flock.EndDate = DateTime.Now;
            dataContext.SaveChanges();
         }
         var flockReo = controller.BatContext().Flocks.First();
         Assert.IsNotNull(flockReo.EndDate);
         var result = controller.ReopenFlocks(new int[] {flockReo.Id});
         Assert.IsNull(controller.BatContext().Flocks.First().EndDate);
      }

      [TestMethod]
      public void GetAllFlocks()
      {
         //Admin
         var result = controller.GetAllFlocks();
         Assert.AreEqual(10, (result.Data as IEnumerable<object>).Count());
         //FarmerAdmin
         controller.SetExecutiveUser(users.ElementAt(1));
         result = controller.GetAllFlocks();
         Assert.AreEqual(5, (result.Data as IEnumerable<object>).Count());
         //User without asigned scale from same company but wihout Admin or farmer_admin rights
         controller.SetExecutiveUser(users.ElementAt(2));
         result = controller.GetAllFlocks();
         Assert.AreEqual(0, (result.Data as IEnumerable<object>).Count());
      }

      [TestMethod]
      public void GetOpenedFlocks()
      {
         using (var dataContext = new BatModelContainer())
         {
            var flock = dataContext.Flocks.First();
            flock.EndDate = DateTime.Now;
            dataContext.SaveChanges();
         }
         var result = controller.GetOpenedFlocks();
         Assert.AreEqual(9, (result.Data as IEnumerable<object>).Count());
      }

      [TestMethod]
      public void GetFlockNameById()
      {
         var ids = controller.BatContext().Flocks.Where(p => p.Id < 4).Select(s => s.Id).ToArray();
         var result = controller.GetFlockNameById(ids);
         Assert.AreEqual(ids.Count(), (result.Data as IEnumerable<object>).Count());
      }

      [TestMethod]
      public void GetFlock()
      {
         var flock = controller.BatContext().Flocks.First();
         var result = controller.GetFlock(flock.Id);
         Assert.AreEqual(flock.Name, (result.Data as Flock).Name);
         Assert.AreEqual(flock.Id, (result.Data as Flock).Id);
      }

      [TestMethod]
      public void GetScales()
      {
         //Admin
         var result = controller.GetScales();
         Assert.AreEqual(2, (result.Data as IEnumerable<object>).Count());
         //FarmerAdmin
         controller.SetExecutiveUser(users.ElementAt(1));
         result = controller.GetScales();
         Assert.AreEqual(1, (result.Data as IEnumerable<object>).Count());
         //User without asigned scale from same company but wihout Admin or farmer_admin rights
         controller.SetExecutiveUser(users.ElementAt(2));
         result = controller.GetScales();
         Assert.AreEqual(0, (result.Data as IEnumerable<object>).Count());
      }

      [TestMethod]
      public void GetCurves()
      {
         using (var dataContext = new BatModelContainer())
         {
            dataContext.Curves.AddRange(SampleData.CreateCurves(user.Company, 2));
            dataContext.Curves.AddRange(SampleData.CreateCurves(users.ElementAt(5).Company, 2));
            dataContext.SaveChanges();
         }

         //Admin
         var result = controller.GetCurves();
         Assert.AreEqual(4, (result.Data as IEnumerable<object>).Count());
         //FarmerAdmin
         controller.SetExecutiveUser(users.ElementAt(1));
         result = controller.GetCurves();
         Assert.AreEqual(2, (result.Data as IEnumerable<object>).Count());
         //User without asigned scale from same company but wihout Admin or farmer_admin rights
         controller.SetExecutiveUser(users.ElementAt(2));
         result = controller.GetCurves();
         Assert.AreEqual(2, (result.Data as IEnumerable<object>).Count());
      }

      [TestMethod]
      public void GetGraphData()
      {
         //var flockIds = controller.BatContext().Flocks.Where(w => w.Id < 5).Select(s => s.Id).ToArray();
         //var result = controller.GetGraphData(flockIds, "Average", new JQueryDataTablesRequest());
         ////How to test?
         //Assert.IsNotNull(result.Content);
      }

      [TestMethod]
      public void ActualFlocksHandler()
      {
         var result = controller.ActualFlocksHandler(new JQueryDataTablesRequest()
         {

         });

         Assert.IsNotNull(result.Data);
      }

      [TestMethod]
      public void ClosedFlocksHandler()
      {
         var result = controller.ClosedFlocksHandler(new JQueryDataTablesRequest()
         {


         });
         Assert.IsNotNull(result.Data);
      }

      [TestMethod]
      public void SaveUserSelectedFlocks()
      {
         var sFlocks = controller.BatContext().Flocks.Where(w => w.Id < 5).Select(s => s.Id).ToArray();
         var result = controller.SaveUserSelectedFlocks(sFlocks) as JsonResult;
         Assert.AreEqual("success", result.Data.ToString().ToLowerInvariant());
      }
   }

   internal class MockFlocksController : FlocksController
   {
      public Guid UserGuid()
      {
         return CurrentUserId;
      }

      public BatContext BatContext()
      {
         return Context;
      }

      public User GetCurrentUser()
      {
         return CurrentUser;
      }
   }
}
