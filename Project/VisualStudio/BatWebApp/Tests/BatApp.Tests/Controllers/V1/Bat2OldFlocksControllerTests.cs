﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Bat2Library.Bat2Old.DB.Helpers;
using BatApp.Controllers.V1;
using DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BatApp.Tests.Controllers.V1
{
   [TestClass]
   public class Bat2OldFlocksControllerTests
   {
      [TestInitialize]
      public void Init()
      {
         ICollection<User> users = new Collection<User>();
         var baseController = new MockController();
         users = SampleData.GetCreatedUsers();

         using (var dataContext = new BatModelContainer())
         {
            if (dataContext.Database.Exists())
            {
               dataContext.Database.Delete();
            }
            dataContext.Database.Create();
            dataContext.Users.AddRange(users);
            var scale1 = new Scale { Name = "scale01", SerialNumber = 123456 };
            dataContext.Scales.Add(scale1);
            var user = users.FirstOrDefault();
            dataContext.SaveChanges();
            baseController.SetExecutiveUser(user);
         }
      }

      [TestMethod]
      public void CreateFlock_WithName()
      {
         var controller = new Bat2OldFlocksController();
         using (var dataContext = new BatModelContainer())
         {
            var origFlocksCount = dataContext.Flocks.Count();
            var origCurvesCount = dataContext.Curves.Count();

            short number = 3;
            string name = "flock01";
            var flock = new DataFlock
            {
               EndDate = DateTime.Now.AddDays(31),
               Name = name,
               Scale = number,
               StartDate = DateTime.Now
            };

            var f = controller.Post(flock);
            Assert.IsNotNull(f);
            Assert.AreEqual(origFlocksCount + 1, dataContext.Flocks.Count());
            Assert.AreEqual(origCurvesCount, dataContext.Curves.Count());

            var dbFlock = dataContext.Flocks.FirstOrDefault(x => x.Name.Equals(name));
            Assert.IsNotNull(dbFlock);
         }
      }
   }
}
