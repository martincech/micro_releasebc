﻿using System;
using System.Timers;
using System.Windows;
using Bat2WebTransfer.ModelViews.Applications;

namespace Bat2WebTransfer.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for SynchronizationProgressView.xaml
   /// </summary>
   public partial class SynchronizationProgressView
   {
      private readonly Timer _timer;
      private int _countDown = 500;       //ms
      private double _epsilon = 0.0001;

      public SynchronizationProgressView()
      {
         InitializeComponent();

         _timer = new Timer(_countDown);
         _timer.Elapsed += TimerOnElapsed;
      }

      private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
      {
         _timer.Stop();
         Dispatcher.BeginInvoke(new Action(Close));
      }

      /// <summary>
      /// Start synchronization after page is loaded.
      /// </summary>
      private void SynchronizationProgressView_OnLoaded(object sender, RoutedEventArgs e)
      {
         var vm = DataContext as SynchronizationProgressViewModel;
         if (vm == null) return;
         
         vm.SynchroCommand.Execute(null);
      }

      /// <summary>
      /// After progress bar reach maximum, close the window, 
      /// but before we start timer so user can see that progress bar reached maximum.
      /// </summary>
      private void TotalProgress_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
      {
         if (Math.Abs(e.NewValue - TotalProgress.Maximum) <= _epsilon)
         {
            _timer.Start();
         }
      }
   }
}
