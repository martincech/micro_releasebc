﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Connection.Usb;
using GUI.Annotations;
using Connection.Routines;

namespace GUI
{
   
   public partial class MainWindow : INotifyPropertyChanged
   {
      private ConfigLoadTest ComObject
      {
         get { return comObject; }
         set
         {
            comObject = value; 
            HidDeviceCountChanged();
         }
      }

      private HidWatcher usbWatcher;
      private Stream connection ;

      private bool isProblem;
      private readonly BackgroundWorker backgroundWorker;
      private bool isBusy;
      private bool isConnected;
      private ConfigLoadTest comObject;
      private bool runsManager;

      public MainWindow()
      {
         usbWatcher = new HidWatcher();
         usbWatcher.DeviceConnected += UsbWatcherOnDeviceConnected;
         usbWatcher.DeviceDisconnected += UsbWatcherOnDeviceDisconnected;
         InitializeComponent();
         backgroundWorker = (BackgroundWorker) FindResource("backgroundWorker");
         Debug.Assert(backgroundWorker != null);
      }

      private void UsbWatcherOnDeviceDisconnected(object sender, HidDevice hidDevice)
      {
         if (ComObject != null && ComObject.Device == hidDevice)
         {
            StopSending();
            ComObject = null;
         }
         HidDeviceCountChanged();
      }

      private void UsbWatcherOnDeviceConnected(object sender, HidDevice hidDevice)
      {
         try
         {
            var device = new ConfigLoadTest(hidDevice);
            if (ComObject == null)
            {
               ComObject = device;
            }
         }
         catch
         {
         }
         HidDeviceCountChanged();
      }

      #region Bindable properties

      public bool IsProblem
      {
         get { return isProblem && IsBusy; }
         private set
         {
            if (value.Equals(isProblem)) return;
            isProblem = value;
            OnPropertyChanged();
         }
      }

      public bool IsBusy
      {
         get { return isBusy && IsConnected; }
         set
         {
            if (value.Equals(isBusy)) return;
            isBusy = value;
            OnPropertyChanged();
            OnPropertyChanged("IsProblem");
         }
      }

      public bool IsConnected
      {
         get { return IsBat2Connected && usbWatcher.Watching; }
      }

      public int HidDeviceCount
      {
         get { return HidDevice.Count; }
      }

      public int HidDeviceInnerCount
      {
         get { return HidDevice.InnerCount; }
      }
      public int HidWatcherCount
      {
         get { return usbWatcher.ConnectedDevices.Count(); }
      }

      public bool IsBat2Connected
      {
         get { return ComObject != null; }
      }

      public bool ManagerRuns
      {
         get { return runsManager; }
         set
         {
            if (value.Equals(runsManager)) return;
            runsManager = value;
            if (!value && IsBusy)
            {
               StopSending();
            }
            usbWatcher.Watching = value;
            OnPropertyChanged();
         }
      }

      #endregion

      #region Button events

      private void startButton_Click(object sender, RoutedEventArgs e)
      {
         Debug.Assert(ComObject != null);
         backgroundWorker.RunWorkerAsync(null);
      }

      private void stopButton_Click(object sender, RoutedEventArgs e)
      {
         StopSending();
      }

      private void window_Closing(object sender, CancelEventArgs e)
      {
         StopSending();
         ManagerRuns = false;
         usbWatcher = null;
      }
      #endregion

      #region Background job

      private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
      {
         IsBusy = true;
         while (!backgroundWorker.CancellationPending)
         {
            IsProblem = !ComObject.LoadConfig();
         }
      }

      private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
      {
         IsBusy = false;
      }

      #endregion

      #region Notification

      public event PropertyChangedEventHandler PropertyChanged;

      [NotifyPropertyChangedInvocator]
      protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
      {
         var handler = PropertyChanged;
         if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
      }

      #endregion

      private void ReadProblem()
      {
         MessageBox.Show(this, "Can't connect to BAT2, check cables!");
      }

      private void StopSending()
      {
         backgroundWorker.CancelAsync();
      }
      private void HidDeviceCountChanged()
      {
         OnPropertyChanged("HidWatcherCount");
         OnPropertyChanged("HidDeviceCount");
         OnPropertyChanged("HidDeviceInnerCount");
         OnPropertyChanged("IsBat2Connected");
         OnPropertyChanged("IsConnected");
         OnPropertyChanged("IsBusy");
      }

      private void Button_Click(object sender, RoutedEventArgs e)
      {
         HidDeviceCountChanged();
      }

   }
}
