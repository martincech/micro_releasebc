﻿using System;
using System.Linq;
using System.Windows.Data;

namespace Bat2Tester.ModelViews.Converters
{
   public class ChangeEndianToHexConverters : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         try
         {
            var val = (uint)value;
            if (val == 0) return 0;

            var bytes = BitConverter.GetBytes(val);
            var result = BitConverter.ToString(bytes.Reverse().ToArray()).Replace("-", string.Empty);
            return result;
         }
         catch (Exception)
         {
            return null;
         }
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
