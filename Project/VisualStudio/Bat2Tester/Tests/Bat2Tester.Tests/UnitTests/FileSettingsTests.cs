﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bat2Tester.Settings;
using Desktop.Wpf.Presentation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bat2Tester.SerialNumber.UnitTests
{
   [TestClass]
   public class FileSettingsTests
   {
      private static string testFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Test";

      [ClassInitialize]
      public static void Init(TestContext context)
      {
         DispatcherHelper.Initialize();
         if (!Directory.Exists(testFolder))
         {
            Directory.CreateDirectory(testFolder);
         }
      }

      [ClassCleanup]
      public static void ClassCleanup()
      {
         Directory.Delete(testFolder);
      }

      [TestCleanup]
      public void CleanUp()
      {
         File.Delete(testFolder + @"\" + EntryStorege.FILENAME);
      }

      private List<Record> GetListRecords(EntryStorege settings)
      {
         var pObject = new PrivateObject(settings);
         return (List<Record>)pObject.Invoke("Read", new object[] { testFolder + @"\" + EntryStorege.FILENAME });            
      }
         
      [TestMethod]
      public void Write_OK_Data()
      {
         var settings = new EntryStorege(new TextBox()) { Path = testFolder };
         settings.WriteEntry(101, 10);

         var list = GetListRecords(settings);
         Assert.AreEqual(1, list.Count);
         Assert.AreEqual(101, list.First().SerialNumber);
         Assert.AreEqual(10, list.First().Modification);
      }

      [TestMethod]
      public void Write_Existing_SerialNumber()
      {
         var settings = new EntryStorege(new TextBox()) { Path = testFolder };

         settings.WriteEntry(101, 10);
         var list = GetListRecords(settings);
         Assert.AreEqual(1, list.Count);
         Assert.AreEqual(101, list.First().SerialNumber);
         Assert.AreEqual(10, list.First().Modification);

         settings.WriteEntry(101, 1);
         list = GetListRecords(settings);
         Assert.AreEqual(1, list.Count);
         Assert.AreEqual(101, list.First().SerialNumber);
         Assert.AreEqual(1, list.First().Modification);
      }
   }
}
