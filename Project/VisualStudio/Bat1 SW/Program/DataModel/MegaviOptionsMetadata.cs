//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
namespace DataModel
{
   using System;
   using System.ComponentModel.DataAnnotations;
   using System.ComponentModel.DataAnnotations.Schema;
   
   [MetadataType(typeof(MegaviOptionsMetadata))]
   public partial class MegaviOptions
   {
      internal sealed class MegaviOptionsMetadata
   	{
      
        [Display(Name = "Address")]
      	public Nullable<byte> Address { get; set; }
   
        [Display(Name = "Code")]
      	public Nullable<byte> Code { get; set; }
   
        [Display(Name = "Device Address")]
      	public Nullable<byte> DeviceAddress { get; set; }
   
   	}
   }
}
