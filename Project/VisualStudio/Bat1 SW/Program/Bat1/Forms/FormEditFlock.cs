﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using Bat1.Properties;
using DataContext;

namespace Bat1 {
    public partial class FormEditFlock : Form {
        /// <summary>
        /// Edited flock definition
        /// </summary>
        public FlockDefinition FlockDefinition { get { return flockDefinition; } }
        private FlockDefinition flockDefinition;
        
        /// <summary>
        /// List of file names defined in the flock
        /// </summary>
        private List<FlockFile> flockFileNameList;

        private List<FlockFile> unsaveFlockFiles;

        private List<FlockFile> fileList;

        private CurveList curveList;

        private bool isMoreShown = true;

        public FormEditFlock(FlockDefinition flockDefinition) {
            InitializeComponent();

            // Puvodne mel kazdy soubor svoje vlastni casove obdobi, tj. v ramci hejna slo preskakovat z jednoho
            // souboru na jiny. Nakonec jsem to zrusil, takze hejno ma vzdy jen jedno globalni casove obdobi platne
            // pro vsechny soubory v hejnu. Interne (datove struktury, databaze, vypocty ve statistice) stale pracuji
            // tak, ze je preskakovani souboru mozne (co kdyby to do budoucna nekdo chtel). Pouze zde pri editaci hejna
            // pracuji tak, ze vsem souborum nastavim stejne casove obdobi, takze se ve statistice chovaji jakoby
            // preskakovani nebylo mozne. Kod je zde proto zbytecne slozity.
            
            // Preberu data, pracuju s kopii (kopiruju i ID)
            this.flockDefinition = new FlockDefinition(flockDefinition, true);

            // Preberu seznam jmen souboru, ten budu editovat zvlast           
            flockFileNameList = new List<FlockFile>(flockDefinition.FlockFileList);

            // Nahraju seznam dostupnych souboru z databaze
            fileList = Program.Database.LoadFileNameList();
            
            // Nahraju krivky z databaze
            LoadCurves();

            // Nastavim nazev okna na jmeno hejna
            Text = Resources.Flock + ": " + flockDefinition.Name;

            unsaveFlockFiles = new List<FlockFile>();
            // Vykreslim hejno
            
            // Datum zahajeni vezmu z prvniho souboru v hejnu (pro budouci pouziti maji casove obdobi
            // vsechny soubory, zatim nevyuzivam)
            dateTimePickerFrom.Value = flockDefinition.FlockFileList.Count > 0 ? flockDefinition.Started.Year < 1900 ? DateTime.Now : flockDefinition.Started : DateTime.Now;

            // Start vazeni
            numericUpDownStartDay.Text = flockDefinition.StartedDay.ToString();

            // Seznam souboru
            RedrawFiles();

            // Rustove krivky
            RedrawCurve(flockDefinition.CurveDefault, comboBoxCurveDefault);
            RedrawCurve(flockDefinition.CurveFemales, comboBoxCurveFemales);

            // Poznamka
            textBoxNote.Text = flockDefinition.Note;

            // Pokud hejno nema definovane rustove krivky ani neni zadana poznamka, skryju detaily.
            // Jinak zustavaji default zobrazene.
            if (flockDefinition.CurveDefault == null && flockDefinition.CurveFemales == null
                && flockDefinition.Note == "") {
                ShowDetails(false);
            }

            listBoxFiles.SelectedIndexChanged += listBoxFiles_SelectedIndexChanged;         
        }       
       
        private void ShowDetails(bool show) {
            isMoreShown            = show;
            groupBoxCurves.Visible = show;
            panelNote.Visible      = show;
            buttonMore.Text        = show ? Resources.HIDE_DETAILS : Resources.SHOW_DETAILS;

            // Zvetsim nebo zmensim okno
            var difference = groupBoxCurves.Height + groupBoxCurves.Margin.Top + groupBoxCurves.Margin.Bottom
                           + panelNote.Height + panelNote.Margin.Top + panelNote.Margin.Bottom;
            if (show) {
                Height += difference;
            } else {
                Height -= difference;
            }
        }
        
        private bool IsRowSelected() {
            if (listBoxFiles.Items.Count <= 0) {
                return false;     // V tabulce uz neni zadny soubor
            }

            if (listBoxFiles.SelectedIndex < 0) {
                return false;     // Neni vybrany zadny soubor
            }

            return true;
        }
        
        private void LoadCurvesToComboBox(ComboBox comboBox) {
            comboBox.Items.Clear();
            comboBox.Items.Add(Resources.CURVE_NONE);   // <none> na prvni pozici
            foreach (var curve in curveList.List) {
                comboBox.Items.Add(curve.Name);
            }
        }

        private void LoadCurves() {
            curveList = Program.Database.LoadCurveList();
            LoadCurvesToComboBox(comboBoxCurveDefault);
            LoadCurvesToComboBox(comboBoxCurveFemales);
        }

        private void RedrawCurve(Curve curve, ComboBox comboBox) {
            if (curve == null) {
                comboBox.SelectedIndex = 0;             // Vyberu <none>
                return;
            }

            var curveIndex = curveList.GetIndex(curve.Name);
            if (curveIndex < 0) {
                comboBox.SelectedIndex = 0;             // Vyberu <none>
                return;
            }
            comboBox.SelectedIndex = curveIndex + 1;    // Na 1. pozici je <none>
        }
        
        private void RedrawFiles() {
            listBoxFiles.Items.Clear();
            foreach (var fileName in flockFileNameList.Select(f => f.FileName)) 
            {
                listBoxFiles.Items.Add(fileName);
            }

           listBoxFiles_SelectedIndexChanged(null, null);
        }

        private void listBoxFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
           buttonDetail.Enabled = false || listBoxFiles.SelectedIndex >= 0;
        }

        private Curve GetCurve(ComboBox comboBox) {
            if (comboBox.SelectedIndex <= 0) {
                return null;
            } else {
                // Nedelam kopii, aby se mne udrzelo ID krivky
                return curveList.List[comboBox.SelectedIndex - 1];
            }
        }
        
        private bool SaveFlock() {
            // Kontrola hodnot

            // Musi definovat aspon jeden soubor
            if (flockFileNameList.Count == 0) {
                CheckValue.InvalidValueMessage(listBoxFiles, Resources.Flock_No_Files,false,0,0);
                return false;
            }

            // Soubory, datum Od nastavim u vsech souboru stejny, datum Do dam na maximum (default pro otevrene hejno)
            flockDefinition.FlockFileList.Clear();
            foreach (var file in flockFileNameList)
            {
               var flockFile = file;
               flockFile.From = dateTimePickerFrom.Value;
               flockFile.To = DateTime.MaxValue;
               flockFile.Id = file.Id;
               flockDefinition.FlockFileList.Add(flockFile);               
            }
            flockDefinition.Started = flockDefinition.FlockFileList.First().From;

            // Rustove krivky
            flockDefinition.CurveDefault = GetCurve(comboBoxCurveDefault);
            flockDefinition.CurveFemales = GetCurve(comboBoxCurveFemales);

            // Start vazeni
            flockDefinition.StartedDay = (int)numericUpDownStartDay.Value;

            // Poznamka
            flockDefinition.Note = textBoxNote.Text;

            return true;
        }

        private string GetSelectedCurveName(ComboBox comboBox) {
            if (comboBox.SelectedIndex <= 0) {
                return "";
            }
            return  comboBox.Text;
        }

        private void SelectCurve(ComboBox comboBox, string curveName) {
            comboBox.SelectedIndex = 0;            // Default vyberu <none>
            for (var i = 0; i < curveList.List.Count; i++) {
                if (curveList.List[i].Name == curveName) {
                    comboBox.SelectedIndex = i + 1;
                    return;
                }
            }
        }

        private void DeleteSelectedFile() {
            if (!IsRowSelected()) {
                return;     // Neni vybrany zadny radek
            }
            
            var selectedIndex = listBoxFiles.SelectedIndex;

            // Zeptam se, zda chce smazat aktualni soubor
            if (MessageBox.Show(String.Format(Resources.DELETE_FILE, listBoxFiles.Text), Program.ApplicationName, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }

            // Vymazu soubor na aktualni pozici a prekreslim
            flockFileNameList.RemoveAt(selectedIndex);
            listBoxFiles.Items.RemoveAt(selectedIndex);

            // Vyberu soubor pod prave vymazanym souborem (tj. zachovam index radku)
            if (selectedIndex < listBoxFiles.Items.Count) {
                listBoxFiles.SelectedIndex = selectedIndex;
            } else if (listBoxFiles.Items.Count > 0) {
                listBoxFiles.SelectedIndex = listBoxFiles.Items.Count - 1;
            }
            listBoxFiles.Focus();
        }

        private void buttonAddFile_Click(object sender, EventArgs e)
        {
           Cursor.Current = Cursors.WaitCursor;
            var form = new FormFlockFile(fileList);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Pridam do hejna vybrane soubory
            foreach (var selectedFile in form.selectedFileList) 
            {
                if (flockFileNameList.Contains(selectedFile)) 
                {
                    continue;   // Soubor uz v seznamu je
                }
                unsaveFlockFiles.Add(selectedFile);
                flockFileNameList.Add(selectedFile);
            }

            // Setridim podle nazvu
            flockFileNameList.Sort((a, b) => String.Compare(a.FileName, b.FileName, StringComparison.Ordinal));
          
            // Prekreslim
            RedrawFiles();
        }

        private void buttonDeleteFile_Click(object sender, EventArgs e) {
            DeleteSelectedFile();
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            // Ulozim data do hejna
            if (!SaveFlock()) {
                return;
            }
            DialogResult = DialogResult.OK;
        }

        private void buttonMore_Click(object sender, EventArgs e) {
            ShowDetails(!isMoreShown);
        }

        private void listBoxFiles_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode != Keys.Delete) {
                return;     // Klavesa Del maze
            }

            // Smazu
            DeleteSelectedFile();

            // Klavesu Del jsem zpracoval
            e.Handled = true;
        }

        private void buttonDetail_Click(object sender, EventArgs e)
        {
           ShowDetails();
        }

        /// <summary>
        /// Show weighing details.
        /// </summary>
        private void ShowDetails()
        {
           if (listBoxFiles.SelectedIndex < 0)
           {
              return;
           }

           Cursor.Current = Cursors.WaitCursor;
           var weighing = Program.Database.LoadWeighing(fileList[listBoxFiles.SelectedIndex].Id);
           Cursor.Current = Cursors.Default;
           if (weighing == null)
           {
              return;         // Tabulka je prazdna
           }

           if (weighing.WeighingData.SampleList == null)
           {
              // Rucni vazeni
              var formManual = new FormManualResults(weighing, true);
              formManual.ShowDialog();
           }
           else
           {
              // Vazeni z vahy

              // Predani 12tis vzorku trva, dam presypaci hodiny
              FormEditWeighing form;
              Cursor.Current = Cursors.WaitCursor;
              try
              {
                 // Nahraju z databaze seznam souboru
                 var fileListUnique = new NameNoteUniqueList();
                 foreach (var file in Program.Database.LoadFileNameNoteList())
                 {
                    fileListUnique.Add(file);
                 }

                 // Predam data oknu
                 form = new FormEditWeighing(weighing.WeighingData, fileListUnique, weighing.WeighingData.ScaleConfig, true, false);
              }
              finally
              {
                 Cursor.Current = Cursors.Default;
              }

              form.ShowDialog();
           }
        }

       /// <summary>
       /// Remove from flock file list unsave files.
       /// </summary>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
          if (unsaveFlockFiles.Count > 0)
          {
             foreach (var file in unsaveFlockFiles)
             {
                flockFileNameList.Remove(file);
             }
          }
        }

    }
}
