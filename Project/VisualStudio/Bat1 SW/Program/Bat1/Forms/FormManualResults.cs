﻿using System;
using System.Windows.Forms;
using Bat1.Properties;
using DataContext;

namespace Bat1
{
    public partial class FormManualResults : Form
    {
        private Weighing editWeighing = null;

        public FormManualResults()
        {
            InitializeComponent();
        }

        public FormManualResults(Weighing weighing)
        {
            InitializeComponent();

            editWeighing = weighing;

            userControlManualResults.LoadWeighing(weighing);

            buttonSave.Text = Resources.SAVE_CHANGES;
            AcceptButton = buttonSave;
        }

        public FormManualResults(Weighing weighing, bool readOnly)
            : this(weighing)
        {
            if (readOnly)
            {
                buttonSave.Visible = false;
            }
        }

        public FormManualResults(Weighing weighing, ScaleType scaleType)
           : this(weighing)
        {
           if (scaleType != ScaleType.SCALE_BAT1)
           {
              userControlManualResults.DisableControls();
           }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (!userControlManualResults.Save())
            {
                return;
            }

            if (editWeighing != null)
            {
                DialogResult = DialogResult.OK; // Pri editaci hned po ulozeni koncim
            }
        }
    }
}
