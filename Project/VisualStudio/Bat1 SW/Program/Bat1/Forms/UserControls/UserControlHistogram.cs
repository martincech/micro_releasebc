﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Bat1.Properties;
using Bat1.Utilities;
using Bat1Library.Statistics;
using BatLibrary;
using ZedGraph;

namespace Bat1 
{
    public partial class UserControlHistogram : UserControl 
    {
        private int mouseDownX, mouseDownY;
        private bool undoZoom;

        private double xAxisMin, xAxisMax = 1;

        private BoxObj boxUniformity;

        private readonly Color uniformityScreenColor = Color.FromArgb(15, Color.Black);
        private readonly Color uniformityPrintColor  = Color.Gainsboro;
        private readonly Color limitLineColor = Color.Blue;      
        private readonly Color histogramBarColor = Color.Red;
        private const double Y_AXIS_RATIO = 0.175;
        private List<TextObj> textLabels; 

        public ZedGraphControl Graph { get { return graph; } }

        /// <summary>
        /// Pen width for grid, tics and border
        /// </summary>
        private const float penWidth = 0.5F;          // Na monitoru se zobrazi korektne jako 1px, uzsi je jen pri tisku

        private void CreateGraph(ZedGraphControl zedGraphControl) {
			var graphPane = zedGraphControl.GraphPane;

            // Fonty s konstantni velikosti
            zedGraphControl.GraphPane.IsFontsScaled = false;

            // Barva pozadi stejna jako barva controlu, ktery histogram vlastni
            var fill = new Fill(Parent.BackColor);
            graphPane.Fill       = fill;
            graphPane.Chart.Fill = fill;

            // Nazev grafu
            graphPane.Title.IsVisible = false;

            // Schovam legendu
            graphPane.Legend.IsVisible = false;

            // Schovam mrizku okolo
            graphPane.Border.IsVisible = false;
            zedGraphControl.MasterPane.Border.IsVisible = false;
//            graphPane.Chart.Border.Color = Color.Gray;
            
            // Osa X
            graphPane.XAxis.Title.Text            = Resources.WEIGHT;
            graphPane.XAxis.Title.FontSpec.Size   = 11;
            graphPane.XAxis.Title.FontSpec.IsBold = false;
            graphPane.XAxis.MajorGrid.IsVisible   = false;          // Schovam mrizku
            graphPane.XAxis.MajorGrid.IsZeroLine  = false;          // Schovam cernou linku na hodnote 0
            graphPane.XAxis.MajorTic.IsOpposite   = false;
            graphPane.XAxis.MajorTic.Size         = 3.1F;
            graphPane.XAxis.MajorTic.IsInside     = false;
            graphPane.XAxis.MinorTic.IsOpposite   = false;
            graphPane.XAxis.MinorTic.Size         = 1.3F;
            graphPane.XAxis.MinorTic.IsInside     = false;
            graphPane.XAxis.Scale.FontSpec.Size   = 11;
    
            // Osa Y
            graphPane.YAxis.Title.Text            = Resources.COUNT;
            graphPane.YAxis.Title.FontSpec.Size   = 11;
            graphPane.YAxis.Title.FontSpec.IsBold = false;
            graphPane.YAxis.Scale.IsVisible       = false;          // Schovam hodnoty
            graphPane.YAxis.MajorGrid.IsVisible   = false;          // Schovam mrizku
            graphPane.YAxis.MajorGrid.IsZeroLine  = true;           // Zobrazim cernou linku na hodnote 0
            graphPane.YAxis.MajorTic.IsOutside    = false;
            graphPane.YAxis.MajorTic.IsOpposite   = false;
            graphPane.YAxis.MajorTic.IsInside     = false;
            graphPane.YAxis.MinorTic.IsOpposite   = false;
            graphPane.YAxis.MinorTic.IsInside     = false;
            graphPane.YAxis.MinorTic.IsOutside    = false;

			// Calculate the Axis Scale Ranges
			zedGraphControl.AxisChange();
		}

        public UserControlHistogram() 
        {
            InitializeComponent();

            // Disable zoom and panning
            Graph.IsEnableHZoom = false;
            Graph.IsEnableVZoom = false;
            Graph.IsEnableHPan = false;
            Graph.IsEnableVPan = false;
        }

        public void SetForPrinting() {
            // Pozadi
            graph.GraphPane.Fill.Type       = FillType.None;
            graph.GraphPane.Chart.Fill.Type = FillType.None;           

            // Rozsah histogramu
            boxUniformity.Fill.Color = uniformityPrintColor;

            // labels are hidden for printing, becasue in print mode they have different coordinates
            textLabels.ForEach(l => graph.GraphPane.GraphObjList.Remove(l));
        }

        public void SetForScreen() {
            // Pozor, hodnoty musi odpovidat hodnotam ve fci CreateGraph()

            // Pozadi
            graph.GraphPane.Fill.Type       = FillType.Solid;
            graph.GraphPane.Chart.Fill.Type = FillType.Solid;

            // Rozsah histogramu
            boxUniformity.Fill.Color = uniformityScreenColor;

            textLabels.ForEach(l => graph.GraphPane.GraphObjList.Add(l));
        }

        private void Redraw() {
            graph.ZoomOutAll(graph.GraphPane);  // Odstranim pripadny zoom
			graph.AxisChange();                 // Auto osy
            graph.Invalidate();                 // Prekreslim pomalu
        }
        
        public void ClearData() {
            // Smazu predchozi data
            graph.GraphPane.GraphObjList.Clear();     // Popisky nad sloupci, rozsah uniformity
            graph.GraphPane.CurveList.Clear();        // Prubeh

            // Prekreslim
            Redraw();
        }
        
        public void SetData(Histogram histogram, double average, int uniformityRange, Units units) 
        {
		    var graphPane = graph.GraphPane;
            graphPane.XAxis.Title.Text = Resources.WEIGHT + " ["+ units.ToLocalizedString() + "]";
            double minValue = double.MaxValue, maxValue = double.MinValue;

            // Smazu predchozi data
            ClearData();

            if (histogram == null)
            {
               return;     // Necham histogram prazdny a koncim
            }

            // Values are in base units [g], must be converted
            var avg = ConvertWeight.Convert(average, Units.G, units);
            var step = ConvertWeight.Convert(histogram.Step, Units.G, units);
            var viewValues = histogram.Values.Select(i => ConvertWeight.Convert(i, Units.G, units)).ToArray();

            // Ulozim do grafu data z histogramu, zaroven zjistim min a max hodnotu
			var list = new PointPairList();
			for (var i = 0; i < histogram.SlotsCount; i++) 
            {
               list.Add(viewValues[i], histogram.Counts[i]);
               if (viewValues[i] < minValue)
               {
                  minValue = viewValues[i];
               }
               if (viewValues[i] > maxValue)
               {
                  maxValue = viewValues[i];
               }
            }
            var barItem = graphPane.AddBar("", list, histogramBarColor);

            // Vypln sloupcu
            barItem.Bar.Fill = new Fill(histogramBarColor);

            // Sirka sloupcu
            graphPane.BarSettings.ClusterScaleWidthAuto = false;
            graphPane.BarSettings.ClusterScaleWidth     = step * 1.6;
            
            // Hodnoty sloupcu - default se vykresluje svisly text, musim delat sam pomoci TextObj

            // The ValueHandler is a helper that does some position calculations for us.
            var valueHandler = new ValueHandler(graphPane, true);
            var curve = graphPane.CurveList[0];
            var bar = curve as BarItem;

            if (bar == null) 
            {
                return;
            }
            
            var points = curve.Points;
            for (var i = 0; i < points.Count; i++) 
            {
                // Nuly nevykresluju
                if (points[i].Y == 0) 
                {
                    continue;
                }

                var xVal = valueHandler.BarCenterValue(curve, curve.GetBarWidth(graphPane), i, points[i].X, 0);

                // Calculate the Y value at the center of each bar
                var yVal = points[i].Y;

                // create the text item (assumes the x axis is ordinal or text)
                // for negative bars, the label appears just above the zero value
                var text = new TextObj(yVal.ToString("F0"), (float)xVal, (float)yVal);

                // Orezu hodnoty na ramecek grafu, jinak se pri zoomu nebo posuvu zobrazuji i mimo
                text.IsClippedToChartRect = true;
                
                // tell Zedgraph to use user scale units for locating the TextObj
                text.Location.CoordinateFrame = CoordType.AxisXYScale;
                text.FontSpec.Size = 9;
                // AlignH the left-center of the text to the specified point
                text.Location.AlignH = AlignH.Center;
                text.Location.AlignV = AlignV.Bottom;
                text.FontSpec.Border.IsVisible = false;
                // rotate the text 90 degrees
                text.FontSpec.Angle = 0;
                text.FontSpec.Fill.IsVisible = false;
                // add the TextObj to the list
                graphPane.GraphObjList.Add(text);
            }

            // Rozsahu uniformity
            // Zjistim maximalni vysku
            var maxCount = 0;
            foreach (var count in histogram.Counts) 
            {
                if (count > maxCount) {
                    maxCount = count;
                }
            }

            // Vykreslim obdelnik, vysku nastavim na dvojnasobek nejvyssiho sloupce
           boxUniformity = new BoxObj(avg*(1.0 - uniformityRange/100.0), 2*maxCount,
                                      2.0*uniformityRange*avg/100.0, 2*maxCount,
                                      Color.Transparent, uniformityScreenColor)
           {
              ZOrder = ZOrder.E_BehindCurves, 
              IsClippedToChartRect = true
           };
           // Umisteni az za sloupci histogramu
           // Orez rameckem grafu
           graphPane.GraphObjList.Add(boxUniformity);

            // Rucne nastavim minimum a maximum osy X, zaroven si meze zapamatuju pro rucni unzoom
            if (points.Count > 0) 
            {
               xAxisMin = viewValues[0] - step;
                if (xAxisMin < 0)
                {
                    xAxisMin = 0;
                }
                xAxisMax = viewValues[histogram.SlotsCount - 1] + step;
                graphPane.XAxis.Scale.Min = xAxisMin;
                graphPane.XAxis.Scale.Max = xAxisMax;
            }

            // Prekreslim
            Redraw();

           // draw limits lines
           var indent = step / 2.0;
           var limitBelow = ConvertWeight.Convert(histogram.Values[1], Units.G, units) - indent;
           var limitAbove = ConvertWeight.Convert(histogram.Values[histogram.Counts.Count() - 2], Units.G, units) + indent;
           AddLimitLines(limitBelow, limitAbove);

           // Print limit legend
           textLabels = new List<TextObj>();
           if (histogram.Counts[0] != 0)
           {
              var label = AddLabel(limitBelow, "Less or equal");
              textLabels.Add(label);
           }
           if (histogram.Counts[histogram.Counts.Count() - 1] != 0)
           {
              var label = AddLabel(limitAbove, "Greater or equal");
              textLabels.Add(label);
           }

           // Repair outer's column width
           if (limitBelow < step)
           {
              Graph.GraphPane.XAxis.Scale.Min -= step;
           }
           if (graphPane.XAxis.Scale.Max - limitAbove < step)
           {
              Graph.GraphPane.XAxis.Scale.Max += step;
           }
        }

       /// <summary>
       /// View text label below x-axis.
       /// </summary>
       /// <param name="x">x coordination</param>
       /// <param name="text">view text</param>
       private TextObj AddLabel(double x, string text)
       {
          var ydiff = (Graph.GraphPane.YAxis.Scale.Max - Graph.GraphPane.YAxis.Scale.Min) * Y_AXIS_RATIO;
          var label = new TextObj(text, x, -ydiff);
          label.FontSpec.Border.IsVisible = false;
          Graph.GraphPane.GraphObjList.Add(label);
          return label;
       }

       /// <summary>
       /// Add vertical limit lines to histogram, 
       /// which define below and above histogram's intervals. 
       /// </summary>
       /// <param name="x1">x position of first line</param>
        /// <param name="x2">x position of second line</param>
       private void AddLimitLines(double x1, double x2)
       {        
          var curve1 = new PointPairList
          {
             {x1, Graph.GraphPane.YAxis.Scale.Max}, 
             {x1, Graph.GraphPane.YAxis.Scale.Min}
          };
          Graph.GraphPane.AddCurve("", curve1, limitLineColor, SymbolType.None);
         
          var curve2 = new PointPairList
          {
             {x2, Graph.GraphPane.YAxis.Scale.Max}, 
             {x2, Graph.GraphPane.YAxis.Scale.Min}
          };
          Graph.GraphPane.AddCurve("", curve2, limitLineColor, SymbolType.None);
       }


        private void UserControlHistogram_Load(object sender, EventArgs e) {
            CreateGraph(graph);
        }

        private void graph_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState) {
            if (undoZoom) {
                // Misto zvetseni nastavim puvodni meritko
                sender.ZoomOutAll(sender.GraphPane);
                
                // Osu X mam nastavenou rucne
                sender.GraphPane.XAxis.Scale.Min = xAxisMin;
                sender.GraphPane.XAxis.Scale.Max = xAxisMax;

                // Osa Y je automaticky
                sender.GraphPane.YAxis.Scale.MinAuto = true;
                sender.GraphPane.YAxis.Scale.MaxAuto = true;
            }
        }

        private bool graph_MouseUpEvent(ZedGraphControl sender, MouseEventArgs e) {
            undoZoom = false;
            if (e.Button == MouseButtons.Left) {
                if (e.X < mouseDownX && e.Y < mouseDownY) {
                    // Smer doleva nahoru => udelam unzoom stejne jak jsem zvykly
                    undoZoom = true;
                }
            }
            return default(bool);
        }

        private bool graph_MouseDownEvent(ZedGraphControl sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                // Zapamatuju si souradnice stisku leveho tlacitka
                mouseDownX = e.X;
                mouseDownY = e.Y;
            }
            return default(bool);
        }
    }
}
