﻿using DataContext;

namespace Bat1.Forms.UserControls.Interfaces
{
   public interface IUserControlScaleConfigBase
   {
      /// <summary>
      /// Scale config to edit
      /// </summary>
      Bat1Scale ScaleConfig { get; set; }

      /// <summary>
      /// Read-only mode
      /// </summary>
      bool ReadOnly { get; set; }

      /// <summary>
      /// Values are being loaded into the controls
      /// </summary>
      bool IsLoading { get; set; }

      /// <summary>
      /// Set new config
      /// </summary>
      /// <param name="scaleConfig">Scale config to edit</param>
      void SetScaleConfig(Bat1Scale scaleConfig);

      /// <summary>
      /// Redraw control
      /// </summary>
      void Redraw();

   }
}
