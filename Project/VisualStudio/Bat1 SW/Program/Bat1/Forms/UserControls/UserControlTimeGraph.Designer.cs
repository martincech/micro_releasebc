﻿namespace Bat1 {
    partial class UserControlTimeGraph {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlTimeGraph));
         this.graph = new ZedGraph.ZedGraphControl();
         this.SuspendLayout();
         // 
         // graph
         // 
         resources.ApplyResources(this.graph, "graph");
         this.graph.EditButtons = System.Windows.Forms.MouseButtons.Left;
         this.graph.IsShowContextMenu = false;
         this.graph.Name = "graph";
         this.graph.PanButtons = System.Windows.Forms.MouseButtons.Right;
         this.graph.PanButtons2 = System.Windows.Forms.MouseButtons.Right;
         this.graph.PanModifierKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.None)));
         this.graph.ScrollGrace = 0D;
         this.graph.ScrollMaxX = 0D;
         this.graph.ScrollMaxY = 0D;
         this.graph.ScrollMaxY2 = 0D;
         this.graph.ScrollMinX = 0D;
         this.graph.ScrollMinY = 0D;
         this.graph.ScrollMinY2 = 0D;
         this.graph.ZoomEvent += new ZedGraph.ZedGraphControl.ZoomEventHandler(this.graph_ZoomEvent);
         this.graph.MouseDownEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.graph_MouseDownEvent);
         this.graph.MouseUpEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.graph_MouseUpEvent);
         // 
         // UserControlTimeGraph
         // 
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.Controls.Add(this.graph);
         this.Name = "UserControlTimeGraph";
         this.Load += new System.EventHandler(this.UserControlTimeGraph_Load);
         this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl graph;
    }
}
