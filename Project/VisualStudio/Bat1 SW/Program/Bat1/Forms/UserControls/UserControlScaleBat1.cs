﻿using System;
using System.Windows.Forms;
using Bat1.Forms.UserControls;
using Bat1.Properties;
using DataContext;

namespace Bat1 {
    public partial class UserControlScaleBat1 : UserControl, IBat1ScaleControl{
        public UserControlScaleBat1() {
            InitializeComponent();

            // Skryju vysledky nactene z vahy
            userControlScaleResults.Visible = false;
        }

        private void buttonRead_Click(object sender, EventArgs e) {
            // Zkontroluju, zda byla naposledy nactena data ulozena do DB
            if (!SaveStatus.Check(buttonRead.Text)) {
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            try {
                // Zkusim nacist vysledky z vahy
                if (!userControlScaleResults.ReadFromScale()) {
                    // Vaha neni pripojena, nepokracuju
                    userControlScaleResults.Visible = false;
                    SaveStatus.Saved = true;    // Neni co ukladat, tabulka je schovana
                    return;
                }
                
                // Uspesne jsem nacetl vysledky, zobrazim
                userControlScaleResults.Visible = true;

                // Zapamatuju si, ze nactena data zatim neulozil do DB
                SaveStatus.Saved     = false;
                SaveStatus.ScaleName = userControlScaleResults.ScaleName;
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            // Zkontroluju, zda byla naposledy nactena data ulozena do DB
            if (!SaveStatus.Check(buttonDelete.Text)) {
                return;
            }

            if (MessageBox.Show(Resources.ALL_DATA_WILL_BE_DELETED, buttonDelete.Text, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }
            
            // Vytvorim instanci vahy a zaroven zjistim, zda je nainstalovany driver vahy
            Bat1Version7 bat1;
            try {
                bat1 = new Bat1Version7();
            } catch {
                // Ovladac vahy neni nainstalovan, nelze pokracovat
                MessageBox.Show(Resources.DRIVER_NOT_INSTALLED, Program.ApplicationName);
                return;
            }
            
            Cursor.Current = Cursors.WaitCursor;
            Refresh();
            try {
                if (!bat1.ClearAllFiles()) {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show(Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
                    return;
                }
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonSetName_Click(object sender, EventArgs e) {
            // Nactu stavajici jmeno
            ScaleConfig scaleConfig = null;
            Bat1Version7 bat1;
            var waitForm = new FormScaleWait(ParentForm);
            Cursor.Current = Cursors.WaitCursor;
            try {
                waitForm.Show();
                waitForm.Refresh();

                if (!UserControlScaleSetup.LoadConfig(out bat1)) {
                    return;
                }

                scaleConfig = Bat1Version7.ReadGlobalConfig();
            } finally {
                waitForm.Close();
                Cursor.Current = Cursors.Default;
            }

            // Necham uzivatele jmeno editovat
            var form = new FormName(buttonSetName.Text, scaleConfig.ScaleName, true);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Ulozim nove jmeno
            Cursor.Current = Cursors.WaitCursor;
            Refresh();
            try {
                if (!bat1.SetName(form.EnteredName)) {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show(Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
                    return;
                }
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonSetDateTime_Click(object sender, EventArgs e) {
            if (MessageBox.Show(Resources.SET_DATE_TIME, buttonSetDateTime.Text, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }
            
            Bat1Version7 bat1;
            try {
                bat1 = new Bat1Version7();
            } catch {
                // Driver vahy neni nainstalovan
                MessageBox.Show(Resources.DRIVER_NOT_INSTALLED, Program.ApplicationName);
                return;
            }

            var waitForm = new FormScaleWait(ParentForm);
            Cursor.Current = Cursors.WaitCursor;
            try {
                waitForm.Show();
                waitForm.Refresh();

                if (!bat1.SetDateTime()) {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show(Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
                    return;
                }
            } finally {
                waitForm.Close();
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonSetParameters_Click(object sender, EventArgs e) {
            new FormScaleSetup().ShowDialog();
        }

       #region Implementation of IView

       /// <summary>
       /// Gets or sets the data context of the view.
       /// This will be set to ViewModel of the view.
       /// </summary>
       public object DataContext { get; set; }

       #endregion
    }

   public interface IBat1ScaleControl : IScaleControl
   {
   }
}
