﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DataContext;
using Desktop.WinForms;

namespace Bat1 {
    public partial class UserControlFlockList : UserControl {
        /// <summary>
        /// Sort mode of the flock list
        /// </summary>
        enum SortMode {
            /// <summary>
            /// Sort by name
            /// </summary>
            NAME,

            /// <summary>
            /// Sort by date and time of the flock start
            /// </summary>
            TIME
        }

        /// <summary>
        /// List of flocks
        /// </summary>
        public FlockDefinitionList FlockDefinitionList { get { return flockDefinitionList; } }
        private FlockDefinitionList flockDefinitionList;

        /// <summary>
        /// Sorting mode of flocks (default by name)
        /// </summary>
        private SortMode sortMode = SortMode.NAME;

        /// <summary>
        /// Gets or sets a value indicating whether the user can select more rows
        /// </summary>
        public bool MultiSelect {
            get { return dataGridViewFlocks.MultiSelect; }
            set { dataGridViewFlocks.MultiSelect = value; }
        }

        /// <summary>
        /// Flock delete event delegate
        /// </summary>
        /// <param name="flockDefinition">Flock to delete</param>
        public delegate void DeleteEventHandler(FlockDefinition flockDefinition);

        /// <summary>
        /// Flock delete event
        /// </summary>
        public event DeleteEventHandler DeleteEvent;

        /// <summary>
        /// Flock double-click event delegate
        /// </summary>
        /// <param name="flockDefinition">Flock to edit</param>
        public delegate void DoubleClickEventHandler(FlockDefinition flockDefinition);

        /// <summary>
        /// Flock double-click event
        /// </summary>
        public event DoubleClickEventHandler DoubleClickEvent;

        public UserControlFlockList() {
            InitializeComponent();
        }

        public void SetData(FlockDefinitionList flockDefinitionList) 
        {
            // Preberu data
            this.flockDefinitionList = flockDefinitionList;

            // Setridim a prekreslim
            Sort();

            // Vyberu prvni hejno v seznamu
            SelectFirstFlock();
        }

        /// <summary>
        /// Clear sorting glyphs in all columns
        /// </summary>
        private void ClearSortGlyphs() {
            foreach (DataGridViewColumn column in dataGridViewFlocks.Columns) {
                column.HeaderCell.SortGlyphDirection = SortOrder.None;
            }
        }

        private void DrawSortGlyph(int columnIndex) {
            ClearSortGlyphs();
            dataGridViewFlocks.Columns[columnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
        }

        /// <summary>
        /// Sort flock list by name and redraw
        /// </summary>
        private void SortByName() {
            // Setridim
            if (flockDefinitionList != null) {
                flockDefinitionList.SortByName();
            }

            // Prekreslim
            DrawSortGlyph(0);
        }

        /// <summary>
        /// Sort flock list by time and redraw
        /// </summary>
        private void SortByTime() {
            // Setridim
            if (flockDefinitionList != null) {
                flockDefinitionList.SortByTime();
            }

            // Prekreslim
            DrawSortGlyph(1);
        }

        private void Redraw() 
        {
            dataGridViewFlocks.Rows.Clear();
            foreach (var flockDefinition in flockDefinitionList.List)
            {
               var date = flockDefinition.FlockFileList.Count == 0 ? DateTime.Now : flockDefinition.Started; // flockDefinition.FlockFileList[0].From

               var span = DateTime.Now.Date - date.Date;
                  var currentDay = flockDefinition.StartedDay + (int) span.TotalDays;
                  dataGridViewFlocks.Rows.Add(flockDefinition.Name,
                     date.ToShortDateString(),
                     Program.Database.CheckIsFlockActive(flockDefinition.Name) ? currentDay.ToString() : "");               
            }
        }

        private void Sort() {
            switch (sortMode) {
                case SortMode.NAME:
                    // Setridim podle jmena
                    SortByName();
                    break;

                case SortMode.TIME:
                    // Setridim podle data
                    SortByTime();
                    break;
            }
        
            // Prekreslim
            Redraw();
        }

        /// <summary>
        /// Select flock with a specified index
        /// </summary>
        /// <param name="name">Flock index</param>
        public void SelectFlock(int index) {
            ControlSelection.SelectRow(dataGridViewFlocks, index);
        }

        private void SelectFirstFlock() {
            SelectFlock(0);
        }

        /// <summary>
        /// Select flock with a specified name
        /// </summary>
        /// <param name="name">Flock name</param>
        public void SelectFlock(string name) {
            SelectFlock(flockDefinitionList.GetIndex(name));
        }

        /// <summary>
        /// Return first selected flock
        /// </summary>
        /// <returns>First selected flock</returns>
        public FlockDefinition GetSelectedFlock() {
            if (dataGridViewFlocks.SelectedRows.Count == 0) {
                return null;      // Tabulka je prazdna
            }
            
            var index = dataGridViewFlocks.SelectedRows[0].Index;
            if (index < 0) {
                return null;
            }

            return flockDefinitionList.List[index];
        }

        /// <summary>
        /// Return all selected flocks
        /// </summary>
        /// <returns>FlockDefinitionList instance</returns>
        public FlockDefinitionList GetSelectedFlocks() {
            if (dataGridViewFlocks.SelectedRows.Count == 0) {
                return null;      // Tabulka je prazdna
            }

            var selectedFlockDefinitionList = new FlockDefinitionList();
            foreach (DataGridViewRow row in dataGridViewFlocks.SelectedRows) {
                selectedFlockDefinitionList.Add(flockDefinitionList.List[row.Index]);
            }

            return selectedFlockDefinitionList;
        }

        public bool AddFlock(FlockDefinition flockDefinition) {
            // Pridam do seznamu
            if (!flockDefinitionList.Add(flockDefinition)) {
                return false;
            }

            // Pretridim a prekreslim
            Sort();

            // Vyberu prave zalozene hejno v seznamu
            SelectFlock(flockDefinition.Name);

            return true;
        }

        public void AddFlocks(List<FlockDefinition> list) {
            // Pridam do seznamu
            foreach (var flockDefinition in list) {
                flockDefinitionList.Add(flockDefinition);
            }

            // Pretridim a prekreslim
            Sort();

            // Vyberu prvni pridane hejno
            SelectFlock(list[0].Name);
        }

        public void RemoveSelectedFlocks() {
            if (dataGridViewFlocks.SelectedRows.Count == 0) {
                return;         // Tabulka je prazdna
            }

            foreach (DataGridViewRow row in dataGridViewFlocks.SelectedRows) {
                flockDefinitionList.Delete(row.Index);
            }

            // Pretridim a prekreslim
            Sort();
        }

        public void RemoveAllFlocks() {
            flockDefinitionList.List.Clear();

            // Pretridim a prekreslim
            Sort();
        }

        public void ReplaceSelectedFlock(FlockDefinition newFlockDefinition) {
            if (dataGridViewFlocks.SelectedRows.Count == 0) {
                return;         // Tabulka je prazdna
            }
            
            var index = dataGridViewFlocks.SelectedRows[0].Index;
            if (index < 0) {
                return;
            }

            flockDefinitionList.Delete(index);
            AddFlock(newFlockDefinition);
        }
        
        public bool RenameSelectedFlock(string newName) {
            if (dataGridViewFlocks.SelectedRows.Count == 0) {
                return false;         // Tabulka je prazdna
            }
            
            var index = dataGridViewFlocks.SelectedRows[0].Index;
            if (index < 0) {
                return false;
            }


            if (!flockDefinitionList.Rename(index, newName)) {
                return false;
            }
            
            // Pretridim a prekreslim
            Sort();

            // Vyberu prave prejmenovane hejno v seznamu
            SelectFlock(newName);

            return true;
        }

        private void dataGridViewFlocks_CellClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex >= 0) {
                return;     // Kliknul na bunku, ne na hlavicku
            }

            switch (e.ColumnIndex) {
                case 0:
                    // Setridim podle jmena
                    if (sortMode == SortMode.NAME) {
                        return;     // Tabulka uz je spravne setridena
                    }
                    sortMode = SortMode.NAME;
                    break;

                case 1:
                    // Setridim podle data
                    if (sortMode == SortMode.TIME) {
                        return;     // Tabulka uz je spravne setridena
                    }
                    sortMode = SortMode.TIME;
                    break;

                default:
                    // Podle jineho sloupce netridim
                    return;
            }
            Cursor.Current = Cursors.WaitCursor;
            try {
                Sort();
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void dataGridViewFlocks_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode != Keys.Delete) {
                return;     // Klavesa Del maze
            }

            if (DeleteEvent == null) {
                return;     // Event neni zaregistrovan
            }

                // Zjistim vybrane hejno
            var flockDefinition = GetSelectedFlock();
            if (flockDefinition == null) {
                return;         // Zadne hejno neni vybrane
            }
            
            // Nahodim event
            DeleteEvent(flockDefinition);

            // Klavesu Del jsem zpracoval
            e.Handled = true;
        }

        private void dataGridViewFlocks_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex < 0) {
                return;     // Hlavicka
            }

            if (DoubleClickEvent == null) {
                return;     // Event neni zaregistrovan
            }

            // Zjistim vybrane hejno
            var flockDefinition = GetSelectedFlock();
            if (flockDefinition == null) {
                return;         // Zadne hejno neni vybrane
            }
            
            // Nahodim event
            DoubleClickEvent(flockDefinition);
        }

    }
}
