﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Bat1.Properties;
using Bat1Library;
using BatLibrary;
using DataContext;

namespace Bat1 
{
    public partial class UserControlWeighingResult : UserControl
    {
       private const int DECIMAL_POINTS = 3;

        private void SelectControl(Control control, TabControl tabControl, TabPage tabPage) {
            if (tabControl != null) {
                tabControl.SelectedTab = tabPage;
            }
            CheckValue.SelectControl(control);
        }

        private string toLow(string text)
        {
            return text.ToLower().Replace(":","");
        }

        private bool SaveInt(TextBox textBox, out int i, bool canBeEmpty, int min, int max, TabControl tabControl, TabPage tabPage, Label label) {
            i = 0;      // Pokud je textbox brazdny, hodnota je 0
            if (canBeEmpty && textBox.Text == "") {
                return true;
            }
            if (!Int32.TryParse(textBox.Text, out i)) {
                MessageBox.Show(Resources.VALUE_NOT_VALID + " : " + toLow(label.Text), Program.ApplicationName);
                SelectControl(textBox, tabControl, tabPage);
                ToolTip tt = new ToolTip();
                tt.Show(min + " - " + max, textBox, 50, -20, 7000);
                return false;
            }
            if (i < min || i > max) {
                MessageBox.Show(Resources.VALUE_NOT_VALID + " : " + toLow(label.Text), Program.ApplicationName);
                SelectControl(textBox, tabControl, tabPage);
                ToolTip tt = new ToolTip();
                tt.Show(min+" - "+max, textBox, 50, -20, 7000);
                return false;
            }
            return true;
        }

        private bool SaveDouble(TextBox textBox, out double d, bool canBeEmpty, double min, double max, TabControl tabControl, TabPage tabPage, Label label) {
            d = 0;      // Pokud je textbox brazdny, hodnota je 0
            if (canBeEmpty && textBox.Text == "") {
                return true;
            }
            if (!Double.TryParse(textBox.Text, out d)) {
                MessageBox.Show(Resources.VALUE_NOT_VALID + " : " + toLow(label.Text), Program.ApplicationName);
                tabControl.SelectedTab = tabPage;
                textBox.Focus();
                ToolTip tt = new ToolTip();
                tt.Show(min + " - " + max, textBox, 50, -20, 7000);
                return false;
            }
            if (d < min || d > max) {
                MessageBox.Show(Resources.VALUE_NOT_VALID + " : " + toLow(label.Text), Program.ApplicationName);
                tabControl.SelectedTab = tabPage;
                textBox.Focus();
                ToolTip tt = new ToolTip();
                tt.Show(min + " - " + max, textBox, 50, -20, 7000);
                return false;
            }
            return true;
        }

       public void SetReadOnly()
       {
          var textBoxes = new List<TextBox>
          {
             textBoxCount,
             textBoxAverage,
             textBoxSigma,
             textBoxCv,
             textBoxUniformity,
             textBoxGain
          };

          foreach (var textbox in textBoxes)
          {
             textbox.ReadOnly = true;
             //textbox.BackColor = System.Drawing.SystemColors.Window;
          }
       }

        public void ShowGain(bool show)
        {
            label1.Visible = show;
            textBoxGain.Visible = show;
            textBoxGain.BringToFront();
        }

        public bool GetResult(Units units, Flag flag, out StatisticResult result, TabControl tabControl, TabPage tabPage) {
            result = null;
            
            // Zkontroluju zadane hodnoty - pocet a prumer musi zadat, ostatni nemusi
            int count;
            if (!SaveInt(textBoxCount, out count, false, 1, 99999, tabControl, tabPage, label5)) {
                return false;
            }

            // Maximalni a minimalni hmotnost podle jednotek
            var minWeight = ConvertWeight.MinWeight(units);
            var maxWeight = ConvertWeight.MaxWeight(units, WeighingCapacity.EXTENDED);     // Povolim 50kg

            double average;
            if (!SaveDouble(textBoxAverage, out average, false, minWeight, maxWeight, tabControl, tabPage, label10)) {
                return false;
            }

            double sigma;
            if (!SaveDouble(textBoxSigma, out sigma, true, 0, maxWeight,tabControl, tabPage, label11)) {
                return false;
            }

            double cv;
            if (!SaveDouble(textBoxCv, out cv, true, 0, 99.9, tabControl, tabPage,label12)) {
                return false;
            }

            double uniformity;
            if (!SaveDouble(textBoxUniformity, out uniformity, true, 0, 100.0, tabControl, tabPage, label15)) {
                return false;
            }

            double gain;
            if (!SaveDouble(textBoxGain, out gain, true, 0, maxWeight, tabControl, tabPage, label1))
            {
                return false;
            }

            result = new StatisticResult(flag, count, (float)average, (float)sigma, (float)cv, (float)uniformity, DateTime.Now, (float?)gain);
            return true;
        }

        public void SetResult(StatisticResult result, Units units, long id)
        {
            textBoxCount.Text = result.Count.ToString();

            var avg = result.Average;
            var sigma = result.Sigma;
            float? gain = null;
            if (result.Gain != null)
            {  
                gain = (float)ConvertWeight.Convert((float) result.Gain, Units.G, units);
            }


            if (id > -1)
            {
                avg = (float) ConvertWeight.Convert(result.Average, Units.G, units);
                sigma = (float) ConvertWeight.Convert(result.Sigma, Units.G, units);
                
            }
            textBoxAverage.Text = Math.Round(avg, DECIMAL_POINTS).ToString("g");

            if (result.Sigma != 0)
            {
                textBoxSigma.Text = Math.Round(sigma, DECIMAL_POINTS).ToString("g");
            }
            if (result.Cv != 0)
            {
                textBoxCv.Text = DisplayFormat.Cv(result.Cv);
            }
            if (result.Uniformity != 0)
            {
                var ratio = 1.0f;
                if (id == -1)
                {
                    // record is not saved in db yet => uniformity hasn't right format
                    ratio = 10.0f;
                }
                textBoxUniformity.Text =
                    DisplayFormat.Uniformity(StatisticResult.ViewUniformity(result.Uniformity)*ratio);
            }
            if (gain != null)
            {
                textBoxGain.Text = Math.Round((float)gain, DECIMAL_POINTS).ToString("g");
            }

        }

        public UserControlWeighingResult() {
            InitializeComponent();
        }

        private void textBoxManualCount_KeyPress(object sender, KeyPressEventArgs e) {
            if (!KeyFilter.IsInt(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void textBoxManualAverage_KeyPress(object sender, KeyPressEventArgs e) {
            if (!KeyFilter.IsFloat(e.KeyChar)) {
                e.Handled = true;
            }
        }
    }
}
