﻿namespace Bat1
{
   partial class UserControlScale
   {
      /// <summary> 
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary> 
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Component Designer generated code

      /// <summary> 
      /// Required method for Designer support - do not modify 
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScale));
         this.scaleBat1 = new System.Windows.Forms.TabPage();
         this.tabControl = new System.Windows.Forms.TabControl();
         this.scaleBat2 = new System.Windows.Forms.TabPage();
         this.tabControl.SuspendLayout();
         this.SuspendLayout();
         // 
         // scaleBat1
         // 
         resources.ApplyResources(this.scaleBat1, "scaleBat1");
         this.scaleBat1.Name = "scaleBat1";
         this.scaleBat1.UseVisualStyleBackColor = true;
         // 
         // tabControl
         // 
         this.tabControl.Controls.Add(this.scaleBat1);
         this.tabControl.Controls.Add(this.scaleBat2);
         resources.ApplyResources(this.tabControl, "tabControl");
         this.tabControl.Name = "tabControl";
         this.tabControl.SelectedIndex = 0;
         // 
         // scaleBat2
         // 
         resources.ApplyResources(this.scaleBat2, "scaleBat2");
         this.scaleBat2.Name = "scaleBat2";
         this.scaleBat2.UseVisualStyleBackColor = true;
         // 
         // UserControlScale
         // 
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.Controls.Add(this.tabControl);
         this.Name = "UserControlScale";
         this.tabControl.ResumeLayout(false);
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.TabControl tabControl;
      private System.Windows.Forms.TabPage scaleBat2;
      private System.Windows.Forms.TabPage scaleBat1;
   }
}
