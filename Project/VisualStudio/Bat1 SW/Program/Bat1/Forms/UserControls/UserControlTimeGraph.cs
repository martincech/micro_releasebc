﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Bat1.Properties;
using ZedGraph;

namespace Bat1 {
   public partial class UserControlTimeGraph : UserControl {
        private int mouseDownX = 0, mouseDownY = 0;
        private bool undoZoom;

        public ZedGraphControl Graph { get { return graph; } }

        /// <summary>
        /// Pen width for grid, tics and border
        /// </summary>
        private const float penWidth = 0.5F;          // Na monitoru se zobrazi korektne jako 1px, uzsi je jen pri tisku

        /// <summary>
        /// Grid color
        /// </summary>
        private Color gridColor = Color.FromArgb(30, Color.Black);

        public bool IsMouseInGraph = false;


        private void SetAxis(Axis axis) {
            axis.Title.FontSpec.Size   = 11;
            axis.Title.FontSpec.IsBold = false;
            axis.MajorGrid.IsVisible   = true;
            axis.MajorGrid.DashOn      = 1;              // Chci plnou caru
            axis.MajorGrid.DashOff     = 0;
            axis.MajorGrid.Color       = gridColor;
            axis.MajorGrid.PenWidth    = penWidth;
            axis.MajorGrid.IsZeroLine  = false;          // Schovam cernou linku na hodnote 0
            axis.MajorTic.IsOpposite   = false;
            axis.MajorTic.Size         = 3.1F;
            axis.MajorTic.PenWidth     = penWidth;
            axis.MajorTic.IsInside     = false;
            axis.MinorTic.IsOpposite   = false;
            axis.MinorTic.Size         = 1.3F;
            axis.MinorTic.PenWidth     = penWidth;
            axis.MinorTic.IsInside     = false;
            axis.Scale.FontSpec.Size   = 11;
        }
        
        private void CreateGraph(ZedGraphControl zedGraphControl) {
			var graphPane = zedGraphControl.GraphPane;

            // Fonty s konstantni velikosti
            zedGraphControl.GraphPane.IsFontsScaled = false;

            // Graf protahnu co nejvic do strana dolu. Horni okraj ponecham v puvodni velikosti.
            zedGraphControl.GraphPane.Margin.Bottom = 0;
            zedGraphControl.GraphPane.Margin.Left   = 0;
            zedGraphControl.GraphPane.Margin.Right  = 0;

            // Barva pozadi stejna jako barva controlu, ktery histogram vlastni
            // Nastavuju zvlast az v eventu OnLoad, zde jeste Parent neznam

            // Nazev grafu
            graphPane.Title.IsVisible = false;

            // Zobrazim legendu
            graphPane.Legend.IsVisible = true;
            graphPane.Legend.Border.IsVisible = false;
            graphPane.Legend.Fill.Type = FillType.None;
            graphPane.Legend.FontSpec.Size = 11;

            // Schovam mrizku okolo
            graphPane.Border.IsVisible = false;
            zedGraphControl.MasterPane.Border.IsVisible = false;
            graphPane.Chart.Border.Width = penWidth;

            // Osa X
            graphPane.XAxis.Title.Text = Resources.DAY;
            SetAxis(graphPane.XAxis);
            graphPane.XAxis.Scale.LabelGap = 0.2F;      // Trosku snizim mezeru (puvodne 0.3)
            graphPane.XAxis.Title.Gap = 0;              // Mezeru nadpisu zrusim uplne

            // Pokud bych chtel na ose X datum:
//            graphPane.XAxis.Scale.Format          = "g";
//            graphPane.XAxis.Type                  = AxisType.Date;
//            graphPane.XAxis.Scale.MajorStep       = 24.0;           // Krok 1 den = 24hod
    
            // Osa Y1
            SetAxis(graphPane.YAxis);
            graphPane.YAxis.Scale.LabelGap = 0.2F;      // Snizim jako u osy X, at je to stejne

            // Osa Y2
            SetAxis(graphPane.Y2Axis);
            graphPane.Y2Axis.Scale.LabelGap = 0.2F;     // Snizim jako u osy X, at je to stejne
            graphPane.Y2Axis.MajorGrid.IsVisible = false;
            graphPane.Y2Axis.IsVisible = true;

            // Pri tisku chci zoom na celou plochu papiru
            zedGraphControl.IsPrintFillPage        = true;
            zedGraphControl.IsPrintKeepAspectRatio = false;

			// Calculate the Axis Scale Ranges
			zedGraphControl.AxisChange();
		}

        public void SetForPrinting(string title) {
            // Nazev grafu
            graph.GraphPane.Title.FontSpec.Size = 13;
            graph.GraphPane.Title.Text          = title;
            if (title == "") {
                graph.GraphPane.Title.IsVisible = false;
            } else {
                graph.GraphPane.Title.IsVisible = true;
            }
            
            // Pozadi
            graph.GraphPane.Fill.Type       = FillType.None;
            graph.GraphPane.Chart.Fill.Type = FillType.None;

            // Mrizka
            graph.GraphPane.XAxis.MajorGrid.Color = Color.Silver;   // Pruhlednou barvu to tiskne teckovane
            graph.GraphPane.YAxis.MajorGrid.Color = Color.Silver;
        }

        public void SetForScreen() {
            // Pozor, hodnoty musi odpovidat hodnotam ve fci CreateGraph()

            // Nazev grafu
            graph.GraphPane.Title.Text      = "";
            graph.GraphPane.Title.IsVisible = false;

            // Pozadi
            graph.GraphPane.Fill.Type       = FillType.Solid;
            graph.GraphPane.Chart.Fill.Type = FillType.Solid;

            // Mrizka
            graph.GraphPane.XAxis.MajorGrid.Color = gridColor;
            graph.GraphPane.YAxis.MajorGrid.Color = gridColor;
        }

        /// <summary>
        /// Clear all curves
        /// </summary>
        public void ClearCurves() {
            graph.GraphPane.GraphObjList.Clear();     // Popisky zatim nepouzivam, ale co kdyby v budoucnu
            graph.GraphPane.CurveList.Clear();        // Prubehy
        }

        public void SetAxisY1(string name) {
            graph.GraphPane.YAxis.Title.Text = name;
        }
        
        public void SetAxisY2(string name) {
            graph.GraphPane.Y2Axis.Title.Text = name;
        }

        private void AddCurve(string curveName, PointPairList list, Color color, bool isY2Axis) {
			// Pridam krivku
            var curve = graph.GraphPane.AddCurve(curveName, list, color, SymbolType.Circle);

            // Osa Y
            curve.IsY2Axis = isY2Axis;

            // Prubehy na prave ose nezobrazuju v legende
            if (isY2Axis) {
                curve.Label.IsVisible = false;
            }
            
            // Parametry bodu
            curve.Symbol.Size        = 4.0F;
            curve.Symbol.Fill        = new Fill(color);
            curve.Symbol.IsAntiAlias = true;

            // Parametry cary
            curve.Line.Width       = 2.0F;
            curve.Line.IsAntiAlias = true;
            if (isY2Axis) {
                curve.Line.Style   = DashStyle.Dot;     // Carkovana cara pro pravou osu
            } else {
                curve.Line.Style   = DashStyle.Solid;   // Plna cara pro levou
            }
        }
        
        /// <summary>
        /// Add new curve
        /// </summary>
        /// <param name="curveName">Curve name in legend</param>
        /// <param name="timeDoublePointList">Point list</param>
        public void AddCurve(string curveName, List<TimeDoublePoint> timeDoublePointList, Color color, bool isY2Axis) {
            // Make up some data points from the Sine function
			var list = new PointPairList();

            foreach (var point in timeDoublePointList) {
                list.Add(point.dateTime.ToOADate(), point.value);
            }

            AddCurve(curveName, list, color, isY2Axis);
        }

        /// <summary>
        /// Add new curve
        /// </summary>
        /// <param name="curveName">Curve name in legend</param>
        /// <param name="dayDoublePointList">Point list</param>
        public void AddCurve(string curveName, List<DayDoublePoint> dayDoublePointList, Color color, bool isY2Axis) {
			var list = new PointPairList();

            foreach (var point in dayDoublePointList) {
                list.Add(point.day, point.value);
            }

            AddCurve(curveName, list, color, isY2Axis);
        }

// JESTE BLBNE PRI ZOOMOVANI
/*        private void CheckXRange() {
            // Pokud vybere hejno jen s 1 vazenim, zobrazi se rozsah osy X 0.95 az 1.05
            // Na ose X zobrazim minimalne 6 dnu
            if (graph.GraphPane.XAxis.Scale.Max - graph.GraphPane.XAxis.Scale.Min < 6) {
                graph.GraphPane.XAxis.Scale.MajorStep = 1;
            }
        }*/
        
        public void FinishAddingCurves() {
            // Prekreslim
            graph.ZoomOutAll(graph.GraphPane);  // Odstranim pripadny zoom
			graph.AxisChange();                 // Auto osy

            // Zkontroluju rozsah osy X
//            CheckXRange();

            graph.Invalidate();                 // Prekreslim pomalu
        }
        
        public UserControlTimeGraph() {
            InitializeComponent();
            CreateGraph(graph);
        }

        private void UserControlTimeGraph_Load(object sender, EventArgs e) {
            // Az pri nahrani grafu muzu nastavit barvu pozadi stejnou jako barvu controlu, ktery histogram vlastni
            // Musim delat az zde, driv Parent neznam
            var fill = new Fill(Parent.BackColor);
            graph.GraphPane.Fill       = fill;
            graph.GraphPane.Chart.Fill = fill;
        }

        private void graph_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState) {
            if (undoZoom) {
                // Misto zvetseni nastavim puvodni meritko
                sender.ZoomOutAll(sender.GraphPane);

                // Osa X i obe Y jsou automaticky
                sender.GraphPane.XAxis.Scale.MinAuto  = true;
                sender.GraphPane.XAxis.Scale.MaxAuto  = true;
                sender.GraphPane.YAxis.Scale.MinAuto  = true;
                sender.GraphPane.YAxis.Scale.MaxAuto  = true;
                sender.GraphPane.Y2Axis.Scale.MinAuto = true;
                sender.GraphPane.Y2Axis.Scale.MaxAuto = true;

                // Zkontroluju rozsah osy X
//                graph.AxisChange();                 // Auto osy
//                CheckXRange();
            }
        }

        private bool graph_MouseUpEvent(ZedGraphControl sender, MouseEventArgs e) {
            undoZoom = false;
            if (e.Button == MouseButtons.Left) {
                if (e.X < mouseDownX && e.Y < mouseDownY) {
                    // Smer doleva nahoru => udelam unzoom stejne jak jsem zvykly
                    undoZoom = true;
                }
            }
            return default(bool);
        }

        private bool graph_MouseDownEvent(ZedGraphControl sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                // Zapamatuju si souradnice stisku leveho tlacitka
                mouseDownX = e.X;
                mouseDownY = e.Y;
            }
            return default(bool);
        }


    }
}
