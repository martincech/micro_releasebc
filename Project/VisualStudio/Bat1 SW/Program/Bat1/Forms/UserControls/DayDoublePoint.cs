namespace Bat1
{
   /// <summary>
   /// One point in a time graph
   /// </summary>
   public struct DayDoublePoint {
      public int day;
      public double value;
   }
}