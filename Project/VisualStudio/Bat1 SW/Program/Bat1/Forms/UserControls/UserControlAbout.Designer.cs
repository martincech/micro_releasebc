﻿namespace Bat1 {
    partial class UserControlAbout {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlAbout));
         this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
         this.labelCompanyAddress = new System.Windows.Forms.Label();
         this.labelCompanyName = new System.Windows.Forms.Label();
         this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
         this.labelApplicationName = new System.Windows.Forms.Label();
         this.linkLabelMail = new System.Windows.Forms.LinkLabel();
         this.linkLabelWeb = new System.Windows.Forms.LinkLabel();
         this.labelVersion = new System.Windows.Forms.Label();
         this.tableLayoutPanel1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
         this.SuspendLayout();
         // 
         // tableLayoutPanel1
         // 
         resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
         this.tableLayoutPanel1.Controls.Add(this.labelCompanyAddress, 1, 2);
         this.tableLayoutPanel1.Controls.Add(this.labelCompanyName, 1, 1);
         this.tableLayoutPanel1.Controls.Add(this.pictureBoxLogo, 0, 0);
         this.tableLayoutPanel1.Controls.Add(this.labelApplicationName, 1, 0);
         this.tableLayoutPanel1.Controls.Add(this.linkLabelMail, 1, 3);
         this.tableLayoutPanel1.Controls.Add(this.linkLabelWeb, 1, 4);
         this.tableLayoutPanel1.Controls.Add(this.labelVersion, 2, 0);
         this.tableLayoutPanel1.Name = "tableLayoutPanel1";
         // 
         // labelCompanyAddress
         // 
         resources.ApplyResources(this.labelCompanyAddress, "labelCompanyAddress");
         this.tableLayoutPanel1.SetColumnSpan(this.labelCompanyAddress, 2);
         this.labelCompanyAddress.Name = "labelCompanyAddress";
         // 
         // labelCompanyName
         // 
         resources.ApplyResources(this.labelCompanyName, "labelCompanyName");
         this.tableLayoutPanel1.SetColumnSpan(this.labelCompanyName, 2);
         this.labelCompanyName.Name = "labelCompanyName";
         // 
         // pictureBoxLogo
         // 
         resources.ApplyResources(this.pictureBoxLogo, "pictureBoxLogo");
         this.pictureBoxLogo.Name = "pictureBoxLogo";
         this.tableLayoutPanel1.SetRowSpan(this.pictureBoxLogo, 5);
         this.pictureBoxLogo.TabStop = false;
         // 
         // labelApplicationName
         // 
         resources.ApplyResources(this.labelApplicationName, "labelApplicationName");
         this.labelApplicationName.Name = "labelApplicationName";
         // 
         // linkLabelMail
         // 
         resources.ApplyResources(this.linkLabelMail, "linkLabelMail");
         this.tableLayoutPanel1.SetColumnSpan(this.linkLabelMail, 2);
         this.linkLabelMail.Name = "linkLabelMail";
         this.linkLabelMail.TabStop = true;
         this.linkLabelMail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelMail_LinkClicked);
         // 
         // linkLabelWeb
         // 
         resources.ApplyResources(this.linkLabelWeb, "linkLabelWeb");
         this.tableLayoutPanel1.SetColumnSpan(this.linkLabelWeb, 2);
         this.linkLabelWeb.Name = "linkLabelWeb";
         this.linkLabelWeb.TabStop = true;
         this.linkLabelWeb.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelWeb_LinkClicked);
         // 
         // labelVersion
         // 
         resources.ApplyResources(this.labelVersion, "labelVersion");
         this.labelVersion.Name = "labelVersion";
         // 
         // UserControlAbout
         // 
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.Controls.Add(this.tableLayoutPanel1);
         this.Name = "UserControlAbout";
         this.tableLayoutPanel1.ResumeLayout(false);
         this.tableLayoutPanel1.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
         this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelCompanyAddress;
        private System.Windows.Forms.Label labelCompanyName;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Label labelApplicationName;
        private System.Windows.Forms.LinkLabel linkLabelMail;
        private System.Windows.Forms.LinkLabel linkLabelWeb;
        private System.Windows.Forms.Label labelVersion;
    }
}
