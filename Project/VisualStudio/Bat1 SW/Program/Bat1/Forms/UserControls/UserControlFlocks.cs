﻿using System;
using System.Windows.Forms;
using Desktop.Wpf.Presentation;

namespace Bat1 {
    public partial class UserControlFlocks : UserControl, IFlocksControl {
        private UserControlFlockStatistics userControlCurrent;
        private UserControlFlockStatistics userControlSelected;
        
        public UserControlFlocks() {
            InitializeComponent();

            // Vytvorim user controly
            userControlCurrent = new UserControlFlockStatistics(false);
            tabPageCurrentFlocks.Controls.Add(userControlCurrent);
            userControlCurrent.Dock = DockStyle.Fill;

            userControlSelected = new UserControlFlockStatistics(true);
            tabPageSelectedFlocks.Controls.Add(userControlSelected);
            userControlSelected.Dock = DockStyle.Fill;
        }

        private void RedrawDisplayedPage() {
            if (tabControl.SelectedTab == tabPageFlocks) {
                // Mohl pomazat rustove krivky, ktere se umazaly i v hejnech => znovu nahraju z databaze
                userControlEditFlocks.LoadFlocks();
            } else if (tabControl.SelectedTab == tabPageCurves) {
                // Neni treba delat nic, rustove krivky muze uzivatel editovat pouze zde
            } else if (tabControl.SelectedTab == tabPageCurrentFlocks) {
                userControlCurrent.RedrawCurrentFlocks();
            } else {  // tabPageSelectedFlocks
                userControlSelected.ReloadSelectedFlocks();     // Nahraju vybrana hejna znovu z databaze
                userControlSelected.RedrawSelectedFlocks();     // Prekreslim aktualni stav (mohl smazat soubory)
            }
        }
        
        public void ClearData() {
            // Default zobrazim definici hejn
            tabControl.SelectedTab = tabPageFlocks;

            RedrawDisplayedPage();
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e) {
            RedrawDisplayedPage();
        }

       #region Implementation of IView

       /// <summary>
       /// Gets or sets the data context of the view.
       /// This will be set to ViewModel of the view.
       /// </summary>
       public object DataContext { get; set; }

       #endregion
    }

   public interface IFlocksControl : IView
   {
      void ClearData();
   }
}
