﻿namespace Bat1 {
    partial class UserControlMaintenance {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlMaintenance));
         this.tabControl = new System.Windows.Forms.TabControl();
         this.tabPageBackup = new System.Windows.Forms.TabPage();
         this.userControlBackup = new Bat1.UserControlBackup();
         this.tabPageOptions = new System.Windows.Forms.TabPage();
         this.userControlOptions = new Bat1.UserControlOptions();
         this.tabPageDiagnostics = new System.Windows.Forms.TabPage();
         this.userControlDiagnostics = new Bat1.UserControlDiagnostics();
         this.tabPageAbout = new System.Windows.Forms.TabPage();
         this.userControlAbout = new Bat1.UserControlAbout();
         this.tabControl.SuspendLayout();
         this.tabPageBackup.SuspendLayout();
         this.tabPageOptions.SuspendLayout();
         this.tabPageDiagnostics.SuspendLayout();
         this.tabPageAbout.SuspendLayout();
         this.SuspendLayout();
         // 
         // tabControl
         // 
         resources.ApplyResources(this.tabControl, "tabControl");
         this.tabControl.Controls.Add(this.tabPageBackup);
         this.tabControl.Controls.Add(this.tabPageOptions);
         this.tabControl.Controls.Add(this.tabPageDiagnostics);
         this.tabControl.Controls.Add(this.tabPageAbout);
         this.tabControl.Name = "tabControl";
         this.tabControl.SelectedIndex = 0;
         // 
         // tabPageBackup
         // 
         resources.ApplyResources(this.tabPageBackup, "tabPageBackup");
         this.tabPageBackup.Controls.Add(this.userControlBackup);
         this.tabPageBackup.Name = "tabPageBackup";
         this.tabPageBackup.UseVisualStyleBackColor = true;
         // 
         // userControlBackup
         // 
         resources.ApplyResources(this.userControlBackup, "userControlBackup");
         this.userControlBackup.Name = "userControlBackup";
         // 
         // tabPageOptions
         // 
         resources.ApplyResources(this.tabPageOptions, "tabPageOptions");
         this.tabPageOptions.Controls.Add(this.userControlOptions);
         this.tabPageOptions.Name = "tabPageOptions";
         this.tabPageOptions.UseVisualStyleBackColor = true;
         // 
         // userControlOptions
         // 
         resources.ApplyResources(this.userControlOptions, "userControlOptions");
         this.userControlOptions.Name = "userControlOptions";
         // 
         // tabPageDiagnostics
         // 
         resources.ApplyResources(this.tabPageDiagnostics, "tabPageDiagnostics");
         this.tabPageDiagnostics.Controls.Add(this.userControlDiagnostics);
         this.tabPageDiagnostics.Name = "tabPageDiagnostics";
         this.tabPageDiagnostics.UseVisualStyleBackColor = true;
         // 
         // userControlDiagnostics
         // 
         resources.ApplyResources(this.userControlDiagnostics, "userControlDiagnostics");
         this.userControlDiagnostics.Name = "userControlDiagnostics";
         // 
         // tabPageAbout
         // 
         resources.ApplyResources(this.tabPageAbout, "tabPageAbout");
         this.tabPageAbout.Controls.Add(this.userControlAbout);
         this.tabPageAbout.Name = "tabPageAbout";
         this.tabPageAbout.UseVisualStyleBackColor = true;
         // 
         // userControlAbout
         // 
         resources.ApplyResources(this.userControlAbout, "userControlAbout");
         this.userControlAbout.Name = "userControlAbout";
         // 
         // UserControlMaintenance
         // 
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.Controls.Add(this.tabControl);
         this.Name = "UserControlMaintenance";
         this.tabControl.ResumeLayout(false);
         this.tabPageBackup.ResumeLayout(false);
         this.tabPageOptions.ResumeLayout(false);
         this.tabPageDiagnostics.ResumeLayout(false);
         this.tabPageAbout.ResumeLayout(false);
         this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageBackup;
        private System.Windows.Forms.TabPage tabPageOptions;
        private System.Windows.Forms.TabPage tabPageDiagnostics;
        private System.Windows.Forms.TabPage tabPageAbout;
        private UserControlAbout userControlAbout;
        private UserControlOptions userControlOptions;
        private UserControlBackup userControlBackup;
        private UserControlDiagnostics userControlDiagnostics;
    }
}
