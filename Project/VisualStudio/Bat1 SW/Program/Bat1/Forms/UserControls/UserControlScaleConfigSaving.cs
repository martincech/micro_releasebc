﻿using System;
using Bat1.Utilities;
using Bat1Library;
using BatLibrary;
using DataContext;
using Desktop.Wpf.Presentation;

namespace Bat1 {
   public partial class UserControlScaleConfigSaving : UserControlScaleConfigBase, IConfigSavingControl
   {

        public UserControlScaleConfigSaving(Bat1Scale scaleConfig, bool readOnly) {
            InitializeComponent();

            // Read-only
            this.readOnly = readOnly;
            Enabled = !readOnly;

            // Preberu config
            SetScaleConfig(scaleConfig);
        }

        private void SetUnits() {
            // Nastavim minimalni a maximalni hodnoty podle jednotek
           numericUpDownMinimumWeight.Minimum = (decimal)BatLibrary.ConvertWeight.MinWeight(scaleConfig.Units.Units);
           numericUpDownMinimumWeight.Maximum = (decimal)BatLibrary.ConvertWeight.MaxWeight(scaleConfig.Units.Units, scaleConfig.Units.WeighingCapacity);
            switch (scaleConfig.Units.Units) {
                case Units.G:
                    numericUpDownMinimumWeight.DecimalPlaces      = 0;
                    numericUpDownMinimumWeight.Increment          = 10;
                    break;
                    
                default:    // KG nebo LB
                    numericUpDownMinimumWeight.DecimalPlaces      = 3;
                    numericUpDownMinimumWeight.Increment          = (decimal)0.010;
                    break;
            }
        }

        private void RedrawUnits() {
            labelMinimumWeightUnits.Text = scaleConfig.Units.Units.ToLocalizedString();
        }

        private void SetSaving() {
            // Ustaleni jen v automatickem rezimu
            var autoMode = comboBoxMode.SelectedIndex == (int)SavingMode.AUTOMATIC;
            labelStabilizationTime.Enabled          = autoMode;
            numericUpDownStabilizationTime.Enabled  = autoMode;
            labelStabilizationTimeSec.Enabled       = autoMode;
            labelStabilizationRange.Enabled         = autoMode;
            numericUpDownStabilizationRange.Enabled = autoMode;
            labelStabilizationRangeUnits.Enabled    = autoMode;

            // Pokud vybral Manual by sex, zakazu trideni (tyto 2 rezimy se vylucuji)
            if (comboBoxMode.SelectedIndex == (int)SavingMode.MANUAL_BY_SEX) {
                comboBoxSortingMode.SelectedIndex = (int)WeightSorting.NONE;
                comboBoxSortingMode.Enabled = false;
            } else {
                comboBoxSortingMode.Enabled = true;
            }
        }

        public override void Redraw() {
            isLoading = true;
            
            try {
                // Nastavim minimalni a maximalni hodnoty podle jednotek
                SetUnits();

                comboBoxMode.SelectedIndex           = (int)scaleConfig.WeighingConfig.Saving.Mode;
                comboBoxSortingMode.SelectedIndex    = (int)scaleConfig.WeighingConfig.WeightSorting.Mode;
                numericUpDownFilter.Text             =      scaleConfig.WeighingConfig.Saving.Filter.ToString();
                numericUpDownMinimumWeight.Text      =      DisplayFormat.RoundWeight(scaleConfig.WeighingConfig.Saving.MinimumWeight, scaleConfig.Units.Units);
                numericUpDownStabilizationTime.Text  =      scaleConfig.WeighingConfig.Saving.StabilisationTime.ToString();
                numericUpDownStabilizationRange.Text =      scaleConfig.WeighingConfig.Saving.StabilisationRange.ToString("N1");
                checkBoxEnableMoreBirds.Checked      =      scaleConfig.WeighingConfig.Saving.EnableMoreBirds;
                checkBoxEnableFileSettings.Checked   =      scaleConfig.EnableFileParameters;
                RedrawUnits();

                // Povoleni zobrazeni na zaklade nastaveni
                SetSaving();
            } finally {
                isLoading = false;
            }
        }

        private void CopyToFiles() {
            // Pokud nechce parametry pro kazdy soubor zvlast, prekopiruju globalni nastaveni ukladani do kazdeho souboru
            if (!scaleConfig.EnableFileParameters) {
                foreach (var file in scaleConfig.FileList.List) {
                    file.FileConfig.WeighingConfig = scaleConfig.WeighingConfig;    // Struct kopiruju primo
                }
            }
        }

        private void ControlsToConfig() {
            if (readOnly || isLoading) {
                return;
            }

            scaleConfig.WeighingConfig.Saving.Mode = (SavingMode)comboBoxMode.SelectedIndex;
            SetSaving();
            scaleConfig.WeighingConfig.WeightSorting.Mode = (WeightSorting)comboBoxSortingMode.SelectedIndex;
            scaleConfig.WeighingConfig.Saving.Filter = (double)numericUpDownFilter.Value;
            scaleConfig.WeighingConfig.Saving.MinimumWeight = (double)numericUpDownMinimumWeight.Value;
            scaleConfig.WeighingConfig.Saving.StabilisationTime = (double)numericUpDownStabilizationTime.Value;
            scaleConfig.WeighingConfig.Saving.StabilisationRange = (double)numericUpDownStabilizationRange.Value;
            scaleConfig.WeighingConfig.Saving.EnableMoreBirds = checkBoxEnableMoreBirds.Checked;
            scaleConfig.EnableFileParameters = checkBoxEnableFileSettings.Checked;

            // Pokud pouziva globalni nastaveni, prekopiruju nastaveni ukladani do kazdeho souboru
            CopyToFiles();
        }

        private void comboBox_SelectionChangeCommitted(object sender, EventArgs e) {
            ControlsToConfig();
        }



        #region Implementation of IView

        /// <summary>
        /// Gets or sets the data context of the view.
        /// This will be set to ViewModel of the view.
        /// </summary>
        public object DataContext { get; set; }

        #endregion
   }

   public interface IConfigSavingControl : IView
   {
   }
}
