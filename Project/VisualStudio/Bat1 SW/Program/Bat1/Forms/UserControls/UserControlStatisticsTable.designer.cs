﻿namespace Bat1 {
    partial class UserControlStatisticsTable {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlStatisticsTable));
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
         this.panelBottom = new System.Windows.Forms.Panel();
         this.splitContainerBottom = new System.Windows.Forms.SplitContainer();
         this.dataGridWeighings = new Bat1.CustomControlHistogramDataGridView();
         this.ColumnDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnAverage = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnTarget = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnDifference = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnAbove = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnBelow = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnGain = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnSigma = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnCv = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnUniformity = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnSpeed = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.ColumnHistogram = new System.Windows.Forms.DataGridViewImageColumn();
         this.tabControlBottom = new System.Windows.Forms.TabControl();
         this.tabPageHistogram = new System.Windows.Forms.TabPage();
         this.userControlHistogram = new Bat1.UserControlHistogram();
         this.tabPageSamples = new System.Windows.Forms.TabPage();
         this.userControlSamples = new Bat1.UserControlSamples();
         this.flowLayoutPanelBottom = new System.Windows.Forms.FlowLayoutPanel();
         this.buttonExportSelectedWeighing = new System.Windows.Forms.Button();
         this.buttonExportAllWeighings = new System.Windows.Forms.Button();
         this.buttonExportTable = new System.Windows.Forms.Button();
         this.buttonPrintTable = new System.Windows.Forms.Button();
         this.buttonPrintSelectedWeighing = new System.Windows.Forms.Button();
         this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
         this.panelBottom.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.splitContainerBottom)).BeginInit();
         this.splitContainerBottom.Panel1.SuspendLayout();
         this.splitContainerBottom.Panel2.SuspendLayout();
         this.splitContainerBottom.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.dataGridWeighings)).BeginInit();
         this.tabControlBottom.SuspendLayout();
         this.tabPageHistogram.SuspendLayout();
         this.tabPageSamples.SuspendLayout();
         this.flowLayoutPanelBottom.SuspendLayout();
         this.SuspendLayout();
         // 
         // panelBottom
         // 
         this.panelBottom.Controls.Add(this.splitContainerBottom);
         this.panelBottom.Controls.Add(this.flowLayoutPanelBottom);
         resources.ApplyResources(this.panelBottom, "panelBottom");
         this.panelBottom.Name = "panelBottom";
         // 
         // splitContainerBottom
         // 
         resources.ApplyResources(this.splitContainerBottom, "splitContainerBottom");
         this.splitContainerBottom.Name = "splitContainerBottom";
         // 
         // splitContainerBottom.Panel1
         // 
         this.splitContainerBottom.Panel1.Controls.Add(this.dataGridWeighings);
         // 
         // splitContainerBottom.Panel2
         // 
         this.splitContainerBottom.Panel2.Controls.Add(this.tabControlBottom);
         this.splitContainerBottom.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainerBottom_SplitterMoved);
         this.splitContainerBottom.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainerBottom_Paint);
         this.splitContainerBottom.Resize += new System.EventHandler(this.splitContainerBottom_Resize);
         // 
         // dataGridWeighings
         // 
         this.dataGridWeighings.AllowUserToAddRows = false;
         this.dataGridWeighings.AllowUserToDeleteRows = false;
         this.dataGridWeighings.AllowUserToResizeRows = false;
         this.dataGridWeighings.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
         this.dataGridWeighings.BackgroundColor = System.Drawing.SystemColors.Window;
         this.dataGridWeighings.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
         dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
         dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
         dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
         dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
         dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
         dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
         dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
         this.dataGridWeighings.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
         this.dataGridWeighings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dataGridWeighings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnDateTime,
            this.ColumnDay,
            this.ColumnFile,
            this.ColumnCount,
            this.ColumnAverage,
            this.ColumnTarget,
            this.ColumnDifference,
            this.ColumnAbove,
            this.ColumnBelow,
            this.ColumnGain,
            this.ColumnSigma,
            this.ColumnCv,
            this.ColumnUniformity,
            this.ColumnSpeed,
            this.ColumnHistogram});
         resources.ApplyResources(this.dataGridWeighings, "dataGridWeighings");
         this.dataGridWeighings.MultiSelect = false;
         this.dataGridWeighings.Name = "dataGridWeighings";
         this.dataGridWeighings.ReadOnly = true;
         dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
         dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
         dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
         dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
         dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
         dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
         this.dataGridWeighings.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
         this.dataGridWeighings.RowHeadersVisible = false;
         this.dataGridWeighings.RowTemplate.Height = 18;
         this.dataGridWeighings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dataGridWeighings.SelectionChanged += new System.EventHandler(this.dataGridWeighings_SelectionChanged);
         // 
         // ColumnDateTime
         // 
         this.ColumnDateTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         resources.ApplyResources(this.ColumnDateTime, "ColumnDateTime");
         this.ColumnDateTime.Name = "ColumnDateTime";
         this.ColumnDateTime.ReadOnly = true;
         this.ColumnDateTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnDay
         // 
         this.ColumnDay.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(0, 0, 8, 0);
         this.ColumnDay.DefaultCellStyle = dataGridViewCellStyle2;
         resources.ApplyResources(this.ColumnDay, "ColumnDay");
         this.ColumnDay.Name = "ColumnDay";
         this.ColumnDay.ReadOnly = true;
         this.ColumnDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnFile
         // 
         this.ColumnFile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         resources.ApplyResources(this.ColumnFile, "ColumnFile");
         this.ColumnFile.Name = "ColumnFile";
         this.ColumnFile.ReadOnly = true;
         this.ColumnFile.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnCount
         // 
         this.ColumnCount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnCount.DefaultCellStyle = dataGridViewCellStyle3;
         resources.ApplyResources(this.ColumnCount, "ColumnCount");
         this.ColumnCount.Name = "ColumnCount";
         this.ColumnCount.ReadOnly = true;
         this.ColumnCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnAverage
         // 
         this.ColumnAverage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnAverage.DefaultCellStyle = dataGridViewCellStyle4;
         resources.ApplyResources(this.ColumnAverage, "ColumnAverage");
         this.ColumnAverage.Name = "ColumnAverage";
         this.ColumnAverage.ReadOnly = true;
         this.ColumnAverage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnTarget
         // 
         this.ColumnTarget.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnTarget.DefaultCellStyle = dataGridViewCellStyle5;
         resources.ApplyResources(this.ColumnTarget, "ColumnTarget");
         this.ColumnTarget.Name = "ColumnTarget";
         this.ColumnTarget.ReadOnly = true;
         this.ColumnTarget.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnDifference
         // 
         this.ColumnDifference.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnDifference.DefaultCellStyle = dataGridViewCellStyle6;
         resources.ApplyResources(this.ColumnDifference, "ColumnDifference");
         this.ColumnDifference.Name = "ColumnDifference";
         this.ColumnDifference.ReadOnly = true;
         this.ColumnDifference.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnAbove
         // 
         this.ColumnAbove.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnAbove.DefaultCellStyle = dataGridViewCellStyle7;
         resources.ApplyResources(this.ColumnAbove, "ColumnAbove");
         this.ColumnAbove.Name = "ColumnAbove";
         this.ColumnAbove.ReadOnly = true;
         this.ColumnAbove.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnBelow
         // 
         this.ColumnBelow.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnBelow.DefaultCellStyle = dataGridViewCellStyle8;
         resources.ApplyResources(this.ColumnBelow, "ColumnBelow");
         this.ColumnBelow.Name = "ColumnBelow";
         this.ColumnBelow.ReadOnly = true;
         this.ColumnBelow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnGain
         // 
         this.ColumnGain.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnGain.DefaultCellStyle = dataGridViewCellStyle9;
         resources.ApplyResources(this.ColumnGain, "ColumnGain");
         this.ColumnGain.Name = "ColumnGain";
         this.ColumnGain.ReadOnly = true;
         this.ColumnGain.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnSigma
         // 
         this.ColumnSigma.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnSigma.DefaultCellStyle = dataGridViewCellStyle10;
         resources.ApplyResources(this.ColumnSigma, "ColumnSigma");
         this.ColumnSigma.Name = "ColumnSigma";
         this.ColumnSigma.ReadOnly = true;
         this.ColumnSigma.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnCv
         // 
         this.ColumnCv.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnCv.DefaultCellStyle = dataGridViewCellStyle11;
         resources.ApplyResources(this.ColumnCv, "ColumnCv");
         this.ColumnCv.Name = "ColumnCv";
         this.ColumnCv.ReadOnly = true;
         this.ColumnCv.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnUniformity
         // 
         this.ColumnUniformity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnUniformity.DefaultCellStyle = dataGridViewCellStyle12;
         resources.ApplyResources(this.ColumnUniformity, "ColumnUniformity");
         this.ColumnUniformity.Name = "ColumnUniformity";
         this.ColumnUniformity.ReadOnly = true;
         this.ColumnUniformity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnSpeed
         // 
         this.ColumnSpeed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
         this.ColumnSpeed.DefaultCellStyle = dataGridViewCellStyle13;
         resources.ApplyResources(this.ColumnSpeed, "ColumnSpeed");
         this.ColumnSpeed.Name = "ColumnSpeed";
         this.ColumnSpeed.ReadOnly = true;
         this.ColumnSpeed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
         // 
         // ColumnHistogram
         // 
         this.ColumnHistogram.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         resources.ApplyResources(this.ColumnHistogram, "ColumnHistogram");
         this.ColumnHistogram.Name = "ColumnHistogram";
         this.ColumnHistogram.ReadOnly = true;
         // 
         // tabControlBottom
         // 
         this.tabControlBottom.Controls.Add(this.tabPageHistogram);
         this.tabControlBottom.Controls.Add(this.tabPageSamples);
         resources.ApplyResources(this.tabControlBottom, "tabControlBottom");
         this.tabControlBottom.Name = "tabControlBottom";
         this.tabControlBottom.SelectedIndex = 0;
         this.tabControlBottom.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControlBottom_Selected);
         // 
         // tabPageHistogram
         // 
         this.tabPageHistogram.Controls.Add(this.userControlHistogram);
         resources.ApplyResources(this.tabPageHistogram, "tabPageHistogram");
         this.tabPageHistogram.Name = "tabPageHistogram";
         this.tabPageHistogram.UseVisualStyleBackColor = true;
         // 
         // userControlHistogram
         // 
         resources.ApplyResources(this.userControlHistogram, "userControlHistogram");
         this.userControlHistogram.Name = "userControlHistogram";
         // 
         // tabPageSamples
         // 
         this.tabPageSamples.Controls.Add(this.userControlSamples);
         resources.ApplyResources(this.tabPageSamples, "tabPageSamples");
         this.tabPageSamples.Name = "tabPageSamples";
         this.tabPageSamples.UseVisualStyleBackColor = true;
         // 
         // userControlSamples
         // 
         resources.ApplyResources(this.userControlSamples, "userControlSamples");
         this.userControlSamples.Name = "userControlSamples";
         this.userControlSamples.ReadOnly = true;
         this.userControlSamples.ShowDivide = true;
         // 
         // flowLayoutPanelBottom
         // 
         resources.ApplyResources(this.flowLayoutPanelBottom, "flowLayoutPanelBottom");
         this.flowLayoutPanelBottom.Controls.Add(this.buttonExportSelectedWeighing);
         this.flowLayoutPanelBottom.Controls.Add(this.buttonExportAllWeighings);
         this.flowLayoutPanelBottom.Controls.Add(this.buttonExportTable);
         this.flowLayoutPanelBottom.Controls.Add(this.buttonPrintTable);
         this.flowLayoutPanelBottom.Controls.Add(this.buttonPrintSelectedWeighing);
         this.flowLayoutPanelBottom.Name = "flowLayoutPanelBottom";
         // 
         // buttonExportSelectedWeighing
         // 
         resources.ApplyResources(this.buttonExportSelectedWeighing, "buttonExportSelectedWeighing");
         this.buttonExportSelectedWeighing.Name = "buttonExportSelectedWeighing";
         this.buttonExportSelectedWeighing.UseVisualStyleBackColor = true;
         this.buttonExportSelectedWeighing.Click += new System.EventHandler(this.buttonExportSelectedWeighing_Click);
         // 
         // buttonExportAllWeighings
         // 
         resources.ApplyResources(this.buttonExportAllWeighings, "buttonExportAllWeighings");
         this.buttonExportAllWeighings.Name = "buttonExportAllWeighings";
         this.buttonExportAllWeighings.UseVisualStyleBackColor = true;
         this.buttonExportAllWeighings.Click += new System.EventHandler(this.buttonExportAllWeighings_Click);
         // 
         // buttonExportTable
         // 
         resources.ApplyResources(this.buttonExportTable, "buttonExportTable");
         this.buttonExportTable.Name = "buttonExportTable";
         this.buttonExportTable.UseVisualStyleBackColor = true;
         this.buttonExportTable.Click += new System.EventHandler(this.buttonExportTable_Click);
         // 
         // buttonPrintTable
         // 
         resources.ApplyResources(this.buttonPrintTable, "buttonPrintTable");
         this.buttonPrintTable.Name = "buttonPrintTable";
         this.buttonPrintTable.UseVisualStyleBackColor = true;
         this.buttonPrintTable.Click += new System.EventHandler(this.buttonPrintTable_Click);
         // 
         // buttonPrintSelectedWeighing
         // 
         resources.ApplyResources(this.buttonPrintSelectedWeighing, "buttonPrintSelectedWeighing");
         this.buttonPrintSelectedWeighing.Name = "buttonPrintSelectedWeighing";
         this.buttonPrintSelectedWeighing.UseVisualStyleBackColor = true;
         this.buttonPrintSelectedWeighing.Click += new System.EventHandler(this.buttonPrintSelectedWeighing_Click);
         // 
         // UserControlStatisticsTable
         // 
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.Controls.Add(this.panelBottom);
         this.Name = "UserControlStatisticsTable";
         this.panelBottom.ResumeLayout(false);
         this.panelBottom.PerformLayout();
         this.splitContainerBottom.Panel1.ResumeLayout(false);
         this.splitContainerBottom.Panel2.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.splitContainerBottom)).EndInit();
         this.splitContainerBottom.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.dataGridWeighings)).EndInit();
         this.tabControlBottom.ResumeLayout(false);
         this.tabPageHistogram.ResumeLayout(false);
         this.tabPageSamples.ResumeLayout(false);
         this.flowLayoutPanelBottom.ResumeLayout(false);
         this.flowLayoutPanelBottom.PerformLayout();
         this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelBottom;
        private System.Windows.Forms.SplitContainer splitContainerBottom;
        private CustomControlHistogramDataGridView dataGridWeighings;
        private System.Windows.Forms.TabControl tabControlBottom;
        private System.Windows.Forms.TabPage tabPageHistogram;
        private UserControlHistogram userControlHistogram;
        private System.Windows.Forms.TabPage tabPageSamples;
        private UserControlSamples userControlSamples;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonPrintSelectedWeighing;
        private System.Windows.Forms.Button buttonPrintTable;
        private System.Windows.Forms.Button buttonExportSelectedWeighing;
        private System.Windows.Forms.Button buttonExportAllWeighings;
        private System.Windows.Forms.Button buttonExportTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAverage;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTarget;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDifference;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAbove;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnBelow;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnGain;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSigma;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCv;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUniformity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSpeed;
        private System.Windows.Forms.DataGridViewImageColumn ColumnHistogram;
    }
}
