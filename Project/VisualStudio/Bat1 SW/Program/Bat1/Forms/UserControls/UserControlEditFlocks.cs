﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Bat1.Database;
using Bat1.Properties;
using DataContext;
using Desktop.WinForms;

namespace Bat1 {
    public partial class UserControlEditFlocks : UserControl {
        /// <summary>
        /// Sort mode of the flock list
        /// </summary>
        enum SortMode {
            /// <summary>
            /// Sort by name
            /// </summary>
            NAME,

            /// <summary>
            /// Sort by date and time of the flock start
            /// </summary>
            TIME
        }

        /// <summary>
        /// List of closed flocks
        /// </summary>
        private FlockDefinitionList closedFlockList;

        /// <summary>
        /// Sorting mode of closed flocks (default by name)
        /// </summary>
        private SortMode sortModeClosed = SortMode.NAME;

        public UserControlEditFlocks() {
            InitializeComponent();

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) {
                return;         // V designeru z databaze nenahravam
            }

            // Zaregistruju eventy
            userControlCurrentFlockList.DeleteEvent += CurrentFlocksDeleteEventHandler;
            userControlCurrentFlockList.DoubleClickEvent += CurrentFlocksDoubleClickEventHandler;

            // Nahraju vsechna hejna z databaze a zobrazim
            LoadFlocks();
        }

        /// <summary>
        /// Reload flocks from database
        /// </summary>
        public void LoadFlocks() {
            // Nahraju vsechna hejna z databaze
            var availableFlockList = Program.Database.LoadFlockList();

            // Roztridim na bezici a ukoncena hejna
            var currentFlockList = new FlockDefinitionList();
            closedFlockList  = new FlockDefinitionList();
            foreach (var flockDefinition in availableFlockList.List)
            {
               if (Program.Database.CheckIsFlockActive(flockDefinition.Name))
               {
                  currentFlockList.Add(flockDefinition);
               }
               else
               {
                  closedFlockList.Add(flockDefinition);
               }
            }

            // Zobrazim aktualni hejna
            userControlCurrentFlockList.SetData(currentFlockList);

            // Zobrazim seznam zavrenych hejn
            SortClosedFlocks();

            // Obnovim pocet hejn v seznamu
            RedrawNumberOfCurrentFlocks();
        }

        /// <summary>
        /// Clear sorting glyphs in all columns
        /// </summary>
        private void ClearSortGlyphs(DataGridView dataGridView) {
            foreach (DataGridViewColumn column in dataGridView.Columns) {
                column.HeaderCell.SortGlyphDirection = SortOrder.None;
            }
        }

        /// <summary>
        /// Sort sample list by name and redraw
        /// </summary>
        private void SortByName(FlockDefinitionList flockDefinitionList, DataGridView dataGridView) {
            // Setridim
            if (flockDefinitionList != null) {
                flockDefinitionList.SortByName();
            }

            // Prekreslim
            ClearSortGlyphs(dataGridView);
            dataGridView.Columns[0].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
        }

        /// <summary>
        /// Sort sample list by time and redraw
        /// </summary>
        private void SortByTime(FlockDefinitionList flockDefinitionList, DataGridView dataGridView) {
            // Setridim
            if (flockDefinitionList != null) {
                flockDefinitionList.SortByTime();
            }

            // Prekreslim
            ClearSortGlyphs(dataGridView);
            dataGridView.Columns[1].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
        }

        private void SortClosedFlocks() {
            switch (sortModeClosed) {
                case SortMode.NAME:
                    // Setridim podle jmena
                    SortByName(closedFlockList, dataGridViewClosedFlocks);
                    break;

                case SortMode.TIME:
                    // Setridim podle data
                    SortByTime(closedFlockList, dataGridViewClosedFlocks);
                    break;
            }
        
            // Prekreslim
            RedrawClosedFlocks();
        }

       private void SelectCurrentFlock(int index) {
            userControlCurrentFlockList.SelectFlock(index);
        }

        private void SelectClosedFlock(int index) {
            ControlSelection.SelectRow(dataGridViewClosedFlocks, index);
        }

        /// <summary>
        /// Select closed flock with a specified name
        /// </summary>
        /// <param name="name">Flock name</param>
        private void SelectClosedFlock(string name) {
            SelectClosedFlock(closedFlockList.GetIndex(name));
        }

        private void RedrawNumberOfCurrentFlocks() {
            // Pocet hejn v seznamu
            labelCurrentFlocks.Text = string.Format(Resources.FLOCKS_CURRENT + " ({0:N0}):", userControlCurrentFlockList.FlockDefinitionList.List.Count);
        }

        private void RedrawNumberOfClosedFlocks() {
            // Pocet hejn v seznamu
            labelClosedFlocks.Text = string.Format(Resources.FLOCKS_CLOSED + " ({0:N0}):", closedFlockList.List.Count);
        }
        
        private void RedrawClosedFlocks() {
            dataGridViewClosedFlocks.Rows.Clear();
            foreach (var flockDefinition in closedFlockList.List)
            {
               dataGridViewClosedFlocks.Rows.Add(flockDefinition.Name,
                                                 flockDefinition.FlockFileList[0].From.ToShortDateString(),
                                                 flockDefinition.FlockFileList[0].To.ToShortDateString());
            }

            // Pocet hejn v seznamu
            RedrawNumberOfClosedFlocks();
        }

        private bool CanSave(string flockName) {
            // Kontrola spravneho jmena
            if (!userControlCurrentFlockList.FlockDefinitionList.CheckName(flockName)) {
                MessageBox.Show(Resources.VALUE_NOT_VALID, Program.ApplicationName);
                return false;
            }

            // Zkoktroluju, zda uz hejno neexistuje - v obou seznamech
            if (userControlCurrentFlockList.FlockDefinitionList.Exists(flockName) || closedFlockList.Exists(flockName)) {
                MessageBox.Show(String.Format(Resources.FLOCK_EXISTS, flockName), Program.ApplicationName, MessageBoxButtons.OK);
                return false;
            }

            return true;
        }

        private bool EnterName(string windowTitle, string oldName, out string newName) {
            var form = new FormName(windowTitle, oldName, false);
            if (form.ShowDialog() != DialogResult.OK) {
                newName = null;
                return false;
            }
            // Zkopiruju jmeno, maximalni delka je NAME_MAX_LENGTH definovane v DatabaseTableFlocks
            if (form.EnteredName.Length <= DatabaseTableFlocks.NAME_MAX_LENGTH) {
                newName = form.EnteredName;
            } else {
                newName = form.EnteredName.Remove(DatabaseTableFlocks.NAME_MAX_LENGTH);
            }

            return true;
        }

        private FlockDefinition GetSelectedFlock(DataGridView dataGridView, FlockDefinitionList flockDefinitionList) {
            if (dataGridView.SelectedRows.Count == 0) {
                return null;      // Tabulka je prazdna
            }
            
            var index = dataGridView.SelectedRows[0].Index;
            if (index < 0) {
                return null;
            }

            return flockDefinitionList.List[index];
        }

        private FlockDefinition GetSelectedCurrentFlock() {
            return userControlCurrentFlockList.GetSelectedFlock();
        }

        private FlockDefinition GetSelectedClosedFlock() {
            return GetSelectedFlock(dataGridViewClosedFlocks, closedFlockList);
        }

        private void EditCurrentFlock() {
            var flockDefinition = GetSelectedCurrentFlock();
            if (flockDefinition == null) {
                return;         // Zadne hejno neni vybrane
            }

            var flockName = flockDefinition.Name;

            // Edituju hejno
            var form = new FormEditFlock(flockDefinition);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Preberu editovane hejno (editace pracuje s kopii)
            flockDefinition = form.FlockDefinition;

            // Updatuju hejno v databazi
            Program.Database.UpdateFlock(flockDefinition);
            
            // Updatuju hejno v seznamu hejn, setridim a prekreslim
            userControlCurrentFlockList.ReplaceSelectedFlock(flockDefinition);
        }

        private bool CopyCurrentFlock(out FlockDefinition flockDefinition) {
            flockDefinition = null;

            // Vytvorim celkovy seznam jmen
            var list = new FlockDefinitionList();
            foreach (var definition in userControlCurrentFlockList.FlockDefinitionList.List) {
                list.Add(definition);
            }
            foreach (var definition in closedFlockList.List) {
                list.Add(definition);
            }

            // Zeptam se na sablonu
            var form = new FormSelectFlockTemplate(list);
            if (form.ShowDialog() != DialogResult.OK) {
                return false;
            }

            // Vytvorim nove hejno zkopirovanim z puvodniho (kopiruju bez ID)
            flockDefinition = new FlockDefinition(form.SelectedFlockDefinition, false);
            return true;
        }

        private void DeleteCurrentFlock() {
            // Smazu vybrane hejno
            var flockDefinition = GetSelectedCurrentFlock();
            if (flockDefinition == null) {
                return;         // Zadne hejno neni vybrane
            }

            if (MessageBox.Show(String.Format(Resources.FLOCK_DELETE, flockDefinition.Name),
                                Program.ApplicationName, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;         // Nechce smazat
            }

            // Smazu stavajici hejno z databaze - driv nez ho smazu ze seznamu
            Program.Database.DeleteFlock(flockDefinition.Id);

            // Smazu vybrane hejno ze seznamu hejn, prekreslim
            userControlCurrentFlockList.RemoveSelectedFlocks();

            // Obnovim pocet hejn v seznamu
            RedrawNumberOfCurrentFlocks();
        }

        private void DeleteClosedFlock() {
            // Smazu vybrane uzavrene hejno
            var flockDefinition = GetSelectedClosedFlock();
            if (flockDefinition == null) {
                return;         // Zadne hejno neni vybrane
            }

            if (MessageBox.Show(String.Format(Resources.FLOCK_DELETE, flockDefinition.Name),
                                Program.ApplicationName, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;         // Nechce smazat
            }

            // Smazu stavajici hejno z databaze - driv nez ho smazu ze seznamu
            Program.Database.DeleteFlock(flockDefinition.Id);

            // Smazu vybrane hejno ze seznamu hejn
            var index = dataGridViewClosedFlocks.SelectedRows[0].Index;
            closedFlockList.Delete(index);
            
            // Smazu z tabulky
            dataGridViewClosedFlocks.Rows.RemoveAt(index);       
            
            // Obnovim pocet hejn v seznamu
            RedrawNumberOfClosedFlocks();
        }
        
        /// <summary>
        /// Current flocks delete event handler
        /// </summary>
        /// <param name="sender"></param>
        private void CurrentFlocksDeleteEventHandler(FlockDefinition flockDefinition) {
            DeleteCurrentFlock();
        }
        
        /// <summary>
        /// Current flock double-click event handler
        /// </summary>
        /// <param name="flockDefinition">Flock to edit</param>
        private void CurrentFlocksDoubleClickEventHandler(FlockDefinition flockDefinition) {
            EditCurrentFlock();
        }

        private void buttonNew_Click(object sender, EventArgs e) 
        {
            FlockDefinition flockDefinition = null;

            // Zeptam se, zda chce pouzit nejake starsi hejno jako sablonu
            // Ptam se pokud je jiz definovane aspon jedno hejno
            if (userControlCurrentFlockList.FlockDefinitionList.List.Count > 0 || closedFlockList.List.Count > 0) 
            {
                if (MessageBox.Show(Resources.FLOCKS_USE_TEMPLATE, buttonNew.Text, MessageBoxButtons.YesNo) == DialogResult.Yes) 
                {
                    // Chce vybrat sablonu
                    if (!CopyCurrentFlock(out flockDefinition)) 
                    {
                        return;
                    }
                }
            }

            // Zeptam se na jmeno. Pokud vybral sablonu, default nabidnu jmeno stareho hejna,
            // aby ho mohl pouze upravit
            string newName;
            var oldName = flockDefinition == null ? "" : flockDefinition.Name;
            if (!EnterName(buttonNew.Text, oldName, out newName)) 
            {
                return;
            }

            // Kontrola jmena
            if (!CanSave(newName)) 
            {
                return;
            }

            // Pokud nevybral sablonu, zalozim nove hejno
            if (flockDefinition == null) 
            {
                flockDefinition = new FlockDefinition(newName);
            } 
            else 
            {
                flockDefinition.Name = newName;     // Pouze zmenim jmeno
            }

            // Zeptam se na parametry hejna
            var form = new FormEditFlock(flockDefinition);
            if (form.ShowDialog() != DialogResult.OK) 
            {
                return;
            }

            // Preberu editovane hejno (editace pracuje s kopii)
            flockDefinition = form.FlockDefinition;

            // Pridam nove hejno do seznamu
            if (!userControlCurrentFlockList.AddFlock(flockDefinition)) 
            {
                return;             // Nemelo by selhat, jmeno jsem uz kontroloval drive
            }

            // Pridam nove hejno do databaze
            Program.Database.SaveFlock(ref flockDefinition);
            LoadFlocks();

            // Obnovim pocet hejn v seznamu
            //RedrawNumberOfCurrentFlocks();
        }

        private void buttonDeleteCurrent_Click(object sender, EventArgs e) {
            DeleteCurrentFlock();
        }

        private void buttonRename_Click(object sender, EventArgs e) {
            // Prejmenuju vybrane hejno
            var flockDefinition = GetSelectedCurrentFlock();
            if (flockDefinition == null) {
                return;         // Zadne hejno neni vybrane
            }

            var oldName = flockDefinition.Name;

            // Zeptam se na nove jmeno
            string newName;
            if (!EnterName(buttonRename.Text, oldName, out newName)) {
                return;
            }

            if (newName == oldName) {
                return;         // Jmeno nezmenil
            }

            // Kontrola jmena
            if (!CanSave(newName)) {
                return;
            }

            // Nastavim nove jmeno
            if (!userControlCurrentFlockList.RenameSelectedFlock(newName)) {
                return;
            }

            // Updatuju hejno v databazi - pozor, index hejna se po prejmenovani mohl zmenit, musim ho nacist znovu
            Program.Database.UpdateFlock(userControlCurrentFlockList.FlockDefinitionList.GetFlockDefinition(newName));
        }

        private void buttonEdit_Click(object sender, EventArgs e) {
            EditCurrentFlock();
        }

        private void buttonClose_Click(object sender, EventArgs e) {
            var flockDefinition = GetSelectedCurrentFlock();
            if (flockDefinition == null) {
                return;         // Zadne hejno neni vybrane
            }

            // Zeptam se na datum uzavreni hejna
            DateTime closedDateTime;
            using (var form = new FormCloseFlock(flockDefinition.Name, flockDefinition.Started))
            {
               if (form.ShowDialog() != DialogResult.OK)
               {
                  return;
               }
               closedDateTime = form.ClosedDateTime;
            }

            // Nastavim hejno jako uzavrene
            Program.Database.SetFlockClosed(flockDefinition.Name, closedDateTime);         

            // Updatuju hejno v databazi
            Program.Database.UpdateFlock(flockDefinition);

            // Odeberu hejno ze seznamu bezicich hejn
            userControlCurrentFlockList.RemoveSelectedFlocks();

            // Pridam hejno do seznamu uzavrenych hejn
            closedFlockList.Add(flockDefinition);

            // Obnovim zobrazeni seznamu uzavrenych hejn
            SortClosedFlocks();

            // Zachovam pozici kurzoru v tabulce bezicich hejn           
            //SelectCurrentFlock(index);

            // Vyberu nove uzavrene hejno v seznamu
            SelectClosedFlock(flockDefinition.Name);
        }

        private void buttonBack_Click(object sender, EventArgs e) {
            var flockDefinition = GetSelectedClosedFlock();
            if (flockDefinition == null) {
                return;         // Zadne hejno neni vybrane
            }

            // Zeptam se, zda chce opravdu hejno vratit mezi bezici hejna
            if (MessageBox.Show(String.Format(Resources.FLOCK_BACK_CURRENT, flockDefinition.Name), Program.ApplicationName, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }

            // Nastavim hejno jako bezici
            Program.Database.SetFlockCurrent(flockDefinition.Name);

            // Updatuju hejno v databazi
            Program.Database.UpdateFlock(flockDefinition);

            // Odeberu hejno ze seznamu uzavrenych hejn
            var index = dataGridViewClosedFlocks.SelectedRows[0].Index;
            closedFlockList.Delete(index);

            // Pridam hejno do seznamu bezicich hejn
            userControlCurrentFlockList.AddFlock(flockDefinition);

            // Obnovim zobrazeni seznamu uzavrenych hejn
            SortClosedFlocks();

            // Zachovam pozici kurzoru v tabulce zavrenych hejn
            SelectClosedFlock(index);
        }

        private void buttonDeleteClosed_Click(object sender, EventArgs e) {
            DeleteClosedFlock();
        }

        private void UserControlEditFlocks_Load(object sender, EventArgs e) {
            userControlCurrentFlockList.Focus();
        }

        private void dataGridViewClosedFlocks_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode != Keys.Delete) {
                return;     // Klavesa Del maze
            }

            // Smazu
            DeleteClosedFlock();

            // Klavesu Del jsem zpracoval
            e.Handled = true;
        }

        private void dataGridViewClosedFlocks_CellClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex >= 0) {
                return;     // Kliknul na bunku, ne na hlavicku
            }

            switch (e.ColumnIndex) {
                case 0:
                    // Setridim podle jmena
                    if (sortModeClosed == SortMode.NAME) {
                        return;     // Tabulka uz je spravne setridena
                    }
                    sortModeClosed = SortMode.NAME;
                    break;

                case 1:
                    // Setridim podle data
                    if (sortModeClosed == SortMode.TIME) {
                        return;     // Tabulka uz je spravne setridena
                    }
                    sortModeClosed = SortMode.TIME;
                    break;

                default:
                    // Podle jineho sloupce netridim
                    return;
            }
            Cursor.Current = Cursors.WaitCursor;
            try {
                SortClosedFlocks();
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }
    }
}
