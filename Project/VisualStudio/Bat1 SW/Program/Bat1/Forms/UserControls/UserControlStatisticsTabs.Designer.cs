﻿using Desktop.Wpf.Presentation;

namespace Bat1 {
    partial class UserControlStatisticsTabs : IView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlStatisticsTabs));
         this.tabControl = new System.Windows.Forms.TabControl();
         this.tabPageLastWeighings = new System.Windows.Forms.TabPage();
         this.userControlStatisticsLast = new Bat1.UserControlStatisticsTable();
         this.userControlStatisticsTopLast = new Bat1.UserControlStatisticsTop();
         this.tabPageSelectedWeighings = new System.Windows.Forms.TabPage();
         this.userControlStatisticsSelected = new Bat1.UserControlStatisticsTable();
         this.userControlStatisticsTopSelected = new Bat1.UserControlStatisticsTop();
         this.tabControl.SuspendLayout();
         this.tabPageLastWeighings.SuspendLayout();
         this.tabPageSelectedWeighings.SuspendLayout();
         this.SuspendLayout();
         // 
         // tabControl
         // 
         this.tabControl.Controls.Add(this.tabPageLastWeighings);
         this.tabControl.Controls.Add(this.tabPageSelectedWeighings);
         resources.ApplyResources(this.tabControl, "tabControl");
         this.tabControl.Name = "tabControl";
         this.tabControl.SelectedIndex = 0;
         // 
         // tabPageLastWeighings
         // 
         this.tabPageLastWeighings.Controls.Add(this.userControlStatisticsLast);
         this.tabPageLastWeighings.Controls.Add(this.userControlStatisticsTopLast);
         resources.ApplyResources(this.tabPageLastWeighings, "tabPageLastWeighings");
         this.tabPageLastWeighings.Name = "tabPageLastWeighings";
         this.tabPageLastWeighings.UseVisualStyleBackColor = true;
         // 
         // userControlStatisticsLast
         // 
         resources.ApplyResources(this.userControlStatisticsLast, "userControlStatisticsLast");
         this.userControlStatisticsLast.Name = "userControlStatisticsLast";
         // 
         // userControlStatisticsTopLast
         // 
         resources.ApplyResources(this.userControlStatisticsTopLast, "userControlStatisticsTopLast");
         this.userControlStatisticsTopLast.Name = "userControlStatisticsTopLast";
         // 
         // tabPageSelectedWeighings
         // 
         this.tabPageSelectedWeighings.Controls.Add(this.userControlStatisticsSelected);
         this.tabPageSelectedWeighings.Controls.Add(this.userControlStatisticsTopSelected);
         resources.ApplyResources(this.tabPageSelectedWeighings, "tabPageSelectedWeighings");
         this.tabPageSelectedWeighings.Name = "tabPageSelectedWeighings";
         this.tabPageSelectedWeighings.UseVisualStyleBackColor = true;
         // 
         // userControlStatisticsSelected
         // 
         resources.ApplyResources(this.userControlStatisticsSelected, "userControlStatisticsSelected");
         this.userControlStatisticsSelected.Name = "userControlStatisticsSelected";
         // 
         // userControlStatisticsTopSelected
         // 
         resources.ApplyResources(this.userControlStatisticsTopSelected, "userControlStatisticsTopSelected");
         this.userControlStatisticsTopSelected.Name = "userControlStatisticsTopSelected";
         // 
         // UserControlStatisticsTabs
         // 
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.Controls.Add(this.tabControl);
         this.Name = "UserControlStatisticsTabs";
         this.tabControl.ResumeLayout(false);
         this.tabPageLastWeighings.ResumeLayout(false);
         this.tabPageSelectedWeighings.ResumeLayout(false);
         this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageLastWeighings;
        private System.Windows.Forms.TabPage tabPageSelectedWeighings;
        private UserControlStatisticsTable userControlStatisticsLast;
        private UserControlStatisticsTop userControlStatisticsTopLast;
        private UserControlStatisticsTable userControlStatisticsSelected;
        private UserControlStatisticsTop userControlStatisticsTopSelected;
    }
}
