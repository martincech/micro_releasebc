﻿using Bat2Library.Connection.Interface.Domain;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using Bat1.Forms.UserControls;

namespace Bat1 
{
    public partial class FormScaleSetup : Form 
    {
        public FormScaleSetup() 
        {
            InitializeComponent();

            var uc = new UserControlScaleSetup(Text);           
            uc.SetScaleDefaultConfig();
            CreateHost(uc);
        }

        public FormScaleSetup(Bat2DeviceData device)
        {
           InitializeComponent();

           var uc = new UserControlScaleSetup(Text, device);
           CreateHost(uc);
        }

       private void CreateHost(UserControlScaleSetup uc)
       {
          var host = new ElementHost
          {
             Dock = DockStyle.Fill,
             Child = uc
          };
          Controls.Add(host);
       }
    }
}
