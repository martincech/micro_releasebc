﻿using System;
using System.Windows.Forms;
using Bat1.Properties;
using Bat1Library;
using DataContext;

namespace Bat1 {
    public partial class FormSelectWeighing : Form {
        /// <summary>
        /// Filtering of available weighings
        /// </summary>
        private WeighingFilter weighingFilter;

        /// <summary>
        /// Weighings selected for statistics
        /// </summary>
        public WeighingSearchInfoList  SelectedWeighingInfoList { get { return selectedWeighingInfoList; } }
        private WeighingSearchInfoList selectedWeighingInfoList;
        
        /// <summary>
        /// Load list of weighings into the list of selected weighings
        /// </summary>
        /// <param name="weighingList">Weighing list to load</param>
        public void LoadSelectedWeighings(WeighingList weighingList) {
            selectedWeighingInfoList.Clear();
            
            // Jsou zobrazena hejna
            if (weighingList == null) {
                return;
            }

            foreach (var weighing in weighingList.List) {
                if (weighing.WeighingData.Id < 0) {
                    continue;       // Zprumerovane vazeni z hejna neberu
                }
                var weighingSearchInfo = new WeighingSearchInfo();
                weighingSearchInfo.Id          = weighing.WeighingData.Id;
                weighingSearchInfo.MinDateTime = weighing.GetMinDateTime(Flag.ALL);
                weighingSearchInfo.MaxDateTime = weighing.GetMaxDateTime(Flag.ALL);
                weighingSearchInfo.FileName    = weighing.WeighingData.File.Name;
                weighingSearchInfo.ScaleName   = weighing.WeighingData.ScaleConfig.ScaleName;
                selectedWeighingInfoList.Add(weighingSearchInfo);
            }
        }
        
        public FormSelectWeighing() {
            InitializeComponent();
            selectedWeighingInfoList = new WeighingSearchInfoList();
        }

        private void ShowWeighingsInStatistics() {
            // Zobrazim seznam vazeni vybranych pro statistiku
            Cursor.Current = Cursors.WaitCursor;
            try {
                dataGridViewStatisticsWeighings.Rows.Clear();

                foreach (var weighingInfo in selectedWeighingInfoList.WeighingInfoList) {
                    dataGridViewStatisticsWeighings.Rows.Add(weighingInfo.MinDateTime.ToString("g"),
                                                             weighingInfo.FileName, weighingInfo.ScaleName);
                }
                
                // Pocet vazeni v seznamu
                labelStatistics.Text = string.Format(Resources.SELECT_WEIGHINGS_STATISTICS + " ({0:N0}):", dataGridViewStatisticsWeighings.Rows.Count);
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary>
        /// Available list changed event handler
        /// </summary>
        /// <param name="sender"></param>
        private void AvailableChangedEventHandler(object sender) {
            // Pocet vazeni v seznamu
            labelAvailable.Text = string.Format(Resources.SELECT_WEIGHINGS_AVAILABLE + " ({0:N0}):", dataGridViewAvailableWeighings.Rows.Count);
        }

        private void ShowDetails(Weighing weighing) 
        {
            if (weighing == null) 
            {
                return;         // Vazeni jsem v DB nenalezl (jde napr. o prumer z vice vazeni u hejn)
            }

           if (weighing.WeighingData.SampleList == null)
           {
              //return;         // Rucni vazeni nelze upravovat ani zobazit detaily
              var formManual = new FormManualResults(weighing, true);
              formManual.ShowDialog();
              return;
           }
         
            // Predani 12tis vzorku trva, dam presypaci hodiny
            FormEditWeighing form;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
               // Nahraju z databaze seznam souboru
               var fileList = new NameNoteUniqueList();
               foreach (var file in Program.Database.LoadFileNameNoteList())
               {
                  fileList.Add(file);
               }

               // Predam data oknu, nastvim read-only rezim
               form = new FormEditWeighing(weighing.WeighingData, fileList, weighing.WeighingData.ScaleConfig, 
                  true, false);
            }
            finally
            {
               Cursor.Current = Cursors.Default;
            }
            form.ShowDialog();       
        }

        private void ShowDetailsAvailable() {
            if (dataGridViewAvailableWeighings.SelectedRows.Count == 0) {
                return;     // Zadne vazeni neni vybrane
            }
            
            // Nactu z databaze vybrane vazeni (pokud je jich vybrano vice, vezmu prvni v poradi)
            ShowDetails(Program.Database.LoadWeighing(weighingFilter.WeighingInfoFilter.FilteredInfoList[dataGridViewAvailableWeighings.SelectedRows[0].Index].Id));
        }

        private void ShowDetailsStatistics() {
            if (dataGridViewStatisticsWeighings.SelectedRows.Count == 0) {
                return;     // Zadne vazeni neni vybrane
            }
            
            // Nactu z databaze vybrane vazeni (pokud je jich vybrano vice, vezmu prvni v poradi)
            ShowDetails(Program.Database.LoadWeighing(selectedWeighingInfoList.WeighingInfoList[dataGridViewStatisticsWeighings.SelectedRows[0].Index].Id));
        }

        private void FormSelectWeighing_Load(object sender, EventArgs e) {
            // Nahraju a zobrazim vazeni
            Cursor.Current = Cursors.WaitCursor;
            try {
                // Dostupna vazeni nahraju z DB  zobrazim
                weighingFilter = new WeighingFilter(this, dateTimePickerFrom, dateTimePickerTo, comboBoxFile, comboBoxScale, dataGridViewAvailableWeighings);

                // Zaregistruju event handler po zmene seznamu dostupnych vazeni
                weighingFilter.ChangedEvent += AvailableChangedEventHandler;

                // Nahraju seznamy (musim az pote, comam zaregistrovan event)
                weighingFilter.Load();

                // Vazeni vybrana ve statistice
                ShowWeighingsInStatistics();
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e) {
            Cursor.Current = Cursors.WaitCursor;       // Trideni trva dlouho
            try {
                // Pridam do statistiky vsechny vybrane polozky
                foreach (DataGridViewRow row in dataGridViewAvailableWeighings.SelectedRows) {
                    selectedWeighingInfoList.Add(weighingFilter.WeighingInfoFilter.FilteredInfoList[row.Index]);
                }

                // Na zaver setridim
                selectedWeighingInfoList.Sort();

                // Zobrazim
                ShowWeighingsInStatistics();
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e) {
            // Odeberu ze statistiky vsechny vybrane polozky, tridit znovu neni treba
            // Poradi v dataGridViewStatisticsWeighings.SelectedRows odpovida poradi vyberu mysi,
            // musim tedy projet vsechny polozky od konce a mazat pokud je vybrana
            for (var i = dataGridViewStatisticsWeighings.Rows.Count - 1; i >= 0; i--) {
                if (dataGridViewStatisticsWeighings.Rows[i].Selected) {
                    selectedWeighingInfoList.Delete(i);
                }
            }

            // Zobrazim
            ShowWeighingsInStatistics();
        }

        private void FormSelectWeighing_Shown(object sender, EventArgs e) {
            // Nastavim focus na seznam dostupnych vazeni
            dataGridViewAvailableWeighings.Focus();
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            if (selectedWeighingInfoList.WeighingInfoList.Count == 0) {
                // Zadne vazeni nevybral, nemuzu pokracovat
                MessageBox.Show(Resources.SELECT_WEIGHINGS_NO_WEIGHING_SELECTED, Text);
                return;
            }
            DialogResult = DialogResult.OK;
        }

        private void buttonDetailsAvailable_Click(object sender, EventArgs e) {
            ShowDetailsAvailable();
        }

        private void buttonDetailsStatistics_Click(object sender, EventArgs e) {
            ShowDetailsStatistics();
        }

        private void dataGridViewAvailableWeighings_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex < 0) {
                return;     // Hlavicka
            }
            buttonAdd_Click(null, null);
        }

        private void dataGridViewStatisticsWeighings_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex < 0) {
                return;     // Hlavicka
            }
            buttonRemove_Click(null, null);
        }

        private void buttonAddAll_Click(object sender, EventArgs e) {
            Cursor.Current = Cursors.WaitCursor;       // Trideni trva dlouho
            try {
                // Pridam do statistiky vsechny polozky
                foreach (var info in weighingFilter.WeighingInfoFilter.FilteredInfoList) {
                    selectedWeighingInfoList.Add(info);
                }

                // Na zaver setridim
                selectedWeighingInfoList.Sort();

                // Zobrazim
                ShowWeighingsInStatistics();
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonRemoveAll_Click(object sender, EventArgs e) {
            // Odeberu ze statistiky vsechny polozky
            selectedWeighingInfoList.Clear();

            // Zobrazim
            ShowWeighingsInStatistics();
        }
    }
}
