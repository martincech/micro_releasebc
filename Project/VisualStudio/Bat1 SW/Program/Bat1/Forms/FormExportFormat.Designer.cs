﻿namespace Bat1 {
    partial class FormExportFormat {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormExportFormat));
         this.radioButtonBat1 = new System.Windows.Forms.RadioButton();
         this.radioButtonCsv = new System.Windows.Forms.RadioButton();
         this.buttonOk = new System.Windows.Forms.Button();
         this.buttonCancel = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.SuspendLayout();
         // 
         // radioButtonBat1
         // 
         resources.ApplyResources(this.radioButtonBat1, "radioButtonBat1");
         this.radioButtonBat1.Name = "radioButtonBat1";
         this.radioButtonBat1.TabStop = true;
         this.radioButtonBat1.UseVisualStyleBackColor = true;
         // 
         // radioButtonCsv
         // 
         resources.ApplyResources(this.radioButtonCsv, "radioButtonCsv");
         this.radioButtonCsv.Name = "radioButtonCsv";
         this.radioButtonCsv.TabStop = true;
         this.radioButtonCsv.UseVisualStyleBackColor = true;
         // 
         // buttonOk
         // 
         resources.ApplyResources(this.buttonOk, "buttonOk");
         this.buttonOk.Name = "buttonOk";
         this.buttonOk.UseVisualStyleBackColor = true;
         this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
         // 
         // buttonCancel
         // 
         this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         resources.ApplyResources(this.buttonCancel, "buttonCancel");
         this.buttonCancel.Name = "buttonCancel";
         this.buttonCancel.UseVisualStyleBackColor = true;
         // 
         // label1
         // 
         resources.ApplyResources(this.label1, "label1");
         this.label1.Name = "label1";
         // 
         // FormExportFormat
         // 
         this.AcceptButton = this.buttonOk;
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.CancelButton = this.buttonCancel;
         this.Controls.Add(this.label1);
         this.Controls.Add(this.buttonCancel);
         this.Controls.Add(this.buttonOk);
         this.Controls.Add(this.radioButtonCsv);
         this.Controls.Add(this.radioButtonBat1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "FormExportFormat";
         this.ShowIcon = false;
         this.ShowInTaskbar = false;
         this.ResumeLayout(false);
         this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonBat1;
        private System.Windows.Forms.RadioButton radioButtonCsv;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label1;
    }
}