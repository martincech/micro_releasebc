﻿namespace Bat1 {
    partial class FormEditWeighing {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditWeighing));
         this.groupBoxNote = new System.Windows.Forms.GroupBox();
         this.textBoxNote = new System.Windows.Forms.TextBox();
         this.groupBoxTime = new System.Windows.Forms.GroupBox();
         this.label1 = new System.Windows.Forms.Label();
         this.dateTimePickerStartTime = new System.Windows.Forms.DateTimePicker();
         this.labelEndOfWeighing = new System.Windows.Forms.Label();
         this.dateTimePickerStartDate = new System.Windows.Forms.DateTimePicker();
         this.label2 = new System.Windows.Forms.Label();
         this.groupBoxFile = new System.Windows.Forms.GroupBox();
         this.label9 = new System.Windows.Forms.Label();
         this.comboBoxFileName = new System.Windows.Forms.ComboBox();
         this.label3 = new System.Windows.Forms.Label();
         this.textBoxFileNote = new System.Windows.Forms.TextBox();
         this.buttonOk = new System.Windows.Forms.Button();
         this.buttonCancel = new System.Windows.Forms.Button();
         this.groupBoxSamples = new System.Windows.Forms.GroupBox();
         this.tabControl = new System.Windows.Forms.TabControl();
         this.tabPageWeighing = new System.Windows.Forms.TabPage();
         this.tabPageConfig = new System.Windows.Forms.TabPage();
         this.groupBoxNote.SuspendLayout();
         this.groupBoxTime.SuspendLayout();
         this.groupBoxFile.SuspendLayout();
         this.tabControl.SuspendLayout();
         this.tabPageWeighing.SuspendLayout();
         this.SuspendLayout();
         // 
         // groupBoxNote
         // 
         this.groupBoxNote.Controls.Add(this.textBoxNote);
         resources.ApplyResources(this.groupBoxNote, "groupBoxNote");
         this.groupBoxNote.Name = "groupBoxNote";
         this.groupBoxNote.TabStop = false;
         // 
         // textBoxNote
         // 
         resources.ApplyResources(this.textBoxNote, "textBoxNote");
         this.textBoxNote.Name = "textBoxNote";
         // 
         // groupBoxTime
         // 
         this.groupBoxTime.Controls.Add(this.dateTimePickerStartDate);
         this.groupBoxTime.Controls.Add(this.label1);
         this.groupBoxTime.Controls.Add(this.dateTimePickerStartTime);
         this.groupBoxTime.Controls.Add(this.labelEndOfWeighing);
         this.groupBoxTime.Controls.Add(this.label2);
         resources.ApplyResources(this.groupBoxTime, "groupBoxTime");
         this.groupBoxTime.Name = "groupBoxTime";
         this.groupBoxTime.TabStop = false;
         // 
         // label1
         // 
         resources.ApplyResources(this.label1, "label1");
         this.label1.Name = "label1";
         // 
         // dateTimePickerStartTime
         // 
         resources.ApplyResources(this.dateTimePickerStartTime, "dateTimePickerStartTime");
         this.dateTimePickerStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
         this.dateTimePickerStartTime.Name = "dateTimePickerStartTime";
         this.dateTimePickerStartTime.ShowUpDown = true;
         this.dateTimePickerStartTime.ValueChanged += new System.EventHandler(this.dateTimePickerStartDate_ValueChanged);
         // 
         // labelEndOfWeighing
         // 
         resources.ApplyResources(this.labelEndOfWeighing, "labelEndOfWeighing");
         this.labelEndOfWeighing.Name = "labelEndOfWeighing";
         // 
         // dateTimePickerStartDate
         // 
         resources.ApplyResources(this.dateTimePickerStartDate, "dateTimePickerStartDate");
         this.dateTimePickerStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
         this.dateTimePickerStartDate.Name = "dateTimePickerStartDate";
         this.dateTimePickerStartDate.ValueChanged += new System.EventHandler(this.dateTimePickerStartDate_ValueChanged);
         // 
         // label2
         // 
         resources.ApplyResources(this.label2, "label2");
         this.label2.Name = "label2";
         // 
         // groupBoxFile
         // 
         this.groupBoxFile.Controls.Add(this.textBoxFileNote);
         this.groupBoxFile.Controls.Add(this.comboBoxFileName);
         this.groupBoxFile.Controls.Add(this.label9);
         this.groupBoxFile.Controls.Add(this.label3);
         resources.ApplyResources(this.groupBoxFile, "groupBoxFile");
         this.groupBoxFile.Name = "groupBoxFile";
         this.groupBoxFile.TabStop = false;
         // 
         // label9
         // 
         resources.ApplyResources(this.label9, "label9");
         this.label9.Name = "label9";
         // 
         // comboBoxFileName
         // 
         this.comboBoxFileName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
         this.comboBoxFileName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
         this.comboBoxFileName.DropDownHeight = 240;
         resources.ApplyResources(this.comboBoxFileName, "comboBoxFileName");
         this.comboBoxFileName.FormattingEnabled = true;
         this.comboBoxFileName.Name = "comboBoxFileName";
         this.comboBoxFileName.TextChanged += new System.EventHandler(this.comboBoxFileName_TextChanged);
         this.comboBoxFileName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxFileName_KeyPress);
         // 
         // label3
         // 
         resources.ApplyResources(this.label3, "label3");
         this.label3.Name = "label3";
         // 
         // textBoxFileNote
         // 
         resources.ApplyResources(this.textBoxFileNote, "textBoxFileNote");
         this.textBoxFileNote.Name = "textBoxFileNote";
         this.textBoxFileNote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxFileName_KeyPress);
         // 
         // buttonOk
         // 
         resources.ApplyResources(this.buttonOk, "buttonOk");
         this.buttonOk.Name = "buttonOk";
         this.buttonOk.UseVisualStyleBackColor = true;
         this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
         // 
         // buttonCancel
         // 
         this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
         resources.ApplyResources(this.buttonCancel, "buttonCancel");
         this.buttonCancel.Name = "buttonCancel";
         this.buttonCancel.UseVisualStyleBackColor = true;
         // 
         // groupBoxSamples
         // 
         resources.ApplyResources(this.groupBoxSamples, "groupBoxSamples");
         this.groupBoxSamples.Name = "groupBoxSamples";
         this.groupBoxSamples.TabStop = false;
         // 
         // tabControl
         // 
         this.tabControl.Controls.Add(this.tabPageWeighing);
         this.tabControl.Controls.Add(this.tabPageConfig);
         resources.ApplyResources(this.tabControl, "tabControl");
         this.tabControl.Name = "tabControl";
         this.tabControl.SelectedIndex = 0;
         // 
         // tabPageWeighing
         // 
         this.tabPageWeighing.Controls.Add(this.groupBoxFile);
         this.tabPageWeighing.Controls.Add(this.groupBoxSamples);
         this.tabPageWeighing.Controls.Add(this.groupBoxTime);
         this.tabPageWeighing.Controls.Add(this.groupBoxNote);
         resources.ApplyResources(this.tabPageWeighing, "tabPageWeighing");
         this.tabPageWeighing.Name = "tabPageWeighing";
         this.tabPageWeighing.UseVisualStyleBackColor = true;
         // 
         // tabPageConfig
         // 
         resources.ApplyResources(this.tabPageConfig, "tabPageConfig");
         this.tabPageConfig.Name = "tabPageConfig";
         this.tabPageConfig.UseVisualStyleBackColor = true;
         // 
         // FormEditWeighing
         // 
         this.AcceptButton = this.buttonOk;
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.CancelButton = this.buttonCancel;
         this.Controls.Add(this.tabControl);
         this.Controls.Add(this.buttonCancel);
         this.Controls.Add(this.buttonOk);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "FormEditWeighing";
         this.ShowIcon = false;
         this.ShowInTaskbar = false;
         this.Shown += new System.EventHandler(this.FormWeighingParameters_Shown);
         this.groupBoxNote.ResumeLayout(false);
         this.groupBoxNote.PerformLayout();
         this.groupBoxTime.ResumeLayout(false);
         this.groupBoxTime.PerformLayout();
         this.groupBoxFile.ResumeLayout(false);
         this.groupBoxFile.PerformLayout();
         this.tabControl.ResumeLayout(false);
         this.tabPageWeighing.ResumeLayout(false);
         this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxNote;
        private System.Windows.Forms.TextBox textBoxNote;
        private System.Windows.Forms.GroupBox groupBoxTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartTime;
        private System.Windows.Forms.Label labelEndOfWeighing;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxFile;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxFileName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxFileNote;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBoxSamples;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageWeighing;
        private System.Windows.Forms.TabPage tabPageConfig;
    }
}