﻿namespace Bat1
{
    partial class FormScaleFlashing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormScaleFlashing));
         this.tableLayoutPanelFlash = new System.Windows.Forms.TableLayoutPanel();
         this.progressBarFlash = new System.Windows.Forms.ProgressBar();
         this.richTextBoxMessage = new System.Windows.Forms.RichTextBox();
         this.tableLayoutPanelFlash.SuspendLayout();
         this.SuspendLayout();
         // 
         // tableLayoutPanelFlash
         // 
         resources.ApplyResources(this.tableLayoutPanelFlash, "tableLayoutPanelFlash");
         this.tableLayoutPanelFlash.Controls.Add(this.progressBarFlash, 0, 0);
         this.tableLayoutPanelFlash.Controls.Add(this.richTextBoxMessage, 1, 0);
         this.tableLayoutPanelFlash.Name = "tableLayoutPanelFlash";
         // 
         // progressBarFlash
         // 
         resources.ApplyResources(this.progressBarFlash, "progressBarFlash");
         this.progressBarFlash.MarqueeAnimationSpeed = 20;
         this.progressBarFlash.Name = "progressBarFlash";
         this.progressBarFlash.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
         // 
         // richTextBoxMessage
         // 
         resources.ApplyResources(this.richTextBoxMessage, "richTextBoxMessage");
         this.richTextBoxMessage.Name = "richTextBoxMessage";
         this.richTextBoxMessage.ReadOnly = true;
         // 
         // FormScaleFlashing
         // 
         resources.ApplyResources(this, "$this");
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ControlBox = false;
         this.Controls.Add(this.tableLayoutPanelFlash);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
         this.Name = "FormScaleFlashing";
         this.tableLayoutPanelFlash.ResumeLayout(false);
         this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelFlash;
        private System.Windows.Forms.ProgressBar progressBarFlash;
        private System.Windows.Forms.RichTextBox richTextBoxMessage;

    }
}