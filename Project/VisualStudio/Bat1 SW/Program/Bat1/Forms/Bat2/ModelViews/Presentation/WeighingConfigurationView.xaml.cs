﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat1.Forms.Bat2.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for WeighingConfigurationView.xaml
   /// </summary>
   public partial class WeighingConfigurationView : IWeighingConfigurationView
   {
      public StatisticsView Statistics { get; private set; }

      public WeighingConfigurationView()
      {
         InitializeComponent();

         Statistics = new StatisticsView();
         StatisticsPanel.Children.Add(Statistics);
      }

      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }    
   }

   public interface IWeighingConfigurationView : IView
   {
   }
}
