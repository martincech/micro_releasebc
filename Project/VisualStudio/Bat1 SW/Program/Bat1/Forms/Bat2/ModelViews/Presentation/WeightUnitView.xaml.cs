﻿using System;
using System.Windows;
using Bat1.Forms.Bat2.ModelViews.Applications;
using Desktop.Wpf.Presentation;

namespace Bat1.Forms.Bat2.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for WeightUnitView.xaml
   /// </summary>
   public partial class WeightUnitView : IWeightUnitView
   {
      private WeightUnitViewModel vm;
  
      public WeightUnitView(bool readOnly = false)
      {
         InitializeComponent();
         Layout.IsEnabled = !readOnly;
      }

      private void UpDownBase_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
      {
         vm = DataContext as WeightUnitViewModel;
         if (vm == null) return;

         int dec = vm.Decimals;
         double newDivision;
         if (!Double.TryParse(e.NewValue.ToString(), out newDivision))
         {
            return;
         }

         while (dec > 0)
         {
            newDivision *= 10;
            dec--;
         }

         divisionDoubleUpDown.ValueChanged -= UpDownBase_OnValueChanged;
         if (newDivision > vm.DivisionMax)
         {
             vm.Division = vm.DivisionMax;
             divisionDoubleUpDown.Value = (double)e.OldValue;
         }
         else if (newDivision < vm.MinDivision)
         {
             vm.Division = (short)vm.MinDivision;
             if (e.OldValue != null)
             {
                 divisionDoubleUpDown.Value = (double) e.OldValue;
             }
         }
         else
         {
             vm.Division = (short)newDivision;
         }
         divisionDoubleUpDown.ValueChanged += UpDownBase_OnValueChanged;
      }

      public void Show()
      {
         WeightUnitControl.Visibility = Visibility.Visible;         
      }

      public void Hide()
      {
         WeightUnitControl.Visibility = Visibility.Collapsed;
      }
   }

   public interface IWeightUnitView : IView
   {     
   }
}
