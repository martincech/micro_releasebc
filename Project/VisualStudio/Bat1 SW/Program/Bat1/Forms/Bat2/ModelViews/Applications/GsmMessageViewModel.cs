﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class GsmMessageViewModel : ObservableObject
   {
      #region Private fields

      private bool commandsEnabled;
      private bool commandsCheckPhoneNumber;
      private short commandsExpiration;
      private GsmEventMaskE eventMask;
      private GsmPowerModeE mode;
      private byte switchOnPeriod;
      private byte switchOnDuration;
      private ObservableCollection<TimeRange> switchOnTimes;

      private bool recalcCollection;
      private ObservableCollection<GsmEventMaskE> selectedEvents;

      #endregion

      #region Public interface

      #region Constructors

      public GsmMessageViewModel(IView view, GsmMessage msg)
      {       
         SelectedEvents = new ObservableCollection<GsmEventMaskE>();
         SelectedEvents.CollectionChanged += SelectedEvents_CollectionChanged;
         recalcCollection = true; 

         CommandsEnabled = msg.CommandsEnabled;
         CommandsCheckPhoneNumber = msg.CommandsCheckPhoneNumber;
         CommandsExpiration = msg.CommandsExpiration;       
         EventMask = msg.EventMask;        
         Mode = msg.Mode;
         SwitchOnPeriod = msg.SwitchOnPeriod;
         SwitchOnDuration = msg.SwitchOnDuration;
         SwitchOnTimes = new ObservableCollection<TimeRange>(msg.SwitchOnTimes);

         view.DataContext = this;
      }

      #endregion

      #region Properties

      public ObservableCollection<GsmEventMaskE> SelectedEvents
      {
         get { return selectedEvents; }
         private set { SetProperty(ref selectedEvents, value); }
      }

      public GsmEventMaskE EventMask
      {
         get { return eventMask; }
         set
         {
            //SetProperty( ref eventMask, value);
            eventMask = value;

            if (recalcCollection)
            {
               SelectedEvents.CollectionChanged -= SelectedEvents_CollectionChanged;
               SelectedEvents.Clear();
               foreach (GsmEventMaskE ev in typeof(GsmEventMaskE).GetEnumValues())
               {
                  if (EventMask.HasFlag(ev))
                  {
                     SelectedEvents.Add(ev);
                  }
               }
               SelectedEvents.CollectionChanged += SelectedEvents_CollectionChanged;
            }
         }
      }

      public bool CommandsEnabled { get { return commandsEnabled; } set { SetProperty(ref commandsEnabled, value); } }
      public bool CommandsCheckPhoneNumber { get { return commandsCheckPhoneNumber; } set { SetProperty(ref commandsCheckPhoneNumber, value); } }
      public short CommandsExpiration { get { return commandsExpiration; } set { SetProperty(ref commandsExpiration, value); } }
      public GsmPowerModeE Mode { get { return mode; } set { SetProperty(ref mode, value); } }
      public byte SwitchOnPeriod { get { return switchOnPeriod; } set { SetProperty(ref switchOnPeriod, value); } }
      public byte SwitchOnDuration { get { return switchOnDuration; } set { SetProperty(ref switchOnDuration, value); } }
      public ObservableCollection<TimeRange> SwitchOnTimes { get { return switchOnTimes; } set { SetProperty(ref switchOnTimes, value); } }

      #endregion 

      #endregion

      #region Private helpers

      private void SelectedEvents_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         GsmEventMaskE mask = default(GsmEventMaskE);
         foreach (GsmEventMaskE ev in SelectedEvents)
         {
            mask |= ev;
         }
         recalcCollection = false;
         EventMask = mask;
         recalcCollection = true;
      }

      #endregion
   }
}
