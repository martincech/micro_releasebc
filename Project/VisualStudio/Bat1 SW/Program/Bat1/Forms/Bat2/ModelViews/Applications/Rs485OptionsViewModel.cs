﻿using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Utilities.Observable;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class Rs485OptionsViewModel : ObservableObject
   {
      #region Private fields

      private Rs485ModeE mode;
      private bool enabled;
      private byte physicalInterfaceAddress;

      #endregion

      #region Public interface

      #region Constructors

      public Rs485OptionsViewModel(Rs485Options option)
      {
         Mode = (Rs485ModeE)option.Mode;
         Enabled = option.Enabled;
         PhysicalInterfaceAddress = option.PhysicalDeviceAddress;
      }     

      #endregion

      #region Properties

      public Rs485ModeE Mode { get { return mode; } set { SetProperty(ref mode, value); } }
      public bool Enabled { get { return enabled; } set { SetProperty(ref enabled, value); } }
      public byte PhysicalInterfaceAddress { get { return physicalInterfaceAddress; } set { SetProperty(ref physicalInterfaceAddress, value); } }      

      #endregion 

      #endregion
   }
}
