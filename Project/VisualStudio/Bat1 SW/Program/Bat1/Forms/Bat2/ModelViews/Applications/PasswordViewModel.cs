﻿using System.Windows.Input;
using Bat1.Forms.Bat2.ModelViews.Presentation;
using DataContext;
using Desktop.Wpf.Applications;
using Utilities.Observable;
using Desktop.Wpf.Presentation;

namespace Bat1.Forms.Bat2.ModelViews.Applications
{
   public class PasswordViewModel : ObservableObject
   {
      #region Private fields

      private string password;

      private Command changePasswordCommand;

      #endregion

      #region Public interface

      #region Constructors

      public PasswordViewModel(IView view, ScaleConfig device)
      {
         //Password = device.Password;
         view.DataContext = this;
      }

      #endregion

      public ICommand ChangePasswordCommand
      {
         get
         {
            if (changePasswordCommand == null)
            {
               changePasswordCommand = new RelayCommand(
                  //Execute
                  () =>
                  {
                     var passwordView = new ChangePasswordView();
                     passwordView.DataContext = this;
                     passwordView.Show();
                  },
                  // CanExecute
                  () => true
               );
            }
            return changePasswordCommand;
         }
      }

      #region Properties

      public string Password { get { return password; } set { SetProperty(ref password, value); } }
   

      #endregion 

      #endregion
   }
}
