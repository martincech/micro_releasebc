﻿using System;
using Bat2Library.Connection.Interface.Contract;

namespace Bat1.Services
{
   internal class Bat2DataReadEventsContractMessage
   {

      public Bat2DataReadEventsContractMessage(Action<IBat2DataReadEventsContract> actionToInvoke)
      {
         ActionToInvoke = actionToInvoke;
      }
      public Action<IBat2DataReadEventsContract> ActionToInvoke { get; private set; }
   }
}
