﻿using System;
using Bat2Library.Connection.Interface.Contract;

namespace Bat1.Services
{
    internal class Bat2ErrorEventsContractMessage
    {
        public Bat2ErrorEventsContractMessage(Action<IBat2ErrorEventsContract> actionToInvoke)
      {
         ActionToInvoke = actionToInvoke;
      }
      public Action<IBat2ErrorEventsContract> ActionToInvoke { get; private set; }
    }
}
