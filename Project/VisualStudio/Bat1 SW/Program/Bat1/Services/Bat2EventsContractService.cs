﻿using System;
using System.ServiceModel;
using Bat2Library.Connection.Interface.Contract;
using Bat2Library.Connection.Interface.Domain;
using Utilities.Messenger;

namespace Bat1.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    internal class Bat2EventsContractService : IBat2EventsContract
    {
        #region Implementation of IBat2ConnectionEventsContract

        /// <summary>
        /// Event is invoked when device is connected.
        /// </summary>
        /// <param name="deviceData">device data according to connected device</param>
        public void DeviceConnected(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ConnectionEventsContractMessage(e => e.DeviceConnected(deviceData))), deviceData);
        }

        /// <summary>
        /// Event is invoked when device is disconnected.
        /// </summary>
        /// <param name="deviceData">device data according to disconnected device</param>
        public void DeviceDisconnected(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ConnectionEventsContractMessage(e => e.DeviceDisconnected(deviceData))), deviceData);
        }

        #endregion

        #region Implementation of IBat2DataReadEventsContract

        /// <summary>
        /// Event is invoked when new data are readed from device.
        /// </summary>
        /// <param name="deviceData">Data readed from device</param>
        public void DataRead(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2DataReadEventsContractMessage(e => e.DataRead(deviceData))), deviceData);
        }

        #endregion

        #region Implementation of IBat2DataWriteEventsContract

        /// <summary>
        /// Event is invoked when new data are writen to device
        /// </summary>
        /// <param name="deviceData">Data writen to device</param>
        public void DataWriten(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2DataWriteEventsContractMessage(e => e.DataWriten(deviceData))), deviceData);
        }

        #endregion

        #region Implementation of IBat2ActionEventsContract

        public void WeighingStarted(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.WeighingStarted(deviceData))), deviceData);
        }

        public void WeighingPlanned(Bat2DeviceData deviceData, DateTime at)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.WeighingPlanned(deviceData, at))), deviceData);
        }

        public void WeighingStopped(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.WeighingStopped(deviceData))), deviceData);
        }

        public void WeighingSuspended(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.WeighingSuspended(deviceData))), deviceData);
        }

        public void WeighingReleased(Bat2DeviceData deviceData)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.WeighingReleased(deviceData))), deviceData);
        }

        public void TimeSet(Bat2DeviceData deviceData, DateTime time)
        {
            MessengerSend((new Bat2ActionEventsContractMessage(e => e.TimeSet(deviceData, time))), deviceData);
        }

        public void TimeGet(Bat2DeviceData deviceData, DateTime time)
        {
            MessengerSend(new Bat2ActionEventsContractMessage(e => e.TimeGet(deviceData, time)), deviceData);
        }

        #endregion

        #region Implementation of IBat2ErrorEventsContract

        public void ErrorRised(Bat2DeviceData deviceData, ErrorMessage errorMessage)
        {
            MessengerSend(new Bat2ErrorEventsContractMessage(e => e.ErrorRised(deviceData, errorMessage)), deviceData);
        }

        #endregion

        /// <summary>
        /// Send message with and without serial number token
        /// </summary>
        private void MessengerSend<T>(T message, Bat2DeviceData deviceData)
        {
            Messenger.Default.Send(message);
            Messenger.Default.Send(message, deviceData.Configuration.VersionInfo.SerialNumber);
        }


    }
}
