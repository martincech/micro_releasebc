using Bat1Library;

namespace Bat1
{
   /// <summary>
   /// Sounds configuration
   /// </summary>
   public struct SoundsConfig {
      public Tone ToneDefault;                   // Tone of the saving without limits
      public Tone ToneLight;                     // Tone of the below limit
      public Tone ToneOk;                        // Tone of the within limits
      public Tone ToneHeavy;                     // Tone of the above limits
      public Tone ToneKeyboard;                  // Tone of the keyboard click
      public bool EnableSpecial;                 // Enable special sounds
      public int  VolumeKeyboard;                // Volume of the keyboard click
      public int  VolumeSaving;                  // Volume of the saving beep
   }
}