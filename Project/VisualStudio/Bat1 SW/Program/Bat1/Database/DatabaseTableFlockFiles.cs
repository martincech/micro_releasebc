﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using Database;
using DataContext;

namespace Bat1.Database {
    public class DatabaseTableFlockFiles : DatabaseTable {

        // Tabulka s definici souboru v jednotlivych hejnech. Tabulka je pripravena na preskakovani z jednoho
        // souboru na dalsi v ramci hejna, tj. u kazdeho souboru lze definovat casove obdobi. Zatim se
        // nevyuziva, tj. vsechny soubory hejna maji ulozene stejne obdobi.

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableFlockFiles(DatabaseFactory factory)
            : base("FlockFiles", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "FlockId "      + factory.TypeLong      + " NOT NULL,"  // ID hejna, do ktereho soubor patri
                 + "FileName "     + factory.TypeNVarChar  + "(15) NOT NULL,"
                 + "FromDateTime " + factory.TypeTimestamp + " NOT NULL,"
                 + "ToDateTime "   + factory.TypeTimestamp + " NOT NULL,"
                 + "CONSTRAINT FK_P_Flocks FOREIGN KEY (FlockId) REFERENCES Flocks(FlockId)"
                 + ")");
        }

        /// <summary>
        /// Add files of new flock
        /// </summary>
        /// <param name="flockDefinition">Flock with valid ID</param>
        public void Add(FlockDefinition flockDefinition) {
           if (flockDefinition.FlockFileList.Count == 0)
           {
              return;     // Hejno neobsahuje zadne soubory
           }
           using (var command = factory.CreateCommand("INSERT INTO " + tableName + " VALUES (@FlockId, "
                                                          + "@FileName, @FromDateTime, @ToDateTime)"))
           {
              foreach (var flockFile in flockDefinition.FlockFileList)
              {
                 command.Parameters.Clear();
                 command.Parameters.Add(factory.CreateParameter("@FlockId", flockDefinition.Id));
                 command.Parameters.Add(factory.CreateParameter("@FileName", flockFile.FileName));
                 command.Parameters.Add(factory.CreateParameter("@FromDateTime", flockFile.From));
                 command.Parameters.Add(factory.CreateParameter("@ToDateTime", flockFile.To));
                 command.ExecuteNonQuery();
              }
           }
        }

        /// <summary>
        /// Delete files of a specified flock
        /// </summary>
        /// <param name="flockId">Flock Id</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long flockId) {
            using (var command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE FlockId = @FlockId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@FlockId", flockId));
                return command.ExecuteNonQuery();
            }
        }

        private FlockFile ReaderToFlockFile(DbDataReader reader)
        {
           var flockFile = new FlockFile();
           flockFile.FileName = (string)reader["FileName"];
           flockFile.From = (DateTime)reader["FromDateTime"];
           flockFile.To = (DateTime)reader["ToDateTime"];
           return flockFile;
        }
        
        /// <summary>
        /// Load flock files from database
        /// </summary>
        /// <param name="curve">Curve to load the points to</param>
        public void Load(ref FlockDefinition flockDefinition)
        {
           using (var command = factory.CreateCommand("SELECT * FROM " + tableName + " WHERE FlockId = @FlockId"))
           {
              command.Parameters.Clear();
              command.Parameters.Add(factory.CreateParameter("@FlockId", flockDefinition.Id));
              using (var reader = command.ExecuteReader())
              {
                 // Vyplnim seznam souboru v zadanem hejnu
                 flockDefinition.FlockFileList.Clear();
                 while (reader.Read())
                 {
                    flockDefinition.FlockFileList.Add(ReaderToFlockFile(reader));
                 }
              }
           }
        }

        /// <summary>
        /// Load flock list from database
        /// </summary>
        /// <param name="flockDefinitionList">Flock list to load the points to</param>
        public void LoadList(ref FlockDefinitionList flockDefinitionList)
        {
           for (var i = 0; i < flockDefinitionList.List.Count; i++)
           {
              var flockDefinition = flockDefinitionList.List[i];
              Load(ref flockDefinition);
           }
        }

        /// <summary>
        /// Load ID list of flocks that are curently being weighed
        /// </summary>
        /// <returns>List of flock IDs</returns>
        public List<long> LoadCurrentFlockIds() {
            var idList = new List<long>();
            
            // Bezici hejna maji datum Do nastaven na DateTime.MaxValue
            using (var command = factory.CreateCommand("SELECT FlockId FROM " + tableName + " "
                                                           + "WHERE FromDateTime <= @FromDateTime "
                                                           + "AND ToDateTime >= @ToDateTime "
                                                           + "GROUP BY FlockId")) {
                command.Parameters.Clear();
                var now = DateTime.Now;
                var from = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59); // Jakykoliv cas tohoto dne vyhovuje
                command.Parameters.Add(factory.CreateParameter("@FromDateTime", from));
                command.Parameters.Add(factory.CreateParameter("@ToDateTime", DateTime.MaxValue.Date));
                using (var reader = command.ExecuteReader()) {
                    while (reader.Read()) {
                        idList.Add((long)reader["FlockId"]);
                    }
                }
            }
            
            return idList;
        }

    
    }
}
