﻿using System.Data.SqlServerCe;
using System.Linq;
using DataContext;

namespace Bat1.Database
{
   public class Database : BatContext
   {
      /// <summary>
      /// Database file name including path
      /// </summary>
      public readonly string DatabaseFileName;

      public Database(string databaseFileName)
      {
         ConnectionString = GetConnectionString(databaseFileName);
         BatContext.connectionString = ConnectionString;
         DatabaseFileName = databaseFileName;
      }

      #region SQLite implementation

      #region Private

      private static string GetConnectionString(string databaseFileName)
      {
         var connectionBuilder = new SqlCeConnectionStringBuilder
         {
            DataSource = databaseFileName, 
         };

         return connectionBuilder.ConnectionString;    
      }

      #endregion

      /// <summary>
      /// Check integrity of the database file
      /// </summary>
      /// <returns>True if the database is OK</returns>
      public bool CheckIntegrity()
      {
         //using (var container = CreateContainer())
         //{
         //   try
         //   {
         //      container.Database.Connection.Open();

         //      using (var command = container.Database.Connection.CreateCommand())
         //      {
         //         command.CommandText = "PRAGMA integrity_check";
         //         try
         //         {
         //            return (string) command.ExecuteScalar() == "ok";
         //         }
         //         catch (Exception)
         //         {
         //            return false;
         //         }
         //      }
         //   }
         //   finally
         //   {
         //      container.Database.Connection.Close();
         //   }
         //}
         return true;
      }

      /// <summary>
      /// Compact database file
      /// </summary>
      public void Vacuum()
      {
         //using (var container = CreateContainer())
         //{
         //   try
         //   {
         //      container.Database.Connection.Open();

         //      using (var command = container.Database.Connection.CreateCommand())
         //      {
         //         command.CommandText = "VACUUM";
         //         try
         //         {
         //            command.ExecuteNonQuery();
         //         }
         //         catch (Exception)
         //         {
         //            return;
         //         }
         //      }
         //   }
         //   finally
         //   {
         //      container.Database.Connection.Close();
         //   }
         //}

      }

      /// <summary>
      /// Safely copy database file to another file
      /// </summary>
      /// <param name="destination">New file name including path</param>
      public void Copy(string destination)
      {
         var path = destination.Split('\\');

         if (path[path.Length - 2].Equals("Temp"))
         {
            path[path.Length - 2] = "StartupBackup";

            var source = path.Aggregate("", (current, str) => current + (str + '\\'));
            source = source.Remove(source.Length - 1);
            System.IO.File.Copy(source, destination, true);
         }

         System.IO.File.Copy(DatabaseFileName, destination, true);
      }

      #endregion

      /// <summary>
      /// Save application setup to database
      /// </summary>
      public void SaveSetup()
      {
         SaveSetup(Program.Setup);
      }
   }
}