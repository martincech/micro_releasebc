﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.IO;

namespace Bat1
{
   public class Bat2Device
   {
      #region Fields and properties

      public uint SerialNumber { get; private set; }
      public string Name { get; set; }

      /// <summary>
      /// Device's connected flag
      /// </summary>
      public bool IsConnected
      {
         get { return connectedDevices.Count > 0; }
      }

      /// <summary>
      /// Unique archive items from all devices with the same serial number.
      /// </summary>
      public List<ArchiveItem> Archive { get; private set; }
      public double HistogramStep { get; private set; }

      /// <summary>
      /// Usb scale
      /// </summary>
      private Bat2DeviceData Scale;

      private List<Bat2DeviceData> connectedDevices;
      private List<Bat2DeviceData> disconnectedDevices;
      private IBat2NewDeviceManager manager;

      #endregion

      #region Public interfaces

      #region Constructor     

      /// <summary>
      /// Add new device
      /// </summary>
      /// <param name="device">device</param>    
      public Bat2Device(Bat2DeviceData device, IBat2NewDeviceManager manager)
      {
         this.manager = manager;
         SerialNumber = device.Configuration.VersionInfo.SerialNumber;
         Name = device.Configuration.DeviceInfo.Name;
         connectedDevices = new List<Bat2DeviceData> { device };
         disconnectedDevices = new List<Bat2DeviceData>();

         Archive = device.Archive != null ? new List<ArchiveItem>(device.Archive) : new List<ArchiveItem>();
         UpdateDevice(device);
      }

      #endregion

      /// <summary>
      /// Add another device with the same serial number.
      /// </summary>
      /// <param name="device">device</param>    
      public void AddDevice(Bat2DeviceData device)
      {
         if (disconnectedDevices.Contains(device))
         {
            disconnectedDevices.Remove(device);
         }

         connectedDevices.Add(device);
         UpdateDevice(device);
      }

      public void RemoveDevice(Bat2DeviceData device)
      {
         connectedDevices.Remove(device);
         disconnectedDevices.Add(device);

         // Check if removing device is usb or flash
         if (device == Scale)
         {
            Scale = null;
         }
      }
   
      public void ReadArchive()
      {
         if (Scale == null) 
         {
            return;
         }

         AddArchive(manager.GetDataReader(Scale).LoadArchive());

         // for histogram, we need uniformity range
         var config = manager.GetDataReader(Scale).LoadConfig();
         HistogramStep = config != null ? config.WeighingConfiguration.HistogramStep : 0.0;
      }

      public void SetTime(DateTime setDateTime)
      {
         if (Scale == null)
         {  //set time to FLASH device is unnecessary
            return;
         }

         manager.GetActionCommander(Scale).TimeSet(setDateTime);
      }

      /// <summary>
      /// Set scale's name at all connected devices with the same serial number.
      /// </summary>
      /// <param name="newName">new name</param>
      public void SetScaleName(string newName)
      {       
         //TODO
         //foreach (var dev in connectedDevices)
         //{
         //   dev.Configuration.DeviceInfo.Name = newName;
         //   //manager.GetDataWriter(dev).SaveConfig(dev.Configuration);         
         //}
      }

      public Bat2DeviceData GetDevice()
      {
         if (Scale == null)
         {
            return connectedDevices.FirstOrDefault();
         }

         try
         {
            var loadedScale = new Bat2DeviceData
            {
               Configuration = manager.GetDataReader(Scale).LoadConfig() ?? Scale.Configuration,
               CorrectionCurves = manager.GetDataReader(Scale).LoadCorrectionCurves() ?? Scale.CorrectionCurves,
               GrowthCurves = manager.GetDataReader(Scale).LoadGrowthCurves() ?? Scale.GrowthCurves,
               WeighingPlans = manager.GetDataReader(Scale).LoadWeighingPlans() ?? Scale.WeighingPlans
            };
            return loadedScale;
         }
         catch (Exception)
         {
            throw new Exception("Load data from scale failed!");
         }           
      }

      #endregion

      #region Private helpers

      private void UpdateDevice(Bat2DeviceData device)
      {
         if (CheckIsUsb(device))
         {
            ReadArchive();
         }
         else
         {
            AddArchive(device.Archive);
            HistogramStep = device.Configuration.WeighingConfiguration.HistogramStep;
         }
      }

      private bool CheckIsUsb(Bat2DeviceData device)
      {
         if (device.Archive == null)
         {  // device is physic scale (USB)
            Scale = device;
            return true;
         }
         return false;
      }

      /// <summary>
      ///  Add unique archive items from device.
      /// </summary>
      /// <param name="newItems"></param>
      private void AddArchive(IEnumerable<ArchiveItem> newItems)
      {
         if (newItems == null)
         {
            return;
         }

         foreach (var a in newItems.Where(a =>
                  !Archive.Any(item =>
                     item.Average == a.Average &&
                     item.Count == a.Count &&
                     item.Sex == a.Sex &&
                     item.Sigma == a.Sigma &&
                     item.Uniformity == a.Uniformity &&
                     item.Timestamp == a.Timestamp
                  )))
         {
            Archive.Add(a);
         }
      }

      #endregion
   }
}
