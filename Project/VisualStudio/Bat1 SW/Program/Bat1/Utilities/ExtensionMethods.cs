﻿using Bat1.Properties;
using Bat1Library;
using BatLibrary;

namespace Bat1.Utilities
{
   public static class ExtensionMethods
   {

      /// <summary>
      /// Convert units to string
      /// </summary>
      /// <param name="units">Units to convers</param>
      /// <returns>String representation</returns>
      public static string ToLocalizedString(this Units units)
      {
         switch (units)
         {
            case Units.G:
               return Resources.G;

            case Units.KG:
               return Resources.KG;

            default: // Units.LB
               return Resources.LB;
         }
      }

      /// <summary>
      /// Convert flag to string
      /// </summary>
      /// <param name="flag">Flag to convert</param>
      /// <returns>String representation</returns>
      public static string ToLocalizedString(this Flag flag)
      {
         switch (flag)
         {
            case Flag.NONE: return Resources.FLAG_NONE;
            case Flag.LIGHT: return Resources.FLAG_LIGHT;
            case Flag.OK: return Resources.FLAG_OK;
            case Flag.HEAVY: return Resources.FLAG_HEAVY;
            case Flag.MALE: return Resources.FLAG_MALE;
            case Flag.FEMALE: return Resources.FLAG_FEMALE;
            default: return Resources.FLAG_ALL;
         }
      }
   }
}
