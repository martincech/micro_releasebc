﻿using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Input;
using Bat1Wpf.ModelViews.Models;
using Bat1Wpf.ModelViews.Presentation;

namespace Bat1Wpf.Utilities
{
   /// <summary>
   /// Creates Collections of <see cref="CommandObject"/> for generating buttons.
   /// </summary>
   public class CreateCommandObjectItems
   {
      #region MainView

      //main window menu items
      public ObservableCollection<CommandObject> CreateMenuItems()
      {
         var menuItems = new ObservableCollection<CommandObject>
         {
            AddItem(Bat1.Properties.Resources.IconScale,
                    Bat1.Properties.Resources.SCALE,
                    Bat1.Properties.Resources.ToolTipButtonScale,
                    null,
                    new ScaleBat1View()),
            AddItem(Bat1.Properties.Resources.IconWeighings,
                    Bat1.Properties.Resources.Weighing,
                    Bat1.Properties.Resources.ToolTipButtonWeighings,
                    null,
                    new UserControl()),
            AddItem(Bat1.Properties.Resources.IconStatistics,
                    Bat1.Properties.Resources.Statistics,
                    Bat1.Properties.Resources.ToolTipButtonStatistics,
                    null,
                    new UserControl()),
            AddItem(Bat1.Properties.Resources.IconFlocks,
                    Bat1.Properties.Resources.Flocks,
                    Bat1.Properties.Resources.ToolTipButtonFlocks,
                    null,
                    new UserControl()),
            AddItem(Bat1.Properties.Resources.IconMaintenance,
                    Bat1.Properties.Resources.Maintenance,
                    Bat1.Properties.Resources.ToolTipButtonMaintenance,
                    null,
                    new UserControl()),
            AddItem(Bat1.Properties.Resources.IconScale,
                    Bat1.Properties.Resources.Synchronize,
                    Bat1.Properties.Resources.ToolTipButtonSynchronize,
                    null,
                    new Bat1.Forms.UserControls.UserControlSynchronize())
         };
         return menuItems;
      }

      #endregion

      #region ScaleBat1View

      // scale items
      public ObservableCollection<CommandObject> CreateScaleBat1ScaleItems()
      {
         var scaleItems = new ObservableCollection<CommandObject>
         {
            AddItem(null,
                    "Read data",
                    "",
                    null,
                    null),
            AddItem(null,
                    "Delete data",
                    "",
                    null,
                    null),
            AddItem(null,
                    "Set scale name",
                    "",
                    null,
                    null),
            AddItem(null,
                    "Set scale date and time",
                    "",
                    null,
                    null),
            AddItem(null,
                    "Set scale parameters",
                    "",
                    null,
                    null)
         };
         return scaleItems;
      }

      // archive items
      public ObservableCollection<CommandObject> CreateScaleBat1ArchiveItems()
      {
         var archiveItems = new ObservableCollection<CommandObject>
         {
            AddItem(null,
                    "Save all weighings",
                    "",
                    null,
                    null),
            AddItem(null,
                    "Save selected weighings",
                    "",
                    null,
                    null),
            AddItem(null,
                    "Export all weighings",
                    "",
                    null,
                    null),
            AddItem(null,
                    "Export selected weighings",
                    "",
                    null,
                    null)
         };
         return archiveItems;
      }

      // global items
      public ObservableCollection<CommandObject> CreateScaleBat1Items()
      {
         var items = new ObservableCollection<CommandObject>
         {
            AddItem(null,
                    "Edit",
                    "",
                    null,
                    null)
         };
         return items;
      }

      #endregion

      private CommandObject AddItem(System.Drawing.Bitmap picture, string text, string toolTip, ICommand command, object content)
      {
         return new CommandObject
         {
            Picture = picture,
            Text = text,
            ToolTip = toolTip,
            Command = command,
            Content = content
         };       
      }
   }
}
