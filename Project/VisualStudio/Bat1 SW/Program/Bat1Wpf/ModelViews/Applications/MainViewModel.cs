﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Bat1Wpf.ModelViews.Models;
using Desktop.Wpf.Applications;
using Utilities.Observable;

namespace Bat1Wpf.ModelViews.Applications
{
   public class MainViewModel : ObservableObject
   {
      #region Private Fields

      private object uiElement;      
      private ObservableCollection<CommandObject> commandObjects;
      private Command menuCommand;

      #endregion

      #region Public interfaces

      #region Constructor

      /// <summary>
      /// Initialize Main view model.
      /// </summary>    
      /// <param name="objects">list of objects for create menu items</param>    
      public MainViewModel(IEnumerable<CommandObject> objects)
      {      
         CommandObjects = new ObservableCollection<CommandObject>(objects);
      }

      #endregion

      public object UiElement { get { return uiElement; } set { SetProperty(ref uiElement, value); } }
     
      public ObservableCollection<CommandObject> CommandObjects
      {
         get { return commandObjects; } 
         set { SetProperty(ref commandObjects, value); }
      }

      public ICommand MenuCommand
      {
         get
         {
            if (menuCommand == null)
            {
               menuCommand = new RelayCommand<CommandObject>(
                  o =>
                  {
                     if (o.Command != null)
                     {
                        o.Command.Execute(null);
                     }                                     
                     // Change content
                     if (o.Content != null)
                     {
                        UiElement = o.Content;
                     }
                  },
                  o => true
                  );
            }
            return menuCommand;
         }
      }

      #endregion
   }
}
