﻿using System;
using DataModel;

namespace DataContext
{
   /// <summary>
   /// One file and date range defined in a flock
   /// </summary>
   public struct FlockFile
   {
      /// <summary>
      /// File name
      /// </summary>
      public string FileName;

      /// <summary>
      /// From what date
      /// </summary>
      public DateTime From;

      /// <summary>
      /// To what date
      /// </summary>
      public DateTime To;

      public long Id;

      public FlockFile(string fileName, long id, DateTime from, DateTime to)
      {
         Id = id;
         FileName = fileName;
         From = from;
         To = to;
      }

      /// <summary>
      /// Compare function for List(FlockFile).Sort()
      /// </summary>
      /// <param name="x">First parameter</param>
      /// <param name="y">Second parameter</param>
      /// <returns>1, -1 or 0</returns>
      public static int CompareByFrom(FlockFile x, FlockFile y)
      {
         var result = DateTime.Compare(x.From, y.From);
         if (result != 0)
         {
            return result; // Datumy jsou rozdilne
         }

         // Datumy jsou shodne, rozhodne jmeno souboru
         result = String.CompareOrdinal(x.FileName, y.FileName);
         return result != 0 ? result : DateTime.Compare(x.To, y.To);

         // Musi rozhodnout datum To
      }


      public static FlockFile FromDbFlockFile(SampleResult dbFile)
      {
         var flockFile = new FlockFile
         {       
            Id = dbFile.Id,
            FileName = dbFile.Name, 
            From = dbFile.SamplesMinDateTime.GetValueOrDefault(), 
            To = dbFile.SamplesMaxDateTime.GetValueOrDefault()
         };
         return flockFile;
      }

      public static FlockFile FromDbFlockFile(ManualResult dbFile)
      {
         var flockFile = new FlockFile
         {
            Id = dbFile.Id,
            FileName = dbFile.Name,
            From = dbFile.OriginDateTime,
            To = dbFile.OriginDateTime.AddHours(1)
         };
         return flockFile;
      }
   }
}