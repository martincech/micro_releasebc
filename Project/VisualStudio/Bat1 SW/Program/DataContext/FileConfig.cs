﻿using Bat1Library;

namespace DataContext
{
   /// <summary>
   /// File-specific config
   /// </summary>
   public class FileConfig
   {
      /// <summary>
      /// Weighing parameters
      /// </summary>
      public WeighingConfig WeighingConfig;

      /// <summary>
      /// Constructor
      /// </summary>
      public FileConfig()
      {
      }

      /// <summary>
      /// Create file-specific config from global config (used only with older scale)
      /// </summary>
      /// <param name="scaleConfig">Global config</param>
      public FileConfig(ScaleConfig scaleConfig)
      {
         // Struktury muzu kopirovat normalne
         WeighingConfig = scaleConfig.WeighingConfig;
      }

      /// <summary>
      /// Create file-specific config from specified config
      /// </summary>
      /// <param name="weighingConfig">Weighing config to use</param>
      public FileConfig(WeighingConfig weighingConfig)
      {
         // Struktury muzu kopirovat normalne
         WeighingConfig = weighingConfig;
      }
   }
}