namespace DataContext
{
   /// <summary>
   /// Result type
   /// </summary>
   public enum ResultType {
      NEW_SCALE,          // Vysledky stazene z nove vahy
      OLD_SCALE,          // Vysledky stazene ze stare vahy verze 6
      MANUAL              // Rucne zadane vysledky
   }
}