namespace DataContext
{
   /// <summary>
   /// Export file format
   /// </summary>
   public enum ExportFormat {
      BAT1,       // Export do formatu Bat1
      CSV         // Export do souboru CSV
   }
}