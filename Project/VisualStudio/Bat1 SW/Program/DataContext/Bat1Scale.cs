﻿using Bat1Library;
using BatLibrary;

namespace DataContext
{
   public class Bat1Scale : ScaleConfig
   {
      /// <summary>
      /// Setup of sounds
      /// </summary>
      public SoundsConfig Sounds;

      /// <summary>
      /// Setup of printer
      /// </summary>
      public PrintConfig Printer;

      /// <summary>
      /// File specific parameters (YES/NO)
      /// </summary>
      public bool EnableFileParameters;

      /// <summary>
      /// Password parameters
      /// </summary>
      public PasswordConfig PasswordConfig;

      /// <summary>
      /// Index of an active file in the FileList or -1 if no file is active
      /// </summary>
      public int ActiveFileIndex;

      public byte HwVersion;

      public Bat1Scale() : base()
      {
         PasswordConfig.Password = new byte[Const.PASSWORD_LENGTH];
         ActiveFileIndex = -1;
      }

      public Bat1Scale(Bat1Scale config)
         : base(config)
      {
         Sounds = config.Sounds;
         Printer = config.Printer;
         EnableFileParameters = config.EnableFileParameters;
         PasswordConfig.Enable = config.PasswordConfig.Enable;
         PasswordConfig.Password = new byte[Const.PASSWORD_LENGTH];
         config.PasswordConfig.Password.CopyTo(PasswordConfig.Password, 0);
         ActiveFileIndex = config.ActiveFileIndex;
      }

      public Bat1Scale(ScaleConfig config)
         : base(config)
      {
         PasswordConfig.Password = new byte[Const.PASSWORD_LENGTH];
         Printer.CommunicationSpeed = 9600;
         Printer.PaperWidth = 1;
         ActiveFileIndex = -1;
      }

      public Bat1Scale(Units units)
         : base(units)
      {
         ActiveFileIndex = -1;
      }

      /// <summary>
      /// Encode password into single integer number
      /// </summary>
      /// <returns>Encoded password</returns>
      internal static int EncodePassword(Bat1Scale config)
      {
         int encoded = 0;
         for (int i = 0; i < Const.PASSWORD_LENGTH; i++)
         {
            if (config.PasswordConfig.Enable)
            {
               encoded += config.PasswordConfig.Password[i];
            }
            else
            {
               encoded += (int)Keys.K_NULL; // Zakazane heslo = 4 x K_NULL
            }
            encoded *= 10;
         }
         return encoded / 10; // Musim odstranit posledni nasobeni
      }

      /// <summary>
      /// Decode password from an integer number
      /// </summary>
      /// <returns>Decoded password</returns>
      internal static byte[] DecodePassword(int encoded)
      {
         byte[] password = new byte[Const.PASSWORD_LENGTH];
         for (int i = Const.PASSWORD_LENGTH - 1; i >= 0; i--)
         {
            password[i] = (byte)(encoded % 10);
            encoded /= 10;
         }
         return password;
      }

      internal static Bat1Scale FromDbScaleConfig(DataModel.Bat1Scale dbConfig)
      {
         var scaleConfig = ScaleConfig.FromDbScaleConfig(dbConfig);
         var bat1Config = new Bat1Scale(scaleConfig);      

         bat1Config.Sounds.ToneDefault = (Tone)dbConfig.SoundsToneDefault;             
         bat1Config.Sounds.ToneLight = (Tone)dbConfig.SoundsToneLight;
         bat1Config.Sounds.ToneOk = (Tone)dbConfig.SoundsToneOk;
         bat1Config.Sounds.ToneHeavy = (Tone)dbConfig.SoundsToneHeavy;
         bat1Config.Sounds.ToneKeyboard = (Tone)dbConfig.SoundsToneKeyboard;
         bat1Config.Sounds.EnableSpecial = dbConfig.SoundsEnableSpecial;
         bat1Config.Sounds.VolumeKeyboard = dbConfig.SoundsVolumeKeyboard;
         bat1Config.Sounds.VolumeSaving = dbConfig.SoundsVolumeSaving;                              
         bat1Config.Printer.PaperWidth = dbConfig.PrinterPaperWidth;
         bat1Config.Printer.CommunicationFormat = (ComFormat)dbConfig.PrinterCommFormat;
         bat1Config.Printer.CommunicationSpeed = dbConfig.PrinterCommSpeed;
         bat1Config.EnableFileParameters = dbConfig.EnableFileParameters;
         bat1Config.PasswordConfig.Enable = dbConfig.EnablePassword;
         bat1Config.PasswordConfig.Password = DecodePassword(dbConfig.Password);
         bat1Config.ActiveFileIndex = dbConfig.ActiveFileIndex;
         bat1Config.Units.WeighingCapacity = (WeighingCapacity)dbConfig.WeighingCapacity;
         bat1Config.HwVersion = dbConfig.HwVersion;
         return bat1Config;
      }

      internal DataModel.Bat1Scale ToDbScaleConfig(DataModel.Bat1Scale dbConfig)
      {
         var scaleConfig = base.ToDbScaleConfig(dbConfig);
         dbConfig = (DataModel.Bat1Scale)scaleConfig;

         dbConfig.SoundsToneLight = (byte)Sounds.ToneLight;
         dbConfig.SoundsToneOk = (byte)Sounds.ToneOk;
         dbConfig.SoundsToneHeavy = (byte)Sounds.ToneHeavy;
         dbConfig.SoundsToneKeyboard = (byte)Sounds.ToneKeyboard;
         dbConfig.SoundsEnableSpecial = Sounds.EnableSpecial;
         dbConfig.SoundsVolumeKeyboard = (byte)Sounds.VolumeKeyboard;
         dbConfig.SoundsVolumeSaving = (byte)Sounds.VolumeSaving;
         dbConfig.PrinterPaperWidth = (byte)Printer.PaperWidth;
         dbConfig.PrinterCommFormat = (byte)Printer.CommunicationFormat;
         dbConfig.PrinterCommSpeed = Printer.CommunicationSpeed;
         dbConfig.EnableMoreBirds = WeighingConfig.Saving.EnableMoreBirds;
         dbConfig.EnableFileParameters = EnableFileParameters;
         dbConfig.EnablePassword = PasswordConfig.Enable;
         dbConfig.Password = EncodePassword(this);
         dbConfig.MinimumWeight = WeighingConfig.Saving.MinimumWeight;
         dbConfig.ActiveFileIndex = (short)ActiveFileIndex;
         dbConfig.WeighingCapacity = (byte)Units.WeighingCapacity;
         dbConfig.HwVersion = HwVersion;
         return dbConfig;
      }

      internal static void FromWeighing(ref DataModel.Bat1Scale dbConfig, Weighing weighing)
      {
         var con = dbConfig as DataModel.ScaleConfig;
         ScaleConfig.FromWeighing(ref con, weighing);
         dbConfig = con as DataModel.Bat1Scale;

         dbConfig.SoundsToneDefault = (byte)((Bat1Scale)weighing.WeighingData.ScaleConfig).Sounds.ToneDefault;
         dbConfig.SoundsToneLight = (byte)((Bat1Scale)weighing.WeighingData.ScaleConfig).Sounds.ToneLight;
         dbConfig.SoundsToneOk = (byte)((Bat1Scale)weighing.WeighingData.ScaleConfig).Sounds.ToneOk;
         dbConfig.SoundsToneHeavy = (byte)((Bat1Scale)weighing.WeighingData.ScaleConfig).Sounds.ToneHeavy;
         dbConfig.SoundsToneKeyboard = (byte)((Bat1Scale)weighing.WeighingData.ScaleConfig).Sounds.ToneKeyboard;
         dbConfig.SoundsEnableSpecial = ((Bat1Scale)weighing.WeighingData.ScaleConfig).Sounds.EnableSpecial;
         dbConfig.SoundsVolumeKeyboard = (byte)((Bat1Scale)weighing.WeighingData.ScaleConfig).Sounds.VolumeKeyboard;
         dbConfig.SoundsVolumeSaving = (byte)((Bat1Scale)weighing.WeighingData.ScaleConfig).Sounds.VolumeSaving;
         dbConfig.PrinterPaperWidth = (byte)((Bat1Scale)weighing.WeighingData.ScaleConfig).Printer.PaperWidth;
         dbConfig.PrinterCommFormat = (byte)((Bat1Scale)weighing.WeighingData.ScaleConfig).Printer.CommunicationFormat;
         dbConfig.PrinterCommSpeed = (byte)((Bat1Scale)weighing.WeighingData.ScaleConfig).Printer.CommunicationSpeed;
         dbConfig.EnableMoreBirds = weighing.WeighingData.ScaleConfig.WeighingConfig.Saving.EnableMoreBirds;
         dbConfig.EnableFileParameters = ((Bat1Scale)weighing.WeighingData.ScaleConfig).EnableFileParameters;
         dbConfig.EnablePassword = ((Bat1Scale)weighing.WeighingData.ScaleConfig).PasswordConfig.Enable;
         //dbConfig.Password = ((Bat1Scale)weighing.WeighingData.ScaleConfig).PasswordConfig.Password;   //TODO: save password
         dbConfig.MinimumWeight = weighing.WeighingData.ScaleConfig.WeighingConfig.Saving.MinimumWeight;
         dbConfig.ActiveFileIndex = (byte)((Bat1Scale)weighing.WeighingData.ScaleConfig).ActiveFileIndex;
         dbConfig.WeighingCapacity = (byte)weighing.WeighingData.ScaleConfig.Units.WeighingCapacity;
         dbConfig.HwVersion = ((Bat1Scale)weighing.WeighingData.ScaleConfig).HwVersion;
      }
   }
}
