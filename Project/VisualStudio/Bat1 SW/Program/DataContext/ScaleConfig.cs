﻿using System.Globalization;
using Bat1Library;
using BatLibrary;

namespace DataContext
{
   /// <summary>
   /// Global config of the scale
   /// </summary>
   public class ScaleConfig
   {
      /// <summary>
      /// Scale version
      /// </summary>
      public ScaleConfigVersion SwVersion;

      /// <summary>
      /// Scale name
      /// </summary>
      public string ScaleName;

      /// <summary>
      /// Country data
      /// </summary>
      public Country Country;

      /// <summary>
      /// Language in a universal format used in the DB
      /// </summary>
      public ScaleLanguagesInDatabase LanguageInDatabase;

      /// <summary>
      /// Date format
      /// </summary>
      public DateFormat DateFormat;

      /// <summary>
      /// First date separator
      /// </summary>
      public char DateSeparator1;

      /// <summary>
      /// Second date separator
      /// </summary>
      public char DateSeparator2;

      /// <summary>
      /// Time format
      /// </summary>
      public TimeFormat TimeFormat;

      /// <summary>
      /// Time separator
      /// </summary>
      public char TimeSeparator;

      /// <summary>
      /// DST
      /// </summary>
      public DaylightSavingMode DaylightSavingMode;

      /// <summary>
      /// Weighing units
      /// </summary>
      public UnitsConfig Units;

      /// <summary>
      /// Setup of display
      /// </summary>
      public DisplayConfig Display;

      /// <summary>
      /// Auto power off timeout [s]
      /// </summary>
      public int PowerOffTimeout;

      /// <summary>
      /// Weighing parameters
      /// </summary>
      public WeighingConfig WeighingConfig;

      /// <summary>
      /// Statistic parameters
      /// </summary>
      public StatisticConfig StatisticConfig;

      /// <summary>
      /// List of defined files
      /// </summary>
      public FileList FileList;

      /// <summary>
      /// List of defined file groups
      /// </summary>
      public FileGroupList FileGroupList;

      /// <summary>
      /// Constructor
      /// </summary>
      public ScaleConfig()
      {
         // Default config zde vytvaret nemuzu, musel bych volat Bat1Library.Dll.NewDevice(), coz je staticka
         // fce a rozdrbala by se tak vaha, kterou jsem treba zrovna nacetl a zpracovavam
         FileList = new FileList();
         FileGroupList = new FileGroupList();
         DefaultScaleConfig();
      }

      /// <summary>
      /// Copy constructor
      /// </summary>
      /// <param name="config">Source config to duplicate</param>
      public ScaleConfig(ScaleConfig config)
      {
         SwVersion = config.SwVersion;
         ScaleName = config.ScaleName;
         Country = config.Country;
         LanguageInDatabase = config.LanguageInDatabase;
         DateFormat = config.DateFormat;
         DateSeparator1 = config.DateSeparator1;
         DateSeparator2 = config.DateSeparator2;
         TimeFormat = config.TimeFormat;
         TimeSeparator = config.TimeSeparator;
         DaylightSavingMode = config.DaylightSavingMode;
         Units = config.Units;       
         Display = config.Display;       
         PowerOffTimeout = config.PowerOffTimeout;       
         WeighingConfig = config.WeighingConfig;
         StatisticConfig = config.StatisticConfig;
         FileList = new FileList(config.FileList);
         FileGroupList = new FileGroupList(config.FileGroupList);       
      }

      /// <summary>
      /// Constructor for manually entered results
      /// </summary>
      /// <param name="units">Weighing units</param>
      public ScaleConfig(Units units)
      {
         Units.Units = units;
         ScaleName = ""; // Jmeno vahy nastavim na prazdny string, jinak by zustalo null a blblo by dale.
         DefaultScaleConfig();
      }

      private void DefaultScaleConfig()
      {
         DateSeparator1 = '.';
         DateSeparator2 = '.';
         TimeSeparator = ':';
      }

      private void SetNewUnitsSavingConfig(ref SavingConfig savingConfig, Units oldUnits, Units newUnits)
      {
         savingConfig.MinimumWeight = ConvertWeight.Convert(savingConfig.MinimumWeight, oldUnits, newUnits);
      }

      private void SetNewUnitsWeighingConfig(ref WeighingConfig weighingConfig, Units oldUnits, Units newUnits,
         bool setLimits)
      {
         weighingConfig.Saving.MinimumWeight = ConvertWeight.Convert(weighingConfig.Saving.MinimumWeight, oldUnits,
            newUnits);
         if (setLimits)
         {
            weighingConfig.WeightSorting.HighLimit = ConvertWeight.Convert(weighingConfig.WeightSorting.HighLimit,
               oldUnits, newUnits);
            weighingConfig.WeightSorting.LowLimit = ConvertWeight.Convert(weighingConfig.WeightSorting.LowLimit,
               oldUnits, newUnits);
         }
      }

      /// <summary>
      /// Convert all weight values in config to new units
      /// </summary>
      /// <param name="newUnits">New units</param>
      public void SetNewUnits(Units newUnits)
      {
         if (newUnits == Units.Units)
         {
            return;
         }

         // Prepoctu vsechny hmotnosti v configu

         // U globalniho configu ponecham meze na default hodnote 1.000 a 2.000 nehlede na jednotky.
         // Meze tedy neprepocitavam, pouze posouvam desetinnou carku
         SetNewUnitsSavingConfig(ref WeighingConfig.Saving, Units.Units, newUnits);
         WeighingConfig.WeightSorting.HighLimit =
            ConvertWeight.MoveDecimalPoint((float) WeighingConfig.WeightSorting.HighLimit, Units.Units, newUnits);
         WeighingConfig.WeightSorting.LowLimit =
            ConvertWeight.MoveDecimalPoint((float) WeighingConfig.WeightSorting.LowLimit, Units.Units, newUnits);
         StatisticConfig.Histogram.Step = ConvertWeight.Convert(StatisticConfig.Histogram.Step, Units.Units, newUnits);
         Units.Division = ConvertWeight.Convert(Units.Division, Units.Units, newUnits);

         // File-specific congig
         foreach (File file in FileList.List)
         {
            // Prepoctu i pokud nepouziva file-specific config, je to nutne kvuli pozdejsi zmene jendotek v Weighings
            SetNewUnitsSavingConfig(ref file.FileConfig.WeighingConfig.Saving, Units.Units, newUnits);
            file.FileConfig.WeighingConfig.WeightSorting.HighLimit =
               ConvertWeight.Convert(file.FileConfig.WeighingConfig.WeightSorting.HighLimit, Units.Units, newUnits);
            file.FileConfig.WeighingConfig.WeightSorting.LowLimit =
               ConvertWeight.Convert(file.FileConfig.WeighingConfig.WeightSorting.LowLimit, Units.Units, newUnits);
         }

         // Na zaver si jednotky ulozim
         Units.Units = newUnits;
      }

      /// <summary>
      /// Move decimal point in all weight values in config. Use only with old scale.
      /// </summary>
      /// <param name="newUnits">New units</param>
      public void MoveDecimalPoint(Units newUnits)
      {
         // Osetrim pripady, kdy neni treba prepocet - provadim pouze pri prevodu z gramu nebo na gramy, mezi kg-lb ne
         if (Units.Units == newUnits)
         {
            return;
         }
         if (Units.Units == BatLibrary.Units.KG && newUnits == BatLibrary.Units.LB
             || Units.Units == BatLibrary.Units.LB && newUnits == BatLibrary.Units.KG)
         {
            // Pouze si ulozim nove jednotky, prepocet hodnot neni treba
            Units.Units = newUnits;
            return;
         }

         // Stara vaha ma v configu nastavenou pouze mez a krok histogramu
         WeighingConfig.WeightSorting.LowLimit =
            ConvertWeight.MoveDecimalPoint((float) WeighingConfig.WeightSorting.LowLimit, Units.Units, newUnits);
         StatisticConfig.Histogram.Step = ConvertWeight.MoveDecimalPoint((float) StatisticConfig.Histogram.Step,
            Units.Units, newUnits);
         foreach (File file in FileList.List)
         {
            file.FileConfig.WeighingConfig.WeightSorting.LowLimit =
               ConvertWeight.MoveDecimalPoint((float) file.FileConfig.WeighingConfig.WeightSorting.LowLimit, Units.Units,
                  newUnits);
         }

         // Na zaver si jednotky ulozim
         Units.Units = newUnits;
      }

      /// <summary>
      /// Read file with a specified name from file list in config
      /// </summary>
      /// <param name="fileName">File name</param>
      /// <returns>File instance</returns>
      public File GetFile(string fileName)
      {
         int index = FileList.GetIndex(fileName);
         if (index < 0)
         {
            return null;
         }
         return FileList.List[index];
      }

      internal static char FirstChar(string text)
      {
         // Pozor, u rucniho vazeni je pouzity prazdny string, tj. je treba to osetrit
         return text == "" ? ' ' : text[0];
      }

      internal static ScaleConfig FromDbScaleConfig(DataModel.ScaleConfig dbConfig)
      {
         var scaleConfig = new ScaleConfig
         {
            SwVersion =
            {
               Major = dbConfig.SwMajorVersion,
               Minor = dbConfig.SwMinorVersion,
               Build = dbConfig.SwBuildVersion              
            },
            ScaleName = dbConfig.ScaleName,
            Country = (Country)dbConfig.Country,
            LanguageInDatabase = (ScaleLanguagesInDatabase)dbConfig.Language,
            DateFormat = (DateFormat)dbConfig.DateFormat,
            DateSeparator1 = FirstChar(dbConfig.DateSeparator1),
            DateSeparator2 = FirstChar(dbConfig.DateSeparator2),
            TimeFormat = (TimeFormat)dbConfig.TimeFormat,
            TimeSeparator = FirstChar(dbConfig.TimeSeparator),
            DaylightSavingMode = (DaylightSavingMode)dbConfig.DaylightSavingMode,
            Units = { Units = (Units)dbConfig.Units, Division = dbConfig.UnitsDivision },           
            Display =
            {
               Mode = (DisplayMode)dbConfig.DisplayMode,
               Backlight =
               {
                  Mode = (BacklightMode)dbConfig.BacklightMode,
                  Intensity = dbConfig.BacklightIntensity,
                  Duration = dbConfig.BacklightDuration
               }
            },          
            PowerOffTimeout = dbConfig.PowerOffTimeout
         };         
         scaleConfig.WeighingConfig.WeightSorting.Mode = (WeightSorting)dbConfig.WeightSortingMode;
         scaleConfig.WeighingConfig.Saving.Mode = (SavingMode)dbConfig.SavingMode;
         scaleConfig.WeighingConfig.Saving.Filter = dbConfig.Filter;
         scaleConfig.WeighingConfig.Saving.StabilisationTime = dbConfig.StabilisationTime;        
         scaleConfig.WeighingConfig.Saving.StabilisationRange = dbConfig.StabilisationRange;
         scaleConfig.StatisticConfig.UniformityRange = dbConfig.UniformityRange;
         scaleConfig.StatisticConfig.Histogram.Mode = (HistogramMode)dbConfig.HistogramMode;
         scaleConfig.StatisticConfig.Histogram.Range = dbConfig.HistogramRange;
         scaleConfig.StatisticConfig.Histogram.Step = dbConfig.HistogramStep;
      
         return scaleConfig;
      }

      internal DataModel.ScaleConfig ToDbScaleConfig(DataModel.ScaleConfig dbConfig)
      {
         dbConfig.SwMajorVersion = (short)SwVersion.Major;
         dbConfig.SwMinorVersion = (short)SwVersion.Minor;
         dbConfig.SwBuildVersion = (short)SwVersion.Build;
         dbConfig.ScaleName = ScaleName;
         dbConfig.Country = (byte)Country;
         dbConfig.Language = (byte)LanguageInDatabase;
         dbConfig.DateFormat = (byte)DateFormat;
         dbConfig.DateSeparator1 = DateSeparator1.ToString(CultureInfo.InvariantCulture);
         dbConfig.DateSeparator2 = DateSeparator2.ToString(CultureInfo.InvariantCulture);
         dbConfig.TimeFormat = (byte)TimeFormat;
         dbConfig.TimeSeparator = TimeSeparator.ToString(CultureInfo.InvariantCulture);
         dbConfig.DaylightSavingMode = (byte)DaylightSavingMode;
         dbConfig.Units = (byte)Units.Units;
         dbConfig.UnitsDivision = Units.Division;       
         dbConfig.DisplayMode = (byte)Display.Mode;
         dbConfig.BacklightMode = (byte)Display.Backlight.Mode;
         dbConfig.BacklightIntensity = (byte)Display.Backlight.Intensity;
         dbConfig.BacklightDuration = (byte)Display.Backlight.Duration;      
         dbConfig.PowerOffTimeout = (short)PowerOffTimeout;      
         dbConfig.WeightSortingMode = (byte)WeighingConfig.WeightSorting.Mode;
         dbConfig.SavingMode = (byte)WeighingConfig.Saving.Mode;
         dbConfig.Filter = WeighingConfig.Saving.Filter;
         dbConfig.StabilisationTime = WeighingConfig.Saving.StabilisationTime;     
         dbConfig.StabilisationRange = WeighingConfig.Saving.StabilisationRange;
         dbConfig.UniformityRange = (byte)StatisticConfig.UniformityRange;
         dbConfig.HistogramMode = (byte)StatisticConfig.Histogram.Mode;
         dbConfig.HistogramRange = (byte)StatisticConfig.Histogram.Range;
         dbConfig.HistogramStep = StatisticConfig.Histogram.Step;
         return dbConfig;
      }

      internal static void FromWeighing(ref DataModel.ScaleConfig dbConfig, Weighing weighing)
      {
         dbConfig.SwMajorVersion = (short)weighing.WeighingData.ScaleConfig.SwVersion.Major;
         dbConfig.SwMinorVersion = (short)weighing.WeighingData.ScaleConfig.SwVersion.Minor;
         dbConfig.SwBuildVersion = (short)weighing.WeighingData.ScaleConfig.SwVersion.Build;
         dbConfig.Country = (byte)weighing.WeighingData.ScaleConfig.Country;
         dbConfig.Language = (byte)weighing.WeighingData.ScaleConfig.LanguageInDatabase;
         dbConfig.DateFormat = (byte)weighing.WeighingData.ScaleConfig.DateFormat;
         dbConfig.DateSeparator1 = weighing.WeighingData.ScaleConfig.DateSeparator1.ToString();
         dbConfig.DateSeparator2 = weighing.WeighingData.ScaleConfig.DateSeparator2.ToString();
         dbConfig.TimeFormat = (byte)weighing.WeighingData.ScaleConfig.TimeFormat;
         dbConfig.TimeSeparator = weighing.WeighingData.ScaleConfig.TimeSeparator.ToString();
         dbConfig.DaylightSavingMode = (byte)weighing.WeighingData.ScaleConfig.DaylightSavingMode;
         dbConfig.Units = (byte)weighing.WeighingData.ScaleConfig.Units.Units;
         dbConfig.UnitsDivision = weighing.WeighingData.ScaleConfig.Units.Division;
         dbConfig.DisplayMode = (byte)weighing.WeighingData.ScaleConfig.Display.Mode;
         dbConfig.BacklightMode = (byte)weighing.WeighingData.ScaleConfig.Display.Backlight.Mode;
         dbConfig.BacklightIntensity = (byte)weighing.WeighingData.ScaleConfig.Display.Backlight.Intensity;
         dbConfig.BacklightDuration = (byte)weighing.WeighingData.ScaleConfig.Display.Backlight.Duration;
         dbConfig.PowerOffTimeout = (short)weighing.WeighingData.ScaleConfig.PowerOffTimeout;
         dbConfig.WeightSortingMode = (byte)weighing.WeighingData.ScaleConfig.WeighingConfig.WeightSorting.Mode;
         dbConfig.SavingMode = (byte)weighing.WeighingData.ScaleConfig.WeighingConfig.Saving.Mode;
         dbConfig.Filter = weighing.WeighingData.ScaleConfig.WeighingConfig.Saving.Filter;
         dbConfig.StabilisationTime = weighing.WeighingData.ScaleConfig.WeighingConfig.Saving.StabilisationTime;
         dbConfig.StabilisationRange = weighing.WeighingData.ScaleConfig.WeighingConfig.Saving.StabilisationRange;
         dbConfig.UniformityRange = (byte)weighing.WeighingData.ScaleConfig.StatisticConfig.UniformityRange;
         dbConfig.HistogramMode = (byte)weighing.WeighingData.ScaleConfig.StatisticConfig.Histogram.Mode;
         dbConfig.HistogramRange = (byte)weighing.WeighingData.ScaleConfig.StatisticConfig.Histogram.Range;
         dbConfig.HistogramStep = ConvertWeight.Convert(
                  weighing.WeighingData.ScaleConfig.StatisticConfig.Histogram.Step, 
                  weighing.WeighingData.ScaleConfig.Units.Units, BatLibrary.Units.G);
      }
   }
}