namespace DataContext
{
   /// <summary>
   /// CSV file encoding
   /// </summary>
   public enum CsvEncoding {
      DEFAULT,    // Encoding.Default: ANSI kodovani, ktere je prave nastavene ve Windows
      UTF16,      // Encoding.Unicode: Unicode UTF-16, v Excelu funguje dobre
      UTF8        // Encoding.UTF8:    Unicode UTF-8, v Excelu nefunguje, v OpenOffice ano
   }
}