﻿using System;
using Bat1Library;
using BatLibrary;

namespace DataContext
{
   /// <summary>
   /// Source data and statistic results of one weighing
   /// </summary>
   public class Weighing
   {
      /// <summary>
      /// Source data
      /// </summary>
      public WeighingData WeighingData { get; private set; }

      /// <summary>
      /// Statistic results
      /// </summary>
      public WeighingResults WeighingResults { get; private set; }

      /// <summary>
      /// Additional flock data used in statistics (if the weighing belongs to a flock)
      /// </summary>
      public FlockData FlockData;

      public ScaleType ScaleType;

      /// <summary>
      /// Constructor for manually entered results
      /// </summary>
      /// <param name="weighingData">Source weighing data</param>
      /// <param name="weighingResults">Weighing statistics results</param>
      public Weighing(WeighingData weighingData, WeighingResults weighingResults)
      {
         WeighingData = weighingData;
         WeighingResults = weighingResults;
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="weighingData">Source weighing data</param>
      public Weighing(WeighingData weighingData)
      {
         WeighingData = weighingData;
         RecalculateResults();
      }

      /// <summary>
      /// Get date and time of weighing start
      /// </summary>
      /// <returns></returns>
      public DateTime GetMinDateTime()
      {
         //if (WeighingData.ResultType == ResultType.MANUAL)
         if (WeighingData.SampleList == null)
         {
            // U rucne zadaneho vazeni vezmu datum startu z vysledku
            return GetMinDateTime(Flag.ALL);
         }

         // U vazeni z vahy beru start ze vzorku - napr. pri editaci vzorku behem importu musim brat v uvahu pouze
         // vzorky, ne vysledky. Pokud napr. umazu nejaky vzorek, behem editace se upravi jen seznam vzorku, vysledky
         // se neprepocitavaji.
         return WeighingData.SampleList.MinDateTime;
      }

      public DateTime GetMinDateTime(Flag flag)
      {
         var result = WeighingResults.GetResult(flag);
         if (result == null)
         {
            return DateTime.MinValue;
         }
         return result.WeighingStarted;
      }

      public DateTime GetMaxDateTime(Flag flag)
      {
         //if (WeighingData.ResultType == ResultType.MANUAL)
         if (WeighingData.SampleList == null)
         {
            // U rucne zadanych vysledku beru ze vazil 1 hodinu
            return GetMinDateTime(flag).AddHours(1);
         }
         return WeighingData.SampleList.MaxDateTime;
      }

      /// <summary>
      /// Recalculate results for this weighing
      /// </summary>
      public void RecalculateResults()
      {
         WeighingResults = new WeighingResults(WeighingData.SampleList,
            WeighingData.ScaleConfig.StatisticConfig.UniformityRange,
            WeighingData.ScaleConfig.StatisticConfig.Histogram,
            WeighingData.ScaleConfig.Units.Units);
      }

      /// <summary>
      /// Recalculate weighing to new units
      /// </summary>
      /// <param name="units">New units</param>
      public void ChangeUnits(Units units)
      {
         // Pokud chce jednotky, ve kterych vazeni uz je, neni treba prepocet
         //if (WeighingData.ScaleConfig.Units.Units == units)
         if (units == Units.G)
         {
            return;
         }

         if (WeighingData.SampleList == null)
         {  // manual result
            // U rucne zadanych vysledku prepocitam rovnou vysledek           
            WeighingData.ScaleConfig.SetNewUnits(units); // Config vahy
         }
         else
         {
            // U vysledku stazenych z vah zmenim vzorky a vysledky vypoctu znovu
            WeighingData.SampleList.Convert(WeighingData.ScaleConfig.Units.Units, units);

            // Config vahy musim zmenit pred prepoctem vysledku
            WeighingData.ScaleConfig.SetNewUnits(units);

            // Prepocitam vysledky
            RecalculateResults();
         }
      }

      /// <summary>
      /// Compare function for List(Weighing).Sort()
      /// </summary>
      /// <param name="x">First parameter</param>
      /// <param name="y">Second parameter</param>
      /// <returns>1, -1 or 0</returns>
      public static int CompareByTime(Weighing x, Weighing y)
      {
         var xDateTime = x.GetMinDateTime();
         var yDateTime = y.GetMinDateTime();
         if (xDateTime < yDateTime)
         {
            return -1;
         }
         if (xDateTime > yDateTime)
         {
            return 1;
         }

         // Datumy jsou shodne, porovnam soubory
         var result = String.Compare(x.WeighingData.File.Name, y.WeighingData.File.Name, StringComparison.Ordinal);
         if (result < 0)
         {
            return -1;
         }
         if (result > 0)
         {
            return 1;
         }

         // Soubory jsou taky shodne, porovnam jeste vahy
         result = String.Compare(x.WeighingData.ScaleConfig.ScaleName, y.WeighingData.ScaleConfig.ScaleName, StringComparison.Ordinal);
         if (result < 0)
         {
            return -1;
         }
         return result > 0 ? 1 : 0;

         // Vse je shodne
      }

      /// <summary>
      /// Compare function for List(Weighing).Sort()
      /// </summary>
      /// <param name="x">First parameter</param>
      /// <param name="y">Second parameter</param>
      /// <returns>1, -1 or 0</returns>
      public static int CompareByFlockDay(Weighing x, Weighing y)
      {
         var xDay = x.FlockData.Day;
         var yDay = y.FlockData.Day;
         if (xDay < yDay)
         {
            return -1;
         }
         // Dny jsou shodne, porovnam soubory podle data a casu
         return xDay > yDay ? 1 : CompareByTime(x, y);
      }
   }
}