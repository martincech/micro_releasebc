namespace DataContext
{
   /// <summary>
   /// Predefined CSV configs
   /// </summary>
   public enum CsvTemplate {
      EXCEL,
      OPEN_OFFICE,
      CUSTOM
   }
}