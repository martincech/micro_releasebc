﻿using System;
using System.Windows.Forms;
using System.IO;
using Veit.SFolders;

namespace Bat1Db {
    public partial class FormDbMain : Form {
        private string dataFolder;
        
        /// <summary>
        /// Database file name without path
        /// </summary>
        private const string databaseFileName = "Bat1.b1d";

        /// <summary>
        /// Full database file name including path
        /// </summary>
        private string fullDatabaseFileName {
            get { return dataFolder + @"\" + databaseFileName; }
        }

        public FormDbMain() {
            InitializeComponent();

            // Nactu nazev pracovniho adresare (struktura "Veit\Bat1\V7")
            SFolders sFolders = new SFolders(@"Veit\Bat1\V7");
            dataFolder = sFolders.DataFolder;       // Zapamatuju si pracovni adresar
        }

        private void DelTree(string directoryName) {
            // Pokud adresar neexistuje, nedelam nic
            if (!Directory.Exists(directoryName)) {
                MessageBox.Show("Folder doesn't exist");
                return;
            }
            
            // Smazu vsechny soubory v adresari
            foreach (string file in Directory.GetFiles(directoryName)) {
                System.IO.File.Delete(file);
            }

            // Smazu vsechny podadresaree - rekurzivnim volanim
            foreach (string directory in Directory.GetDirectories(directoryName)) {
                // Soubory v podadresari + podadresare samotne
                Directory.Delete(directory, true);
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            if (MessageBox.Show("Really?", buttonDelete.Text, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }
            if (!System.IO.File.Exists(fullDatabaseFileName)) {
                MessageBox.Show("File doesn't exist");
                return;
            }
            System.IO.File.Delete(fullDatabaseFileName);
            MessageBox.Show("File " + fullDatabaseFileName + " has been deleted");
        }

        private void button1_Click(object sender, EventArgs e) {
            // Smazu obsah cele slozky, ale slozku samotnou ponecham
            if (MessageBox.Show("Really?", button1.Text, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }
            DelTree(dataFolder);
            MessageBox.Show("All files in directory " + dataFolder + " have been deleted");
        }

        private void buttonShowLocation_Click(object sender, EventArgs e) {
            MessageBox.Show(dataFolder);
        }

        private void buttonCopy_Click(object sender, EventArgs e) {
            if (!System.IO.File.Exists(fullDatabaseFileName)) {
                MessageBox.Show("File doesn't exist");
                return;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = fullDatabaseFileName;
            saveFileDialog.DefaultExt = "b1d";
            saveFileDialog.Filter = "BAT1 database file (*.b1d)|*.b1d";
            if (saveFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }
            System.IO.File.Copy(fullDatabaseFileName, saveFileDialog.FileName, true);

            MessageBox.Show("Copy successful");
        }
    }
}
