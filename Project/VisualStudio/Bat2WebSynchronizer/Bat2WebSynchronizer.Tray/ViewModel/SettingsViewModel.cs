﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Input;
using Bat2Library.Sms;
using Bat2WebSynchronizer.Tray.Model;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Usb.ELO;
using Usb.MODEM;
using Utilities.Observable;

namespace Bat2WebSynchronizer.Tray.ViewModel
{
   /// <summary>
   /// Program data holder
   /// </summary>
   public class SettingsViewModel : ObservableObject
   {
      #region Private properties

      private bool stayOpened;
      private bool logged;
      private Settings userSettings;

      private ObservableCollection<Device> deviceList;

      private ObservableCollection<ModemDevice> modemList;
      private ObservableCollection<SmsStatus> modemSettings;
      private ObservableCollection<EloDevice> eloList;
      private ObservableCollection<EloSetting> eloSettings;

      private ICommand saveEloSettings;
      private ICommand startScanElo;
      private ICommand stopScanElo;
      private ICommand loginCommand;

      #endregion

      #region Constructor

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public SettingsViewModel()
      {
         ModemList = new ObservableCollection<ModemDevice>();
         ModemSettings = new ObservableCollection<SmsStatus>();

         EloList = new ObservableCollection<EloDevice>();
         EloSettings = new ObservableCollection<EloSetting>();

         deviceList = new ObservableCollection<Device>();
      }

      #endregion

      #region Public properties

      public string Folder
      {
         get
         {
            var folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Veit\Bat2WebSynchronizer";
            if (!Directory.Exists(folder))
            {
               Directory.CreateDirectory(folder);
            }
            return folder;
         }
      }

      public ObservableCollection<ModemDevice> ModemList
      {
         get { return modemList; }
         private set
         {
            SetProperty(ref modemList, value);
         }
      }

      public ObservableCollection<SmsStatus> ModemSettings
      {
         get { return modemSettings; }
         private set
         {
            SetProperty(ref modemSettings, value);
         }
      }

      public ObservableCollection<EloDevice> EloList
      {
         get { return eloList; }
         private set
         {
            SetProperty(ref eloList, value);
         }
      }

      public ObservableCollection<EloSetting> EloSettings
      {
         get { return eloSettings; }
         private set
         {
            SetProperty(ref eloSettings, value);
         }
      }

      /// <summary>
      /// List of all connected devices (modem & elo)
      /// </summary>
      public ObservableCollection<Device> DeviceList
      {
         get { return deviceList; }
         private set
         {
            SetProperty(ref deviceList, value);
         }
      }

      /// <summary>
      /// User settings
      /// </summary>
      public Settings UserSettings
      {
         get { return userSettings; }
         set { SetProperty(ref userSettings, value); }
      }

      /// <summary>
      /// Indicate if user is logged
      /// </summary>
      public bool Logged
      {
         get { return logged; }
         set
         {
            SetProperty(ref logged, value);
            DispatcherHelper.RunAsync(() =>
            {
               ((RelayCommand)LoginCommand).RaiseCanExecuteChanged();
            });
         }
      }

      /// <summary>
      /// Define if window is closed when loses focus
      /// </summary>
      public bool StayOpened
      {
         get { return stayOpened; }
         set { SetProperty(ref stayOpened, value); }
      }

      /// <summary>
      /// Test connection to server command
      /// </summary>
      public ICommand LoginCommand
      {
         get { return loginCommand; }
         set { SetProperty(ref loginCommand, value); }
      }

      /// <summary>
      /// Save elo settings command
      /// </summary>
      public ICommand SaveEloSettings
      {
         get { return saveEloSettings; }
         set { SetProperty(ref saveEloSettings, value); }
      }

      /// <summary>
      /// Start scanning command
      /// </summary>
      public ICommand StartScanElo
      {
         get { return startScanElo; }
         set { SetProperty(ref startScanElo, value); }
      }

      /// <summary>
      /// Stop scanning command
      /// </summary>
      public ICommand StopScanElo
      {
         get { return stopScanElo; }
         set { SetProperty(ref stopScanElo, value); }
      }
      #endregion
   }
}
