﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Bat2Library.Sms;
using Bat2Library.Sms.Workers;
using Bat2WebSynchronizer.Tray.Model;
using Bat2WebSynchronizer.Tray.Properties;
using Bat2WebSynchronizer.Tray.ViewModel;
using Desktop.Wpf.Presentation;
using Usb.MODEM;

namespace Bat2WebSynchronizer.Tray.Code
{
   public class CodeSms
   {
      //Data file name for messages in right format
      private const string TOSYNC = @"\ToSync.xml";
      //Check modem delay 20s
      private const int TIMER_INTERVAL = 20 * 1000;

      private readonly SettingsViewModel model;
      // Background workers for reading the SMS
      private SmsReadWorkers smsReadWorkers;

      public CodeSms(SettingsViewModel model)
      {
         this.model = model;
         this.model.ModemList.CollectionChanged += ModemList_CollectionChanged;
      }

      /// <summary>
      /// Handles changes in collection of modems
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void ModemList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         if (e.Action == NotifyCollectionChangedAction.Add)
         {
            foreach (var newItem in e.NewItems)
            {
               var device = newItem as ModemDevice;
               if (device == null)
               {
                  continue;
               }

               var setting = model.ModemSettings.FirstOrDefault(f => f.PortName == device.PortName);
               if (setting == null)
               {
                  setting = new SmsStatus(device);
                  model.ModemSettings.Add(setting);

               }

               DispatcherHelper.RunAsync(() =>
               {
                  model.DeviceList.Add(new Device()
                  {
                     SerialNumber = device.SerialNumber,
                     Event = setting.Event,
                     ModemName = setting.ModemName,
                     ProductName = setting.Device == null ? "" : setting.Device.ProductName,
                     SignalStrength = setting.SignalStrength,
                     Type = DeviceType.Modem,
                     PortName = device.PortName
                  });

                  UpdatePorts();
                  ModemStatus(setting);
               });
            }
         }

         if (e.Action == NotifyCollectionChangedAction.Remove)
         {
            foreach (var oldItem in e.OldItems)
            {
               var device = oldItem as ModemDevice;
               if (device == null)
               {
                  continue;
               }

               var setting = model.ModemSettings.FirstOrDefault(f => f.PortName == device.PortName);
               if (setting != null)
               {
                  model.ModemSettings.Remove(setting);
               }

               var deviceItem = model.DeviceList.FirstOrDefault(f => f.PortName == device.PortName);
               if (deviceItem != null)
               {
                  DispatcherHelper.RunAsync(() =>
                  {
                     model.DeviceList.Remove(deviceItem);
                  });
               }
            }
         }
      }

      /// <summary>
      /// After specific time interval check if new connected modem is ready for receving sms messages
      /// </summary>
      /// <param name="setting"></param>
      private async void ModemStatus(SmsStatus setting)
      {
         await Task.Delay(TIMER_INTERVAL);
         if (!model.ModemSettings.Contains(setting) || setting.Event == SmsEvent.READY) return;

         var msg = string.Format(Resources.DeviceNoResponse, setting.ModemName);
         if (setting.Event == SmsEvent.RESET)
         {
            msg = string.Format(Resources.SimNoResponse, setting.ModemName);
         }
         CodeMessageBox.Instance.Show(msg, model.StayOpened);
      }

      /// <summary>
      /// Update ports with  modems
      /// </summary>
      private void UpdatePorts()
      {
         if (smsReadWorkers != null)
         {
            smsReadWorkers.Stop();
            smsReadWorkers = null;
         }
         if (model.ModemSettings.Count == 0) return;
         smsReadWorkers = new SmsReadWorkers(model.ModemSettings.Select(s => s.Device).ToList(),
             gsmWorker_ProgressChanged,
             (sender, e) => { });
         smsReadWorkers.Start();
      }

      /// <summary>
      /// Gsm event handler witch processes changes on gsm modem
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void gsmWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
      {
         var date = DateTime.Now;
         var status = (SmsStatus)e.UserState;

         //Received SMS message
         if (status.Event == SmsEvent.SMS_READ_OK)
         {
            XmlSerializer.Append(status, model.Folder + TOSYNC);

            var message = new KeyValuePair<DateTime, SmsStatus>(date, status);
            model.UserSettings.ToSyncSms.Add(message);
            Debug.WriteLine(status.SmsText);
            DispatcherHelper.RunAsync(() => model.UserSettings.LogMessage.Add(new KeyValuePair<DateTime, string>(date, String.Format(Resources.LOG_SMS_RECEIVED, status.SmsText))));
         }

         //Change status in of modem
         if (status.Event == SmsEvent.READY || status.Event == SmsEvent.DETECT ||
             status.Event == SmsEvent.RESET)
         {
            UpdateStatus(status);
         }
      }

      /// <summary>
      /// Update modem status
      /// </summary>
      /// <param name="status"></param>
      private void UpdateStatus(SmsStatus status)
      {
         var modem = model.ModemSettings.FirstOrDefault(x => x.Device.PortName.Equals(status.PortName));
         if (modem != null)
         {
            modem.SignalStrength = status.SignalStrength;
            modem.Event = status.Event;
            modem.ModemName = status.ModemName;
            modem.PortName = status.PortName;
            if (status.Device != null)
            {
               modem.Device = status.Device;
            }
         }

         var device = model.DeviceList.FirstOrDefault(x => x.PortName == status.PortName);
         if (device != null)
         {
            var newDevice = new Device()
            {
               SignalStrength = status.SignalStrength,
               Event = status.Event,
               ModemName = status.ModemName,
               PortName = status.PortName,
               SerialNumber = status.Device != null ? status.Device.SerialNumber : "",
               Type = DeviceType.Modem
            };

            DispatcherHelper.RunAsync(() =>
            {
               try
               {
                  device = model.DeviceList.FirstOrDefault(x => x.PortName == status.PortName);
                  var index = model.DeviceList.IndexOf(device);
                  model.DeviceList[index] = newDevice;
               }
               catch (Exception){}
            });
         }
      }
   }
}
