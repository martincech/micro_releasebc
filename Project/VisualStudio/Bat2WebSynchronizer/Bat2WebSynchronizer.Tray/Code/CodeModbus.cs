﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Timers;
using Bat2Library.ModbusWorker;
using Bat2Library.Sms;
using Bat2WebSynchronizer.Tray.Helpers;
using Bat2WebSynchronizer.Tray.Model;
using Bat2WebSynchronizer.Tray.ViewModel;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Usb.ELO;
using Utilities.Extensions;

namespace Bat2WebSynchronizer.Tray.Code
{
   public class CodeModbus
   {
      private const string ELO_FILE = @"\EloDevices.xml";
      private const int ELO_CHECK_TIME = 60 * 1000;      //60s
      private Timer timer;

      private readonly SettingsViewModel model;
      private readonly ModbusManager manager;

      public CodeModbus(SettingsViewModel model)
      {
         this.model = model;

         manager = new ModbusManager();
         manager.ReadHandler += Read;

         this.model.EloList.CollectionChanged += EloList_CollectionChanged;

         this.model.SaveEloSettings = new RelayCommand(port => { SaveElos((int)port); }, port => true);
         this.model.StartScanElo = new RelayCommand(port => { ScanElo((int)port); }, port => true);
         this.model.StopScanElo = new RelayCommand(port => { StopElo((int)port); }, port => true);

         InitTimer();
         LoadElo();
      }

      /// <summary>
      /// Load elo settings from file
      /// </summary>
      private void LoadElo()
      {
         var eloSettings = XmlSerializer.Load<EloSetting>(model.Folder + ELO_FILE);
         foreach (var elo in eloSettings)
         {
            model.EloSettings.Add(elo);
         }
      }

      /// <summary>
      /// Create timer which periodicaly check connected modbus scales
      /// </summary>
      private void InitTimer()
      {
         timer = new Timer(ELO_CHECK_TIME);
         timer.Elapsed += TimerOnElapsed;
         timer.Start();
      }

      /// <summary>
      /// Timer tick to check connected modbus scales
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="elapsedEventArgs"></param>
      private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
      {
         foreach (var eloDevice in model.EloList)
         {
            var setting = model.EloSettings.FirstOrDefault(f => !f.Scanning && f.SerialNumber == eloDevice.SerialNumber);
            if (setting != null)
            {
               CheckElos(setting);
            }
         }
      }

      /// <summary>
      /// Collection of elo devices changed
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void EloList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         if (e.Action == NotifyCollectionChangedAction.Add)
         {
            foreach (var newItem in e.NewItems)
            {
               var device = newItem as EloDevice;
               if (device == null)
               {
                  continue;
               }

               var setting = model.EloSettings.FirstOrDefault(f => f.SerialNumber == device.SerialNumber);
               var newSetting = device.Setting();
               if (setting != null)
               {
                  setting.PortNumber = newSetting.PortNumber;
               }
               else
               {
                  model.EloSettings.Add(newSetting);
                  setting = newSetting;
               }
               //Scane new elo device
               ScanElo(setting.PortNumber);
               //Create scheduler 
               CreateEloScheduler(setting);
               //Add to list of all devices shown in menu
               DispatcherHelper.RunAsync(() =>
               {
                  model.DeviceList.Add(new Device()
                  {
                     Event = SmsEvent.READY,
                     ModemName = "Wire connection(ELO)",
                     ProductName = device.ProductName,
                     SignalStrength = 100,
                     Type = DeviceType.ELO,
                     SerialNumber = device.SerialNumber,
                     PortName = device.PortName
                  });
               });
            }
         }

         if (e.Action == NotifyCollectionChangedAction.Remove)
         {
            foreach (var newItem in e.OldItems)
            {
               var device = newItem as EloDevice;
               if (device == null)
               {
                  continue;
               }

               var setting = model.EloSettings.FirstOrDefault(f => f.SerialNumber == device.SerialNumber);
               if (setting != null)
               {
                  manager.StopScheduler(setting.PortNumber);
               }

               var deviceItem = model.DeviceList.FirstOrDefault(f => f.SerialNumber == device.SerialNumber);
               if (deviceItem != null)
               {
                  DispatcherHelper.RunAsync(() =>
                  {
                     model.DeviceList.Remove(deviceItem);
                  });
               }

            }
         }
      }

      /// <summary>
      /// Scan connected elo to find modbus scales
      /// </summary>
      /// <param name="port">elo port</param>
      private void ScanElo(int port)
      {
         var setting = GetEloSetting(port);
         setting.Scanning = true;
         DispatcherHelper.RunAsync(() =>
         {
            setting.Scales.Clear();
         });
         setting.Scanning = manager.Scan(setting.Setup());
      }

      /// <summary>
      /// Stop scanning/reading elo on sepcific port
      /// </summary>
      /// <param name="port">elo port</param>
      private void StopElo(int port)
      {
         manager.Stop(port);
      }

      /// <summary>
      /// Save elo settings to file + update elo scheduler
      /// </summary>
      private void SaveElos(int port)
      {
         var setting = GetEloSetting(port);
         EloHelper.SaveEloSettings(model.EloSettings.ToList(), model.Folder + ELO_FILE);
         manager.UpdateScheduler(setting.PortNumber, setting.Hour, setting.Minute);
      }

      /// <summary>
      /// Create new delayed task to read modbus scales data
      /// </summary>
      /// <param name="setting"></param>
      private void CreateEloScheduler(EloSetting setting)
      {
         manager.Read(setting.Setup(), setting.Scales.Select(s => s.Address).ToList(), setting.Hour, setting.Minute);
      }

      /// <summary>
      /// Check connected scales if thay are still connected
      /// </summary>
      /// <param name="setting"></param>
      private void CheckElos(EloSetting setting)
      {
         if (setting.Scanning || setting.Scales.Count < 1)
         {
            return;
         }
         setting.Scales.ForEach((item) => { item.Connected = false; });
         manager.Scan(setting.Setup(), setting.Scales.Select(s => s.Address).ToList());
      }

      /// <summary>
      ///  Read data handler
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="data"></param>
      private void Read(object sender, EloTaskEventArgs data)
      {
         var elo = model.EloSettings.FirstOrDefault(f => f.PortNumber == data.Data.Port);
         if (elo == null)
         {
            return;
         }

         switch (data.Operation)
         {
            case EloOperation.Scan:
               elo.Address = data.Data.Address;
               if (data.Data.State == null)
               {
                  break;
               }

               DispatcherHelper.RunAsync(() =>
               {
                  var oldScale = elo.Scales.FirstOrDefault(f => f.Address == data.Data.Address);
                  if (oldScale != null)
                  {
                     elo.Scales.Remove(oldScale);
                  }

                  elo.Scales.Add(new ScaleDevice()
                  {
                     Address = data.Data.Address,
                     Connected = true,
                     Name = data.Data.State.ScaleName
                  });
                  manager.UpdateScheduler(elo.PortNumber, elo.Scales.Select(s => s.Address).ToList());
               });
               break;
            case EloOperation.Check:
               var scale = elo.Scales.FirstOrDefault(f => f.Address == data.Data.Address);
               if (scale != null && data.Data.State != null)
               {
                  scale.Connected = true;
               }
               break;
            case EloOperation.Read:
               if (data.Data.State == null)
               {
                  return;
               }
               var smsStatus = EloHelper.Status(data.Data.State);
               model.UserSettings.ToSyncSms.Add(new KeyValuePair<DateTime, SmsStatus>(DateTime.Now, smsStatus));
               model.UserSettings.RecievedSmsCount++;
               break;
            case EloOperation.Canceled:
               elo.Address = data.Data.Address;
               elo.Scanning = false;
               break;
         }

         if (data.Last)
         {
            elo.Scanning = false;
         }
      }

      /// <summary>
      /// Get elo settings for specific port number
      /// </summary>
      /// <param name="port">port number</param>
      /// <returns>elo settings</returns>
      private EloSetting GetEloSetting(int port)
      {
         var device = model.EloList.FirstOrDefault(f => f.PortName.ToLower() == "com" + port);
         if (device == null) return null;

         return model.EloSettings.FirstOrDefault(f => f.SerialNumber == device.SerialNumber);
      }
   }
}
