﻿using System;
using System.Windows;
using System.Windows.Threading;
using Bat2WebSynchronizer.Tray.Properties;

namespace Bat2WebSynchronizer.Tray.Code
{
   /// <summary>
   /// Singleton for showing message boxes
   /// </summary>
   public class CodeMessageBox
   {
      private static CodeMessageBox instance;

      private CodeMessageBox() { }

      public event EventHandler<bool> StayOpened;

      public static CodeMessageBox Instance
      {
         get { return instance ?? (instance = new CodeMessageBox()); }
      }

      /// <summary>
      /// Show message box
      /// </summary>
      /// <param name="message">Message to show</param>
      /// <param name="stayOpened">existing window stays open if true</param>
      public void Show(string message, bool stayOpened)
      {
         if (!stayOpened)
         {
            MessageBox.Show(message, Resources.Warning);
            return;
         }
         var dispatcher = Application.Current.Dispatcher;
         dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate
         {
            InvokeEvent(true);
            MessageBox.Show(message, Resources.Warning);
            InvokeEvent(false);
         });
      }
      
      private void InvokeEvent(bool flag)
      {
         if (StayOpened != null)
         {
            StayOpened(this, flag);
         }
      }
   }
}
