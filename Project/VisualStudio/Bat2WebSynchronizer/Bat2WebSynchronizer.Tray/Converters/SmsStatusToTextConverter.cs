﻿using System;
using System.Windows.Data;
using Bat2Library.Sms;
using Bat2WebSynchronizer.Tray.Model;

namespace Bat2WebSynchronizer.Tray.Converters
{
   public class SmsStatusToTextConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         var device = value as Device;
         if (device == null) return null;

         string msg;
         switch (device.Event)
         {
            case SmsEvent.RESET:
               msg = Properties.Resources.MODEM_DETECT + " : " + device.ModemName;
               break;
            case SmsEvent.READY:
               if (device.Type == DeviceType.Modem)
               {
                  msg = device.ModemName + " : Signal " + device.SignalStrength + "%";
               }
               else
               {
                  msg = device.ModemName + " SN : " + device.SerialNumber;
               }
               break;
            default:
               msg = Properties.Resources.MODEM_DISCONECTED + " : " + device.ProductName;
               break;
         }
         return msg;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
