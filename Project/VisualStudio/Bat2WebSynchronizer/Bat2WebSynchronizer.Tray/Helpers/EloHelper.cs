﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Bat2Library.ModbusWorker;
using Bat2Library.ModbusWorker.Worker;
using Bat2WebSynchronizer.Tray.Model;
using Modbus;
using Usb.Helpers;
using Bat2Library.Sms;
using Usb.ELO;
using Usb.MODEM;

namespace Bat2WebSynchronizer.Tray.Helpers
{
   public static class EloHelper
   {
      private const int MODBUS_TIMEOUT = 175;

      private const string SPACE = " ";
      private const string EMPTY = "-";

      /// <summary>
      /// Create new setup from elo settings
      /// </summary>
      /// <param name="settings"></param>
      /// <returns></returns>
      public static Setup Setup(this EloSetting settings)
      {
         Setup setup = new Setup();
         setup.SetDefaults();
         setup.ModbusScales.PortNumber = settings.PortNumber;
         setup.ModbusScales.FirstAddress = settings.AddressFrom;
         setup.ModbusScales.LastAddress = settings.AddressTo;
         setup.ModbusProtocol.Parity = (Uart.Parity)settings.Parity;
         setup.ModbusProtocol.Protocol = (ModbusPacket.PacketMode)settings.Mode;
         setup.ModbusProtocol.Speed = settings.BaudRate;
         setup.ModbusProtocol.ReplyTimeout = MODBUS_TIMEOUT;
         return setup;
      }

      /// <summary>
      /// Create new elo settings from elo device
      /// </summary>
      /// <param name="elo">connected elo device</param>
      /// <returns></returns>
      public static EloSetting Setting(this EloDevice elo)
      {
         Setup setup = new Setup();
         setup.SetDefaults();
         return new EloSetting()
         {
            AddressFrom = 1,
            AddressTo = (byte)setup.ModbusScales.LastAddress,
            Mode = (int)setup.ModbusProtocol.Protocol,
            Parity = (int)setup.ModbusProtocol.Parity,
            SerialNumber = elo.SerialNumber,
            BaudRate = setup.ModbusProtocol.Speed,
            PortNumber = int.Parse(elo.PortName.ToLower().Substring(3)),
            Hour = 23,
            Minute = 59
         };
      }

      public static SmsStatus Status(CurrentState state)
      {
         // SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999
         // SCALE 123456789012345 DAY 999 31.12.9999 23:59:59PM FEMALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999 MALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999
         string message = "SCALE";
         message += SPACE;
         message += int.Parse(state.ScaleName).ToString("X");
         message += SPACE;
         message += "DAY";
         message += SPACE;
         message += state.StatisticsBuilderF.Statistics.DayNumber;
         message += SPACE;
         message += string.Format("{0}.{1}.{2}", state.DateTimeBuilder.Day, state.DateTimeBuilder.Month, state.DateTimeBuilder.Year);
         message += SPACE;
         if (state.UseBothGenders)
         {
            message += "FEMALES:";
            message += SPACE;
         }
         //Female stats
         message += Stats(state.StatisticsBuilderF.Statistics, state.DailyGainF);

         //Male stats
         if (state.UseBothGenders)
         {
            message += SPACE;
            message += "MALES:";
            message += SPACE;
            message += Stats(state.StatisticsBuilderM.Statistics, state.DailyGainM);
         }

         return  new SmsStatus()
         {
            Device = null,
            Event = SmsEvent.SMS_READ_OK,
            ModemName = "ELO E216",
            SmsText = message,
            Synced = false
         };
      }

      private static string Stats(Statistics stats, float gain)
      {
         string message = "Cnt";
         message += SPACE;
         message += stats.Count.ToString();
         message += SPACE;
         message += "Avg";
         message += SPACE;
         message += stats.Average.ToString(CultureInfo.InvariantCulture);
         message += SPACE;
         message += "Gain";
         message += SPACE;
         message += gain.ToString(CultureInfo.InvariantCulture); ;
         message += SPACE;
         message += "Sig";
         message += SPACE;
         message += stats.Sigma.ToString(CultureInfo.InvariantCulture); ;
         message += SPACE;
         message += "Cv";
         message += SPACE;
         message += stats.Cv.ToString(CultureInfo.InvariantCulture); ;
         message += SPACE;
         message += "Uni";
         message += SPACE;
         message += stats.Uniformity.ToString(CultureInfo.InvariantCulture); ;
         return message;
      }
      /// <summary>
      /// Save elo setting into file
      /// </summary>
      public static void SaveEloSettings(List<EloSetting> elos, string path)
      {
         XmlSerializer.Update(elos, path);
      }
   }
}
