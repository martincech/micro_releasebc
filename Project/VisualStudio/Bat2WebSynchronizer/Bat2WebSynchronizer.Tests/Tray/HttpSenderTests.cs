﻿using System;
using System.Collections.Generic;
using System.Net;
using Bat2Library.Sms;
using Bat2Library.Utilities.Sms;
using Bat2Library.Utilities.Sms.Models;
using Bat2WebSynchronizer.Tray.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Bat2WebSynchronizer.Tests.Tray
{
   [TestClass]
   public class HttpSenderTests
   {
      /// <summary>
      /// Need to run BatWebApp with specification described below
      /// </summary>
      private Settings settings;

      private const string CONTROLLER_PATH = "/api/v1/stats";

      [TestInitialize]
      public void Init()
      {
         settings = new Settings
         {
            Password = "Admin_123",
            ServerUrl = "http://localhost:49942",
            Name = "admin"
         };
         settings.ToSyncSms.Add(
            new KeyValuePair<DateTime, SmsStatus>(DateTime.Now, new SmsStatus
            {
               SmsNumber = "123456789",
               Synced = false,
               SmsText =
                  "SCALE 123456789012345 DAY 999 31.12.9999 11:59:59PM Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999"
            }));
         settings.ToSyncSms.Add(
            new KeyValuePair<DateTime, SmsStatus>(DateTime.Now, new SmsStatus
            {
               SmsNumber = "234567891",
               Synced = false,
               SmsText =
                  "SCALE 123456789012345 DAY 999 31.12.9999 11:59:59PM FEMALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999 MALES: Cnt 9999 Avg 99.999 Gain -99.999 Sig 9.999 Cv 999 Uni 999"
            }));
         settings.ToSyncSms.Add(
            new KeyValuePair<DateTime, SmsStatus>(DateTime.Now, new SmsStatus
            {
               SmsNumber = "345678912",
               Synced = false,
               SmsText = "7FFFFFFF 999 12/31/9999 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999"
            }));
         settings.ToSyncSms.Add(
            new KeyValuePair<DateTime, SmsStatus>(DateTime.Now, new SmsStatus
            {
               SmsNumber = "456789123",
               Synced = false,
               SmsText =
                  "7FFFFFFF 999 12/31/9999 99999 99.999 -99.999 9.999 99.9 99.9 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 9999999 9999999 99.999"
            }));
         settings.ToSyncSms.Add(
            new KeyValuePair<DateTime, SmsStatus>(DateTime.Now, new SmsStatus
            {
               SmsNumber = "456789123",
               Synced = false,
               SmsText =
                  "7FFFFFFF 997 12/29/9999 99999 99.999 -99.999 9.999 99.9 99.9 99999 99.999 -99.999 9.999 99.9 99.9 - - - -"
            }));
         settings.ToSyncSms.Add(
            new KeyValuePair<DateTime, SmsStatus>(DateTime.Now, new SmsStatus
            {
               SmsNumber = "456789123",
               Synced = false,
               SmsText =
                  "7FFFFFFF 998 12/30/9999 99999 99.999 -99.999 9.999 99.9 99.9 99999 99.999 -99.999 9.999 99.9 99.9 -999.99 - 9999999 -"
            }));
      }

      [TestMethod]
      public void Sync_WhenSettingsOk()
      {
         foreach (var sms in settings.ToSyncSms)
         {
            SmsData data;
            if (SmsDecode.Decode(sms.Value.SmsText, out data))
            {
               data.PhoneNumber = sms.Value.SmsNumber;
               Assert.AreEqual(HttpStatusCode.OK,HttpSender.SendMessage(JsonConvert.SerializeObject(data), settings.Name, settings.Password,
                 settings.ServerUrl + CONTROLLER_PATH));
            }
         }
      }
   }
}
