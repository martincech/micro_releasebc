﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kinetis;
using Kinetis.JtagKey;
using Kinetis.JtagKey.JtagKey;
using Kinetis.RomWriter;

namespace KinetisLoader
{
   public partial class KinetisLoader : Form
   {
      private const float SYSTEM_CLOCK_MHZ = 3.0f;
      private readonly HexCodec hex = new HexCodec();
      private bool hexLoaded;
      private CancellationTokenSource cancellationTokenSource;
      private Cursor cursorBackup;
      private bool ezConnected;

      private Flash flashDevice;
      private bool done;

      //------------------------------------------------------------------------------
      //  Constructor
      //------------------------------------------------------------------------------

      public KinetisLoader()
      {
         InitializeComponent();
         RefreshButton_Click(this, null);
         cursorBackup = Cursor.Current;
         cancellationTokenSource = new CancellationTokenSource();
         flashDevice = new Flash(new EzPort(2), this.TextBox, this.ProgressBar);

         flashDevice.OnExecutionStart += FlashDeviceOnExecutionStart;
         flashDevice.OnExecutionStop += FlashDeviceOnExecutionStop;
      }

      public bool EzConnected
      {
         get { return ezConnected; }
         set { ezConnected = value; }
      }

      void FlashDeviceOnExecutionStop(object sender, EventArgs e)
      {
         done = true;
         EnDisControl(CancelButton, false);
         EnDisControls(new Control[]
               {
                  OpenFileButton, EraseButton, WriteButton, RefreshButton, DeviceComboBox,
                  MemoryReadButton, CheckBlankRom,EzPortConnectedButton
               }, true);
      }

      void FlashDeviceOnExecutionStart(object sender, EventArgs e)
      {
        done = false;
        EnDisControls(new Control[]
         {
            OpenFileButton, EraseButton, WriteButton, RefreshButton, DeviceComboBox,
            MemoryReadButton, CheckBlankRom, EzPortConnectedButton
         }, false);
        EnDisControl(CancelButton, true);
      }

      //------------------------------------------------------------------------------
      //  Open file
      //------------------------------------------------------------------------------

      private void OpenFileButton_Click(object sender, EventArgs e)
      {
         if (openFileDialog.ShowDialog() != DialogResult.OK)
         {
            return;
         }

         TextBox.Clear();

         try
         {
            hex.Decode(openFileDialog.FileName);
         }
         catch
         {
            WriteMessage(Properties.Resources.InvalidFileFormat);
            hexLoaded = false;
            return;
         }

         hexLoaded = true;
         WriteMessage(Properties.Resources.Loaded + openFileDialog.FileName);
      }

      //------------------------------------------------------------------------------
      //  Write
      //------------------------------------------------------------------------------
      private void WriteButton_Click(object sender, EventArgs e)
      {
         if (!IsEzConnected())
         {
            return;
         }

         if (!hexLoaded)
         {
             WriteMessage(Properties.Resources.SelectFile);
            return;
         }
        
         flashDevice.ExecuteOperation(flash =>
         {
            try
            {
               if (!flash.Program(hex.data, true, SYSTEM_CLOCK_MHZ))
               {
                  throw new Exception(Properties.Resources.ProgramingFailed);
               }
            }
            catch (Exception ex)
            {
               TextBox.Text += ex.Message + Environment.NewLine;
            }
         }, DeviceComboBox.SelectedIndex, cancellationTokenSource);
      }

      private void EraseButton_Click(object sender, EventArgs e)
      {
         if (!IsEzConnected())
         {
            return;
         }
         flashDevice.ExecuteOperation(flash =>
         {
            try
            {
               flash.EraseAll();
            }
            catch (Exception ex)
            {
               TextBox.Text += ex.Message + Environment.NewLine;
            }
         }, DeviceComboBox.SelectedIndex,cancellationTokenSource);
      }

      private void CancelButton_Click(object sender, EventArgs e)
      {
         cancellationTokenSource.Cancel();
      }


      private void RefreshButton_Click(object sender, EventArgs e)
      {
         EnDisControls(new Control[] {OpenFileButton, EraseButton, WriteButton, RefreshButton, DeviceComboBox,
            MemoryReadButton, CheckBlankRom, EzPortConnectedButton}, false);
         var devices = new List<string>();
         try
         {
            devices = AbstractSpi.ListUp();
         }
         catch (Exception)
         {
         }

         DeviceComboBox.DataSource = devices;
         if (devices.Any())
         {
            if (DeviceComboBox.SelectedIndex >= devices.Count)
            {
               DeviceComboBox.SelectedIndex = 0;
            }
            NoConnectedLabel.Visible = false;
         }
         else
         {
            NoConnectedLabel.Visible = true;
            DeviceComboBox.SelectedIndex = -1;
         }
         EnDisControls(new Control[] { OpenFileButton, EraseButton, WriteButton, RefreshButton, DeviceComboBox,
            MemoryReadButton, CheckBlankRom, EzPortConnectedButton}, true);
      }

      private void FlashOnProgramming(object sender, bool b)
      {
         if (b)
         {
            Invoke(new Action(() =>
            {
               cursorBackup = Cursor;
               Cursor = Cursors.WaitCursor;
            }));
         }
         else
         {
            Invoke(new Action(() => { Cursor = cursorBackup; }));
         }
      }

      /// <summary>
      /// Enable/Disable single control button in multiplatform manner.
      /// </summary>
      private static void EnDisControl(Control button, bool value)
      {
         EnDisControls(new[] {button}, value);
      }

      /// <summary>
      /// Enable/Disable multiple control buttons in multiplatform manner.
      /// </summary>
      private static void EnDisControls(IEnumerable<Control> buttons, bool value)
      {
         foreach (var button in buttons)
         {
            var button1 = button;
            if (button.InvokeRequired)
            {
               button1.Invoke(new Action<bool>(b => button1.Enabled = b), new object[] {value});
            }
            else
            {
               button1.Enabled = value;
            }
         }
      }

      private void ReadButton_Click(object sender, EventArgs e)
      {
         if (!IsEzConnected())
         {
            return;
         }

         flashDevice.ExecuteOperation(flash =>
         {
            try
            {
               byte[] memory;
               var res = flash.ReadMemory(out memory, SYSTEM_CLOCK_MHZ);
               Invoke(new Action(() =>
               {
                  if (res)
                  {
                     saveFileDialog.Filter = "Binary files (*.bin)|*.bin";
                     saveFileDialog.AddExtension = true;
                     if (saveFileDialog.ShowDialog() != DialogResult.OK)
                     {
                        return;
                     }

                     TextBox.Clear();
                     try
                     {
                        var file = new FileStream(saveFileDialog.FileName, FileMode.Create);
                        file.Write(memory, 0, memory.Length);
                        file.Close();
                     }
                     catch (Exception)
                     {
                        WriteMessage(Properties.Resources.CantWrite + saveFileDialog.FileName);
                        return;
                     }
                     WriteMessage(Properties.Resources.MemImageWriten + saveFileDialog.FileName);
                  }
                  else
                  {
                     WriteMessage(Properties.Resources.MemErorReading);
                  }
               }));
            }
            catch (Exception ex)
            {
               if (InvokeRequired)
               {
                  Invoke(new Action(() => { TextBox.Text += ex.Message + Environment.NewLine; }));
               }
               else
               {
                  TextBox.Text += ex.Message + Environment.NewLine;
               }
            }
         }, DeviceComboBox.SelectedIndex, cancellationTokenSource);
      }

      private void CheckBlankRom_Click(object sender, EventArgs e)
      {
         if (!IsEzConnected())
         {
            return;
         }

         flashDevice.ExecuteOperation(flash =>
         {
            bool blank;
            try
            {
               blank = flash.IsBlanROM();
            }
            catch (Exception ex)
            {
               Invoke(new Action(() =>
               {
                  TextBox.Text += ex.Message + Environment.NewLine;
               }));
               return;
            }

            Invoke(new Action(() =>
            {
               TextBox.Text += blank ?  Properties.Resources.FlashIsEmpty : Properties.Resources.FlashIsNotEmpty;
            }));
         }, DeviceComboBox.SelectedIndex, cancellationTokenSource);
      }

      private void button1_Click(object sender, EventArgs e)
      {
         if (!IsEzPortConnected())
         {
            WriteMessage(Properties.Resources.EzPortNotConnected);
         }
         else
         {
            WriteMessage(Properties.Resources.EzPortConnected);
         }

      }

      private bool IsEzConnected()
      {
         if (!IsEzPortConnected())
         {
            WriteMessage(Properties.Resources.EzPortNotConnected);
            return false;
         }
         return true;
      }

      private void WriteMessage(string message)
      {
         TextBox.Text += message + Environment.NewLine;
      }

      private bool IsEzPortConnected()
      {
         flashDevice.Execute(flash =>
         {
            EzConnected = flash.IsConnected;
         }, DeviceComboBox.SelectedIndex, ref cancellationTokenSource);

         return ezConnected;
      }
   }
}
