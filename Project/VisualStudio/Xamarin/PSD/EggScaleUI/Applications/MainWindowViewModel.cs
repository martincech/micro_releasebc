﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Desktop.Wpf.Applications;
using Utilities.Observable;

namespace EggScaleUI.Applications
{
   public class MainWindowViewModel : ObservableObject
   {
      #region Private Fields

      private ObservableCollection<string> _log;
      private string _rawCommandData;
      private string _comPort;
      private double _currentWeight;
      private bool _isConnect;
      private int _baudRate;

      private ICommand _sendCommand;
      private ICommand _clearLogCommand;
      private ICommand _connectCommand;

      private Uart.Uart _uart;

      #endregion

      #region Public intefaces

      public MainWindowViewModel()
      {        
         Log = new ObservableCollection<string>();
         //Log.CollectionChanged += LogOnCollectionChanged;

         PropertyChanged += OnPropertyChanged;

         BaudRate = Uart.Uart.DEFAULT_BAUD_RATE;
      }

      public ObservableCollection<string> Log { get { return _log; } set { SetProperty(ref _log, value); } }
      public string RawCommandData { get { return _rawCommandData; } set { SetProperty(ref _rawCommandData, value); } }
      public string ComPort { get { return _comPort; } set { SetProperty(ref _comPort, value); } }
      public double CurrentWeight { get { return _currentWeight; } set { SetProperty(ref _currentWeight, value); } }
      public bool IsConnect { get { return _isConnect; } set { SetProperty(ref _isConnect, value); } }

      public int BaudRate
      {
         get
         {
            return _baudRate;
         }
         set
         {
            if (value < Uart.Uart.DEFAULT_BAUD_RATE || value > Uart.Uart.MAXIMUM_BAUD_RATE)
            {
               MessageBox.Show(string.Format("Baud rate must be in range: <{0};{1}>", Uart.Uart.DEFAULT_BAUD_RATE, Uart.Uart.MAXIMUM_BAUD_RATE));
               return;
            }
            SetProperty(ref _baudRate, value);
         }
      }

      #region Commands

      public ICommand SendCommand
      {
         get
         {
            if (_sendCommand == null)
            {
               _sendCommand = new RelayCommand<string>(
                  c =>
                  {
                     if (c != null)
                     {  // specific command
                        Log.Add("SEND command: " + c);

                        if (c.Equals("CurrentWeight"))
                        {
                           CurrentWeight = _uart.CurrentWeight();
                        }
                        else
                        {
                           MessageBox.Show("Not implemented command!");
                        }
                     }
                     else
                     {  // raw command
                        if (RawCommandData == null || RawCommandData.Equals("")) return;

                        Log.Add("SEND Raw data: " + RawCommandData);
                        SendRawCommand();  
                     }
                  },
                  c => IsConnect
                  );
            }
            return _sendCommand;
         }
      }

      public ICommand ClearLogCommand
      {
         get
         {
            if (_clearLogCommand == null)
            {
               _clearLogCommand = new RelayCommand(
                  () =>
                  {
                     Log.Clear();
                  },
                  () => true//Log.Any()
                  );
            }
            return _clearLogCommand;
         }
      }

      public ICommand ConnectCommand
      {
         get
         {
            if (_connectCommand == null)
            {
               _connectCommand = new RelayCommand(
                  () =>
                  {
                     if (IsConnect)
                     {  // disconnect
                        ScaleDisconnect();
                        Log.Add("Scale: DISCONNECT");
                     }
                     else
                     {  // connect to scale
                        if (ScaleConnect())
                        {
                           Log.Add("Scale: CONNECT");
                        }
                     }
                  },
                  () => true
                  );
            }
            return _connectCommand;
         }
      }

      #endregion

      #endregion

      #region Private helpers

      private void ScaleDisconnect()
      {
         _uart.DataRead -= UartOnDataRead;
         _uart.Close();

         IsConnect = false;
      }

      private bool ScaleConnect()
      {
         int port;
         if (!int.TryParse(ComPort, out port) || port < 1 || port > byte.MaxValue)
         {
            MessageBox.Show("Invalid com port number!");
            return false;
         }

         try
         {
            Mouse.OverrideCursor = Cursors.Wait;
            _uart = new Uart.Uart("COM" + port, BaudRate);
            _uart.DataRead += UartOnDataRead;

            IsConnect = true;
            return true;
         }
         catch (Exception e)
         {
            MessageBox.Show(e.Message);
            return false;
         }
         finally
         {
            Mouse.OverrideCursor = null;
         }
      }

      private void UartOnDataRead(object sender, string s)
      {
         if (s == "") return;

         var dispatcher = Application.Current.Dispatcher;
         dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate
         {
            Log.Add("Receive: " + s);
         });
      }

      private void LogOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
      {
         ((RelayCommand)ClearLogCommand).RaiseCanExecuteChanged();
      }

      private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if (e.PropertyName.Equals("IsConnect"))
         {
            UpdateCommands();
         }
      }

      private void UpdateCommands()
      {
         ((RelayCommand<string>)SendCommand).RaiseCanExecuteChanged();
      }

      private void SendRawCommand()
      {
         var cmd = RawCommandData;
         RawCommandData = "";

         var reply = _uart.CallRaw(cmd);
         Log.Add("Raw response: " + reply);
      }

      #endregion
   }
}
