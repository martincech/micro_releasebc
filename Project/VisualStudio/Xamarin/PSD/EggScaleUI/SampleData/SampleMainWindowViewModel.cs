﻿using EggScaleUI.Applications;

namespace EggScaleUI.SampleData
{
   public class SampleMainWindowViewModel : MainWindowViewModel
   {
      public SampleMainWindowViewModel()
      {
         Log.Add("SEND: current weight");
         Log.Add("RECEIVE: 20g");

         ComPort = "5";
         CurrentWeight = 28;
      }
   }
}
