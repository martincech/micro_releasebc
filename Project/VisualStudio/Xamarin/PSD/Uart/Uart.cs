﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using PSD.Core.EggScale;
using PSD.Core.EggScale.Interfaces;

namespace Uart
{
   public class Uart : IEggScale
   {
      #region Private fields

      private bool _continue;
      private SerialPort _serialPort;

      public const int DEFAULT_BAUD_RATE = 600;
      public const int MAXIMUM_BAUD_RATE = 9600;
      private IEggScale _protocol;

      public event EventHandler<string> DataRead;

      #endregion

      public Uart(string comPort, int baudRate = DEFAULT_BAUD_RATE)
      {
         if (baudRate < DEFAULT_BAUD_RATE || baudRate > MAXIMUM_BAUD_RATE)
         {
            throw new ArgumentOutOfRangeException(string.Format("Baudrate is out of range <{0};{1}>", DEFAULT_BAUD_RATE, MAXIMUM_BAUD_RATE));
         }
         var readThread = new Thread(Read);

         // Create a new SerialPort object with default settings.
         _serialPort = new SerialPort(comPort, baudRate, Parity.None, 8, StopBits.One)
         {
            Handshake = Handshake.RequestToSend,
            RtsEnable = true,
            DtrEnable = true,
            //ReadTimeout = 5000,
            //WriteTimeout = 5000
         };

         Connect();
         _continue = true;
         _protocol = new Protocol(_serialPort.BaseStream, _serialPort.BaseStream);
         readThread.Start();
              

         //readThread.Join();
         //_serialPort.Close();
      }

      public void Read()
      {
         while (_continue)
         {
            try
            {
               var message = _serialPort.ReadExisting();
               //var message = _serialPort.ReadLine();
               InvokeEvent(message);
            }
            catch (TimeoutException) { }
         }
      }

      /// <summary>
      /// Stop reading and close serial port.
      /// </summary>
      public void Close()
      {
         _continue = false;
         _serialPort.Close();
      }

      #region Private helpers

      /// <summary>
      /// Open serial port.
      /// </summary>
      private void Connect()
      {
         if (_serialPort.IsOpen) return;

         var ports = SerialPort.GetPortNames();
         if (!ports.Contains(_serialPort.PortName))
         {
            throw new ArgumentException("Number of COM port doesn't exist!");
         }

         try
         {
            _serialPort.Open();
         }
         catch (Exception)
         {         
            Thread.Sleep(200);
            Connect();          
         }
      }

      private void InvokeEvent(string data)
      {
         if (DataRead != null)
         {
            DataRead(this, data);
         }
      }

      #endregion

      public byte[] CallRaw(string command)
      {
         //return ((Protocol) _protocol).CallRaw(command);
         return null;
      }

      #region Implementation of IEggScale

      public void Tare()
      {
         _protocol.Tare();
      }

      public void Backlight()
      {
         _protocol.Backlight();
      }

      public double CurrentWeight()
      {
         //return _protocol.CurrentWeight();
         return -1;
      }

      public void Calibration()
      {
         _protocol.Calibration();
      }

      public void Count()
      {
         _protocol.Count();
      }

      public void ChangeUnit()
      {
         _protocol.ChangeUnit();
      }

      #endregion


      public void Start()
      {
         throw new NotImplementedException();
      }

      public void Stop()
      {
         throw new NotImplementedException();
      }

      public event EventHandler<CommandArgs> EggWeighted;
   }
}
