using System.Globalization;
using System.Linq;
using Android.App;
using Android.Views;
using Android.Widget;
using PSD.Core;
using PSD.Core.Enums;
using PSD.Core.Helpers;
using PSD.Core.Models;
using PSD.Droid.ViewHolders;

namespace PSD.Droid.Adapters
{
   public class CageRecordAdapter : BaseAdapter<CageRecord>
   {
      #region Fields and properties

      private Cage cage;
      private readonly Activity context;

      public int CageRecordIndex { get; private set; }

      #endregion

      public CageRecordAdapter(Activity activity, Cage cage)
      {
         this.cage = cage;
         context = activity;
      }

      public void SetCage(Cage newCage)
      {
         cage = newCage;
      }

      #region Overrides of BaseAdapter


      public override long GetItemId(int position)
      {
         return (long) this[position].Type;
      }


      public override int Count
      {
         get { return cage.Records.Count(); }
      }

      public override CageRecord this[int position]
      {
         get { return cage.Records.ElementAt(position); }
      }


      /// <param name="position">To be added.</param><param name="convertView">To be added.</param><param name="parent">To be added.</param>
      /// <summary>
      /// To be added.
      /// </summary>
      /// <returns>
      /// To be added.
      /// </returns>
      /// <remarks>
      /// To be added.
      /// </remarks>
      public override View GetView(int position, View convertView, ViewGroup parent)
      {
         CageListParameterViewHolder holder = null;
         var view = convertView;
         if (view != null)
         {
            holder = view.Tag as CageListParameterViewHolder;
         }
         if (holder == null)
         {
            view = context.LayoutInflater.Inflate(Resource.Layout.CageListParameter, null);
            holder = new CageListParameterViewHolder
            {
               Parameter = view.FindViewById<TextView>(Resource.Id.parameterText),
               Count = view.FindViewById<TextView>(Resource.Id.countText)
            };
            view.Tag = holder;
         }

         var cageRecord = this[position];      
         holder.Parameter.Text = cageRecord.Type.ConvertToString(cageRecord.Weight);          
         holder.Count.Text = "";       
         if (cageRecord.Type != ParameterE.ParameterDead)
         {
            holder.Count.Text = cageRecord.Count.ToString(CultureInfo.InvariantCulture);
         }
       
         view.Touch += (sender, args) => ParameterTouched(args, position);
         return view;
      }

      private void ParameterTouched(View.TouchEventArgs e, int position)
      {
         CageRecordIndex = position;
         e.Handled = false;
      }

      #endregion
   }
}