using System;
using System.Globalization;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using PSD.Core;
using PSD.Core.Enums;
using PSD.Core.Helpers;
using PSD.Core.Models;
using PSD.Core.ViewModels;
using PSD.Droid.Activities;
using PSD.Droid.Helpers;
using PSD.Droid.ViewHolders;
using Utilities.IOC;
// ReSharper disable InconsistentNaming

namespace PSD.Droid.Adapters
{
   public class CagesAdapter : BaseAdapter<Cage>
   {
      #region Fields and properties

      private Cage entryCage;
      private readonly Cage alignmentCage = new Cage();
      private readonly CagesViewModel viewModel = IocContainer.Resolve<CagesViewModel>();
      private readonly CagesListActivity context;
      private readonly ListView adapterView;
      private int firstCageViewHeight;                // first cage serves as empty padding     
      private long rate = DateTime.Now.Ticks;         // variable to limit number of event - keypress down
      private const long MIN_TICKS_BETWEEN_EVENT = 3000000;
      private readonly DeleteCageContent deleteContent;
      private bool isEditable;                        // If list is redraw, cagenumber will lost focus. This flag serves to reassigned focus

      // Selected view and its index
      private View selectedItem;
      private int selectedItemIndex = -1;
      private SelectedCageE selectedCage = SelectedCageE.Unselected;

      // Last edited cage number and its index
      // To check if cagenumber was changed. Direct access to cagenumber give old, non actual value.
      private int indexLastEditCageNumber = -1;
      private int lastEditCageNumber = -1;   

      public event EventHandler<bool> ShowSoftwareKeyboard;

      private int EntryCageIndex { get { return Count - 1; } }
      private bool CanCageNumberUse(int id)
      {
         return !viewModel.Cages.ToList().Exists(i => i.CageNumber == id);
      }

      #endregion

      public CagesAdapter(CagesListActivity context, ListView adapterView)
      {
         this.context = context;
         this.adapterView = adapterView;       
         AddInsertingCage();

         deleteContent = new DeleteCageContent(context.Resources.DisplayMetrics.WidthPixels);
         adapterView.Touch += AdapterViewOnTouch; 
      }

      #region Private helpers

      /// <summary>
      /// Add new inserting cage at the end of list.
      /// </summary>
      private void AddInsertingCage()
      {
         entryCage = new Cage { CageNumber = viewModel.FreeCageNumber };
         selectedCage = SelectedCageE.EntryCage;
      }

      /// <param name="cageNumber">cage number</param>
      private void AddInsertingCage(int cageNumber)
      {
         entryCage = new Cage { CageNumber = cageNumber };
         selectedCage = SelectedCageE.EntryCage;
      }

      /// <summary>
      /// Deselect previous active item and set current item to selected 
      /// </summary>
      /// <param name="view"></param>
      /// <param name="position"></param>
      private void SetItemSelected(View view, int position)
      {
         if (this[position] == entryCage)
         {  // last item is new cage that can't be selected
            selectedCage = SelectedCageE.EntryCage;
            DeselectItem();
            return;
         }

         if (selectedItem != null && selectedItem != view)
         {
            selectedItem.SetBackgroundColor(Color.ParseColor(AppColors.BACKGROUND_DEFAULT));
            adapterView.InvalidateViews();
         }
         selectedItem = view;
         selectedItemIndex = position;
         selectedCage = SelectedCageE.Cage;
         view.SetBackgroundColor(Color.ParseColor(AppColors.BACKGROUND_SELECTED));

         var newCageView = GetView(EntryCageIndex, null, null);
         newCageView.SetBackgroundColor(Color.ParseColor(AppColors.BACKGROUND_NEWCAGE_INACTIVE));        
         NotifyDataSetChanged();         
      }

      /// <summary>
      /// Validate selected item or new cage number
      /// with last edit number of cage.
      /// </summary>
      /// <returns>true - cage number is valid and can be used</returns>
      private bool CageNumberValidation(int position = -1)
      {
         var index = selectedItem == null ? EntryCageIndex : selectedItemIndex;
         if (position != -1 && index == position) return true; // validation performed for current editText (cage number) is cancelled
         if (indexLastEditCageNumber != index || lastEditCageNumber == this[index].CageNumber)
         {
            indexLastEditCageNumber = -1;
            lastEditCageNumber = -1;
            return true;
         }

         if (CanCageNumberUse(lastEditCageNumber))
         {  //can number use
            this[index].CageNumber = lastEditCageNumber;        
            return true;
         }         
         var view = GetView(index, null, null);
         var cageNumber = ((CageListItemViewHolder)view.Tag).CageNumber;
         cageNumber.Text = this[index].CageNumber.ToString(CultureInfo.InvariantCulture);

         Toast.MakeText(context, lastEditCageNumber + " " + Application.Context.Resources.GetString(Resource.String.MsgNumberIsUsed), ToastLength.Long).Show();

         CageNumberClicked(null, view, cageNumber, index);
         indexLastEditCageNumber = -1;
         lastEditCageNumber = -1;            
         return false;
      }

      /// <summary>
      /// Set selection to item at list and scroll to bottom.
      /// </summary>
      /// <param name="position">position at list</param>
      /// <param name="height">item's height</param>
      private void SetSelectionFromTop(int position, int height)
      {
         var listHeight = adapterView.Height;
         adapterView.SetSelectionFromTop(position, listHeight - height);
      }    

      /// <summary>
      /// Add or update parameter of cage.
      /// </summary>
      /// <param name="parameter">updating parameter</param>
      /// <param name="count">value</param>
      private void AddParameter(ParameterE parameter, int count)
      {
         // get selected or new cage       
         var index = selectedItemIndex == -1 ? EntryCageIndex : selectedItemIndex;
         var selCage = this[index];

         //Check if cage contains already this parameter
         var record = new CageRecord
         {
            Type = parameter,
            Count = count
         };

         var item = selCage.All().FirstOrDefault(i => i.Type == parameter);
         if (item != null)
         {  //edit count        
            if (parameter != ParameterE.ParameterOk)
            {
               item.Count++;             
            }
            else
            {
               item.Count = count;
            }
            selCage.Update(item);
         }
         else
         {
            selCage.Add(record);
            if (selCage == entryCage  && 
               (parameter == ParameterE.ParameterOk || parameter == ParameterE.ParameterDead))
            {         
               EntryCageAddToList();
               return;
            }
         }

         adapterView.Adapter = adapterView.Adapter;       
         adapterView.RequestFocusFromTouch();       
         var v = GetView(index, null, null);
         SetSelectionFromTop(index, ((CageListItemViewHolder)v.Tag).CageContent.SetListViewHeightBasedOnItems());
         NotifyDataSetChanged();
      }

      /// <summary>
      /// Navigation arrows service.
      /// </summary>
      /// <param name="keycode">Arrow code</param>
      /// <param name="switchToCageNumber">flag if focus will be set to cage number (true) or content</param>
      private void DpadClicked(Keycode keycode, bool switchToCageNumber = false)
      {
         int newItemIndex;
         switch (keycode)
         {
            case Keycode.DpadDown:
               newItemIndex = selectedItemIndex + 1;
               if (selectedItemIndex == -1) return;
               if (newItemIndex > Count) return;
               break;
            case Keycode.DpadUp:
               newItemIndex = selectedItemIndex - 1;
               if (selectedItemIndex == -1)
               {
                  newItemIndex = Count - 2;
               }
               if (newItemIndex < 1) return;
               break;
            case Keycode.DpadLeft:
               newItemIndex = selectedItemIndex;
               if (selectedItemIndex == -1)
               {
                  newItemIndex = Count - 1;
               }
               switchToCageNumber = true;
               break;
            case Keycode.DpadRight:
               newItemIndex = selectedItemIndex;
               if (selectedItemIndex == -1)
               {
                  newItemIndex = Count - 1;
               }
               break;
            default:
               return;
         }

         var view = GetView(newItemIndex, null, null);
         if (switchToCageNumber)
         {
            int.TryParse(((CageListItemViewHolder) view.Tag).CageNumber.Text, out lastEditCageNumber);
            indexLastEditCageNumber = newItemIndex;
            CageNumberClicked(null, view, ((CageListItemViewHolder)view.Tag).CageNumber, newItemIndex);
         }
         else
         {
            CageContentClicked(null, view, (CageListItemViewHolder)view.Tag, newItemIndex);
         }
      }       

      /// <summary>
      /// Add entry cage to list of cages.
      /// </summary>
      private void EntryCageAddToList()
      {
         if (selectedItemIndex != -1) return;

         if (viewModel.AddCage(entryCage))
         {
            AddInsertingCage(entryCage.CageNumber + 1);
         }
         adapterView.Adapter = adapterView.Adapter;
         adapterView.RequestFocusFromTouch();
         var v = GetView(EntryCageIndex, null, null);
         SetSelectionFromTop(EntryCageIndex, ((CageListItemViewHolder)v.Tag).CageContent.SetListViewHeightBasedOnItems());
         NotifyDataSetChanged();
      }

      /// <summary>
      /// Delete parameter from cage.
      /// </summary>
      /// <param name="position">cage index at list</param>
      /// <param name="cageParameterIndex">parameter index</param>
      private void DeleteParameter(int position, int cageParameterIndex)
      {
         var cage = this[position];
         if (!cage.Records.Any()) return;

         AlertDialog(Application.Context.Resources.GetString(Resource.String.ConfirmDeleteParameter), delegate
         {
            var record = cage.Records.ElementAt(cageParameterIndex);
            this[position].Delete(record);
            NotifyDataSetChanged();
         });
         deleteContent.Position = -1;
      }

      /// <summary>
      /// Delete cage.
      /// </summary>
      /// <param name="position">cage index at list</param>
      private void DeleteCage(int position)
      {
         AlertDialog(Application.Context.Resources.GetString(Resource.String.ConfirmDeleteCage), delegate
         {
            viewModel.DeleteCage(this[position]);

            if (selectedItemIndex == position)
            {
               selectedCage = SelectedCageE.Unselected;
               selectedItem = null;
               selectedItemIndex = -1;
            }

            NotifyDataSetChanged();
         });
         deleteContent.Position = -1;
      }

      private void AlertDialog(string msg, EventHandler<DialogClickEventArgs> action)
      {
         var dialog = new AlertDialog.Builder(context);
         dialog.SetPositiveButton(Application.Context.Resources.GetString(Resource.String.ButtonOK), action);
         dialog.SetNegativeButton(Application.Context.Resources.GetString(Resource.String.Cancel), delegate { });
         dialog.SetMessage(msg);
         dialog.Show();
      }

      #endregion

      #region Event service

      private void AdapterViewOnTouch(object sender, View.TouchEventArgs e)
      {
         if (e.Event.Action == MotionEventActions.Up)
         {  // check if user want delete cage/parameter or only scroll by list
            var currentYPos = e.Event.RawY;
            var currentXPos = e.Event.RawX;
          
            if (deleteContent.CanDelete(currentXPos, currentYPos))
            {
               switch (deleteContent.Type)
               {
                  case DeleteTypeE.Cage:
                     DeleteCage(deleteContent.Position);
                     break;
                  case DeleteTypeE.Parameter:
                     DeleteParameter(deleteContent.Position, deleteContent.ParameterIndex);
                     break;
               }            
            } 
         }
         e.Handled = false;
      }

      private void CageNumberClicked(View.TouchEventArgs args, View view, EditText cageNumber, int position)
      {        
         if (args != null)
         {  // Touch detect
            if (args.Event.Action == MotionEventActions.Down)
            {
               deleteContent.Type = DeleteTypeE.Cage;
               deleteContent.PositionX = args.Event.RawX;
               deleteContent.PositionY = args.Event.RawY;
               deleteContent.Position = position;
            }
            if (args.Event.Action == MotionEventActions.Up)
            {
               var currentXPos = args.Event.RawX;
               if (deleteContent.CanDelete(currentXPos) && position != Count - 1)
               {  //Delete cage
                  DeleteCage(deleteContent.Position);               
                  return;
               }
            }  // End of touch process for deleting

            if (args.Event.Action != MotionEventActions.Up || !CageNumberValidation(position)) return;
         }
         cageNumber.RequestFocusFromTouch();
         SetItemSelected(view, position);
         InvokeEvent(true);
         isEditable = true;
         NotifyDataSetInvalidated();
         var height = view.Height != 0 ? view.Height : ((CageListItemViewHolder)view.Tag).CageContent.SetListViewHeightBasedOnItems();
         SetSelectionFromTop(position, height);
      }

      private void CageContentClicked(View.TouchEventArgs args, View view, CageListItemViewHolder holder, int position)
      {
         if (args != null)
         {  // Touch detect
            if (args.Event.Action == MotionEventActions.Down)
            {
               deleteContent.Type = DeleteTypeE.Parameter;
               deleteContent.PositionX = args.Event.RawX;
               deleteContent.PositionY = args.Event.RawY;
               deleteContent.Position = position;
               var cageParameterIndex = ((CageRecordAdapter) holder.CageContent.Adapter).CageRecordIndex;
               deleteContent.ParameterIndex = cageParameterIndex;
            }
            if (args.Event.Action == MotionEventActions.Up)
            {
               var currentXPos = args.Event.RawX;
               if (deleteContent.CanDelete(currentXPos) && this[deleteContent.Position].Records.Any())
               {  //Delete parameter             
                  DeleteParameter(deleteContent.Position, deleteContent.ParameterIndex);              
                  return;
               }
            }  // End of touch process for deleting 

            if (args.Event.Action != MotionEventActions.Up || !CageNumberValidation()) return;
         }

         SetItemSelected(view, position);
         InvokeEvent(true);
         isEditable = false;

         if (holder.Layout == null)
         {
            holder.Layout = context.FindViewById<RelativeLayout>(Resource.Id.cageListItemRelativeLayout);
         }
         holder.Layout.RequestFocusFromTouch();

         NotifyDataSetInvalidated();
         var height = view.Height != 0 ? view.Height : ((CageListItemViewHolder)view.Tag).CageContent.SetListViewHeightBasedOnItems();
         SetSelectionFromTop(position, height);
      }    

      private void CageNumberKeyPressed(View.KeyEventArgs e, CageListItemViewHolder holder)
      {
         var now = DateTime.Now.Ticks;
         if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.DpadRight && isEditable &&
             holder.CageNumber.SelectionStart == holder.CageNumber.Text.Length)
         {  // change focus from cage number to cage content
            DpadClicked(Keycode.DpadRight);
         }
         else if (e.Event.Action == KeyEventActions.Down && isEditable &&
                 (e.KeyCode == Keycode.DpadUp || e.KeyCode == Keycode.DpadDown))
         {  // switch to cage above/below
            if (now - rate > MIN_TICKS_BETWEEN_EVENT)
            {
               var keycode = Keycode.DpadDown;
               if (e.KeyCode == Keycode.DpadUp)
               {
                  keycode = e.KeyCode;
               }
               DpadClicked(keycode, true);
               rate = DateTime.Now.Ticks;   
            }
            else
            {
               e.Handled = true;
            }
         }
         else if (e.Event.Action == KeyEventActions.Up && e.KeyCode == Keycode.Enter)
         {  //change cage number
            int newcageNumber;
            if (!int.TryParse(holder.CageNumber.Text, out newcageNumber)) return;
            
            var index = selectedItemIndex == -1 ? EntryCageIndex : selectedItemIndex;
            // Check if new number is not used
            if (!CanCageNumberUse(newcageNumber))
            {
               var cNumber = this[index].CageNumber.ToString(CultureInfo.InvariantCulture);
               if (!holder.CageNumber.Text.Equals(cNumber))
               {
                  Toast.MakeText(context, lastEditCageNumber + " " + Application.Context.Resources.GetString(Resource.String.MsgNumberIsUsed), ToastLength.Long).Show();
                  SetSelectionFromTop(index, holder.CageContent.Height);

                  // set previous correct cage number and select them
                  holder.CageNumber.Text = cNumber;
                  lastEditCageNumber = this[index].CageNumber;
                  holder.CageNumber.SelectAll();
                  return;
               }
               holder.CageNumber.Text = cNumber;
            }
            var cage = this[index];
            cage.CageNumber = newcageNumber;
            viewModel.UpdateCage(cage);
            isEditable = false;
            DeselectItem();
            NotifyDataSetInvalidated();
            adapterView.SetSelection(EntryCageIndex);
         }
         else
         {
            if (e.Event.Action == KeyEventActions.Up)
            {  // save last edit cage number and index
               indexLastEditCageNumber = selectedItemIndex == -1 ? EntryCageIndex : selectedItemIndex;
               int.TryParse(holder.CageNumber.Text, out lastEditCageNumber);
            }
            e.Handled = false;
         }
      }

      private void LayoutKeyPressed(View.KeyEventArgs e)
      {
         if (e.KeyCode == Keycode.Menu || e.KeyCode == Keycode.Back)
         {
            e.Handled = false;
         }

         // arrow events (keydown)
         var now = DateTime.Now.Ticks;
         if (e.Event.Action == KeyEventActions.Down && now - rate > MIN_TICKS_BETWEEN_EVENT)
         {
            switch (e.KeyCode)
            {
               case Keycode.DpadUp:
                  DpadClicked(Keycode.DpadUp);                 
                  break;
               case Keycode.DpadDown:
                  DpadClicked(Keycode.DpadDown);                
                  break;
            }
            rate = DateTime.Now.Ticks;
         }
           
         // other events (keyup)
         if (e.Event.Action != KeyEventActions.Up) return;
         
         var count = -1;
         var parameter = ParameterE.ParameterOk;
         var isInsertParam = false;

         switch (e.KeyCode)
         {
            case Keycode.Num0:
               count = 0;
               break;
            case Keycode.Num1:
               count = 1;
               break;
            case Keycode.Num2:
               count = 2;
               break;
            case Keycode.Num3:
               count = 3;
               break;
            case Keycode.Num4:
               count = 4;
               break;
            case Keycode.Num5:
               count = 5;
               break;
            case Keycode.Num6:
               count = 6;
               break;
            case Keycode.Num7:
               count = 7;
               break;
            case Keycode.Num8:
               count = 8;
               break;
            case Keycode.Num9:
               count = 9;
               break;
            case Keycode.F1:
            case Keycode.D:
               parameter = ParameterE.ParameterTwin;
               isInsertParam = true;
               break;
            case Keycode.F2:
            case Keycode.K:
               parameter = ParameterE.ParameterCracked;
               isInsertParam = true;
               break;
            case Keycode.F3:
            case Keycode.B:
               parameter = ParameterE.ParameterMembrane;
               isInsertParam = true;
               break;
            case Keycode.F4:
            case Keycode.U:
               parameter = ParameterE.ParameterDead;
               isInsertParam = true;
               break;
            case Keycode.Enter:
               EntryCageAddToList();
               break;
            case Keycode.DpadLeft:
               DpadClicked(Keycode.DpadLeft);
               break;
            default:
               return;
         }
         if (count != -1)
         {
            AddParameter(ParameterE.ParameterOk, count);
         }
         else if (isInsertParam)
         {
            AddParameter(parameter, 1);
         }
      }

      #endregion

      #region Implementation of BaseAdapter

      public override int Count
      {
         // +2 for entry cage and alignment cage
         get { return viewModel.Cages.Count() + 2; }
      }

      public override long GetItemId(int position)
      {
         return this[position].CageNumber;
      }

      public override Cage this[int position]
      {
         get
         {
            if (position == 0)
            {
               return alignmentCage;
            }
            position--;
            if (position >= viewModel.Cages.Count())
            {
               return entryCage;
            }
            return viewModel.Cages.ElementAt(position);
         }
      }

      public override View GetView(int position, View convertView, ViewGroup parent)
      {
         var view = context.LayoutInflater.Inflate(Resource.Layout.CageListItem, parent, false);
         var holder = new CageListItemViewHolder
         {
            CageNumber = view.FindViewById<EditText>(Resource.Id.cageNumber),
            CageContent = view.FindViewById<ListView>(Resource.Id.cageContent),
            DividerHorizontal = view.FindViewById<TextView>(Resource.Id.dividerHorizontal),
            NoParameters = view.FindViewById<TextView>(Resource.Id.NoParameters),
            Layout = context.FindViewById<RelativeLayout>(Resource.Id.cageListItemRelativeLayout)
         };         
         view.Tag = holder;         
         var cage = this[position];

         // Events        
         //holder.CageContent.ItemClick += (sender, e) => CageContentClicked(null, view, holder, position);
         holder.CageContent.Touch += (sender, e) => CageContentClicked(e, view, holder, position);   
         holder.NoParameters.Touch += (sender, args) => CageContentClicked(args, view, holder, position);        
         view.Touch += (sender, args) => CageNumberClicked(args, view, holder.CageNumber, position);
         holder.CageNumber.Touch += (sender, args) => CageNumberClicked(args, view, holder.CageNumber, position);      
         holder.CageNumber.KeyPress += (sender, e) => CageNumberKeyPressed(e, holder);
         if (holder.Layout != null)
         {
            holder.Layout.KeyPress -= view_KeyPress;
            holder.Layout.KeyPress += view_KeyPress;
         }

         if (position == 0)   // first cage is for padding list
         {
            view.SetMinimumHeight(adapterView.Height - firstCageViewHeight);
            view.Visibility = ViewStates.Invisible;
            return view;
         }

         // set content
         // number of the cage    
         holder.CageNumber.Text = cage.CageNumber.ToString(CultureInfo.InvariantCulture);
         
         if (cage == entryCage)
         {  // check number for new cage
            if (!CanCageNumberUse(cage.CageNumber))
            {
               var freeNumber = entryCage.CageNumber;
               while (!CanCageNumberUse(freeNumber))
               {
                  freeNumber++;
               }
               entryCage.CageNumber = freeNumber;
               Toast.MakeText(context, Application.Context.Resources.GetString(Resource.String.SkipNumber), ToastLength.Long).Show();
               holder.CageNumber.Text = entryCage.CageNumber.ToString(CultureInfo.InvariantCulture);
            }

            var color = AppColors.BACKGROUND_NEWCAGE_ACTIVE;
            if (selectedCage != SelectedCageE.EntryCage)
            {
               color = AppColors.BACKGROUND_NEWCAGE_INACTIVE;
            }
            view.SetBackgroundColor(Color.ParseColor(color));

            if (isEditable && selectedItem == null)
            {
               holder.CageNumber.RequestFocusFromTouch();
            }
         }      

         // records in cage
         if (!cage.Records.Any())
         {
            holder.NoParameters.Visibility = ViewStates.Visible;          
            holder.CageContent.Visibility = ViewStates.Gone;
         }
         else
         {
            holder.NoParameters.Visibility = ViewStates.Gone;
            holder.CageContent.Visibility = ViewStates.Visible;
         }

         // if was scrolling with list, background was cancelled and must be set again
         if (selectedItemIndex == position)
         {
            if (isEditable)
            {
               if (lastEditCageNumber != -1)
               {
                  holder.CageNumber.Text = lastEditCageNumber.ToString(CultureInfo.InvariantCulture);
               }
               holder.CageNumber.RequestFocusFromTouch();
            }
            view.SetBackgroundColor(Color.ParseColor(AppColors.BACKGROUND_SELECTED));
         }       

         holder.CageContent.Adapter = new CageRecordAdapter(context, cage);
         var height = holder.CageContent.SetListViewHeightBasedOnItems();
         holder.DividerHorizontal.SetHeight(height); 
         holder.NoParameters.SetHeight(height);
         if (position == 1)
         {
            firstCageViewHeight = height;
         }

         return view;
      }

      private void view_KeyPress(object sender, View.KeyEventArgs e)
      {
         LayoutKeyPressed(e);
      }

      #endregion

      /// <summary>
      /// Deselecting active item at list.
      /// </summary>
      public void DeselectItem()
      {
         if (selectedItem != null)
         {
            selectedItem.SetBackgroundColor(Color.ParseColor(AppColors.BACKGROUND_DEFAULT));
            adapterView.InvalidateViews();
         }
         selectedItem = null;
         selectedItemIndex = -1;
      }

      public void ActivateEntryCage()
      {
         selectedCage = SelectedCageE.EntryCage;
         var view = GetView(EntryCageIndex, null, null);
         CageContentClicked(null, view, (CageListItemViewHolder)view.Tag, EntryCageIndex);
      }

      /// <summary>
      /// Read egg's weight
      /// </summary>
      /// <param name="weight">egg's weight</param>
      public void ReadWeight(double weight)
      {
         // save weight to cage, insert cage to list and move on to new entry cage
         if (selectedCage != SelectedCageE.EntryCage)
         {
            ActivateEntryCage();
         }
         
         var cage = this[EntryCageIndex];
         var rec = new CageRecord
         {
            Type = ParameterE.ParameterOk,
            Count = 1,
            Weight = weight
         };
         cage.Add(rec);
         EntryCageAddToList();
      }

      private void InvokeEvent(bool show)
      {
         if (ShowSoftwareKeyboard != null)
         {
            ShowSoftwareKeyboard(this, show);
         }
      }
   }
}