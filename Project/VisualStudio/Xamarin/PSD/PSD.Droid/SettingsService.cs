using Android.Preferences;
using PSD.Core.Repository.Interfaces;

namespace PSD.Droid
{
   public class SettingsService : ISettings
   {
      public string PickerName { get; set; }       // Picker name
      public string LastFileName { get; set; }     // Last campaign name used for file
      public string LastFileFolder { get; set; }   // Last name used for folder

      // Constants for parsing data
      private const string PICKER_NAME = "PickerName";
      private const string LAST_FILE_NAME = "LastFileName";
      private const string LAST_FILE_FOLDER = "LastFileFolder";

      public SettingsService()
      {
         PickerName = "";
         LastFileFolder = "";
         LastFileName = "";
      }

      public void Save(object context)
      {
         var preferences = PreferenceManager.GetDefaultSharedPreferences((Android.Content.Context)context);
         var editor = preferences.Edit();
         editor.PutString(PICKER_NAME, PickerName);
         editor.PutString(LAST_FILE_NAME, LastFileName);
         editor.PutString(LAST_FILE_FOLDER, LastFileFolder);
         editor.Apply();
      }

      public void Load(object context)
      {
         var preferences = PreferenceManager.GetDefaultSharedPreferences((Android.Content.Context)context);
         PickerName = preferences.GetString(PICKER_NAME, "");
         var defaultName = Program.GetDefaultName();
         var fileDefName = defaultName;
         if (!PickerName.Equals(""))
         {
            fileDefName += "_" + PickerName;
         }
         LastFileName = preferences.GetString(LAST_FILE_NAME, fileDefName);
         LastFileFolder = preferences.GetString(LAST_FILE_FOLDER, defaultName);
      }

      public void Delete(object context)
      {
         var preferences = PreferenceManager.GetDefaultSharedPreferences((Android.Content.Context)context);
         var editor = preferences.Edit();
         editor.Clear();
         editor.Apply();
      }
   }
}