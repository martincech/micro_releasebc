using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using PSD.Core;
using PSD.Core.Repository.Interfaces;

namespace PSD.Droid.Activities
{
   [Activity(Label = "CampaignNameActivity", ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize,
      ScreenOrientation = ScreenOrientation.Portrait)]
   public class CampaignNameActivity : BaseActivity<ISettings>
   {
      private EditText _fileName;

      protected override void OnCreate(Bundle bundle)
      {
         base.OnCreate(bundle);
         SetContentView(Resource.Layout.CampaignName);

         _fileName = FindViewById<EditText>(Resource.Id.campaignNameEditText);
         _fileName.Text = Program.GetUsableFileName();
         var startCampaignButton = FindViewById<Button>(Resource.Id.startCampaignButton);
         var usePredefNameButton = FindViewById<Button>(Resource.Id.predefinedNameButton);
         
         // Events
         usePredefNameButton.Click += UsePredefNameButtonOnClick;
         startCampaignButton.Click += StartCampaignButtonOnClick;
         _fileName.FocusChange += FileNameOnFocusChange;
         _fileName.KeyPress += FileNameOnKeyPress;
      }

      private void FileNameOnKeyPress(object sender, View.KeyEventArgs e)
      {
         if (e.Event.Action == KeyEventActions.Down && (e.KeyCode == Keycode.Enter || e.KeyCode == Keycode.NumpadEnter))
         {  // cancel inserting enter to edit box
            e.Handled = true;
         }
         else
         {
            e.Handled = false;
         }

         if (e.Event.Action == KeyEventActions.Up && (e.KeyCode == Keycode.Enter || e.KeyCode == Keycode.NumpadEnter))
         {  // service on key up, because if it was on keydown, the activity with new list was been started 
            // and keyup event was been catch in this new activity and empty cage was benn added to list.
            e.Handled = true;
            StartCampaignButtonOnClick(null, null);
         }
      }

      private void FileNameOnFocusChange(object sender, View.FocusChangeEventArgs args)
      {
         if (!args.HasFocus) return;
         
         ShowSoftwareKeyboard(this, true);
         _fileName.SetSelection(_fileName.Text.Length);
      }

      private void StartCampaignButtonOnClick(object sender, EventArgs eventArgs)
      {
         // check name if contains valid characters only
         if (!System.Text.RegularExpressions.Regex.IsMatch(_fileName.Text, Settings.ValidFilePattern))
         {
            var alert = new AlertDialog.Builder(this);
            alert.SetTitle(Resources.GetString(Resource.String.Warning));
            alert.SetMessage(Resources.GetString(Resource.String.InvalidName));
            alert.SetPositiveButton("OK", (o, args) => { });
            var dialog = alert.Create();
            dialog.Show();

            _fileName.Text = Program.GetUsableFileName();
            return;
         }

         //check file name
         if (!Program.NewCampaign(_fileName.Text))
         {
            Toast.MakeText(this, Application.Context.Resources.GetString(Resource.String.NameIsUsed), ToastLength.Long).Show();
            return;
         }         
         // Save settings
         ViewModel.LastFileName = Program.DbFileName;
         ViewModel.LastFileFolder = Program.DbFolder;
         ViewModel.Save(BaseContext);

         SetResult(Result.Ok);
         Finish();
      }

      private void UsePredefNameButtonOnClick(object sender, EventArgs eventArgs)
      {
         _fileName.Text = Program.GetUsableFileName();
      }
   }
}