﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using PSD.Core.EggScale.Interfaces;
using PSD.Core.ViewModels;
using PSD.Droid.Adapters;

namespace PSD.Droid.Activities
{
   [Activity(Label = "PSD", ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize,
      ScreenOrientation = ScreenOrientation.Portrait)]
   public class CagesListActivity : BaseActivity<CagesViewModel>
   {
      #region Field and properties

      private CagesAdapter _adapter;
      private Button _zoomStrip;

      #endregion

      protected override void OnCreate(Bundle bundle)
      {
         base.OnCreate(bundle);
         SetContentView(Resource.Layout.CageList);
         var cageListView = FindViewById<ListView>(Resource.Id.cageListView);

         _adapter = new CagesAdapter(this, cageListView);
         cageListView.Adapter = _adapter;
         cageListView.ItemsCanFocus = true;
         IsHardwareKeyboardAvailable = HasHwKeys;

         _zoomStrip = FindViewById<Button>(Resource.Id.zoomStrip);
         _zoomStrip.Click += CagesListActivity_Click;


         //ViewModel.CagesChange += ViewModelOnCagesChange;
         CagesViewModel.CagesChange += ViewModelOnCagesChange;
      }

      protected override void OnPause()
      {
         base.OnPause();
         EggWeighted -= OnEggWeighted;
         _adapter.ShowSoftwareKeyboard -= ShowSoftwareKeyboardIfHwMiss;
         _zoomStrip.Click -= CagesListActivity_Click;

         //Program.SaveDb(this);
      }

      protected override void OnResume()
      {
         base.OnResume();
         EggWeighted += OnEggWeighted;
         _adapter.ShowSoftwareKeyboard += ShowSoftwareKeyboardIfHwMiss;
         _zoomStrip.Click += CagesListActivity_Click;         
      }

      #region Private helpers

      private void ViewModelOnCagesChange(object sender, EventArgs eventArgs)
      {
         Program.SaveDb(this);
      }

      private void OnEggWeighted(object sender, CommandArgs commandArgs)
      {
         RunOnUiThread(() =>
         {
            //Toast.MakeText(this, Application.Context.Resources.GetString(Resource.String.ReadWeight) + " " + commandArgs.Weight + " g", ToastLength.Short).Show();
            _adapter.ReadWeight(commandArgs.Weight);   
         });
      }

      /// <summary>
      /// Zoom to last list's item.
      /// </summary>
      private void CagesListActivity_Click(object sender, EventArgs e)
      {
         _adapter.ActivateEntryCage();
      }

      #endregion
   }
}
