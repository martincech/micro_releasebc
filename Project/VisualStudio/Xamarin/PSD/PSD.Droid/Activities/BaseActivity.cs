using System;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using PSD.Core;
using PSD.Core.EggScale.Interfaces;
using PSD.Droid.Scale;
using Utilities.IOC;

namespace PSD.Droid.Activities
{
   [Activity]
   public class BaseActivity<TViewModel> : Activity, IScale where TViewModel : class
   {
      #region Fields and properties

      public const int RequestNewCampaign = 101;
      public TViewModel ViewModel { get; protected set; }
      public bool IsHardwareKeyboardAvailable { get; protected set; }

      private BluetoothAdapter _btAdapter;
      private ScaleService scaleService;

      protected bool HasHwKeys
      {
         get
         {
#if DEBUG
            if (Build.Manufacturer.ToLower().Equals("xamarin"))
            {
               return true;
            }
#endif
            return KeyCharacterMap.DeviceHasKey(Keycode.F1) &&
                   KeyCharacterMap.DeviceHasKey(Keycode.F2) &&
                   KeyCharacterMap.DeviceHasKey(Keycode.F3) &&
                   KeyCharacterMap.DeviceHasKey(Keycode.F4);
         }
      }

      public event EventHandler<CommandArgs> EggWeighted;

      #endregion

      public BaseActivity()
      {
         ViewModel = IocContainer.Resolve(typeof (TViewModel)) as TViewModel;
         scaleService = IocContainer.Resolve<ScaleService>();
         scaleService.EggWeighted += ScaleServiceOnEggWeighted;
      }

      protected override void OnCreate(Bundle bundle)
      {
         SetLanguage(Settings.CultureName);
         base.OnCreate(bundle);
      }

      protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
      {
         base.OnActivityResult(requestCode, resultCode, data);

         switch (requestCode)
         {
            case RequestNewCampaign:
               // if parent class is CagesListActivity then finish it
               var parentClassName = GetType().UnderlyingSystemType.Name;
               if (resultCode == Result.Ok)
               {
                  StartActivity(typeof(CagesListActivity));
                  if (parentClassName.Equals("CagesListActivity"))
                  {
                     Finish();
                  }
               }
               break;
            case (int)Request.ConnectDevice:
               // When DeviceListActivity returns with a device to connect
               if (resultCode == Result.Ok)
               {
                  // Get the device MAC address
                  var address = data.Extras.GetString(DeviceListActivity.ExtraDeviceAddress);
                  // Get the BLuetoothDevice object
                  var device = _btAdapter.GetRemoteDevice(address);
                  // Attempt to connect to the device
                  scaleService.Connect(device, address);
               }
               break;
            case (int)Request.EnableBt:
               // When the request to enable Bluetooth returns
               if (resultCode == Result.Ok)
               {
                  ShowDeviceList();
               }
               else
               {  // User did not enable Bluetooth or an error occured
                  Toast.MakeText(this, Resources.GetString(Resource.String.BtTurnOff), ToastLength.Short).Show();
               }
               break;
         }
      }

      public override void OnConfigurationChanged(Configuration newConfig)
      {
         base.OnConfigurationChanged(newConfig);
         SetLanguage(Settings.CultureName);
         IsHardwareKeyboardAvailable = HasHwKeys;
      }

      #region Main menu
    
      public override bool OnCreateOptionsMenu(IMenu menu)   
      {
         MenuInflater.Inflate(Resource.Menu.MainMenu, menu);     
         return base.OnCreateOptionsMenu(menu);       
      }

      public override bool OnPrepareOptionsMenu(IMenu menu)
      {
         var btDevice = menu.FindItem(Resource.Id.connectScale);
         var closeDevice = menu.FindItem(Resource.Id.disconnectScale);
         
         var closeDeviceVisibility = false;
         var connectScaleTitle = Resources.GetString(Resource.String.ConnectScale);
         if (scaleService.ConnectedDeviceName != null && ! scaleService.ConnectedDeviceName.Equals(""))
         {
            connectScaleTitle = Resources.GetString(Resource.String.ConnectedTo) + " " + scaleService.ConnectedDeviceName; 
            closeDeviceVisibility = true;
         }
         btDevice.SetTitle(connectScaleTitle);
         closeDevice.SetVisible(closeDeviceVisibility);

         return base.OnPrepareOptionsMenu(menu);
      }

      public override bool OnOptionsItemSelected(IMenuItem item)
      {
         switch (item.ItemId)
         {
            case Resource.Id.newCampaign:
               var serverIntent = new Intent(this, typeof(CampaignNameActivity));
               StartActivityForResult(serverIntent, RequestNewCampaign);
               return true;
            case Resource.Id.pickerName:
               StartActivity(typeof(PickerActivity));
               return true;
            case Resource.Id.connectScale:
               if (BluetoothAdapter.DefaultAdapter == null)
               {
                  Toast.MakeText(this, Application.Context.Resources.GetString(Resource.String.BtNotExist), ToastLength.Long).Show();
                  return false;
               }
               CheckBluetooth();
               if (_btAdapter.IsEnabled)
               {
                  ShowDeviceList();
               }
               return true;
            case Resource.Id.disconnectScale:
               scaleService.Stop();
               return true;
         }

         return base.OnOptionsItemSelected(item);
      }

      /// <summary>
      /// Show list of available bluetooth devices.
      /// </summary>
      public void ShowDeviceList()
      {
         // Launch the DeviceListActivity to see devices and do scan
         var serverIntent = new Intent(this, typeof(DeviceListActivity));
         StartActivityForResult(serverIntent, (int)Request.ConnectDevice);
      }

      #endregion

      #region Protected helpers

      /// <summary>
      /// Check if bluetooth is enabled and potentially turn on it.
      /// </summary>
      private void CheckBluetooth()
      {
         // get local adapter
         _btAdapter = BluetoothAdapter.DefaultAdapter;
         if (_btAdapter == null)
         {
            Toast.MakeText(this, Application.Context.Resources.GetString(Resource.String.BtNotExist), ToastLength.Long).Show();
            return;
         }

         if (!_btAdapter.IsEnabled)
         {         
            var enableIntent = new Intent(BluetoothAdapter.ActionRequestEnable);
            StartActivityForResult(enableIntent, (int)Request.EnableBt);
         }
      }

      private void ScaleServiceOnEggWeighted(object sender, CommandArgs commandArgs)
      {
         if (EggWeighted != null)
         {
            EggWeighted(this, commandArgs);
         }
      }

      /// <summary>
      /// Show or hide software keyboard.
      /// If device contain hardware keyboard, software keyboard isn't display. 
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e">truw - show keyboard</param>
      protected void ShowSoftwareKeyboardIfHwMiss(object sender, bool e)
      {
         if (IsHardwareKeyboardAvailable) return;
         ShowSoftwareKeyboard(sender, e);
      }

      protected void ShowSoftwareKeyboard(object sender, bool e)
      {
         if (e) //show
         {
            ((InputMethodManager)GetSystemService(InputMethodService)).ShowSoftInput(CurrentFocus, ShowFlags.Forced);
         }
         else // hide
         {
            var inputManager = (InputMethodManager)GetSystemService(InputMethodService);
            inputManager.HideSoftInputFromWindow(CurrentFocus.WindowToken, HideSoftInputFlags.None);
         }
      }

      /// <summary>
      /// Change language.
      /// </summary>
      protected void SetLanguage(string cultureName)
      {
         var locale = new Java.Util.Locale(cultureName);
         Java.Util.Locale.Default = locale;
         var config = new Configuration { Locale = locale };
         BaseContext.Resources.UpdateConfiguration(config, BaseContext.Resources.DisplayMetrics);
      }

      #endregion
   }
}