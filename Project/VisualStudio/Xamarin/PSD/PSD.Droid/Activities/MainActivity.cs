using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using PSD.Core.Repository.Interfaces;

namespace PSD.Droid.Activities
{
   [Activity(Label = "PSD", MainLauncher = true, ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize,
      ScreenOrientation = ScreenOrientation.Portrait)]
   public class MainActivity : BaseActivity<ISettings>
   {
      protected override void OnCreate(Bundle bundle)
      {
         base.OnCreate(bundle);
         SetContentView(Resource.Layout.Main);

         var startButton = FindViewById<Button>(Resource.Id.startNewCampaignButton);
         var continueButton = FindViewById<Button>(Resource.Id.continueCampaignButton);
         continueButton.SetTextColor(Color.White);
         startButton.SetTextColor(Color.White);

         startButton.Click += StartButtonOnClick;
         continueButton.Click += ContinueButtonOnClick;
      }

      protected override void OnResume()
      {
         base.OnResume();
         var continueButton = FindViewById<Button>(Resource.Id.continueCampaignButton);

         if (!Program.IsFileExist(Program.DbFileName, false))
         {  //Test if previous file exist
            continueButton.Enabled = false;
            continueButton.SetTextColor(Color.Black);
         }
         else
         {
            continueButton.Enabled = true;
            continueButton.SetTextColor(Color.White);
         }
      }

      private void ContinueButtonOnClick(object sender, EventArgs eventArgs)
      {
         StartActivity(typeof(CagesListActivity));
      }

      private void StartButtonOnClick(object sender, EventArgs eventArgs)
      {
         var serverIntent = new Intent(this, typeof(CampaignNameActivity));
         StartActivityForResult(serverIntent, RequestNewCampaign);
      }

      public override void OnBackPressed()
      {
         var dialog = new AlertDialog.Builder(this);
         dialog.SetPositiveButton(Resources.GetString(Resource.String.ButtonOK), (sender, args) => base.OnBackPressed());
         dialog.SetNegativeButton(Resources.GetString(Resource.String.Cancel), delegate { });
         dialog.SetMessage(Resources.GetString(Resource.String.QuitApp));
         dialog.Show();
      }
   }
}