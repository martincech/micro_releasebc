using System;
using Android.App;
using Android.Runtime;
using Ninject;
using PSD.Core.Repository;
using PSD.Core.Repository.Interfaces;
using PSD.Core.ViewModels;
using PSD.Droid.Scale;
using Utilities.IOC;

namespace PSD.Droid.Activities
{
   [Application(Theme = "@android:style/Theme.Holo.Light.NoActionBar")]
   public class App : Application
   {
      public App(IntPtr javaReference, JniHandleOwnership transfer) :
         base(javaReference, transfer)
      {
      }

      public override void OnCreate()
      {
         base.OnCreate();
         
         IKernel ninjectKernel = new StandardKernel();
         IocContainer.Container = new NinjectIocContainer(ninjectKernel);
         
         //Models
         ninjectKernel.Bind<ICagesRepository>().To<CagesCsvRepository>().InSingletonScope();
         ninjectKernel.Bind<ISettings>().To<SettingsService>().InSingletonScope();
         ninjectKernel.Bind<ScaleService>().To<ScaleService>().InSingletonScope();

         //ViewModels
         IocContainer.Register(() => new CagesViewModel());

         // Load settings
         var settings = IocContainer.Resolve<ISettings>();
         settings.Load(BaseContext);
         Program.DbFolder = settings.LastFileFolder;
         Program.DbFileName = settings.LastFileName;
         Program.PickerName = settings.PickerName;

         // Load data from file
         var repository = (CagesCsvRepository)IocContainer.Resolve<ICagesRepository>();
         var data = Program.LoadDb();
         repository.Parse(data);
      }
   }
}