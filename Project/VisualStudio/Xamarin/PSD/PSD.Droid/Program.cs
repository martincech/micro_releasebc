using System;
using System.Collections.Generic;
using System.IO;
using Android.App;
using Android.Media;
using PSD.Core;
using PSD.Core.Extensions;
using PSD.Core.Repository.Interfaces;
using Utilities.IOC;
using PSD.Core.ViewModels;
using Environment = Android.OS.Environment;

namespace PSD.Droid
{
   public static class Program
   {
      public static string DbFolder;
      public static string DbFileName;
      public static string PickerName;
      public const string Mounted = "mounted";  

      /// <summary>
      /// Load list of cages from file.
      /// </summary>
      /// <returns>loaded data</returns>
      public static byte[] LoadDb()
      {
         if (!Environment.ExternalStorageState.Equals(Mounted)) return null;
         var path = Path.Combine(Environment.ExternalStorageDirectory.Path, Settings.RootFolder, DbFolder);

         if (!Directory.Exists(path))
         {
            return null;
         }
         var fullPath = Path.Combine(path, DbFileName + Settings.FileSuffix);
         return File.Exists(fullPath) ? File.ReadAllBytes(fullPath) : null;       
      }

      /// <summary>
      /// Save list of cages to file.
      /// </summary>
      /// <param name="activity">Activity</param>
      public static void SaveDb(Activity activity)
      {
         var header = new List<string>
         {
            activity.Resources.GetString(Resource.String.CageNumber),
            activity.Resources.GetString(Resource.String.Count),
            activity.Resources.GetString(Resource.String.Parameter),
            activity.Resources.GetString(Resource.String.Weight) + " [" + Settings.Unit + "]"
         };

         var path = Path.Combine(Environment.ExternalStorageDirectory.Path, Settings.RootFolder, DbFolder);
         if (!Directory.Exists(path))
         {
            Directory.CreateDirectory(path);
         }

         var fullPath = Path.Combine(path, DbFileName + Settings.FileSuffix);

         var repository = IocContainer.Resolve<ICagesRepository>();
         var text = repository.ToWriteString(header);
         File.WriteAllText(fullPath, text, System.Text.Encoding.UTF8);
         MediaScannerConnection.ScanFile(activity, new string[] { fullPath }, null, null);       
      }

      public static bool NewCampaign(string name)
      {           
         if (IsFileExist(name)) return false;

         DbFolder = GetDefaultName();
         DbFileName = name;       

         // clear list of cages   
         var vm = IocContainer.Resolve<CagesViewModel>();
         vm.DeleteAllCages();
         return true;
      }
      
      /// <summary>
      /// Get default name with actual date. Format: yyyy_mm_dd
      /// </summary>
      public static string GetDefaultName()
      {
         var currentDate = DateTime.Now;
         var month = currentDate.Month.ToString();
         if (month.Length == 1)
         {
            month = month.Insert(0, "0");
         }
         var day = currentDate.Day.ToString();
         if (day.Length == 1)
         {
            day = day.Insert(0, "0");
         }
         return currentDate.Year + "_" + month + "_" + day;
      }

      /// <summary>
      /// Get unique file name.
      /// </summary>
      /// <returns></returns>
      public static string GetUsableFileName()
      {
         var name = GetDefaultName();
         if (!PickerName.Equals(""))
         {
            name += "_" + PickerName;
         }
         var i = 0;
         var fileName = name;
         
         while(IsFileExist(fileName))
         {
            i++;
            fileName = name + "_" + i;
         }
         return fileName;
      }

      /// <summary>
      /// Check if file exist on disk
      /// </summary>
      /// <param name="name">file name</param>
      /// <param name="defaultFolder">flag if search file at default folder (true) or at db folder.
      /// Note: default folder has structure consisting from current date.</param>
      /// <returns>true - file exist</returns>
      public static bool IsFileExist(string name, bool defaultFolder = true)
      {
         if (!Environment.ExternalStorageState.Equals(Mounted)) return false;
         var folder = GetDefaultName();
         if (!defaultFolder)
         {
            folder = DbFolder;
         }
         var path = Path.Combine(Environment.ExternalStorageDirectory.Path, Settings.RootFolder, folder);

         if (!Directory.Exists(path))
         {
            return false;
         }
         var finalPath = Path.Combine(path, name + Settings.FileSuffix);
         return File.Exists(finalPath);
      }
   }
}