using Android.App;
using Android.Widget;
using PSD.Core.EggScale.Interfaces;

namespace PSD.Droid.Scale
{
   /// <summary>
   /// The Handler that gets information back from the ScaleService
   /// </summary>
   internal class ScaleHandler : Android.OS.Handler
   {
	  public override void HandleMessage (Android.OS.Message msg)
	  {
		 switch (msg.What) 
         {
            //case (int)Message.StateChange:
            //   switch (msg.Arg1)
            //   {
            //      case (int)State.Connected:
            //         break;
            //      case (int)State.Connecting:
            //      case (int)State.Listen:
            //      case (int)State.None:
            //         break;
            //   }
            //   break;

            //case (int)Message.Read:
            //   var weight = msg.Data.GetString(ScaleService.CURRENT_WEIGHT);
            //   Toast.MakeText(Application.Context, Application.Context.Resources.GetString(Resource.String.ReadWeight) + " " + weight + " g", ToastLength.Long).Show();
            //   ((CagesListActivity)_context).WeightRead(weight);   
            //   break;
            case (int)Message.DeviceName:
			   // connected device's name
               var connectedDeviceName = msg.Data.GetString(ScaleService.DeviceName);
			   Toast.MakeText(Application.Context, 
                     Application.Context.Resources.GetString(Resource.String.ConnectedTo) + " " + connectedDeviceName, ToastLength.Short).Show();
			   break;
            case (int)Message.Toast:
			   Toast.MakeText(Application.Context, msg.Data.GetString(ScaleService.Toast), ToastLength.Short).Show ();
			   break;
		 }
	  }
   }
}