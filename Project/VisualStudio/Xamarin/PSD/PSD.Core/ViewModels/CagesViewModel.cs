﻿using System;
using System.Collections.Generic;
using System.Linq;
using PSD.Core.Models;
using PSD.Core.Repository.Interfaces;
using Utilities.IOC;

namespace PSD.Core.ViewModels
{
   public class CagesViewModel
   {
      private readonly ICagesRepository service = IocContainer.Resolve<ICagesRepository>();
      public static event EventHandler CagesChange;

      public IEnumerable<Cage> Cages
      {
         get { return service.Cages; }
      }

      public int FreeCageNumber
      {
         get { return service.GetFreeCageNumber(); }
      }

      public bool AddCage(Cage cage)
      {
         if (!service.Add(cage)) return false;
         
         InvokeEvent();
         return true;
      }

      public bool UpdateCage(Cage cage)
      {
         if (service.Update(cage)) return false;

         InvokeEvent();
         return true;
      }

      public bool DeleteCage(Cage cage)
      {
         if (!service.Delete(cage)) return false;

         InvokeEvent();
         return true;
      }

      public bool DeleteCage(int cageNumber)
      {
         var cage = Cages.First(c => c.CageNumber == cageNumber);
         var result = cage != null && DeleteCage(cage);

         if (result)
         {
            InvokeEvent();
         }
         return result;
      }

      public void DeleteAllCages()
      {
         for (var i = Cages.Count() - 1; i >= 0; i--)
         {
            DeleteCage(Cages.ElementAt(i));
         }
         InvokeEvent();
      }

      private void InvokeEvent()
      {
         if (CagesChange != null)
         {
            CagesChange(this, new EventArgs());
         }
      }
   }
}
