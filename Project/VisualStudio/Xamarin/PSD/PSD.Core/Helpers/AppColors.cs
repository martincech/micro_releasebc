﻿namespace PSD.Core
{
   /// <summary>
   /// Colors use at application.
   /// </summary>
   public class AppColors
   {
      public const string BACKGROUND_DEFAULT = "#ffffffff";
      public const string BACKGROUND_SELECTED = "#ffffbb33";
      public const string BACKGROUND_NEWCAGE_ACTIVE = "#ffff6060";
      public const string BACKGROUND_NEWCAGE_INACTIVE = "#ffdddddd";
   }
}
