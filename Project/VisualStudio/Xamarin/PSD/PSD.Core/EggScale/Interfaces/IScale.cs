﻿using System;

namespace PSD.Core.EggScale.Interfaces
{
   public interface IScale
   {
      /// <summary>
      /// Read weight event.
      /// </summary>
      event EventHandler<CommandArgs> EggWeighted;
   }
}
