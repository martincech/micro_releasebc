﻿namespace PSD.Core.EggScale.Interfaces
{
   public enum Request
   {
      ConnectDevice = 1,
      EnableBt = 2
   }
}
