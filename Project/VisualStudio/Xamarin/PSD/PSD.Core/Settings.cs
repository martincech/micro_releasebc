﻿namespace PSD.Core
{
   public static class Settings
   {
      public static string Unit = "g";
      public static string FileSuffix = ".csv";

      public const string ValidFilePattern = @"^[a-zA-Z0-9_]+$";
      public const string RootFolder = "PSD";
      public const string CultureName = "cs-CZ";
   }
}
