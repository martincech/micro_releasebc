﻿namespace PSD.Core.Repository.Interfaces
{
   public interface ISettings
   {
      string PickerName { get; set; }
      string LastFileName { get; set; }
      string LastFileFolder { get; set; }

      void Save(object context);
      void Load(object context);
   }
}
