using System.Collections.Generic;
using System.Linq;
using PSD.Core.Enums;
using PSD.Core.Models;
using PSD.Core.Repository.Interfaces;

namespace PSD.Core.Repository
{
   public class CagesSampleData : ICagesRepository
   {
      public CagesSampleData()
      {
         Cages = new List<Cage>();
         //////////////////////////////////////////////////////////////////////
         var c1 = new Cage { CageNumber = 121 };
         var c1Rec1 = new CageRecord
         {
            Type = ParameterE.ParameterOk,
            Count = 1
         };
         var c1Rec2 = new CageRecord
         {
            Type = ParameterE.ParameterTwin,
            Count = 1
         };
         c1.Add(c1Rec1);
         c1.Add(c1Rec2);
         Add(c1);
         //////////////////////////////////////////////////////////////////////
         var c2 = new Cage { CageNumber = 122 };
         var c2Rec1 = new CageRecord
         {
            Type = ParameterE.ParameterTwin,
            Count = 1
         };
         c2.Add(c2Rec1);
         Add(c2);
         //////////////////////////////////////////////////////////////////////
         /*var c3 = new Cage { CageNumber = 123 };
         var c3Rec1 = new CageRecord
         {
            Type = ParameterE.PARAMETER_CRACKED,
            Count = 2
         };
         var c3Rec2 = new CageRecord
         {
            Type = ParameterE.PARAMETER_OK,
            Count = 1
         };
         var c3Rec3 = new CageRecord
         {
            Type = ParameterE.PARAMETER_DEAD,
            Count = 1
         };
         c3.Add(c3Rec1);
         c3.Add(c3Rec2);
         c3.Add(c3Rec3);
         Add(c3);

         //////////////////////////////////////////////////////////////////////
         var c4 = new Cage { CageNumber = 124 };
         var c4Rec1 = new CageRecord
         {
            Type = ParameterE.PARAMETER_TWIN,
            Count = 1
         };
         var c4Rec2 = new CageRecord
         {
            Type = ParameterE.PARAMETER_CRACKED,
            Count = 2
         };
         var c4Rec3 = new CageRecord
         {
            Type = ParameterE.PARAMETER_OK,
            Count = 2
         };
         c4.Add(c4Rec1);
         c4.Add(c4Rec2);
         c4.Add(c4Rec3);
         Add(c4);

         //////////////////////////////////////////////////////////////////////
         var c5 = new Cage { CageNumber = 125 };
         var c5Rec1 = new CageRecord
         {
            Type = ParameterE.PARAMETER_OK,
            Count = 1
         };
         c5.Add(c5Rec1);
         Add(c5);

         //////////////////////////////////////////////////////////////////////
         var c6 = new Cage { CageNumber = 126 };
         var c6Rec1 = new CageRecord
         {
            Type = ParameterE.PARAMETER_DEAD,
            Count = 1
         };
         c6.Add(c6Rec1);
         Add(c6);

         //////////////////////////////////////////////////////////////////////
         var c7 = new Cage { CageNumber = 127 };
         var c7Rec1 = new CageRecord
         {
            Type = ParameterE.PARAMETER_MEMBRANE,
            Count = 2
         };
         c7.Add(c7Rec1);
         Add(c7);

         //////////////////////////////////////////////////////////////////////
         var c8 = new Cage {CageNumber = 128};
         var c8Rec1 = new CageRecord
         {
            Type = ParameterE.PARAMETER_OK,
            Count = 1
         };
         var c8Rec2 = new CageRecord
         {
            Type = ParameterE.PARAMETER_CRACKED,
            Count = 1
         };
         c8.Add(c8Rec1);
         c8.Add(c8Rec2);
         Add(c8);

         //////////////////////////////////////////////////////////////////////
         var c9 = new Cage { CageNumber = 4481 };
         var c9Rec1 = new CageRecord
         {
            Type = ParameterE.PARAMETER_MEMBRANE,
            Count = 2
         };
         var c9Rec2 = new CageRecord
         {
            Type = ParameterE.PARAMETER_TWIN,
            Count = 2
         };
         var c9Rec3 = new CageRecord
         {
            Type = ParameterE.PARAMETER_DEAD,
            Count = 1
         };
         c9.Add(c9Rec1);
         c9.Add(c9Rec2);
         c9.Add(c9Rec3);
         Add(c9);

         //////////////////////////////////////////////////////////////////////
         var c10 = new Cage { CageNumber = 4482 };
         var c10Rec1 = new CageRecord
         {
            Type = ParameterE.PARAMETER_MEMBRANE,
            Count = 1
         };
         var c10Rec2 = new CageRecord
         {
            Type = ParameterE.PARAMETER_TWIN,
            Count = 2
         };
         c10.Add(c10Rec1);
         c10.Add(c10Rec2);
         Add(c10);

         //////////////////////////////////////////////////////////////////////
         var c11 = new Cage { CageNumber = 4483 };
         var c11Rec1 = new CageRecord
         {
            Type = ParameterE.PARAMETER_CRACKED,
            Count = 2
         };
         c11.Add(c11Rec1);
         Add(c11);

         //////////////////////////////////////////////////////////////////////
         var c12 = new Cage { CageNumber = 4484 };
         Add(c12);

         //////////////////////////////////////////////////////////////////////
         var c13 = new Cage { CageNumber = 4485 };
         Add(c13);

         //////////////////////////////////////////////////////////////////////
         */
         //// stress test 
         //for (var i = 0; i < 4500; i++)
         //{
         //   var rec = new CageRecord
         //   {
         //      Type = ParameterE.PARAMETER_OK,
         //      Count = 1
         //   };
         //   var cage = new Cage { CageNumber = 9876 };
         //   cage.Add(rec);
         //   Add(cage);

         //}
      }

      #region Implementation of ICagesRepository

      public bool Add(Cage cage)
      {
         if (((List<Cage>) Cages).Contains(cage))
         {
            return false;
         }
         ((List<Cage>)Cages).Add(cage);
         OrderList();
         return true;

      }

      public bool Update(Cage cage)
      {
         var cageAt = ((List<Cage>)Cages).IndexOf(cage);
         if (cageAt < 0)
         {
            return false;
         }
         ((List<Cage>)Cages)[cageAt] = cage;
         OrderList();
         return true;
      }

      public bool Delete(Cage cage)
      {
         if (!((List<Cage>) Cages).Remove(cage))
         {
            return false;
         }
         OrderList();
         return true;
      }

      /// <summary>
      /// Get first available cage number.
      /// </summary>
      public int GetFreeCageNumber()
      {
         var maxNumber = 0;
         if (Cages.Any())
         {
            maxNumber = Cages.Max(i => i.CageNumber);
         }
         return maxNumber + 1;
      }

      public IQueryable<Cage> All()
      {
         return Cages.AsQueryable();
      }

      public IEnumerable<Cage> Cages { get; private set; }

      #endregion     
      private void OrderList()
      {
         Cages = Cages.OrderBy(c => c.CageNumber).ToList(); // order by cage number
      }
   }
}