using System;
using System.Runtime.Serialization;

namespace Kinetis
{
   /// <summary>  An exception from JTAG Tap controler 
   /// </summary>
   public class TapException : ApplicationException
   {
      /// <summary>  default constructor. Do nothing 
      /// </summary>
      public TapException()
      {
      }

      /// <summary>  default constructor. Do nothing 
      /// </summary>
      public TapException(string message) : base(message)
      {
      }

      /// <summary>  default constructor. Do nothing 
      /// </summary>
      public TapException(string message, Exception inner) : base(message, inner)
      {
      }

      /// <summary>  default constructor. Do nothing 
      /// </summary>
      protected TapException(
         SerializationInfo info,
         StreamingContext context
         )
         : base(info, context)
      {
      }
   }
}