/**
 * Copyright 2008-2009 The Asagao Project. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE ASAGAO PROJECT ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ASAGAO PROJECT OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation 
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of the Asagao Project.
 * 
 * http://sourceforge.net/projects/asagao/
* \file tapcontrol.cs
* \brief Amontec JTAGKEY control 
*/

using System.Text;
using System.Threading;
using Ftdi;
using Ftdi.Jtag;

namespace Kinetis.JtagKey
{
   /// <summary>
   /// \brief Amontec JTAGKEY TAP controling class
   /// 
   /// This class provide a set of TAP enable/disable, target reset. Note that 
   /// refer the Amontec Web site about  JTAGKEY's circuit schematic. 
   /// </summary>
   public class TapControl
   {
      /// <summary> \brief An abstruction for the reset pin control /// </summary>
      private static IResetControl _resetControl = new OOCDlinkResetControl();

      /// <summary> 
      /// \brief Set the pod type to interface with target
      /// \param Type of Pod
      /// 
      /// Specify the FT2232 JTAG pod type. As default, the pod is Amontec compatible. 
      /// If you use gnICE, use this function to change the pod type. 
      /// -Amontec http://www.amontec.com/jtagkey.shtml
      /// -gnICE http://docs.blackfin.uclinux.org/doku.php?id=hw:jtag:gnice
      /// </summary>
      public static void SetPodType(PodType aPod)
      {
         _resetControl = ResetControlFactory.Instance().GetResetControl(aPod);
      }

      /// <summary> 
      /// \brief set given pins as output
      /// \param gpio pins to be output
      /// 
      /// Set all the pins output. Set all pins as low.
      /// </summary>
      private static void SetPinDirection(out InputOutputPins gpio)
      {
         InputOutputPins port;

         port.Pin0_Dir = PinDirection.Output;
         port.Pin1_Dir = PinDirection.Output;
         port.Pin2_Dir = PinDirection.Output;
         port.Pin3_Dir = PinDirection.Output;

         port.Pin0 = LogicLevel.Undef;
         port.Pin1 = LogicLevel.Undef;
         port.Pin2 = LogicLevel.Undef;
         port.Pin3 = LogicLevel.Undef;

         gpio = port;
      }

      /// <summary>
      /// \brief extract GPIOH state of pins
      /// \param handle FT2232 device handle to be refered
      /// \param gpioH a variable which will receive pin state
      /// 
      /// Check an FT2232 device which is pointed by handle parameter, and 
      /// copy the pin state into gpioH parameter. 
      /// </summary>
      private static void CopyCurrentGpioh(int handle, ref InputOutputPins gpioH)
      {
         InputPins gpiL, gpiH;

         FtcJtag.GetGPIOs(
            handle,
            false,
            out gpiL,
            true,
            out gpiH
            );

         gpioH.Pin0 = gpiH.Pin0;
         gpioH.Pin1 = gpiH.Pin1;
         gpioH.Pin2 = gpiH.Pin2;
         gpioH.Pin3 = gpiH.Pin3;
      }

      /// <summary> 
      /// \brief If error, thrown an exception 
      /// \param error status
      /// 
      /// Check the error status and if error, throw an exception
      /// </summary>
      private static void ErrorCheck(JtagStatus status)
      {
         if (status == JtagStatus.Success) return;
         var s = new StringBuilder(256);
         FtcJtag.GetErrorCodeString(
            "EN",
            status,
            s,
            s.Capacity
            );
         throw new TapException("FTDI JTAG Error // " + s);
      }

      /// <summary>
      /// \brief Enabling tap.
      /// \param handle
      /// 
      /// Enable the given JTAG TAP. Amontec JTAGKEY has a bidirectional buffer
      /// to convert the logic level. To turn on this buffer, you make  
      /// GPIO_L_Pin0 to output and keept it L. 
      /// 
      /// Also, this sets the SRST Hi-Z and TRST H.
      /// </summary>
      public static void EnableTap(int handle)
      {
         InputOutputPins gpioL, gpioH;

         gpioL.Pin0_Dir = PinDirection.Output; //  Tap Enable Pin
         gpioL.Pin1_Dir = PinDirection.Input;
         gpioL.Pin2_Dir = PinDirection.Input;
         gpioL.Pin3_Dir = PinDirection.Input;

         gpioL.Pin0 = LogicLevel.L; //  Tap Enable
         gpioL.Pin1 = LogicLevel.L;
         gpioL.Pin2 = LogicLevel.L;
         gpioL.Pin3 = LogicLevel.L;

         SetPinDirection(out gpioH);
         _resetControl.EnableTap(ref gpioH);


         ErrorCheck(
            FtcJtag.SetGPIOs(
               handle,
               true, // control GPIO_L
               ref gpioL,
               true, // control GPIO_H
               ref gpioH
               )
            );
      }

      /// <summary>
      /// \brief Disabling tap.
      /// \param handle
      /// 
      /// Disable the given JTAG TAP. Amontec JTAGKEY has a bidirectional buffer
      /// to convert the logic level. To turn on this buffer, you make  
      /// GPIO_L_Pin0 to output and keept it H. 
      /// 
      /// Also, this sets the SRST and TRST Hi-Z.
      /// </summary>
      public static void DisableTap(int handle)
      {
         InputOutputPins gpioL, gpioH;

         gpioL.Pin0_Dir = PinDirection.Output; //  Tap Enable Pin
         gpioL.Pin1_Dir = PinDirection.Input;
         gpioL.Pin2_Dir = PinDirection.Input;
         gpioL.Pin3_Dir = PinDirection.Input;

         gpioL.Pin0 = LogicLevel.H; //   Tap Disable
         gpioL.Pin1 = LogicLevel.L;
         gpioL.Pin2 = LogicLevel.L;
         gpioL.Pin3 = LogicLevel.L;

         SetPinDirection(out gpioH);
         _resetControl.DisableTap(ref gpioH);

         ErrorCheck(
            FtcJtag.SetGPIOs(
               handle,
               true, // control GPIO_L
               ref gpioL,
               true, // control GPIO_H
               ref gpioH
               )
            );
         Thread.Sleep(100);
      }

      /// <summary>
      /// \brief Asserting SRST.
      /// \param handle
      /// 
      /// Turn the SRST of given JTAG TAP to L.
      /// 
      /// The state of TRST is not change.
      /// </summary>
// ReSharper disable once InconsistentNaming
      public static void AssertSRST(int handle)
      {
         InputOutputPins gpioL, gpioH;

         gpioL.Pin0_Dir = PinDirection.Output; // Output. Tap Enable Pin
         gpioL.Pin1_Dir = PinDirection.Input;
         gpioL.Pin2_Dir = PinDirection.Input;
         gpioL.Pin3_Dir = PinDirection.Input;

         gpioL.Pin0 = LogicLevel.L; // Tap Enable
         gpioL.Pin1 = LogicLevel.L;
         gpioL.Pin2 = LogicLevel.L;
         gpioL.Pin3 = LogicLevel.L;

         SetPinDirection(out gpioH);
         CopyCurrentGpioh(handle, ref gpioH);

         _resetControl.AssertSRST(ref gpioH);

         ErrorCheck(
            FtcJtag.SetGPIOs(
               handle,
               true, // control GPIO_L
               ref gpioL,
               true, // control GPIO_H
               ref gpioH
               )
            );
      }

      /// <summary>
      /// \brief Desserting SRST.
      /// \param handle
      /// 
      /// Turn the SRST of given JTAG TAP to Hi-Z.
      /// 
      /// The state of TRST is not change.
      /// </summary>
// ReSharper disable once InconsistentNaming
      public static void DeassertSRST(int handle)
      {
         InputOutputPins gpioL, gpioH;

         gpioL.Pin0_Dir = PinDirection.Output; // Output. Tap Enable Pin
         gpioL.Pin1_Dir = PinDirection.Input;
         gpioL.Pin2_Dir = PinDirection.Input;
         gpioL.Pin3_Dir = PinDirection.Input;

         gpioL.Pin0 = LogicLevel.L; // Tap Enable
         gpioL.Pin1 = LogicLevel.L;
         gpioL.Pin2 = LogicLevel.L;
         gpioL.Pin3 = LogicLevel.L;

         SetPinDirection(out gpioH);
         CopyCurrentGpioh(handle, ref gpioH);

         _resetControl.DeassertSRST(ref gpioH);

         ErrorCheck(
            FtcJtag.SetGPIOs(
               handle,
               true, // control GPIO_L
               ref gpioL,
               true, // control GPIO_H
               ref gpioH
               )
            );
      }

      /// <summary>
      /// \brief Asserting TRST.
      /// \param handle
      /// 
      /// Turn the TRST of given JTAG TAP to L.
      /// 
      /// The state of SRST is not change.
      /// </summary>
// ReSharper disable once InconsistentNaming
      public static void AssertTRST(int handle)
      {
         InputOutputPins gpioL, gpioH;


         gpioL.Pin0_Dir = PinDirection.Output; // Output. Tap Enable Pin
         gpioL.Pin1_Dir = PinDirection.Input;
         gpioL.Pin2_Dir = PinDirection.Input;
         gpioL.Pin3_Dir = PinDirection.Input;

         gpioL.Pin0 = LogicLevel.L; // Tap Enable
         gpioL.Pin1 = LogicLevel.L;
         gpioL.Pin2 = LogicLevel.L;
         gpioL.Pin3 = LogicLevel.L;

         SetPinDirection(out gpioH);
         CopyCurrentGpioh(handle, ref gpioH);

         _resetControl.AssertTRST(ref gpioH);

         ErrorCheck(
            FtcJtag.SetGPIOs(
               handle,
               true, // control GPIO_L
               ref gpioL,
               true, // control GPIO_H
               ref gpioH
               )
            );
      }

      /// <summary>
      /// \brief Deasserting TRST.
      /// \param handle
      /// 
      /// Turn the TRST of given JTAG TAP to H.
      /// 
      /// The state of SRST is not change.
      /// </summary>
// ReSharper disable once InconsistentNaming
      public static void DeassertTRST(int handle)
      {
         InputOutputPins gpioL, gpioH;

         gpioL.Pin0_Dir = PinDirection.Output; // Output. Tap Enable Pin
         gpioL.Pin1_Dir = PinDirection.Input;
         gpioL.Pin2_Dir = PinDirection.Input;
         gpioL.Pin3_Dir = PinDirection.Input;

         gpioL.Pin0 = LogicLevel.L; // Tap Enable
         gpioL.Pin1 = LogicLevel.L;
         gpioL.Pin2 = LogicLevel.L;
         gpioL.Pin3 = LogicLevel.L;

         SetPinDirection(out gpioH);
         CopyCurrentGpioh(handle, ref gpioH);

         _resetControl.DeassertTRST(ref gpioH);

         ErrorCheck(
            FtcJtag.SetGPIOs(
               handle,
               true, // control GPIO_L
               ref gpioL,
               true, // control GPIO_H
               ref gpioH
               )
            );
      }
   }
} // JtagKey