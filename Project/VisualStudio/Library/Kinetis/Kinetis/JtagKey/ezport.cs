/*
 * Copyright 2008-2009 The Asagao Project. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE ASAGAO PROJECT ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ASAGAO PROJECT OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation 
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of the Asagao Project.
 * 
 * http://sourceforge.net/projects/asagao/
 */
/** 
* \file ezport.cs
* \Author Suikan
* \brief Interface for ezPort by Amontec JTAG 
* 
* Contains basic interface class for Amontec JTAG Key or its compatible. 
* This file supports "Dicrect ezPort connection" between JTAGKEY and Freescale MCF5223x.
* 
*/

namespace Kinetis.JtagKey
{
   namespace JtagKey
   {
      public class EzPort : DirectSpi
      {
         /**
             * \brief constructor
             * \param aDivisor  12/(1+aDivisor)/2 MHz.
             * 
             * Setup given parameter into the instans variable to be ready to open the device.
             */

         public EzPort(int aDivisor)
            : base(aDivisor)
         {
         }

         /**
             * \brief Set the CS pin as "L"
             */

         public override void AssertCs(LatchOn latchingEdge)
         {
            var command = new byte[256];
            var index = 0; // current index of command buffer
            int transfered; // how much data is transfered


            var initClock = (byte) (latchingEdge == LatchOn.Falling ? 0x01 : 0x00);

            // construct instruction stream for MPSSE
            // Set start condition
            command[index++] = 0x80; // Output GPIO
            command[index++] = (byte) (0x08 | initClock); // BufferEnable, TMS = H, TDI = L
            command[index++] = 0x1B; // GPIOL0, TMS, TDI, CLK is Out, TDO is In.

            // Assert TMS
            command[index++] = 0x80; // Output GPIO
            command[index++] = (byte) (0x00 | initClock); // BufferEnable, TMS = L, TDI = L
            command[index++] = 0x1B; // GPIOL0, TMS, TDI, CLK is Out, TDO is In.


            Ftdi.Ftdi.Write(Handle, command, index, out transfered);
         }

         /**
             * \brief Set the CS pin as "H"
             */

         public override void DeassertCs(LatchOn latchingEdge)
         {
            var command = new byte[256];
            var index = 0; // current index of command buffer
            int transfered; // how much data is transfered

            var initClock = (byte) (latchingEdge == LatchOn.Falling ? 0x01 : 0x00);

            // Deassert TMS
            command[index++] = 0x80; // Output GPIO
            command[index++] = (byte) (0x08 | initClock); // BufferEnable, TMS = H, TDI = L
            command[index++] = 0x1B; // GPIOL0, TMS, TDI, CLK is Out, TDO is In.

            Ftdi.Ftdi.Write(Handle, command, index, out transfered);
         }
      }
   }
}
