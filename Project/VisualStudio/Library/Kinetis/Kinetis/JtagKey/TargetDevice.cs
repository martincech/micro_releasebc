using System;

namespace Kinetis.JtagKey
{
   /// <summary>
   /// \brief Basic class of JTAG target sepcification
   /// 
   /// This class describes JTAG target specification. For example, safe settings of boundary 
   /// classes, instructions and idcode. Because is designed for JTAG-SPI control, 
   /// this definition includes SPI functionality mapping on boundary register cell.
   /// </summary>
   public class TargetDevice
   {
      /// Target name for end user. This is showed on UI
      public string Name;

      /// Target Vendor 
      public string Vendor;

      /// Board name
      public string Board;

      /// 32bit ID code, which should be returned from target
      public int RegIdcode;

      /// The number of bits in the boundary register
      public int BoundaryBitlength;

      /// Boundary register with safe setting. Note that Left is MSB.
      public byte[] RegBoundary;

      /// The number of bits in the instruction
      public int InstBitlength;

      /// Bit pattern of Extest instruction
      public byte[] InstExtest;

      /// Bit pattern of IDCODE instruction
      public byte[] InstIdcode;

      /// Bit pattern of Sample/Preload instruction
      public byte[] InstSample;

      /// Boundary cell number of SPI_CLOKC output
      public int SpiClock;

      /// Boundary cell number of MOSI output
      public int SpiMosi;

      /// Boundary cell number of MISO input
      public int SpiMiso;

      /// Boundary cell numbers of cs 
      public int SpiCs;

      /// Not listed if this value is true
      public Boolean Hidden;
   }
}