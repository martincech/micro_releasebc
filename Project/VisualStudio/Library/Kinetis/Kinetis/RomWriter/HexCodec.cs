/*
 * Copyright 2008-2009 The Asagao Project. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright 
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright 
 *     notice, this list of conditions and the following disclaimer in 
 *     the documentation and/or other materials provided with the 
 *     distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE ASAGAO PROJECT ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ASAGAO PROJECT OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation 
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of the Asagao Project.
 * 
 * http://sourceforge.net/projects/asagao/

 * \file hexcodec.cs
 * \brief Intel Hex/Motorola S <-> Binary converter
*/

using System;
using System.IO;

namespace Kinetis.RomWriter
   {
      ///<summary>
      ///\brief Intel Hex/Motorola S <-> Binary converter
      ///
      ///Converting text file and binary. The text file format should be 
      ///one of the intel Hex or motorola S format.
      ///
      ///The binary size is limited to 16MByte
      ///
      ///</summary>
      public class HexCodec
      {
         private const int MEMORY_SIZE = 16*1024*1024; // 16MByte


         ///<summary>
         ///\brief binary data.
         ///
         ///Binary data which have to be converted from/to text file. This is
         ///initialized to all 0xFF by constructor.
         ///</summary>
         public byte[] data = new byte[MEMORY_SIZE]; // 16MB

         ///<summary>
         ///\brief constructor
         ///
         ///Iniitialize internal variable.
         ///</summary>
         public HexCodec()
         {
            // initialize data by 0xFF
            for (var i = 0; i < data.Length; i++)
            {
               data[i] = 0xff;
            }
         }

         ///<summary>
         ///\brief convert text file to binary
         ///
         ///Open the given file and convert it to binary file. The input file
         ///is assumed as intel hex or motorola s format. In case of any format
         ///error is happen, RecordException is raised.
         ///</summary>
         public void Decode(string filename)
         {
            var sr = new StreamReader(filename);
            long addressBase = 0; // offset address for intel hex format.
            string record;

            while ((record = sr.ReadLine()) != null)
            {
               if (record.Length != 0) // if empty, skip it.
               {
                  if (record[0] == ':') // Intel Hex
                  {
                     // Check sum calc
                     var checkSum = 0;
                     for (var i = 1; i < record.Length; i += 2)
                        checkSum += Convert.ToInt32(record.Substring(i, 2), 16);
                     if ((checkSum & 0xFF) != 0)
                        throw new RecordException(Properties.Resources.CheckSumWrong + record);

                     // Retrieve record information.
                     var numDataBytes = Convert.ToInt32(record.Substring(1, 2), 16);
                     var dataAddress = addressBase + Convert.ToInt64(record.Substring(3, 4), 16);


                     // Processing record
                     switch (record.Substring(7, 2))
                     {
                        case "00": // data record
                           if (dataAddress + numDataBytes - 1 > MEMORY_SIZE) // is recordEnd exeed memory size?
                              throw new RecordException(Properties.Resources.AddressExceeds + record);
                           for (var i = 0; i < numDataBytes; i++)
                           {
                              data[dataAddress] = (byte) Convert.ToInt32(record.Substring(9 + i*2, 2), 16);
                              dataAddress++;
                           }
                           break;
                        case "01": // end record. do nothing
                           break;
                        case "02": // segment address
                           if (numDataBytes != 2)
                              throw new RecordException(Properties.Resources.WrongDataFieldSize + record);
                           addressBase = Convert.ToInt64(record.Substring(9, 4), 16) << 4;
                           break;
                        case "03": // start address record. do nothing
                           break;
                        case "04": // extended linear segment address
                           if (numDataBytes != 2)
                              throw new RecordException(Properties.Resources.WrongDataFieldSize + record);
                           addressBase = Convert.ToInt64(record.Substring(9, 4), 16) << 16;
                           break;
                        case "05": // linear start address. do nothing
                           break;
                        default:
                           throw new RecordException(Properties.Resources.WrongRecordType + record);
                     }
                  }
                  else if (Char.ToUpper(record[0]) == 'S') // Motorola S
                  {
                     // check sum calc
                     var checkSum = 0;
                     for (var i = 2; i < record.Length; i += 2)
                        checkSum += Convert.ToInt32(record.Substring(i, 2), 16);
                     if ((checkSum & 0xFF) != 0xFF)
                        throw new RecordException(Properties.Resources.WrongCheckSum + record);

                     // Retrieve record information.
                     var numDataBytes = Convert.ToInt32(record.Substring(2, 2), 16);
                     long dataAddress;


                     switch (record[1]) // record type
                     {
                        case '0': // start record. do nothing.
                           break;
                        case '1': // data record. 16bit address
                           dataAddress = Convert.ToInt64(record.Substring(4, 4), 16); // address length is 4char
                           numDataBytes -= 3; // data size[byte]=numDataByte-addressLenght(2)-1
                           if (dataAddress + numDataBytes - 1 > MEMORY_SIZE) // is recordEnd exeed memory size?
                              throw new RecordException(Properties.Resources.AddressExceeds + record);
                           for (var i = 0; i < numDataBytes; i++)
                           {
                              // data start is 4+4
                              data[dataAddress] = (byte) Convert.ToInt32(record.Substring(8 + i*2, 2), 16);
                              dataAddress++;
                           }
                           break;
                        case '2': // data record. 24bit address
                           dataAddress = Convert.ToInt64(record.Substring(4, 6), 16); // address length is 6char
                           numDataBytes -= 4; // data size[byte]=numDataByte-addressLenght(3)-1
                           if (dataAddress + numDataBytes - 1 > MEMORY_SIZE) // is recordEnd exeed memory size?
                              throw new RecordException(Properties.Resources.AddressExceeds + record);
                           for (var i = 0; i < numDataBytes; i++)
                           {
                              // data start at 4+6
                              data[dataAddress] = (byte) Convert.ToInt32(record.Substring(10 + i*2, 2), 16);
                              dataAddress++;
                           }
                           break;
                        case '3': // data record. 32bit address
                           dataAddress = Convert.ToInt64(record.Substring(4, 8), 16); // address length is 8 char
                           numDataBytes -= 5; // data size[byte]=numDataByte-addressLenght(2)-1
                           if (dataAddress + numDataBytes - 1 > MEMORY_SIZE) // is recordEnd exeed memory size?
                              throw new RecordException(Properties.Resources.AddressExceeds + record);
                           for (var i = 0; i < numDataBytes; i++)
                           {
                              // data start at 4+8
                              data[dataAddress] = (byte) Convert.ToInt32(record.Substring(12 + i*2, 2), 16);
                              dataAddress++;
                           }
                           break;
                        case '4': // symbol record. do nothing
                        case '5': // total record (16bit). do nothing
                        case '6': // total record (24bit). do nothing
                        case '7': // End of S3. do nothing
                        case '8': // End of S2. do nothing
                        case '9': // End of S1. do nothing
                           break;
                        default:
                           throw new RecordException(Properties.Resources.WrongRecordType + record);
                     }
                  }
                  else
                     throw new RecordException(Properties.Resources.UknownRecord + record);
               }
            }

            sr.Close();
         }
      }
   }