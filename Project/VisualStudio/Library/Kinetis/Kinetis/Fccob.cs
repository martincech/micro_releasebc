//******************************************************************************
//
//    Fccob.cs            Flash common command object over EzPort
//    Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

using System;
using System.Linq;
using Kinetis.JtagKey;

namespace Kinetis {
   public class Fccob {
      protected Flash Flash;

      public Fccob(Flash flash) {
         Flash = flash;
      }

      const byte WRFCCOB = 0xBA;
      const byte READFCCOB = 0xBB;

      const byte PGMONCE = 0x43;
      const byte READONCE = 0x41;

      const int PROGRAM_ONCE_SIZE = 64;
      const int PROGRAM_ONCE_FRAGMENT = 4;

      const int WREN = 0x06;

      //-----------------------------------------------------------------------------
      // Program once
      //-----------------------------------------------------------------------------

      public bool ProgramOnce(int address, byte[] data)
      // Programs <Address> with <Data>, Address aligned to PROGRAM_ONCE_FRAGMENT
      {
         if(address % PROGRAM_ONCE_FRAGMENT != 0) {
            return false;
         }
         if(data.Length == 0) {
            return true;
         }
         var currentAddress = address / PROGRAM_ONCE_FRAGMENT;
         var length = data.Length;
         var dataPtr = 0;
         while(length != 0) {
            if(currentAddress >= PROGRAM_ONCE_SIZE / PROGRAM_ONCE_FRAGMENT) {
               return false;
            }

            var fragmentLength = length >= PROGRAM_ONCE_FRAGMENT ? PROGRAM_ONCE_FRAGMENT : length;

            var cmd = new byte[] { PGMONCE, (byte)currentAddress, 0, 0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

            for(var i = 0; i < fragmentLength; i++) {
               cmd[i + 4] = data[dataPtr++];
            }

            if(!SendCmd(cmd)) {
               return false;
            }

            currentAddress++;
            length -= fragmentLength;
         }
         return true;
      } // ProgramOnce

      //-----------------------------------------------------------------------------
      // Read once
      //-----------------------------------------------------------------------------

      public bool ReadOnce(int address, ref byte[] data)
         // Reads <Address>
      {
         var length = data.Length;
         if(length == 0) {
            return true;
         }

         var ignoreCount = address % PROGRAM_ONCE_FRAGMENT;
         var dataPtr = 0;
         while(length != 0) {
            if(address >= PROGRAM_ONCE_SIZE) {
               return false;
            }

            var cmd = new[] { READONCE, (byte)(address / PROGRAM_ONCE_FRAGMENT) };

            if(!SendCmd(cmd)) {
               return false;
            }

            var reply = new byte[4 + PROGRAM_ONCE_FRAGMENT];

            if(!ReceiveReply(reply)) {
               return false;
            }

            if(reply[0] != READONCE) {
               return false;
            }

            var fragmentLength = length >= PROGRAM_ONCE_FRAGMENT ? PROGRAM_ONCE_FRAGMENT : length;

            if(4 - ignoreCount < fragmentLength) {
               fragmentLength = PROGRAM_ONCE_FRAGMENT - ignoreCount;
            }

            Buffer.BlockCopy(reply, 4 + ignoreCount, data, dataPtr, fragmentLength);

            length -= fragmentLength;
            dataPtr += fragmentLength;
            ignoreCount = 0;

            address += PROGRAM_ONCE_FRAGMENT;
         }
         return true;
      } // ReadOnce

      //*****************************************************************************

      //-----------------------------------------------------------------------------
      // Send FCCOB command via EzPort
      //-----------------------------------------------------------------------------

      protected bool SendCmd(byte[] cmd)
         // Write FCCOB
      {
         if(cmd.Length > 12) {
            return false;
         }

         var ezPortCmd = Enumerable.Repeat((byte)0xFF, 13).ToArray();
         Buffer.BlockCopy(cmd, 0, ezPortCmd, 1, cmd.Length);
         ezPortCmd[0] = WRFCCOB;

         while (Flash.Busy)
         {
         }
         Flash.EzPort.Transfer(new byte[] { WREN }, null, LatchOn.Rising);
         Flash.EzPort.Transfer(ezPortCmd, null, LatchOn.Rising);
         
         return true;
      } // SendCmd

      //-----------------------------------------------------------------------------
      // Read FCCOB via EzPort
      //-----------------------------------------------------------------------------

      protected bool ReceiveReply(byte[] reply)
         // Read FCCOB
      {
         var ezPortCmd = new byte[] { READFCCOB, 0xFF }; // Dummy 
         var ezPortReply = new byte[reply.Length + 2];
         if(reply.Length == 0) {
            return false;
         }

         if(reply.Length > 12) {
            return false;
         }

         while (Flash.Busy)
         {
         }
         Flash.EzPort.Transfer(ezPortCmd, ezPortReply, LatchOn.Rising);
         Buffer.BlockCopy(ezPortReply, 2, reply, 0, reply.Length);
         return true;
      } // ReceiveReply
   }
}