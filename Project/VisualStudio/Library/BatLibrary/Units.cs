﻿//******************************************************************************
//
//   Units.cs       Units definitions
//   Version 1.0
//
//******************************************************************************

namespace BatLibrary
{
   /// <summary>
   /// Weighing units
   /// </summary>
   public enum Units
   {
      KG,
      G,
      LB,
   };
}