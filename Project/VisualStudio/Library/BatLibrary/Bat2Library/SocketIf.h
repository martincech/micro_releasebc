//******************************************************************************
//
//   SocketIf.h      Abstract socket interface
//   Version 1.0     (c) Veit Electronics
//
//******************************************************************************

#ifndef __SocketIf_H__
#define __SocketIf_H__

#include "Remote/Socket.h"

class SocketIf
{
public:
   virtual ~SocketIf(){}
   virtual byte State(void) = 0;
   // Gets state of socket

   virtual TYesNo Receive(void *Buffer, int Size) = 0;
   // Receive into <Buffer> with <Size>

   virtual int ReceiveSize(void) = 0;
   // Gets number of received bytes

   virtual TYesNo Send(const void *Buffer, int Size) = 0;
   // Send <Buffer> with <Size>

   virtual void Close(void) = 0;
   // Close socket

   virtual byte Permission(void) = 0;
   // Permission
};

#endif // __SocketIf_H__
