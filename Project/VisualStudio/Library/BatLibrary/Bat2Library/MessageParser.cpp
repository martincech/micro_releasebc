#include "MessageParser.h"
#include "Time\uDateTime.c"
#include "Time\uDate.c"
#include "Time\uTime.c"
#include "Crc\Crc.c"

using namespace System::Runtime::InteropServices;
using namespace System;
using namespace Bat2Library;

bool MessageParser::Parse(array<System::Byte> ^data, [Out] PublishedData^% parsedData)
{
	try{
		pin_ptr<System::Byte> pptr = &data[0];
		Bat2Library::PublishedData ^dataVariable = gcnew Bat2Library::PublishedData();
		TBinaryStatistics *statsPtr = (TBinaryStatistics *)pptr;

		word *crcPtr;
		if (data->Length == ExpectedCompleteMessageSize)
		{
			crcPtr = &statsPtr->Crc16;
		}
		else
		if (data->Length == ExpectedMaleMessageSize)
		{
			crcPtr = &statsPtr->Female.Crc16;
		}
		else
		{
			parsedData = nullptr;
			return false;
		}

		int size = (unsigned)crcPtr - (unsigned)statsPtr;
		word calcCrc = CalculateCrc16(pptr, size, 0);
		if (calcCrc != *crcPtr || calcCrc == 0)
		{
			parsedData = nullptr;
			return false;
		}

		UDateTime dateTime;

		uDateTime(&dateTime, statsPtr->DateTime);

		DateTime date(dateTime.Date.Year + 2000, dateTime.Date.Month, dateTime.Date.Day,
			dateTime.Time.Hour, dateTime.Time.Min, dateTime.Time.Sec);

		dataVariable->DateTime = date;
		dataVariable->Day = statsPtr->Day;
		GenderStats ^Male = gcnew GenderStats();
		Male->Average = statsPtr->Male.Average;
		Male->Count = statsPtr->Male.Count;
		Male->Cv = statsPtr->Male.Cv;
		Male->Gain = statsPtr->Male.Gain;
		Male->Sigma = statsPtr->Male.Sigma;
		Male->Target = statsPtr->Male.Target;
		Male->Uniformity = statsPtr->Male.Uniformity;
		dataVariable->Male = Male;

		if (data->Length > ExpectedMaleMessageSize)
		{
			GenderStats ^Female = gcnew GenderStats();
			Female->Average = statsPtr->Female.Female.Average;
			Female->Count = statsPtr->Female.Female.Count;
			Female->Cv = statsPtr->Female.Female.Cv;
			Female->Gain = statsPtr->Female.Female.Gain;
			Female->Sigma = statsPtr->Female.Female.Sigma;
			Female->Target = statsPtr->Female.Female.Target;
			Female->Uniformity = statsPtr->Female.Female.Uniformity;
			dataVariable->Female = Female;
		}
		dataVariable->SerialNumber = statsPtr->SerialNumber;
		dataVariable->Day = statsPtr->Day;
		parsedData = dataVariable;
	}
	catch (Exception^ e)
	{
		parsedData = nullptr;
		return false;
	}
	return true;
}

array<System::Byte>^ MessageParser::GenerateData(PublishedData ^dataVariable)
{
	if (dataVariable == nullptr)
	{
		return nullptr;
	}
	array<System::Byte> ^data = gcnew array<System::Byte>(ExpectedCompleteMessageSize);

	try{
		pin_ptr<System::Byte> pptr = &data[0];
		TBinaryStatistics *statsPtr = (TBinaryStatistics *)pptr;


		UDateTime dateTime;

		if (dataVariable->DateTime.Year > 100)
		{
			dateTime.Date.Year = dataVariable->DateTime.Year - 2000;
		}
		else
		{
			dateTime.Date.Year = dataVariable->DateTime.Year;
		}
		dateTime.Date.Month = dataVariable->DateTime.Month;
		dateTime.Date.Day = dataVariable->DateTime.Day;

		dateTime.Time.Hour = dataVariable->DateTime.Hour;
		dateTime.Time.Min = dataVariable->DateTime.Minute;
		dateTime.Time.Sec = dataVariable->DateTime.Second;

		UDateTimeGauge dateTimeGague = uDateTimeGauge(&dateTime);
		statsPtr->DateTime = dateTimeGague;
		statsPtr->Day = dataVariable->Day;
		statsPtr->SerialNumber = dataVariable->SerialNumber;
		statsPtr->Male.Average = dataVariable->Male->Average;
		statsPtr->Male.Count = dataVariable->Male->Count;
		statsPtr->Male.Cv = dataVariable->Male->Cv;
		statsPtr->Male.Gain = dataVariable->Male->Gain;
		statsPtr->Male.Sigma = dataVariable->Male->Sigma;
		statsPtr->Male.Target = dataVariable->Male->Target;
		statsPtr->Male.Uniformity = dataVariable->Male->Uniformity;

		word *crcPtr;
		int Size;
		if (dataVariable->Female != nullptr)
		{
			TBinaryGenderStats Female;
			Female.Average = dataVariable->Female->Average;
			Female.Count = dataVariable->Female->Count;
			Female.Cv = dataVariable->Female->Cv;
			Female.Gain = dataVariable->Female->Gain;
			Female.Sigma = dataVariable->Female->Sigma;
			Female.Target = dataVariable->Female->Target;
			Female.Uniformity = dataVariable->Female->Uniformity;
			statsPtr->Female.Female = Female;
			crcPtr = &statsPtr->Crc16;
		}
		else {
			crcPtr = &statsPtr->Female.Crc16;
		}

		Size = (unsigned)crcPtr - (unsigned)statsPtr;

		*crcPtr = CalculateCrc16(pptr, Size, 0);
		Size += sizeof(statsPtr->Crc16);
		Array::Resize(data, Size);
	}
	catch (Exception^ e)
	{
		return nullptr;
	}
	return data;
}
