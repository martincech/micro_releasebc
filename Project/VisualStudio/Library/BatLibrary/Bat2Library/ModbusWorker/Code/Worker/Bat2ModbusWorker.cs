﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using Modbus;
using Usb.Helpers;

namespace Bat2Library.ModbusWorker.Worker
{
   /// <summary>
   /// One slave connected using Modbus
   /// </summary>
   public struct Bat2Slave
   {
      /// <summary>
      /// COM port number
      /// </summary>
      public int PortNumber;

      /// <summary>
      /// Scale address
      /// </summary>
      public int Address;
   }

   public enum WorkerEvent
   {
      INIT,               // Inicializace
      READ_OK,            // Nacteny stav vahy na dane pozici
      READ_ERROR,         // Chyba cteni stavu vahy na dane pozici
      CANCEL              // Rucni preruseni
   }

   /// <summary>
   /// Data passed to the main thread
   /// </summary>
   public struct WorkerData
   {
      public WorkerEvent Event;
      public CurrentState State;

      /// <summary>
      /// Counter of all sent messages
      /// </summary>
      public int MessageCounter;

      /// <summary>
      /// Counter of wrong replies
      /// </summary>
      public int ErrorCounter;

      /// <summary>
      /// Port
      /// </summary>
      public int Port;

      /// <summary>
      /// Address
      /// </summary>
      public int Address;
   }

   public struct WorkerInitData
   {
      public List<Bat2Slave> ScaleCollection;
      public Modbus.Modbus Modbus;
      public int NumberOfAttempts;
      public bool ReadAll;
   }

   /// <summary>
   /// Worker reading online status of all scales
   /// </summary>
   public class Bat2ModbusOnlineWorker
   {
      /// <summary>
      /// Background worker used for the operation
      /// </summary>
      private BackgroundWorker backgroundWorker;

      /// <summary>
      /// Collection of scales connected to the PC
      /// </summary>
      private List<Bat2Slave> scaleCollection;

      /// <summary>
      /// Modbus connection
      /// </summary>
      private Modbus.Modbus modbus;

      /// <summary>
      /// Max number of attempts in communication
      /// </summary>
      private int numberOfAttempts;

      /// <summary>
      /// Returns true if the worker is still running
      /// </summary>
      public bool IsBusy
      {
         get
         {
            return backgroundWorker.IsBusy;
         }
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="backgroundWorker"></param>
      /// <param name="scaleCollection"></param>
      public Bat2ModbusOnlineWorker(ProgressChangedEventHandler progressChanged, RunWorkerCompletedEventHandler runWorkerCompleted, int baudRate, Uart.Parity parity,
                                    int dataBits, int totalTimeout, int intercharacterTimeout, int numberOfAttempts, ModbusPacket.PacketMode mode, List<Bat2Slave> scaleCollection)
      {
         // Vytvorim background worker
         backgroundWorker = new BackgroundWorker();
         backgroundWorker.WorkerReportsProgress = true;
         backgroundWorker.WorkerSupportsCancellation = true;
         backgroundWorker.DoWork += new DoWorkEventHandler(Bat2ModbusOnlineWorkerRaw.DoWork);
         backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(progressChanged);
         backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(runWorkerCompleted);

         this.scaleCollection = scaleCollection;
         this.numberOfAttempts = numberOfAttempts;
         modbus = new Modbus.Modbus(baudRate, parity, dataBits, totalTimeout, intercharacterTimeout, mode);

      }

      /// <summary>
      /// Start operation
      /// </summary>
      public void Start(EloOperation operation)
      {
         WorkerInitData initData;
         initData.ScaleCollection = scaleCollection;
         initData.Modbus = modbus;
         initData.NumberOfAttempts = numberOfAttempts;
         initData.ReadAll = true;
         if (operation != EloOperation.Read)
         {
            initData.ReadAll = false;
         }
         backgroundWorker.RunWorkerAsync(initData);
      }

      /// <summary>
      /// Stop operation
      /// </summary>
      public void Stop()
      {
         backgroundWorker.CancelAsync();
      }
   }

   /// <summary>
   /// Raw Modbus functions for BackgroundWorker
   /// </summary>
   public class Bat2ModbusOnlineWorkerRaw
   {

      /// <summary>
      /// Update scale status and report it to the UI
      /// </summary>
      static void UpdateStatus(int scaleIndex, WorkerEvent workerEvent, CurrentState newState, int messageCount, int errorCount, BackgroundWorker worker, int port, int address)
      {
         // Ulozim si novou udalost
         WorkerData workerData = new WorkerData();
         workerData.Event = workerEvent;
         if (newState != null)
         {
            workerData.State = new CurrentState(newState);
         }
         workerData.MessageCounter = messageCount;
         workerData.ErrorCounter = errorCount;
         workerData.Port = port;
         workerData.Address = address;

         // Pozor, do UI musim predat kopii stavu. Pokud bych predal jen ukazatel, UI by s timto stavem pracovalo a postupne ho zobrazovalo,
         // ale thread by s tim samym stavem pracoval take a zmenil by ho na pozadi.
         worker.ReportProgress(scaleIndex, workerData);
      }

      public static void DoWork(object sender, DoWorkEventArgs e)
      {
         BackgroundWorker worker = (BackgroundWorker)sender;        // Preberu worker, ktery tuto fci vola
         WorkerInitData initData = (WorkerInitData)e.Argument;

         // Musi zadat aspon 1 vahu
         if (initData.ScaleCollection.Count == 0)
         {
            e.Cancel = true;
            return;
         }

         // Vytvorim seznam vah
         List<Bat2Modbus> modbusScaleCollection = new List<Bat2Modbus>();
         foreach (Bat2Slave bat2Slave in initData.ScaleCollection)
         {
            modbusScaleCollection.Add(new Bat2Modbus(bat2Slave.PortNumber, (byte)bat2Slave.Address, initData.Modbus));
         }

         foreach (var bat2Modbus in modbusScaleCollection)
         {
            // Otevru port, pokud je otevreny z predchozi vahy, automaticky ho zavre
            bat2Modbus.Open();

            // Nacteni aktualniho stavu
            int attempt;        // Aktualni cislo pokusu
            for (attempt = 0; attempt < initData.NumberOfAttempts; attempt++)
            {
               if (worker.CancellationPending)
               {
                  break;
               }
               if (!bat2Modbus.ReadConfig())
               {
                  continue;       // Zkusim dalsi pokus
               }

               if (initData.ReadAll)
               {

                  if (worker.CancellationPending)
                  {
                     break;
                  }
                  if (!bat2Modbus.ReadPause())
                  {
                     continue; // Zkusim dalsi pokus
                  }
                  if (worker.CancellationPending)
                  {
                     break;
                  }
                  if (!bat2Modbus.ReadDateTime())
                  {
                     continue; // Zkusim dalsi pokus
                  }
                  if (worker.CancellationPending)
                  {
                     break;
                  }
                  if (!bat2Modbus.ReadToday())
                  {
                     continue; // Zkusim dalsi pokus
                  }
                  if (worker.CancellationPending)
                  {
                     break;
                  }
               }

               // Nacetl jsem vse, predam nacteny vysledek
               UpdateStatus(modbusScaleCollection.IndexOf(bat2Modbus), WorkerEvent.READ_OK, bat2Modbus.CurrentState, bat2Modbus.MessageCounter, bat2Modbus.ErrorCounter, worker, bat2Modbus.PortNumber,bat2Modbus.Address);
               break;          // Preskocim na dalsi vahu
            }
            bat2Modbus.Close();   

            // Pokud chce ukoncit, koncim
            if (worker.CancellationPending)
            {
               UpdateStatus(modbusScaleCollection.IndexOf(bat2Modbus), WorkerEvent.CANCEL,null, bat2Modbus.MessageCounter, bat2Modbus.ErrorCounter, worker, bat2Modbus.PortNumber, bat2Modbus.Address);
               e.Cancel = true;
               break;         
            }
            
            if (attempt == initData.NumberOfAttempts)
            {
               UpdateStatus(modbusScaleCollection.IndexOf(bat2Modbus), WorkerEvent.READ_ERROR, null, bat2Modbus.MessageCounter, bat2Modbus.ErrorCounter, worker, bat2Modbus.PortNumber, bat2Modbus.Address);
               Thread.Sleep(50);
            }
         }//while
      }
   }

}
