﻿using Modbus;
using Usb.Helpers;

namespace Bat2Library.ModbusWorker {
    
    /// <summary>
    /// Modbus scales connection
    /// </summary>
    public struct ModbusScalesConfig {
        /// <summary>
        /// COM port
        /// </summary>
        public int PortNumber;
        
        /// <summary>
        /// Address of first scale
        /// </summary>
        public int FirstAddress;
        
        /// <summary>
        /// Address of last scale
        /// </summary>
        public int LastAddress;
    }

    /// <summary>
    /// Modbus protocol
    /// </summary>
    public struct ModbusProtocolConfig {
        /// <summary>
        /// Protocol type
        /// </summary>
       public ModbusPacket.PacketMode Protocol;

        /// <summary>
        /// Baud rate
        /// </summary>
        public int Speed;

        /// <summary>
        /// Parity
        /// </summary>
        public Uart.Parity Parity;

        /// <summary>
        /// Reply timeout in miliseconds
        /// </summary>
        public int ReplyTimeout;

        /// <summary>
        /// Silent interval in miliseconds (only in RTU mode)
        /// </summary>
        public int SilentInterval;

        /// <summary>
        /// Max number of attampts in communication
        /// </summary>
        public int NumberOfAttempts;
    }
    
    /// <summary>
    /// Complete setup
    /// </summary>
    public class Setup {
        public static int MIN_RS485_ADDRESS         = 1;
        public static int MAX_RS485_ADDRESS         = 247;
        public static int MIN_RS485_REPLY_DELAY     = 0;
        public static int MAX_RS485_REPLY_DELAY     = 9999;
        public static int MIN_RS485_SILENT_INTERVAL = 1;
        public static int MAX_RS485_SILENT_INTERVAL = 255;
        public static int MAX_DAY                   = 999;
        
        /// <summary>
        /// Parameters of the Modbus protocol
        /// </summary>
        public ModbusProtocolConfig ModbusProtocol;

        /// <summary>
        /// Scales connection
        /// </summary>
        public ModbusScalesConfig ModbusScales;

        /// <summary>
        /// Constructor
        /// </summary>
        public Setup() {
            // Nastavim defaulty
            SetDefaults();
        }
        
        /// <summary>
        /// Set default values
        /// </summary>
        public void SetDefaults() {
            ModbusProtocol.Protocol         = ModbusPacket.PacketMode.RTU;
            ModbusProtocol.Speed            = 9600;
            ModbusProtocol.Parity           = Uart.Parity.Even;
            ModbusProtocol.ReplyTimeout     = 1000;
            ModbusProtocol.SilentInterval   = 50;
            ModbusProtocol.NumberOfAttempts = 2;

            ModbusScales.PortNumber   = 1;
            ModbusScales.FirstAddress = -1;
            ModbusScales.LastAddress  = -1;
        }
    }
}
