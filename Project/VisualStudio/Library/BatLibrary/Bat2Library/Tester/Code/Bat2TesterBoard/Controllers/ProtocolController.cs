﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Bat2TesterBoard.Interfaces;

namespace Bat2TesterBoard.Controllers
{
   public class ProtocolController
   {
      private readonly Stream _stream;
      private const int BufSize = 10;

      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public ProtocolController(Stream stream)
      {
         _stream = stream;
      }

      public byte[] ReadRtc()
      {
         var t = ReadRtcAsync();
         t.Wait();
         return t.Result;
      }

      public Task<byte[]> ReadRtcAsync()
      {
         return SendAndReceiveAsync(Commands.Rtc);
      }

      public byte[] ReadDisplay()
      {
         var t = ReadDisplayAsync();
         t.Wait();
         return t.Result;
      }

      public Task<byte[]> ReadDisplayAsync()
      {
         return SendAndReceiveAsync(Commands.Display);
      }

      public byte[] ReadKeyboard()
      {
         var t = ReadKeyboardAsync();
         t.Wait();
         return t.Result;
      }

      public Task<byte[]> ReadKeyboardAsync()
      {
         return SendAndReceiveAsync(Commands.Keyboard);
      }

      public byte[] ReadAdc()
      {
         var t = ReadAdcAsync();
         t.Wait();
         return t.Result;
      }

      public Task<byte[]> ReadAdcAsync()
      {
         return SendAndReceiveAsync(Commands.Adc);
      }

      public byte[] ReadNwmWrite()
      {
         var t = ReadNwmWriteAsync();
         t.Wait();
         return t.Result;
      }

      public Task<byte[]> ReadNwmWriteAsync()
      {
         return SendAndReceiveAsync(Commands.NwmWrite);
      }

      public byte[] ReadNwmCheck()
      {
         var t = ReadNwmCheckAsync();
         t.Wait();
         return t.Result;
      }

      public Task<byte[]> ReadNwmCheckAsync()
      {
         return SendAndReceiveAsync(Commands.NwmCheck);
      }

      #region Private helper methods

      private Task<byte[]> SendAndReceiveAsync(Commands command)
      {
         int timeout;
         switch (command)
         {
            case Commands.NwmCheck:
            case Commands.NwmWrite:
               timeout = (int) Timeouts.Extended;
               break;
            default:
               timeout = (int) Timeouts.Classic;
               break;
         }

         var data = new[] {(byte) command};

         var readTask = StartReading(timeout);
         _stream.Write(data, 0, data.Length);
         return readTask;
      }

      private Task<byte[]> StartReading(int waitTime)
      {
         var task = new Task<byte[]>(() =>
         {
            var buffer = new byte[BufSize];
            var buf = new List<byte>();
            int readSize;
            if (_stream.CanTimeout)
            {
               _stream.ReadTimeout = waitTime;
               do
               {
                  readSize = _stream.Read(buffer, 0, BufSize);
                  if (readSize != 0)
                  {
                     buf.AddRange(buffer.Take(readSize));
                  }
               } while (readSize == BufSize);
               return buf.ToArray();
            }
            // stream can't timeout, do active read with timer
            var timer = new Timer(waitTime);
            var timeout = false;
            timer.Elapsed += (sender, args) =>
            {
               timeout = true;
            };
            timer.Start();
            do
            {
               readSize = _stream.Read(buffer, 0, BufSize);
               if (readSize != 0)
               {
                  buf.AddRange(buffer.Take(readSize));
               }
            } while ((readSize == 0 || readSize == BufSize) && !timeout);
            timer.Close();
            return buf.ToArray();
         });
         task.Start();
         return task;
      }

      #endregion
   }
}
