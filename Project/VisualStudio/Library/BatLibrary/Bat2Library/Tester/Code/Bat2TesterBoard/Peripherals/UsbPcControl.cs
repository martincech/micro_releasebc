using Bat2TesterBoard.Interfaces;

namespace Bat2TesterBoard.Peripherals
{
   /// <summary>
   ///  USB PC port controller
   /// </summary>
   public class UsbPcControl
   {

      #region Private fields

      private readonly IOController gpio;
      
      #endregion

      /// <summary>
      ///  Constructor
      /// </summary>
      /// <param name="gpio">Gpio controller</param>
      public UsbPcControl(IOController gpio)
      {
         this.gpio = gpio;
      }

      public void UsbPower()
      {
         gpio.UsbAuxiliaryPowerPin= true;
      }

      public void AuxiliaryPower()
      {
         gpio.UsbAuxiliaryPowerPin= false;
      }

      public void PowerSwitchOn()
      {
         gpio.UsbPowerSwitchPin= true;
      }

      public void PowerSwitchOff()
      {
         gpio.UsbPowerSwitchPin= false;
      }

      public void DataSwitchOn()
      {
         gpio.UsbDataPin= true;
      }

      public void DataSwitchOff()
      {
         gpio.UsbDataPin= false;
      }
   }
}