namespace Bat2TesterBoard.Interfaces
{
   public interface IKey
   {
      void Push();
      void Release();
   }
}