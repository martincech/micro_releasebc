using Bat2TesterBoard.Interfaces;

namespace Bat2TesterBoard.Peripherals
{
   /// <summary>
   ///  Represents load cell simulator
   /// </summary>
   public class LoadCellControl
   {
      #region Private fields

      private readonly IOController gpio;

      #endregion


      /// <summary>
      ///  Constructor
      /// </summary>
      public LoadCellControl(IOController gpio)
      {
         this.gpio = gpio;
      }

      /// <summary>
      ///  Simulate 1st level
      /// </summary>
      public void Level1()
      {
         gpio.LoadCellPin = false;
      }

      /// <summary>
      ///  Simulate 2nd level
      /// </summary>
      public void Level2()
      {
         gpio.LoadCellPin = true;
      }
   }
}