﻿using System;
using System.Collections.Generic;
using Bat2Library.Bat2Old.DB.Helpers;

namespace Bat2Library.Bat2Old.DB
{
   public class Stat
   {
      public Stat()
      {
         FemaleHist = new List<Histogram>(Histogram.Capacity);
         MaleHist = new List<Histogram>(Histogram.Capacity);
      }

      public short DayNumber { get; set; }
      public DateTime DateTime { get; set; }
      public double FemaleStatXSum { get; set; }
      public double FemaleStatX2Sum { get; set; }
      public int FemaleStatCount { get; set; }
      public double MaleStatXSum { get; set; }
      public double MaleStatX2Sum { get; set; }
      public int MaleStatCount { get; set; }

      public ICollection<Histogram> FemaleHist { get; set; }
      public ICollection<Histogram> MaleHist { get; set; }

      public double FemaleYesterdayAverage { get; set; }
      public double MaleYesterdayAverage { get; set; }
      public double FemaleTarget { get; set; }
      public double MaleTarget { get; set; }
      public bool UseRealUni { get; set; }
      public short FemaleRealUni { get; set; }
      public short MaleRealUni { get; set; }
   }
}
