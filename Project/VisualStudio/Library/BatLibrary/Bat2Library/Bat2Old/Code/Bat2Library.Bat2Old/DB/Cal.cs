﻿namespace Bat2Library.Bat2Old.DB
{
   public class Cal
   {
      public short Platform { get; set; }
      public int CalZero { get; set; }
      public int CalRange { get; set; }
      public int Range { get; set; }
      public short Division { get; set; }
   }
}
