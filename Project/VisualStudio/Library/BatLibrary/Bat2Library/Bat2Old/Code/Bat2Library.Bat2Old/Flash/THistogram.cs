using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct THistogram
   {
      public System.Int32 Center; // Stredni hodnota, kolem ktere se histogram vytvari - je treba zadat pri inicializaci
      public System.Int32 Step; // Krok histogramu - je treba zadat pri inicializaci
      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.HISTOGRAM_SLOTS)] public System.UInt16[] Slot;

      public void Swap()
      {
         Center = Endian.SwapInt32(Center);
         Step = Endian.SwapInt32(Step);
         for (var i = 0; i < TConstants.HISTOGRAM_SLOTS; i++)
         {
            Slot[i] = Endian.SwapUInt16(Slot[i]);
         }
      }
   } // 86 bajtu
}