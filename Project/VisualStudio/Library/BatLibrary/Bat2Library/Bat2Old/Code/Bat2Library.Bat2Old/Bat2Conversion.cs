﻿using System;
using System.Collections.Generic;
using Bat2Library.Bat2Old.Constants;
using Bat2Library.Bat2Old.Flash;
using Bat2Library.Bat2Old.Flash.v111;
using Bat2Library.Bat2Old.Map;
using Bat2Library.Connection.Interface.Domain.Old;
using Utilities;

namespace Bat2Library.Bat2Old
{
   public static class Bat2Conversion
   {
      private const int FLASH_PAGES = 4096; // pages count
      private const int FLASH_VIRTUAL_PAGE = 512; // virtual page size
      private const int FLASH_SIZE = (FLASH_PAGES*FLASH_VIRTUAL_PAGE); // memory size

      /// <summary>
      /// Convert raw binary data to object representing flash memory 
      /// </summary>
      /// <param name="rawFlashData">raw binary data</param>
      /// <returns>object representing flash memory</returns>
      public static OldBat2DeviceData LoadFlash(byte[] rawFlashData)
      {
         if (rawFlashData == null)
         {
            return null;
         }

         if (rawFlashData.Length != FLASH_SIZE)
         {
            return null;
         }

         OldBat2DeviceData oldFlash = null;

         switch (FlashVersion.GetVersion(rawFlashData))
         {
            case FlashVersionE.FlashV111:
               var flash = StructConversion.ArrayToStruct<TFlash>(rawFlashData);
               flash.Swap();
               oldFlash = flash.Map();
               break;

            case FlashVersionE.FlashV150:
               var flash2 = StructConversion.ArrayToStruct<Flash.v150.TFlash>(rawFlashData);
               flash2.Swap();
               oldFlash = flash2.Map();
               break;

            case FlashVersionE.None:
               return null;
         }

         if (oldFlash != null)
         {
            oldFlash.Archive = LoadArchive(rawFlashData);
         }

         return oldFlash;
      }

      /// <summary>
      /// Convert raw binary data to object representing archive items in flash memory 
      /// </summary>
      /// <param name="rawFlashData">raw binary data</param>
      /// <returns>collection representing archive items in flash memory</returns>
      public static IEnumerable<OldArchiveDailyInfo> LoadArchive(byte[] rawFlashData)
      {
         var archives = new List<OldArchiveDailyInfo>();
         if (rawFlashData == null)
         {
            return archives;
         }

         var position = 0;

         //Check if data are whole flash or collection of archive items
         if (rawFlashData.Length == FLASH_SIZE)
         {
            position = TConstants.FL_ARCHIVE_BASE;
         }
         else if (rawFlashData.Length%TConstants.FL_ARCHIVE_DAY_SIZE != 0)
         {
            return archives;
         }

         while (true)
         {
            if (position + TConstants.FL_ARCHIVE_DAY_SIZE > rawFlashData.Length)
            {
               break;
            }

            var archive = new byte[TConstants.FL_ARCHIVE_DAY_SIZE];
            Array.Copy(rawFlashData, position, archive, 0, archive.Length);

            if ((archive[0] == TConstants.FL_ARCHIVE_EMPTY) || (archive[0] == TConstants.FL_ARCHIVE_FULL))
            {
               break;
            }

            var archiveItem = StructConversion.ArrayToStruct<TArchiveDailyInfo>(archive);
            archiveItem.Swap();
            archives.Add(archiveItem.Map());
            position += TConstants.FL_ARCHIVE_DAY_SIZE;
         }

         return archives;
      }


      /// <summary>
      /// Convert raw binary data to object representing configuration in flash memory 
      /// </summary>
      /// <param name="rawFlashData">raw binary data</param>
      /// <returns>object representing configuration in flash memory</returns>
      public static OldConfiguration LoadConfigSection(byte[] rawFlashData)
      {
         if (rawFlashData == null)
         {
            return null;
         }

         if (rawFlashData.Length < TConstants.FL_CONFIG_SIZE)
         {
            return null;
         }


         var rawConfig = new byte[TConstants.FL_CONFIG_SIZE];

         //check if rawFlashData contains entire flash or just config section
         if (rawFlashData.Length == TConstants.FL_CONFIG_SIZE)
         {
            rawConfig = rawFlashData;
         }
         else
         {
            //check if rawFlashData size is equal to size of whole flash memory 
            if (rawFlashData.Length != FLASH_SIZE)
            {
               return null;
            }
            Array.Copy(rawFlashData, rawConfig, TConstants.FL_CONFIG_SIZE);
         }

         OldConfiguration configOld = null;

         switch (FlashVersion.GetVersion(rawFlashData))
         {
            case FlashVersionE.FlashV111:
               var config = StructConversion.ArrayToStruct<TConfigSection>(rawConfig);
               config.Swap();
               configOld = config.Map();
               break;

            case FlashVersionE.FlashV150:
               var config2 = StructConversion.ArrayToStruct<Flash.v150.TConfigSection>(rawConfig);
               config2.Swap();
               configOld = config2.Map();
               break;

            case FlashVersionE.None:
               return null;
         }

         return configOld;
      }


      /// <summary>
      /// Convert <see cref="OldConfiguration"/> to raw binary data flash memory representation
      /// </summary>
      /// <param name="configuration">configuration object</param>
      /// <returns>byte array to be write to flash memory</returns>
      public static byte[] SaveConfigSection(OldConfiguration configuration)
      {
         if (configuration == null || configuration.VersionInfo == null)
         {
            return null;
         }

         switch (FlashVersion.GetVersion(configuration.VersionInfo))
         {
            case FlashVersionE.FlashV111:
               var config = configuration.MapV111();
               config.Swap();
               return StructConversion.StructToArray(config);

            case FlashVersionE.FlashV150:
               var config2 = configuration.MapV150();
               config2.Swap();
               return StructConversion.StructToArray(config2);

            case FlashVersionE.None:
               return null;
         }
         return null;
      }
   }
}