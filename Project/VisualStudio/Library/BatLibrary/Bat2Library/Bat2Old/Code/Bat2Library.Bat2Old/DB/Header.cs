﻿using System;

namespace Bat2Library.Bat2Old.DB
{
   public class Header
   {
      public string VersionSw { get; set; }
      public DateTime DateTime { get; set; }
      public short DataType { get; set; }
      public string Note { get; set; }
   }
}
