﻿using System;
using System.Linq;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Bat2Old.Constants
{
   public class FlashVersion
   {
      private static readonly byte[] FlashV111 = {0x01, 0x11};
      private static readonly byte[] FlashV150 = {0x01, 0x50};

      public static FlashVersionE GetVersion(byte[] rawData)
      {
         if (rawData == null)
         {
            return FlashVersionE.None;
         }

         if (rawData.Length < 2)
         {
            return FlashVersionE.None;
         }

         if (rawData.Take(2).SequenceEqual(FlashV111))
         {
            return FlashVersionE.FlashV111;
         }

         if (rawData.Take(2).SequenceEqual(FlashV150))
         {
            return FlashVersionE.FlashV150;
         }

         return FlashVersionE.None;
      }

      public static FlashVersionE GetVersion(BaseVersionInfo version)
      {
         if (version == null)
         {
            return FlashVersionE.None;
         }

         if (IsSameVersion(FlashV111, version))
         {
            return FlashVersionE.FlashV111;
         }


         if (IsSameVersion(FlashV150, version))
         {
            return FlashVersionE.FlashV150;
         }

         return FlashVersionE.None;
      }

      private static bool IsSameVersion(byte[] flash, BaseVersionInfo version)
      {
         var swBuild = flash[0];
         var swMajor = flash[1] >> 4;
         var swMinor = flash[1] & 0x0F;
         return (swBuild == version.SoftwareBuild && swMajor == version.SoftwareMajor &&
                 swMinor == version.SoftwareMinor);
      }
   }

   public enum FlashVersionE
   {
      FlashV111,
      FlashV150,
      None
   }
}