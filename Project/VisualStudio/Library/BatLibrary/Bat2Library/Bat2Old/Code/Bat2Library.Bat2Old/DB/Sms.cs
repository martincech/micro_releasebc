﻿using System;
using Bat2Library.Bat2Old.DB.Helpers;

namespace Bat2Library.Bat2Old.DB
{
   public class Sms
   {
      public string GsmNumber { get; set; }
      public DateTime InsertDateTime { get; set; }
      public DateTime EditDateTime { get; set; }
      public short Id { get; set; }
      public short DayNumber { get; set; }
      public DateTime DayDate { get; set; }

      public Statistic FemaleStat { get; set; }
      public Statistic MaleStat { get; set; }

      public string Note { get; set; }
   }
}
