﻿using System;
using System.Runtime.InteropServices;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   // ---------------------------------------------------------------------------------------------
   // Datum a cas
   // ---------------------------------------------------------------------------------------------

   // Uplny datum, vcetne celeho roku a na cele bajty (v pripade vicenasobneho ukladani predelat na bity)
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TLongDateTime
   {
      public byte Min;
      public byte Hour;
      public byte Day;
      public byte Month;
      public UInt16 Year;

      public void Swap()
      {
         Year = Endian.SwapUInt16(Year);
      }
   } // 6 bajtu
}