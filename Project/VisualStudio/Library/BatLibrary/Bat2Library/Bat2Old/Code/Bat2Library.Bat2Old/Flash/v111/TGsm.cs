using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;

namespace Bat2Library.Bat2Old.Flash.v111
{
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TGsm
   {
      public byte Use; // Flag, zda se GSM modul vyuziva (zda je pripojen)
      public byte SendMidnight; // Flag, zda se ma posilat o pulnoci statistika
      public byte PeriodMidnight; // Perioda ve dnech, kdy se ma posilat pulnocni statistika
      public byte SendOnRequest; // Flag, zda se ma odpovidat na zaslane SMS
      public byte ExecuteOnRequest; // Flag, zda se ma provadet cinnost podle zaslanych SMS (zatim nevyuzito)

      public byte CheckNumbers;
         // Flag, zda se ma pri prijeti prikazu kontrolovat cislo (odpovi se jen na prikazy odeslane z cisla, ktere je v seznamu)

      public byte NumberCount; // Pocet definovanych telefonnich cisel, na ktere se vysila o pulnoci

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GSM_NUMBER_MAX_COUNT*TConstants.GSM_NUMBER_MAX_LENGTH)
      ] // Delka pole = X_COUNT * Y_COUNT
      public byte[] Numbers; // Definovana telefonni cisla
   } // 82 bajtu
}