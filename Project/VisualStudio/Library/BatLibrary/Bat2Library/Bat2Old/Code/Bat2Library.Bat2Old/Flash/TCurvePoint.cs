﻿using System.Runtime.InteropServices;
using Utilities;

namespace Bat2Library.Bat2Old.Flash
{
   // Format bodu rustove krivky
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TCurvePoint
   {
      public System.UInt16 Day; // 0..CURVE_MAX_DAY (999)
      public System.UInt16 Weight; // 0..CURVE_MAX_WEIGHT (0xFFFF)

      public void Swap()
      {
         Day = Endian.SwapUInt16(Day);
         Weight = Endian.SwapUInt16(Weight);
      }
   } // 4 bajty
}