﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.Domain.Old;

namespace Bat2Library.Bat2Old.Map
{
   public static class MapOldToNew
   {

      private const string FEMALE_NAME = "_Female";
      private const string MALE_NAME = "_Male";
      private const string PLAN_NAME = "_Plan";

      public static Bat2DeviceData Map(OldBat2DeviceData data, Bat2DeviceData baseData)
      {
         if (baseData == null)
         {
            baseData = new Bat2DeviceData();
         }
         //Remove default
         baseData.PredefinedWeighings = new List<WeighingConfiguration>();
         baseData.WeighingPlans = new List<WeighingPlan>();
         baseData.GrowthCurves = new List<Curve>();
         baseData.CorrectionCurves = new List<Curve>();

         //Mapping Bat2Old settings
         //Contact list
         baseData.Contacts = Map(data.Configuration.Gsm, baseData.Contacts.ToList());
         //Growth curves
         baseData.GrowthCurves = Map(data.Configuration.Flocks, baseData.GrowthCurves.ToList());
         //Weighing plan list
         baseData.WeighingPlans = Map(data, baseData.WeighingPlans.ToList());
         //Weighing configuration list
         baseData.PredefinedWeighings = Map(data, baseData.PredefinedWeighings.ToList(), baseData.GrowthCurves.ToList(), baseData.WeighingPlans.ToList());

         //Actual configuration
         baseData.Configuration = Map(data.Configuration, baseData.Configuration, baseData.PredefinedWeighings.ToList());
       
         return baseData;
      }

      private static IEnumerable<WeighingPlan> Map(OldBat2DeviceData data, List<WeighingPlan> baseData)
      {
         if (baseData == null)
         {
            baseData = new List<WeighingPlan>();
         }

         foreach (var flock in data.Configuration.Flocks)
         {
            var wp = new WeighingPlan
            {
               Name = flock.Header.Title.Trim() + PLAN_NAME,
               SyncWithDayStart = true,
               WeighingDaysDays = (WeighingDaysMaskE)127,
               WeighingDaysMode = WeighingDaysModeE.WEIGHING_DAYS_MODE_DAY_OF_WEEK,
               WeighingDaysOnlineDays = 1,
               WeighingDaysStartDay = 1,
               WeighingDaysSuspendedDays = 1,
               WeighingTimes = new ObservableCollection<TimeRange>()
            };

            if (flock.Header.WeighFrom < 24 && flock.Header.WeighTill < 24)
            {
               wp.WeighingTimes = new ObservableCollection<TimeRange>
               {
                  new TimeRange
                  {
                     From = new DateTime(2000, 1, 1, flock.Header.WeighFrom, 0, 0),
                     To = new DateTime(2000, 1, 1, flock.Header.WeighTill, 0, 0)
                  }
               };
            }
            else
            {
               {
                  wp.WeighingTimes = new ObservableCollection<TimeRange>
               {
                  new TimeRange
                  {
                     From = new DateTime(2000, 1, 1, 0, 0, 0),
                     To = new DateTime(2000, 1, 1, 23, 59, 59)
                  }
               };
               }
            }

            baseData.Add(wp);
         }


         return baseData;
      }

      private static IEnumerable<WeighingConfiguration> Map(OldBat2DeviceData data, List<WeighingConfiguration> baseData, List<Curve> growtCurves, List<WeighingPlan> weighingPlans)
      {
         if (baseData == null)
         {
            baseData = new List<WeighingConfiguration>();
         }

         foreach (var flock in data.Configuration.Flocks)
         {

            var planIndex = 255;
            var femaleGC = 255;
            var maleGC = 255;

            var femaleGrowthCurve = growtCurves.FirstOrDefault(x => x.Name.Replace(FEMALE_NAME, "") == flock.Header.Title.Trim());
            if (femaleGrowthCurve != null && flock.Header.UseCurves)
            {
               femaleGC = growtCurves.IndexOf(femaleGrowthCurve);
            }

            var maleGrowthtCurve = growtCurves.FirstOrDefault(x => x.Name.Replace(MALE_NAME, "") == flock.Header.Title.Trim());
            if (maleGrowthtCurve != null && flock.Header.UseCurves)
            {
               maleGC = growtCurves.IndexOf(maleGrowthtCurve);
            }

            var weighingPlan =
               weighingPlans.FirstOrDefault(x => x.Name.Replace(PLAN_NAME, "") == flock.Header.Title.Trim());

            if (weighingPlan != null)
            {
               planIndex = weighingPlans.IndexOf(weighingPlan);
            }

            var wc = new WeighingConfiguration()
            {
               Flock = flock.Header.Title.Trim(),
               Name = flock.Header.Title.Trim(),
               AdjustTargetWeights = OnlineAdjustmentE.ADJUST_TARGET_WEIGHTS_NONE, //??
               CorrectionCurveIndex = 255, //??
               DayStart = new DateTime(2000, 1, 1, 0, 0, 0),
               FemaleGrowthCurveIndex = femaleGC,
               FemaleInitialWeight = 0, //??
               FemaleMarginAbove = data.Configuration.MarginAboveFemale,
               FemaleMarginBelow = data.Configuration.MarginBelowFemale,
               Filter = data.Configuration.Filter, //??
               Growth = PredictionGrowthE.PREDICTION_GROWTH_SLOW, //??
               HistogramMode = HistogramModeE.HISTOGRAM_MODE_RANGE, //??
               HistogramRange = data.Configuration.HistogramRange,
               HistogramStep = 0,//??
               InitialDay = 1, //??
               MaleGrowthCurveIndex = maleGC,
               MaleInitialWeight = 0,
               MaleMarginAbove = data.Configuration.MarginAboveMale,
               MaleMarginBelow = data.Configuration.MarginBelowMale,
               MenuMask = (WeighingConfigurationMenuMaskE)0x1FFF, //??
               Mode = flock.Header.UseCurves ? (byte)PredictionModeE.PREDICTION_MODE_GROWTH_CURVE : (byte)PredictionModeE.PREDICTION_MODE_AUTOMATIC,
               Planning = planIndex < 255,
               PredefinedIndex = 255,//??
               Sex = flock.Header.UseBothGenders ? SexE.SEX_UNDEFINED : SexE.SEX_FEMALE,//??
               SexDifferentiation = flock.Header.UseBothGenders ? SexDifferentiationE.SEX_DIFFERENTIATION_YES : SexDifferentiationE.SEX_DIFFERENTIATION_NO,//??
               ShortPeriod = 0,//??
               ShortType = StatisticShortTypeE.STATISTIC_SHORT_TYPE_INTERVAL, //??
               StabilizationRange = data.Configuration.StabilizationRange,
               StabilizationTime = data.Configuration.StabilizationTime,
               Step = data.Configuration.JumpMode,
               UniformityRange = data.Configuration.UniformityRange,
               WeighingPlanIndex = planIndex//??
            };
            baseData.Add(wc);
         }

         return baseData;
      }

      private static IEnumerable<Curve> Map(IEnumerable<OldFlock> flocks, List<Curve> baseData)
      {
         if (baseData == null)
         {
            baseData = new List<Curve>();
         }

         foreach (var flock in flocks)
         {
            Curve curveM = new Curve();
            curveM.Name = flock.Header.Title.Trim() + ((flock.Header.UseBothGenders) ? MALE_NAME : "");
            curveM.Points =
               flock.GrowthCurveMale.Where(y => y != null).Select(x => new CurvePoint() { ValueX = x.Day, ValueY = x.Weight * 10 }).ToList();

            if (curveM.Points.Count > 0)
            {
               baseData.Add(curveM);
            }

            Curve curveF = new Curve();
            curveF.Name = flock.Header.Title.Trim() + ((flock.Header.UseBothGenders) ? FEMALE_NAME : "");

            curveF.Points =
               flock.GrowthCurveFemale.Where(y => y != null).Select(x => new CurvePoint() { ValueX = x.Day, ValueY = x.Weight * 10 }).ToList();
            if (curveF.Points.Count > 0)
            {
               baseData.Add(curveF);
            }

         }
         return baseData;
      }

      public static WeighingConfiguration Map(OldBat2DeviceData data, WeighingConfiguration baseData)
      {
         if (baseData == null)
         {
            baseData = new WeighingConfiguration();
         }

         return baseData;
      }


      public static Configuration Map(OldConfiguration data, Configuration baseData, List<WeighingConfiguration> predefinedWeighings)
      {
         if (baseData == null)
         {
            baseData = new Configuration();
         }

         //DeviceInfo
         if (baseData.DeviceInfo == null)
            baseData.DeviceInfo = new DeviceInfo() {Name = "Bat2"};

         //Country use default 
         //baseData.Country.Language = (LanguageE)data.Language; //??

         //WeightUnits
         baseData.WeightUnits.Units = data.Units;

         //PlatformCalibration use default

         //DisplayConfiguration use default

         //WeighingConfiguration
         if (data.WeighingStart.UseFlock == 1 && data.WeighingStart.CurrentFlock < predefinedWeighings.Count)
         {
            //Use predefined weighing
            baseData.WeighingConfiguration = predefinedWeighings[data.WeighingStart.CurrentFlock];
         }
         else
         {
            //Default settings + global settings 
            baseData.WeighingConfiguration.FemaleMarginAbove = data.MarginAboveFemale;
            baseData.WeighingConfiguration.FemaleMarginBelow = data.MarginBelowFemale;
            baseData.WeighingConfiguration.Filter = data.Filter;
            baseData.WeighingConfiguration.HistogramRange = data.HistogramRange;
            baseData.WeighingConfiguration.MaleMarginAbove = data.MarginAboveMale;
            baseData.WeighingConfiguration.MaleMarginBelow = data.MarginBelowMale;
            baseData.WeighingConfiguration.StabilizationRange = data.StabilizationRange;
            baseData.WeighingConfiguration.StabilizationTime = data.StabilizationTime;
            baseData.WeighingConfiguration.Step = data.JumpMode;
            baseData.WeighingConfiguration.UniformityRange = data.UniformityRange;
         }

         //Rs485Options
         baseData.Rs485Options[0].Enabled = true;
         baseData.Rs485Options[0].Mode = Rs485ModeE.RS485_MODE_MODBUS;

         baseData.Rs485Options[1].Enabled = false;

         //ModbusOptions
         baseData.ModbusOptions.Address = data.Rs485.Address;
         baseData.ModbusOptions.BaudRate = (int)data.Rs485.Speed;
         if (data.Rs485.Parity == TParity.RS485_PARITY_EVEN)
             baseData.ModbusOptions.Parity = ModbusParityE.MB_PAR_EVEN;
         if (data.Rs485.Parity == TParity.RS485_PARITY_NONE)
            baseData.ModbusOptions.Parity = ModbusParityE.MB_PAR_NONE;
         if (data.Rs485.Parity == TParity.RS485_PARITY_ODD)
            baseData.ModbusOptions.Parity = ModbusParityE.MB_PAR_ODD;
         if (data.Rs485.Protocol == TProtocol.RS485_PROTOCOL_MODBUS_ASCII)
            baseData.ModbusOptions.Mode = ModbusModeE.MB_ASCII;
         if (data.Rs485.Protocol == TProtocol.RS485_PROTOCOL_MODBUS_RTU)
            baseData.ModbusOptions.Mode = ModbusModeE.MB_RTU;

         //MegaviOptions use default

         //DacsOptions use default

         //GsmMessage
         baseData.GsmMessage.CommandsEnabled = data.Gsm.SendOnRequest;
        

         //DataPublication use default
         if (data.Gsm.Use)
         {
            baseData.GsmMessage.Mode = GsmPowerModeE.GSM_POWER_MODE_ON;
         }
         else
         {
            baseData.GsmMessage.Mode = GsmPowerModeE.GSM_POWER_MODE_OFF;
         }

         //CellularData use default

         //Ethernet use default

         return baseData;
      }



      public static List<Contact> Map(OldGsm gsm, List<Contact> baseData)
      {
         if (baseData == null)
         {
            baseData = new List<Contact>();
         }
         var con = gsm.Numbers.Select(x => new Contact()
         {
            Name = x.StartsWith("+") ? x.Trim() : "+" + x.Trim(),
            PhoneNumber = x.Trim().StartsWith("+") ? x.Trim() : "+" + x.Trim(),
            Commands = gsm.ExecuteOnRequest,
            SmsFormat = GsmSmsFormatE.GSM_SMS_FORMAT_MOBILE_PHONE,
            Statistics = gsm.SendMidnight
         });
         baseData.AddRange(con);
         return baseData;
      }
   }
}
