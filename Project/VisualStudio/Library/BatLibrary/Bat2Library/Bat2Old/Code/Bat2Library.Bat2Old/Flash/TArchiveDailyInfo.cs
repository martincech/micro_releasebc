using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;

namespace Bat2Library.Bat2Old.Flash
{
   // struktura dne (jen pro pro Builder a vypocet velikosti) :
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TArchiveDailyInfo
   {
      public TArchive Archive; // 211  bytu

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.FL_LOGGER_SAMPLES)] public TLoggerSample[] Samples;
         // 5403 bytu

      public void Swap()
      {
         Archive.Swap();
         for (var i = 0; i < TConstants.FL_LOGGER_SAMPLES; i++)
         {
            Samples[i].Swap();
         }
      }
   } // 5614 bytu
}