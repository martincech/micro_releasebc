using System.Runtime.InteropServices;
using Bat2Library.Bat2Old.Constants;

namespace Bat2Library.Bat2Old.Flash.v150
{
   [StructLayout(LayoutKind.Sequential, Pack = 1)]
   public struct TGsm
   {
      public byte Use; // Flag, zda se GSM modul vyuziva (zda je pripojen)
      public byte SendMidnight; // Flag, zda se ma posilat o pulnoci statistika
      public byte PeriodMidnight1; // Perioda ve dnech, kdy se ma posilat pulnocni statistika do dne DayMidnight1
      public System.UInt16 DayMidnight1; // Do tohoto dne vcetne se posila s periodou PeriodMidnight1
      public byte PeriodMidnight2; // Perioda posilani po dni DayMidnight1
      public byte MidnightSendHour; // Hodina, pri ktere se ma poslat pulnocni SMS
      public byte MidnightSendMin; // Minuta, pri ktere se ma poslat pulnocni SMS
      public byte SendOnRequest; // Flag, zda se ma odpovidat na zaslane SMS

      public byte CheckNumbers;
         // Flag, zda se ma pri prijeti prikazu kontrolovat cislo (odpovi se jen na prikazy odeslane z cisla, ktere je v seznamu)

      public byte NumberCount; // Pocet definovanych telefonnich cisel, na ktere se vysila o pulnoci

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = TConstants.GSM_NUMBER_MAX_COUNT*TConstants.GSM_NUMBER_MAX_LENGTH)
      ] // Delka pole = X_COUNT * Y_COUNT
      public byte[] Numbers; // Definovana telefonni cisla
   } // 86 bajtu
}