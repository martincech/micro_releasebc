using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Resources;
using Bat2Library.Desktop.Properties;

namespace Bat2Library.Desktop.Localization
{
   /// <summary>
   /// Defines a type converter for enum values that converts enum values to 
   /// and from string representations using resources
   /// </summary>
   /// <remarks>
   /// This class makes localization of display values for enums in a project easy.  Simply
   /// derive a class from this class and pass the ResourceManagerin the constructor.  
   /// 
   /// <code lang="C#" escaped="true" >
   /// class LocalizedEnumConverter : ResourceEnumConverter
   /// {
   ///    public LocalizedEnumConverter(Type type)
   ///        : base(type, Properties.Resources.ResourceManager)
   ///    {
   ///    }
   /// }    
   /// </code>

   /// Then define the enum values in the resource editor.   The names of
   /// the resources are simply the enum value prefixed by the enum type name with an
   /// underscore separator eg MyEnum_MyValue.  You can then use the TypeConverter attribute
   /// to make the LocalizedEnumConverter the default TypeConverter for the enums in your
   /// project.
   /// </remarks>
   public class ResourceEnumConverter : EnumConverter
   {
      private class LookupTable : Dictionary<string, object>
      {
      }

      private readonly Dictionary<CultureInfo, LookupTable> lookupTables = new Dictionary<CultureInfo, LookupTable>();
      private readonly ResourceManager resourceManager;
      private readonly bool isFlagEnum;
      private readonly Array flagValues;

      /// <summary>
      /// Get the lookup table for the given culture (creating if necessary)
      /// </summary>
      /// <param name="culture"></param>
      /// <returns></returns>
      private LookupTable GetLookupTable(CultureInfo culture)
      {
         LookupTable result;
         if (culture == null)
            culture = CultureInfo.CurrentCulture;

         if (!lookupTables.TryGetValue(culture, out result))
         {
            result = new LookupTable();
            var standardValues = GetStandardValues();
            if (standardValues != null)
               foreach (object value in standardValues)
               {
                  string text = GetValueText(culture, value);
                  if (text != null)
                  {
                     result.Add(text, value);
                  }
               }
            lookupTables.Add(culture, result);
         }
         return result;
      }

      /// <summary>
      /// Return the name of the resource to use
      /// </summary>
      /// <param name="value">The value to get</param>
      /// <returns>The name of the resource to use</returns>
      protected virtual string GetResourceName(object value)
      {
         Type type = value.GetType();
         return string.Format("{0}_{1}", type.Name, value);
      }

      /// <summary>
      /// Return the text to display for a simple value in the given culture
      /// </summary>
      /// <param name="culture">The culture to get the text for</param>
      /// <param name="value">The enum value to get the text for</param>
      /// <returns>The localized text</returns>
      private string GetValueText(CultureInfo culture, object value)
      {
         string resourceName = GetResourceName(value);
         string result = resourceManager.GetString(resourceName, culture);
         if (result == null)
            result = resourceName;
         return result;
      }

      /// <summary>
      /// Return true if the given value is can be represented using a single bit
      /// </summary>
      /// <param name="value"></param>
      /// <returns></returns>
      private bool IsSingleBitValue(ulong value)
      {
         switch (value)
         {
            case 0:
               return false;
            case 1:
               return true;
         }
         return ((value & (value - 1)) == 0);
      }

      /// <summary>
      /// Return the text to display for a flag value in the given culture
      /// </summary>
      /// <param name="culture">The culture to get the text for</param>
      /// <param name="value">The flag enum value to get the text for</param>
      /// <returns>The localized text</returns>
      private string GetFlagValueText(CultureInfo culture, object value)
      {
         // if there is a standard value then use it
         //
         if (Enum.IsDefined(value.GetType(), value))
         {
            return GetValueText(culture, value);
         }

         // otherwise find the combination of flag bit values
         // that makes up the value
         //
         ulong lValue = Convert.ToUInt32(value);
         string result = null;
         foreach (object flagValue in flagValues)
         {
            ulong lFlagValue = Convert.ToUInt32(flagValue);
            if (IsSingleBitValue(lFlagValue))
            {
               if ((lFlagValue & lValue) == lFlagValue)
               {
                  string valueText = GetValueText(culture, flagValue);
                  if (result == null)
                  {
                     result = valueText;
                  }
                  else
                  {
                     result = string.Format("{0}, {1}", result, valueText);
                  }
               }
            }
         }
         return result;
      }

      /// <summary>
      /// Return the Enum value for a simple (non-flagged enum)
      /// </summary>
      /// <param name="culture">The culture to convert using</param>
      /// <param name="text">The text to convert</param>
      /// <returns>The enum value</returns>
      private object GetValue(CultureInfo culture, string text)
      {
         LookupTable lookupTable = GetLookupTable(culture);
         object result;
         lookupTable.TryGetValue(text, out result);
         return result;
      }

      /// <summary>
      /// Return the Enum value for a flagged enum
      /// </summary>
      /// <param name="culture">The culture to convert using</param>
      /// <param name="text">The text to convert</param>
      /// <returns>The enum value</returns>
      private object GetFlagValue(CultureInfo culture, string text)
      {
         LookupTable lookupTable = GetLookupTable(culture);
         string[] textValues = text.Split(',');
         ulong result = 0;
         foreach (string textValue in textValues)
         {
            object value;
            string trimmedTextValue = textValue.Trim();
            if (!lookupTable.TryGetValue(trimmedTextValue, out value))
            {
               return null;
            }
            result |= Convert.ToUInt32(value);
         }
         return Enum.ToObject(EnumType, result);
      }

      /// <summary>
      /// Create a new instance of the converter using translations from the given resource manager
      /// </summary>
      /// <param name="type"></param>
      /// <param name="resourceManager"></param>
      public ResourceEnumConverter(Type type, ResourceManager resourceManager)
         : base(type)      
      {
         this.resourceManager = resourceManager;
         object[] flagAttributes = type.GetCustomAttributes(typeof (FlagsAttribute), true);
         isFlagEnum = flagAttributes.Length > 0;
         if (isFlagEnum)
         {
            flagValues = Enum.GetValues(type);
         }
      }

      /// <summary>
      /// Convert string values to enum values
      /// </summary>
      /// <param name="context"></param>
      /// <param name="culture"></param>
      /// <param name="value"></param>
      /// <returns></returns>
      public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
      {
         if (value is string)
         {
            var result = (isFlagEnum)
               ? GetFlagValue(culture, (string) value)
               : GetValue(culture, (string) value);
            if (result == null)
            {
               result = base.ConvertFrom(context, culture, value);
            }
            return result;
         }
         if (
            value is IEnumerable<String> ||
            value is IEnumerable)
         {
            var list = new ObservableCollection<object>();
            var a = value as IEnumerable;
            foreach (string e in a)
            {
               list.Add(GetValue(culture, e));
            }
            return list;
         }
         return base.ConvertFrom(context, culture, value);
      }

      /// <summary>
      /// Convert the enum value to a string
      /// </summary>
      /// <param name="context"></param>
      /// <param name="culture"></param>
      /// <param name="value"></param>
      /// <param name="destinationType"></param>
      /// <returns></returns>
      public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value,
         Type destinationType)
      {
         if (culture == null)
            culture = CultureInfo.CurrentCulture;

         if (value == null) return null;
         if (destinationType == typeof (string) || destinationType == typeof (object))
         {
            object result = (isFlagEnum)
               ? GetFlagValueText(culture, value)
               : GetValueText(culture, value);
            return result;
         }
         if (
            destinationType == typeof (IEnumerable<String>) ||
            destinationType == typeof (IEnumerable) ||
            destinationType == typeof (ICollection) ||
            destinationType == typeof (IList))
         {
            var list = new ObservableCollection<string>();
            var a = (value as IEnumerable);
            if (a == null)
            {
               return null;
            }
            foreach (var e in a)
            {
               list.Add(GetValueText(culture, e));
            }
            return list;
         }
         return base.ConvertTo(context, culture, value, destinationType);
      }

      /// <summary>
      /// Convert the given enum value to string using the registered type converter
      /// </summary>
      /// <param name="value">The enum value to convert to string</param>
      /// <returns>The localized string value for the enum</returns>
      public static string ConvertToString(Enum value)
      {
         return new ResourceEnumConverter(value.GetType(), Resources.ResourceManager)
            .ConvertTo(value, typeof(string)) as string;
      }

      /// <summary>
      /// Return a list of the enum values and their associated display text for the given enum type
      /// </summary>
      /// <param name="enumType">The enum type to get the values for</param>
      /// <param name="culture">The culture to get the text for</param>
      /// <returns>
      /// A list of KeyValuePairs where the key is the enum value and the value is the text to display
      /// </returns>
      /// <remarks>
      /// This method can be used to provide localized binding to enums in ASP.NET applications.   Unlike 
      /// windows forms the standard ASP.NET controls do not use TypeConverters to convert from enum values
      /// to the displayed text.   You can bind an ASP.NET control to the list returned by this method by setting
      /// the DataValueField to "Key" and theDataTextField to "Value". 
      /// </remarks>
      public static List<KeyValuePair<Enum, string>> GetValues(Type enumType, CultureInfo culture)
      {
         var result = new List<KeyValuePair<Enum, string>>();
         var converter = TypeDescriptor.GetConverter(enumType);
         foreach (Enum value in Enum.GetValues(enumType))
         {
            var pair = new KeyValuePair<Enum, string>(value,
               converter.ConvertToString(null, culture, value));
            result.Add(pair);
         }
         return result;
      }

      /// <summary>
      /// Return a list of the enum values and their associated display text for the given enum type in the current UI Culture
      /// </summary>
      /// <param name="enumType">The enum type to get the values for</param>
      /// <returns>
      /// A list of KeyValuePairs where the key is the enum value and the value is the text to display
      /// </returns>
      /// <remarks>
      /// This method can be used to provide localized binding to enums in ASP.NET applications.   Unlike 
      /// windows forms the standard ASP.NET controls do not use TypeConverters to convert from enum values
      /// to the displayed text.   You can bind an ASP.NET control to the list returned by this method by setting
      /// the DataValueField to "Key" and theDataTextField to "Value". 
      /// </remarks>
      public static List<KeyValuePair<Enum, string>> GetValues(Type enumType)
      {
         return GetValues(enumType, CultureInfo.CurrentUICulture);
      }
   }
}