﻿using System.Collections.Generic;
using System.ComponentModel;
using Usb.MODEM;

namespace Bat2Library.Sms.Workers
{
   /// <summary>
   /// Multiple background workers that continuosly read SMS from multiple modems
   /// </summary>
   public class SmsReadWorkers : SmsPeriodicWorkersBase 
   {
      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="devices">List of GSM modems are connected</param>
      /// <param name="progressChanged"></param>
      /// <param name="runWorkerCompleted"></param>
      public SmsReadWorkers(List<ModemDevice> devices, ProgressChangedEventHandler progressChanged, RunWorkerCompletedEventHandler runWorkerCompleted)
         : base(devices, progressChanged, runWorkerCompleted) 
      {
      }

      /// <summary>
      /// Start all workers
      /// </summary>
      public void Start() 
      {
         foreach (var worker in workerList) 
         {
               worker.StartReadAll();
         }
      }
   }
}