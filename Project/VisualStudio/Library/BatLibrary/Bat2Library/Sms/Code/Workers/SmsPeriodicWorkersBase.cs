﻿using System.Collections.Generic;
using System.ComponentModel;
using Usb.MODEM;

namespace Bat2Library.Sms.Workers{
   /// <summary>
    /// Abstract class for multiple background workers with periodic (endless) operation
    /// </summary>
    public abstract class SmsPeriodicWorkersBase {
        /// <summary>
        /// List of workers
        /// </summary>
        protected List<SmsWorker> workerList;

        /// <summary>
        /// Event called once at the end, after all workers are completed
        /// </summary>
        private RunWorkerCompletedEventHandler runWorkerCompleted;

        /// <summary>
        /// Returns true if any of the workers is still running
        /// </summary>
        public bool IsBusy {
            get {
                foreach (SmsWorker worker in workerList) {
                    if (worker.IsBusy) {
                        // Nektery worker jeste bezi, cekam dal
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="devices">List of GSM modems are connected</param>
        /// <param name="progressChanged"></param>
        /// <param name="runWorkerCompleted"></param>
        public SmsPeriodicWorkersBase(List<ModemDevice> devices, ProgressChangedEventHandler progressChanged, RunWorkerCompletedEventHandler runWorkerCompleted)
        {
            // Zapamatuju si event po dokonceni vsech workeru
            this.runWorkerCompleted = runWorkerCompleted;
            
            // Vytvorim zadany pocet workeru a pridam je do seznamu
            workerList = new List<SmsWorker>();
            foreach (var device in devices) {
                workerList.Add(new SmsWorker(NewWorker(SmsWorkerRaw.DoWork, progressChanged, LocalRunWorkerCompleted), device));
            }
        }

        /// <summary>
        /// Create new background worker
        /// </summary>
        /// <param name="doWork"></param>
        /// <param name="progressChanged"></param>
        /// <param name="runWorkerCompleted"></param>
        /// <returns>New BackgroundWorker instance</returns>
        private BackgroundWorker NewWorker(DoWorkEventHandler doWork, ProgressChangedEventHandler progressChanged, RunWorkerCompletedEventHandler runWorkerCompleted) {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress      = true;
            worker.WorkerSupportsCancellation = true;
            worker.DoWork                    += new DoWorkEventHandler(doWork);
            worker.ProgressChanged           += new ProgressChangedEventHandler(progressChanged);
            worker.RunWorkerCompleted        += new RunWorkerCompletedEventHandler(runWorkerCompleted);

            return worker;
        }

        /// <summary>
        /// Stop all workers
        /// </summary>
        public void Stop() {
            foreach (SmsWorker worker in workerList) {
                worker.Stop();
            }
        }

        private void LocalRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            //e.Cancelled: use this if you need to determine how the worker thread completed. 
            //             Must set e.Cancel to True in DoWork (False by default) for this be reliable

            //e.Result   : Like this.piCalcWorker.ReportProgress() allows us to pass a user token
            //             to ProgressChanged via the ProgressChangedEventArgs, you can pass a result
            //             object back to RunWorkerCompleted vie RunWorkerCompletedEventArgs.Result,
            //             but only if not cancelled, as e.Result, when cancelled, is an exception:
            //             Result	'e.Result' threw an exception of type 'System.InvalidOperationException'	object {System.InvalidOperationException}

            // Kazdy worker jde po dokonceni do teto lokalni funkce. Teprve pote, kdy dokonci vsechny workery
            // zavolam hlavni runWorkerCompleted. Hlavni runWorkerCompleted se tak zavola jen jednou az nakonec,
            // kdy se dokonci posledni worker.
            
            // Pockam, dokud se neukonci vsechny workery
            if (IsBusy) {
                // Nektery worker jeste bezi, cekam dal
                return;
            }

            runWorkerCompleted(sender, e);
        }


    }
}
