﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using Usb.MODEM;

namespace Bat2Library.Sms.Workers
{
   /// <summary>
   /// Worker working with one modem
   /// </summary>
   public class SmsWorker 
   {
      /// <summary>
      /// Background worker used for the operation
      /// </summary>
      private BackgroundWorker backgroundWorker;
        
      /// <summary>
      /// Modem device
      /// </summary>
      private ModemDevice device;

      /// <summary>
      /// Returns true if the worker is still running
      /// </summary>
      public bool IsBusy 
      {
         get { return backgroundWorker.IsBusy; }
      }

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="backgroundWorker"></param>
      /// <param name="device"></param>
      public SmsWorker(BackgroundWorker backgroundWorker, ModemDevice device)
      {
         this.backgroundWorker = backgroundWorker;
         this.device = device;
      }

      /// <summary>
      /// Start periodic mode (standby or read all SMS)
      /// </summary>
      private void StartPeriodicMode(SmsOperationMode smsOperationMode) 
      {
         var gsmModemInitData = new SmsInitData
         {
            Device = device,
            Mode = smsOperationMode
         };
         backgroundWorker.RunWorkerAsync(gsmModemInitData);
      }

      /// <summary>
      /// Start standby mode
      /// </summary>
      public void StartStandBy() 
      {
         StartPeriodicMode(SmsOperationMode.STANDBY);
      }

      /// <summary>
      /// Start reading of all SMS
      /// </summary>
      public void StartReadAll() 
      {
         StartPeriodicMode(SmsOperationMode.READ_ALL);
      }

      /// <summary>
      /// Start sending of 1 SMS to multiple phone numbers
      /// </summary>
      /// <param name="phoneNumberCollection">List of phone numbers to send the SMS to</param>
      /// <param name="text">SMS text</param>
      public void StartSend(List<string> phoneNumberCollection, string text) 
      {
         var gsmModemInitData = new SmsInitData
         {
            Device = device,
            Mode = SmsOperationMode.SEND,
            SendPhoneNumberCollection = phoneNumberCollection,
            SendSmsText = text
         };
         backgroundWorker.RunWorkerAsync(gsmModemInitData);
      }
    
      /// <summary>
      /// Start request
      /// </summary>
      /// <param name="phoneNumber">Phone number to send the request to</param>
      /// <param name="day">Day number or -1 for today's statistics</param>
      public void StartRequest(string phoneNumber, int day) 
      {
         var gsmModemInitData = new SmsInitData
         {
            Device = device,
            Mode = SmsOperationMode.REQUEST,
            RequestPhoneNumber = phoneNumber,
            RequestDay = day
         };
         backgroundWorker.RunWorkerAsync(gsmModemInitData);
      }

      /// <summary>
      /// Stop operation
      /// </summary>
      public void Stop() 
      {
         backgroundWorker.CancelAsync();
      }
   }

   /// <summary>
   /// Raw SMS functions for BackgroundWorker
   /// </summary>
   public class SmsWorkerRaw 
   {
      /// <summary>
      /// Step interval at idle (for checking of signal strength)
      /// </summary>
      private const int STEP_INTERVAL = 500;

      /// <summary>
      /// Wait in miliseconds after an SMS has been sent
      /// </summary>
      private const int WAIT_AFTER_SEND = 100;
        
      static bool StrengthRawValue(Gsm modem, out int strength) 
      {
         for (var i = 0; i < 3; i++) 
         {
            if (modem.SignalStrength(out strength)) 
            {
               return true;        // Podarilo se
            }
         }

         // Nepodarilo se
         strength = 0;
         return false;
      }

      /// <summary>
      /// Read signal strength
      /// </summary>
      /// <param name="percents">Signal strength in percents</param>
      /// <returns>True if successful</returns>
      static bool SignalStrength(Gsm modem, ref SmsStatus status) 
      {
         status.SignalStrength = 0;
         int rawValue;
         if (!StrengthRawValue(modem, out rawValue))
         {
            return false;
         }

         if (rawValue == 99)
         {
               return false;   // Nelze zjistit
         }

         status.SignalStrength = rawValue * 100 / 31;
         return true;
      }

      /// <summary>
      /// Update modem status and report it to the UI
      /// </summary>
      /// <param name="action"></param>
      /// <param name="newStatus"></param>
      /// <param name="progress"></param>
      /// <param name="worker"></param>
      static void UpdateStatus(SmsStatus status, SmsEvent newEvent, int progress, BackgroundWorker worker) 
      {
         // Ulozim si novou udalost
         status.Event = newEvent;
            
         // Pozor, do UI musim predat kopii stavu. Pokud bych predal jen ukazatel, UI by s timto stavem pracovalo a postupne ho zobrazovalo,
         // ale thread by s tim samym stavem pracoval take a zmenil by ho na pozadi.
         worker.ReportProgress(progress, new SmsStatus(status));
      }

      /// <summary>
      /// Update modem status with progress = 0 and report it to the UI
      /// </summary>
      /// <param name="status"></param>
      /// <param name="newEvent"></param>
      /// <param name="worker"></param>
      static void UpdateStatus(SmsStatus status, SmsEvent newEvent, BackgroundWorker worker) 
      {
         UpdateStatus(status, newEvent, 0, worker);
      }


      /// <summary>
      /// Detect modem.
      /// </summary>
      /// <param name="status"></param>
      /// <param name="modem"></param>
      /// <param name="worker"></param>
      /// <returns>True if successful</returns>
      static bool DetectModem(SmsStatus status, Gsm modem, BackgroundWorker worker)
      {
         modem.SetBaudRate(38400);
         modem.CmdTimeout = 200;       // [ms]
         modem.SmsCenterTimeout = 15;  // [s]
         modem.SmsReceiveTimeout = 1;  // [s]
         modem.SmsDeleteTimeout = 3;   // [s]         
         modem.MemoryAtSim = true;     // use SMS memory at SIM card

         // Detekce modemu na zadanem seriovem portu
         UpdateStatus(status, SmsEvent.DETECT, worker);
         if (!modem.Detect(out status.ModemName))
         {
            return false;
         }

         // Zkontroluju ukonceni
         if (worker.CancellationPending)
         {
            return false;
         }
         return true;
      }


      /// <summary>
      /// Init the GSM modem
      /// </summary>
      /// <param name="status"></param>
      /// <param name="modem"></param>
      /// <param name="worker"></param>
      /// <returns>True if successful</returns>
      static bool InitModem(SmsStatus status, Gsm modem, BackgroundWorker worker) 
      {
         // Zkontroluju ukonceni
         if (worker.CancellationPending) {
               return false;
         }
                    
         // Reset modemu
         UpdateStatus(status, SmsEvent.RESET, worker);
         if (!modem.Reset(status.ModemName)) {
               return false;
         }

         // Zkontroluju ukonceni
         if (worker.CancellationPending) {
               return false;
         }

         // Kontrola modemu
         UpdateStatus(status, SmsEvent.CHECK, worker);
         if (!modem.Check()) {
               return false;
         }

         // Zkontroluju ukonceni
         if (worker.CancellationPending) {
               return false;
         }

         // Pockam na signal
         UpdateStatus(status, SmsEvent.SIGNAL, worker);
         while (!modem.IsRegistered()) {
               if (worker.CancellationPending) {
                  return false;
               }
         }

         // Nactu operatora
         UpdateStatus(status, SmsEvent.OPERATOR, worker);
         if (!modem.Operator(out status.OperatorName)) {
               return false;
         }

         // Zkontroluju ukonceni
         if (worker.CancellationPending) {
               return false;
         }

         // Nactu silu signalu a prepocitam na procenta
         UpdateStatus(status, SmsEvent.STRENGTH, worker);
         if (!SignalStrength(modem, ref status)){
               return false;
         }       

         // Modem je pripraven
         UpdateStatus(status, SmsEvent.READY, worker);
         return true;
      }

      static bool CountSms(Gsm modem, out int count) 
      {
         // Zkusim nacist pocet SMS opakovane
         for (var i = 0; i < 3; i++) 
         {
            if (modem.SmsCount(out count)) 
            {
               return true;        // Podarilo se
            }
         }

         // Nepodarilo se
         count = 0;
         return false;
      }

      /// <summary>
      /// Try to read SMS from specified memory position
      /// </summary>
      /// <param name="modem">Modem to use</param>
      /// <param name="memoryIndex">Memory index</param>
      /// <param name="phoneNumber">SMS phone number</param>
      /// <param name="smsText">SMS text</param>
      /// <returns>True if successful</returns>
      static bool ReadSms(Gsm modem, int memoryIndex, out string phoneNumber, out string smsText) 
      {
         // Kazdou pozici se pokusim nacist max. 3 krat
         for (var attempt = 0; attempt < 3; attempt++) 
         {
            if (modem.SmsReceive(memoryIndex, out phoneNumber, out smsText)) 
            {
               return true;      // Podarilo se nacist
            }
         }
            
         // Nepodarilo se
         phoneNumber = "";
         smsText     = "";
         return false;
      }

      static bool DeleteSms(Gsm modem, int memoryIndex) 
      {
         // Zkusim smazat zase opakovane, pokud se nepodari, neda se nic delat a pokracuju dal
         for (var i = 0; i < 3; i++) 
         {
            if (modem.SmsDelete(memoryIndex)) 
            {
               return true;        // Podarilo se
            }
         }

         // Nepodarilo se
         return false;
      }

      static bool SendRequestSms(Gsm modem, SmsInitData gsmModemInitData) 
      {
         // Sestavim text SMS
         string requestText;
         if (gsmModemInitData.RequestDay < 0) 
         {
            requestText = "STAT";
         } 
         else 
         {
            requestText = "STAT " + gsmModemInitData.RequestDay;
         }

         // Poslu SMS
         return modem.SmsSend(gsmModemInitData.RequestPhoneNumber, requestText);
      }

      public static void DoWork(object sender, DoWorkEventArgs e) 
      {
         var worker = (BackgroundWorker)sender;                // Preberu worker, ktery tuto fci vola
         var smsInitData = (SmsInitData)e.Argument;            // Preberu co ode me uzivatel chce
         var status = new SmsStatus();                         // Vytvorim novy stav
         var modem = new Gsm(smsInitData.Device.Open());       // Modem
         var requestSent = false;                              // Flag, ze jsem SMS s requestem uspesne odeslal

         // Ulozim si do stavu cislo portu, abych pozdeji modem mohl rozpoznat
         //status.PortNumber = smsInitData.PortNumber;
         status.PortName = smsInitData.Device.PortName;

         var isDetect = false;
         while (!isDetect)
         {
            isDetect = DetectModem(status, modem, worker);
         }

         // Smycka komunikace s modemem, preskocenim na zacatek cyklu muzu kdykoliv udelat inicializaci modemu od zacatku
         while (true) 
         {
            // Kontrola ukonceni
            if (worker.CancellationPending) break;

            // Inicializace modemu
            if (!InitModem(status, modem, worker)) 
            {
               // Kontrola ukonceni
               if (worker.CancellationPending) break;                

               // Pred opetovnym navratem do stavu inicializace pockam ze 2 duvodu:
               // 1) Bez cekani bude UI zahlceno zpravami o zmene stavu a okno zatuhne
               // 2) Pokud se modem nedari inicializovat, tak uzivatel aspon ma sanci videt, ve kterem stavu se inicializace sekne
               Thread.Sleep(STEP_INTERVAL);
               continue;           // Znovu na inicializaci
            }

            // V rezimu standby pouze nacitam silu signalu, nic vic
            if (smsInitData.Mode == SmsOperationMode.STANDBY) 
            {
               while (true) 
               {
                  // Kontrola ukonceni
                  if (worker.CancellationPending) break;                  

                  // Nactu aktualni silu signalu a prepocitam na procenta
                  if (!SignalStrength(modem, ref status)) break; // Silu signalu se nepodarilo nacist, skocim zpet na inicializaci modemu
                    
                  UpdateStatus(status, SmsEvent.READY, worker);

                  // Nemusim znovu kontrolovat hned, chvili pockam
                  Thread.Sleep(STEP_INTERVAL);
               }//while

               // Kontrola ukonceni
               if (worker.CancellationPending) break;
            }              
            // Pokud chce poslat SMS, poslu na vsechna cisla a ukoncim cinnost
            if (smsInitData.Mode == SmsOperationMode.SEND) 
            {
               for (var numberIndex = 0; numberIndex < smsInitData.SendPhoneNumberCollection.Count; numberIndex++) 
               {
                  // Kontrola ukonceni
                  if (worker.CancellationPending) break;                    

                  UpdateStatus(status, SmsEvent.SMS_SENDING, numberIndex, worker);
                  if (!modem.SmsSend(smsInitData.SendPhoneNumberCollection[numberIndex], smsInitData.SendSmsText)) 
                  {
                     UpdateStatus(status, SmsEvent.SMS_SEND_ERROR, numberIndex, worker);
                     continue;       // SMS se nepodarilo odeslat, neda se nic delat, pokracuju na dalsi cislo
                  }

                  UpdateStatus(status, SmsEvent.SMS_SEND_OK, numberIndex, worker);

                  // Kontrola ukonceni
                  if (worker.CancellationPending) break;                    

                  Thread.Sleep(WAIT_AFTER_SEND);     // Po odeslani musim chvili pockat, jinak nasledujici kontrola poctu SMS skonci chybou a jde to znovu na inicializaci
               }

               // Kontrola ukonceni
               if (worker.CancellationPending) break;                

               // Vsechny SMS jsem (uspesne nebo neuspesne) odeslal
               e.Result = new SmsStatus(status);      // Predam vysledek
               break;
            }

            // Pokud chce request, poslu SMS
            // Pokud jsem SMS jiz odeslal, nedelam nic (do inicializace modemu to muze spadnout kdykoliv pri chybe komunikace behem cteni SMS, pak odesilani preskocim)
            if (smsInitData.Mode == SmsOperationMode.REQUEST && !requestSent) 
            {
               UpdateStatus(status, SmsEvent.SMS_SENDING, worker);
               if (!SendRequestSms(modem, smsInitData)) 
               {
                  UpdateStatus(status, SmsEvent.SMS_SEND_ERROR, worker);
                  continue;       // SMS se nepodarilo odeslat, zpet na inicializaci modemu a pokusim se odeslat znovu
               }

               UpdateStatus(status, SmsEvent.SMS_SEND_OK, worker);

               // Kontrola ukonceni
               if (worker.CancellationPending) break;
                 
               // SMS jsem uspesne odeslal
               requestSent = true;                                 // Zapamatuju si
               Thread.Sleep(WAIT_AFTER_SEND);     // Po odeslani musim chvili pockat, jinak nasledujici kontrola poctu SMS skonci chybou a jde to znovu na inicializaci
            }
                
            // Nacteni poctu SMS
            int smsCount;
            while (CountSms(modem, out smsCount)) 
            {      // Pokud se nepodari pocet SMS nacist, skoci zpet na inicializaci modemu
               // Kontrola ukonceni
               if (worker.CancellationPending) break;           

               // Kontrola poctu SMS v modemu
               if (smsCount == 0) 
               {
                  // Modem neobsahuje zadnou SMS, cekam az neco prijde

                  // Nactu aktualni silu signalu a prepocitam na procenta
                  if (!SignalStrength(modem, ref status)) break; // Silu signalu se nepodarilo nacist, skocim zpet na inicializaci modemu
                      
                  UpdateStatus(status, SmsEvent.READY, worker);

                  // Nemusim znovu kontrolovat hned, chvili pockam
                  Thread.Sleep(STEP_INTERVAL);
                  continue;
               }

               // V modemu je nejaka SMS, nacitam jednotlive pozice v pameti (SMS muze byt kdekoliv)
               string phoneNumber = "", smsText = "";
               for (var memoryIndex = 0; memoryIndex < modem.MemorySize; memoryIndex++) 
               {
                  // Kontrola ukonceni
                  if (worker.CancellationPending) break;                   

                  // Kontrola poctu SMS v pameti, pokud jsem nacetl vsechny SMS, jdu hned zpet na cekani
                  if (CountSms(modem, out smsCount)) 
                  {      // Pokud se nepodari pocet SMS nacist, nic se nedeje, pokracuju ve cteni dalsi pozice a pocet zkontroluju zase v dalsim kroku
                     if (smsCount == 0) break; // Zpet na cekani, zaroven posle stav do UI                         
                  }

                  // Aktualizuju stav a poslu ho do UI
                  UpdateStatus(status, SmsEvent.SMS_READING, memoryIndex, worker);

                  if (!ReadSms(modem, memoryIndex, out phoneNumber, out smsText)) 
                  {
                     // Pozici se nepodarilo nacist, obsahuje zrejme SMS v neplatnem formatu (diakritika atd.) nebo jde o odeslanou zpravu ulozenou na SIM / v pameti.
                     // Pripadne chyba v komunikaci s modemem. SMS smazu a pokracuju dal.

                     // Oznamim UI spatnou SMS
                     UpdateStatus(status, SmsEvent.SMS_READ_ERROR, memoryIndex, worker);

                     // Smazu a oznamim UI smazani
                     UpdateStatus(status, SmsEvent.SMS_DELETING, memoryIndex, worker);
                     if (DeleteSms(modem, memoryIndex)) 
                     {
                        UpdateStatus(status, SmsEvent.SMS_DELETE_OK, memoryIndex, worker);
                     } 
                     else 
                     {
                        UpdateStatus(status, SmsEvent.SMS_DELETE_ERROR, memoryIndex, worker);
                     }
                     continue;       // Pokracuju na dalsi pozici v pameti
                  }

                  // Pozici se podarilo nacist
                  if (phoneNumber == "") continue; // Pozice neobsahuje zadnou SMS, pokracuju na dalsi pozici v pameti                     

                  // Pozice obsahuje SMS
                        
                  // Pokud chce request, chci jen SMS z daneho cisla
                  if (smsInitData.Mode == SmsOperationMode.REQUEST)
                  {
                     if (phoneNumber != smsInitData.RequestPhoneNumber) continue; // SMS z jineho cisla, pokracuju na dalsi pozici v pameti                     
                  }
                        
                  // Ulozim si cislo a text SMS
                  status.SmsNumber  = phoneNumber;
                  status.SmsText    = smsText;

                  // Predam nactenou SMS ke zpracovani
                  UpdateStatus(status, SmsEvent.SMS_READ_OK, memoryIndex, worker);

                  // Smazu SMS, pokud se nepodari, neda se nic delat a pokracuju dal
                  UpdateStatus(status, SmsEvent.SMS_DELETING, memoryIndex, worker);
                  if (DeleteSms(modem, memoryIndex)) 
                  {
                     UpdateStatus(status, SmsEvent.SMS_DELETE_OK, memoryIndex, worker);
                  } 
                  else 
                  {
                     UpdateStatus(status, SmsEvent.SMS_DELETE_ERROR, memoryIndex, worker);
                  }

                  // Pokud chce request a SMS je z daneho cisla, predam SMS a koncim (SMS uz je smazana)
                  if (smsInitData.Mode == SmsOperationMode.REQUEST) 
                  {
                     // Koncim
                     e.Result = new SmsStatus(status);      // Predam vysledek
                     modem.Disconnect();                    // Odpojeni modemu na zaver                         
                     return;
                  }
               }//for

               // Kontrola ukonceni
               if (worker.CancellationPending) break;               

               // Vratim stav na ready
               UpdateStatus(status, SmsEvent.READY, worker);

            }//while

            // Kontrola ukonceni
            if (worker.CancellationPending) break;            

            // Nepodarilo se nacist pocet SMS, pokracuji na inicializaci na zacatku cyklu

         }//while

         // Konecna kontrola ukonceni
         if (worker.CancellationPending) 
         {
               UpdateStatus(status, SmsEvent.CANCEL, worker);
               e.Cancel = true;
         }

         // Odpojeni modemu na zaver
         modem.Disconnect();
      }
   }
}
