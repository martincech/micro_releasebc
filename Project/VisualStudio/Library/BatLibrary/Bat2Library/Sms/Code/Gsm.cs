﻿using System;
using System.Text;
using System.Threading;
using Usb.MODEM;

namespace Bat2Library.Sms
{
    public class Gsm
    {
       #region Fields

       protected int MMemorySize;
       protected bool MMemoryAtSim = true;
       protected int MCmdTimeout = 500;
       protected int MSmsCenterTimeout = 15;
       protected int MSmsReceiveTimeout = 1;
       protected int MSmsDeleteTimeout = 3;
       protected DeviceModel MDeviceModel = DeviceModel.Siemens;
       protected int MBaseIndex;

       protected const string StrOk = "\r\nOK\r\n";
       protected const string StrError = "\r\n+CMS ERROR: ";

       private readonly ModemStream _stream;

       #endregion

       /// <summary>
       /// modem device model
       /// </summary>
       public enum DeviceModel
       {
          Siemens,
          Teltonika,
          Last
       }

       public Gsm(ModemStream stream)
       {
          _stream = stream;
       }

       public void SetBaudRate(int rate)
       {
          _stream.Device.BaudRate = rate;
       }

       /// <summary>
       /// SMS Memory location
       /// </summary>
        public bool MemoryAtSim
        {
            get { return (MMemoryAtSim); }
            set { MMemoryAtSim = value; }
        }
        
        /// <summary>
        /// SMS Memory size
        /// </summary>
        public int MemorySize
        {
            get { return (MMemorySize); }
        }

        /// <summary>
        /// Command timeout
        /// </summary>
        public int CmdTimeout
        {
            get { return (MCmdTimeout); }
            set { MCmdTimeout = value; }
        }

        /// <summary>
        /// SMS Center timeout
        /// </summary>
        public int SmsCenterTimeout
        {
            get { return (MSmsCenterTimeout); }
            set { MSmsCenterTimeout = value; }
        }

        /// <summary>
        /// SMS receive timeout
        /// </summary>
        public int SmsReceiveTimeout
        {
            get { return (MSmsReceiveTimeout); }
            set { MSmsReceiveTimeout = value; }
        }

        /// <summary>
        /// SMS delete timeout
        /// </summary>
        public int SmsDeleteTimeout
        {
            get { return (MSmsDeleteTimeout); }
            set { MSmsDeleteTimeout = value; }
        }

        /// <summary>
        /// device model
        /// </summary>
        public DeviceModel Model { get { return (MDeviceModel); } }

        #region Gsm modem methods

        /// <summary>
        /// Detect method
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Detect(out string model)
        {  
           model = "";
           string reply;
           // echo off :
           if (!_stream.Send("ATE0"))
           {
              return false;
           }
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           // get model :
           if (!_stream.Send("AT+GMM"))
           {
              return false;
           }
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           if (!ParseDeviceModel(reply, out model))
           {
              return (false);
           }
           return (true);
        }

        /// <summary>
        /// Reset method
        /// </summary>
        /// <returns></returns>
        public bool Reset(string model)
        {
           string reply;
           // echo off :
           if (!_stream.Send("ATE0"))
           {
              return (false);
           }
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           
           if (!reply.Contains(StrOk))        
           {             
               return (false); 
           }
           // SMS text mode :
           if (!_stream.Send("AT+CMGF=1"))
           {
              return (false);
           }
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           if (!reply.Contains(StrOk))
           {
              return (false);
           }
           // SMS receive - short data :
           if (!_stream.Send("AT+CSDH=0"))
           {
              return (false);
           }
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           if (!reply.Contains(StrOk))
           {
              return (false);
           }
           // Long error code :
           if (!_stream.Send("ATV1+CMEE=1"))
           {
              return (false);
           }
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           if (!reply.Contains(StrOk))         
           {
              return (false);
           }        
           MDeviceModel = DeviceModel.Siemens;
           if (model.Equals("TM3 EDGE"))
           {
              MDeviceModel = DeviceModel.Teltonika;
           }
           // Memory to SIM/device :
           MBaseIndex = 0;                 // set standard memory base index
           if (MMemoryAtSim)
           {
              if (!_stream.Send("AT+CPMS=\"SM\",\"SM\",\"SM\""))
              {
                 return (false);
              }
              if (MDeviceModel == DeviceModel.Teltonika)
              {
                 MBaseIndex = 300;        // change memory base index by Teltonika at SIM
              }
           }
           else
           {
              if (!_stream.Send("AT+CPMS=\"MT\",\"MT\",\"MT\""))
              {
                 return (false);
              }
           }
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           // check for memory reply header :
           var memoryHeader = "\r\n+CPMS: ";
           if (!reply.Contains(memoryHeader))
           {
              return (false);
           }
           reply = reply.Substring(memoryHeader.Length);
           // skip number of messages :
           var index = reply.IndexOf(',');
           if (index < 0)
           {
              return (false);                      // comma not found
           }
           reply = reply.Substring(index + 1);     // cut after comma
           // truncate tail of number :
           index = reply.IndexOf(',');
           if (index < 0)
           {
              return (false);                      // trailing comma not found
           }
           reply = reply.Substring(0, index);      // cut trailer after comma
           // read number :
           try
           {
              MMemorySize = Convert.ToInt32(reply);
           }
           catch
           {
              return (false);
           }
           return (true);
        } // Reset

        /// <summary>
        /// Check method
        /// </summary>
        /// <returns></returns>
        public bool Check()
        {
           string reply;

           if (!_stream.Send("AT"))
           {
              return (false);
           }
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           if (!reply.Contains(StrOk))
           {
              return (false);
           }
           return (true);
        } // Check

        /// <summary>
        /// IsRegistered method
        /// </summary>
        /// <returns></returns>
        public bool IsRegistered()
        {
           string reply;

           if (!_stream.Send("AT+CREG?"))
           {
              return (false);
           }
           var regHeader = "\r\n+CREG: ";
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           if (!reply.Contains(regHeader))
           {
              return (false);
           }
           var index = reply.IndexOf(',');
           if (index < 0)
           {
              return (false);
           }
           reply = reply.Substring(index + 1);     // cut after comma
           // truncate tail of number :
           index = reply.IndexOf('\r');
           if (index < 0)
           {
              return (false);
           }
           reply = reply.Substring(0, index);      // cut trailer after CR
           // read number :
           int registered;
           try
           {
              registered = Convert.ToInt32(reply);
           }
           catch
           {
              return (false);
           }
           if (registered == 1 || registered == 5)
           {
              return (true);                       // registered or roaming
           }
           return (false);
        } // IsRegistered

        /// <summary>
        /// Operator method
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Operator(out string name)
        {
           string reply;

           name = "";
           if (!_stream.Send("AT+COPS?"))
           {
              return (false);
           }
           var opsHeader = "\r\n+COPS: ";
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           if (!reply.Contains(opsHeader))         
           {
              return (false);
           }
           // skip <mode> field
           var index = reply.IndexOf(',');
           if (index < 0)
           {
              return (false);
           }
           reply = reply.Substring(index + 1);     // cut after comma
           // skip <format> field
           index = reply.IndexOf(',');
           if (index < 0)
           {
              return (false);
           }
           reply = reply.Substring(index + 1);     // cut after comma
           // leading delimiter
           index = reply.IndexOf('"');
           if (index < 0)
           {
              return (false);
           }
           reply = reply.Substring(index + 1);     // cut after quotatiton
           // trailing delimiter
           index = reply.IndexOf('"');
           if (index < 0)
           {
              return (false);
           }
           reply = reply.Substring(0, index);      // cut trailer after quotation
           name = reply;
           return (true);
        } // Operator

        /// <summary>
        /// SignalStrength method
        /// </summary>
        /// <param name="strength"></param>
        /// <returns></returns>
        public bool SignalStrength(out int strength)
        {
           string reply;

           strength = 99;                           // no signal
           if (!_stream.Send("AT+CSQ"))
           {
              return (false);
           }
           var rssiHeader = "\r\n+CSQ: ";
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           if (!reply.Contains(rssiHeader))
           {
              return (false);
           }
           var index = reply.IndexOf(',');
           if (index < 0)
           {
              return (false);
           }
           reply = reply.Substring(0, index);     // cut after comma
           // read number :
           int rssi;
           try
           {
              var charIndex = reply.LastIndexOf(' ');
              reply = reply.Replace(reply.Substring(0, charIndex), string.Empty);
              rssi = Convert.ToInt32(reply);
           }
           catch
           {
              return (false);
           }
           strength = rssi;
           return (true);
        } // SignalStrength

       public void Disconnect()
       {
          _stream.Disconnect();
       }

        #endregion

        #region Sms methods

        /// <summary>
        /// SmsSend method
        /// </summary>
        /// <param name="to"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool SmsSend(string to, string message)
        {
           string reply;

           message = AsciiToGsm(message);
           message = message + "\x1A";              // append CTRL-Z
           var cmdSend = "AT+CMGS=\"" + to + "\"";
           if (!_stream.Send(cmdSend))
           {
              return (false);
           }
           // wait for prompt :
           Thread.Sleep(500);
           var msgPrompt = "\r\n> ";
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           if (reply != msgPrompt)
           {
              return (false);
           }
           Thread.Sleep(100);                     // wait after prompt
           if (!_stream.Send(message))
           {
              return (false);
           }
           const string sendOk = "\r\n+CMGS: ";
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           if (reply != sendOk)
           {
              return (false);
           }
           return (true);
        } // SmsSend

        /// <summary>
        /// SmsReceive method
        /// </summary>
        /// <param name="address"></param>
        /// <param name="from"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool SmsReceive(int address, out string @from, out string message)
        {
           string reply;

           @from = "";
           message = "";
           address++;                               // starts at 1
           address += MBaseIndex;
           var cmdRead = "AT+CMGR=" + address;
           if (!_stream.Send(cmdRead))
           {
              return (false);
           }
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           // check for empty position :
           if (reply.Contains("\r\n+CMGR: 0,,0\r\n") ||
               reply.Contains("\r\n+CMS ERROR: 321"))
           {
              message = "<*EMPTY*>";
              return (true);
           }
           // check for error :
           if (reply.Contains(StrError))
           {
              return (false);
           }
           // reply header :
           const string rxHeader = "\r\n+CMGR: ";
           var index = reply.IndexOf(rxHeader, StringComparison.Ordinal);
           if (index < 0)
           {
              return (false);
           }
           reply = reply.Substring(index + rxHeader.Length);   // cut header
           // check for read status :
           var recRead = "\"REC READ\"";
           var recUnread = "\"REC UNREAD\"";
           var recSize = recRead.Length;
           index = reply.IndexOf(recRead, StringComparison.Ordinal);
           if (index < 0)
           {
              index = reply.IndexOf(recUnread, StringComparison.Ordinal);
              recSize = recUnread.Length;
              if (index < 0)
              {
                 return (false);
              }
           }
           reply = reply.Substring(index + recSize); // cut read status
           // get phone number
           index = reply.IndexOf(",\"", StringComparison.Ordinal);           // comma and quotation after read status
           if (index < 0)
           {
              return (false);
           }
           reply = reply.Substring(index + 2);     // cut comma and quotation
           index = reply.IndexOf('"');             // terminating quotation
           if (index < 0)
           {
              return (false);
           }
           @from = reply.Substring(0, index);      // phone number
           reply = reply.Substring(index);         // cut phone number
           // get message
           index = reply.IndexOf("\r\n", StringComparison.Ordinal);          // CR LF at start of message
           if (index < 0)
           {
              return (false);
           }
           reply = reply.Substring(index + 2);     // start of message
           index = reply.IndexOf('\r');            // CR at end of message
           if (index < 0)
           {
              return (false);
           }
           reply = reply.Substring(0, index);      // cut trailing characters
           message = GsmToAscii(reply);
           return (true);
        } // SmsReceive

        /// <summary>
        /// SmsDelete method
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public bool SmsDelete(int address)
        {
           string reply;
           address++;                               // starts at 1
           address += MBaseIndex;
           var cmdDelete = "AT+CMGD=" + address;
           if (!_stream.Send(cmdDelete))
           {
              return (false);
           }
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           if (reply != StrOk)
           {
              return (false);
           }
           return (true);
        } // SmsDelete

        /// <summary>
        /// SmsCount method
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public bool SmsCount(out int count)
        {
           string reply;
           count = 0;

           if (!_stream.Send("AT+CPMS?"))
           {
              return (false);
           }
           if (!_stream.Receive(out reply))
           {
              return (false);
           }
           // check for memory reply header :
           var memoryHeader = "\r\n+CPMS: ";
           if (!reply.Contains(memoryHeader))
           {
              return (false);
           }
           reply = reply.Substring(memoryHeader.Length);
           // skip number of messages :
           var index = reply.IndexOf(',');
           if (index < 0)
           {
              return (false);                      // comma not found
           }
           reply = reply.Substring(index + 1);     // cut after comma
           // truncate tail of number :
           index = reply.IndexOf(',');
           if (index < 0)
           {
              return (false);                      // trailing comma not found
           }
           reply = reply.Substring(0, index);      // cut trailer after comma
           // read number :
           try
           {
              count = Convert.ToInt32(reply);
           }
           catch
           {
              return (false);
           }
           return (true);
        } // SmsCount

        #endregion

        #region Private helpers

        protected string AsciiToGsm(string text)
        {
           var str = new StringBuilder();
           foreach (var t in text)
           {
              str.Append(t == '@' ? '\0' : t);
           }
           return (str.ToString());
        } // AsciiToGsm

        protected string GsmToAscii(string text)
        {
            var str = new StringBuilder();
            foreach (var t in text)
            {
               str.Append(t == '\0' ? '@' : t);
            }
           return (str.ToString());
        } // GsmToAscii

        protected static bool ParseDeviceModel(string reply, out string model)
        {
            // separate model name :
            model = "";
            var index = reply.IndexOf("\r\n", StringComparison.Ordinal);
            if (index != 0)
            {
                return (false);
            }
            reply = reply.Substring(2);
            if (!reply.Contains("\r"))
            {
                return (false);
            }
            model = reply.Substring(0, reply.IndexOf("\r", StringComparison.Ordinal));
            return (true);
        } // ParseDeviceModel

        #endregion
    }
}