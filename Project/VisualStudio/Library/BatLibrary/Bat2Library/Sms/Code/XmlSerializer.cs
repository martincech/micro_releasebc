﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Bat2Library.Sms
{
   public class XmlSerializer
   {
      /// <summary>
      /// Load xml into object
      /// </summary>
      /// <param name="file"></param>
      /// <returns></returns>
      public static List<T> Load<T>(string file)
      {
         System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));
         List<T> smsList = new List<T>();
         try
         {
            if (!File.Exists(file))
            {
               return new List<T>();
            }
            using (TextReader writer = new StreamReader(file))
            {
               smsList = (List<T>)serializer.Deserialize(writer);
            } 
         }
         catch (Exception)
         {
           return new List<T>();
         }
         return smsList;
      }


      /// <summary>
      /// Replace xml data in file
      /// </summary>
      /// <param name="smsList"></param>
      /// <param name="file"></param>
      /// <returns></returns>
      public static bool Update<T>(List<T> smsList, string file)
      {
         System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));
         var tempFile = file + ".tmp";
         try
         {
            if (File.Exists(file))
            {
               File.Move(file, tempFile);
            }
            FileStream fs = new FileStream(file, FileMode.Create);
            using (TextWriter writer = new StreamWriter(fs))
            {
               serializer.Serialize(writer,smsList);
            }
            if (File.Exists(tempFile))
            {
               File.Delete(tempFile);
            }
         }
         catch (Exception)
         {
            return false;
         }

         return true;
      }

      /// <summary>
      /// Append data to existing xml file
      /// </summary>
      /// <param name="obj"></param>
      /// <param name="file"></param>
      /// <returns></returns>
      public static bool Append<T>(T obj, string file)
      {
         XmlDocument doc = new XmlDocument();
         try
         {
            if (!File.Exists(file))
            {
               return Update(new List<T>() {obj}, file);
            }

            doc.Load(file);
         
            if (doc.FirstChild != null && doc.FirstChild.NextSibling != null)
            {
               XmlDocumentFragment xfrag = doc.CreateDocumentFragment();
               xfrag.InnerXml = SerializeToString(obj);
               doc.FirstChild.NextSibling.AppendChild(xfrag);
            }
            doc.Save(file);
         }
         catch (Exception e)
         {
            return false;
         }


         return true;
      }

      private static string SerializeToString<T>(T toSerialize)
      {
         var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
         var serializer = new System.Xml.Serialization.XmlSerializer(toSerialize.GetType());
         var settings = new XmlWriterSettings();
         settings.Indent = true;
         settings.OmitXmlDeclaration = true;

         using (var stream = new StringWriter())
         using (var writer = XmlWriter.Create(stream, settings))
         {
            serializer.Serialize(writer, toSerialize, emptyNamepsaces);
            return stream.ToString();
         }
      }

   }
}
