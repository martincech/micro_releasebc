﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using Bat2Library.Utilities.Interfaces;

namespace Bat2Library.Utilities
{
   public class XmlFileParser : IParser
   {
      public List<Report> Reports { get; private set; }
      private XmlNode file;
         
      public XmlFileParser(XmlNode document)
      {
         file = document;
      }

      public IEnumerable<Report> Parse()
      {
         try
         {
            Reports = new List<Report>();
            ParseXmlDoc(file);
            return Reports;
         }
         catch (Exception e)
         {
            throw new Exception(e.Message);
         }
      }

      /// <summary>
      /// Parse statistics from xml document
      /// </summary>    
      /// <param name="document">document to parse</param>     
      private void ParseXmlDoc(XmlNode document)
      {        
         CultureInfo cultureInfo;     
         // ReSharper disable PossibleNullReferenceException
         var lang = document.SelectSingleNode(StatFormat.EXPORT_FILE).Attributes[StatFormat.LANGUAGE];
         if (lang == null)
         {
            cultureInfo = CultureInfo.CreateSpecificCulture("en-US");
         }
         else
         {
            if (CultureInfo.GetCultures(CultureTypes.AllCultures).Any(culture => culture.Name == lang.Value))
            {
               cultureInfo = CultureInfo.CreateSpecificCulture(lang.Value);
            }
            else
            {
               cultureInfo = CultureInfo.CreateSpecificCulture("en-US");
            }
         }
         var xmlNodeList = document.SelectNodes(StatFormat.EXPORT_STATISTICS);
         if (xmlNodeList == null)
         {
            throw new InvalidDataException();
         }

         Reports = new List<Report>();
         foreach (XmlNode xmlStat in xmlNodeList)
         {
            try
            {
               //date
                var day = int.Parse(xmlStat.SelectSingleNode(StatFormat.DAY).InnerText);
                var month = int.Parse(xmlStat.SelectSingleNode(StatFormat.MONTH).InnerText);
                var year = int.Parse(xmlStat.SelectSingleNode(StatFormat.YEAR).InnerText);
                var sexStrig = xmlStat.SelectSingleNode(StatFormat.SEX).InnerText;

               var report = new Report
               {
                   ScaleName = xmlStat.SelectSingleNode(StatFormat.SCALE).InnerText,
                  Date = new DateTime(year, month, day),
                  Sex =
                     sexStrig.Equals(StatFormat.FEMALE) ? SexE.SEX_FEMALE : sexStrig.Equals(StatFormat.MALE) ? SexE.SEX_MALE : SexE.SEX_UNDEFINED,
                   Day = int.Parse(xmlStat.SelectSingleNode(StatFormat.DAY_NUMBER).InnerText),
                   Count = int.Parse(xmlStat.SelectSingleNode(StatFormat.COUNT).InnerText),
                   Average = double.Parse(xmlStat.SelectSingleNode(StatFormat.AVERAGE).InnerText,
                     cultureInfo),
                   Gain = double.Parse(xmlStat.SelectSingleNode(StatFormat.AVERAGE).InnerText,
                     cultureInfo),
                   Sigma = double.Parse(xmlStat.SelectSingleNode(StatFormat.SIGMA).InnerText,
                     cultureInfo),
                   Uniformity = double.Parse(xmlStat.SelectSingleNode(StatFormat.UNI).InnerText,
                     cultureInfo),
                   Cv = double.Parse(xmlStat.SelectSingleNode(StatFormat.CV).InnerText,
                     cultureInfo)
               };
               Reports.Add(report);
            }
            catch (Exception)
            {
               throw new InvalidDataException();
            }
         }
      }
   }
}
