﻿using System;

namespace Bat2Library.Utilities.Sms.Models
{
    public class SmsData
    {
        /// <summary>
        /// Phone number of the scale
        /// </summary>
        public string PhoneNumber;

        /// <summary>
        /// Id. number of the scale
        /// </summary>
        public long ScaleNumber;

        /// <summary>
        /// Name of the scale
        /// </summary>
        public string ScaleName;

        /// <summary>
        /// Day number
        /// </summary>
        public int DayNumber;

        /// <summary>
        /// Date of the day
        /// </summary>
        public DateTime Date;

        /// <summary>
        /// Male data
        /// </summary>
        public StatData MaleData;

        /// <summary>
        /// Female Data
        /// </summary>
        public StatData FemaleData;
    }
}
