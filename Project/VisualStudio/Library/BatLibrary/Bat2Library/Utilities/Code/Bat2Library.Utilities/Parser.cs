﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Bat2Library.Utilities
{
   public abstract class Parser
   {
      public List<Report> Reports { get; protected set; }
      public int FoundRecords { get; protected set; }

      protected const int MINIMUM_PARSE_DATA = 2;
      protected byte[] byteArray;

      protected Parser(byte[] file)
      {
         byteArray = file;
      }

      protected Parser(string filename)
      {
         try
         {
            byteArray = File.ReadAllBytes(filename);
         }
         catch (Exception ex)
         {
            throw new Exception(ex.Message);
         }
      }

      /// <summary>
      /// Check if this row represent data.
      /// </summary>
      /// <param name="list">results of parsing data</param>
      /// <returns>true - row contains any data</returns>
      protected bool CheckIsData(IEnumerable<bool> list)
      {
         if (Reports.Count > 0)
         {  // If any data already correctly saved, 
            // other rows are expected as data automatically.
            return true;
         }

         var count = list.Count(item => item);
         return count >= MINIMUM_PARSE_DATA;
      }

      protected void AddReport(DateTime date, SexE sex, string scalename, int day,
               int count, double average, double gain, double sigma, double cv,
               double uniformity, string note)
      {
         var report = new Report
         {
            Date = date,
            ScaleName = scalename,
            Day = day,
            Count = count,
            Average = average,
            Gain = gain,
            Sigma = sigma,
            Cv = cv,
            Uniformity = uniformity,
            Note = note,
            Sex = sex
         };
         Reports.Add(report);
      }
   }
}
