﻿namespace Bat2Library.Utilities
{
   internal class StatFormat
   {
      public const string EXPORT_FILE = "ExportFile";
      public const string LANGUAGE = "lang";
      public const string STATISTICS = "Statistics";
      public const string EXPORT_STATISTICS = EXPORT_FILE + @"/" + STATISTICS;
      public const string DAY = "Day";
      public const string MONTH = "Month";
      public const string YEAR = "Year";
      public const string SEX = "Sex";
      public const string SCALE = "Scale";
      public const string MALE = "M";
      public const string FEMALE = "F";
      public const string DAY_NUMBER = "DayNumber";
      public const string COUNT = "Count";
      public const string AVERAGE = "Average";
      public const string GAIN = "Gain";
      public const string SIGMA = "Sigma";
      public const string UNI = "Uni";
      public const string CV = "Cv";
      public const string DATE = "Date";    
      public const string NOTE = "Note";
   }
}
