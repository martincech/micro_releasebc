﻿namespace Bat2Library.Utilities.Sms.Models
{
    public class StatData
    {
        /// <summary>
        /// Number of birds
        /// </summary>
        public int Count;

        /// <summary>
        /// Average weight
        /// </summary>
        public double Average;

        /// <summary>
        /// Daily gain
        /// </summary>
        public double Gain;

        /// <summary>
        /// Standard deviation
        /// </summary>
        public double Sigma;

        /// <summary>
        /// Coeficient of variation
        /// </summary>
        public double Cv;

        /// <summary>
        /// Uniformity
        /// </summary>
        public double Uniformity;

       /// <summary>
       /// Temperature
       /// </summary>
       public double? Temperature;

       /// <summary>
       /// Carbon dioxide
       /// </summary>
       public int? CarbonDioxide;

       /// <summary>
       /// Ammonia
       /// </summary>
       public int? Ammonia;

       /// <summary>
       /// Humidity
       /// </summary>
       public double? Humidity;
    }
}
