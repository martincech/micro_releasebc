﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bat2Library.Utilities.Tests.UnitTests
{
   [TestClass]
   public class ExcelParserTests
   {
      [TestMethod]
      public void Females_WithHeader_OpenByByteArray()
      {       
         var filename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Resources/excelFemale2.xlsx";
         var parser = new ExcelParser(File.ReadAllBytes(filename));       
         parser.Parse();

         CheckReports(parser);
      }

      [TestMethod]
      public void Females_WithHeader_OpenByName()
      {
         var filename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Resources/excelFemale2.xlsx";
         var parser = new ExcelParser(filename);
         parser.Parse();

         CheckReports(parser);
      }

      private void CheckReports(ExcelParser parser)
      {
         // total count of records
         Assert.AreEqual(5, parser.Reports.Count);

         //exact data for second item
         var index = 1;
         Assert.AreEqual(new DateTime(2015, 3, 10), parser.Reports[index].Date);
         Assert.AreEqual("456", parser.Reports[index].ScaleName);
         Assert.AreEqual(10, parser.Reports[index].Day);
         Assert.AreEqual(25, parser.Reports[index].Count);
         Assert.AreEqual(1, parser.Reports[index].Average);
         Assert.AreEqual(1, parser.Reports[index].Gain);
         Assert.AreEqual(1, parser.Reports[index].Sigma);
         Assert.AreEqual(1, parser.Reports[index].Cv);
         Assert.AreEqual(1, parser.Reports[index].Uniformity);
         Assert.AreEqual("", parser.Reports[index].Note);

         Assert.AreEqual(SexE.SEX_UNDEFINED, parser.Reports[index].Sex);
         Assert.AreEqual(5, parser.FoundRecords);
      }

      [TestMethod]
      public void Males_WithHeader()
      {
         const string filename = "Resources/excelMale.xlsx";
         var parser = new ExcelParser(File.ReadAllBytes(filename));
         
         parser.Parse();

         // total count of records
         Assert.AreEqual(3, parser.Reports.Count);

         //exact data for third item
         var index = 2;
         Assert.AreEqual(new DateTime(2015, 3, 31), parser.Reports[index].Date);
         Assert.AreEqual("999", parser.Reports[index].ScaleName);
         Assert.AreEqual(1, parser.Reports[index].Day);
         Assert.AreEqual(10, parser.Reports[index].Count);
         Assert.AreEqual(1, parser.Reports[index].Average);
         Assert.AreEqual(11, parser.Reports[index].Gain);
         Assert.AreEqual(1, parser.Reports[index].Sigma);
         Assert.AreEqual(1, parser.Reports[index].Cv);
         Assert.AreEqual(25, parser.Reports[index].Uniformity);
         Assert.AreEqual("", parser.Reports[index].Note);

         Assert.AreEqual(SexE.SEX_MALE, parser.Reports[index].Sex);
         Assert.AreEqual(3, parser.FoundRecords);
      }

      [TestMethod]
      public void File_WithoutHeader()
      {
         const string filename = "Resources/excelWithoutHeader.xlsx";
         var parser = new ExcelParser(filename);
         parser.Parse();

         // total count of records
         Assert.AreEqual(2, parser.Reports.Count);

         //exact data for third item
         var index = 0;
         Assert.AreEqual(new DateTime(2015, 3, 10), parser.Reports[index].Date);
         Assert.AreEqual("1111", parser.Reports[index].ScaleName);
         Assert.AreEqual(1, parser.Reports[index].Day);
         Assert.AreEqual(10, parser.Reports[index].Count);
         Assert.AreEqual(1, parser.Reports[index].Average);
         Assert.AreEqual(1, parser.Reports[index].Gain);
         Assert.AreEqual(1, parser.Reports[index].Sigma);
         Assert.AreEqual(1, parser.Reports[index].Cv);
         Assert.AreEqual(1, parser.Reports[index].Uniformity);
         Assert.AreEqual("", parser.Reports[index].Note);

         Assert.AreEqual(SexE.SEX_UNDEFINED, parser.Reports[index].Sex);
         Assert.AreEqual(2, parser.FoundRecords);
      }

      [TestMethod]
      public void File_AdditionalParameters()
      {
         const string filename = "Resources/excelFemalesAdditionalParamOtherLang.xlsx";
         var parser = new ExcelParser(filename);
         var reports = parser.Parse().ToList();

         // total count of records
         Assert.AreEqual(13, parser.Reports.Count);

         //exact data for third item
         var index = 10;
         Assert.AreEqual(new DateTime(2015, 4, 29), reports[index].Date);
         Assert.AreEqual("101", reports[index].ScaleName);
         Assert.AreEqual(30, reports[index].Day);
         Assert.AreEqual(7, reports[index].Count);
         Assert.AreEqual(0.099, reports[index].Average);
         Assert.AreEqual(0.099, reports[index].Gain);
         Assert.AreEqual(0.001, reports[index].Sigma);
         Assert.AreEqual(1, reports[index].Cv);
         Assert.AreEqual(100, reports[index].Uniformity);
         Assert.AreEqual("", reports[index].Note);

         Assert.AreEqual(SexE.SEX_UNDEFINED, reports[index].Sex);
         Assert.AreEqual(13, parser.FoundRecords);
      }

      [TestMethod]
      public void File_Note()
      {
         const string filename = "Resources/excelFemalesAdditionalParamOtherLang.xlsx";
         var parser = new ExcelParser(filename);
         var reports = parser.Parse().ToList();

         // total count of records
         Assert.AreEqual(13, parser.Reports.Count);
         var index = 11;
         Assert.AreEqual("Test poznamky", reports[index].Note);
         Assert.AreEqual(13, parser.FoundRecords);
      }

      [TestMethod]
      public void File_Header_Error()
      {
         const string filename = "Resources/excelHeaderError.xlsx";
         var parser = new ExcelParser(filename);
         var reports = parser.Parse().ToList();

         // total count of records
         Assert.AreEqual(3, parser.Reports.Count);

         //exact data for third item
         var index = 0;
         Assert.AreEqual(new DateTime(2015, 3, 10), reports[index].Date);
         Assert.AreEqual("789", reports[index].ScaleName);
         Assert.AreEqual(2, reports[index].Day);
         Assert.AreEqual(20, reports[index].Count);
         Assert.AreEqual(1, reports[index].Average);
         Assert.AreEqual(1, reports[index].Gain);
         Assert.AreEqual(1, reports[index].Sigma);
         Assert.AreEqual(1, reports[index].Cv);
         Assert.AreEqual(1, reports[index].Uniformity);
         Assert.AreEqual("", reports[index].Note);

         Assert.AreEqual(SexE.SEX_UNDEFINED, reports[index].Sex);
         Assert.AreEqual(3, parser.FoundRecords);
      }

      [TestMethod]
      public void File_DataError_MissingColumn()
      {
         const string filename = "Resources/excelMissingColumn.xlsx";
         var parser = new ExcelParser(filename);
         var reports = parser.Parse();
         Assert.IsNull(reports);
         Assert.AreEqual(3, parser.FoundRecords);
      }

      [TestMethod]    
      public void File_DataError_MissingValue()
      {
         const string filename = "Resources/excelMissingData.xlsx";
         var parser = new ExcelParser(filename);
         var reports = parser.Parse().ToList();

         // total count of records
         Assert.AreEqual(2, reports.Count);

         //exact data for third item
         var index = 1;
         Assert.AreEqual(new DateTime(2015, 3, 10), reports[index].Date);
         Assert.AreEqual("101", reports[index].ScaleName);
         Assert.AreEqual(1, reports[index].Day);
         Assert.AreEqual(10, reports[index].Count);
         Assert.AreEqual(1, reports[index].Average);
         Assert.AreEqual(3, reports[index].Gain);
         Assert.AreEqual(1, reports[index].Sigma);
         Assert.AreEqual(1, reports[index].Cv);
         Assert.AreEqual(100, reports[index].Uniformity);
         Assert.AreEqual("Test", reports[index].Note);

         Assert.AreEqual(SexE.SEX_UNDEFINED, reports[index].Sex);
         Assert.AreEqual("", reports[--index].Note);

         Assert.AreEqual(5, parser.FoundRecords);
      }

      [TestMethod]
      public void File_DataError_MissingDataHeader()
      {
         const string filename = "Resources/excelMissingHeaderData.xlsx";
         var parser = new ExcelParser(filename);
         var reports = parser.Parse().ToList();
         Assert.AreEqual(2, reports.Count);
         Assert.AreEqual(2, parser.FoundRecords);
      }

      [TestMethod]
      public void File_OldExcelFormat()
      {
         const string filename = "Resources/excelMaleOldExcel.xls";
         var parser = new ExcelParser(File.ReadAllBytes(filename));
         parser.Parse();

         // total count of records
         Assert.AreEqual(3, parser.Reports.Count);

         //exact data for third item
         var index = 2;
         Assert.AreEqual(new DateTime(2015, 3, 31), parser.Reports[index].Date);
         Assert.AreEqual("999", parser.Reports[index].ScaleName);
         Assert.AreEqual(1, parser.Reports[index].Day);
         Assert.AreEqual(10, parser.Reports[index].Count);
         Assert.AreEqual(1, parser.Reports[index].Average);
         Assert.AreEqual(11, parser.Reports[index].Gain);
         Assert.AreEqual(1, parser.Reports[index].Sigma);
         Assert.AreEqual(1, parser.Reports[index].Cv);
         Assert.AreEqual(25, parser.Reports[index].Uniformity);
         Assert.AreEqual("", parser.Reports[index].Note);

         Assert.AreEqual(SexE.SEX_MALE, parser.Reports[index].Sex);
         Assert.AreEqual(3, parser.FoundRecords);
      }

   }
}
