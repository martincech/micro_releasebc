﻿using System;
using System.Collections.Generic;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;

namespace Bat2Library.Configuration
{
   public class FileWriter
   {
      private string BasePath { get; set; }
      private int SerialNumber { get; set; }

      public FileWriter(string basePath, int serialNumber)
      {
         BasePath = basePath;
         SerialNumber = serialNumber;
      }

      #region Configuration

      public void SaveAll(Bat2DeviceData data)
      {
         Save((writer) =>
         {
            if (data.Contacts != null)
            {
               writer.SaveContactList(data.Contacts);
            }

            if (data.CorrectionCurves != null)
            {
               writer.SaveCorrectionCurves(data.CorrectionCurves);
            }

            if (data.GrowthCurves != null)
            {
               writer.SaveGrowthCurves(data.GrowthCurves);
            }

            if (data.PredefinedWeighings != null)
            {
               writer.SavePredefinedWeighings(data.PredefinedWeighings);
            }

            if (data.WeighingPlans != null)
            {
               writer.SaveWeighingPlans(data.WeighingPlans);
            }
            if (data.Configuration != null)
            {
               writer.SaveConfig(data.Configuration);
            }
           
         });
      }

      #endregion

      #region Actions

      public void SaveAction_TimeSet(DateTime dateTime)
      {
         SaveAction((writer) => { writer.TimeSet(dateTime); });
      }

      public void SaveAction_WeighingStart(DateTime? dateTime)
      {
         SaveAction((writer) =>
         {
            if (dateTime == null)
            {
               writer.WeighingStart();
            }
            else
            {
               writer.WeighingStart((DateTime)dateTime);
            }
         });
      }

      public void SaveAction_WeighingStop()
      {
         SaveAction((writer) => { writer.WeighingStop(); });
      }

      public void SaveAction_WeighingSuspend()
      {
         SaveAction((writer) => { writer.WeighingSuspend(); });
      }

      public void SaveAction_WeighingRelease()
      {
         SaveAction((writer) => { writer.WeighingRelease(); });
      }

      #endregion

      private void Save(Action<IBat2NewDataWriter> save)
      {
         using (var socket = new Bat2FlashSocket(BasePath, SerialNumber))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               using (var rw = new Bat2NewDataRW(b2D))
               {
                  save(rw);
               }
            }
         }
      }

      private void SaveAction(Action<IBat2ActionCommander> action)
      {
         using (var socket = new Bat2FlashSocket(BasePath, SerialNumber))
         {
            using (var b2D = new FlashBat2Device(socket))
            {
               using (var rw = new Bat2NewDataRW(b2D))
               {
                  action(rw);
               }
            }
         }
      }
   }
}
