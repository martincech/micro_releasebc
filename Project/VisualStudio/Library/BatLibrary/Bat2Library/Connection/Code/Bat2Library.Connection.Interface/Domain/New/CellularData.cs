﻿using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
    /// <summary>
    /// SmsGate protocol options
    /// </summary>
    [DataContract]
    public class CellularData
    {
        #region Private fields

        #endregion

        [DataMember]
        public string Apn { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}