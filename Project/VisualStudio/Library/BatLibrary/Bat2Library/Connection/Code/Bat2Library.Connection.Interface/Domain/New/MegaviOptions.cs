﻿using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Configuration for Megavi protocol
   /// </summary>
   [DataContract]
   public class MegaviOptions
   {
      #region Private fields

       #endregion

      /// <summary>
      /// Id of protocol
      /// </summary>
      [DataMember]
      public byte Code { get; set; }

       /// <summary>
      /// Address of megavi protocol
      /// </summary>
      [DataMember]
      public byte Address { get; set; }

       /// <summary>
      /// Address of RS485 line to use
      /// </summary>
      [DataMember]
      public byte DeviceAddress { get; set; }
   }
}