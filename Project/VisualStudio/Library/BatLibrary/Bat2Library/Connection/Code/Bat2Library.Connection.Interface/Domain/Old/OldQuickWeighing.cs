﻿using System;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldQuickWeighing
    {
        public bool UseBothGenders { get; set; } // Flag, zda se ma pouzivat rozliseni na samce a samice

        public UInt16 InitialWeightFemale { get; set; }
        public UInt16 InitialWeightMale { get; set; }
       
    }
}
