﻿using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// SmsGate protocol options
   /// </summary>
   [DataContract]
   public class SmsGateOptions
   {
      /// <summary>
      /// Address of RS485 line to use
      /// </summary>
      [DataMember]
      public byte DeviceAddress { get; set; }
      /// <summary>
      /// Baud rate of line
      /// </summary>
      [DataMember]
      public int BaudRate { get; set; }
      /// <summary>
      /// Data bits on line
      /// </summary>
      [DataMember]
      public byte DataBits { get; set; }
      /// <summary>
      /// Parity on line
      /// </summary>
      [DataMember]
      public byte Parity { get; set; }
   }
}