﻿using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Menu context
   /// </summary>
   [DataContract]
   public class MenuContext
   {
      /// <summary>
      /// Is menu switched off?
      /// </summary>
      [DataMember]
      public bool SwitchedOff { get; set; }
   }
}