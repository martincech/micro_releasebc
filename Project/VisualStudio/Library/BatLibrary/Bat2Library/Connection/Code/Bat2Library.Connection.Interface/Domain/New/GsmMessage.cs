﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Configuration for GSM message module
   /// </summary>
   [DataContract]
   public class GsmMessage
   {
      #region Private fields
   
      //private DateTime sendAt;

       #endregion

      ///// <summary>
      ///// Time to send at
      ///// </summary>
      //[DataMember]
      //public DateTime SendAt
      //{
      //   get { return sendAt; }
      //   set { sendAt = value; }
      //}

      /// <summary>
      /// Enable commands input
      /// </summary>
      [DataMember]
      public bool CommandsEnabled { get; set; }

       /// <summary>
      /// Check allowed phone number on commands
      /// </summary>
      [DataMember]
      public bool CommandsCheckPhoneNumber { get; set; }

       /// <summary>
      /// Check command expiration
      /// </summary>
      [DataMember]
      public short CommandsExpiration { get; set; }

       /// <summary>
      /// Mask for events to be send
      /// </summary>
      [DataMember]
      public GsmEventMaskE EventMask { get; set; }

       /// <summary>
      /// Mode of operation
      /// </summary>
      [DataMember]
      public GsmPowerModeE Mode { get; set; }

       /// <summary>
      /// Period during which is module switched on
      /// </summary>
      [DataMember]
      public byte SwitchOnPeriod { get; set; }

       /// <summary>
      /// Duration of switch on 
      /// </summary>
      [DataMember]
      public byte SwitchOnDuration { get; set; }

       /// <summary>
      /// Specific times to switch on
      /// </summary>
      [DataMember]
      public List<TimeRange> SwitchOnTimes { get; set; }
   }
}