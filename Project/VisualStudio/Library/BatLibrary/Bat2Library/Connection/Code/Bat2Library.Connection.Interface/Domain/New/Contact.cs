﻿using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Contact for gsm module
   /// </summary>
   [DataContract]
   public class Contact
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Contact name
      /// </summary>
      [DataMember]
      public string Name { get; set; }

       /// <summary>
      /// Phone number
      /// </summary>
      [DataMember]
      public string PhoneNumber { get; set; }

       /// <summary>
      /// SMS format
      /// </summary>
      [DataMember]
      public GsmSmsFormatE SmsFormat { get; set; }

       /// <summary>
      /// Send daily statistics
      /// </summary>
      [DataMember]
      public bool Statistics { get; set; }

       /// <summary>
      /// Accept commands
      /// </summary>
      [DataMember]
      public bool Commands { get; set; }

       /// <summary>
      /// Send events
      /// </summary>
      [DataMember]
      public bool Events { get; set; }

       /// <summary>
      /// Send histogram also
      /// </summary>
      [DataMember]
      public bool SendHistogram { get; set; }
   }
}