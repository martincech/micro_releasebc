﻿using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Configuration for Megavi protocol
   /// </summary>
   [DataContract]
   public class MegaviOptions
   {
      /// <summary>
      /// Id of protocol
      /// </summary>
      [DataMember]
      public byte Id { get; set; }
      /// <summary>
      /// Address of RS485 line to use
      /// </summary>
      [DataMember]
      public byte DeviceAddress { get; set; }
   }
}