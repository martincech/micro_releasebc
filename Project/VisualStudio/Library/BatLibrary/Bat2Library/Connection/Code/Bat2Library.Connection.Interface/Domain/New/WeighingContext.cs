﻿using System;
using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Context of current weighing
   /// </summary>
   [DataContract]
   public class WeighingContext
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Actual status
      /// </summary>
      [DataMember]
      public WeighingStatusE Status { get; set; }

       /// <summary>
      /// Previous status
      /// </summary>
      [DataMember]
      public WeighingStatusE LastStatus { get; set; }

       /// <summary>
      /// This day is active (YES/NO)
      /// </summary>
      [DataMember]
      public bool DayActive { get; set; }

       /// <summary>
      ///  Unique ID per weighing
      /// </summary>
      [DataMember]
      public short Id { get; set; }

       /// <summary>
      ///  Current day number
      /// </summary>
      [DataMember]
      public short Day { get; set; }

       /// <summary>
      /// Target weight (male) for today
      /// </summary>
      [DataMember]
      public Int32 TargetWeight { get; set; }

       /// <summary>
      /// Target weight female for today
      /// </summary>
      [DataMember]
      public Int32 TargetWeightFemale { get; set; }

       /// <summary>
      /// Weighing correction factor (male)
      /// </summary>
      [DataMember]
      public short Correction { get; set; }

       /// <summary>
      /// Todays weighing started at this time
      /// </summary>
      [DataMember]
      public DateTime StartAt { get; set; }

       /// <summary>
      /// Zero day start time
      /// </summary>
      [DataMember]
      public DateTime DayZero { get; set; }

       /// <summary>
      /// Next day close time
      /// </summary>
      [DataMember]
      public DateTime DayCloseAt { get; set; }

       /// <summary>
      /// Day duration (24h or other for simulation)
      /// </summary>
      [DataMember]
      public TimeSpan DayDuration { get; set; }


       /// <summary>
      /// Growth curve for males this context uses
      /// </summary>
      [DataMember]
      public Curve GrowthCurveMale { get; set; }

       /// <summary>
      /// Growth curve for females this context uses
      /// </summary>
      [DataMember]
      public Curve GrowthCurveFemale { get; set; }

       /// <summary>
      /// Correction curve this context uses
      /// </summary>
      [DataMember]
      public Curve CorrectionCurve { get; set; }

       /// <summary>
      /// Weighing plan this context uses
      /// </summary>
      [DataMember]
      public WeighingPlan WeighingPlan { get; set; }
   }
}