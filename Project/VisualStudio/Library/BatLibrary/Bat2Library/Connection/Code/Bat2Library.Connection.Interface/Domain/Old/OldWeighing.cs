using System;
using System.Collections.Generic;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    // Struktura s provoznimi promennymi pro aktualni den vykrmu - tato je v RAM, jsou zde jiz predem predpocitane veci (meze z rustove krivky atd.) a dalsi
    // veci, ktere jsou stale potreba a v prubehu dne se nemeni, napr. pro zobrazeni. Je to jakasi cache dat z EEPROM, do RAM se to nacte vzdy po nejake
    // zmene v EEPROM, pri prechodu na dalsi den atd.
    // Meze musi byt long, kdyby zadal napr. 65.535kg +- 10%, tak horni mez se nevejde do 2 bajtu
    //TODO:Neexistuje ve starem projektu a podle komentare se jedna pouze o objek uchovavany v pameti. Asi smazat!  
    public class OldWeighing
    {
        public OldFlockHeader Header { get; set; }

        // Cislo dne od pocatku vykrmu
        public UInt16 DayNumber { get; set; }

        // Normovana hmotnost nactena z rustove krivky pro samice a samce
        public IEnumerable<UInt16> TargetWeight { get; set; }

        // Horni mez hmotnosti samic a samcu
        public IEnumerable<Single> MarginAbove { get; set; }

        // Dolni mez hmotnosti samic a samcu
        public IEnumerable<Single> MarginBelow { get; set; }

        // Hmotnost pro porovnani nactena z rustove porovnavaci krivky pro samice a samce
        public IEnumerable<UInt16> ComparisonWeight { get; set; }
    }
}