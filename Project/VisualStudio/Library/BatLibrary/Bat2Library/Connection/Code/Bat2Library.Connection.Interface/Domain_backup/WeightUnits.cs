using System;
using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Weighing units to weight with
   /// </summary>
   [DataContract]
   public class WeightUnits
   {
      /// <summary>
      /// unit range
      /// </summary>
      [DataMember]
      public Int32 Range { get; set; }
      /// <summary>
      /// Unit type
      /// </summary>
      [DataMember]
      public byte Units { get; set; }
      /// <summary>
      /// Decimal positions
      /// </summary>
      [DataMember]
      public byte Decimals { get; set; }
      /// <summary>
      /// Division factor
      /// </summary>
      [DataMember]
      public short Division { get; set; }
      /// <summary>
      /// Maximum division factor
      /// </summary>
      [DataMember]
      public short DivisionMax { get; set; }
   }
}