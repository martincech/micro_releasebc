﻿using System.Collections.Generic;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldGsm : BaseContact
    {
        // Flag, zda se GSM modul vyuziva (zda je pripojen)
        public bool Use { get; set; }

        // Flag, zda se ma posilat o pulnoci statistika 
        public bool SendMidnight { get; set; }

        // Perioda ve dnech, kdy se ma posilat pulnocni statistika
        public byte PeriodMidnight { get; set; }

        // Flag, zda se ma odpovidat na zaslane SMS
        public bool SendOnRequest { get; set; }

        // Flag, zda se ma provadet cinnost podle zaslanych SMS (zatim nevyuzito)
        public bool ExecuteOnRequest { get; set; }

        // Flag, zda se ma pri prijeti prikazu kontrolovat cislo (odpovi se jen na prikazy odeslane z cisla, ktere je v seznamu)
        public bool CheckNumbers { get; set; }

        // Pocet definovanych telefonnich cisel, na ktere se vysila o pulnoci
        public byte NumberCount { get; set; }

        // Definovana telefonni cisla //TODO: Max 5 cisel + Max delka 15 znaků
        public IEnumerable<string> Numbers { get; set; }

        #region Bat2Old v1.50

        // Do tohoto dne vcetne se posila s periodou PeriodMidnight1
        public ushort DayMidnight1 { get; set; }

        // Hodina, pri ktere se ma poslat pulnocni SMS
        public byte MidnightSendHour { get; set; }

        // Minuta, pri ktere se ma poslat pulnocni SMS
        public byte MidnightSendMin { get; set; }

        // Perioda ve dnech, kdy se ma posilat pulnocni statistika do dne DayMidnight1
        public byte PeriodMidnight1 { get; set; }

        // Perioda posilani po dni DayMidnight1
        public byte PeriodMidnight2 { get; set; }

        #endregion
    }
}
