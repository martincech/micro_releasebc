using System;
using System.ServiceModel;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.Domain.Old;


namespace Bat2Library.Connection.Interface.Contract
{
   /// <summary>
   /// Contract for connection DeviceStatus messages
   /// </summary>
   [ServiceContract]
   public interface IBat2ConnectionEventsContract
   {
      /// <summary>
      /// Event is invoked when device is connected.
      /// </summary>
      /// <param name="deviceData">device data according to connected device</param>
      [OperationContract(IsOneWay = true)]
      void DeviceConnected(Bat2DeviceData deviceData);

      /// <summary>
      /// Event is invoked when device is disconnected.
      /// </summary>
      /// <param name="deviceData">device data according to disconnected device</param>
      [OperationContract(IsOneWay = true)]
      void DeviceDisconnected(Bat2DeviceData deviceData);
   }

   /// <summary>
   /// Contreact for messages about data readed from device
   /// </summary>
   [ServiceContract]
   public interface IBat2DataReadEventsContract
   {
      /// <summary>
      /// Event is invoked when new data are readed from device.
      /// </summary>
      /// <param name="data">Data readed from device</param>
      [OperationContract(IsOneWay = true)]
       void DataRead(Bat2DeviceData data);
   }

   /// <summary>
   /// Contract for messages about data writen to device
   /// </summary>
   [ServiceContract]
   public interface IBat2DataWriteEventsContract
   {
      /// <summary>
      /// Event is invoked when new data are writen to device
      /// </summary>
      /// <param name="data">Data writen to device</param>
      [OperationContract(IsOneWay = true)]
       void DataWriten(Bat2DeviceData data);
   }

   /// <summary>
   /// Contact for messages about action results
   /// </summary>
   [ServiceContract]
   public interface IBat2ActionEventsContract
   {
      [OperationContract(IsOneWay = true)]
      void WeighingStarted(Bat2DeviceData deviceData);
      [OperationContract(IsOneWay = true)]
      void WeighingPlanned(Bat2DeviceData deviceData, DateTime at);
      [OperationContract(IsOneWay = true)]
      void WeighingStopped(Bat2DeviceData deviceData);
      [OperationContract(IsOneWay = true)]
      void WeighingSuspended(Bat2DeviceData deviceData);
      [OperationContract(IsOneWay = true)]
      void WeighingReleased(Bat2DeviceData deviceData);
      [OperationContract(IsOneWay = true)]
      void TimeSet(Bat2DeviceData deviceData, DateTime time);
      [OperationContract(IsOneWay = true)]
      void TimeGet(Bat2DeviceData deviceData, DateTime time);
   }

   /// <summary>
   /// Contact for messages about errors
   /// </summary>
   [ServiceContract]
   public interface IBat2ErrorEventsContract
   {
       [OperationContract(IsOneWay = true)]
       void ErrorRised(Bat2DeviceData deviceData, ErrorMessage errorMessage);
   }

   /// <summary>
   /// This service serves for server -> client data propagation purposes
   /// </summary>
   [ServiceContract]
   public interface IBat2EventsContract : 
      IBat2ConnectionEventsContract,
      IBat2DataReadEventsContract,
      IBat2DataWriteEventsContract,
      IBat2ActionEventsContract,
      IBat2ErrorEventsContract
   {
   }
}