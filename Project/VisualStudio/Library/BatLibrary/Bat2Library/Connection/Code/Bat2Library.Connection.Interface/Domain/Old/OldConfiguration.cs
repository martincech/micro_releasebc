﻿using System.Collections.Generic;

namespace Bat2Library.Connection.Interface.Domain.Old
{
   public sealed class OldConfiguration : BaseConfiguration<BaseVersionInfo>
   {
      public OldConfiguration()
      {
         VersionInfo = new BaseVersionInfo();
      }

      // BUILD
      public byte Build { get; set; }

      // Verze hardware (GSM, Dacs LW1, Modbus, ...)   
      public THwVersion HwVersion { get; set; }

      public byte Language { get; set; }

      // Okoli nad prumerem pro samce
      public byte MarginAboveMale { get; set; }

      // Okoli nad prumerem pro samice
      public byte MarginAboveFemale { get; set; }

      // Okoli pod prumerem pro samce
      public byte MarginBelowMale { get; set; }

      // Okoli pod prumerem pro samice
      public byte MarginBelowFemale { get; set; }

      // Filtr prevodniku
      public byte Filter { get; set; }

      // Ustaleni hmotnosti v +- desetinach procenta (max +-25.0%)
      public byte StabilizationRange { get; set; }

      // Delka ustaleni hmotnosti v krocich prevodu (cca 0.5sec) (2 - 9)
      public byte StabilizationTime { get; set; }

      // Typ rezimu automatickeho hledani cilove hmotnosti (bez nebo s pouzitim denniho prirustku)
      public TAutoMode AutoMode { get; set; }

      // Typ naskoku/seskoku na vahu, ktery se vyhodnocuje      
      public PlatformStepModeE JumpMode { get; set; }

      // Zobrazovane jednotky
      public WeightUnitsE Units { get; set; }

      // Rozsah histogramu v +- % stredni hodnoty
      public byte HistogramRange { get; set; }

      // Rozsah uniformity v +- %
      public byte UniformityRange { get; set; }

      // Parametry vazeni
      public OldWeighingStart WeighingStart { get; set; }

      // Parametry GSM modulu
      public OldGsm Gsm { get; set; }

      // Nastaveny rezim podsvitu
      public BacklightModeE Backlight { get; set; }

      // Vaha je napajena z baterie - zatim se nepouziva 
      public byte Battery { get; set; }

      // Parametry linky RS-485
      public OldRs485 Rs485 { get; set; }

      // Cislo hejna, se kterym se porovnavaji vysledky, pripadne FLOCK_EMPTY_NUMBER pokud se neporovnava
      public byte ComparisonFlock { get; set; }

      // Korekce hmotnosti
      public OldWeightCorrection WeightCorrection { get; set; }

      // Kalibrace zkopirovana z interni EEPROM
      public OldCalibration Calibration { get; set; }

      // Definice jednotlivych hejn !! MAX 10 TODO: Nejaka forma kotroly ?
      public IEnumerable<OldFlock> Flocks { get; set; }
   }
}
