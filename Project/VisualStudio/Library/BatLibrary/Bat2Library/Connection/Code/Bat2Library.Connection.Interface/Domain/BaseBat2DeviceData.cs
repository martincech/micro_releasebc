﻿namespace Bat2Library.Connection.Interface.Domain.Old
{
   public class BaseBat2DeviceData<S, T> : IBaseBat2DeviceData
      where S : BaseVersionInfo
      where T : BaseConfiguration<S> 
   {
      public T Configuration { get; set; }

      public bool IsValid
      {
         get { return Configuration.IsValid; }
      }
   }

   public interface IBaseBat2DeviceData
   {
      bool IsValid { get; }
   }
}
