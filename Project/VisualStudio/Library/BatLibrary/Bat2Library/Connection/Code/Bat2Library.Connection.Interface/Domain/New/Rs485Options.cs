﻿using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Configuration of RS485 line
   /// </summary>
   [DataContract]
   public class Rs485Options
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Enabling/disabling
      /// </summary>
      [DataMember]
      public bool Enabled { get; set; }

       /// <summary>
      /// Mode of operation
      /// </summary>
      [DataMember]
      public Rs485ModeE Mode { get; set; }
   }
}