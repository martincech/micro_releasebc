﻿using System.Runtime.Serialization;

namespace Connection.Interface.Domain
{
   /// <summary>
   /// Info about device
   /// </summary>
   [DataContract]
   public class DeviceInfo
   {
      /// <summary>
      /// Name of device
      /// </summary>
      [DataMember]
      public string Name { get; set; }
      /// <summary>
      /// Password protection
      /// </summary>
      [DataMember]
      public string Password { get; set; }
   }
}