﻿using System;

namespace Bat2Library.Connection.Interface.Domain.Old
{
    public class OldArchive : BaseArchive
    {
        // Cislo dne od pocatku vykrmu
        public UInt16 DayNumber { get; set; } 

        // datum a cas porizeni
        public DateTime DateTime { get; set; }

        //Archiv dart samce
        public OldArchiveItem ArchiveItemMale { get; set; }

        //Archiv dat samice
        public OldArchiveItem ArchiveItemFemale { get; set; }

        // YES/NO, zda se pouzil presny vypocet uniformity, ktery je v promennych RealnUniformitaSamice a RealnUniformitaSamci
        public bool RealUniformityUsed { get; set; } 
    } 
}
