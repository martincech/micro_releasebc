﻿using System.Runtime.Serialization;

namespace Bat2Library.Connection.Interface.Domain
{
   /// <summary>
   /// Info about device
   /// </summary>
   [DataContract]
   public class DeviceInfo
   {
      #region Private fields

       #endregion


      /// <summary>
      /// Name of device
      /// </summary>
      [DataMember(IsRequired = true)]
      public string Name { get; set; }

       /// <summary>
      /// Password protection
      /// </summary>
      [DataMember(IsRequired = true)]
      public string Password { get; set; }
   }
}