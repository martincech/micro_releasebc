#pragma once
#include "Bat2NewDevice.h"
#include "Bat2UsbSocket.h"
#include "Hardware.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System;
            using namespace Bat2Library::Connection::Interface::Domain;
            
            private ref class UsbBat2Device : Bat2NewDevice
            {
            public:

               static const int BAT2_VID = MY_VID;
               static const int BAT2_PID = MY_PID;

               static bool IsValid(int vid, int pid)
               {
                  return vid == MY_VID && pid == MY_PID;
               }

               UsbBat2Device(Bat2UsbSocket ^socket)
                  : Bat2NewDevice(socket, gcnew Bat2DeviceData())
               {
               }

               property Bat2UsbSocket ^UsbSocket{ Bat2UsbSocket ^get(){ return static_cast<Bat2UsbSocket ^>(Socket); }}
            };
         }
      }
   }
}