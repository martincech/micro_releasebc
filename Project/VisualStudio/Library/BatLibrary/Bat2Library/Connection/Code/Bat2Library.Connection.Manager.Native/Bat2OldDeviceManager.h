#pragma once
#include "Bat2DeviceManager.h"
#include "Bat2OldDataRW.h"
#include "Bat2OldDevice.h"

namespace Bat2Library
{
	namespace Connection
	{
		namespace Manager
		{
			namespace Native{
            using namespace System::Diagnostics;
            using namespace Bat2Library::Connection::Interface::IO;

            public ref class Bat2OldDeviceManager abstract : 
               Bat2DeviceManager <
                  Bat2OldINativeSocket ^,
                  BaseVersionInfo ^,
                  OldConfiguration ^,
                  OldBat2DeviceData ^,
                  IBat2OldDataReader ^,
                  IBat2OldDataWriter ^,
                  IBat2ActionCommander ^>,
               IBat2OldDeviceManager
				{
				protected:
               virtual Bat2DataRW<BaseVersionInfo ^, OldConfiguration ^, Bat2OldINativeSocket ^, OldBat2DeviceData ^> ^CreateDataRwObject(Bat2Device<BaseVersionInfo ^, OldConfiguration ^, Bat2OldINativeSocket ^, OldBat2DeviceData^> ^dev) override sealed
               {
                  Bat2OldDevice ^b2d = static_cast<Bat2OldDevice ^>(dev);
                  Debug::Assert(b2d != nullptr);
                  return CreateDataRwObject(b2d);
               }

               virtual Bat2OldDataRW ^CreateDataRwObject(Bat2OldDevice ^dev) override sealed
               {
                  return gcnew Bat2OldDataRW(dev);
               }
				};
			}
		}
	}
}