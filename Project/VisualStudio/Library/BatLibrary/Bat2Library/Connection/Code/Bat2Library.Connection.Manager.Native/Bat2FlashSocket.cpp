#include "Bat2FlashSocket.h"
#undef _MANAGED
#include "Remote\SocketIfMsd.h"
#include "Remote\Rc.h"
#include "File\Efs.h"

using namespace System;
using namespace Usb::UMS;
using namespace System::Runtime::InteropServices;
using namespace Bat2Library::Connection::Manager::Native;

FlashSocketIf::FlashSocketIf(String ^efsRoot)
{
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(efsRoot);
   ::EfsRoot((const char *)strPtr.ToPointer());
}

byte FlashSocketIf::State(void)
{
   return SocketIfMsdState(this);
}

bool FlashSocketIf::OpenForRead()
{
   if (IsOpenedForRead()){
      return true;
   }
   Close();
   return SocketIfMsdOpenForReceiveCmd() == YES;
}
bool FlashSocketIf::OpenForWrite()
{
   if (IsOpenedForWrite()){
      return true;
   }
   Close();
   return SocketIfMsdOpenForSendCmd(this) == YES;
}

bool FlashSocketIf::IsOpenedForRead()
{
   return (Permission() & FILE_MODE_READ_ONLY) == FILE_MODE_READ_ONLY;
}
bool FlashSocketIf::IsOpenedForWrite()
{
   return (Permission() & FILE_MODE_WRITE_ONLY) == FILE_MODE_WRITE_ONLY;
}

TYesNo FlashSocketIf::Receive(void *Buffer, int Size)
{
   if (!OpenForRead()){
      return NO;
   }
   return SocketIfMsdReceive(this, Buffer, Size);
}

int FlashSocketIf::ReceiveSize(void)
{
   return SocketIfMsdReceiveSize(this);
}

TYesNo FlashSocketIf::Send(const void *Buffer, int Size)
{
   if (!OpenForWrite()){
      return NO;
   }
   return SocketIfMsdSend(this, Buffer, Size);
}

void FlashSocketIf::Close(void)
{
   SocketIfMsdClose(this);
}

byte FlashSocketIf::Permission(void)
{
   return SocketIfMsdPermission(this);
}

Bat2FlashSocket::Bat2FlashSocket(String ^basePath, int serialNumber)
{
   this->serialNumber = serialNumber;
   baseDirectory = basePath;
   socket = new FlashSocketIf(basePath);
   DataLoaded = false;
   EverythingRead = false;
}

Bat2FlashSocket::~Bat2FlashSocket()
{
   if (socket != NULL){
      Close();
      delete socket;
      socket = NULL;
   }
}
bool Bat2FlashSocket::SetupConnection()
{
	if (!Bat2NewINativeSocket::SetupConnection()){
      return false;
   }
   IntPtr strPtr = Marshal::StringToHGlobalAnsi(baseDirectory);
   ::EfsRoot((const char *)strPtr.ToPointer());
   SocketIfMsdSetSerialNumber(serialNumber);
   DataLoaded = false;
   return true;
}
bool Bat2FlashSocket::FreeConnection()
{
   DataLoaded = false;
   return Bat2NewINativeSocket::FreeConnection();
}

TYesNo Bat2FlashSocket::LoadMethod()
{
#define BUF_SIZE 255
   if (EverythingRead){ return NO; }
   if (DataLoaded){ return YES; }
   array<byte> ^buf = gcnew array <byte>(BUF_SIZE);
   TRcId RcId;
   word ReplyLength;
   RcInit();
   if (!socket->OpenForRead()){ return NO; }
   RcId.SessionId = RcSessionStart();
   RcId.InterfaceId = 0;
   while (Receive(buf) && State() != Bat2Library::SocketStateE::SOCKET_STATE_DISCONNECTED){
      pin_ptr<byte> ptr = &buf[0];
      TRcCmd *Command = (TRcCmd *)ptr;
      if (Command->Cmd < _RC_CMD_NO_SESSION){
         if (RcCommand(&RcId, Command, SocketIfMsdReceiveSize(NULL), (TRcReply *)ptr, &ReplyLength) == NO){
            return NO;
         }
         
      }
   }
   RcSessionTerminate(RcId.SessionId);
   SocketIfMsdSetSerialNumber(serialNumber);
   DataLoaded = true;
   return YES;
}
bool Bat2FlashSocket::SaveMethod()
{
   if (!socket->OpenForWrite()){ return false; }
   SocketIfMsdSetSerialNumber(serialNumber);
   return true;
}

TYesNo Bat2FlashSocket::ConfigRemoteLoad()  {
   return LoadMethod();
}

TYesNo Bat2FlashSocket::ArchiveRemoteLoad()  {
   return LoadMethod();
}

TYesNo Bat2FlashSocket::ContactListRemoteLoad()  {
   return LoadMethod();
}


TYesNo Bat2FlashSocket::ContextRemoteLoad()  {
   return LoadMethod();
}

TYesNo Bat2FlashSocket::CurveListRemoteLoad()  {
   return LoadMethod();
}

TYesNo Bat2FlashSocket::CorrectionListRemoteLoad()  {
   return LoadMethod();
}

TYesNo Bat2FlashSocket::WeighingPlanListRemoteLoad()  {
   return LoadMethod();
}

TYesNo Bat2FlashSocket::PredefinedListRemoteLoad()  {
   return LoadMethod();
}

TYesNo Bat2FlashSocket::PredefinedListRemoteSave()  {
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::PredefinedListRemoteSave();
}
TYesNo Bat2FlashSocket::WeighingPlanListRemoteSave()  {
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::WeighingPlanListRemoteSave();
}
TYesNo Bat2FlashSocket::CorrectionListRemoteSave()  {
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::CorrectionListRemoteSave();
}
TYesNo Bat2FlashSocket::CurveListRemoteSave()  {
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::CurveListRemoteSave();
}
TYesNo Bat2FlashSocket::ConfigRemoteSave()  {
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::ConfigRemoteSave();
}
TYesNo Bat2FlashSocket::ContactListRemoteSave()  {
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::ContactListRemoteSave();
}


TYesNo Bat2FlashSocket::ActionRemoteWeighingSchedulerStart(void)
{
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::ActionRemoteWeighingSchedulerStart();
}
TYesNo Bat2FlashSocket::ActionRemoteWeighingSchedulerStartAt(UClockGauge DateTime)
{
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::ActionRemoteWeighingSchedulerStartAt(DateTime);
}
TYesNo Bat2FlashSocket::ActionRemoteWeighingSchedulerStop(void)
{
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::ActionRemoteWeighingSchedulerStop();
}
TYesNo Bat2FlashSocket::ActionRemoteWeighingSchedulerSuspend(void)
{
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::ActionRemoteWeighingSchedulerSuspend();
}
TYesNo Bat2FlashSocket::ActionRemoteWeighingSchedulerRelease(void)
{
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::ActionRemoteWeighingSchedulerRelease();
}
TYesNo Bat2FlashSocket::ActionRemoteTimeSet(UDateTimeGauge DateTime)
{
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::ActionRemoteTimeSet(DateTime);
}
TYesNo Bat2FlashSocket::ActionRemoteTimeGet(UDateTimeGauge *DateTime)
{
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::ActionRemoteTimeGet(DateTime);
}
TYesNo Bat2FlashSocket::ActionRemoteReboot()
{
   if (!SaveMethod()){ return NO; }
   return Bat2NewINativeSocket::ActionRemoteReboot();
}