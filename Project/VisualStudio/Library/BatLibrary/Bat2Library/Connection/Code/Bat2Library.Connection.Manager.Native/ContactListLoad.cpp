#include "Bat2NewDataRW.h"
using namespace System;
using namespace System::Collections::Generic;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface::Domain;
#include <assert.h>

#undef _MANAGED
#include "Message\ContactList.h"

bool LoadList(List<Contact^> ^%contacts);

bool Bat2NewDataRW::Load(List<Contact^> ^%contacts)
{
   if (device->Socket->ContactListRemoteLoad() == NO){
      return false;
   }
   return LoadList(contacts);
}

bool Bat2NewDataRW::LoadList(List<Contact^> ^%contacts)
{
	TContactList list;
	TContactIndex itemCount;
	TGsmContact item;
	ContactListInit();
	if (ContactListOpen(&list) == NO){
		return false;
	}
	itemCount = ContactListCount(&list);
	for (TContactIndex i = 0; i < itemCount; i++){
		ContactListLoad(&list, &item, i);
		Contact ^contact = gcnew Contact();
		contact->Name = gcnew String(item.Name);
		contact->PhoneNumber = gcnew String(item.PhoneNumber);
		contact->SmsFormat = (GsmSmsFormatE)item.SmsFormat;
		contact->Statistics = item.Statistics == YES;
		contact->Commands = item.Commands == YES;
		contact->Events = item.Events == YES;		
		contact->SendHistogram = item.SendHistogram == YES;
		contacts->Add(contact);
	}
	ContactListClose(&list);
	return true;
}