#include "Bat2OldUsbSocket.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Diagnostics;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Bat2Old::Constants;
using namespace Bat2Library::Bat2Old;
using namespace Bat2Library::Connection::Interface::Domain::Old;

Bat2OldUsbSocket::Bat2OldUsbSocket(Stream ^socket)
{
   Socket = socket;
}

TYesNo Bat2OldUsbSocket::ConfigRemoteLoad()
{
   array<byte> ^data = gcnew array<byte>(TConstants::FL_CONFIG_SIZE);
   Debug::Assert(Socket->CanSeek);
   Socket->Seek(TConstants::FL_CONFIG_BASE, SeekOrigin::Begin);
   if (Socket->Read(data, 0, data->Length) != TConstants::FL_CONFIG_SIZE)
   {
      Bat2Memory = nullptr;
      return NO;
   }
   Bat2Memory = data;
   return YES;
}
TYesNo Bat2OldUsbSocket::ConfigRemoteSave() 
{ 
   if (Bat2Memory == nullptr || 
      ((Marshal::SizeOf(Bat2Old::Flash::v111::TConfigSection::typeid) != Bat2Memory->Length) &&
       (Marshal::SizeOf(Bat2Old::Flash::v150::TConfigSection::typeid) != Bat2Memory->Length)
      ))
   {
      return NO;
   }
   Debug::Assert(Socket->CanSeek);
   Socket->Seek(TConstants::FL_CONFIG_BASE, SeekOrigin::Begin);
   
   Socket->Write(Bat2Memory, 0, Bat2Memory->Length);
   return YES;
}

TYesNo Bat2OldUsbSocket::ArchiveRemoteLoad() 
{
   array<byte> ^data = gcnew array<byte>(TConstants::FL_ARCHIVE_SIZE);
   Debug::Assert(Socket->CanSeek);
   Socket->Seek(TConstants::FL_ARCHIVE_BASE, SeekOrigin::Begin);
   if (Socket->Read(data, 0, data->Length) != TConstants::FL_ARCHIVE_SIZE)
   {
      Bat2Memory = nullptr;
      return NO;
   }
   Bat2Memory = data;
   return YES;
}

TYesNo Bat2OldUsbSocket::ActionRemoteWeighingSchedulerStart(void) { return NO;}
TYesNo Bat2OldUsbSocket::ActionRemoteWeighingSchedulerStartAt(UClockGauge DateTime) { return NO;}
TYesNo Bat2OldUsbSocket::ActionRemoteWeighingSchedulerStop(void) { return NO;}
TYesNo Bat2OldUsbSocket::ActionRemoteWeighingSchedulerSuspend(void) { return NO;}
TYesNo Bat2OldUsbSocket::ActionRemoteWeighingSchedulerRelease(void) { return NO;}
TYesNo Bat2OldUsbSocket::ActionRemoteTimeSet(UDateTimeGauge DateTime) { return NO;}
TYesNo Bat2OldUsbSocket::ActionRemoteTimeGet(UDateTimeGauge *DateTime) { return NO;}
TYesNo Bat2OldUsbSocket::ActionRemoteReboot() { return NO;}