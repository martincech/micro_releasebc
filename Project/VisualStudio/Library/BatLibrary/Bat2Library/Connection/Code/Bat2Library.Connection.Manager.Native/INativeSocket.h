#pragma once

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{

            private interface class INativeSocket
            {
               bool SetupConnection();
               bool FreeConnection();
            };
         }
      }
   }
}