#include "Bat2UsbSocket.h"

#define CHUNK_SIZE 64
using namespace System::IO;
using namespace System;

UsbSocketIf::UsbSocketIf(Stream ^device)
{
   this->device = device;
   state = new TFrameState();
   FrameInit(state);
   ReplySize = 0;
   isClosed = false;
   CheckOpen();
}


UsbSocketIf::~UsbSocketIf()
{
   delete state;
}

byte UsbSocketIf::State(void)
{
   if (static_cast<Stream^>(device) == nullptr || isClosed)
   {
      return SOCKET_STATE_DISCONNECTED;
   }
   switch (state->Status){
   case FRAME_STATE_IDLE:
      return SOCKET_STATE_CONNECTED;

   case FRAME_STATE_SEND_ACTIVE:
      return SOCKET_STATE_SEND_ACTIVE;

   case FRAME_STATE_SEND_DONE:
      return SOCKET_STATE_SEND_DONE;

   case FRAME_STATE_RECEIVE_ACTIVE:
   case FRAME_STATE_RECEIVE_WAITING_HEADER:
      return SOCKET_STATE_RECEIVE_ACTIVE;

   case FRAME_STATE_RECEIVE_FRAME:
      return SOCKET_STATE_RECEIVE_DONE;
   default:
      return SOCKET_STATE_SEND_ERROR;
   }
}

TYesNo UsbSocketIf::Receive(void *Buffer, int Size)
{
   gcroot<array<byte> ^> chunkBuf = gcnew array<byte>(CHUNK_SIZE);
   pin_ptr<byte> Chunk = &chunkBuf[0];
   if (!CheckOpen()) {
      return NO;
   }

   if (!FrameReceiveStart(state, Buffer, Size)) {
      return NO;
   }

   forever{
      int rcvSize = device->Read(chunkBuf, 0, CHUNK_SIZE);
      if (rcvSize == 0) {
         return NO;
      }

      FrameReceiveProccess(state, Chunk, rcvSize);
      switch (state->Status) {
      case FRAME_STATE_RECEIVE_ACTIVE:
         break;

      case FRAME_STATE_RECEIVE_FRAME:
         ReplySize = state->SizeProccessed;
         return YES;

      default:
         return NO;
      }
   }
}

int UsbSocketIf::ReceiveSize(void)
{
   return ReplySize;
}

TYesNo UsbSocketIf::Send(const void *Buffer, int Size)
{
   gcroot<array<byte> ^> chunkBuf = gcnew array<byte>(Size + CHUNK_SIZE);
   pin_ptr<byte> Chunk = &chunkBuf[0];
   int ChunkSize;
   if (!CheckOpen()) {
      return NO;
   }

   if (!FrameSendInit(state, Buffer, Size)) {
      return NO;
   }

   forever{
      if (!FrameSendProccess(state, Chunk, chunkBuf->Length, &ChunkSize)) {
         return NO;
      }
      try{
         device->Write(chunkBuf, 0, ChunkSize);
      }
      catch (Exception ^){
         return NO;
      }
      switch (state->Status) {
      case FRAME_STATE_SEND_ACTIVE:
         break;

      case FRAME_STATE_SEND_DONE:
         return YES;

      default:
         return NO;
      }
   }
}

void UsbSocketIf::Close(void)
{
   isClosed = true;
}

byte UsbSocketIf::Permission(void)
{
   byte mode = 0;
   mode |= device->CanRead ? FILE_MODE_READ_ONLY : 0;
   mode |= device->CanWrite ? FILE_MODE_WRITE_ONLY : 0;
   return mode;
}

bool UsbSocketIf::CheckOpen()
{
   return static_cast<Stream^>(device) != nullptr && !isClosed;
}



