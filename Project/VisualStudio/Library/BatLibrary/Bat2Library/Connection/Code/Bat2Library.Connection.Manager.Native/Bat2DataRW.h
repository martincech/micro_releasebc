#pragma once
#include "Bat2Device.h"
#include "Bat2INativeSocket.h"

namespace Bat2Library
{
	namespace Connection
	{
		namespace Manager
		{
			namespace Native{

				using namespace Bat2Library::Connection::Interface::IO;
				using namespace Bat2Library::Connection::Interface::Domain;
				using namespace System::Collections::Generic;
				using namespace System::Reflection;
				using namespace Bat2Library;
				using namespace System::Runtime::InteropServices;

            generic<typename R, typename S, typename U, typename V>
               where R: BaseVersionInfo
               where S : BaseConfiguration<R>
               where U: Bat2INativeSocket
               where V : BaseBat2DeviceData<R, S>
            public ref class Bat2DataRW abstract :
               IBat2DataReader<R, S>,
               IBat2DataWriter<R, S>,
               IBat2ActionCommander,
               IDisposable
				{
            public:
               Bat2DataRW(Bat2Device<R, S, U, V> ^device);
               ~Bat2DataRW();
               !Bat2DataRW();

               virtual bool SaveConfig(S config) abstract;
               virtual S LoadConfig() abstract;

               // Action methods
               virtual bool WeighingStart() sealed;
               virtual bool WeighingStart(DateTime at) sealed;
               virtual bool WeighingStop() sealed;
               virtual bool WeighingSuspend() sealed;
               virtual bool WeighingRelease() sealed;
               virtual bool TimeSet(DateTime at) sealed;
               virtual bool TimeGet(Nullable<DateTime> %time) sealed;


				protected:
					// device to comunicate with
               Bat2Device<R, S, U, V> ^device;

					// data lock for synchronization purposes
					static Object ^dataLock = gcnew Object();

					// delegates and invocation functions for data loading
					generic <class T> where T : ref class, gcnew()
						delegate bool LoadFuncRef(T %value);
					// delegates and invocation functions for data saving
					generic <class T> where T : ref class
						delegate bool SaveFuncRef(T value);

					// make communication action (actual load functions)
					generic <class T> where T : ref class, gcnew()
						T DoLoad(LoadFuncRef<T> ^LoadAction, PropertyInfo ^setProperty);
					generic <class T> where T : ref class, gcnew()
						T DoLoadExecute(LoadFuncRef<T> ^LoadAction, PropertyInfo ^setProperty);
					// make communication action (actual save functions)
					generic <class T> where T : ref class
						bool DoSave(SaveFuncRef<T> ^SaveAction, T param, PropertyInfo ^setProperty);
					// make action on remote device
					bool DoAction(ActionCmdE action, Object ^param, [Out] Object^%outParam);

					// find property in Bat2DeviceData and set it to value of T, find according to type
					generic <class T> where T : ref class
						void FindAndSetProperty(T value, PropertyInfo ^setProperty);
					generic <class T> where T : ref class
						T FindAndGetProperty(PropertyInfo ^prop);

					bool Action(ActionCmdE action, Object ^param, [Out] Object^%outParam);
				};

			}
		}
	}
}