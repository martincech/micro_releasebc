#pragma once

#undef _MANAGED
#include "Remote\Frame.h"
#include "Remote\SocketIf.h"

private class FlashSocketIf : SocketIf
{
public:
   FlashSocketIf(System::String ^efsRoot);
   virtual byte State(void);

   virtual TYesNo Receive(void *Buffer, int Size);

   virtual int ReceiveSize(void);

   virtual TYesNo Send(const void *Buffer, int Size);

   virtual void Close(void);

   virtual byte Permission(void);

   bool OpenForRead();
   bool OpenForWrite();

private:
   bool IsOpenedForRead();
   bool IsOpenedForWrite();
};

#include "INativeSocket.h"
#include "Bat2NewINativeSocket.h"
#include "IStreamSocket.h"
#include "Remote\SocketIfMsdDef.h"

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace System;
            using namespace Usb::UMS;
            using namespace System::Diagnostics;
            using namespace System::Threading;
            using namespace ::Native;

            private ref class Bat2FlashSocket : Bat2NewINativeSocket
            {
            public:
               Bat2FlashSocket(String ^basePath, int serialNumber);
               ~Bat2FlashSocket();


               virtual bool SetupConnection() override;
               virtual bool FreeConnection() override;
               virtual Bat2Library::FileModeE Permission(void) override{ return Bat2Library::FileModeE::FILE_MODE_READ_WRITE; }

               property SocketIf *NativeSocket{virtual SocketIf *get() override { return (SocketIf*)socket; }}

               virtual TYesNo ConfigRemoteLoad() override;
               virtual TYesNo ArchiveRemoteLoad() override;
               virtual TYesNo ContactListRemoteLoad() override;
               virtual TYesNo ContextRemoteLoad() override;
               virtual TYesNo CurveListRemoteLoad() override;
               virtual TYesNo CorrectionListRemoteLoad() override;
               virtual TYesNo WeighingPlanListRemoteLoad() override;
               virtual TYesNo PredefinedListRemoteLoad() override;

               virtual TYesNo PredefinedListRemoteSave() override;
               virtual TYesNo WeighingPlanListRemoteSave() override;
               virtual TYesNo CorrectionListRemoteSave() override;
               virtual TYesNo CurveListRemoteSave() override;
               virtual TYesNo ConfigRemoteSave() override;
               virtual TYesNo ContactListRemoteSave() override;

               virtual TYesNo ActionRemoteWeighingSchedulerStart(void) override;
               virtual TYesNo ActionRemoteWeighingSchedulerStartAt(UClockGauge DateTime) override;
               virtual TYesNo ActionRemoteWeighingSchedulerStop(void) override;
               virtual TYesNo ActionRemoteWeighingSchedulerSuspend(void) override;
               virtual TYesNo ActionRemoteWeighingSchedulerRelease(void) override;
               virtual TYesNo ActionRemoteTimeSet(UDateTimeGauge DateTime) override;
               virtual TYesNo ActionRemoteTimeGet(UDateTimeGauge *DateTime) override;
               virtual TYesNo ActionRemoteReboot() override;

#ifdef _DEBUG
               literal String ^BaseDirectory = SOCKET_MSD_BASE_DIRECTORY;
               literal String ^ClientSuffix = SOCKET_MSD_CLIENT_SUFIX;
               literal String ^FileExtenssion = SOCKET_MSD_CMD_FILE;
               literal String ^FileName = "0";
#endif
            internal:
               property bool EverythingRead{bool get(){ return everythingRead; } void set(bool value){ everythingRead = value; }}
            private:
               int serialNumber;
               FlashSocketIf *socket = NULL;
               String ^baseDirectory;
               bool DataLoaded;
               bool everythingRead;

               TYesNo LoadMethod();
               bool SaveMethod();
            };
         }
      }
   }
}