#include "Bat2NewDataRW.h"
using namespace System;
using namespace System::Collections::Generic;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface::Domain;
using namespace Bat2Library::Connection::Interface;
using namespace System::Runtime::InteropServices;
#include <assert.h>

#undef _MANAGED
#include "Config\Config.h"
#include "Config\ConfigDef.h"
#include "Display\DisplayConfigurationDef.h"
#include "Weighing\WeighingConfigurationDef.h"
#include "Menu\Menu.h"
#include "Calibration\CalibrationDef.h"
#include "Curve\CurveList.h"
#include "Curve\CorrectionList.h"
#include "Scheduler\WeighingPlanList.h"
#include "Message\GsmMessageDef.h"
#include "Rs485\Rs485Config.h"
#include "Modbus\ModbusConfigDef.h"
#include "Megavi\MegaviConfigDef.h"
#include "SmsGate\SmsGateConfigDef.h"
#include "Dacs\DacsConfigDef.h"
#include "CurvesPlan.h"
#include "Communication\CommunicationDef.h"


extern void toNative(Connection::Interface::Domain::Configuration ^config);
extern void toNativeVersion(VersionInfo ^d);
extern void toNativeDevice(DeviceInfo ^d);
extern void toNativeCountry(Connection::Interface::Domain::Country ^d);
extern void toNativeWeightUnits(WeightUnits ^d);
//extern void toNativePlatformCalibration(PlatformCalibration ^d);
extern void toNativeDisplayConfiguration(DisplayConfiguration ^d);
extern void toNativeWeighingConfiguration(WeighingConfiguration ^d);
extern void toNativeRs485Configuration(List<Domain::Rs485Options^> ^d);
extern void toNativeModbusConfiguration(ModbusOptions ^d);
extern void toNativeMegaviConfiguration(MegaviOptions ^d);
extern void toNativeDacsConfiguration(DacsOptions ^d);
extern void toNativeGsmMessage(GsmMessage ^d);
extern void toNativeDataPublication(DataPublication ^d);
extern void toNativeCellularData(CellularData ^d);
extern void toNativeEthernet(Ethernet ^d);

extern bool SaveList(IEnumerable<WeighingPlan^> ^plans);
extern bool SaveList(IEnumerable<WeighingConfiguration^> ^predefined);
extern bool SaveList(IEnumerable<Contact^> ^contacts);
//#if DEBUG
static void toNativeContext(Context ^d);
static void toNativeMenuContext(Domain::MenuContext ^d);
static void toNativeCalibrationContext(CalibrationContext ^d);
static void tomNativeWeighingContext(WeighingContext ^d);
//#endif
bool Bat2NewDataRW::toNativeGeneric(Object ^o)
{
	Type ^type = o->GetType();
	if (type == VersionInfo::typeid){
		VersionInfo ^data = dynamic_cast<VersionInfo^>(o);
		toNativeVersion(data);
	}
	else if (type == Connection::Interface::Domain::Configuration::typeid){
		Connection::Interface::Domain::Configuration ^data = dynamic_cast<Domain::Configuration^>(o);
		toNative(data);
	}
	else if (type == DeviceInfo::typeid){
		DeviceInfo ^data = dynamic_cast<DeviceInfo^>(o);
		toNativeDevice(data);
	}
	else if (type == Country::typeid){
		Country ^data = dynamic_cast<Country^>(o);
		toNativeCountry(data);
	}
	else if (type == WeightUnits::typeid){
		WeightUnits ^data = dynamic_cast<WeightUnits^>(o);
		toNativeWeightUnits(data);
	}
	//else if (type == PlatformCalibration::typeid){
	//	PlatformCalibration ^data = dynamic_cast<PlatformCalibration^>(o);
	//	toNativePlatformCalibration(data);
	//}
	else if (type == WeighingConfiguration::typeid){
		WeighingConfiguration ^data = dynamic_cast<WeighingConfiguration^>(o);
		toNativeWeighingConfiguration(data);
	}
	else if (type == DisplayConfiguration::typeid){
		DisplayConfiguration ^data = dynamic_cast<DisplayConfiguration^>(o);
		toNativeDisplayConfiguration(data);
	}
	else if (type == ModbusOptions::typeid){
		ModbusOptions ^data = dynamic_cast<ModbusOptions^>(o);
		toNativeModbusConfiguration(data);
	}
	else if (type == MegaviOptions::typeid){
		MegaviOptions ^data = dynamic_cast<MegaviOptions^>(o);
		toNativeMegaviConfiguration(data);
	}
	else if (type == DacsOptions::typeid){
		DacsOptions ^data = dynamic_cast<DacsOptions^>(o);
		toNativeDacsConfiguration(data);
	}
	else if (type == GsmMessage::typeid){
		GsmMessage ^data = dynamic_cast<GsmMessage^>(o);
		toNativeGsmMessage(data);
	}
	else if (type == DataPublication::typeid){
		DataPublication ^data = dynamic_cast<DataPublication^>(o);
		toNativeDataPublication(data);
	}
	else if (type == DataPublication::typeid){
		DataPublication ^data = dynamic_cast<DataPublication^>(o);
		toNativeDataPublication(data);
	}
	else if (type == CellularData::typeid){
		CellularData ^data = dynamic_cast<CellularData^>(o);
		toNativeCellularData(data);
	}
	else if (type == Ethernet::typeid){
		Ethernet ^data = dynamic_cast<Ethernet^>(o);
		toNativeEthernet(data);
	}
	else if (type == List<WeighingPlan^>::typeid){
		IEnumerable<WeighingPlan^> ^data = dynamic_cast<IEnumerable<WeighingPlan^>^>(o);
		return SaveList(data);
	}
	else if (type == List<WeighingConfiguration^>::typeid){
		IEnumerable<WeighingConfiguration^> ^data = dynamic_cast<IEnumerable<WeighingConfiguration^>^>(o);
		return SaveList(data);
	}
	else if (type == List<Contact^>::typeid){
		List<Contact^> ^data = dynamic_cast<List<Contact^>^>(o);
		return SaveList(data);
	}
	else if (type == List<Domain::Rs485Options^>::typeid){
		List<Domain::Rs485Options^> ^data = dynamic_cast<List<Domain::Rs485Options^>^>(o);
		toNativeRs485Configuration(data);
	}
//#if DEBUG
	else if (type == Context::typeid){
		Context ^data = dynamic_cast<Context^>(o);
		toNativeContext(data);
	}
	else if (type == MenuContext::typeid){
		MenuContext ^data = dynamic_cast<MenuContext^>(o);
		toNativeMenuContext(data);
	}
	else if (type == CalibrationContext::typeid){
		CalibrationContext ^data = dynamic_cast<CalibrationContext^>(o);
		toNativeCalibrationContext(data);
	}
	else if (type == WeighingContext::typeid){
		WeighingContext ^data = dynamic_cast<WeighingContext^>(o);
		tomNativeWeighingContext(data);
	}
//#endif
	else {
		return false;
	}
	return true;
}

//#if DEBUG

static void toNativeContext(Context ^d)
{
	if (d == nullptr){
		throw gcnew ArgumentNullException("d", "Context can't be null!");
	}
	toNativeMenuContext(d->MenuContext);
	toNativeCalibrationContext(d->CalibrationContext);
	tomNativeWeighingContext(d->WeighingContext);
	
}

static void toNativeMenuContext(Domain::MenuContext ^d)
{
	if (d == nullptr){
		throw gcnew ArgumentNullException("d", "MenuContext can't be null!");
	}
	const UStorageDescriptor *storage = &ContextDescriptor;
	assert(sizeof(TMenuContext) == storage->Items[CONTEXT_MENU].Size);
	TMenuContext *menuContext = ((TMenuContext*)storage->Items[CONTEXT_MENU].Data);

	menuContext->SwitchedOff = d->SwitchedOff ? YES : NO;
}

static void toNativeCalibrationContext(CalibrationContext ^d)
{
	if (d == nullptr){
		throw gcnew ArgumentNullException("d", "MenuContext can't be null!");
	}
	const UStorageDescriptor *storage = &ContextDescriptor;
	assert(sizeof(TCalibration) == storage->Items[CONTEXT_CALIBRATION_COEFFICIENTS].Size);
	TCalibration *calibration = ((TCalibration*)storage->Items[CONTEXT_CALIBRATION_COEFFICIENTS].Data);

	calibration->Inversion = d->Inversion ? YES : NO;

	UDateTime dt;
	dt.Date.Year = d->Time.Year - 2000;
	dt.Date.Month = d->Time.Month;
	dt.Date.Day = d->Time.Day;
	dt.Time.Hour = d->Time.Hour;
	dt.Time.Min = d->Time.Minute;
	dt.Time.Sec = d->Time.Second;
	calibration->Time = uDateTimeGauge(&dt);

	List<int>^ weightPoints = gcnew List<int>(d->WeightPoints);
	List<int>^ rawtPoints = gcnew List<int>(d->RawPoints);
	calibration->Points = weightPoints->Count > CALIBRATION_POINTS_MAX ? CALIBRATION_POINTS_MAX : weightPoints->Count;
	for (int i = 0; i < calibration->Points; i++)
	{
		calibration->Weight[i] = weightPoints[i];
		calibration->Raw[i] = rawtPoints[i];
	}
}

static void tomNativeWeighingContext(WeighingContext ^d)
{
	if (d == nullptr){
		throw gcnew ArgumentNullException("d", "WeighingContext can't be null!");
	}
	const UStorageDescriptor *storage = &ContextDescriptor;
	assert(sizeof(TWeighingContext) == storage->Items[CONTEXT_WEIGHING].Size);
	TWeighingContext *weighingContext = ((TWeighingContext*)storage->Items[CONTEXT_WEIGHING].Data);

	weighingContext->Status = (byte)d->Status;
	weighingContext->LastStatus = (byte)d->LastStatus;
	weighingContext->DayActive = d->DayActive;
	weighingContext->Id = d->Id;
	weighingContext->Day = d->Day;
	weighingContext->TargetWeight = d->TargetWeight;
	weighingContext->TargetWeightFemale = d->TargetWeightFemale;
	weighingContext->Correction = d->Correction;

	UDateTime dt;
	dt.Time.Hour = d->StartAt.Hour;
	dt.Time.Min = d->StartAt.Minute;
	dt.Time.Sec = d->StartAt.Second;
	dt.Date.Year = d->StartAt.Year;
	dt.Date.Month = d->StartAt.Month;
	dt.Date.Day = d->StartAt.Day;
	UClockGauge gauge = uClockGauge(&dt);
	weighingContext->StartAt = uClockGauge(&dt);

	
	dt.Time.Hour = d->DayZero.Hour;
	dt.Time.Min = d->DayZero.Minute;
	dt.Time.Sec = d->DayZero.Second;
	dt.Date.Year = d->DayZero.Year;
	dt.Date.Month = d->DayZero.Month;
	dt.Date.Day = d->DayZero.Day;
	gauge = uClockGauge(&dt);
	weighingContext->DayZero = uClockGauge(&dt);

	
	dt.Time.Hour = d->DayCloseAt.Hour;
	dt.Time.Min = d->DayCloseAt.Minute;
	dt.Time.Sec = d->DayCloseAt.Second;
	dt.Date.Year = d->DayCloseAt.Year;
	dt.Date.Month = d->DayCloseAt.Month;
	dt.Date.Day = d->DayCloseAt.Day;
	gauge = uClockGauge(&dt);
	weighingContext->DayCloseAt = uClockGauge(&dt);

	UTime ut;
	ut.Hour = d->DayDuration.Hours;
	ut.Min = d->DayDuration.Minutes;
	ut.Sec = d->DayDuration.Seconds;
	weighingContext->DayDuration = uTimeGauge(&ut);

	toNativeCorrectionCurve(&weighingContext->CorrectionCurve, d->CorrectionCurve);
	toNativeGrowthCurve(&weighingContext->GrowthCurveMale, d->GrowthCurveMale);
	toNativeGrowthCurve(&weighingContext->GrowthCurveFemale,d->GrowthCurveFemale);
	toNativePlan(&weighingContext->WeighingPlan, d->WeighingPlan);
}
//#endif