#pragma once

#include "INativeSocket.h"
#undef _MANAGED
#include "Time\uClock.h"
#include "Time\uDateTime.h"

using namespace System;

namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace ::Native;
            private ref class Bat2INativeSocket abstract : INativeSocket, IDisposable
            {
            private:
               Lock ^lock;
            public:
               Bat2INativeSocket()
               {
                  lock = nullptr;
               }
               ~Bat2INativeSocket()
               {
                  FreeConnection();
               }
               virtual bool SetupConnection()
               {
                  if (lock == nullptr){
                     lock = gcnew Lock(this);
                  }
                  return true;
               }

               virtual bool FreeConnection()
               {
                  if (lock != nullptr)
                  {
                     try{
                        delete(lock);
                     }
                     catch (Exception ^){}
                     lock = nullptr;
                  }
                  return true;
               }

               virtual TYesNo ConfigRemoteLoad() abstract;
               virtual TYesNo ConfigRemoteSave() abstract;
               virtual TYesNo ArchiveRemoteLoad() abstract;

               virtual TYesNo ActionRemoteWeighingSchedulerStart(void) abstract;
               virtual TYesNo ActionRemoteWeighingSchedulerStartAt(UClockGauge DateTime) abstract;
               virtual TYesNo ActionRemoteWeighingSchedulerStop(void) abstract;
               virtual TYesNo ActionRemoteWeighingSchedulerSuspend(void) abstract;
               virtual TYesNo ActionRemoteWeighingSchedulerRelease(void) abstract;
               virtual TYesNo ActionRemoteTimeSet(UDateTimeGauge DateTime) abstract;
               virtual TYesNo ActionRemoteTimeGet(UDateTimeGauge *DateTime) abstract;
               virtual TYesNo ActionRemoteReboot() abstract;
            };
         }
      }
   }
}