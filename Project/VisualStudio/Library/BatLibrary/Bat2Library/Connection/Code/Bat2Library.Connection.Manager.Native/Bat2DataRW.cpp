#include "Bat2DataRW.h"

using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface::IO;
using namespace Bat2Library::Connection::Interface::Domain;
using namespace Native;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Reflection;
using namespace System::Diagnostics;

generic<typename R, typename S, typename U, typename V>
Bat2DataRW<R, S, U, V>::Bat2DataRW(Connection::Manager::Native::Bat2Device<R, S, U, V> ^device)
{
   this->device = device;
   Debug::Assert(device != nullptr);
}

generic<typename R, typename S, typename U, typename V>
Bat2DataRW<R, S, U, V>::~Bat2DataRW()
{
   this->!Bat2DataRW();
}

generic<typename R, typename S, typename U, typename V>
Bat2DataRW<R, S, U, V>::!Bat2DataRW()
{
   this->device = nullptr;
}

generic<typename R, typename S, typename U, typename V>
generic <class T> where T: ref class, gcnew()
T Bat2DataRW<R, S, U, V>::DoLoadExecute(LoadFuncRef<T> ^func, PropertyInfo ^setProperty)
{
	T c;
	try {
		c = FindAndGetProperty<T>(setProperty);
		if (c == nullptr){
			c = gcnew T();
		}
		if (!func(c))
		{
			c = T(); // is like default(T)
		}
		else {
			//find and set property on devicedata
			FindAndSetProperty(c, setProperty);
		}
	}
	catch (Exception ^ex)
	{
		c = T();
	}
	return c;
}

generic<typename R, typename S, typename U, typename V>
generic <class T> where T: ref class, gcnew()
T Bat2DataRW<R, S, U, V>::DoLoad(LoadFuncRef<T> ^func, PropertyInfo ^setProperty)
{
	Lock lock(dataLock);
	T c;
	try{
		if (device->Socket->SetupConnection())
		{
			c = DoLoadExecute(func, setProperty);
		}
	}
	catch (Exception ^ex)
	{
		c = T();
	}
	device->Socket->FreeConnection();
	return c;
}

generic<typename R, typename S, typename U, typename V>
generic <class T> where T : ref class
bool Bat2DataRW<R, S, U, V>::DoSave(SaveFuncRef<T> ^SaveAction, T param, PropertyInfo ^setProperty)
{
	Lock lock(dataLock);
	bool res = false;
	try{
		if (device->Socket->SetupConnection() &&
			SaveAction(param))
		{
			FindAndSetProperty(param, setProperty);
			res = true;
		}
	}
	catch (Exception ^ex){}
	device->Socket->FreeConnection();
	return res;
}

generic<typename R, typename S, typename U, typename V>
bool Bat2DataRW<R, S, U, V>::DoAction(ActionCmdE action, Object ^param, [Out] Object^%outParam)
{
	Lock lock(dataLock);
	bool res = false;
	try{
		if (device->Socket->SetupConnection())
		{
			res = Action(action, param, outParam);
		}
	}
	catch (Exception ^ex){}
	device->Socket->FreeConnection();
	return res;
}

generic<typename R, typename S, typename U, typename V>
generic <class T> where T : ref class
void Bat2DataRW<R, S, U, V>::FindAndSetProperty(T value, PropertyInfo ^setProperty)
{
	Debug::Assert(setProperty != nullptr);
	setProperty->SetMethod->Invoke(device->DeviceData, gcnew array<Object ^>{value});
}

generic<typename R, typename S, typename U, typename V>
generic <class T> where T : ref class
T Bat2DataRW<R, S, U, V>::FindAndGetProperty(PropertyInfo ^prop)
{
	Debug::Assert(prop != nullptr);
	return safe_cast<T>(prop->GetMethod->Invoke(device->DeviceData, nullptr));
}

//------------------------------------------------------------------------------
//  Actions
//------------------------------------------------------------------------------
generic<typename R, typename S, typename U, typename V>
bool Bat2DataRW<R, S, U, V>::WeighingStart()
{
   Object ^o;
   return DoAction(ActionCmdE::ACTION_WEIGHING_SCHEDULER_START, nullptr, o);
}

generic<typename R, typename S, typename U, typename V>
bool Bat2DataRW<R, S, U, V>::WeighingStart(DateTime at)
{
   Object ^o;
   return DoAction(ActionCmdE::ACTION_WEIGHING_SCHEDULER_START_AT, Nullable<DateTime>(at), o);
}

generic<typename R, typename S, typename U, typename V>
bool Bat2DataRW<R, S, U, V>::WeighingStop()
{
   Object ^o;
   return DoAction(ActionCmdE::ACTION_WEIGHING_SCHEDULER_STOP, nullptr, o);
}

generic<typename R, typename S, typename U, typename V>
bool Bat2DataRW<R, S, U, V>::WeighingSuspend()
{
   Object ^o;
   return DoAction(ActionCmdE::ACTION_WEIGHING_SCHEDULER_SUSPEND, nullptr, o);
}

generic<typename R, typename S, typename U, typename V>
bool Bat2DataRW<R, S, U, V>::WeighingRelease()
{
   Object ^o;
   return DoAction(ActionCmdE::ACTION_WEIGHING_SCHEDULER_RELEASE, nullptr, o);
}

generic<typename R, typename S, typename U, typename V>
bool Bat2DataRW<R, S, U, V>::TimeSet(DateTime at)
{
   Object ^o;
   return DoAction(ActionCmdE::ACTION_TIME_SET, Nullable<DateTime>(at), o);
}

generic<typename R, typename S, typename U, typename V>
bool Bat2DataRW<R, S, U, V>::TimeGet(Nullable<DateTime> %time)
{
   Object ^o;
   if (DoAction(ActionCmdE::ACTION_TIME_GET, nullptr, o)){
      time = Nullable<DateTime>((DateTime)o);
      return true;
   }
   time = Nullable<DateTime>();
   return false;
}