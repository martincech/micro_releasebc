#include "Bat2NewDataRW.h"
using namespace System;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface;
using namespace Bat2Library::Connection::Interface::Domain;
#include <assert.h>

#undef _MANAGED
#include "CurvesPlan.h"
#include "Predefined\PredefinedList.h"
#include "Data\uDirectory.h"
#include "Data\uNamedList.h"

bool SaveGrowthList(IEnumerable<Curve^> ^curves);
bool SaveCorrectionList(IEnumerable<Curve^> ^curves);
bool SaveList(IEnumerable<WeighingPlan^> ^plans);
bool SaveList(IEnumerable<WeighingConfiguration^> ^plans);

bool Bat2NewDataRW::SaveGrowth(IEnumerable<Curve^> ^curves)
{
	if (!SaveGrowthList(curves))
	{
		return false;
	}
   return device->Socket->CurveListRemoteSave() == YES;
}

bool SaveGrowthList(IEnumerable<Curve^> ^curves)
{
	TCurveList list;
	TCurveIndex index;
	TGrowthCurve item;

	CurveListInit();
	if (CurveListOpen(&list) == NO){
		return false;
	}
	for each (Curve^ curve in curves)
	{
		toNativeGrowthCurve(&item, curve);
		index = uNamedListAdd(&list, item.Name, 0);
		CurveListSave(&list, &item, index);
	}
	CurveListClose(&list);
	return true;
}

bool Bat2NewDataRW::SaveCorrection(IEnumerable<Curve^> ^curves)
{
	if (!SaveCorrectionList(curves))
	{
		return false;
	}
   return device->Socket->CorrectionListRemoteSave() == YES;
}

bool SaveCorrectionList(IEnumerable<Curve^> ^curves)
{
	TCorrectionList list;
	TCorrectionIndex index;
	TCorrectionCurve item;

	CorrectionListInit();
	if (CorrectionListOpen(&list) == NO){
		return false;
	}

	for each (Curve^ curve in curves)
	{
		toNativeCorrectionCurve(&item, curve);
		index = uNamedListAdd(&list, item.Name, 0);
		CorrectionListSave(&list, &item, index);
	}
	CorrectionListClose(&list);
	return true;
}

bool Bat2NewDataRW::Save(IEnumerable<WeighingPlan^> ^plans)
{
	if (!SaveList(plans))
	{
		return false;
	}
	return device->Socket->WeighingPlanListRemoteSave() == YES;
}

bool SaveList(IEnumerable<WeighingPlan^> ^plans)
{
	TWeighingPlanList list;
	TWeighingPlan item;

	WeighingPlanListInit();
	if (WeighingPlanListOpen(&list) == NO){
		return false;
	}
	for each (WeighingPlan^ plan in plans)
	{
		toNativePlan(&item, plan);
		WeighingPlanListAdd(&list, &item);
	}
	WeighingPlanListClose(&list);
	return true;
}

bool Bat2NewDataRW::Save(IEnumerable<WeighingConfiguration^> ^predefinedWeighings)
{
	if (!SaveList(predefinedWeighings))
	{
		return false;
	}
   return device->Socket->PredefinedListRemoteSave() == YES;
}

bool SaveList(IEnumerable<WeighingConfiguration^> ^predefinedWeighings)
{
	TPredefinedList list;
	TPredefinedWeighingIndex index;
	TPredefinedWeighing item;

	PredefinedListInit();
	if (PredefinedListOpen(&list) == NO){
		return false;
	}
	for each (WeighingConfiguration^ plan in predefinedWeighings)
	{
		toNativeWeighingConfiguration(&item, plan);
		index = uNamedListAdd(&list, item.Name, 0);
		PredefinedListSave(&list, &item, index);
	}
	PredefinedListClose(&list);
	return true;
}