#pragma once
#include "UsbBat2Device.h"
#include "Bat2NewDeviceManager.h"
#include "Remote\SocketIfMsdDef.h"
namespace Bat2Library
{
   namespace Connection
   {
      namespace Manager
      {
         namespace Native{
            using namespace Usb;
            using namespace Usb::UMS;
            using namespace System::Collections::Generic;

            public ref class FlashBat2NewDeviceManager : Bat2NewDeviceManager
            {
            public:
               static property FlashBat2NewDeviceManager ^Instance{
                  FlashBat2NewDeviceManager ^get(){
                     Lock l(FlashBat2NewDeviceManager::typeid);
                     if (_instance == nullptr){
                        _instance = gcnew FlashBat2NewDeviceManager();
                     }
                     return _instance;
                  }
               }

            internal:
               static property String ^BaseDirectoryName{ String ^get(){ return baseDirectoryName; } void set(String ^value){ baseDirectoryName = value; } }
      
            private:
               FlashBat2NewDeviceManager();
               static FlashBat2NewDeviceManager ^_instance = nullptr;
               static String ^baseDirectoryName = SOCKET_MSD_BASE_DIRECTORY;
               Dictionary<UmsDevice ^, IEnumerable<Bat2NewDevice^> ^> ^existingDevices;

               UsbDeviceManager ^usbWatcher;
               void OnDeviceConnected(System::Object ^sender, UsbDevice ^e);
               void OnDeviceDisconnected(System::Object ^sender, UsbDevice ^e);
            };
         }
      }
   }
}

