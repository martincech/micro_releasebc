#include "Bat2NewDataRW.h"
using namespace System;
using namespace System::Collections::Generic;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Connection::Interface::Domain;
#include <assert.h>

#undef _MANAGED
#include "Message\ContactList.h"

bool SaveList(IEnumerable<Contact^> ^contacts);

bool Bat2NewDataRW::Save(IEnumerable<Contact^> ^contacts)
{
	if (!SaveList(contacts))
	{
		return false;
	}

   return device->Socket->ContactListRemoteSave() == YES;
}

bool SaveList(IEnumerable<Contact^> ^contacts)
{
	TContactList list;
	TGsmContact item;

	ContactListInit();
	if (ContactListOpen(&list) == NO){
		return false;
	}

	for each (Contact^contact in contacts)
	{
		if (String::IsNullOrEmpty(contact->PhoneNumber)){
			throw gcnew ArgumentOutOfRangeException("contact", "Contact Phone number have to be non-zero length string!");
		}

		if (String::IsNullOrEmpty(contact->Name)){
			throw gcnew ArgumentOutOfRangeException("contact", "Contact Name have to be non-zero length string!");
		}

		IntPtr str = Marshal::StringToHGlobalAnsi(contact->Name);
		strncpy(item.Name, (char*)str.ToPointer(), GSM_CONTACT_NAME_SIZE);
		item.Name[GSM_CONTACT_NAME_SIZE] = '\0';
		Marshal::FreeHGlobal(str);
		str = Marshal::StringToHGlobalAnsi(contact->PhoneNumber);
		strncpy(item.PhoneNumber, (char*)str.ToPointer(), GSM_PHONE_NUMBER_SIZE);
		item.Name[GSM_PHONE_NUMBER_SIZE] = '\0';
		Marshal::FreeHGlobal(str);

		item.SmsFormat = (byte)contact->SmsFormat;
		item.Statistics = contact->Statistics ? YES : NO;
		item.Commands = contact->Commands ? YES : NO;
		item.Events = contact->Events ? YES : NO;
		//item.SmsDataTransfer = contact->RemoteControl ? YES : NO;
		item.SendHistogram = contact->SendHistogram ? YES : NO;
		ContactListAdd(&list, &item);
	}
	ContactListClose(&list);
	return true;
}