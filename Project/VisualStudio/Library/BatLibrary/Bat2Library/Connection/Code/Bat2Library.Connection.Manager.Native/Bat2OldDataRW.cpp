#include "Bat2OldDataRW.h"

using namespace System::IO;
using namespace Bat2Library::Connection::Interface::Domain::Old;
using namespace Bat2Library::Connection::Manager::Native;
using namespace Bat2Library::Bat2Old::Map;
using namespace Bat2Library::Bat2Old::Constants;
using namespace Bat2Library::Bat2Old;
using namespace System::Runtime::InteropServices;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Reflection;
using namespace System::Diagnostics;
using namespace Utilities;


//------------------------------------------------------------------------------
//  Load data
//------------------------------------------------------------------------------


OldBat2DeviceData ^Bat2OldDataRW::LoadAll()
{
	Lock lock(dataLock);
	try{
		if (!device->Socket->SetupConnection())
		{
			return nullptr;
		}
	}
	catch (Exception ^ex)
	{
		return nullptr;
	}

	if (LoadConfig(false) == nullptr){
		return nullptr;
	}
	
	if (LoadArchive(false) == nullptr){
		return nullptr;
	}

	device->Socket->FreeConnection();

   return device->DeviceData;
}

OldConfiguration ^Bat2OldDataRW::LoadConfig(bool withSetup)
{
   if (withSetup){
      return DoLoad(gcnew LoadFuncRef<OldConfiguration ^>(this, &Bat2OldDataRW::Load), OldBat2DeviceData::typeid->GetProperty("Configuration"));
   }
   return DoLoadExecute(gcnew LoadFuncRef<OldConfiguration ^>(this, &Bat2OldDataRW::Load), OldBat2DeviceData::typeid->GetProperty("Configuration"));
}

IEnumerable<OldArchiveDailyInfo^> ^Bat2OldDataRW::LoadArchive(bool withSetup)
{
   if (withSetup){
      return DoLoad(gcnew LoadFuncRef<List<OldArchiveDailyInfo^> ^>(this, &Bat2OldDataRW::Load), OldBat2DeviceData::typeid->GetProperty("Archive"));
   }
   return DoLoadExecute(gcnew LoadFuncRef<List<OldArchiveDailyInfo^> ^>(this, &Bat2OldDataRW::Load), OldBat2DeviceData::typeid->GetProperty("Archive"));
}

bool Bat2OldDataRW::Load(OldConfiguration ^%config)
{
   if (!device->Socket->ConfigRemoteLoad())
   {
      return false;
   }

   config = Bat2Conversion::LoadConfigSection(device->Socket->Bat2Memory);
   return config != nullptr;
}

bool Bat2OldDataRW::Load(List<OldArchiveDailyInfo^> ^%archive)
{
   if (!device->Socket->ArchiveRemoteLoad())
   {
      return false;
   }

   archive = gcnew List<OldArchiveDailyInfo^>(Bat2Conversion::LoadArchive(device->Socket->Bat2Memory));
   return archive != nullptr;
}

//------------------------------------------------------------------------------
//  Save data
//------------------------------------------------------------------------------

bool Bat2OldDataRW::SaveConfig(OldConfiguration ^config)
{
	if (config == nullptr){
		throw gcnew ArgumentNullException("config", "Configuration can't be null!");
	}
	return DoSave(gcnew SaveFuncRef<OldConfiguration ^>(this, &Bat2OldDataRW::Save), config, OldBat2DeviceData::typeid->GetProperty("Configuration"));
}

bool Bat2OldDataRW::Save(OldConfiguration ^config)
{
   device->Socket->Bat2Memory = Bat2Conversion::SaveConfigSection(config);
   return device->Socket->ConfigRemoteSave() == YES;   
}
