using System;
using System.Collections.Generic;
using System.Linq;
using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;

namespace Connection.Manager.Tests.Bat2New.Sockets.BaseClasses
{
   public abstract class Bat2DataWriteTests
   {
      protected abstract void SaveMethod_ReturnFalse_WhenNotConnected<T>(Func<T, IBat2NewDataWriter, bool> saveAction)
         where T : class;

      protected abstract void SaveMethod_ReturnTrue_WhenConnected<T>(Func<T, IBat2NewDataWriter, bool> saveAction)
         where T : class;

      #region Config

      [TestMethod]
      public void Config_Save_ReturnFalse_WhenNotConnected()
      {
         SaveMethod_ReturnFalse_WhenNotConnected<Configuration>(
            (data, writer) =>
            {
               data.Rs485Options.RemoveRange(2, data.Rs485Options.Count - 2);
               return writer.SaveConfig(data);
            });
      }

      [TestMethod]
      public void Config_Save_ReturnTrue_WhenConnected()
      {
         SaveMethod_ReturnTrue_WhenConnected<Configuration>(
            (data, writer) =>
            {
               data.Rs485Options.RemoveRange(2, data.Rs485Options.Count - 2);
               return writer.SaveConfig(data);
            });
      }

      #endregion

      #region Growth curves

      [TestMethod]
      public void GrowthCurves_Save_ReturnFalse_WhenNotConnected()
      {
         SaveMethod_ReturnFalse_WhenNotConnected<IEnumerable<Curve>>(
            (data, writer) => writer.SaveGrowthCurves(data));
      }

      [TestMethod]
      public void GrowthCurves_Save_ReturnTrue_WhenConnected()
      {
         SaveMethod_ReturnTrue_WhenConnected<IEnumerable<Curve>>((data, writer) => writer.SaveGrowthCurves(data));
      }

      #endregion

      #region Correction curves

      [TestMethod]
      public void CorrectionCurves_Save_ReturnFalse_WhenNotConnected()
      {
         SaveMethod_ReturnFalse_WhenNotConnected<IEnumerable<Curve>>(
            (data, writer) => writer.SaveCorrectionCurves(data));
      }

      [TestMethod]
      public void CorrectionCurves_Save_ReturnTrue_WhenConnected()
      {
         SaveMethod_ReturnTrue_WhenConnected<IEnumerable<Curve>>((data, writer) => writer.SaveCorrectionCurves(data));
      }

      #endregion

      #region Weighing plan

      [TestMethod]
      public void WeighingPlan_Save_ReturnFalse_WhenNotConnected()
      {
         SaveMethod_ReturnFalse_WhenNotConnected<IEnumerable<WeighingPlan>>(
            (data, writer) => writer.SaveWeighingPlans(data));
      }

      [TestMethod]
      public void WeighingPlan_Save_ReturnTrue_WhenConnected()
      {
         SaveMethod_ReturnTrue_WhenConnected<IEnumerable<WeighingPlan>>((data, writer) => writer.SaveWeighingPlans(data));
      }

      #endregion

      #region Contact List

      [TestMethod]
      public void ContactList_Save_ReturnFalse_WhenNotConnected()
      {
         SaveMethod_ReturnFalse_WhenNotConnected<IEnumerable<Contact>>(
            (data, writer) => writer.SaveContactList(data));
      }

      [TestMethod]
      public void ContactList_Save_ReturnTrue_WhenConnected()
      {
         SaveMethod_ReturnTrue_WhenConnected<IEnumerable<Contact>>((data, writer) => writer.SaveContactList(data));
      }

      #endregion

      #region Predefined weighings

      [TestMethod]
      public void PredefinedWeighings_Save_ReturnFalse_WhenNotConnected()
      {
         SaveMethod_ReturnFalse_WhenNotConnected<IEnumerable<WeighingConfiguration>>(
            (data, writer) => writer.SavePredefinedWeighings(data));
      }

      [TestMethod]
      public void PredefinedWeighings_Save_ReturnTrue_WhenConnected()
      {
         SaveMethod_ReturnTrue_WhenConnected<IEnumerable<WeighingConfiguration>>(
            (data, writer) => writer.SavePredefinedWeighings(data));
      }

      #endregion

      #region Save methods

      internal static void SaveMethod_ReturnFalse_WhenNotConnected<T>(Func<T, IBat2NewDataWriter, bool> saveAction,
         Bat2NewDevice b2D) where T : class
      {
         using (var rw = new Bat2NewDataRW(b2D))
         {
            var gen = new Fixture();
            gen.CustomizeData();
            var data = gen.Create<T>();
            Assert.IsFalse(saveAction(data, rw));
         }
      }

      internal static void SaveMethod_ReturnTrue_WhenConnected<T>(Func<T, IBat2NewDataWriter, bool> saveAction,
         Bat2NewDevice b2D) where T : class
      {
         using (var rw = new Bat2NewDataRW(b2D))
         {
            var allData = rw.LoadAll();
            Assert.IsNotNull(allData);
            T dPropValue;
            var dProp =
               typeof (Bat2DeviceData).GetProperties().FirstOrDefault(prop =>
                  prop.PropertyType == typeof (T) || prop.PropertyType.IsAssignableFrom(typeof (T)));
            if (typeof (T) == typeof (Bat2DeviceData))
            {
               dPropValue = allData as T;
            }
            else
            {
               Assert.IsNotNull(dProp);
               dPropValue = dProp.GetMethod.Invoke(allData, null) as T;
            }
            Assert.IsNotNull(dPropValue);
            Assert.IsTrue(saveAction(dPropValue, rw));

            T newDpropValue;
            if (typeof (T) == typeof (Bat2DeviceData))
            {
               newDpropValue = allData as T;
            }
            else
            {
               Assert.IsNotNull(dProp);
               newDpropValue = dProp.GetMethod.Invoke(b2D.DeviceData, null) as T;
            }
            Assert.AreSame(newDpropValue, dPropValue, "Property on inner data not set to valid value!");
         }
      }

      #endregion
   }
}