﻿using System.IO;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Usb.FTDI;

namespace Connection.Manager.Tests.Bat2Old.Sockets.UsbSocket
{
   [TestClass]
   public class Bat2OldUsbSocketTests
   {
      private Bat2OldUsbSocket socket;
      private const string FILE_NAME = "tmpFile.txt";


      [TestInitialize]
      public void Init()
      {
         socket = null;
      }

      [TestCleanup]
      public void Clean()
      {
         if (socket != null)
         {
            socket.Dispose();
         }

         if (File.Exists(FILE_NAME))
         {
            File.Delete(FILE_NAME);
         }
      }

      public FTDIDevice FtdiDevice
      {
         get
         {
            var manager = new FTDIDeviceLoader();
            var device = manager.GetDevice(Bat2Library.Bat2Old.Constants.Bat2Ftdi.USB_DEVICE_NAME);
            return device;
         }
      }

      [TestMethod]
      public void FtdiDevice_IsOk()
      {
         Assert.IsNotNull(FtdiDevice);
      }

      [TestMethod]
      public void Socket_Created_ByStream()
      {
         Assert.IsNotNull(FtdiDevice);
         socket = new Bat2OldUsbSocket(FtdiDevice.Open());
         Assert.IsNotNull(socket);
      }

      [TestMethod]
      public void Socket_Created_ByName()
      {
         socket = new Bat2OldUsbSocket(Bat2Library.Bat2Old.Constants.Bat2Ftdi.USB_DEVICE_NAME);
         Assert.IsNotNull(socket);
      }

      [TestMethod]
      public void Socket_DataSended()
      {
         using (var file = new FileStream(FILE_NAME, FileMode.Create, FileAccess.ReadWrite))
         {
            socket = new Bat2OldUsbSocket(file);
            file.Seek(0, SeekOrigin.Begin);
            var data = new byte[] {0, 0};
            socket.Socket.Write(data, 0, data.Length);

            file.Seek(0, SeekOrigin.Begin);
            var readBuf = new byte[200];
            var rLen = file.Read(readBuf, 0, readBuf.Length);
            Assert.IsFalse(rLen == 0);
            Assert.IsTrue(rLen == data.Length);
         }
      }

      [TestMethod]
      public void Socket_SendAndReaded_IsDataValid()
      {
         using (var file = new FileStream(FILE_NAME, FileMode.Create, FileAccess.ReadWrite))
         {
            socket = new Bat2OldUsbSocket(file);
            file.Seek(0, SeekOrigin.Begin);
            var data = new byte[] {1, 2, 3, 4, 5};
            socket.Socket.Write(data, 0, data.Length);
            file.Flush();

            file.Seek(0, SeekOrigin.Begin);
            var rcv = new byte[data.Length];
            Assert.AreEqual(socket.Socket.Read(rcv, 0, data.Length), data.Length);
            Assert.AreEqual(rcv.Length, data.Length);
            for (var i = 0; i < data.Length; i++)
            {
               Assert.AreEqual(data[i], rcv[i]);
            }
         }
      }
   }
}