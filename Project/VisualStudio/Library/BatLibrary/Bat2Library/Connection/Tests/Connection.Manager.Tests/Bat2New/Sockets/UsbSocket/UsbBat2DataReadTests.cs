﻿using System;
using System.IO;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Connection.Manager.Tests.Bat2New.Sockets.BaseClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2New.Sockets.UsbSocket
{
   [TestClass]
   public class UsbBat2DataReadTests : Bat2DataReadTests
   {
      private const string FILE_NAME = "tmpFile.txt";

      protected override void LoadMethod_ReturnNull_WhenNotConnected<T>(Func<IBat2NewDataReader, T> loadAction)
      {
         using (var file = new FileStream(FILE_NAME, FileMode.Create, FileAccess.ReadWrite))
         {
            using (var socket = new Bat2UsbSocket(file))
            {
               using (var b2D = new UsbBat2Device(socket))
               {
                  LoadMethod_ReturnNull_WhenNotConnected(loadAction, b2D);
               }
            }
         }
      }

      protected override void LoadMethod_ReturnNonNull_WhenConnected<T>(Func<IBat2NewDataReader, T> loadAction)
      {
         using (var socket = new Bat2UsbSocket(Bat2UsbSocketTests.VID, Bat2UsbSocketTests.PID))
         {
            using (var b2D = new UsbBat2Device(socket))
            {
               LoadMethod_ReturnNonNull_WhenConnected(loadAction, b2D);
            }
         }
      }
   }
}