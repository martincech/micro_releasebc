using System;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2Old.Sockets.UsbSocket
{
   [TestClass]
   public class UsbBat2RealDataReadTests : UsbBat2FakeDataReadTests
   {
      protected override void LoadMethod_ReturnNonNull_WhenConnected<T>(Func<IBat2OldDataReader, T> loadAction)
      {
         //real device
         using (var socket = new Bat2OldUsbSocket(Bat2Library.Bat2Old.Constants.Bat2Ftdi.USB_DEVICE_NAME))
         {
            using (var b2D = new UsbBat2OldDevice(socket))
            {
               LoadMethod_ReturnNonNull_WhenConnected(loadAction, b2D);
            }
         }
      }
   }
}