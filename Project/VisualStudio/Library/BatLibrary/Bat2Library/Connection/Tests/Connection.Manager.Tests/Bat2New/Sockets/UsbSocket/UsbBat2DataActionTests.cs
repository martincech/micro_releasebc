﻿using System;
using System.IO;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Connection.Manager.Tests.Bat2New.Sockets.BaseClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2New.Sockets.UsbSocket
{
   [TestClass]
   public class UsbBat2DataActionTests : Bat2DataActionTests
   {
      private const string FILE_NAME = "tmpFile.txt";

      internal override void ActionMethod_ReturnTrue_WhenConnected(Func<IBat2ActionCommander, bool> action)
      {
         using (var socket = new Bat2UsbSocket(Bat2UsbSocketTests.VID, Bat2UsbSocketTests.PID))
         {
            using (var b2D = new UsbBat2Device(socket))
            {
               ActionMethod_ReturnTrue_WhenConnected(action, b2D);
            }
         }
      }

      internal override void ActionMethod_ReturnFalse_WhenNotConnected(Func<IBat2ActionCommander, bool> action)
      {
         using (var file = new FileStream(FILE_NAME, FileMode.Create, FileAccess.ReadWrite))
         {
            using (var socket = new Bat2UsbSocket(file))
            {
               using (var b2D = new UsbBat2Device(socket))
               {
                  ActionMethod_ReturnFalse_WhenNotConnected(action, b2D);
               }
            }
         }
      }
   }
}