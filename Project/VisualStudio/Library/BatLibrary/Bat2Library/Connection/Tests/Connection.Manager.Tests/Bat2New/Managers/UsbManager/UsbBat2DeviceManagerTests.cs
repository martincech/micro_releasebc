﻿using Bat2Library.Connection.Interface.Domain;
using Bat2Library.Connection.Interface.IO;
using Bat2Library.Connection.Manager.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Connection.Manager.Tests.Bat2New.Managers.UsbManager
{
   /// <summary>
   /// For this tests any BAT2 must be connected to some USB port
   /// </summary>
   [TestClass]
   public class UsbBat2DeviceManagerTests :
      BaseIBat2DeviceManagerTests<
         VersionInfo,
         Configuration,
         Bat2DeviceData,
         IBat2NewDataReader,
         IBat2NewDataWriter,
         IBat2ActionCommander>
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      public UsbBat2DeviceManagerTests()
         : base(UsbBat2NewDeviceManager.Instance)
      {
      }
   }
}