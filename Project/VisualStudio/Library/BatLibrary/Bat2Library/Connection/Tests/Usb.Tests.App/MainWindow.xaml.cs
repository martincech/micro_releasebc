﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using Bat2Library.Connection.Manager.Native;
using Desktop.Wpf.Applications;
using Usb.HID;
using Usb.Test.App.Properties;

namespace Usb.Test.App
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : INotifyPropertyChanged
   {
      private HidDeviceLoader loader;
      private ObservableCollection<HidDevice> hidDevices;
      private RelayCommand findDevicesCommand;
      private bool findDevicesCommandCanExecute;
      private HidDevice selectedDevice;
      private RelayCommand<bool> writeFeatureCommand;
      private bool writeFeatureCanExecute;
      private RelayCommand writeDataCommand;
      private bool writeDataCommandCanExecute;
      private RelayCommand readArchiveCommand;
      private bool readArchiveCommandCanExecute;
      private int timeConstant;
      private RelayCommand readFeatureCommand;
      private bool readFeatureCanExecute;
      private const int MY_VID = 0xA600;
      private const int MY_PID = 0xE2A0;

      public MainWindow()
      {
         HidDevices = new ObservableCollection<HidDevice>();
         WriteDataCommandData = new ObservableCollection<byte>();
         ChangeCommandExecution();
         PropertyChanged += OnPropertyChanged;
         TimeConstant = 100;
         InitializeComponent();
      }

      private void Window_Loaded(object sender, RoutedEventArgs e)
      {
         Debug.Assert(FindDevicesCommand.CanExecute());
         FindDevicesCommand.Execute();
      }

      public RelayCommand FindDevicesCommand
      {
         get
         {
            return findDevicesCommand ?? (
               findDevicesCommand = new RelayCommand(
                  () =>
                  {
                     DenyCommands();
                     Status = "Finding devices";
                     loader = new HidDeviceLoader();
                     HidDevices =
                        new ObservableCollection<HidDevice>(
                           loader.GetDevices().Where(dev => dev.VendorID == MY_VID && dev.ProductID == MY_PID));
                     Status = string.Format("{0} devices found.", HidDevices.Count);
                     SelectedDevice = null;
                     AllowCommands();
                  },
                  () => findDevicesCommandCanExecute
                  ));
         }
      }

      public RelayCommand<bool> WriteFeatureCommand
      {
         get
         {
            return writeFeatureCommand ?? (writeFeatureCommand = new RelayCommand<bool>(
               one =>
               {
                  DenyCommands();
                  var stream = SelectedDevice.Open();
                  var value = (byte) (one ? 1 : 0);
                  stream.SetFeature(new[] {value});
                  AllowCommands();
               },
               one => writeFeatureCanExecute));
         }
      }


      public RelayCommand ReadFeatureCommand
      {
         get { return readFeatureCommand ?? (readFeatureCommand = new RelayCommand( () =>
               {
                  DenyCommands();
                  var stream = SelectedDevice.Open();
                  var buf = new byte[4];
                  stream.GetFeature(buf);
                  Status = ByteArrayToString(buf);
                  AllowCommands();
               },
               () => readFeatureCanExecute));
         }
      }

      private static string ByteArrayToString(byte[] ba)
      {
         var hex = new StringBuilder(ba.Length * 2);
         foreach (var b in ba)
         {
            hex.AppendFormat("{0:x2}", b);
         }
         return hex.ToString();
      }
      public RelayCommand WriteDataCommand
      {
         get
         {
            return writeDataCommand ?? (writeDataCommand = new RelayCommand(
               data =>
               {
                  DenyCommands();
                  var stream = SelectedDevice.Open();
                  stream.Write(WriteDataCommandData.ToArray());
                  AllowCommands();
               },
               data => writeDataCommandCanExecute && !string.IsNullOrEmpty(writeData.Text)
               ));
         }
      }


      public RelayCommand ReadArchiveCommand
      {
         get
         {
            return readArchiveCommand ?? (
               readArchiveCommand = new RelayCommand(
                  () =>
                  {
                     DenyCommands();
                     //TODO:Need to fix
                     using (var dataReader = UsbBat2NewDeviceManager.Instance.GetDataReader(null))
                     {
                        var archive = dataReader.LoadArchive();
                        if (archive != null)
                        {
                           Status = string.Format("Archive loaded successfully, items: {0}", archive.Count());
                        }
                        else
                        {
                           Status = "Archive not loaded!";
                        }
                     }
                     AllowCommands();
                  },
                  () => readArchiveCommandCanExecute)
               );
         }
      }

      private void DenyCommands()
      {
         ChangeCommandExecution(false, false, false, false, false);
      }

      private void AllowCommands()
      {
         ChangeCommandExecution(true, true, true, true, true);
      }


      private string Status
      {
         get { return statusbar.Content as string; }
         set { statusbar.Content = value; }
      }


      public ObservableCollection<HidDevice> HidDevices
      {
         get { return hidDevices; }
         set
         {
            if (Equals(value, hidDevices)) return;
            hidDevices = value;
            OnPropertyChanged();
         }
      }

      public HidDevice SelectedDevice
      {
         get { return selectedDevice; }
         set
         {
            if (Equals(value, selectedDevice)) return;
            selectedDevice = value;
            OnPropertyChanged();
         }
      }

      public int TimeConstant
      {
         get { return timeConstant; }
         set
         {
            if (value == timeConstant) return;
            timeConstant = value;
            OnPropertyChanged();
         }
      }

      private ObservableCollection<byte> WriteDataCommandData { get; set; }

      public event PropertyChangedEventHandler PropertyChanged;

      [NotifyPropertyChangedInvocator]
      protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
      {
         var handler = PropertyChanged;
         if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
      }

      private void ChangeCommandExecution(bool findDevicesCommandEnabled = true, bool writeFeatureEnabled = false,
         bool writeDataEnabled = false, bool readArchiveEnabled = false, bool readFeatureEnabled = false)
      {
         findDevicesCommandCanExecute = findDevicesCommandEnabled;
         var enabled = SelectedDevice != null;
         writeFeatureCanExecute = writeFeatureEnabled && enabled;
         writeDataCommandCanExecute = writeDataEnabled && enabled;
         readArchiveCommandCanExecute = readArchiveEnabled && enabled;
         readFeatureCanExecute = readFeatureEnabled && enabled;


         FindDevicesCommand.RaiseCanExecuteChanged();
         WriteFeatureCommand.RaiseCanExecuteChanged();
         WriteDataCommand.RaiseCanExecuteChanged();
         ReadArchiveCommand.RaiseCanExecuteChanged();
         ReadFeatureCommand.RaiseCanExecuteChanged();
      }

      private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
      {
         if (propertyChangedEventArgs.PropertyName == "SelectedDevice")
         {
            AllowCommands();
         }
         if (propertyChangedEventArgs.PropertyName == "TimeConstant")
         {
            //TODO:Need to by fixed
            //UsbBat2NewDeviceManager.SetTimeConstant(TimeConstant);
         }
      }

      private void writeData_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
      {
         WriteDataCommandData.Clear();
         var charArray = writeData.Text.ToCharArray();
         foreach (var ch in charArray)
         {
            WriteDataCommandData.Add(Convert.ToByte(ch));
         }

         WriteDataCommand.RaiseCanExecuteChanged();
      }
   }
}