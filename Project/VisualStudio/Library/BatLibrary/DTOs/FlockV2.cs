﻿namespace DTOs
{
   public class FlockV2
   {
      public int Id { get; set; }
      public string Name { get; set; }
      public bool UseCurves { get; set; }
      public bool UseGender { get; set; }
      public int? WeighFrom { get; set; }
      public int? WeighTo { get; set; }
      public int ConfigurationV2Id { get; set; }
      public int? CurveFemaleId { get; set; }
      public int? CurveMaleId { get; set; }
      public double? InitialFemale { get; set; }
      public double? InitialMale { get; set; }
      public short Index { get; set; }

      public Curve CurveFemale { get; set; }   
      public Curve CurveMale { get; set; }
   }
}
