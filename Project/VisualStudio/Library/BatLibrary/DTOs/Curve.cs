﻿using System.Collections.Generic;

namespace DTOs
{
   public class Curve
   {
      #region Constructors

      public Curve()
      {
         CurveValues = new List<CurveValue>();
      }

      #endregion

      public int Id { get; set; }
      public string Name { get; set; }        
      public ICollection<CurveValue> CurveValues { get; set; }
   }
}
