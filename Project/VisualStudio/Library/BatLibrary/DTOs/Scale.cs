﻿using System.Collections.Generic;

namespace DTOs
{
   public class Scale
   {
      #region Constructors

      public Scale()
      {
         ConfigurationsV2 = new List<ConfigurationV2>();
      }

      #endregion
      public int Id { get; set; }
      public string Name { get; set; }
      public int SerialNumber { get; set; }
      public string PhoneNumber { get; set; }
      public string FarmName { get; set; }

      public ICollection<ConfigurationV2> ConfigurationsV2 { get; set; }
   }
}
