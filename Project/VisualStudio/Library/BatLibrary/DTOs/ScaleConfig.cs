﻿namespace DTOs
{
   public class ScaleConfig
   {
      public short MaleMarginAbove { get; set; }
      public int MaleMarginBelow { get; set; }
      public short FemaleMarginAbove { get; set; }
      public short FemaleMarginBelow { get; set; }
      public short Filter { get; set; }
      public double Stabilization { get; set; }
      public short StabilizationTime { get; set; }
      public bool GainAutomaticMode { get; set; }
      public short SaveUnpon { get; set; }
      public short Units { get; set; }
   }
}
