//******************************************************************************
//
//   BatDevice.cpp    Bat device basics
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#include <windows.h>
#include <stdio.h>

#include "Unisys/Uni.h"
#include "BatDevice.h"

//******************************************************************************
// Constructor
//******************************************************************************

TBatDevice::TBatDevice()
// Constructor
{
   Adapter   = 0;
   WriteVerification = NO;
} // TBatDevice

//******************************************************************************
// Destructor
//******************************************************************************

TBatDevice::~TBatDevice()
// Destructor
{
} // ~TBatDevice

//******************************************************************************
// Check
//******************************************************************************

TYesNo TBatDevice::Check()
// Check for device
{
   int Version;
   return( GetVersion( Version));
} // Check

//******************************************************************************
// Version
//******************************************************************************

TYesNo TBatDevice::GetVersion( int &Version)
// Get bat version
{
   int Cmd  = USB_CMD_VERSION;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( NO);
   }
   Version = Data;
   return( YES);
} // GetVersion

//******************************************************************************
// Status
//******************************************************************************

TYesNo TBatDevice::GetStatus( TYesNo &PowerOn)
// Get power status
{
   int Cmd  = USB_CMD_GET_STATUS;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( NO);
   }
   PowerOn = Data;
   return( YES);
} // GetStatus

//******************************************************************************
// Power off
//******************************************************************************

TYesNo TBatDevice::PowerOff()
// Switch power off
{
   int Cmd  = USB_CMD_POWER_OFF;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( NO);
   }
   return( YES);
} // PowerOff

//******************************************************************************
// Reload Configuration
//******************************************************************************

TYesNo TBatDevice::ReloadConfig()
// Reload configuration by EEPROM
{
   int Cmd  = USB_CMD_RELOAD;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( NO);
   }
   return( YES);
} // ReloadConfig

//******************************************************************************
// Get Time
//******************************************************************************

TYesNo TBatDevice::GetTime( TTimestamp &Timestamp)
// Get device time
{
   int Cmd  = USB_CMD_GET_TIME;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( NO);
   }
   Timestamp = Data;
   return( YES);
} // GetTime

//******************************************************************************
// Set Time
//******************************************************************************

TYesNo TBatDevice::SetTime( TTimestamp Timestamp)
// Set device time
{
   int Cmd  = USB_CMD_SET_TIME;
   int Data = Timestamp;
   if( !SendCommand( Cmd, Data)){
      return( NO);
   }
   return( YES);
} // SetTime

//******************************************************************************
// Read Memory
//******************************************************************************

TYesNo TBatDevice::ReadMemory( int Address, void *Buffer, int Size)
// Read EEPROM
{
   if( !Adapter){
      return( NO);
   }
   int Command = USB_CMD_ADDRESS;
   int Data    = Address;
   if( !SendCommand( Command, Data)){
      return( NO);
   }
   // Split into packets :
   int Length = Size > USB_MAX_DATA ? USB_MAX_DATA : Size;
   int TotalLength = Size;
   int RLength;
   int Offset      = 0;
   do {
      Data    = Length;
      Command = USB_CMD_READ;
      if( !SendCommand( Command, Data)){
         return( NO);
      }
      if( Command != TPktAdapter::DATA_MESSAGE){
         return( NO);
      }
      // check for length :
      Adapter->GetData( RLength, 0);
      if( RLength != Length){
         return( NO);
      }
      // copy data :
      Adapter->GetData( RLength, &((char *)Buffer)[ Offset]);
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_MAX_DATA){
         Length = USB_MAX_DATA;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( NO);
   }
   if( Data != (Address + Size)){
      return( NO);
   }
   return( YES);
} // ReadMemory

//******************************************************************************
// Write Memory
//******************************************************************************

TYesNo TBatDevice::WriteMemory( int Address, void *Buffer, int Size)
// Write EEPROM data
{
   if( !Adapter){
      return( NO);
   }
   // set address counter before write :
   int Command = USB_CMD_ADDRESS;
   int Data    = Address;
   if( !SendCommand( Command, Data)){
      return( NO);
   }
   // Split into packets :
   int Length = Size > USB_WRITE_SIZE ? USB_WRITE_SIZE : Size;
   int TotalLength = Size;
   int Offset      = 0;
   do {
      if( !WriteFragment( USB_CMD_WRITE, &((char *)Buffer)[ Offset], Length)){
         return( NO);
      }
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_WRITE_SIZE){
         Length = USB_WRITE_SIZE;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( NO);
   }
   if( Data != (Address + Size)){
      return( NO);
   }
   //----- verification :
   if( !WriteVerification){
      return( YES);          // without verification
   }
   byte *TmpBuffer  = new byte[ Size];
   if( !ReadMemory( Address, TmpBuffer, Size)){
      delete[] TmpBuffer;
      return( NO);
   }
   if( !memequ( Buffer, TmpBuffer, Size)){
      delete[] TmpBuffer;
      return( NO);        // different contents
   }
   delete[] TmpBuffer;
   return( YES);
} // WriteMemory

//******************************************************************************
// Directory Begin
//******************************************************************************

TYesNo TBatDevice::DirectoryBegin( int &Count)
// Read directory begin. Returns <Count> of directory items
{
   if( !Adapter){
      return( NO);
   }
   int Cmd  = USB_CMD_DIRECTORY_BEGIN;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( NO);
   }
   Count = Data;
   return( YES);
} // DirectoryBegin

//******************************************************************************
// Directory Next
//******************************************************************************

TYesNo TBatDevice::DirectoryNext( TUsbDirectoryItem *Item)
// Read directory entry. Fills <Item>
{
   if( !Adapter){
      return( NO);
   }
   int Command = USB_CMD_DIRECTORY_NEXT;
   int Data    = 0;
   if( !SendCommand( Command, Data)){
      return( NO);
   }
   if( Command != TPktAdapter::DATA_MESSAGE){
      return( NO);
   }
   // check for length :
   int RLength;
   Adapter->GetData( RLength, 0);
   if( RLength != sizeof( TUsbDirectoryItem)){
      return( NO);
   }
   // copy data :
   Adapter->GetData( RLength, Item);
   return( YES);
} // DirectoryNext

//******************************************************************************
// File Open
//******************************************************************************

TYesNo TBatDevice::FileOpen( int Handle)
// Open file by <Handle>
{
   if( !Adapter){
      return( NO);
   }
   int Cmd  = USB_CMD_FILE_OPEN;
   int Data = Handle;
   if( !SendCommand( Cmd, Data)){
      return( NO);
   }
   return( YES);
} // FileOpen

//******************************************************************************
// File Close
//******************************************************************************

TYesNo TBatDevice::FileClose()
// Close Current file
{
   if( !Adapter){
      return( NO);
   }
   int Cmd  = USB_CMD_FILE_CLOSE;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( NO);
   }
   return( YES);
} // FileClose

//******************************************************************************
// File Create
//******************************************************************************

TYesNo TBatDevice::FileCreate( char *Name, char *Note, int Class, int &Handle)
// Create file by <Name>, <Note>, <Class>. Returns <Handle>
{
   int RCommand, RData;
   TUsbCmdUnion  CmdUnion;
   // send create data :
   CmdUnion.Create.Cmd   = USB_CMD_FILE_CREATE;
   CmdUnion.Create.Class = Class;
   strcpy( CmdUnion.Create.Name, Name);
   strcpy( CmdUnion.Create.Note, Note);
   if( !Adapter->SendData( &CmdUnion, sizeof( TUsbCmdFileCreate))){
      return( NO);
   }
   if( !Adapter->Receive( RCommand, RData)){
      return( NO);
   }
   if( !(RCommand & USB_CMD_REPLY)){
      return( NO);
   }
   if( (RCommand & ~USB_CMD_REPLY) != USB_CMD_FILE_CREATE){
      return( NO);
   }
   Handle = RData;
   return( YES);
} // FileCreate

//******************************************************************************
// File Read
//******************************************************************************

TYesNo TBatDevice::FileRead( void *Buffer, int Size)
// Read file
{
   if( !Adapter){
      return( NO);
   }
   // clear address counter before read :
   int Command = USB_CMD_ADDRESS;
   int Data    = 0;
   if( !SendCommand( Command, Data)){
      return( NO);
   }
   // Split into packets :
   int Length = Size > USB_MAX_DATA ? USB_MAX_DATA : Size;
   int TotalLength = Size;
   int RLength;
   int Offset      = 0;
   do {
      Data    = Length;
      Command = USB_CMD_FILE_READ;
      if( !SendCommand( Command, Data)){
         return( NO);
      }
      if( Command != TPktAdapter::DATA_MESSAGE){
         return( NO);
      }
      // check for length :
      Adapter->GetData( RLength, 0);
      if( RLength != Length){
         return( NO);
      }
      // copy data :
      Adapter->GetData( RLength, &((char *)Buffer)[ Offset]);
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_MAX_DATA){
         Length = USB_MAX_DATA;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( NO);
   }
   if( Data != Size){
      return( NO);
   }
   return( YES);
} // FileRead

//******************************************************************************
// File Write
//******************************************************************************

TYesNo TBatDevice::FileWrite( void *Buffer, int Size)
// Write file
{
   if( !Adapter){
      return( NO);
   }
   // clear address counter before write :
   int Command = USB_CMD_ADDRESS;
   int Data    = 0;
   if( !SendCommand( Command, Data)){
      return( NO);
   }
   // Split into packets :
   int Length = Size > USB_WRITE_SIZE ? USB_WRITE_SIZE : Size;
   int TotalLength = Size;
   int Offset      = 0;
   do {
      if( !WriteFragment( USB_CMD_FILE_WRITE, &((char *)Buffer)[ Offset], Length)){
         return( NO);
      }
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_WRITE_SIZE){
         Length = USB_WRITE_SIZE;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( NO);
   }
   if( Data != Size){
      return( NO);
   }
   return( YES);
} // FileWrite

//******************************************************************************
// Format
//******************************************************************************

TYesNo TBatDevice::FormatFilesystem()
// Format filesystem
{
   int Cmd  = USB_CMD_FORMAT;
   int Data = USB_FORMAT_MAGIC;
   if( !SendCommand( Cmd, Data)){
      return( NO);
   }
   return( YES);
} // FormatFilesystem

//------------------------------------------------------------------------------

//******************************************************************************
// Send command
//******************************************************************************

TYesNo TBatDevice::SendCommand( int &Command, int &Data)
// Send command with error recovery
{
   if( !Adapter){
      return( NO);
   }
   int RCommand, RData;
   if( !Adapter->Send( Command, Data)){
      return( NO);
   }
   if( !Adapter->Receive( RCommand, RData)){
      return( NO);
   }
   if( RCommand != TPktAdapter::DATA_MESSAGE &&
      (RCommand & ~USB_CMD_REPLY) != Command){
      return( NO);
   }
   Data    = RData;
   Command = RCommand;
   return( YES);
} // SendCommand

//******************************************************************************
// Write Fragment
//******************************************************************************

TYesNo TBatDevice::WriteFragment( int WriteCommand, void *Buffer, int Size)
// Write fragment of data with recovery
{
   int RCommand, RData;
   TUsbCmdUnion  CmdUnion;
   // send data :
   CmdUnion.Write.Cmd = WriteCommand;
   memcpy( CmdUnion.Write.Data, Buffer, Size);
   if( !Adapter->SendData( &CmdUnion, Size + USB_CMD_SIZE)){
      return( NO);
   }
   if( !Adapter->Receive( RCommand, RData)){
      return( NO);
   }
   if( !(RCommand & USB_CMD_REPLY)){
      return( NO);
   }
   if( (RCommand & ~USB_CMD_REPLY) != WriteCommand){
      return( NO);
   }
   return( YES);
} // WriteFragment
