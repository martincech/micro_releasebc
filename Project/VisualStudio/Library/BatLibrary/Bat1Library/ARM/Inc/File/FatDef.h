//******************************************************************************
//
//   FatDef.h     File allocation table definitions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __FatDef_H__
   #define __FatDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#define FAT_INVALID   0                // invalid FAT index
#define FAT_EOF       0xFFFE           // end of chain marker
#define FAT_FREE      0xFFFF           // empty item marker

typedef word TFatIndex;                // FAT index (public)
typedef word TFatItem;                 // FAT item (internal)

#define FatGetBlock( FatIndex) ((FatIndex) - 1)      // FAT index -> block index
#define FatByIndex( Index)     ((Index) + 1)         // block index -> FAT index

#endif
