//******************************************************************************
//
//   ConsoleLogger.h  Raw data viewer
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef ConsoleLoggerH
   #define ConsoleLoggerH

#ifndef LoggerH
   #include "Logger.h"
#endif

//******************************************************************************
// Class TConsoleLogger
//******************************************************************************

class TConsoleLogger : public TLogger
{
public:
   // Display mode :
   typedef enum {
      BINARY, // binary dump
      TEXT,   // text dump
      MIXED,  // binary and text dump 
   } TDisplayMode;

   TConsoleLogger();
   // Constructor
   virtual void Write( TDataType Type, void *Buffer, int Length);
   // Write data
   virtual void Report( char *Format, ...);
   // Write report
   TDisplayMode Mode;
   // Display mode

//------------------------------------------------------------------------------
protected :
   void BinaryWrite( char *Buffer, int Length);
   // Binary dump
   void TextWrite( char *Buffer, int Length);
   // Text dump
   void MixedWrite( char *Buffer, int Length);
   // Mixed dump
   void TextLineWrite( char *Line);
   // Write text line
   void MixedLineWrite( char *Line, int LineLength);
   // Write mixed line
}; // TConsoleLogger


extern TConsoleLogger *Logger;    // global pointer

#endif
