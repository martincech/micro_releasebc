//******************************************************************************
//
//   Logger.h     Raw data logger template
//   Version 1.0  (c) Vymos
//
//******************************************************************************

#ifndef LoggerH
   #define LoggerH

//******************************************************************************
// Class TLogger
//******************************************************************************

class TLogger
{
public :
   // Data types :
   typedef enum {
      UNKNOWN,        // unknown data
      RX_DATA,        // received data
      TX_DATA,        // sended data
      RX_GARBAGE,     // received garbage
   } TDataType;

   virtual void Write( TDataType Type, void *Buffer, int Length) = 0;
   // Write data
   virtual void Report( char *Format, ...) = 0;
   // Write report

}; // TLogger

#endif

