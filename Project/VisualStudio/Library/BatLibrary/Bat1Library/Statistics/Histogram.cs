﻿//******************************************************************************
//
//   Statistics.cs      Statistic calculations with same algorithms as in the scale
//   Version 1.0
//
//******************************************************************************

namespace Bat1Library.Statistics
{
   /// <summary>
   /// Histogram data
   /// </summary>
   public class Histogram
   {
      /// <summary>
      /// Number of slots in histogram
      /// </summary>
      public readonly int SlotsCount;

      /// <summary>
      /// Array of values
      /// </summary>
      public float[] Values;

      /// <summary>
      /// Array of counts
      /// </summary>
      public int[] Counts;

      /// <summary>
      /// Histogram step
      /// </summary>
      public float Step;

      public Histogram(int slotsCount)
      {
         SlotsCount = slotsCount;
         Values = new float[slotsCount];
         Counts = new int[slotsCount];
         Step = 0;
      }
   }
}