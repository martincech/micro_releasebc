using System.Runtime.InteropServices;

// ReSharper disable InconsistentNaming

namespace Bat1Library
{
   //=============================================================================
   // DLL locales interface
   //=============================================================================
   public class Localesxx
   {
      public static string GetCountryNamexx(int CountryCode)
         // Get country name
      {
         switch ((Country) CountryCode)
         {
            case Country.INTERNATIONAL:
               return ("International");
            case Country.ALBANIA:
               return ("Albania");
            case Country.ALGERIA:
               return ("Algeria");
            case Country.ARGENTINA:
               return ("Argentina");
            case Country.AUSTRALIA:
               return ("Australia");
            case Country.AUSTRIA:
               return ("Austria");
            case Country.BANGLADESH:
               return ("Bangladesh");
            case Country.BELARUS:
               return ("Belarus");
            case Country.BELGIUM:
               return ("Belgium");
            case Country.BOLIVIA:
               return ("Bolivia");
            case Country.BRAZIL:
               return ("Brazil");
            case Country.BULGARIA:
               return ("Bulgaria");
            case Country.CANADA:
               return ("Canada");
            case Country.CHILE:
               return ("Chile");
            case Country.CHINA:
               return ("China");
            case Country.COLOMBIA:
               return ("Colombia");
            case Country.CYPRUS:
               return ("Cyprus");
            case Country.CZECH:
               return ("Czech Republic");
            case Country.DENMARK:
               return ("Denmark");
            case Country.ECUADOR:
               return ("Ecuador");
            case Country.EGYPT:
               return ("Egypt");
            case Country.ESTONIA:
               return ("Estonia");
            case Country.FINLAND:
               return ("Finland");
            case Country.FRANCE:
               return ("France");
            case Country.GERMANY:
               return ("Germany");
            case Country.GREECE:
               return ("Greece");
            case Country.HUNGARY:
               return ("Hungary");
            case Country.INDIA:
               return ("India");
            case Country.INDONESIA:
               return ("Indonesia");
            case Country.IRAN:
               return ("Iran");
            case Country.IRELAND:
               return ("Ireland");
            case Country.ISRAEL:
               return ("Israel");
            case Country.ITALY:
               return ("Italy");
            case Country.JAPAN:
               return ("Japan");
            case Country.JORDAN:
               return ("Jordan");
            case Country.LATVIA:
               return ("Latvia");
            case Country.LEBANON:
               return ("Lebanon");
            case Country.LITHUANIA:
               return ("Lithuania");
            case Country.LUXEMBOURG:
               return ("Luxembourg");
            case Country.MALAYSIA:
               return ("Malaysia");
            case Country.MALTA:
               return ("Malta");
            case Country.MEXICO:
               return ("Mexico");
            case Country.MONGOLIA:
               return ("Mongolia");
            case Country.MOROCCO:
               return ("Morocco");
            case Country.NEPAL:
               return ("Nepal");
            case Country.NETHERLANDS:
               return ("Netherlands");
            case Country.NEW_ZEALAND:
               return ("New Zealand");
            case Country.NIGERIA:
               return ("Nigeria");
            case Country.NORWAY:
               return ("Norway");
            case Country.PAKISTAN:
               return ("Pakistan");
            case Country.PARAGUAY:
               return ("Paraguay");
            case Country.PERU:
               return ("Peru");
            case Country.PHILIPPINES:
               return ("Philippines");
            case Country.POLAND:
               return ("Poland");
            case Country.PORTUGAL:
               return ("Portugal");
            case Country.ROMANIA:
               return ("Romania");
            case Country.RUSSIA:
               return ("Russia");
            case Country.SLOVAKIA:
               return ("Slovakia");
            case Country.SLOVENIA:
               return ("Slovenia");
            case Country.SOUTH_AFRICA:
               return ("South Africa");
            case Country.SOUTH_KOREA:
               return ("South Korea");
            case Country.SPAIN:
               return ("Spain");
            case Country.SWEDEN:
               return ("Sweden");
            case Country.SWITZERLAND:
               return ("Switzerland");
            case Country.SYRIA:
               return ("Syria");
            case Country.THAILAND:
               return ("Thailand");
            case Country.TUNISIA:
               return ("Tunisia");
            case Country.TURKEY:
               return ("Turkey");
            case Country.UKRAINE:
               return ("Ukraine");
            case Country.UK:
               return ("United Kingdom");
            case Country.USA:
               return ("United States");
            case Country.URUGUAY:
               return ("Uruguay");
            case Country.VENEZUELA:
               return ("Venezuela");
            case Country.VIETNAM:
               return ("Vietnam");
            default:
               return ("International");
         }
      } // GetCountryName

      [DllImport("Bat1Dll.dll", EntryPoint = "GetLocaleLanguage")]
      public static extern int GetLanguage(int Country);

      // Get language

      [DllImport("Bat1Dll.dll", EntryPoint = "GetLocaleCodePage")]
      public static extern int GetCodePage(int Language);

      // Get code page

      [DllImport("Bat1Dll.dll", EntryPoint = "GetLocaleDateFormat")]
      public static extern int GetDateFormat(int Country);

      // Get date format

      [DllImport("Bat1Dll.dll", EntryPoint = "GetLocaleDateSeparator1")]
      public static extern char GetDateSeparator1(int Country);

      // Get first date separator

      [DllImport("Bat1Dll.dll", EntryPoint = "GetLocaleDateSeparator2")]
      public static extern char GetDateSeparator2(int Country);

      // Get second date separator

      [DllImport("Bat1Dll.dll", EntryPoint = "GetLocaleTimeFormat")]
      public static extern int GetTimeFormat(int Country);

      // Get time format

      [DllImport("Bat1Dll.dll", EntryPoint = "GetLocaleTimeSeparator")]
      public static extern char GetTimeSeparator(int Country);

      // Get time separator

      [DllImport("Bat1Dll.dll", EntryPoint = "GetLocaleDaylightSavingType")]
      public static extern int GetDaylightSavingType(int Country);

      // Get daylight saving time type
   }
}