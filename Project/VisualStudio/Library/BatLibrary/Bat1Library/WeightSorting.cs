namespace Bat1Library
{
   public enum WeightSorting
   {
      NONE,
      LIGHT_HEAVY,
      LIGHT_OK_HEAVY,
   };
}