﻿namespace Bat1Library
{
   public class Logo
   {
      public const int WIDTH = 240;
      public const int HEIGHT = 160;
      public const int PLANES = 2;
      public const int ROWS = HEIGHT/8;
      public const int SIZE = WIDTH*ROWS;
   }
}