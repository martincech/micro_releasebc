//******************************************************************************
//
//   DllMain.cpp  Bat1 DLL main module
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <windows.h>

#include <string.h>
#include <math.h>
#include <stdio.h>

#define DLL_EXPORT
#include "Bat1Dll.h"

#include "Hardware.h"
#include "ARM/Bat1/Bat1.h"       // default constants
#include "Bat1.h"
#include "Library/Logger/ConsoleLogger.h"

//-----------------------------------------------------------------------------

#define BAT1DLL_VERSION 0x01020        // actual library version

// debug options :
//#define __DEBUG__
#ifdef __DEBUG__
   #define TRACE( msg)       printf( msg "\n")
   #define TRACE1( msg, p1)  printf( msg "\n", p1)
#else
   #define TRACE( msg)
   #define TRACE1( msg, p1)
#endif

// global devices
TPktAdapter    *Adapter;
TBat1          *Bat1;

int            _DstMode;

// Locales table :
#include "ARM/Bat1/CountryTable.c"

//******************************************************************************
//  Main
//******************************************************************************
#pragma unmanaged
DLL_IMPORT void APIENTRY Initialize()
{
   // Create devices :
   Adapter = new TPktAdapter;
   Bat1 = new TBat1;
   Bat1->Adapter = Adapter;
   Logger = new TConsoleLogger();
   _DstMode = DT_DST_TYPE_OFF;
}
#pragma unmanaged
DLL_IMPORT void APIENTRY DeInitialize()
{
   delete Logger;
   delete Adapter;
   delete Bat1;
}

//TYesNo APIENTRY DllMain( HANDLE hModule, DWORD dwReason, LPVOID lpReserved)
//{
//   // process initialisation :
//   if( dwReason == DLL_PROCESS_ATTACH){
//      TRACE( "DllMain : initialisation");
//      // Create devices :
//      Adapter       = new TPktAdapter;
//      Bat1          = new TBat1;
//      Bat1->Adapter = Adapter;
//      Logger        = new TConsoleLogger();
//      _DstMode      = DT_DST_TYPE_OFF;
//      return( 1);
//   }
//   // process shutdown :
//   if( dwReason == DLL_PROCESS_DETACH){
//      TRACE( "DllMain : terminate");
//      delete Logger;
//      delete Adapter;
//      delete Bat1;
//      return( 1);
//   }
//   // thread maipulations :
//   if( dwReason == DLL_THREAD_ATTACH){
//      TRACE( "DllMain : thread attach");
//      return( 1);
//   } 
//   if( dwReason == DLL_THREAD_DETACH){
//      TRACE( "DllMain : thread detach");
//      return( 1);
//   }
//   // unknown reason :
//   TRACE1( "DllMain : unknown reason %d", dwReason);
//   return( 1);
//} // DllMain

//******************************************************************************
//  Get Version
//******************************************************************************

unsigned APIENTRY GetDllVersion( void)
// Returns DLL version
{
   return( BAT1DLL_VERSION);
} // GetVersion

//******************************************************************************
//  USB less
//******************************************************************************

TYesNo APIENTRY IsUsbLess( void)
// Returns YES if library is without USB support
{
#ifdef BAT1_USBLESS
   return( YES);
#else
   return( NO);
#endif
} // IsUsbLess

//******************************************************************************
//  Enable Logger
//******************************************************************************

void APIENTRY EnableLogger( TYesNo Enable)
// Enable communication logger (at console)
{
   if( Enable){
      Adapter->Logger = Logger;
   } else {
      Adapter->Logger = 0;
   }
} // EnableLogger

//******************************************************************************
//  Decode Time
//******************************************************************************

void APIENTRY DecodeTime( int Timestamp, int *Day, int *Month, int *Year, int *Hour, int *Min, int *Sec)
// Decode <Timestamp> to items (<Mode> = daylight saving type)
{
   _DstMode = Bat1->Config.Config.Country.Locale.DstType;
   Bat1->DecodeTime( Timestamp, *Day, *Month, *Year, *Hour, *Min, *Sec);
} // DecodeTime

//******************************************************************************
//  Encode Time
//******************************************************************************

int APIENTRY EncodeTime( int Day, int Month, int Year, int Hour, int Min, int Sec)
// Returns timestamp by items (<Mode> = daylight saving type)
{
   _DstMode = Bat1->Config.Config.Country.Locale.DstType;
   return( Bat1->EncodeTime( Day, Month, Year, Hour, Min, Sec));
} // EncodeTime

//******************************************************************************
//  Decode Weight
//******************************************************************************

DLL_IMPORT double APIENTRY DecodeWeight( int Weight)
// Decode <Weight> to number
{
double Order;

   Order = pow( 10.0, Bat1->Config.Config.Units.Decimals);
   return( (double)Weight / Order);
} // DecodeWeight

//******************************************************************************
//  Encode Weight
//******************************************************************************

DLL_IMPORT int APIENTRY EncodeWeight( double Weight)
// Encode <Weight> to internal representation
{
double Order;

   Order = pow( 10.0, Bat1->Config.Config.Units.Decimals);
   return( (int)(Weight * Order));
} // EncodeWeight

//******************************************************************************
//  Locale Language
//******************************************************************************

DLL_IMPORT int APIENTRY GetLocaleLanguage( int Country)
// Get language
{
   if( Country >= _COUNTRY_COUNT){
      return( LNG_ENGLISH);
   }
   return( _LocaleIndex[ Country].Language);
} // GetLocaleLanguage

//******************************************************************************
//  Locale Code Page
//******************************************************************************

DLL_IMPORT int APIENTRY GetLocaleCodePage( int Language)
// Get code page
{
   if( Language >= _LNG_COUNT){
      return( CPG_LATIN);
   }
   for( int i = 0; i < _COUNTRY_COUNT; i++){
	  if( _LocaleIndex[ i].Language == Language){
         return( _Locales[ _LocaleIndex[ i].Locale].CodePage);
	  }
   }
   return( CPG_LATIN);
} // GetLocaleCodePage

//******************************************************************************
//  Locale Date Format
//******************************************************************************

DLL_IMPORT int APIENTRY GetLocaleDateFormat( int Country)
// Get date format
{
   if( Country >= _COUNTRY_COUNT){
      return( DATE_FORMAT_DDMMYYYY);
   }
   return( _Locales[ _LocaleIndex[ Country].Locale].DateFormat);
} // GetLocaleDateFormat

//******************************************************************************
//  Locale Date Separator 1
//******************************************************************************

DLL_IMPORT char APIENTRY GetLocaleDateSeparator1( int Country)
// Get first date separator
{
   if( Country >= _COUNTRY_COUNT){
      return( '/');
   }
   return( _Locales[ _LocaleIndex[ Country].Locale].DateSeparator1);
} // GetLocaleDateSeparator1

//******************************************************************************
//  Locale Date Separator 2
//******************************************************************************

DLL_IMPORT char APIENTRY GetLocaleDateSeparator2( int Country)
// Get second date separator
{
   if( Country >= _COUNTRY_COUNT){
      return( '/');
   }
   return( _Locales[ _LocaleIndex[ Country].Locale].DateSeparator2);
} // GetLocaleDateSeparator2

//******************************************************************************
//  Locale Time Format
//******************************************************************************

DLL_IMPORT int APIENTRY GetLocaleTimeFormat( int Country)
// Get time format
{
   if( Country >= _COUNTRY_COUNT){
      return( TIME_FORMAT_24);
   }
   return( _Locales[ _LocaleIndex[ Country].Locale].TimeFormat);
} // GetLocaleTimeFormat

//******************************************************************************
//  Locale Time Separator
//******************************************************************************

DLL_IMPORT char APIENTRY GetLocaleTimeSeparator( int Country)
// Get time separator
{
   if( Country >= _COUNTRY_COUNT){
      return( ':');
   }
   return( _Locales[ _LocaleIndex[ Country].Locale].TimeSeparator);
} // GetLocaleTimeSeparator

//******************************************************************************
//  Locale DST
//******************************************************************************

DLL_IMPORT int APIENTRY GetLocaleDaylightSavingType( int Country)
// Get daylight saving time type
{
   if( Country >= _COUNTRY_COUNT){
      return( DT_DST_TYPE_OFF);
   }
   return( _Locales[ _LocaleIndex[ Country].Locale].DstType);
} // GetLocaleDaylightSavingType

//******************************************************************************
//  Check device
//******************************************************************************

TYesNo APIENTRY CheckDevice( void)
// Returns YES if device present
{
   return( Bat1->Check());
} // CheckDevice

//******************************************************************************
//  Device ON
//******************************************************************************

TYesNo APIENTRY DeviceIsOn( TYesNo *PowerOn)
// Sets <PowerOn> YES on powered device
{
   return( Bat1->GetStatus( *PowerOn));
} // DeviceIsOn

//******************************************************************************
//  Device switch OFF
//******************************************************************************

TYesNo APIENTRY DevicePowerOff( void)
// Switch power off
{
   if( !Bat1->PowerOff()){
      return( NO);
   }
   Sleep( 3000);                       // wait for switch
   return( YES);
} // DevicePowerOff

//******************************************************************************
//  Get clock
//******************************************************************************

TYesNo APIENTRY GetTime( int *Clock)
// Get device clock
{
TTimestamp Ts;

   if( !Bat1->GetTime( Ts)){
      return( NO);
   }
   *Clock = (int)Ts;
   return( YES);
} // GetTime

//******************************************************************************
//  Set clock
//******************************************************************************

TYesNo APIENTRY SetTime( int Clock)
// Set device clock
{
   return( Bat1->SetTime( (TTimestamp)Clock));
} // SetTime

//******************************************************************************
//  New device
//******************************************************************************

void APIENTRY NewDevice( void)
// Create new empty device
{
   Bat1->NewDevice();
} // NewDevice

//******************************************************************************
//  Load configuration
//******************************************************************************

TYesNo APIENTRY LoadConfiguration( void)
// Load configuration data only from device
{
   return( Bat1->Load( NO));
} // LoadConfiguration

//******************************************************************************
//  Load device
//******************************************************************************

TYesNo APIENTRY LoadDevice( void)
// Load data from device
{
   return( Bat1->Load( YES));
} // LoadDevice

//******************************************************************************
//  Save device
//******************************************************************************

TYesNo APIENTRY SaveDevice( void)
// Save data to device
{
   return( Bat1->Save());
} // SaveDevice

//******************************************************************************
//  Reload configuration
//******************************************************************************

DLL_IMPORT TYesNo APIENTRY ReloadConfiguration( void)
// Reload configuration from EEPROM to device
{
   return( Bat1->ReloadConfig());
} // ReloadConfiguration

//******************************************************************************
//  Load Estimation
//******************************************************************************

TYesNo APIENTRY LoadEstimation( int *Promile)
// Load size estimation [%%]
{
   return( Bat1->LoadEstimation( *Promile));
} // LoadEstimation

//******************************************************************************
//  Save Estimation
//******************************************************************************

void APIENTRY SaveEstimation( int *Promile)
// Save size estimation [%%]
{
   Bat1->SaveEstimation( *Promile);
} // SaveEstimation

//******************************************************************************
//  Crash Info
//******************************************************************************

TYesNo APIENTRY LoadCrashInfo( void)
// Load device crash info
{
   return( Bat1->LoadCrashInfo());
} // LoadCrashInfo

//******************************************************************************
//  Read EEPROM
//******************************************************************************

TYesNo APIENTRY ReadEeprom( int Address, unsigned char *Buffer, int Size)
// Read data from device EEPROM
{
   return( Bat1->ReadLargeMemory( Address, Buffer, Size));
} // ReadEeprom

//******************************************************************************
//  Write EEPROM
//******************************************************************************

TYesNo APIENTRY WriteEeprom( int Address, unsigned char *Buffer, int Size)
// Write data to device EEPROM
{
   return( Bat1->WriteLargeMemory( Address, Buffer, Size));
} // WriteEeprom

//******************************************************************************
//  EEPROM size
//******************************************************************************

int APIENTRY GetEepromSize( void)
// Returns device EEPROM size
{
   return( EEP_SIZE);
} // GetEepromSize

//******************************************************************************
//  Device by EEPROM
//******************************************************************************

TYesNo APIENTRY DeviceByEeprom( unsigned char *Buffer)
// Setup device by EEPROM contents
{
   return( Bat1->DeviceByEeprom( Buffer));
} // DeviceByEeprom

//******************************************************************************
//  Logo size
//******************************************************************************

DLL_IMPORT int APIENTRY GetLogoSize( void)
// Returns logo size
{
   return( sizeof( TLogo));
} // GetLogoSize

//******************************************************************************
//  Logo address
//******************************************************************************

DLL_IMPORT int APIENTRY GetLogoAddress( void)
// Returns logo start address
{
   return( offsetof( TEeprom, Logo));
} // GetLogoAddress

//******************************************************************************
//  Version
//******************************************************************************

unsigned APIENTRY GetDeviceVersion( void)
// Get device version
{
   return( Bat1->Config.Config.Version);
} // GetDeviceVersion

void APIENTRY SetDeviceVersion( unsigned Version)
// Set device version
{
   Bat1->Config.Config.Version = (word)Version;
   Bat1->TouchConfig = YES;
} // SetDeviceVersion

//******************************************************************************
//  Build
//******************************************************************************

unsigned APIENTRY GetBuild( void)
// Get device build
{
   return( Bat1->Config.Config.Build);
} // GetBuild

void APIENTRY SetBuild( unsigned Build)
// Set device build
{
   Bat1->Config.Config.Build = (byte)Build;
   Bat1->TouchConfig = YES;
} // SetBuild

//******************************************************************************
//  HW version
//******************************************************************************

unsigned APIENTRY GetHwVersion( void)
// Get hardware version
{
   return( Bat1->Config.Config.HwVersion);
} // GetHwVersion

void APIENTRY SetHwVersion( unsigned Version)
// Set hardware version
{
   Bat1->Config.Config.HwVersion = (byte)Version;
   Bat1->TouchConfig = YES;
} // SetHwVersion

//******************************************************************************
//  Scale Name
//******************************************************************************

void APIENTRY GetScaleName( char *Name)
// Get scale name
{
   strcpy( Name, Bat1->Config.Config.ScaleName);
} // GetScaleName

void APIENTRY SetScaleName( char *Name)
// Set scale name
{
   strncpy( Bat1->Config.Config.ScaleName, Name, SCALE_NAME_LENGTH);
   Bat1->Config.Config.ScaleName[ SCALE_NAME_LENGTH] = '\0';
   Bat1->TouchConfig = YES;
} // SetScaleName

//******************************************************************************
//  Password
//******************************************************************************

void APIENTRY ClearPassword( void)
// Disable password
{
   memset( Bat1->Config.Config.Password, 0, PASSWORD_LENGTH);
   Bat1->TouchConfig = YES;
} // ClearPassword

TYesNo APIENTRY ValidPassword( void)
// Returns YES if password is in use
{
   return( Bat1->Config.Config.Password[ 0] != 0);
} // ValidPassword

void APIENTRY GetPassword( unsigned char *Password)
// Get password
{
   memcpy( Password, Bat1->Config.Config.Password, PASSWORD_LENGTH);
} // GetPassword

void APIENTRY SetPassword( unsigned char *Password)
// Set password
{
   memcpy( Bat1->Config.Config.Password, Password, PASSWORD_LENGTH);
   Bat1->TouchConfig = YES;
} // SetPassword

//******************************************************************************
//  Country
//******************************************************************************

int APIENTRY GetCountry( void)
// Get country
{
   return( Bat1->Config.Config.Country.Country);
} // GetCountry

void APIENTRY SetCountry( int Country)
// Set country
{
   Bat1->Config.Config.Country.Country = (byte)Country;
   Bat1->TouchConfig = YES;
} // SetCountry

//******************************************************************************
//  Language
//******************************************************************************

int APIENTRY GetLanguage( void)
// Get language
{
   return( Bat1->Config.Config.Country.Language);
} // GetLanguage

void APIENTRY SetLanguage( int Language)
// Set language
{
   Bat1->Config.Config.Country.Language = (byte)Language;
   Bat1->TouchConfig = YES;
} // SetLanguage

//******************************************************************************
//  Code Page
//******************************************************************************

DLL_IMPORT int APIENTRY GetCodePage( void)
// Get code page
{
   return( Bat1->Config.Config.Country.Locale.CodePage);
} // GetCodePage

DLL_IMPORT void APIENTRY SetCodePage( int CodePage)
// Set code page
{
   Bat1->Config.Config.Country.Locale.CodePage = (byte)CodePage;
   Bat1->TouchConfig = YES;
} // SetCodePage

//******************************************************************************
//  Date Format
//******************************************************************************

int APIENTRY GetDeviceDateFormat( void)
// Get date format
{
   return( Bat1->Config.Config.Country.Locale.DateFormat);
} // GetDeviceDateFormat

void APIENTRY SetDeviceDateFormat( int Format)
// Set date format
{
   Bat1->Config.Config.Country.Locale.DateFormat = (byte)Format;
   Bat1->TouchConfig = YES;
} // SetDeviceDateFormat

//******************************************************************************
//  Date Separator 1
//******************************************************************************

char APIENTRY GetDateSeparator1( void)
// Get first date separator
{
   return( Bat1->Config.Config.Country.Locale.DateSeparator1);
} // GetDateSeparator1

void APIENTRY SetDateSeparator1( char Separator)
// Set first date separator
{
   Bat1->Config.Config.Country.Locale.DateSeparator1 = Separator;
   Bat1->TouchConfig = YES;
} // SetDateSeparator1

//******************************************************************************
//  Date Separator 2
//******************************************************************************

char APIENTRY GetDateSeparator2( void)
// Get second date separator
{
   return( Bat1->Config.Config.Country.Locale.DateSeparator2);
} // GetDateSeparator2

void APIENTRY SetDateSeparator2( char Separator)
// Set second date separator
{
   Bat1->Config.Config.Country.Locale.DateSeparator2 = Separator;
   Bat1->TouchConfig = YES;
} // SetDateSeparator2

//******************************************************************************
//  Time format
//******************************************************************************

int APIENTRY GetDeviceTimeFormat( void)
// Get time format
{
   return( Bat1->Config.Config.Country.Locale.TimeFormat);
} // GetDeviceTimeFormat

void APIENTRY SetDeviceTimeFormat( int Format)
// Set time format
{
   Bat1->Config.Config.Country.Locale.TimeFormat = (byte)Format;
   Bat1->TouchConfig = YES;
} // SetDeviceTimeFormat

//******************************************************************************
//  Time Separator
//******************************************************************************

char APIENTRY GetTimeSeparator( void)
// Get time separator
{
   return( Bat1->Config.Config.Country.Locale.TimeSeparator);
} // GetTimeSeparator

void APIENTRY SetTimeSeparator( char Separator)
// Set time separator
{
   Bat1->Config.Config.Country.Locale.TimeSeparator = Separator;
   Bat1->TouchConfig = YES;
} // SetTimeSeparator

//******************************************************************************
//  DST type
//******************************************************************************

int APIENTRY GetDaylightSavingType( void)
// Get daylight saving time type
{
   return( Bat1->Config.Config.Country.Locale.DstType);
} // GetDaylightSavingType

void APIENTRY SetDaylightSavingType( int DstType)
// Set daylight saving time type
{
   Bat1->Config.Config.Country.Locale.DstType = (byte)DstType;
   Bat1->TouchConfig = YES;
} // SetDaylightSavingType

//******************************************************************************
//  Weighing units
//******************************************************************************

int APIENTRY GetWeighingUnits( void)
// Get weighing units
{
   return( Bat1->Config.Config.Units.Units);
} // GetWeighingUnits

void APIENTRY SetWeighingUnits( int Units)
// Set weighing units
{
   Bat1->Config.Config.Units.Units = Units;
   switch( Units){
      case UNITS_KG :
         Bat1->Config.Config.Units.Range       = UNITS_KG_RANGE;
         Bat1->Config.Config.Units.Decimals    = UNITS_KG_DECIMALS;
         Bat1->Config.Config.Units.MaxDivision = UNITS_KG_MAX_DIVISION;
         Bat1->Config.Config.Units.Division    = UNITS_KG_DIVISION;
         // saving parameters :
         Bat1->Config.Config.WeighingParameters.Saving.MinimumWeight = SAVING_MINIMUM_KG;
         break;
      case UNITS_G :
         Bat1->Config.Config.Units.Range       = UNITS_G_RANGE;
         Bat1->Config.Config.Units.Decimals    = UNITS_G_DECIMALS;
         Bat1->Config.Config.Units.MaxDivision = UNITS_G_MAX_DIVISION;
         Bat1->Config.Config.Units.Division    = UNITS_G_DIVISION;
         // saving parameters :
         Bat1->Config.Config.WeighingParameters.Saving.MinimumWeight = SAVING_MINIMUM_G;
         break;
      case UNITS_LB :
         Bat1->Config.Config.Units.Range       = UNITS_LB_RANGE;
         Bat1->Config.Config.Units.Decimals    = UNITS_LB_DECIMALS;
         Bat1->Config.Config.Units.MaxDivision = UNITS_LB_MAX_DIVISION;
         Bat1->Config.Config.Units.Division    = UNITS_LB_DIVISION;
         // saving parameters :
         Bat1->Config.Config.WeighingParameters.Saving.MinimumWeight = SAVING_MINIMUM_LB;
         break;
   }
   Bat1->TouchConfig = YES;
   if( !Bat1->FilesCount){
      return;
   }
   // update file configuration :
   for( int i = 0; i < Bat1->FilesCount; i++){
      Bat1->File[ i].Config.Saving.MinimumWeight = Bat1->Config.Config.WeighingParameters.Saving.MinimumWeight;
   }
   Bat1->TouchFilesystem = YES;
} // SetWeighingUnits

//******************************************************************************
//  Weighing Capacity
//******************************************************************************

int APIENTRY GetWeighingCapacity( void)
// Get weighing capacity
{
   switch( Bat1->Config.Config.Units.Units){
      case UNITS_KG :
         if( Bat1->Config.Config.Units.Range == UNITS_KG_EXT_RANGE){
            return( CAPACITY_EXTENDED);
         } else {
            return( CAPACITY_NORMAL);
         }
      case UNITS_G :
         if( Bat1->Config.Config.Units.Range == UNITS_G_EXT_RANGE){
            return( CAPACITY_EXTENDED);
         } else {
            return( CAPACITY_NORMAL);
         }
      case UNITS_LB :
         if( Bat1->Config.Config.Units.Range == UNITS_LB_EXT_RANGE){
            return( CAPACITY_EXTENDED);
         } else {
            return( CAPACITY_NORMAL);
         }
   }
   return( CAPACITY_NORMAL);
} // GetWeighingCapacity

void APIENTRY SetWeighingCapacity( int Capacity)
// Set weighing capacity
{
   switch( Bat1->Config.Config.Units.Units){
      case UNITS_KG :
         if( Capacity == CAPACITY_EXTENDED){
            Bat1->Config.Config.Units.Range = UNITS_KG_EXT_RANGE;
         } else {
            Bat1->Config.Config.Units.Range = UNITS_KG_RANGE;
         }
         break;
      case UNITS_G :
         if( Capacity == CAPACITY_EXTENDED){
            Bat1->Config.Config.Units.Range = UNITS_G_EXT_RANGE;
         } else {
            Bat1->Config.Config.Units.Range = UNITS_G_RANGE;
         }
         break;
      case UNITS_LB :
         if( Capacity == CAPACITY_EXTENDED){
            Bat1->Config.Config.Units.Range = UNITS_LB_EXT_RANGE;
         } else {
            Bat1->Config.Config.Units.Range = UNITS_LB_RANGE;
         }
         break;
   }
   Bat1->TouchConfig = YES;
} // SetWeighingCapacity

//******************************************************************************
//  Weighing Range
//******************************************************************************

DLL_IMPORT int APIENTRY GetWeighingRange( void)
// Get weighing range
{
   return( Bat1->Config.Config.Units.Range);
} // GetWeighingRange

//******************************************************************************
//  Weighing Decimals
//******************************************************************************

DLL_IMPORT int APIENTRY GetWeighingDecimals( void)
// Get weighing decimals
{
   return( Bat1->Config.Config.Units.Decimals);
} // GetWeighingDecimals

//******************************************************************************
//  Weighing Max. Division
//******************************************************************************

DLL_IMPORT int APIENTRY GetWeighingMaxDivision( void)
// Get weighing max. division
{
   return( Bat1->Config.Config.Units.MaxDivision);
} // GetWeighingMaxDivision

//******************************************************************************
//  Weighing Division
//******************************************************************************

DLL_IMPORT int APIENTRY GetWeighingDivision( void)
// Get weighing division
{
   return( Bat1->Config.Config.Units.Division);
} // GetWeighingDivision

DLL_IMPORT void APIENTRY SetWeighingDivision( int Division)
// Set weighing division
{
   Bat1->Config.Config.Units.Division = Division;
   Bat1->TouchConfig = YES;
} // SetWeighingDivision

//******************************************************************************
//  Tone default
//******************************************************************************

int APIENTRY GetToneDefault( void)
// Get default beep
{
   return( Bat1->Config.Config.Sounds.ToneDefault);
} // GetToneDefault

void APIENTRY SetToneDefault( int Tone)
// Set default beep
{
   Bat1->Config.Config.Sounds.ToneDefault = (byte)Tone;
   Bat1->TouchConfig = YES;
} // SetToneDefault

//******************************************************************************
//  Tone light
//******************************************************************************

int APIENTRY GetToneLight( void)
// Get light beep
{
   return( Bat1->Config.Config.Sounds.ToneLight);
} // GetToneLight

void APIENTRY SetToneLight( int Tone)
// Set light beep
{
   Bat1->Config.Config.Sounds.ToneLight = (byte)Tone;
   Bat1->TouchConfig = YES;
} // SetToneLight

//******************************************************************************
//  Tone OK
//******************************************************************************

int APIENTRY GetToneOk( void)
// Get OK beep
{
   return( Bat1->Config.Config.Sounds.ToneOk);
} // GetToneOk

void APIENTRY SetToneOk( int Tone)
// Set OK beep
{
   Bat1->Config.Config.Sounds.ToneOk = (byte)Tone;
   Bat1->TouchConfig = YES;
} // SetToneOk

//******************************************************************************
//  Tone Heavy
//******************************************************************************

int APIENTRY GetToneHeavy( void)
// Get heavy beep
{
   return( Bat1->Config.Config.Sounds.ToneHeavy);
} // GetToneHeavy

void APIENTRY SetToneHeavy( int Tone)
// Set heavy beep
{
   Bat1->Config.Config.Sounds.ToneHeavy = (byte)Tone;
   Bat1->TouchConfig = YES;
} // SetToneHeavy

//******************************************************************************
//  Tone keyboard
//******************************************************************************

int APIENTRY GetToneKeyboard( void)
// Get keyboard beep
{
   return( Bat1->Config.Config.Sounds.ToneKeyboard);
} // GetToneKeyboard

void APIENTRY SetToneKeyboard( int Tone)
// Set keyboard beep
{
   Bat1->Config.Config.Sounds.ToneKeyboard = (byte)Tone;
   Bat1->TouchConfig = YES;
} // SetToneKeyboard

//******************************************************************************
//  Enable Special sounds
//******************************************************************************

TYesNo APIENTRY GetEnableSpecialSounds( void)
// Get enable special sounds
{
   return( Bat1->Config.Config.Sounds.EnableSpecial);
} // GetEnableSpecialSounds

void APIENTRY SetEnableSpecialSounds( TYesNo Enable)
// Set enable special sounds
{
   Bat1->Config.Config.Sounds.EnableSpecial = (byte)Enable;
   Bat1->TouchConfig = YES;
} // SetEnableSpecialSounds

//******************************************************************************
//  Volume saving
//******************************************************************************

int APIENTRY GetVolumeSaving( void)
// Get saving volume
{
   return( Bat1->Config.Config.Sounds.VolumeSaving);
} // GetSavingVolume

void APIENTRY SetVolumeSaving( int Volume)
// Set saving volume
{
   Bat1->Config.Config.Sounds.VolumeSaving = (byte)Volume;
   Bat1->TouchConfig = YES;
} // SetVolumeSaving

//******************************************************************************
//  Volume keyboard
//******************************************************************************

int APIENTRY GetVolumeKeyboard( void)
// Get keyboard volume
{
   return( Bat1->Config.Config.Sounds.VolumeKeyboard);
} // GetVolumeKeyboard

void APIENTRY SetVolumeKeyboard( int Volume)
// Set keyboard volume
{
   Bat1->Config.Config.Sounds.VolumeKeyboard = (byte)Volume;
   Bat1->TouchConfig = YES;
} // SetVolumeKeyboard

//******************************************************************************
//  Display Mode
//******************************************************************************

int APIENTRY GetDisplayMode( void)
// Get display mode
{
   return( Bat1->Config.Config.Display.Mode);
} // GetDisplayMode

void APIENTRY SetDisplayMode( int Mode)
// Set display mode
{
   Bat1->Config.Config.Display.Mode = (byte)Mode;
   Bat1->TouchConfig = YES;
} // SetDisplayMode

//******************************************************************************
//  Display contrast
//******************************************************************************

int APIENTRY GetDisplayContrast( void)
// Get display contrast
{
   return( Bat1->Config.Config.Display.Contrast);
} // GetDisplayContrast

void APIENTRY SetDisplayContrast( int Contrast)
// Set display contrast
{
   Bat1->Config.Config.Display.Contrast = (byte)Contrast;
   Bat1->TouchConfig = YES;
} // SetDisplayContrast

//******************************************************************************
//  Backlight Mode
//******************************************************************************

int APIENTRY GetBacklightMode( void)
// Get backlight mode
{
   return( Bat1->Config.Config.Display.Backlight.Mode);
} // GetBacklightMode

void APIENTRY SetBacklightMode( int Mode)
// Set backlight mode
{
   Bat1->Config.Config.Display.Backlight.Mode = (byte)Mode;
   Bat1->TouchConfig = YES;
} // SetBacklightMode

//******************************************************************************
//  Backlight Intensity
//******************************************************************************

int APIENTRY GetBacklightIntensity( void)
// Get backlight intensity
{
   return( Bat1->Config.Config.Display.Backlight.Intensity);
} // GetBacklightIntensity

void APIENTRY SetBacklightIntensity( int Intensity)
// Set backlight intensity
{
   Bat1->Config.Config.Display.Backlight.Intensity = (byte)Intensity;
   Bat1->TouchConfig = YES;
} // SetBacklightIntensity

//******************************************************************************
//  Backlight Duration
//******************************************************************************

int APIENTRY GetBacklightDuration( void)
// Get backlight duration
{
   return( Bat1->Config.Config.Display.Backlight.Duration);
} // GetBacklightDuration

void APIENTRY SetBacklightDuration( int Duration)
// Set backlight duration
{
   Bat1->Config.Config.Display.Backlight.Duration = (word)Duration;
   Bat1->TouchConfig = YES;
} // SetBacklightDuration

//******************************************************************************
//  Printer paper width
//******************************************************************************

int APIENTRY GetPrinterPaperWidth( void)
// Get printer paper width
{
   return( Bat1->Config.Config.Printer.PaperWidth);
} // GetPrinterPaperWidth

void APIENTRY SetPrinterPaperWidth( int Width)
// Set printer paper width
{
   Bat1->Config.Config.Printer.PaperWidth = (byte)Width;
   Bat1->TouchConfig = YES;
} // SetPrinterPaperWidth

//******************************************************************************
//  Printer Communication Format
//******************************************************************************

int APIENTRY GetPrinterCommunicationFormat( void)
// Get printer communication format
{
   return( Bat1->Config.Config.Printer.CommunicationFormat);
} // GetPrinterCommunicationFormat

void APIENTRY SetPrinterCommunicationFormat( int Format)
// Set printer communication format
{
   Bat1->Config.Config.Printer.CommunicationFormat = (byte)Format;
   Bat1->TouchConfig = YES;
} // SetPrinterCommunicationFormat

//******************************************************************************
//  Printer Communication Speed
//******************************************************************************

int APIENTRY GetPrinterCommunicationSpeed( void)
// Get printer communication format
{
   return( Bat1->Config.Config.Printer.CommunicationSpeed);
} // GetPrinterCommunicationSpeed

void APIENTRY SetPrinterCommunicationSpeed( int Speed)
// Set printer communication format
{
   Bat1->Config.Config.Printer.CommunicationSpeed = (word)Speed;
   Bat1->TouchConfig = YES;
} // SetPrinterCommunicationSpeed

//******************************************************************************
//  Keyboard timeout
//******************************************************************************

int APIENTRY GetKeyboardTimeout( void)
// Get keyboard timeout
{
   return( Bat1->Config.Config.KeyboardTimeout);
} // GetKeyboardTimeout

void APIENTRY SetKeyboardTimeout( int Timeout)
// Set keyboard timeout
{
   Bat1->Config.Config.KeyboardTimeout = (word)Timeout;
   Bat1->TouchConfig = YES;
} // SetKeyboardTimeout

//******************************************************************************
//  Power off timeout
//******************************************************************************

int APIENTRY GetPowerOffTimeout( void)
// Get power off timeout
{
   return( Bat1->Config.Config.PowerOffTimeout);
} // GetPowerOffTimeout

void APIENTRY SetPowerOffTimeout( int Timeout)
// Set power off timeout
{
   Bat1->Config.Config.PowerOffTimeout = (word)Timeout;
   Bat1->TouchConfig = YES;
} // SetPowerOffTimeout

//******************************************************************************
//  Enable file parameters
//******************************************************************************

TYesNo APIENTRY GetEnableFileParameters( void)
// Get enable file parameters
{
   return( Bat1->Config.Config.EnableFileParameters);
} // GetEnableFileParameters

void APIENTRY SetEnableFileParameters( TYesNo Enable)
// Set enable file parameters
{
   Bat1->Config.Config.EnableFileParameters = (byte)Enable;
   Bat1->TouchConfig = YES;
} // SetEnableFileParameters

//******************************************************************************
//  Enable more birds
//******************************************************************************

TYesNo APIENTRY GetEnableMoreBirds( void)
// Get enable more birds
{
   return( Bat1->Config.Config.WeighingParameters.EnableMoreBirds);
} // GetEnableMoreBirds

void APIENTRY SetEnableMoreBirds( TYesNo Enable)
// Set enable more birds
{
   Bat1->Config.Config.WeighingParameters.EnableMoreBirds = (byte)Enable;
   Bat1->TouchConfig = YES;
} // SetEnableMoreBirds

//******************************************************************************
//  Weight Sorting Mode
//******************************************************************************

int APIENTRY GetWeightSortingMode( void)
// Get weight sorting mode
{
   return( Bat1->Config.Config.WeighingParameters.WeightSorting.Mode);
} // GetWeightSortingMode

void APIENTRY SetWeightSortingMode( int Mode)
// Set weight sorting mode
{
   Bat1->Config.Config.WeighingParameters.WeightSorting.Mode = (byte)Mode;
   Bat1->TouchConfig = YES;
} // SetWeightSortingMode 

/*
//******************************************************************************
//  Low Limit
//******************************************************************************

int APIENTRY GetLowLimit( void)
// Get weight sorting low limit
{
} // GetLowLimit

void APIENTRY SetLowLimit( int LowLimit)
// Set weight sorting low limit
{
} // SetLowLimit

//******************************************************************************
//  High Limit
//******************************************************************************

int APIENTRY GetHighLimit( void)
// Get weight sorting high limit
{
} // GetHighLimit

void APIENTRY SetHighLimit( int HighLimit)
// Set weight sorting high limit
{
} // SetHighLimit
*/

//******************************************************************************
//  Saving Mode
//******************************************************************************

int APIENTRY GetSavingMode( void)
// Get saving mode
{
   return( Bat1->Config.Config.WeighingParameters.Saving.Mode);
} // GetSavingMode

void APIENTRY SetSavingMode( int Mode)
// Set saving mode
{
   Bat1->Config.Config.WeighingParameters.Saving.Mode = (byte)Mode;
   Bat1->TouchConfig = YES;
} // SetSavingMode

//******************************************************************************
//  Filter
//******************************************************************************

int APIENTRY GetFilter( void)
// Get filter
{
   return( Bat1->Config.Config.WeighingParameters.Saving.Filter);
} // GetFilter

void APIENTRY SetFilter( int Filter)
// Set filter
{
   Bat1->Config.Config.WeighingParameters.Saving.Filter = (byte)Filter;
   Bat1->TouchConfig = YES;
} // SetFilter

//******************************************************************************
//  Stabilisation Time
//******************************************************************************

int APIENTRY GetStabilisationTime( void)
// Get stabilisation time
{
   return( Bat1->Config.Config.WeighingParameters.Saving.StabilisationTime);
} // GetStabilisationTime

void APIENTRY SetStabilisationTime( int StabilisationTime)
// Set stabilisation time
{
   Bat1->Config.Config.WeighingParameters.Saving.StabilisationTime = (byte)StabilisationTime;
   Bat1->TouchConfig = YES;
} // SetStabilisationTime

//******************************************************************************
//  Minimum Weight
//******************************************************************************

int APIENTRY GetMinimumWeight( void)
// Get minimum weight
{
   return( Bat1->Config.Config.WeighingParameters.Saving.MinimumWeight);
} // GetMinimumWeight

void APIENTRY SetMinimumWeight( int Weight)
// Set minimum weight
{
   Bat1->Config.Config.WeighingParameters.Saving.MinimumWeight = Weight;
   Bat1->TouchConfig = YES;
} // SetMinimumWeight

//******************************************************************************
//  Stabilisation range
//******************************************************************************

int APIENTRY GetStabilisationRange( void)
// Get stabilisation range
{
   return( Bat1->Config.Config.WeighingParameters.Saving.StabilisationRange);
} // GetStabilisationRange

void APIENTRY SetStabilisationRange( int Range)
// Set stabilisation range
{
   Bat1->Config.Config.WeighingParameters.Saving.StabilisationRange = (word)Range;
   Bat1->TouchConfig = YES;
} // SetStabilisationRange

//******************************************************************************
//  Uniformity range
//******************************************************************************

int APIENTRY GetUniformityRange( void)
// Get uniformity range
{
   return( Bat1->Config.Config.StatisticParameters.UniformityRange);
} // GetUniformityRange

void APIENTRY SetUniformityRange( int Range)
// Set uniformity range
{
   Bat1->Config.Config.StatisticParameters.UniformityRange = (byte)Range;
   Bat1->TouchConfig = YES;
} // SetUniformityRange

//******************************************************************************
//  Histogram mode
//******************************************************************************

int APIENTRY GetHistogramMode( void)
// Get histogram mode
{
   return( Bat1->Config.Config.StatisticParameters.Histogram.Mode);
} // GetHistogramMode

//******************************************************************************
//  Histogram Range
//******************************************************************************

int APIENTRY GetHistogramRange( void)
// Get histogram range
{
   return( Bat1->Config.Config.StatisticParameters.Histogram.Range);
} // GetHistogramRange


void APIENTRY SetHistogramRange( int Range)
// Set histogram range
{
   Bat1->Config.Config.StatisticParameters.Histogram.Mode  = HISTOGRAM_MODE_RANGE;
   Bat1->Config.Config.StatisticParameters.Histogram.Range = (byte)Range;
   Bat1->TouchConfig = YES;
} // SetHistogramRange

//******************************************************************************
//  Histogram Step
//******************************************************************************

int APIENTRY GetHistogramStep( void)
// Get histogram step
{
   return( Bat1->Config.Config.StatisticParameters.Histogram.Step);
} // GetHistogramStep

void APIENTRY SetHistogramStep( int Step)
// Set histogram step
{
   Bat1->Config.Config.StatisticParameters.Histogram.Mode = HISTOGRAM_MODE_STEP;
   Bat1->Config.Config.StatisticParameters.Histogram.Step = Step;
   Bat1->TouchConfig = YES;
} // SetHistogramStep

//******************************************************************************
//  Exception
//******************************************************************************

unsigned APIENTRY GetExceptionTimestamp( void)
// Get exception date & time
{
   return( Bat1->ExceptionInfo.Timestamp);
} // GetExceptionTimestamp

unsigned APIENTRY GetExceptionAddress( void)
// Get exception address
{
   return( Bat1->ExceptionInfo.Address);
} // GetExceptionAddress

int APIENTRY GetExceptionType( void)
// Get exception type
{
   return( Bat1->ExceptionInfo.Type);
} // GetExceptionType

unsigned APIENTRY GetExceptionStatus( void)
// Get exception status
{
   return( Bat1->ExceptionInfo.Status);
} // GetExceptionStatus

//******************************************************************************
//  WatchDog
//******************************************************************************

unsigned APIENTRY GetWatchDogTimestamp( void)
// Get watchdog date & time
{
   return( Bat1->WatchDogInfo.Timestamp);
} // GetWatchDogTimestamp

unsigned APIENTRY GetWatchDogStatus( void)
// Get watchdog status
{
   return( Bat1->WatchDogInfo.Status);
} // GetWatchDogStatus

//******************************************************************************
//  Files count
//******************************************************************************

int APIENTRY GetFilesCount( void)
// Get number of files
{
   return( Bat1->FilesCount);
} // GetFilesCount

//******************************************************************************
//  Files delete
//******************************************************************************

void APIENTRY FilesDeleteAll( void)
// Delete all files
{
   Bat1->FilesDelete();
   Bat1->TouchFilesystem = YES;
} // FilesDeleteAll

//******************************************************************************
//  File create
//******************************************************************************

int APIENTRY FileCreate( void)
// Create new file, returns index
{
   int Index = Bat1->FilesCount;
   Bat1->FilesCount++;
   Bat1->File[Index].Config = Bat1->Config.Config.WeighingParameters; // fill with defaults
   Bat1->TouchFilesystem = YES;
   return( Index);
} // FileCreate

//******************************************************************************
//  File Name
//******************************************************************************

void APIENTRY GetFileName( int Index, char *Name)
// Get file name
{
   strcpy( Name, Bat1->File[ Index].Info.Info.Name);
} // GetFileName

void APIENTRY SetFileName( int Index, char *Name)
// Set file name
{
   strncpy( Bat1->File[ Index].Info.Info.Name, Name, FDIR_NAME_LENGTH);
   Bat1->File[ Index].Info.Info.Name[ FDIR_NAME_LENGTH] = '\0';
   Bat1->TouchFilesystem = YES;
} // SetFileName

//******************************************************************************
//  File Note
//******************************************************************************

void APIENTRY GetFileNote( int Index, char *Note)
// Get file note
{
   strcpy( Note, Bat1->File[ Index].Info.Info.Note);
} // GetFileNote

void APIENTRY SetFileNote( int Index, char *Note)
// Set file note
{
   strncpy( Bat1->File[ Index].Info.Info.Note, Note, FDIR_NOTE_LENGTH);
   Bat1->File[ Index].Info.Info.Note[ FDIR_NOTE_LENGTH] = '\0';
   Bat1->TouchFilesystem = YES;
} // SetFileNote

//******************************************************************************
//  File creation
//******************************************************************************

unsigned APIENTRY GetFileCreation( int Index)
// Get file creation date
{
   return( Bat1->File[ Index].Info.Info.Created);
} // GetFileCreation

//******************************************************************************
//  File size
//******************************************************************************

int APIENTRY GetFileRawSize( int Index)
// Get file size [bytes]
{
   return( Bat1->File[ Index].Info.Info.Size);
} // GetFileRawSize

//******************************************************************************
//  File current
//******************************************************************************

TYesNo APIENTRY IsCurrentFile( int Index)
// Returns YES on current working file
{
   return( Bat1->File[ Index].Current);
} // IsCurrentFile

void APIENTRY SetCurrentFile( int Index)
// Set file on <Index> as current working file
{
   // clear all flags :
   for( int i = 0; i < Bat1->FilesCount; i++){
      Bat1->File[ i].Current = NO;
   }
   Bat1->File[ Index].Current = YES;  // set this flag
   Bat1->TouchFilesystem = YES;
} // SetCurrentFile

//******************************************************************************
//  File Enable more birds
//******************************************************************************

DLL_IMPORT TYesNo APIENTRY GetFileEnableMoreBirds( int Index)
// Get enable more birds
{
   return( Bat1->File[ Index].Config.EnableMoreBirds);
} // GetFileEnableMoreBirds

DLL_IMPORT void APIENTRY SetFileEnableMoreBirds( int Index, TYesNo Enable)
// Set enable more birds
{
   Bat1->File[ Index].Config.EnableMoreBirds = Enable;
   Bat1->TouchFilesystem = YES;
} // SetFileEnableMoreBirds

//******************************************************************************
//  File Number of birds
//******************************************************************************

int APIENTRY GetFileNumberOfBirds( int Index)
// Get number of birds
{
   return( Bat1->File[ Index].Config.NumberOfBirds);
} // GetFileNumberOfBirds

void APIENTRY SetFileNumberOfBirds( int Index, int NumberOfBirds)
// Set number of birds
{
   Bat1->File[ Index].Config.NumberOfBirds = NumberOfBirds;
   Bat1->TouchFilesystem = YES;
} // SetFileNumberOfBirds

//******************************************************************************
//  File Weight sorting mode
//******************************************************************************

int APIENTRY GetFileWeightSortingMode( int Index)
// Get weight sorting mode
{
   return( Bat1->File[ Index].Config.WeightSorting.Mode);
} // GetFileWeightSortingMode

void APIENTRY SetFileWeightSortingMode( int Index, int Mode)
// Get weight sorting mode
{
   Bat1->File[ Index].Config.WeightSorting.Mode = (byte)Mode;
   Bat1->TouchFilesystem = YES;
} // SetFileWeightSortingMode

//******************************************************************************
//  File Low Limit
//******************************************************************************

int APIENTRY GetFileLowLimit( int Index)
// Get weight sorting low limit
{
   return( Bat1->File[ Index].Config.WeightSorting.LowLimit);
} // GetFileLowLimit

void APIENTRY SetFileLowLimit( int Index, int LowLimit)
// Set weight sorting low limit
{
   Bat1->File[ Index].Config.WeightSorting.LowLimit = LowLimit;
   Bat1->TouchFilesystem = YES;
} // SetFileLowLimit

//******************************************************************************
//  File High Limit
//******************************************************************************

int APIENTRY GetFileHighLimit( int Index)
// Get weight sorting high limit
{
   return( Bat1->File[ Index].Config.WeightSorting.HighLimit);
} // GetFileHighLimit

void APIENTRY SetFileHighLimit( int Index, int HighLimit)
// Set weight sorting high limit
{
   Bat1->File[ Index].Config.WeightSorting.HighLimit = HighLimit;
   Bat1->TouchFilesystem = YES;
} // SetFileHighLimit

//******************************************************************************
//  File Saving Mode
//******************************************************************************

int APIENTRY GetFileSavingMode( int Index)
// Get saving mode
{
   return( Bat1->File[ Index].Config.Saving.Mode);
} // GetFileSavingMode

void APIENTRY SetFileSavingMode( int Index, int Mode)
// Set saving mode
{
   Bat1->File[ Index].Config.Saving.Mode = (byte)Mode;
   Bat1->TouchFilesystem = YES;
} // SetFileSavingMode

//******************************************************************************
//  File Filter
//******************************************************************************

int APIENTRY GetFileFilter( int Index)
// Get filter
{
   return( Bat1->File[ Index].Config.Saving.Filter);
} // GetFileFilter

void APIENTRY SetFileFilter( int Index, int Filter)
// Set filter
{
   Bat1->File[ Index].Config.Saving.Filter = (byte)Filter;
   Bat1->TouchFilesystem = YES;
} // SetFileFilter

//******************************************************************************
//  File Stabilisation Time
//******************************************************************************

int APIENTRY GetFileStabilisationTime( int Index)
// Get stabilisation time
{
   return( Bat1->File[ Index].Config.Saving.StabilisationTime);
} // GetFileStabilisationTime

void APIENTRY SetFileStabilisationTime( int Index, int StabilisationTime)
// Set stabilisation time
{
   Bat1->File[ Index].Config.Saving.StabilisationTime = (byte)StabilisationTime;
   Bat1->TouchFilesystem = YES;
} // SetFileStabilisationTime

//******************************************************************************
//  File Minimum Weight
//******************************************************************************

int APIENTRY GetFileMinimumWeight( int Index)
// Get minimum weight
{
   return( Bat1->File[ Index].Config.Saving.MinimumWeight);
} // GetMinimumWeight

void APIENTRY SetFileMinimumWeight( int Index, int Weight)
// Set minimum weight
{
   Bat1->File[ Index].Config.Saving.MinimumWeight = Weight;
   Bat1->TouchFilesystem = YES;
} // SetMinimumWeight

//******************************************************************************
//  File Stabilisation Range
//******************************************************************************

int APIENTRY GetFileStabilisationRange( int Index)
// Get stabilisation range
{
   return( Bat1->File[ Index].Config.Saving.StabilisationRange);
} // GetFileStabilisationRange

void APIENTRY SetFileStabilisationRange( int Index, int Range)
// Set stabilisation range
{
   Bat1->File[ Index].Config.Saving.StabilisationRange = (word)Range;
   Bat1->TouchFilesystem = YES;
} // SetFileStabilisationRange

//******************************************************************************
//  Get File Samples
//******************************************************************************

int APIENTRY GetFileSamplesCount( int Index)
// Get file samples count
{
   return( Bat1->File[ Index].SamplesCount);
} // GetFileSamplesCount

//******************************************************************************
//  File Clear Samples
//******************************************************************************

void APIENTRY FileClearSamples( int Index)
// Clear file samples
{
   Bat1->ClearSamples( Index);
   Bat1->TouchFilesystem = YES;
} // FileClearSamples

//******************************************************************************
//  File Alloc Samples
//******************************************************************************

void APIENTRY FileAllocSamples( int Index, int SamplesCount)
// Allocate file samples
{
   Bat1->AllocSamples( Index, SamplesCount);
   Bat1->TouchFilesystem = YES;
} // FileAllocSamples

//******************************************************************************
//  Sample Timestamp
//******************************************************************************

int APIENTRY GetSampleTimestamp( int Index, int SampleIndex)
// Get file sample timestamp
{
   return( Bat1->File[ Index].Sample[ SampleIndex].Timestamp);
} // GetSampleTimestamp

void APIENTRY SetSampleTimestamp( int Index, int SampleIndex, int Timestamp)
// Set file sample timestamp
{
   Bat1->File[ Index].Sample[ SampleIndex].Timestamp = Timestamp;
   Bat1->TouchFilesystem = YES;
} // SetSampleTimestamp

//******************************************************************************
//  Sample Weight
//******************************************************************************

int APIENTRY GetSampleWeight( int Index, int SampleIndex)
// Get file sample weight
{
   return( Bat1->File[ Index].Sample[ SampleIndex].Weight);
} // GetSampleWeight

void APIENTRY SetSampleWeight( int Index, int SampleIndex, int Weight)
// Set file sample weight
{
   Bat1->File[ Index].Sample[ SampleIndex].Weight = Weight;
   Bat1->TouchFilesystem = YES;
} // SetSampleWeight

//******************************************************************************
//  Sample Flag
//******************************************************************************

int APIENTRY GetSampleFlag( int Index, int SampleIndex)
// Get file sample flag
{
   return( Bat1->File[ Index].Sample[ SampleIndex].Flag);
} // GetSampleFlag

void APIENTRY SetSampleFlag( int Index, int SampleIndex, int Flag)
// Set file sample flag
{
   Bat1->File[ Index].Sample[ SampleIndex].Flag = (byte)Flag;
   Bat1->TouchFilesystem = YES;
} // SetSampleFlag

//******************************************************************************
//  Groups Count
//******************************************************************************

int APIENTRY GetGroupsCount( void)
// Get number of groups
{
   return( Bat1->GroupsCount);
} // GetGroupsCount

//******************************************************************************
//  Groups delete
//******************************************************************************

void APIENTRY GroupsDeleteAll( void)
// Delete all groups
{
   Bat1->GroupsDelete();
   Bat1->TouchFilesystem = YES;
} // GroupsDeleteAll

//******************************************************************************
//  Group Create
//******************************************************************************

int APIENTRY GroupCreate( void)
// Create new group, returns index
{
   int Index = Bat1->GroupsCount;
   Bat1->GroupsCount++;
   Bat1->TouchFilesystem = YES;
   return( Index);
} // GroupCreate

//******************************************************************************
//  Group Name
//******************************************************************************

void APIENTRY GetGroupName( int Index, char *Name)
// Get group name
{
   strcpy( Name, Bat1->Group[ Index].Info.Info.Name);
} // GetGroupName

void APIENTRY SetGroupName( int Index, char *Name)
// Set group name
{
   strncpy( Bat1->Group[ Index].Info.Info.Name, Name, FDIR_NAME_LENGTH);
   Bat1->Group[ Index].Info.Info.Name[ FDIR_NAME_LENGTH] = '\0';
   Bat1->TouchFilesystem = YES;
} // SetGroupName

//******************************************************************************
//  Group Note
//******************************************************************************

void APIENTRY GetGroupNote( int Index, char *Note)
// Get group note
{
   strcpy( Note, Bat1->Group[ Index].Info.Info.Note);
} // GetGroupNote

void APIENTRY SetGroupNote( int Index, char *Note)
// Set group note
{
   strncpy( Bat1->Group[ Index].Info.Info.Note, Note, FDIR_NOTE_LENGTH);
   Bat1->Group[ Index].Info.Info.Name[ FDIR_NOTE_LENGTH] = '\0';
   Bat1->TouchFilesystem = YES;
} // SetGroupNote

//******************************************************************************
//  Group Creation
//******************************************************************************

unsigned APIENTRY GetGroupCreation( int Index)
// Get group creation date
{
   return( Bat1->Group[ Index].Info.Info.Created);
} // GetGroupCreation

//******************************************************************************
//  Group Files Count
//******************************************************************************

int APIENTRY GetGroupFilesCount( int Index)
// Returns number of files in the group
{
   return( Bat1->Group[ Index].FilesCount);
} // GetGroupFilesCount

//******************************************************************************
//  Group Clear Files
//******************************************************************************

void APIENTRY GroupClearFiles( int Index)
// Clear group files
{
   TBat1::TGroupEntry *Ge = &Bat1->Group[ Index];
   Ge->FilesCount = 0;
   Bat1->TouchFilesystem = YES;
} // GroupClearFiles

//******************************************************************************
//  Group File
//******************************************************************************

int APIENTRY GetGroupFile( int Index, int FileIndex)
// Returns index into file list from the group at position <FileIndex>
{
   return( Bat1->Group[ Index].FileIndex[ FileIndex]);
} // GetGroupFile

void APIENTRY AddGroupFile( int Index, int FileIndex)
// Add <FileIndex> into group
{
   TBat1::TGroupEntry *Ge = &Bat1->Group[ Index];
   Ge->FileIndex[ Ge->FilesCount] = FileIndex;
   Ge->FilesCount++;
   Bat1->TouchFilesystem = YES;
} // AddGroupFile
