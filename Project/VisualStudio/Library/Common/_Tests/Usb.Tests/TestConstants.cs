﻿using System;

namespace Usb.Tests
{
   public class TestConstants
   {
      public const int VID = 0xA600;
      public const int PID = 0xE2A0;

      public static byte[] SendMessage1
      {
         get
         {
            var rand = new Random(DateTime.Now.Millisecond);
            var s = new byte[rand.Next(500)+1];
            rand.NextBytes(s);
            return s;
         }
      }
      //public static byte[] SendMessage2 = { 0x55, 0x03, 0x00, 0xFD, 0x00, 0x01, 0x01, 0x00, 0x00, 0xCC };
      public static byte[] RcvMessage =  SendMessage1;
      //public static byte[] RcvMessage1 = { 0x55, 0x01, 0x00, 0xFF, 0x80, 0xFF, 0x80, 0xCC };

   }
}
