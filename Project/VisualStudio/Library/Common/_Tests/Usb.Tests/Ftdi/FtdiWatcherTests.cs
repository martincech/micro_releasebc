﻿using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Usb.FTDI;

namespace Usb.Tests.Ftdi
{
   [TestClass]
   public class FtdiWatcherTests
   {
      private UsbDeviceManager ftdiWatcher;
      private ManualResetEvent manualReset;
      private static bool _isConnected;

      [TestInitialize]
      public void Init()
      {
         ftdiWatcher = UsbDeviceManager.Instance;
      }

      [TestMethod]
      public void FastDisposeTest()
      {
         ftdiWatcher.Dispose();
      }

      [TestMethod]
      public void Watcher_WatchingStartedSomeDevicesConnected()
      {
         Assert.IsTrue(ftdiWatcher.ConnectedDevices.Any());
      }

      [TestMethod]
      public void Watcher_Contains_Bat2Old()
      {
         Assert.IsTrue(ftdiWatcher.ConnectedDevices.Any(device =>
            device is FTDIDevice && (device.ProductName == FtdiDeviceConstants.USB_DEVICE_NAME ||
                                     device.ProductName == FtdiDeviceConstants.USB_DUTCHMAN_NAME)
            ));
      }
   }
}
