﻿using System.ServiceModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Services.Tests.UnitTests
{
   [TestClass]
   public class PublishServiceTests
   {
      private PublishingClient publishClient;
      private PublishingClient publishQueuedClient;

      [TestInitialize]
      public void Init()
      {
         PublishingService.StartService();
         publishClient = new PublishingClient(
            PublishingService.NetPipeBinding, new EndpointAddress(PublishingService.ENDPOINT_NET_PIPE));
         publishQueuedClient = new PublishingClient(
            PublishingService.MsmqBinding, new EndpointAddress(PublishingService.ENDPOINT_MSMQ));
      }

      [TestCleanup]
      public void Clean()
      {
         publishClient.Close();
         PublishingService.StopService();
      }

      [TestMethod]
      public void PublisherService_Created()
      {
         Assert.IsNotNull(PublishingService.Host);
      }

      [TestMethod]
      public void PublishClient_Created_And_Connected()
      {
         Assert.IsTrue(publishClient.State == CommunicationState.Created);
         publishClient.Open();
         Assert.IsTrue(publishClient.State == CommunicationState.Opened);
      }

      [TestMethod]
      public void PublishClient_EventRaised()
      {
         publishClient.OnEvent1();
         Assert.IsTrue(publishClient.State == CommunicationState.Opened);
      }

      [TestMethod]
      public void PublishQueuedClient_Created_And_Connected()
      {
         Assert.IsTrue(publishQueuedClient.State == CommunicationState.Created);
         publishQueuedClient.Open();
         Assert.IsTrue(publishQueuedClient.State == CommunicationState.Opened);
      }

      [TestMethod]
      public void PublishQueuedClient_EventRaised()
      {
         publishQueuedClient.OnEvent1();
         Assert.IsTrue(publishQueuedClient.State == CommunicationState.Opened);
      }
   }
}
