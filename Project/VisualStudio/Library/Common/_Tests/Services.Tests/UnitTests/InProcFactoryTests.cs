﻿using System.Linq;
using System.ServiceModel;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.Hosting;

namespace Services.Tests.UnitTests
{
   [TestClass]
   public class InProcFactoryTests
   {
      private IClientFactory<EventsCallbackService, ITestServiceEvents> factory;
      private EventsCallbackService singletonService;

      [TestInitialize]
      public void Init()
      {
         factory = InProcFactory.CreateClientFactory<EventsCallbackService, ITestServiceEvents>();
         singletonService = new EventsCallbackService();
         factory.SetSingleton(singletonService);
         Assert.IsFalse(InProcFactory.Hosts.Any());
      }

      [TestCleanup]
      public void Clean()
      {
         InProcFactory.StopAllServices();
         Assert.IsFalse(InProcFactory.Hosts.Any());
         Assert.IsFalse(InProcFactory.Clients.Any());
      }

      [TestMethod]
      public void ServiceHost_Created_OnlyOnce()
      {
         factory.CreateInstance();
         var hosts = InProcFactory.Hosts.FirstOrDefault(item => item.Key == typeof(EventsCallbackService));
         Assert.IsNotNull(hosts);
         factory.CreateInstance();
         var newHosts = InProcFactory.Hosts.FirstOrDefault(item => item.Key == typeof (EventsCallbackService));
         Assert.AreEqual(hosts, newHosts);
         Assert.IsTrue(InProcFactory.Hosts[typeof (EventsCallbackService)].ContainsKey(typeof (ITestServiceEvents)));
      }

      [TestMethod]   
      public void CanCreateClientService()
      {
         var client = factory.CreateInstance();
         Assert.IsNotNull(client);
         Assert.IsTrue(InProcFactory.Clients.ContainsKey(typeof(ITestServiceEvents)));
      }

      [TestMethod]
      public void CreateClient_CloseClient_Opened_ValidlyClosed()
      {
         var client = factory.CreateInstance();
         Assert.IsInstanceOfType(client, typeof (ICommunicationObject));
         (client as ICommunicationObject).Open();
         Assert.IsTrue((client as ICommunicationObject).State == CommunicationState.Opened);
         Assert.IsTrue(InProcFactory.Clients.ContainsKey(typeof(ITestServiceEvents)));
         factory.CloseProxy(client);
         Assert.IsFalse(InProcFactory.Clients.ContainsKey(typeof(ITestServiceEvents)));
      }

      [TestMethod]
      public void CreateClient_CloseClient_Closed_ValidlyClosed()
      {
         var client = factory.CreateInstance();
         Assert.IsInstanceOfType(client, typeof(ICommunicationObject));
         Assert.IsTrue((client as ICommunicationObject).State == CommunicationState.Created);
         Assert.IsTrue(InProcFactory.Clients.ContainsKey(typeof(ITestServiceEvents)));
         factory.CloseProxy(client);
         Assert.IsFalse(InProcFactory.Clients.ContainsKey(typeof(ITestServiceEvents)));
      }
      [TestMethod]
      public void CallingMethod_InvokedOnServer()
      {
         var raised = false;
         var synchro = new ManualResetEvent(false);
         singletonService.Event1Invoked += (sender, args) =>
         {
            raised = true;
            synchro.Set();
         };

         var client = factory.CreateInstance();
         client.OnEvent1();
         factory.CloseProxy(client);
         synchro.WaitOne(1000);
         Assert.IsTrue(raised);
      }
   }
}
