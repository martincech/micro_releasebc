﻿using System;
using System.Diagnostics;
using System.Messaging;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.PublishSubscribe;
using Services.QueuedProcessing;

namespace Services.Tests
{
   [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
   internal class PublishingService 
      : PublishService<ITestServiceEvents>, ITestServiceEvents
   {
      #region Private fields and constants

      private const int OPEN_CLOSE_TIMEOUT = 2;
      private const int RECEIVE_SEND_TIMEOUT = 15;

// ReSharper disable InconsistentNaming
      private static readonly Binding msmqBinding = new NetMsmqBinding
// ReSharper restore InconsistentNaming
      {
         Security = { Mode = NetMsmqSecurityMode.None },
         CloseTimeout = TimeSpan.FromSeconds(OPEN_CLOSE_TIMEOUT),
         OpenTimeout = TimeSpan.FromSeconds(OPEN_CLOSE_TIMEOUT),
         ReceiveTimeout = TimeSpan.FromSeconds(RECEIVE_SEND_TIMEOUT),
         SendTimeout = TimeSpan.FromSeconds(RECEIVE_SEND_TIMEOUT)
      };
// ReSharper disable InconsistentNaming
      private static readonly Binding netPipeBinding = new NetNamedPipeBinding
// ReSharper restore InconsistentNaming
      {
         Security = { Mode = NetNamedPipeSecurityMode.None},
         CloseTimeout = TimeSpan.FromSeconds(OPEN_CLOSE_TIMEOUT),
         OpenTimeout = TimeSpan.FromSeconds(OPEN_CLOSE_TIMEOUT),
         ReceiveTimeout = TimeSpan.FromSeconds(RECEIVE_SEND_TIMEOUT),
         SendTimeout = TimeSpan.FromSeconds(RECEIVE_SEND_TIMEOUT)
      };

      #endregion

      #region Public stuff

      public static Binding MsmqBinding
      {
         get { return msmqBinding; }
      }

      public static Binding NetPipeBinding
      {
         get { return netPipeBinding; }
      }

      public const string ENDPOINT_MSMQ = "net.msmq://localhost/private/ITestServiceEventsQueued";
      public const string ENDPOINT_NET_PIPE = "net.pipe://localhost/ITestServiceEvents";

      public static ServiceHost Host = null;
      #endregion

      private PublishingService() 
      {
      }

      public static void StartService()
      {
         if (Host == null)
         {
            try
            {
               Host = new ServiceHost(typeof (PublishingService));
               Host.AddServiceEndpoint(typeof (ITestServiceEvents), MsmqBinding, ENDPOINT_MSMQ);
               Host.AddServiceEndpoint(typeof (ITestServiceEvents), NetPipeBinding, ENDPOINT_NET_PIPE);

               var ev = new EventHandler(delegate
               {
                  Debug.Assert(Host != null);
                  foreach (var endpoint in Host.Description.Endpoints)
                  {
                     endpoint.VerifyQueue();
                  }
               });
               Host.Opening += ev;
               Host.Open();
               Host.Opening -= ev;
            }
            catch (Exception ex)
            {
               Assert.IsTrue(false, string.Format("Exception when creating server: {0}", ex.Message));
            }
         }

         Assert.IsTrue(Host.State == CommunicationState.Opened);
         Assert.IsTrue(MessageQueue.Exists(QueuedServiceHelper.GetQueueFromUri(new Uri(ENDPOINT_MSMQ))));
      }

      public static void StopService()
      {
         if (Host == null)
         {
            return;
         }
         var ev = new ManualResetEvent(false);
         var handler = new EventHandler(delegate
         {
            Debug.Assert(Host != null);
            foreach (var endpoint in Host.Description.Endpoints)
            {
               endpoint.DeleteQueue();
            }
            ev.Set();
         });
         Host.Closed += handler;
         Host.Close();
         ev.WaitOne();
         Host.Closed -= handler;
         Assert.IsFalse(MessageQueue.Exists(QueuedServiceHelper.GetQueueFromUri(new Uri(ENDPOINT_MSMQ))));
         Assert.IsTrue(Host.State == CommunicationState.Closed);
         Host = null;
      }

      #region Implementation of ITestServiceEvents

      public void OnEvent1()
      {
         FireEvent();
      }

      public void OnEvent2(int number)
      {
         FireEvent(number);
      }

      public void OnEvent3(int number, string text)
      {
         FireEvent(number, text);
      }

      #endregion
   }
}