﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.QueuedProcessing;

namespace Services.Tests
{
   [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
   public class EventsCallbackService : EventsCallback
   {
      #region Private field and constants

      private const int OPEN_CLOSE_TIMEOUT = 2;
      private const int RECEIVE_SEND_TIMEOUT = 15;

      // ReSharper disable InconsistentNaming
      private static readonly Binding _msmqBinding = new NetMsmqBinding
         // ReSharper restore InconsistentNaming
      {
         Security = { Mode = NetMsmqSecurityMode.None },
         CloseTimeout = TimeSpan.FromSeconds(OPEN_CLOSE_TIMEOUT),
         OpenTimeout = TimeSpan.FromSeconds(OPEN_CLOSE_TIMEOUT),
         ReceiveTimeout = TimeSpan.FromSeconds(RECEIVE_SEND_TIMEOUT),
         SendTimeout = TimeSpan.FromSeconds(RECEIVE_SEND_TIMEOUT)
      };

      private static EventsCallbackService _instance;
      #endregion

      #region Public stuff

      public const string ENDPOINT = "net.msmq://localhost/private/EventsCallbackService";
      public static Binding MsmqBinding
      {
         get { return _msmqBinding; }
      }

      public static ServiceHost Host = null;

      #endregion

      public static EventsCallbackService StartService()
      {
         if (_instance  == null)
         {
            _instance = new EventsCallbackService();
         }
         if (Host == null)
         {
            try
            {
               
               Host = new ServiceHost(_instance);
               Host.AddServiceEndpoint(typeof(ITestServiceEvents), MsmqBinding, ENDPOINT);
               var ev = new EventHandler(delegate
               {
                  Debug.Assert(Host != null);
                  foreach (var endpoint in Host.Description.Endpoints)
                  {
                     endpoint.VerifyQueue();
                  }
               });
               Host.Opening += ev;
               Host.Open();
               Host.Opening -= ev;
            }
            catch (Exception ex)
            {
               Assert.IsTrue(false, string.Format("Exception when creating server: {0}", ex.Message));
            }
         }
         Assert.IsTrue(Host.State == CommunicationState.Opened);
         return _instance;
      }

      public static void StopService()
      {
         if (Host == null)
         {
            return;
         }
         var ev = new ManualResetEvent(false);
         var handler = new EventHandler(delegate
         {
            //Debug.Assert(Host != null);
            //foreach (var endpoint in Host.Description.Endpoints)
            //{
            //   endpoint.DeleteQueue();
            //}
            ev.Set();
         });
         Host.Closed += handler;
         Host.Close();
         ev.WaitOne();
         Host.Closed -= handler;
         Assert.IsTrue(Host.State == CommunicationState.Closed);
         Host = null;
      }
 
   }

}
