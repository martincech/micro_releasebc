﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services.PublishSubscribe.Contracts;

namespace Services.Tests
{
   internal class SubscribtionPersistentClient : ClientBase<IPersistentSubscriptionContract>, IPersistentSubscriptionContract
   {

      public SubscribtionPersistentClient()
      {
      }

      public SubscribtionPersistentClient( string endpointConfigurationName) :
         base( endpointConfigurationName)
      {
      }

      public SubscribtionPersistentClient( string endpointConfigurationName, string remoteAddress) :
         base( endpointConfigurationName, remoteAddress)
      {
      }

      public SubscribtionPersistentClient( string endpointConfigurationName, EndpointAddress remoteAddress) :
         base( endpointConfigurationName, remoteAddress)
      {
      }

      public SubscribtionPersistentClient( Binding binding, EndpointAddress remoteAddress) :
         base( binding, remoteAddress)
      {
      }

      #region Implementation of IPersistentSubscriptionContract

      /// <summary>
      /// Subscribe for all events
      /// </summary>
      /// <param name="address">Address where should events be sended</param>
      public void SubscribeAll(string address)
      {
         Channel.SubscribeAll(address);
      }

      /// <summary>
      /// Subscribe for specific event
      /// </summary>
      /// <param name="address">Address where should events be sended</param>
      /// <param name="eventOperation">event operation name</param>
      public void Subscribe(string address, string eventOperation)
      {
         Channel.Subscribe(address, eventOperation);
      }

      /// <summary>
      /// Unsubscribe for specific event
      /// </summary>
      /// <param name="address">Address which will be unsubscribted</param>
      /// <param name="eventOperation">event operation name</param>
      public void Unsubscribe(string address, string eventOperation)
      {
         Channel.Unsubscribe(address, eventOperation);
      }

      /// <summary>
      /// Unsubscribe for all events
      /// </summary>
      /// <param name="address">Address where should events be sended</param>
      public void UnsubscribeAll(string address)
      {
         Channel.UnsubscribeAll(address);
      }

      #endregion

      public new void Close()
      {
         if (State == CommunicationState.Opened)
         {
            base.Close();
         }
         else
         {
            Abort();
         }
         Assert.IsTrue(State == CommunicationState.Closed);
      }
   }
}