﻿using System.ServiceModel;
using Services.PublishSubscribe.Contracts;

namespace Services.Tests
{
   [ServiceContract(CallbackContract = typeof(ITestServiceEvents))]
   public interface ITestContractContract : ISubscriptionContract
   {
   }

   [ServiceContract(CallbackContract = typeof(ITestServiceEventsInherited))]
   public interface ITestInheritedContract : ISubscriptionContract
   {
   }
}