﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Desktop.Wpf.AttachedProperties;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Desktop.Wpf.Tests.UnitTests
{
   [TestClass]
   public class SelectedItemsSynchronizerTests
   {
      private ObservableCollection<object> synchroList;

      [TestInitialize]
      public void Init()
      {
         synchroList = new ObservableCollection<object>();
      }

      [TestMethod]
      [ExpectedException(typeof (ArgumentException))]
      public void Synchronizer_ThrowsException_OnInvalidSynchroList()
      {
         var tb = new ListBox();
         SelectedItemsSynchronizer.SetSelectedItems(tb, new List<object>());
      }

      [TestMethod]
      [ExpectedException(typeof (InvalidOperationException))]
      public void Synchronizer_ThrowsException_OnInvalidDependencyObject()
      {
         var dependencyObject = new Label();
         SelectedItemsSynchronizer.SetSelectedItems(dependencyObject, synchroList);
      }

      [TestMethod]
      public void ListBox_CanAttach()
      {
         var tb = new ListBox {SelectionMode = SelectionMode.Multiple};
         SelectedItemsSynchronizer.SetSelectedItems(tb, synchroList);
         Assert.AreSame(synchroList, SelectedItemsSynchronizer.GetSelectedItems(tb));
      }

      [TestMethod]
      public void MultiSelector_CanAttach()
      {
         var dg = new DataGrid();
         SelectedItemsSynchronizer.SetSelectedItems(dg, synchroList);
         Assert.AreSame(synchroList, SelectedItemsSynchronizer.GetSelectedItems(dg));
      }

      [TestMethod]
      public void SynchroList_IsSynchronized()
      {
         var lb = new ListBox {SelectionMode = SelectionMode.Multiple};
         var dataList = new List<object>()
         {
            new object(),
            new object(),
            new object(),
         };

         lb.ItemsSource = dataList;
         SelectedItemsSynchronizer.SetSelectedItems(lb, synchroList);

         lb.SelectAll();
         CollectionAssert.AreEquivalent(dataList, synchroList);
         lb.UnselectAll();
         Assert.IsFalse(synchroList.Any());

         lb.SelectedItem = lb.Items.GetItemAt(0);
         Assert.IsTrue(synchroList.Count == 1);
         CollectionAssert.AreEquivalent(lb.SelectedItems, synchroList);

         synchroList.Add(dataList[1]);
         Assert.IsTrue(lb.SelectedItems.Count == 2);
         CollectionAssert.AreEquivalent(synchroList, lb.SelectedItems);
      }
   }

   public class MyPointConverter : IValueConverter
   {
      #region Implementation of IValueConverter

      /// <summary>
      /// Converts a value. 
      /// </summary>
      /// <returns>
      /// A converted value. If the method returns null, the valid null value is used.
      /// </returns>
      /// <param name="value">The value produced by the binding source.</param><param name="targetType">The type of the binding target property.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         if (targetType != typeof(MyPoint) || !(value is Point)) return null;

         var v = (Point)value;
         return new MyPoint { X = v.X, Y = v.Y };
      }

      /// <summary>
      /// Converts a value. 
      /// </summary>
      /// <returns>
      /// A converted value. If the method returns null, the valid null value is used.
      /// </returns>
      /// <param name="value">The value that is produced by the binding target.</param><param name="targetType">The type to convert to.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
         if (targetType != typeof(Point) || !(value is MyPoint)) return null;

         var v = (MyPoint)value;
         return new Point() { X = v.X, Y = v.Y };
      }

      #endregion
   }

   public class MyPoint
   {
      public double X { get; set; }
      public double Y { get; set; }
   }
}
