﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Desktop.Wpf.AttachedProperties;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Desktop.Wpf.Tests.UnitTests
{
   [TestClass]
   public class ItemsSourceSynchronizerTests
   {
      private ObservableCollection<object> synchroList;

      [TestInitialize]
      public void Init()
      {
         synchroList = new ObservableCollection<object>();
         
      }

      [TestMethod]
      [ExpectedException(typeof(ArgumentException))]
      public void Synchronizer_ThrowsException_OnInvalidSynchroList()
      {
         var tb = new ListBox();
         ItemsSourceSynchronizer.SetItemsSource(tb, new List<object>());
      }

      [TestMethod]
      [ExpectedException(typeof(InvalidOperationException))]
      public void Synchronizer_ThrowsException_OnInvalidDependencyObject()
      {
         var dependencyObject = new Label();
         ItemsSourceSynchronizer.SetItemsSource(dependencyObject, synchroList);
      }

      [TestMethod]
      public void ListBox_CanAttach()
      {
         var tb = new ListBox {ItemsSource = new ObservableCollection<object>()};
         ItemsSourceSynchronizer.SetItemsSource(tb, synchroList);
         Assert.AreSame(synchroList, ItemsSourceSynchronizer.GetItemsSource(tb));
      }


      [TestMethod]
      public void SynchroList_IsSynchronized()
      {
         var lb = new ListBox { SelectionMode = SelectionMode.Multiple };
         var dataList = new ObservableCollection<object>
         {
            new object(),
            new object(),
            new object(),
         };

         lb.ItemsSource = dataList;
         ItemsSourceSynchronizer.SetItemsSource(lb, synchroList);
         CollectionAssert.AreEquivalent(dataList, synchroList);

         dataList.Add(new object());
         CollectionAssert.AreEquivalent(lb.ItemsSource as ICollection, synchroList);
         Assert.IsTrue(synchroList.Count == 4);

         synchroList.Add(new object());
         CollectionAssert.AreEquivalent(synchroList, lb.ItemsSource as ICollection);
         Assert.IsTrue(synchroList.Count == 5);
      }

      [TestMethod]
      public void SynchroList_IsSynchronized_WithSpecificConverter()
      {
         var lb = new ListBox { SelectionMode = SelectionMode.Multiple };
         var converter = new MyPointConverter();
         var syncList = new ObservableCollection<Point>();
         var dataList = new ObservableCollection<MyPoint>
         {
            new MyPoint {X = 2, Y = 5},
            new MyPoint {X = 4, Y = 8},
            new MyPoint {X = 2, Y = 1},
         };

         lb.ItemsSource = dataList;

         ItemsSourceSynchronizer.SetItemsSourceConverter(lb, converter);
         ItemsSourceSynchronizer.SetItemsSource(lb, syncList);

         Assert.IsTrue(dataList.Count == syncList.Count);
         Assert.AreEqual(syncList[0].X, dataList[0].X);
         Assert.AreEqual(syncList[0].Y, dataList[0].Y);

         var s = (Point)converter.ConvertBack(dataList[0], typeof(Point), null, CultureInfo.CurrentCulture);
         syncList.Add(s);
         Assert.IsTrue(dataList.Count == 4);
         CollectionAssert.AreEquivalent(lb.ItemsSource as ICollection, dataList);
         Assert.AreEqual(s.X, dataList[3].X);
         Assert.AreEqual(s.Y, dataList[3].Y);

         dataList.Add(new MyPoint {X = 8, Y = 9});
         Assert.IsTrue(syncList.Count == 5);
         CollectionAssert.AreEquivalent(lb.ItemsSource as ICollection, dataList);
         Assert.AreEqual(dataList[4].X, syncList[4].X);
         Assert.AreEqual(dataList[4].Y, syncList[4].Y);


      }
   }
}
