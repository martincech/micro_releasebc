﻿using System.Threading;
using FTD2XX_NET;

/*
 Mění výstupní úrovně pinů na nultém USB FTDI zařízení
 */

namespace FtdiBitBang.Tests.Program
{
    class Program
    {
        static void Main(string[] args)
        {
            uint written = 0;
            var data = new byte[1];
            var device = new FTDI();
            device.OpenByIndex(0); // Otevři nulté FTDI
            device.SetBitMode(0x00, FTDI.FT_BIT_MODES.FT_BIT_MODE_RESET); // reset
            device.SetBitMode(0xFF, FTDI.FT_BIT_MODES.FT_BIT_MODE_ASYNC_BITBANG); // async bitbang mod, vsechny piny vystup
            while (true) {
                for (var i = 0; i < 8; i++) {
                    data[0] = (byte) (1 << i);
                    device.Write(data, 1, ref written); // zapis vystupni urovne
                    Thread.Sleep(200);
                }
            }
        }
    }
}
