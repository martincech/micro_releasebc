﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities.Extensions;

namespace Utilities.Tests
{
   [TestClass]
   public class CollectionExtensionsTests
   {
      private List<object> c1;
      private List<object> c2;
      private List<object> c3;
      private List<object> c4;
      private List<object> c5;
      private List<object> c6;

      [TestInitialize]
      public void Init()
      {
         var o1 = new object();
         var o2 = new object();
         var o3 = new object();

         c1 = new List<object>
         {
            o1,
            o2,
            o3
         };

         c2 = new List<object>
         {
            o3,
            o2,
            o1
         };
         c3 = c1;
         c4 = new List<object>
         {
            o1,
            o2,
            o3
         };

         c5 = new List<object>
         {
            o1,
            o2
         };
         c6 = new List<object>
         {
            new object()
         };
      }

      [TestMethod]
      public void ContainsCollection()
      {
         Assert.IsTrue(c1.ContainsCollection(c1));
         
         Assert.IsTrue(c1.ContainsCollection(c2));
         Assert.IsTrue(c2.ContainsCollection(c1));

         Assert.IsTrue(c1.ContainsCollection(c4));
         Assert.IsTrue(c4.ContainsCollection(c1));

         Assert.IsTrue(c1.ContainsCollection(c5));
         Assert.IsFalse(c5.ContainsCollection(c1));
         Assert.IsFalse(c1.ContainsCollection(c6));
      }
      [TestMethod]
      public void SequenceEqualsUnOrdered()
      {
         Assert.IsTrue(c1.SequenceEqualsUnOrdered(c1));
         Assert.IsTrue(c1.SequenceEqualsUnOrdered(c2));
         Assert.IsTrue(c2.SequenceEqualsUnOrdered(c1));
         Assert.IsTrue(c1.SequenceEqualsUnOrdered(c3));
         Assert.IsTrue(c3.SequenceEqualsUnOrdered(c4));
         Assert.IsTrue(c1.SequenceEqualsUnOrdered(c4));
         
      }
   }
}
