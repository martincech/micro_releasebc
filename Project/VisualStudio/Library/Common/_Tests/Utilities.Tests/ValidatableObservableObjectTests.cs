﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities.Observable;

namespace Utilities.Tests
{
   [TestClass]
   public class ValidatableObservableObjectTests
   {
      private StubValidatableObservableObject obj;
      private bool raised;

      [TestInitialize]
      public void TestInit()
      {
         obj = new StubValidatableObservableObject();
         raised = false;
      }

      [TestMethod]
      public void RequiredAttributeDataErrorIsRaised()
      {
         obj.ErrorsChanged += (sender, e) =>
         {
            raised = true;
         };

         obj.Validate();
         Assert.IsTrue(raised, "ErrorsChanged never invoked");
      }

      [TestMethod]
      public void MaximumLengthAttributeDataErrorIsRaised()
      {
         obj.ErrorsChanged += (sender, e) =>
         {
            Assert.IsTrue(e.PropertyName == "MaxLengthProperty");
            raised = true;
         };
         obj.MaxLengthProperty = "lasjfůlkasjflkajflkjaslkfd";
         Assert.IsTrue(raised, "ErrorsChanged never invoked");
         Assert.IsTrue(obj.HasErrors);
         var errors = obj.GetErrors("MaxLengthProperty");
         var validationResults = errors as ValidationResult[] ?? errors.ToArray();

         Assert.IsTrue(validationResults.Count() == 1, "More errors returned where single should be");
         Assert.IsTrue(validationResults.Any(p => p.ErrorMessage == "Too lenghty error"));
      }

      [TestMethod]
      public void CustomRulesErrorsDataErrorIsRaised()
      {
         obj.ErrorsChanged += (sender, e) =>
         {
            Assert.IsTrue(e.PropertyName == "CustomValidationProperty");
            raised = true;
         };
         obj.CustomValidationProperty = "";
         Assert.IsTrue(raised, "ErrorsChanged never invoked");
         Assert.IsTrue(obj.HasErrors);
         var errors = obj.GetErrors("CustomValidationProperty");
         var validationResults = errors as ValidationResult[] ?? errors.ToArray();

         Assert.IsTrue(validationResults.Count() == 1, "More errors returned where single should be");
         Assert.IsTrue(validationResults.Any(p => p.ErrorMessage == "Too short error"));
      }

      [TestMethod]
      public void CustomRules_ErrorsRemoved_OnConnectedProperties()
      {
         Assert.IsFalse(obj.HasErrors);
         obj.From = new DateTime(2014, 1, 1, 5, 0, 0);
         obj.To = new DateTime(2014, 1, 1, 4, 0, 0);
         Assert.IsTrue(obj.GetErrors("From").Any());
         Assert.IsTrue(obj.GetErrors("To").Any());
         Assert.IsTrue(obj.HasErrors);

         obj.To = new DateTime(2014, 1, 1, 8, 0, 0);
         Assert.IsFalse(obj.GetErrors("From").Any());
         Assert.IsFalse(obj.GetErrors("To").Any());
         Assert.IsFalse(obj.HasErrors);

         obj.From = new DateTime(2014, 1, 1, 9,0,0);
         Assert.IsTrue(obj.GetErrors("From").Any());
         Assert.IsFalse(obj.GetErrors("To").Any());
         Assert.IsTrue(obj.HasErrors);

         obj.To = new DateTime(2014, 1, 1, 10, 0, 0);
         Assert.IsFalse(obj.GetErrors("From").Any());
         Assert.IsFalse(obj.GetErrors("To").Any());
         Assert.IsFalse(obj.HasErrors);

      }
   }

   internal class StubValidatableObservableObject : ValidatableObservableObject
   {
      private string requiredProperty;
      private string maxLengthProperty;
      private string customValidationProperty;
      private DateTime from;
      private DateTime to;

      [Required]
      public string RequiredProperty
      {
         get { return requiredProperty; }
         set { SetPropertyAndValidate(ref requiredProperty, value); }
      }

      [StringLength(4, ErrorMessage = "Too lenghty error")]
      public string MaxLengthProperty
      {
         get { return maxLengthProperty; }
         set { SetPropertyAndValidate(ref maxLengthProperty, value); }
      }

      public string CustomValidationProperty
      {
         get { return customValidationProperty; }
         set { SetPropertyAndValidate(ref customValidationProperty, value); }
      }

      public DateTime From
      {
         get { return from; }
         set { SetPropertyAndValidate(ref from, value); }
      }

      public DateTime To
      {
         get { return to; }
         set { SetPropertyAndValidate(ref to, value); }
      }

      protected override IEnumerable<ValidationResult> AdditionalValidationRules(object value, string propertyName)
      {
         if (propertyName == "CustomValidationProperty")
         {
            var stringValue = value as string;
            if (string.IsNullOrEmpty(stringValue))
            {
               return new[]
               {
                  new ValidationResult("Too short error", new []{propertyName})
               };
            }
         }

         if ((propertyName == "From" && DateTime.Compare((DateTime)value, To) > 0) ||
             (propertyName == "To" && DateTime.Compare(From, (DateTime)value) > 0))
         {
            return new[]
            {
               new ValidationResult("From time must be befor To time", new[]{"From", "To"})
            };
         }
         return base.AdditionalValidationRules(value, propertyName);
      }
   }
}
