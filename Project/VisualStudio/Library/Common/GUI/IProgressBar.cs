﻿namespace GUI
{
   public interface IProgressBar
   {
      double Value { get; set; }
   }
}
