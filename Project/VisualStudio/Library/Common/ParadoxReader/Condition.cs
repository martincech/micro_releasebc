﻿using System;
using System.Collections;
using ParadoxReader.Db;
using ParadoxReader.Enums;

namespace ParadoxReader
{
   public abstract class Condition
   {
      public abstract bool IsDataOk(Record dataRec);
      public abstract bool IsIndexPossible(Record indexRec, Record nextRec);

      public class Compare : Condition
      {
         public CompareOperator Operator { get; private set; }
         public object Value { get; private set; }

         public int DataFieldIndex { get; private set; }
         public int IndexFieldIndex { get; private set; }

         public override bool IsDataOk(Record dataRec)
         {
            var val = dataRec.DataValues[DataFieldIndex];
            var comp = Comparer.Default.Compare(val, Value);
            switch (Operator)
            {
               case CompareOperator.Equal:
                  return comp == 0;
               case CompareOperator.NotEqual:
                  return comp != 0;
               case CompareOperator.Greater:
                  return comp > 0;
               case CompareOperator.GreaterOrEqual:
                  return comp >= 0;
               case CompareOperator.Less:
                  return comp < 0;
               case CompareOperator.LessOrEqual:
                  return comp <= 0;
               default:
                  throw new NotSupportedException();
            }
         }

         public override bool IsIndexPossible(Record indexRec, Record nextRec)
         {
            var val1 = indexRec.DataValues[DataFieldIndex];
            var comp1 = Comparer.Default.Compare(val1, Value);
            int comp2;
            if (nextRec != null)
            {
               var val2 = nextRec.DataValues[DataFieldIndex];
               comp2 = Comparer.Default.Compare(val2, Value);
            }
            else
            {
               comp2 = 1; // last index range ends in infinite
            }
            switch (Operator)
            {
               case CompareOperator.Equal:
                  return comp1 <= 0 && comp2 >= 0;
               case CompareOperator.NotEqual:
                  return comp1 > 0 || comp2 < 0;
               case CompareOperator.Greater:
                  return comp2 > 0;
               case CompareOperator.GreaterOrEqual:
                  return comp2 >= 0;
               case CompareOperator.Less:
                  return comp1 < 0;
               case CompareOperator.LessOrEqual:
                  return comp1 <= 0;
               default:
                  throw new NotSupportedException();
            }
         }

         public Compare(CompareOperator op, object value, int dataFieldIndex, int indexFieldIndex)
         {
            Operator = op;
            Value = value;
            DataFieldIndex = dataFieldIndex;
            IndexFieldIndex = indexFieldIndex;
         }
      }

      public abstract class Multiple : Condition
      {
         protected Condition[] SubConditions { get; private set; }

         protected Multiple(Condition[] subConditions)
         {
            SubConditions = subConditions;
         }

         public override bool IsDataOk(Record dataRec)
         {
            return Test(c => c.IsDataOk(dataRec));
         }

         public override bool IsIndexPossible(Record indexRec, Record nextRec)
         {
            return Test(c => c.IsIndexPossible(indexRec, nextRec));
         }

         protected abstract bool Test(Predicate<Condition> test);
      }

      public class LogicalAnd : Multiple
      {
         public LogicalAnd(params Condition[] subConditions) : base(subConditions) { }

         protected override bool Test(Predicate<Condition> test)
         {
            foreach (var subCondition in SubConditions)
            {
               if (!test(subCondition)) return false;
            }
            return true;
         }
      }

      public class LogicalOr : Multiple
      {
         public LogicalOr(params Condition[] subConditions) : base(subConditions) { }

         protected override bool Test(Predicate<Condition> test)
         {
            foreach (var subCondition in SubConditions)
            {
               if (test(subCondition)) return true;
            }
            return false;
         }
      }
   }
}
