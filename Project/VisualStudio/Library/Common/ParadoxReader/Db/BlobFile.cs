﻿using System;
using System.IO;

namespace ParadoxReader.Db
{
   internal class BlobFile : IDisposable
   {
      private readonly Stream stream;
      private readonly BinaryReader reader;

      public BlobFile(string fileName)
         : this(new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
      {
      }

      public BlobFile(Stream stream)
      {
         this.stream = stream;
         reader = new BinaryReader(stream);
      }

      public virtual void Dispose()
      {
         stream.Dispose();
      }

      public byte[] ReadBlob(byte[] blobInfo)
      {
         uint OffsetAndIndex = BitConverter.ToUInt32(blobInfo, 0);
         uint index = OffsetAndIndex & 0x000000ff;
         uint Offset = OffsetAndIndex & 0xffffff00;

         int size = BitConverter.ToInt32(blobInfo, 4);
         int hsize = 9;

         int mod_nr = BitConverter.ToInt16(blobInfo, 8);

         if (size > 0)
         {
            //Console.WriteLine("Graphic index={0}; blobsize={1}; mod_nr={2}", index, blobsize, mod_nr);

            stream.Position = Offset;

            byte[] head;
            head = new byte[6];
            reader.Read(head, 0, 3);

            //TODO check for type 2 and index=255

            reader.Read(head, 0, hsize - 3); //Read remaining 6 bytes of header
            int checkSize = BitConverter.ToInt32(head, 0);
            if (checkSize == size)
            {
               byte[] buffer;
               buffer = new byte[size];

               reader.Read(buffer, 0, size);
               return buffer;
            }
         }
         return null;
      }
   }
}
