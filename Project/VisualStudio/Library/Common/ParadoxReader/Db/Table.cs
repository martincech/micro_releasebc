﻿using System;
using System.IO;

namespace ParadoxReader.Db
{
   public class Table : File
   {
      public readonly PrimaryKey PrimaryKeyIndex;
      private readonly BlobFile BlobFile;

      public Table(string dbPath, string tableName)
         : base(Path.Combine(dbPath, tableName + ".db"))
      {
         var files = Directory.GetFiles(dbPath, tableName + "*.*");
         foreach (var file in files)
         {
            if (Path.GetFileName(file) == tableName + ".db") continue; // current file
            if (Path.GetFileNameWithoutExtension(file).EndsWith(".PX", StringComparison.InvariantCultureIgnoreCase) ||
                Path.GetExtension(file).Equals(".PX", StringComparison.InvariantCultureIgnoreCase))
            {
               PrimaryKeyIndex = new PrimaryKey(this, file);
               break;
            }
            if (Path.GetFileNameWithoutExtension(file).EndsWith(".MB", StringComparison.InvariantCultureIgnoreCase) ||
                Path.GetExtension(file).Equals(".MB", StringComparison.InvariantCultureIgnoreCase))
            {
               BlobFile = new BlobFile(file);
            }
         }
      }

      internal override byte[] ReadBlob(byte[] blobInfo)
      {
         if (BlobFile == null)
            return base.ReadBlob(blobInfo);
         return BlobFile.ReadBlob(blobInfo);
      }

      public override void Dispose()
      {
         base.Dispose();
         if (PrimaryKeyIndex != null)
         {
            PrimaryKeyIndex.Dispose();
         }
         if (BlobFile != null)
         {
            BlobFile.Dispose();
         }
      }
   }
}
