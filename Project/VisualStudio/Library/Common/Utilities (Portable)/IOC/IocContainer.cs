﻿using System;

namespace Utilities.IOC
{
   public class IocContainer : IIocContainer
   {
      public static IIocContainer Container { get; set; }

      public static void Register<T>(Func<T> function)
      {
         if (Container == null)
         {
            throw new InvalidOperationException("Container not set to valid IIocContainer instance!");
         }
         Container.Bind(function);
      }

      public static T Resolve<T>()
      {
         return (T)Resolve(typeof(T));
      }

      public static object Resolve(Type type)
      {
         if (Container == null)
         {
            throw new InvalidOperationException("Container not set to valid IIocContainer instance!");
         }
         return Container.Get(type);
      }

      #region Implementation of IIocContainer

      /// <summary>
      /// Register function for creation object of type T
      /// </summary>
      /// <typeparam name="T">type of object to be registered</typeparam>
      /// <param name="function">function which creates an instance of <see cref="T"/></param>
      public void Bind<T>(Func<T> function)
      {
         Register(function);
      }

      /// <summary>
      /// Get object from this container specified by its type.
      /// </summary>
      /// <typeparam name="T">Generic type to get</typeparam>
      /// <returns>object of type T if this type is registered in IOC</returns>
      /// <exception cref="NotSupportedException">when T is not registered in IOC or is unknown</exception>
      public T Get<T>()
      {
         return Resolve<T>();
      }

      /// <summary>
      /// Get object from this container as dynamically specified by its type.
      /// </summary>
      /// <param name="t"><see cref="Type"/> of object to get</param>
      /// <returns>object of <see cref="Type"/> <see cref="t"/> if this type is registered in IOC</returns>
      /// /// <exception cref="NotSupportedException">when <see cref="t"/> is not registered in IOC or is unknown</exception>
      public object Get(Type t)
      {
         return Resolve(t);
      }

      #endregion
   }
}
