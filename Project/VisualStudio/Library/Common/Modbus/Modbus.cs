﻿using Usb.Helpers;
using Utilities;

namespace Modbus {
    public partial class Modbus {
        // kontextova pamet :
        private ModbusPdu.Function lastFunction;
        private ushort lastAddress;
        private int lastCount;
        
        public ushort[] ReadData = new ushort[ModbusPdu.MAX_READ_REGISTERS_COUNT];
        public int ReadDataCount;

        // Chybove kody :
        public enum Error {
            OK,                // bez chyby
            PARITY,            // chyba parity
            OVERRUN,           // chyba prebehu
            TIMEOUT,           // timeout odpovedi
            FRAME,             // chyba ramce (CRC)
            WRONG_ADDRESS,     // odpoved s odlisnou adresou
            WRONG_FUNCTION,    // odpoved s odlisnym kodem funkce
            WRONG_ITEM,        // chyba polozky odpovedi
            IMPLEMENTATION,    // implementacni chyba
            _LAST
        }

        /// <summary>
        /// Error of the last command
        /// </summary>
        public  Error LastError { get { return lastError; } }
        private Error lastError;

        /// <summary>
        /// Exception of the last command
        /// </summary>
        public  ModbusPdu.ExceptionType LastException { get { return lastException; } }
        private ModbusPdu.ExceptionType lastException;

        /// <summary>
        /// Serial port for communication
        /// </summary>
        private ModbusCom modbusCom;

        private ModbusPacket modbusPacket;

        // Komunikacni parametry spolecne pro vsechny vahy
        private int baudRate;
        private Uart.Parity parity;
        private int dataBits;
        private int totalTimeout;
        private int intercharacterTimeout;
        public ModbusPacket.PacketMode Mode;        // Zverejnuju

        private int  portNumber;
        private byte slaveAddress;

        public bool Finished { get { return (bool)(modbusCom.Status == ModbusCom.ComStatus.STOPPED); } }

        public Modbus(int baudRate, Uart.Parity parity, int dataBits, int totalTimeout, int intercharacterTimeout, ModbusPacket.PacketMode mode) {
            // Preberu parametry komunikace
            this.baudRate              = baudRate;
            this.parity                = parity;
            this.dataBits              = dataBits;
            this.totalTimeout          = totalTimeout;
            this.intercharacterTimeout = intercharacterTimeout;
            this.Mode                  = mode;

            // Snuluju cislo portu
            portNumber = -1;
        }

        /// <summary>
        /// Open communication with one slave
        /// </summary>
        /// <param name="portNumber">COM port number</param>
        /// <param name="slaveAddress">Slave address</param>
        public void Open(int portNumber, byte slaveAddress) {
            // Pokud chce pouzit jiny port nez minule, otevru novy port
            // Stejne tak pokud port rucne zavrel, musim ho znovu otevrit
            if (portNumber != this.portNumber || (modbusCom != null && !modbusCom.IsOpen)) {
                Close();
                modbusPacket = new ModbusPacket(portNumber, baudRate, parity, dataBits, totalTimeout, intercharacterTimeout, Mode);
                modbusCom = modbusPacket.ModbusCom;
                this.portNumber = portNumber;       // Zapamatuju si novy port
            }
            
            // Zapamatuju si novou adresu
            this.slaveAddress = slaveAddress;
        }
        
        public void Execute() {
            // Automat protokolu, volat co nejcasteji

            // Snuluji posledni chybu a vyjimku
            lastError = Error.OK;
            lastException = ModbusPdu.ExceptionType.NONE;

            switch (modbusCom.Status) {
                case ModbusCom.ComStatus.STOPPED:
                    // zastaveno - stav po inicializaci
                    break;
                case ModbusCom.ComStatus.RX_RUNNING :
                    // ceka se na data
                    break;                        // ceka na Rx data
                case ModbusCom.ComStatus.RX_PARITY :
                    // paket s chybou parity
                    modbusCom.Status = ModbusCom.ComStatus.STOPPED;
                    lastError = Error.PARITY;
                    break;
                case ModbusCom.ComStatus.RX_OVERRUN :
                    // paket s chybou prebehu
                    modbusCom.Status = ModbusCom.ComStatus.STOPPED;
                    lastError = Error.OVERRUN;
                    break;
                case ModbusCom.ComStatus.RX_DONE :
                    // dokoncen ASCII paket
                    modbusCom.Status = ModbusCom.ComStatus.STOPPED;
                    RxPacket();                   // zpracovani paketu
                    break;
                case ModbusCom.ComStatus.RX_TIMEOUT :
                    // ASCII timeout nebo RTU dokoncen paket
                    modbusCom.Status = ModbusCom.ComStatus.STOPPED;
                    if (modbusCom.DataSize == 0) {
                        lastError = Error.TIMEOUT;
                        break;
                    }
                    if (modbusPacket.IsAscii) {
                        lastError = Error.TIMEOUT;
                        break;                     // timeout
                    }
                    RxPacket();                   // zpracovani paketu
                    break;
                case ModbusCom.ComStatus.TX_RUNNING :
                    break;                        // ceka na dokonceni Tx
                case ModbusCom.ComStatus.TX_DONE :
                    modbusCom.ComRxStart();                 // zahajit prijem
                    break;
                default :
                    break;
           }
        }

        /// <summary>
        /// Close COM port
        /// </summary>
        public void Close() {
            if (modbusCom == null) {
                return;
            }
            modbusCom.Close();
        }

        public void WriteFunction(ModbusPdu.Function Code) {
            // Zapise funkcni kod
            modbusPacket.WriteByte((byte)Code);
            lastFunction = Code;      // uschovej jako kontext
        }

        public void WriteItemAddress( ushort Address) {
            // Zapise adresu veliciny
            modbusPacket.WriteWord( Address);
            lastAddress = Address;
        }

        public void WriteCount(ushort Count) {
            // Zapise pocet velicin
            modbusPacket.WriteWord( Count);
            lastCount = Count;
        }

        private void RxPacket() {
            // Zpracuje prijaty paket
            int         Size;
            byte        Address;
            ModbusPdu.Function  FunctionCode;
            byte[] array = new byte[ModbusConst.MAX_PDU_SIZE];

            if (!modbusPacket.Receive(out array, out Size)) {
                lastError = Error.FRAME;
                return;
            }

            // Pole bajtu prevedu na union PDU
            ModbusPdu.PduReply Pdu = StructConversion.ArrayToStruct<ModbusPdu.PduReply>(array);

            // Struktura TReadRegistersReply obsahuje pole, proto nemuze byt soucasti unionu.
            // Tuto strukturu musim resit zvlast
            ModbusPdu.ReadRegistersReply ReadRegisters;
            ReadRegisters.Data = new ushort[ModbusPdu.MAX_READ_REGISTERS_COUNT];
            ReadRegisters = StructConversion.ArrayToStruct<ModbusPdu.ReadRegistersReply>(array);

            Address = modbusPacket.GetAddress();
            if (Address != slaveAddress) {
                lastError = Error.WRONG_ADDRESS;
                return;                          // cizi paket
            }
            FunctionCode = (ModbusPdu.Function)Pdu.Function;
            if (((byte)FunctionCode & 0x80) == 0x80) {
                // exception
                lastException = (ModbusPdu.ExceptionType)Pdu.Exception.Code;
                return;
            }
            if (FunctionCode != lastFunction) {
                lastError = Error.WRONG_FUNCTION;
                return;                          // kod funkce odlisny od pozadavku
            }
            //------------------------------------------------------------------------------
            switch (FunctionCode) {
                case ModbusPdu.Function.READ_HOLDING_REGISTERS :
                    if (ReadRegisters.ByteCount != 2 * lastCount) {
                        lastError = Error.IMPLEMENTATION;
                        break;
                    }
                    if (Size != ModbusPdu.SizeofReadRegistersReply + ReadRegisters.ByteCount) {
                        lastError = Error.IMPLEMENTATION;
                        break;
                    }
                    ReadDataCount = lastCount;
                    for (int i = 0; i < ReadDataCount; i++) {
                        ReadData[i] = ReadRegisters.Data[i];
                    }
                    break;

                case ModbusPdu.Function.WRITE_SINGLE_REGISTER :
                    if (Size != ModbusPdu.SizeofWriteSingleRegister) {
                        lastError = Error.IMPLEMENTATION;
                        break;
                    }
                    if (lastAddress != Endian.SwapUInt16(Pdu.WriteSingleRegister.Address)) {
                        lastError = Error.WRONG_ITEM;
                        break;
                    }
                    lastError = Error.OK;
                    break;

                case ModbusPdu.Function.WRITE_MULTIPLE_REGISTERS :
                    if (Size != ModbusPdu.SizeofWriteRegistersReply()) {
                        lastError = Error.IMPLEMENTATION;
                        break;
                    }
                    if (lastAddress != Endian.SwapUInt16( Pdu.WriteRegisters.Address)) {
                        lastError = Error.WRONG_ITEM;
                        break;
                    }
                    if (lastCount != Endian.SwapUInt16( Pdu.WriteRegisters.Count)) {
                        lastError = Error.WRONG_ITEM;
                        break;
                    }
                    lastError = Error.OK;
                    break;

                //------------------------------------------------------------------------------
                default :
                    lastError = Error.IMPLEMENTATION;
                    break;
            }
        }

    }
}
