﻿using Usb.Helpers;

namespace Modbus {
    public class ModbusPacket {

        // rezimy paketove urovne :
        public enum PacketMode {
            ASCII,                    // ASCII mode
            RTU,                      // RTU mode
            _LAST
        }
         
        public ModbusCom ModbusCom { get { return modbusCom; } }
        private ModbusCom modbusCom;
        private ModbusAscii modbusAscii;
        private ModbusRtu modbusRtu;

        public  bool IsAscii { get { return isAscii; } }
        private bool isAscii;
        
        public ModbusPacket(int portNumber, int baudRate, Uart.Parity parity, int dataBits, int totalTimeout, int intercharacterTimeout, PacketMode mode) {
            // Nastavi rezim paketove urovne
            isAscii = (bool)(mode == PacketMode.ASCII);
            if (IsAscii) {
                modbusCom   = new ModbusCom(portNumber, baudRate, parity, dataBits, (byte)(ModbusCom.ComMode.TIMEOUT | ModbusCom.ComMode.TRAILER | ModbusCom.ComMode.LEADER));
                modbusAscii = new ModbusAscii(modbusCom);
            } else {
                modbusCom = new ModbusCom(portNumber, baudRate, parity, dataBits, (byte)(ModbusCom.ComMode.TIMEOUT));
                modbusRtu = new ModbusRtu(modbusCom);
            }
            modbusCom.SetTimeouts(totalTimeout, intercharacterTimeout);
        }

        public bool Receive(out byte[] Pdu, out int Size) {
            // Dekoduje prijaty paket, vraci ukazatel na zacatek <Pdu> a velikost dat <Size>
            if (IsAscii) {
                return modbusAscii.Receive(out Pdu, out Size);
            } else {
                return modbusRtu.Receive(out Pdu, out Size);
            }
        }

        public byte GetAddress() {
            // Vrati adresu paketu. POZOR, volat az po Receive
            if (IsAscii) {
                return modbusAscii.GetAddress();
            } else {
                return modbusRtu.GetAddress();
            }
        }

        public void WriteAddress(byte Address) {
            // Zapise adresu paketu
            if (IsAscii) {
                modbusAscii.WriteAddress( Address);
            } else {
                modbusRtu.WriteAddress( Address);
            }
        }

        public void WriteByte( byte Data) {
            // Zapise 8bitove slovo do paketu
            if( IsAscii){
                modbusAscii.WriteByte( Data);
            } else {
                modbusRtu.WriteByte( Data);
            }
        }

        public void WriteWord(ushort Data) {
            // Zapise 16bitove slovo do paketu
            if( IsAscii){
                modbusAscii.WriteWord( Data);
            } else {
                modbusRtu.WriteWord( Data);
            }
        }

        public void WriteCrc() {
            // Spocita a zapise zabezpeceni
            if( IsAscii){
                modbusAscii.WriteCrc();
            } else {
                modbusRtu.WriteCrc();
            }
        }
    }
}
