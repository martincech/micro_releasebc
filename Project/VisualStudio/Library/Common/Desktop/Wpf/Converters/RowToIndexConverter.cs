﻿using System;
using System.Windows.Controls;
using System.Windows.Data;

namespace Desktop.Wpf.Converters
{
   public class RowToIndexConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if (value is DataGridRow)
         {
            return (value as DataGridRow).GetIndex() + 1;
         }
         
         return -1;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new InvalidOperationException(string.Format("Invalid use of {0} converter!", typeof (RowToIndexConverter).Name));
      }
   }
}
