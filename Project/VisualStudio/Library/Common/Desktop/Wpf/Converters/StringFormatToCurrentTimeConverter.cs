﻿using System;
using System.Windows.Data;

namespace Desktop.Wpf.Converters
{
   public class StringFormatToCurrentTimeConverter : IValueConverter, IMultiValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if( value == null || value.GetType() != typeof(String))
         {
            return null;
         }

         if (parameter == null || parameter.GetType() != typeof(DateTime))
         {
            return DateTime.Now.ToString(value.ToString());
         }
         return ((DateTime)parameter).ToString(value.ToString());
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }

      public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if (values.Length < 2)
         {
            return null;
         }
         return Convert(values[1], targetType, values[0], culture);
      }

      public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
