﻿using System;
using System.Windows.Data;

namespace Desktop.Wpf.Converters
{
   public class IntToDecimalDoubleConverter : IValueConverter, IMultiValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         int from;
         if (int.TryParse(value.ToString(), out from))
         {
            int decimals;
            if (!int.TryParse(parameter.ToString(), out decimals))
            {
               decimals = 0;
            }            
            return from / Math.Pow(10, decimals);
         }
         return 0;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         double from;
         if (double.TryParse(value.ToString(), out from))
         {
            int decimals;
            if( !int.TryParse(parameter.ToString(), out decimals))
            {
               decimals = 0;
            }
            int? res = (int)(from * Math.Pow(10, decimals));

            if (targetType == typeof(int?))
            {
               return res;
            }
            return res.Value;
         }
         return 0;
      }

      public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         if( values.Length < 2)
         {
            return (double)0;
         }
         return Convert(values[0], targetType, values[1], culture);
      }

      public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
         System.Globalization.CultureInfo culture)
      {
         var retList = new object[2];
         var val = value as double?;
         if( !val.HasValue){ val = 1;}
         //get decimal part to know the power for conversion
         var trunc = Math.Truncate(val.Value);
         var dec = val.Value - trunc;
         var pow = 0;
         while (dec > 0 && dec < 1)
         {
            pow++;
            dec *= 10;
            trunc = Math.Truncate(dec);
            dec = dec - trunc;
         }
         
         retList[0] = ConvertBack(value, targetTypes[0], pow, culture);
         retList[1] = pow;
         return retList;
      }
   }
}
