﻿using System;
using System.Windows.Controls;
using System.Windows.Data;

namespace Desktop.Wpf.Converters
{
   public class TabIndexConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         var msg = "";
         if (parameter != null)
         {
            msg = parameter + " ";
         }

         var tabItem = value as TabItem;
         if (tabItem == null)
         {
            return null;
         }

         var index = ItemsControl.ItemsControlFromItemContainer(tabItem)
             .ItemContainerGenerator.IndexFromContainer(tabItem) + 1;

         return msg + index;
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         throw new NotImplementedException();
      }
   }
}
