﻿using System;
using System.Windows;
using GUI;

namespace Desktop.Wpf.Presentation
{
   public class ProgressBar : System.Windows.Controls.ProgressBar, IProgressBar, IView
   {

      public new double Value
      {
         get { return base.Value; }
         set { DispatcherHelper.RunAsync(() => { base.Value = value; }); }
      }

      #region Implementation of IView

      /// <summary>
      /// Show this view
      /// </summary>
      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      /// <summary>
      /// Hide this view
      /// </summary>
      public void Hide()
      {
         Visibility = Visibility.Hidden;
      }

      #endregion
   }
}
