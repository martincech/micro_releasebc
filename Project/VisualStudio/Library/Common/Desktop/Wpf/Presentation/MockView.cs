﻿using System.Windows.Threading;

namespace Desktop.Wpf.Presentation
{
   /// <summary>
   /// Dummy view as dummy implementation of basic <see cref="IView"/>
   /// </summary>
   public abstract class MockView : IView
   {
      #region Implementation of IView

      /// <summary>
      /// Gets or sets the data context of the view.
      /// This will be set to ViewModel of the view.
      /// </summary>
      public object DataContext { get; set; }

      /// <summary>
      /// Show this view
      /// </summary>
      public void Show()
      {
      }

      /// <summary>
      /// Hide this view
      /// </summary>
      public void Hide()
      {
      }

      #endregion
   }
}