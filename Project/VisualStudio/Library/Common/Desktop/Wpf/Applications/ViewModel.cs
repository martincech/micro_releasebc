﻿#if NET45
using System.Collections.Generic;
using System.Linq;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Desktop.Wpf.Applications
{
   /// <summary>
   /// ViewModel class, represent ViewModel with single or multiple Views of type <see cref="IView"/>
   /// </summary>
   public abstract class ViewModel : ValidatableObservableObject
   {
      #region Private fields

      private IView view;

      #endregion

      #region Constructors

      /// <summary>
      /// Initializes a new instance of the <see cref="ViewModel"/> class for single view and attaches itself as <c>DataContext</c> to the view.
      /// </summary>
      /// <param name="view">The view which will have this ViewModel attached as <c>DataContext</c>.</param>
      protected ViewModel(IView view = null)
         : this(new IView[] {}, view)
      {
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="ViewModel"/> class for multiple views and attaches itself as <c>DataContext</c> to those views.
      /// </summary>
      /// <param name="views">Views which will have this ViewModel attached as <c>DataContext</c>.</param>
      /// <param name="activeView">Active(visible) view from all attached views</param>
      protected ViewModel(IEnumerable<IView> views, IView activeView = null)
      {
         Views = new List<IView>();
         ViewsAddAll(views);
         View = activeView;
      }

      #endregion

      /// <summary>
      /// Associated views.
      /// </summary>
      public IEnumerable<IView> Views { get; private set; }

      /// <summary>
      /// Associated active view
      /// </summary>
      public IView View
      {
         get { return view; }
         set
         {
            ViewsAddAll(new []{value});
            SetProperty(ref view, value);
         }
      }

      #region Private helpers

      private void ViewsAddAll(IEnumerable<IView> views)
      {
         var list = new List<IView>(views);
         list.RemoveAll(v => v == null || Views.Contains(v));
         ((List<IView>) Views).AddRange(list.Distinct());
         foreach (var v in Views)
         {
            v.DataContext = this;
         }
      }

      #endregion


   }
}
#endif