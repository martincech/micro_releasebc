#pragma once

namespace Native
{
   using namespace System;
   using namespace System::Threading;

   /// <summary>
   /// Convenient class for synchronization purposes. Use as local variable to lock some section.
   /// Same efect as using lock Statement  in C#
   /// </summary>
   public ref class Lock
   {
   private:
      /// <summary>
      /// Internal locked object
      /// </summary>
      Object ^lockObject;
      static Object ^staticLockObject = gcnew Object();

   public:
      /// <summary>
      /// Create lock on implicit object - static <see cref="LockObject"/>
      /// </summary>
      Lock() : Lock(LockObject)
      {
      }

      /// <summary>
      /// Create lock on explicit object
      /// </summary>
      /// <param name="pObject"></param>
      Lock(Object ^pObject);

      /// <summary>
      /// Release lock
      /// </summary>
      ~Lock() {
         Monitor::Exit(lockObject);
      }

      /// <summary>
      /// 
      /// </summary>
      static property Object ^LockObject{ Object ^get(){ return staticLockObject; } }
         
   };
}
   
