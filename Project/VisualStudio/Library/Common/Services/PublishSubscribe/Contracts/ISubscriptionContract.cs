﻿using System.ServiceModel;

namespace Services.PublishSubscribe.Contracts
{
   /// <summary>
   /// Specifies contract for general event subscribtion/unsubscribtion
   /// Subscribtion is not persistent when application ends.
   /// </summary>
   [ServiceContract]
   public interface ISubscriptionContract
   {
      /// <summary>
      /// Subscribe for all events
      /// </summary>
      [OperationContract]
      void SubscribeAll();

      /// <summary>
      /// Subscribe for concrete event
      /// </summary>
      /// <param name="eventOperation">event operation name</param>
      [OperationContract]
      void Subscribe(string eventOperation);

      /// <summary>
      /// Unsubscribe for concrete event
      /// </summary>
      /// <param name="eventOperation">event operation name</param>
      [OperationContract]
      void Unsubscribe(string eventOperation);

      /// <summary>
      /// Unsubscribe for all events
      /// </summary>
      [OperationContract]
      void UnsubscribeAll();
   }
}