﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using Utilities.Extensions;

namespace Services.PublishSubscribe
{
   /// <summary>
   /// Publish service, for event firing on every connected subscriber.
   /// </summary>
   /// <typeparam name="T">Type of event callback service</typeparam>
   public abstract class PublishService<T> where T : class
   {
      /// <summary>
      /// Create publish service
      /// </summary>
      protected PublishService()
      {
         Debug.Assert(this is T, string.Format("PublishService<{0}>: Failed to inherit from {0}", typeof(T).Name));
      }
      /// <summary>
      /// Fires specific event baset on incomming call (from <see cref="OperationContext"/>)
      /// for every subscribted client
      /// </summary>
      /// <param name="args">parameters of method to fire</param>
      protected void FireEvent(params object[] args)
      {
         var action = OperationContext.Current.IncomingMessageHeaders.Action;
         var slashes = action.Split('/');
         var methodName = slashes[slashes.Length - 1];

         FireEvent(methodName, args);
      }

      #region Private

      /// <summary>
      /// Fires event for specific method
      /// </summary>
      /// <param name="methodName">name of method</param>
      /// <param name="args">parameters of method</param>
      private static void FireEvent(string methodName, object[] args)
      {
         PublishPersistent(methodName, args);
         PublishTransient(methodName, args);
      }

      /// <summary>
      /// Publish for persistent subscribers
      /// </summary>
      /// <param name="methodName">name of method</param>
      /// <param name="args">parameters of method</param>
      private static void PublishPersistent(string methodName, object[] args)
      {
         var subscribers = SubscriptionManager<T>.GetPersistentList(methodName);
         Publish(subscribers, true, methodName, args);
      }

      /// <summary>
      /// Publish for transient subscribers
      /// </summary>
      /// <param name="methodName">name of method</param>
      /// <param name="args">parameters of method</param>
      private static void PublishTransient(string methodName, object[] args)
      {
         var subscribers = SubscriptionManager<T>.GetTransientList(methodName);
         Publish(subscribers, false, methodName, args);
      }

      /// <summary>
      /// Common publish routine
      /// </summary>
      /// <param name="subscribers">subscribers to publish to</param>
      /// <param name="persistentSubscribers">whether close subscribers after publishing</param>
      /// <param name="methodName">name of method</param>
      /// <param name="args">parameters of method</param>
      private static void Publish(IEnumerable<T> subscribers, bool persistentSubscribers, string methodName, object[] args)
      {
         // start new thread for every subscriber
         Action<T> queueUp = sub =>
            ThreadPool.QueueUserWorkItem(
               subscriber =>
               {
                  try
                  {
                     Invoke(subscriber as T, methodName, args);
                     if (!persistentSubscribers) return;
                     using (subscriber as IDisposable)
                     {
                     }
                  }
                  catch (Exception ex)
                  {
                     // subscriber don't live anymore - unsubscribe him
                     if (ex is TargetInvocationException && !persistentSubscribers)
                     {
                        SubscriptionManager<T>.RemoveTransient(subscriber as T, methodName);
                     }
                  }
               },
               sub);

         subscribers.ForEach(queueUp);
      }

      /// <summary>
      /// Actual method invocation for single subscriber
      /// </summary>
      /// <param name="subscriber">subscriber to be communicated with</param>
      /// <param name="methodName">name of method</param>
      /// <param name="args">parameters of method</param>
      private static void Invoke(T subscriber, string methodName, object[] args)
      {
         Debug.Assert(subscriber != null);
         var type = typeof (T);
         var methodInfo = type.GetMethodR(methodName);
         Debug.Assert(methodInfo != null);
         methodInfo.Invoke(subscriber, args);
      }

      #endregion
   }
}