﻿using System.Data.Entity;

namespace Services.PublishSubscribe
{
   public class PublishSubscribeDataContext : DbContext
   {
      public PublishSubscribeDataContext()
         : base("name=PublishSubscribeDataContext")
      {
      }

      protected override void OnModelCreating(DbModelBuilder modelBuilder)
      {
         //throw new UnintentionalCodeFirstException();
      }

      public virtual DbSet<Subscriber> Subscribers { get; set; }
   }
}
