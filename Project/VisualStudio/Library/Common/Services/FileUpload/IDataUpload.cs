﻿using System.ServiceModel;

namespace Services.FileUpload
{
   [ServiceContract]
   public interface IDataUpload
   {
      [OperationContract]
      StatusInfo LoadFile(RemoteFileInfo fileInfo);
   }
}