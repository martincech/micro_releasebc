﻿using System.ServiceModel;

namespace Services.FileUpload
{
   [ServiceContract]
   public interface IProtectedDataUpload
   {
      [OperationContract]
      StatusInfo LoadFile(ProtectedRemoteFileInfo fileInfo);
   }
}