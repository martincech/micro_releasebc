﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml;

namespace Services.DataContracts
{
   /// <summary>
   /// Resolver for data types - no need for setting <see cref="KnownTypes"/> attribute when using this resolver.
   /// </summary>
   public class GenericResolver : DataContractResolver
   {
      #region Private fields

      private const string DEFAULT_NAMESPACE = "global";

      private readonly Dictionary<Type, Tuple<string, string>> mTypeToNames;
      private readonly Dictionary<string, Dictionary<string, Type>> mNamesToType;

      #endregion

      #region Public access

      /// <summary>
      /// All known types
      /// </summary>
      public Type[] KnownTypes
      {
         get { return mTypeToNames.Keys.ToArray(); }
      }

      /// <summary>
      /// Default constructor, all classes and structs in the calling assembly plus the public classes and structs in referenced assemblies will be added
      /// </summary>
      public GenericResolver()
         : this(ReflectTypes())
      {
      }

      /// <summary>
      /// Using this constructor only specific types can be resolved.
      /// </summary>
      /// <param name="typesToResolve"><see cref="Type"/>s to be resolved</param>
      public GenericResolver(IEnumerable<Type> typesToResolve)
      {
         mTypeToNames = new Dictionary<Type, Tuple<string, string>>();
         mNamesToType = new Dictionary<string, Dictionary<string, Type>>();

         foreach (var type in typesToResolve)
         {
            var typeNamespace = GetNamespace(type);
            var typeName = GetName(type);

            mTypeToNames[type] = new Tuple<string, string>(typeNamespace, typeName);

            if (mNamesToType.ContainsKey(typeNamespace) == false)
            {
               mNamesToType[typeNamespace] = new Dictionary<string, Type>();
            }
            mNamesToType[typeNamespace][typeName] = type;
         }
      }

      /// <summary>
      /// Merges types from two resolver into one common resolver.
      /// </summary>
      /// <param name="resolver1">first resolver to be merged</param>
      /// <param name="resolver2">second resolver to be merged</param>
      /// <returns></returns>
      public static GenericResolver Merge(GenericResolver resolver1, GenericResolver resolver2)
      {
         if (resolver1 == null)
         {
            return resolver2;
         }
         if (resolver2 == null)
         {
            return resolver1;
         }
         var types = new List<Type>();
         types.AddRange(resolver1.KnownTypes);
         types.AddRange(resolver2.KnownTypes);

         return new GenericResolver(types.ToArray());
      }

      #region DataContractResolver overrides

      public override Type ResolveName(string typeName, string typeNamespace, Type declaredType,
         DataContractResolver knownTypeResolver)
      {
         if (mNamesToType.ContainsKey(typeNamespace))
         {
            if (mNamesToType[typeNamespace].ContainsKey(typeName))
            {
               return mNamesToType[typeNamespace][typeName];
            }
         }
         return knownTypeResolver.ResolveName(typeName, typeNamespace, declaredType, null);
      }

      public override bool TryResolveType(Type type, Type declaredType, DataContractResolver knownTypeResolver,
         out XmlDictionaryString typeName, out XmlDictionaryString typeNamespace)
      {
         if (mTypeToNames.ContainsKey(type))
         {
            var dictionary = new XmlDictionary();
            typeNamespace = dictionary.Add(mTypeToNames[type].Item1);
            typeName = dictionary.Add(mTypeToNames[type].Item2);

            return true;
         }
         return knownTypeResolver.TryResolveType(type, declaredType, null, out typeName, out typeNamespace);
      }

      #endregion

      #endregion

      #region Private static helpers

      private static string GetNamespace(Type type)
      {
         return type.Namespace ?? DEFAULT_NAMESPACE;
      }

      private static string GetName(Type type)
      {
         return type.Name;
      }

      private static IEnumerable<Type> ReflectTypes()
      {
         var assemblyReferences = GetCustomReferencedAssemblies();

         var types = new List<Type>();

         foreach (var typesInReferencedAssembly in assemblyReferences.Select(assembly => GetTypes(assembly)))
         {
            types.AddRange(typesInReferencedAssembly);
         }

         if (GenericResolverInstaller.CallingAssembly.FullName != typeof (ServiceHost).Assembly.FullName &&
             GenericResolverInstaller.CallingAssembly.FullName != typeof (GenericResolverInstaller).Assembly.FullName)
         {
            var typesInCallingAssembly = GetTypes(GenericResolverInstaller.CallingAssembly, false);
            types.AddRange(typesInCallingAssembly);
         }
         if (Assembly.GetEntryAssembly() != null)
         {
            if (Assembly.GetEntryAssembly().FullName != GenericResolverInstaller.CallingAssembly.FullName)
            {
               var typesInEntryAssembly = GetTypes(Assembly.GetEntryAssembly(), false);
               types.AddRange(typesInEntryAssembly);
            }
         }
         else if (GenericResolverInstaller.IsWebProcess())
         {
            foreach (Assembly assembly in GenericResolverInstaller.GetWebAssemblies())
            {
               var typesInWebAssembly = GetTypes(assembly, false);
               types.AddRange(typesInWebAssembly);
            }
         }
         return types.ToArray();
      }

      private static IEnumerable<Assembly> GetCustomReferencedAssemblies()
      {
         var assemblies = new List<Assembly>();

         if (GenericResolverInstaller.CallingAssembly.FullName != typeof (ServiceHost).Assembly.FullName &&
             GenericResolverInstaller.CallingAssembly.FullName != typeof (GenericResolverInstaller).Assembly.FullName)
         {
            assemblies.AddRange(GetCustomReferencedAssemblies(GenericResolverInstaller.CallingAssembly));
         }
         if (Assembly.GetEntryAssembly() != null)
         {
            if (Assembly.GetEntryAssembly().FullName != GenericResolverInstaller.CallingAssembly.FullName)
            {
               assemblies.AddRange(GetCustomReferencedAssemblies(Assembly.GetEntryAssembly()));
            }
         }
         else if (GenericResolverInstaller.IsWebProcess())
         {
            foreach (Assembly assembly in GenericResolverInstaller.GetWebAssemblies())
            {
               assemblies.AddRange(GetCustomReferencedAssemblies(assembly));
            }
         }
         //else
         //{
         //   //executing assembly
         //}
         return assemblies.ToArray();
      }

      private static IEnumerable<Assembly> GetCustomReferencedAssemblies(Assembly assembly)
      {
         Debug.Assert(assembly != null);

         var referencedAssemblies = assembly.GetReferencedAssemblies();

         const string dotNetKeyToken1 = "b77a5c561934e089";
         const string dotNetKeyToken2 = "b03f5f7f11d50a3a";
         const string dotNetKeyToken3 = "31bf3856ad364e35";

         var assemblies = new List<Assembly>();
         foreach (var assemblyName in referencedAssemblies)
         {
            var keyToken = assemblyName.FullName.Split('=')[3];

            var serviceNModelEx = Assembly.GetCallingAssembly();
            var serviceNModelExKeyToken = serviceNModelEx.GetName().FullName.Split('=')[3];
            if (keyToken == serviceNModelExKeyToken)
            {
               continue;
            }
            switch (keyToken)
            {
               case dotNetKeyToken1:
               case dotNetKeyToken2:
               case dotNetKeyToken3:
               {
                  continue;
               }
            }
            assemblies.Add(Assembly.Load(assemblyName));
         }
         return assemblies.ToArray();
      }

      private static Type[] GetTypes(Assembly assembly, bool publicOnly = true)
      {
         var allTypes = assembly.GetTypes();

         var types = new List<Type>();

         foreach (var type in allTypes)
         {
            if (type.IsEnum == false &&
                type.IsInterface == false &&
                type.IsGenericTypeDefinition == false)
            {
               if (publicOnly && type.IsPublic == false)
               {
                  if (type.IsNested == false)
                  {
                     continue;
                  }
                  if (type.IsNestedPrivate)
                  {
                     continue;
                  }
               }
               types.Add(type);
            }
         }
         return types.ToArray();
      }

      #endregion
   }
}