﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Runtime.CompilerServices;
using System.Threading;
using Usb.FTDI;
using Usb.HID;
using Usb.MODEM;
using Usb.UMS;
using Usb.ELO;

namespace Usb.Platform.Windows
{
   /// <summary>
   ///    Watches system resource for available HID devices and informs
   ///    about them.
   /// </summary>
   internal class WinUsbDeviceManager : UsbDeviceManager, IPlatformSupport
   {
      #region Private fields

      private List<UsbDevice> connectedDevices;
      private ManagementEventWatcher deviceAttachWatcher;
      private ManagementEventWatcher deviceDetachWatcher;

      #endregion

      #region Public interface

      public bool IsSupported
      {
         get { return WinSystem.IsSupported(); }
      }

      #region Constructors

      /// <summary>
      ///    Initializes a new instance of the <see cref="T:System.Object" /> class.
      /// </summary>
      internal WinUsbDeviceManager()
      {
         // all Plug and Play devices attaching/detaching is reported
         // Add USB plugged event watching
         deviceAttachWatcher = new ManagementEventWatcher();
         deviceAttachWatcher.EventArrived += Attaching;
         // TODO compatibility issue, add compatibility for other systems
         deviceAttachWatcher.Query = new WqlEventQuery("SELECT * FROM __InstanceCreationEvent " +
                                                       "WITHIN 2 WHERE TargetInstance ISA 'Win32_USBControllerDevice'");

         // Add USB unplugged event watching
         deviceDetachWatcher = new ManagementEventWatcher();
         deviceDetachWatcher.EventArrived += Detaching;
         deviceDetachWatcher.Query = new WqlEventQuery("SELECT * FROM __InstanceDeletionEvent " +
                                                       "WITHIN 2 WHERE TargetInstance ISA 'Win32_USBControllerDevice'");
         ThreadPool.QueueUserWorkItem(param =>
         {
            deviceAttachWatcher.Start();
            deviceDetachWatcher.Start();
            AddDevice();
         });
      }

      /// <summary>
      ///    IDisposable implementation
      /// </summary>
      public override void Dispose()
      {
         if (deviceAttachWatcher == null || deviceDetachWatcher == null) return;
         lock (typeof (WinUsbDeviceManager))
         {
            deviceAttachWatcher.Stop();
            deviceDetachWatcher.Stop();
            deviceAttachWatcher.Dispose();
            deviceDetachWatcher.Dispose();
            deviceAttachWatcher = null;
            deviceDetachWatcher = null;
            ClearDevices();
         }
      }

      #endregion

      /// <summary>
      ///    List of connected usb devices.
      /// </summary>
      public override IEnumerable<UsbDevice> ConnectedDevices
      {
         get
         {
            lock (this)
            {
               if (connectedDevices != null) return connectedDevices;
               AddDevice();
               return connectedDevices;
            }
         }
      }

      /// <summary>
      ///    Invoked when some unknown device connected
      /// </summary>
      public override event EventHandler<UsbDevice> DeviceConnected;

      /// <summary>
      ///    Invoked when some known device disconnected
      /// </summary>
      public override event EventHandler<UsbDevice> DeviceDisconnected;

      #endregion

      #region Private methods

      private static IEnumerable<T> Devices<T>()
         where T : UsbDevice
      {
         if (typeof (T) == typeof (UmsDevice))
         {
            var umsLoader = new UmsDeviceLoader();
            return (IEnumerable<T>) umsLoader.GetDevices();
         }
         if (typeof (T) == typeof (FTDIDevice))
         {
            var ftdiLoader = new FTDIDeviceLoader();
            return (IEnumerable<T>) ftdiLoader.GetDevices();
         }
         if (typeof (T) == typeof (HidDevice))
         {
            var loader = new HidDeviceLoader();
            return (IEnumerable<T>) loader.GetDevices();
         }
         if (typeof(T) == typeof(ModemDevice))
         {
            var modemLoader = new ModemDeviceLoader();
            return (IEnumerable<T>)modemLoader.GetDevices();
         }
         if (typeof(T) == typeof(EloDevice))
         {
            var eloLoader = new EloDeviceLoader();
            return (IEnumerable<T>)eloLoader.GetDevices();
         }
         return null;
      }

      private void AddDevices<T>(CountdownEvent waitEvent = null)
         where T : UsbDevice
      {
         ThreadPool.QueueUserWorkItem(param =>
         {
            // hid devices
            IEnumerable<UsbDevice> existingDevices;
            lock (connectedDevices)
            {
               existingDevices = connectedDevices.Where(d => d is T);
            }
            var devices = Devices<T>();
            var newDevices = devices.Except(existingDevices);
            foreach (var newDevice in newDevices)
            {
               Debug.WriteLine("{0}: {1}", typeof (T).Name, newDevice);
               lock (connectedDevices)
               {
                  connectedDevices.Add(newDevice);
                  OnDeviceConnected(newDevice);
               }
            }
            if (waitEvent != null) waitEvent.Signal();
         });
      }

      private void RemoveDevices<T>(CountdownEvent waitEvent = null)
         where T : UsbDevice
      {
         ThreadPool.QueueUserWorkItem(param =>
         {
            var devices = Devices<T>();
            IEnumerable<UsbDevice> removedDevices;
            lock (connectedDevices)
            {
               removedDevices = connectedDevices.Where(d => d is T).Except(devices).ToArray();
            }

            foreach (var removedDevice in removedDevices)
            {
               Debug.WriteLine("{0}: {1}", typeof (T).Name, removedDevice);
               lock (connectedDevices)
               {
                  OnDeviceDisconnected(removedDevice);
                  connectedDevices.Remove(removedDevice);
               }
            }
            if (waitEvent != null) waitEvent.Signal();
         });
      }

      protected virtual void OnDeviceDisconnected(UsbDevice e)
      {
         var handler = DeviceDisconnected;
         if (handler != null) handler(this, e);
      }

      protected virtual void OnDeviceConnected(UsbDevice e)
      {
         var handler = DeviceConnected;
         if (handler != null) handler(this, e);
      }

      [MethodImpl(MethodImplOptions.Synchronized)]
      private void AddDevice()
      {
         if (connectedDevices == null)
         {
            Debug.WriteLine("/===============================");
            Debug.WriteLine("Existing devices");
            connectedDevices = new List<UsbDevice>();
         }

         var waitEvent = new CountdownEvent(5);
         AddDevices<HidDevice>(waitEvent);
         AddDevices<FTDIDevice>(waitEvent);
         AddDevices<UmsDevice>(waitEvent);
         AddDevices<ModemDevice>(waitEvent);
         AddDevices<EloDevice>(waitEvent);
         waitEvent.Wait();
      }

      [MethodImpl(MethodImplOptions.Synchronized)]
      private void RemoveDevice()
      {
         var waitEvent = new CountdownEvent(5);
         RemoveDevices<HidDevice>(waitEvent);
         RemoveDevices<FTDIDevice>(waitEvent);
         RemoveDevices<UmsDevice>(waitEvent);
         RemoveDevices<ModemDevice>(waitEvent);
         RemoveDevices<EloDevice>(waitEvent);
         waitEvent.Wait();
      }

      [MethodImpl(MethodImplOptions.Synchronized)]
      private void ClearDevices()
      {
         if (connectedDevices == null)
         {
            return;
         }
         lock (connectedDevices)
         {
            while (connectedDevices.Any())
            {
               OnDeviceDisconnected(connectedDevices[0]);
               connectedDevices.RemoveAt(0);
            }
         }
      }

      private void Attaching(object sender, EventArrivedEventArgs e)
      {
         if (sender != deviceAttachWatcher) return;
         ThreadPool.QueueUserWorkItem(param =>
         {
            Debug.WriteLine("/===============================");
            Debug.WriteLine("Attaching");
            AddDevice();
            Debug.WriteLine("===============================/");
            Debug.Flush();
         });
      }

      private void Detaching(object sender, EventArrivedEventArgs e)
      {
         if (sender != deviceDetachWatcher) return;
         ThreadPool.QueueUserWorkItem(param =>
         {
            Debug.WriteLine("/===============================");
            Debug.WriteLine("Detaching");
            RemoveDevice();
            Debug.WriteLine("===============================/");
            Debug.Flush();
         });
      }

      #endregion
   }
}