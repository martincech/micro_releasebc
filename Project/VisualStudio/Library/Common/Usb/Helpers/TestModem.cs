﻿using System;
using System.IO.Ports;
using System.Threading;
using Usb.MODEM;
using Usb.MODEM.Platform.Windows;

namespace Usb.Helpers
{
   public class TestModem
   {
      #region Private fields

      private SerialPort _port;
      private ModemStream _stream;
      private const string ModelCommand = "AT+GMM";

      private const int RxTimeout = 1000;
      private const int WxTimeout = 1000;
      private const int BaudRate = 9600;
      private const int DataBits = 8;
      private const Parity Parity = System.IO.Ports.Parity.None;
      private const StopBits StopBits = System.IO.Ports.StopBits.One;

      #endregion

      /// <summary>
      /// Test if on selected com port is connected modem.
      /// </summary>
      /// <param name="portName">port</param>
      /// <param name="model">device name of model</param>
      /// <returns>true - device is modem</returns>
      public bool IsComModem(string portName, out string model)
      {
         model = "";
         _port = new SerialPort(portName, BaudRate, Parity, DataBits, StopBits)
         {
            Handshake = Handshake.RequestToSend,
            RtsEnable = true,
            DtrEnable = true,
            WriteTimeout = WxTimeout,
            ReadTimeout = RxTimeout
         };
         _stream = new WinModemStream(_port);

         try
         {
            _stream.Send(ModelCommand);
            Thread.Sleep(20);
            string reply;
            _stream.Receive(out reply);

            // separate model name :            
            var index = reply.IndexOf("\r\n", StringComparison.Ordinal);
            if (index != 0)
            {
               if (reply.Contains(ModelCommand + "\r"))
               {  // reply contains request -> delete it
                  reply = reply.Replace(ModelCommand + "\r", "");
               }
               else
               {
                  return (false);  
               }
            }
            reply = reply.Substring(2);
            if (!reply.Contains("\r"))
            {
               return (false);
            }
            model = reply.Substring(0, reply.IndexOf("\r", StringComparison.Ordinal));
            return true;
         }
         catch (Exception)
         {
            return false;
         }
         finally
         {  // close com port
            _stream.Disconnect();
         }
      }
   }
}
