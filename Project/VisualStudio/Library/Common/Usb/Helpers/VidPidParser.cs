﻿using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Usb.Helpers
{
   public class VidPidParser
   {
      public static int GetPid(string message)
      {
         var regex = new Regex(@"PID_([^\W]*)");
         var match = regex.Match(message);
         Debug.Assert(match.Success);
         if (match.Success)
         {
            return int.Parse(match.Groups[1].ToString(), System.Globalization.NumberStyles.HexNumber);
         }
         return -1;
      }

      public static int GetVid(string message)
      {
         var regex = new Regex(@"VID_([^\W]*)");
         var match = regex.Match(message);
         Debug.Assert(match.Success);
         if (match.Success)
         {
            return int.Parse(match.Groups[1].ToString(), System.Globalization.NumberStyles.HexNumber);
         }
         return -1;
      }
   }
}
