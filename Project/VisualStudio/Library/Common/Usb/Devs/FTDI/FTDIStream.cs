﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Usb.FTDI
{
// ReSharper disable once InconsistentNaming
   /// <summary>
   /// Communicates with a FTDI device
   /// </summary>
   [ComVisible(true), Guid("0C263D05-0D58-4c6c-AEA7-EB9E0C5338A2")]
   public abstract class FTDIStream : UsbStream
   {
      public abstract FTDIDevice Device { get; }

      /// <exclude />
      public override void Flush()
      {
      }
      /// <exclude />
      public override bool CanRead
      {
         get { return true; }
      }

      /// <exclude />
      public override bool CanSeek
      {
         get { return false; }
      }

      /// <exclude />
      public override bool CanWrite
      {
         get { return true; }
      }

      /// <exclude />
      public override bool CanTimeout
      {
         get { return true; }
      }

      /// <exclude />
      public override void SetLength(long value)
      {
         throw new NotSupportedException();
      }

      /// <exclude />
      public override long Length
      {
         get { throw new NotSupportedException(); }
      }

      /// <exclude />
      public override long Position
      {
         get { throw new NotSupportedException(); }
         set { throw new NotSupportedException(); }
      }

      /// <exclude />
      public override long Seek(long offset, SeekOrigin origin)
      {
         throw new NotSupportedException();
      }
   }
}