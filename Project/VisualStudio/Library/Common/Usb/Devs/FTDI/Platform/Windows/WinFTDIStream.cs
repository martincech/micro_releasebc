﻿#region License

/* Copyright 2012-2013 James F. Bellinger <http://www.zer7.com/software/FTDIsharp>

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. */

#endregion

using System;
using System.IO;
using System.Threading;

namespace Usb.FTDI.Platform.Windows
{
// ReSharper disable once InconsistentNaming
   internal class WinFTDIStream : FTDIStream
   {
      private FTDIDevice device;
      private Ftdi.Ftdi ftdi;
      private readonly EventWaitHandle rxEvent;

      public override FTDIDevice Device
      {
         get { return device; }
      }

      public WinFTDIStream()
      {
         rxEvent = new AutoResetEvent(false);
      }

      ~WinFTDIStream()
      {
         Close();
      }

      internal void Init(FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE deviceInfo, WinFTDIDevice winDevice)
      {
         ftdi = new Ftdi.Ftdi();
         ftdi.OpenBySerialNumber(deviceInfo.SerialNumber);
         ftdi.SetEventNotification(FTD2XX_NET.FTDI.FT_EVENTS.FT_EVENT_RXCHAR, rxEvent);

         device = winDevice;
         HandleInitAndOpen();
      }

#if DEBUG
      internal void Init(Stream testStream, WinFTDIDevice winDevice)
      {
         Init(testStream);

         device = winDevice;
         HandleInitAndOpen();
      }
#endif


      internal override void HandleFree()
      {
         try
         {
            ftdi.Close();
         }
         catch
         {
         }
         ftdi = null;
      }

      protected override void ReadThread(CancellationToken cancellationToken)
      {
         if (!HandleAcquire())
         {
            return;
         }

         try
         {
            while (!cancellationToken.IsCancellationRequested)
            {
#if DEBUG
               if (TestStream != null && TestStream.CanSeek)
               {
                  while (TestStream.Position == TestStream.Length)
                  {
                     if (cancellationToken.IsCancellationRequested)
                     {
                        break;
                     }
                     Thread.Sleep(1);
                  }
                  rxEvent.Set();
               }
#endif
               if (!rxEvent.WaitOne(ReadTimeout))
               {
                  continue;
               }

               if (cancellationToken.IsCancellationRequested)
               {
                  break;
               }

               lock (InputQueue)
               {
                  const uint bufLen = 255;
                  var inputBuffer = new byte[bufLen];
                  var readLength = bufLen;
                  SetTimeouts();
                  while (readLength == bufLen)
                  {
#if DEBUG
                     if (TestStream != null)
                     {
                        readLength = (uint) TestStream.Read(inputBuffer, 0, (int) bufLen);
                     }
                     else
                     {
#endif
                        if (ftdi.Read(inputBuffer, bufLen, ref readLength) != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                        {
                           break;
                        }
#if DEBUG
                     }
#endif
                     if (readLength == 0) break;
                     var input = new byte[readLength];
                     Array.Copy(inputBuffer, input, readLength);
                     InputQueue.Add(new CommonInput {Bytes = input});
                     Monitor.PulseAll(InputQueue);
                  }
               }
            }
         }
         finally
         {
            HandleRelease();
         }
      }

      protected override void WriteThread(CancellationToken cancellationToken)
      {
         if (!HandleAcquire())
         {
            return;
         }

         try
         {
            lock (OutputQueue)
            {
               while (true)
               {
                  while (!cancellationToken.IsCancellationRequested && OutputQueue.Count == 0)
                  {
                     Monitor.Wait(OutputQueue);
                  }
                  if (cancellationToken.IsCancellationRequested)
                  {
                     break;
                  }

                  var output = OutputQueue.Dequeue();
                  try
                  {
                     Monitor.Exit(OutputQueue);
                     SetTimeouts();
                     try
                     {
                        uint length = 0;
#if DEBUG
                        if (TestStream != null)
                        {
                           TestStream.Write(output.Bytes, 0, output.Bytes.Length);
                           length = (uint) output.Bytes.Length;
                        }
                        else
                        {
#endif
                           if (ftdi.Write(output.Bytes, output.Bytes.Length, ref length) !=
                               FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                           {
                              length = 0;
                           }
#if DEBUG
                        }
#endif
                        output.DoneOK = length == output.Bytes.Length;
                     }
                     finally
                     {
                        Monitor.Enter(OutputQueue);
                     }
                  }
                  finally
                  {
                     output.Done = true;
                     Monitor.PulseAll(OutputQueue);
                  }
               }
            }
         }
         finally
         {
            HandleRelease();
         }
      }

      private void SetTimeouts()
      {
         if (ftdi == null)
         {
            return;
         }
         ftdi.SetTimeouts(ReadTimeout == Timeout.Infinite ? 0 : (uint) ReadTimeout,
            WriteTimeout == Timeout.Infinite ? 0 : (uint) WriteTimeout);
      }
   }
}