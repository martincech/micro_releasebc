﻿using System;
using System.IO;

namespace Usb.FTDI.Platform.Windows
{
// ReSharper disable once InconsistentNaming
   internal sealed class WinFTDIDevice : FTDIDevice
   {
      private readonly FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE deviceInfo;

      internal WinFTDIDevice(FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE deviceInfo)
      {
         this.deviceInfo = deviceInfo;
         EepromData = new FTD2XX_NET.FTDI.FT_EEPROM_DATA();
      }

      public override FTDIStream Open()
      {
         var stream = new WinFTDIStream();
         try
         {
            stream.Init(deviceInfo, this);
            return stream;
         }
         catch (Exception e)
         {
            return null;
         }

      }

#if DEBUG
      public override FTDIStream Open(Stream testStream)
      {
         var stream = new WinFTDIStream();
         try
         {
            stream.Init(testStream, this);
            return stream;
         }
         catch
         {
            stream.Close();
            throw;
         }
      }
#endif

      internal FTD2XX_NET.FTDI.FT_DEVICE Type
      {
         get { return deviceInfo.Type; }
      }

      internal FTD2XX_NET.FTDI.FT_EEPROM_DATA EepromData { get; set; }

      #region Overrides of UsbDevice

      /// <summary>
      /// The manufacturer name.
      /// </summary>
      public override string Manufacturer {
         get { return EepromData.Manufacturer; }
      }

      /// <summary>
      /// The USB product ID. These are listed at: http://usb-ids.gowdy.us
      /// </summary>
      public override int ProductID
      {
         get { return EepromData.ProductID; }
      }

      /// <summary>
      /// The product name.
      /// </summary>
      public override string ProductName
      {
         get { return deviceInfo.Description; }
      }

      /// <summary>
      /// The product version.
      /// This is a 16-bit number encoding the major and minor versions in the upper and lower 8 bits, respectively.
      /// </summary>
      public override int ProductVersion
      {
         get { return 1; }
      }

      /// <summary>
      /// The device serial number.
      /// </summary>
      public override string SerialNumber
      {
         get { return deviceInfo.SerialNumber; }
      }

      /// <summary>
      /// The USB vendor ID. These are listed at: http://usb-ids.gowdy.us
      /// </summary>
      public override int VendorID
      {
         get { return EepromData.VendorID; }
      }

      #endregion

      #region Overrides of Object

      /// <summary>
      /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
      /// </summary>
      /// <returns>
      /// true if the specified object  is equal to the current object; otherwise, false.
      /// </returns>
      /// <param name="obj">The object to compare with the current object. </param>
      public override bool Equals(object obj)
      {
         if (!(obj is WinFTDIDevice))
         {
            return false;
         }
         return deviceInfo.Equals((obj as WinFTDIDevice).deviceInfo);
      }

      /// <summary>
      /// Serves as a hash function for a particular type. 
      /// </summary>
      /// <returns>
      /// A hash code for the current <see cref="T:System.Object"/>.
      /// </returns>
      public override int GetHashCode()
      {
         return deviceInfo.GetHashCode();
      }

      #endregion
   }
}