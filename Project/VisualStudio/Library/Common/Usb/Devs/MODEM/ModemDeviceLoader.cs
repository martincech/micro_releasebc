﻿using System.Collections.Generic;
using System.Linq;

namespace Usb.MODEM
{
   public class ModemDeviceLoader
   {
      /// <summary>
      /// Gets a list of connected USB devices.
      /// </summary>
      /// <returns>The device list.</returns>
      public IEnumerable<ModemDevice> GetDevices()
      {
         return Platform.ModemSelector.Instance.GetDevices();
      }

      /// <summary>
      /// Gets a list of connected USB devices, filtered by some criteria.
      /// </summary>
      /// <param name="vendorId">The vendor ID, or null to not filter by vendor ID.</param>
      /// <param name="productId">The product ID, or null to not filter by product ID.</param>
      /// <param name="productVersion">The product version, or null to not filter by product version.</param>
      /// <param name="serialNumber">The serial number, or null to not filter by serial number.</param>
      /// <returns>The filtered device list.</returns>
      public IEnumerable<ModemDevice> GetDevices
          (int? vendorId = null, int? productId = null, int? productVersion = null, string serialNumber = null)
      {
         int vid = vendorId ?? -1, pid = productId ?? -1, ver = productVersion ?? -1;
         return GetDevices().Where(hid => (hid.VendorID == vendorId || vid < 0) &&
                                          (hid.ProductID == productId || pid < 0) &&
                                          (hid.ProductVersion == productVersion || ver < 0) &&
                                          (hid.SerialNumber == serialNumber || string.IsNullOrEmpty(serialNumber)));
      }


      /// <summary>
      /// Gets the first connected USB device that matches specified criteria.
      /// </summary>
      /// <param name="vendorId">The vendor ID, or null to not filter by vendor ID.</param>
      /// <param name="productId">The product ID, or null to not filter by product ID.</param>
      /// <param name="productVersion">The product version, or null to not filter by product version.</param>
      /// <param name="serialNumber">The serial number, or null to not filter by serial number.</param>
      /// <returns>The device, or null if none was found.</returns>
      public ModemDevice GetDeviceOrDefault
          (int? vendorId = null, int? productId = null, int? productVersion = null, string serialNumber = null)
      {
         return GetDevices(vendorId, productId, productVersion, serialNumber).FirstOrDefault();
      }
   }
}
