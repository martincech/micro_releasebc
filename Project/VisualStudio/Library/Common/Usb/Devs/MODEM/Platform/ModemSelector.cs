﻿using System.Threading;
using Usb.MODEM.Platform.Unsupported;
using Usb.MODEM.Platform.Windows;

namespace Usb.MODEM.Platform
{
   internal sealed class ModemSelector
   {
      public static readonly UsbManager<ModemDevice> Instance;

      static ModemSelector()
      {
         foreach (var modemManager in new UsbManager<ModemDevice>[]
         {
            new WinModemManager(),
            new UnsupportedModemManager()
         })
         {
            if (!modemManager.IsSupported) continue;
            var readyEvent = new ManualResetEvent(false);

            Instance = modemManager;
            var managerThread = new Thread(Instance.RunImpl) {IsBackground = true};
            managerThread.Start(readyEvent);
            readyEvent.WaitOne();

            break;
         }     
      }
   }
}
