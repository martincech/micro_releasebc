﻿using System.IO;
using System.IO.Ports;
using System.Text;
using Usb.Helpers;

namespace Usb.MODEM.Platform.Windows
{
   internal class WinModemStream : ModemStream
   {
      private ModemDevice _device;
      private readonly Uart _port;              // connect via Uart class
      private readonly SerialPort _serial;      // connect via SerialPort class
      private const int RxTimeout = 1000;
      private const int MaxConnectAttempt = 20;
      private int _currentCountAttempt;

      #region Constructors

      public WinModemStream()
      {
         _port = new Uart();       
      }

      public WinModemStream(SerialPort serial)
      {
         _serial = serial;
      }

      #endregion

      #region Overrides of ModemStream

      public override ModemDevice Device
      {
         get { return _device; }
      }

      public override void Init(ModemDevice device)
      {
         _device = device;
      }

      public override bool Send(string message)
      {
         if (!CheckConnect())
         {
            Disconnect();
            return false;
         }

         if (_port != null)
         {
            _port.Flush();
            message += "\r"; //append <CR>     
            // convert Message to ASCII :
            var ascii = Encoding.ASCII;
            var unicode = Encoding.Unicode;
            var unicodeBytes = unicode.GetBytes(message);
            var data = Encoding.Convert(unicode, ascii, unicodeBytes);
            // write to port :
            var size = _port.Write(data, data.Length);
            if (size != data.Length)
            {
               Disconnect(); // Tx timeout, try reopen
               return false;
            }
         }
         else if (_serial != null)
         {
            _serial.Write(message + "\r");         
         }
         return true;
      }

      public override bool Receive(out string message)
      {
         message = "";
         if (!CheckConnect())
         {
            Disconnect();
            return (false);
         }

         if (_port != null)
         {
            var mRxBuffer = new byte[512];
            var rxSize = _port.Read(mRxBuffer, 512);
            if (rxSize == 0)
            {
               return false; // Rx timeout
            }

            // convert data to string :
            var ascii = Encoding.ASCII;
            var asciiText = new char[ascii.GetCharCount(mRxBuffer, 0, rxSize)];
            ascii.GetChars(mRxBuffer, 0, rxSize, asciiText, 0);
            message = new string(asciiText);
         }
         else if (_serial != null)
         {
            message = _serial.ReadExisting();
         }
         return true;
      }

      public override void SetBaudRate(int rate)
      {
         if (_port == null) return;
         
         _port.SetParameters(rate, Uart.Parity.None);
      }

      public override void SetReadTimeout(int timeout)
      {
         if (_port == null) return;

         _port.SetRxWait(timeout);
      }

      public override void Disconnect()
      {
         if (_port != null && _port.IsOpen)
         {
            _port.Close();
         }
         if (_serial != null && _serial.IsOpen)
         {
            _serial.Close();
         }
      }

      #endregion

      #region Overrides of Stream

      public override bool CanRead
      {
         get { throw new System.NotImplementedException(); }
      }

      public override bool CanSeek
      {
         get { throw new System.NotImplementedException(); }
      }

      public override bool CanWrite
      {
         get { throw new System.NotImplementedException(); }
      }

      public override void Flush()
      {
         throw new System.NotImplementedException();
      }

      public override long Length
      {
         get { throw new System.NotImplementedException(); }
      }

      public override long Position
      {
         get
         {
            throw new System.NotImplementedException();
         }
         set
         {
            throw new System.NotImplementedException();
         }
      }

      public override int Read(byte[] buffer, int offset, int count)
      {
         throw new System.NotImplementedException();
      }

      public override long Seek(long offset, System.IO.SeekOrigin origin)
      {
         throw new System.NotImplementedException();
      }

      public override void SetLength(long value)
      {
         throw new System.NotImplementedException();
      }

      public override void Write(byte[] buffer, int offset, int count)
      {
         throw new System.NotImplementedException();
      }

      #endregion

      #region Private helpers

      private bool CheckConnect()
      {
         if ((_port != null && _port.IsOpen) || (_serial != null && _serial.IsOpen))
         {
            _currentCountAttempt = 0;
            return true;
         }

         try
         {
            if (_port != null)
            {
               _port.Open(Device.PortName);
               _port.SetRxWait(RxTimeout);
               _port.Flush();
            }
            else if (_serial != null)
            {
               _serial.Open();
            }
            _currentCountAttempt++;
            return true;
         }
         catch (IOException ioEx)
         {
            if (_currentCountAttempt == MaxConnectAttempt || ioEx.Message.Contains("COM"))
            {
               _currentCountAttempt = 0;
               return false;
            }
            System.Threading.Thread.Sleep(200);
            return CheckConnect();
         }
      }

      #endregion
   }
}

