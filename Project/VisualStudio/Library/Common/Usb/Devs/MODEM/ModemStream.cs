﻿using System.IO;

namespace Usb.MODEM
{
   public abstract class ModemStream : Stream
   {
      public abstract ModemDevice Device { get; }
      public abstract void Init(ModemDevice device);

      public abstract bool Send(string message);
      public abstract bool Receive(out string message);

      public abstract void SetBaudRate(int rate);
      public abstract void SetReadTimeout(int timeout);
      public abstract void Disconnect();
   }
}
