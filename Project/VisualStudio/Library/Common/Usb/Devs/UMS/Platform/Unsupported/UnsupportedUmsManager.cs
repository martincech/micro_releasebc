﻿using System;

namespace Usb.UMS.Platform.Unsupported
{
   internal class UnsupportedUmsManager : UsbManager<UmsDevice>
   {
      public override bool IsSupported
      {
         get { return true; }
      }

      protected override object[] Refresh()
      {
         return new object[0];
      }

      protected override bool TryCreateDevice(object key, out UmsDevice device, out object creationState)
      {
         throw new NotImplementedException();
      }

      protected override void CompleteDevice(object key, UmsDevice device, object creationState)
      {
         throw new NotImplementedException();
      }
   }
}