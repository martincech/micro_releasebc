﻿using System.IO;
namespace Usb.ELO
{
   public abstract class EloStream : Stream
   {
      public abstract EloDevice Device { get; }
      public abstract void Init(EloDevice device);

      public abstract bool Send(string message);
      public abstract bool Receive(out string message);

      public abstract void SetBaudRate(int rate);
      public abstract void SetReadTimeout(int timeout);
      public abstract void Disconnect();
   }
}
