﻿namespace Usb.ELO
{
   public abstract class EloDevice : UsbDevice
   {
      public abstract string PortName { get; set; }
      public abstract int BaudRate { get; set; }
      public abstract int RxTimeout { get; set; }

      public abstract EloStream Open();
   }
}