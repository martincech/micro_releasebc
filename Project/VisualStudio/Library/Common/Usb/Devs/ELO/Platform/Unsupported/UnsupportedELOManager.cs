﻿using System;

namespace Usb.ELO.Platform.Unsupported
{
   internal class UnsupportedEloManager : UsbManager<EloDevice>
   {
      public override bool IsSupported
      {
         get { return true; }
      }

      protected override object[] Refresh()
      {
         return new object[0];
      }

      protected override bool TryCreateDevice(object key, out EloDevice device, out object creationState)
      {
         throw new NotImplementedException();
      }

      protected override void CompleteDevice(object key, EloDevice device, object creationState)
      {
         throw new NotImplementedException();
      }
   }
}
