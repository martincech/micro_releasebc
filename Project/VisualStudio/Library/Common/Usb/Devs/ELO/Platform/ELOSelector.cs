﻿using System.Threading;
using Usb.ELO.Platform.Unsupported;
using Usb.ELO.Platform.Windows;

namespace Usb.ELO.Platform
{
   internal sealed class EloSelector
   {
      public static readonly UsbManager<EloDevice> Instance;

      static EloSelector()
      {
         foreach (var modemManager in new UsbManager<EloDevice>[]
         {
            new WinEloManager(),
            new UnsupportedEloManager()
         })
         {
            if (!modemManager.IsSupported) continue;
            var readyEvent = new ManualResetEvent(false);

            Instance = modemManager;
            var managerThread = new Thread(Instance.RunImpl) { IsBackground = true };
            managerThread.Start(readyEvent);
            readyEvent.WaitOne();

            break;
         }
      }
   }
}
