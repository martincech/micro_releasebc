﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using Usb.Helpers;

namespace Usb.ELO.Platform.Windows
{
   internal class WinEloStream : EloStream
   {
      private EloDevice _device;
      private readonly Uart _port;
      private const int RxTimeout = 1000;

      public WinEloStream()
      {
         _port = new Uart();             
      }     

      #region Overrides of ModemStream

      public override EloDevice Device
      {
         get { return _device; }
      }

      public override void Init(EloDevice device)
      {
         _device = device;
      }

      public override bool Send(string message)
      {
         if (!CheckConnect())
         {
            Disconnect();
            return false;
         }
         _port.Flush();
         message += "\r";        //append <CR>     
         // convert Message to ASCII :
         var ascii = Encoding.ASCII;
         var unicode = Encoding.Unicode;
         var unicodeBytes = unicode.GetBytes(message);
         var data = Encoding.Convert(unicode, ascii, unicodeBytes);
         // write to port :
         var size = _port.Write(data, data.Length);
         if (size != data.Length)
         {
            Disconnect();                        // Tx timeout, try reopen
            return (false);
         }
         return (true);
      }

      public override bool Receive(out string message)
      {
         message = "";
         if (!CheckConnect())
         {
            Disconnect();
            return (false);
         }
         var mRxBuffer = new byte[512];
         var rxSize = _port.Read(mRxBuffer, 512);
         if (rxSize == 0)
         {
            return (false);                      // Rx timeout
         }
         
         // convert data to string :
         var ascii = Encoding.ASCII;
         var asciiText = new char[ascii.GetCharCount(mRxBuffer, 0, rxSize)];
         ascii.GetChars(mRxBuffer, 0, rxSize, asciiText, 0);
         message = new string(asciiText);
         return (true);
      }

      public override void SetBaudRate(int rate)
      {
         _port.SetParameters(rate, Uart.Parity.None);
      }

      public override void SetReadTimeout(int timeout)
      {
         _port.SetRxWait(timeout);
      }

      public override void Disconnect()
      {
         if (!_port.IsOpen) return;

         _port.Close();
      }

      #endregion

      #region Overrides of Stream

      public override bool CanRead
      {
         get { throw new NotImplementedException(); }
      }

      public override bool CanSeek
      {
         get { throw new NotImplementedException(); }
      }

      public override bool CanWrite
      {
         get { throw new NotImplementedException(); }
      }

      public override void Flush()
      {
         throw new NotImplementedException();
      }

      public override long Length
      {
         get { throw new NotImplementedException(); }
      }

      public override long Position
      {
         get
         {
            throw new NotImplementedException();
         }
         set
         {
            throw new NotImplementedException();
         }
      }

      public override int Read(byte[] buffer, int offset, int count)
      {
         throw new NotImplementedException();
      }

      public override long Seek(long offset, SeekOrigin origin)
      {
         throw new NotImplementedException();
      }

      public override void SetLength(long value)
      {
         throw new NotImplementedException();
      }

      public override void Write(byte[] buffer, int offset, int count)
      {
         throw new NotImplementedException();
      }

      #endregion

      #region Private helpers

      private bool CheckConnect()
      {
         if (_port.IsOpen) return true;

         try
         {
            _port.Open(Device.PortName);
            //_port.SetParameters(mBaudRate, Uart.Parity.None);
            _port.SetRxWait(RxTimeout);
            _port.Flush();
            return true;
         }
         catch (IOException ioEx)
         {
            if (ioEx.Message.Contains("COM")) return false;
            Thread.Sleep(200);
            return CheckConnect();
         }
      }

      #endregion
   }
}
