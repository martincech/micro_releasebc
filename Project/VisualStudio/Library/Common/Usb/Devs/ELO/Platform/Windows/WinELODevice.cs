﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usb.ELO.Platform.Windows
{
    internal sealed class WinEloDevice : EloDevice
    {
       #region Private Fields

       private readonly string _manufacturer;
       private readonly int _productId;
       private readonly string _productName;
       private readonly int _productVersion;
       private readonly string _serialNumber;
       private readonly int _vendorId;

       private string _portName;
       private int _baudRate;
       private int _rxTimeout;
       private EloStream _stream;

       private const int DefaultBaudRate = 9600;
       private const int DefaultReadTimeout = 1000; // ms

       #endregion

       public WinEloDevice(string port, string productName, int pid, int vid, string manufacturer, string serialNumber)
       {
          _baudRate = DefaultBaudRate;
          _rxTimeout = DefaultReadTimeout;
          _portName = port;
          _productName = productName;

          _manufacturer = manufacturer;
          _serialNumber = serialNumber;
          _productId = pid;
          _productVersion = -1;
          _vendorId = vid;
       }

       #region Overrides of ModemDevice

       public override EloStream Open()
       {
          _stream = new WinEloStream();
          try
          {
             _stream.Init(this);
             return _stream;
          }
          catch (Exception)
          {
             _stream.Close();
             throw;
          }
       }

       public override string PortName
       {
          get { return _portName; }
          set
          {
             _portName = value;
             _stream.Disconnect();
          }
       }

       public override int BaudRate
       {
          get { return _baudRate; }
          set
          {
             _baudRate = value;
             _stream.SetBaudRate(value);
          }
       }

       public override int RxTimeout
       {
          get { return _rxTimeout; }
          set
          {
             _rxTimeout = value;
             _stream.SetReadTimeout(value);
          }
       }

       #endregion

       #region Overrides of UsbDevice

       public override string Manufacturer
       {
          get { return _manufacturer; }
       }

       public override int ProductID
       {
          get { return _productId; }
       }

       public override string ProductName
       {
          get { return _productName; }
       }

       public override int ProductVersion
       {
          get { return _productVersion; }
       }

       public override string SerialNumber
       {
          get { return _serialNumber; }
       }

       public override int VendorID
       {
          get { return _vendorId; }
       }

       #endregion
    }
}
