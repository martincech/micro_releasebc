﻿#region License
/* Copyright 2010, 2013 James F. Bellinger <http://www.zer7.com/software/hidsharp>

   Permission to use, copy, modify, and/or distribute this software for any
   purpose with or without fee is hereby granted, provided that the above
   copyright notice and this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
   WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
   ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
   ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
   OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. */
#endregion

using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace Usb.HID
{
    /// <summary>
    /// Detects USB HID class devices connected to the system.
    /// </summary>
    [ComVisible(true), Guid("CD7CBD7D-7204-473c-AA2A-2B9622CFC6CC")]
    public class HidDeviceLoader
    {
       /// <summary>
        /// Gets a list of connected USB devices.
        /// </summary>
        /// <returns>The device list.</returns>
        public IEnumerable<HidDevice> GetDevices()
        {
            return Platform.HidSelector.Instance.GetDevices();
        }

        /// <summary>
        /// Gets a list of connected USB devices, filtered by some criteria.
        /// </summary>
        /// <param name="vendorId">The vendor ID, or null to not filter by vendor ID.</param>
        /// <param name="productId">The product ID, or null to not filter by product ID.</param>
        /// <param name="productVersion">The product version, or null to not filter by product version.</param>
        /// <param name="serialNumber">The serial number, or null to not filter by serial number.</param>
        /// <returns>The filtered device list.</returns>
        public IEnumerable<HidDevice> GetDevices
            (int? vendorId = null, int? productId = null, int? productVersion = null, string serialNumber = null)
        {
           int vid = vendorId ?? -1, pid = productId ?? -1, ver = productVersion ?? -1;
           return GetDevices().Where(hid => (hid.VendorID == vendorId || vid < 0) &&
                                            (hid.ProductID == productId || pid < 0) &&
                                            (hid.ProductVersion == productVersion || ver < 0) &&
                                            (hid.SerialNumber == serialNumber || string.IsNullOrEmpty(serialNumber)));
        }

       /// <summary>
        /// Gets the first connected USB device that matches specified criteria.
        /// </summary>
        /// <param name="vendorId">The vendor ID, or null to not filter by vendor ID.</param>
        /// <param name="productId">The product ID, or null to not filter by product ID.</param>
        /// <param name="productVersion">The product version, or null to not filter by product version.</param>
        /// <param name="serialNumber">The serial number, or null to not filter by serial number.</param>
        /// <returns>The device, or null if none was found.</returns>
        public HidDevice GetDeviceOrDefault
            (int? vendorId = null, int? productId = null, int? productVersion = null, string serialNumber = null)
        {
            return GetDevices(vendorId, productId, productVersion, serialNumber).FirstOrDefault();
        }
    }
}
