﻿using System;
using Usb.HID;

namespace Usb.Hid.Platform.Unsupported
{
// ReSharper disable once InconsistentNaming
   internal class UnsupportedHidManager : UsbManager<HidDevice>
   {
      public override bool IsSupported
      {
         get { return true; }
      }

      protected override object[] Refresh()
      {
         return new object[0];
      }

      protected override bool TryCreateDevice(object key, out HidDevice device, out object creationState)
      {
         throw new NotImplementedException();
      }

      protected override void CompleteDevice(object key, HidDevice device, object creationState)
      {
         throw new NotImplementedException();
      }
   }
}