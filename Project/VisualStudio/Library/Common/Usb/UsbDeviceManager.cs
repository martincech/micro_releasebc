﻿using System;
using System.Collections.Generic;

namespace Usb
{
   public abstract class UsbDeviceManager : IDisposable
   {
      public static UsbDeviceManager Instance
      {
         get { return UsbDeviceLoader.Instance; }
      }
      /// <summary>
      ///    List of connected usb devices.
      /// </summary>
      public abstract IEnumerable<UsbDevice> ConnectedDevices { get; }

      /// <summary>
      ///    Invoked when some unknown device connected
      /// </summary>
      public abstract event EventHandler<UsbDevice> DeviceConnected;

      /// <summary>
      ///    Invoked when some known device disconnected
      /// </summary>
      public abstract event EventHandler<UsbDevice> DeviceDisconnected;

      public abstract void Dispose();
   }
}