﻿using System.Security.Permissions;
using System.Windows.Threading;

namespace Tests
{
   /// <summary>
   /// For unit testing purposes, invoke events on <see cref="Dispatcher.CurrentDispatcher"/>
   /// </summary>
   public static class DispatcherUtil
   {
      /// <summary>
      /// Invoke waiting dispatcher events
      /// </summary>
      [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
      public static void DoEvents()
      {
         var frame = new DispatcherFrame();
         Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                                                  new DispatcherOperationCallback(ExitFrame), frame);
         Dispatcher.PushFrame(frame);
      }

      private static object ExitFrame(object frame)
      {
         ((DispatcherFrame)frame).Continue = false;
         return null;
      }
   }
}
