﻿//******************************************************************************
//
//   StructConversion.cs      Conversion of byte array to struct
//   Version 1.0
//
//******************************************************************************

using System;
using System.Runtime.InteropServices;

namespace Utilities
{
   public static class StructConversion
   {
      /// <summary>
      /// Convert a byte array to a struct
      /// </summary>
      /// <typeparam name="T">Struct type</typeparam>
      /// <param name="data">Data in array of bytes</param>
      /// <returns>New struct instance</returns>
      public static T ArrayToStruct<T>(byte[] data) where T : struct
      {
         // Zjistim adresu pole v pameti, zaroven zakazu Garbage Collectoru, aby pole v pameti premistoval
         GCHandle gch = GCHandle.Alloc(data, GCHandleType.Pinned);
         try
         {
            // Prevedu pole na strukturu a vratim
            return (T) Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof (T));
         }
         finally
         {
            // Povolim opet Garbage Collectoru manipulaci s polem (je treba uvolnit rucne)
            gch.Free();
         }
      }

      public static byte[] StructToArray<T>(T @struct) where T : struct
      {
         var size = Marshal.SizeOf(@struct);
         var arr = new byte[size];
         var ptr = Marshal.AllocHGlobal(size);

         Marshal.StructureToPtr(@struct, ptr, true);
         Marshal.Copy(ptr, arr, 0, size);
         Marshal.FreeHGlobal(ptr);
         return arr;
      }

      /// <summary>
      /// Calculate size of struct
      /// </summary>
      /// <param name="t">Type</param>
      /// <returns>Size in bytes</returns>
      public static int SizeOf(Type t)
      {
         return Marshal.SizeOf(t);
      }
   }
}