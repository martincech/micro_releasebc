﻿//******************************************************************************
//
//   Csv.cpp        Export to CSV file
//   Version 1.3
//
//******************************************************************************

// Prace se soubory
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

// Decimal separator

namespace Utilities
{
   /// <summary>
   /// Export to CSV file
   /// </summary>
   public class Csv
   {

      /// <summary>
      /// Column delimiter as string
      /// </summary>
      private string delimiter;
      // Excel bere jen tabulator, OpenOffice ma default carku, ale je nastavitelny

      /// <summary>
      /// Decimal separator as string
      /// </summary>
      private string decimalSeparator;
      // Excel2000 bere jen tecku, Excel2007 nastaveni Windows (carku na CZ).
      // OpenOffice zda se jen carku, i kdyz nastavim v Control Panelu desetinnou tecku.

      /// <summary>
      /// File encoding 
      /// </summary>
      private Encoding encoding;
      // - Encoding.Default: nastavi ANSI kodovani, ktere je prave nastavene ve Windows,
      //                     tj. v CZ Win jsou citelne jen ceske znaky atd.
      // - Encoding.Unicode: nastavi Unicode UTF-16, v Excelu i OpenOffice funguje dobre.
      // - Encoding.UTF8: nastavi Unicode UTF-8, v Excelu nefunguje, i kdyz napr. prohlizec
      //                  v TotalCmd text zobrazi korektne, tj. export jako takovy funguje
      //                  jak ma. V OpenOffice funguje.

      /// <summary>
      /// One line as a list of columns
      /// </summary>
      private List<string> lineList = new List<string>();

      /// <summary>
      /// List of lines
      /// </summary>
      private List<List<string>> linesList = new List<List<string>>();

      /// <summary>
      /// Constructor
      /// </summary>
      /// <param name="delimiter">Column delimiter</param>
      /// <param name="decimalSeparator">Decimal separator</param>
      /// <param name="encoding">File encoding</param>
      public Csv(string delimiter, string decimalSeparator, Encoding encoding)
      {
         this.delimiter = delimiter;
         this.decimalSeparator = decimalSeparator;
         this.encoding = encoding;
      }

      /// <summary>
      /// Constructor
      /// </summary>
      public Csv()
         : this(",", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, Encoding.Default)
      {
         // Default pouzivam carku, ANSI nastavene ve Windows a oddelovac desetin nastaveny ve Windows
      }

      /// <summary>
      /// Clear line
      /// </summary>
      public void ClearLine()
      {
         lineList.Clear();
      }

      /// <summary>
      /// Clear whole CSV
      /// </summary>
      public void Clear()
      {
         linesList.Clear();
      }

      /// <summary>
      /// Extend line with empty values
      /// </summary>
      /// <param name="line">Line to extend</param>
      /// <param name="count">Minimum number of values</param>
      private void ExtendLine(ref List<string> line, int count)
      {
         while (line.Count < count)
         {
            line.Add("");
         }
      }

      /// <summary>
      /// Add string to the line
      /// </summary>
      /// <param name="position">Position within the line</param>
      /// <param name="text">Text to add</param>
      public void AddString(int position, string text)
      {
         // Kontrola rozsahu
         if (position < 0)
         {
            return;                 // Mimo rozsah
         }

         // Zkontroluju, zda je na text v radku misto
         ExtendLine(ref lineList, position);

         // Pokud vkladam text doprostred radku, nahradim puvodni string novym)
         if (position < lineList.Count)
         {
            lineList.RemoveAt(position);
         }

         // Pridam text
         lineList.Insert(position, text);
      }

      /// <summary>
      /// Add integer to the line
      /// </summary>
      /// <param name="position">Position within the line</param>
      /// <param name="value">Value to add</param>
      public void AddInteger(int position, int value)
      {
         AddString(position, value.ToString());
      }

      /// <summary>
      /// Add long to the line
      /// </summary>
      /// <param name="position">Position within the line</param>
      /// <param name="value">Value to add</param>
      public void AddLong(int position, long value)
      {
         AddString(position, value.ToString());
      }

      /// <summary>
      /// Add double to the line
      /// </summary>
      /// <param name="position">Position within the line</param>
      /// <param name="value">Value to add</param>
      public void AddDouble(int position, double value)
      {
         // Nahradim desetinny oddelovac
         AddString(position, value.ToString().Replace(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, decimalSeparator));
      }

      /// <summary>
      /// Add date and time to the line
      /// </summary>
      /// <param name="position">Position within the line</param>
      /// <param name="dateTime">DateTime to add</param>
      public void AddDateTime(int position, DateTime dateTime)
      {
         // Excel i OpenOffice importuji spravne format "YYYY-MM-DD hh:mm:ss"
         AddString(position, dateTime.ToString("yyyy-MM-dd HH:mm:ss"));  // HH = 24hod format
      }

      /// <summary>
      /// Add date to the line
      /// </summary>
      /// <param name="position">Position within the line</param>
      /// <param name="dateTime">DateTime to add</param>
      public void AddDate(int position, DateTime dateTime)
      {
         // Excel i OpenOffice importuji spravne format "YYYY-MM-DD"
         AddString(position, dateTime.ToString("yyyy-MM-dd"));
      }

      /// <summary>
      /// Add time to the line
      /// </summary>
      /// <param name="position">Position within the line</param>
      /// <param name="dateTime">DateTime to add</param>
      public void AddTime(int position, DateTime dateTime)
      {
         // Excel i OpenOffice importuji spravne format "hh:mm:ss"
         AddString(position, dateTime.ToString("HH:mm:ss"));     // HH = 24hod format
      }

      /// <summary>
      /// Save line to the list
      /// </summary>
      public void SaveLine()
      {
         linesList.Add(new List<string>(lineList));
      }

      /// <summary>
      /// Format lines so all have same length
      /// </summary>
      private void FormatLines()
      {
         int MaxLength = 0;
         List<string> refLine;

         // Najdu maximalni delku radku
         foreach (List<string> line in linesList)
         {
            if (line.Count > MaxLength)
            {
               MaxLength = line.Count;
            }
         }

         // Rozsirim vsechny radky na maximalni delku
         foreach (List<string> line in linesList)
         {
            refLine = line;
            ExtendLine(ref refLine, MaxLength);
         }
      }

      /// <summary>
      /// Return corrected value that is possible to insert directly into the file
      /// </summary>
      /// <param name="value">Original value</param>
      /// <returns>Corrected value</returns>
      private string GetCorrectedValue(string value)
      {
         bool useDoubleQuote = false;

         // Pokud hodnota obsahuje oddelovac, musim ji dat do uvozovek
         if (value.Contains(delimiter))
         {
            useDoubleQuote = true;
         }

         // Pokud hodnota obsahuje uvozovky, musim je v textu zdvojit
         // a hodnotu dam celou do uvozovek
         for (int i = 0; i < value.Length; i++)
         {
            if (value[i] == '"')
            {
               value = value.Insert(i, "\"");  // Zdvojim uvozovku
               i++;                            // Preskocim pridanou uvozovku
               useDoubleQuote = true;          // Cely text dam na zaver do uvozovek
            }
         }

         // Pokud je treba, dam hodnotu do uvozovek
         if (useDoubleQuote)
         {
            value = "\"" + value + "\"";
         }

         return value;
      }

      /// <summary>
      /// Generate line to be saved to the file
      /// </summary>
      /// <param name="line">Line</param>
      /// <returns>String to save to the file</returns>
      private string MakeCsvLine(List<string> line)
      {
         string Str = "";
         int Count = 0;

         foreach (string Value in line)
         {
            if (Count > 0)
            {
               // V radku uz neco je, pridam oddelovac
               Str += delimiter;
            }
            // Pokud hodnota obsahuje oddelovac, musim ji dat do uvozovek
            Str += GetCorrectedValue(Value);
            Count++;
         }

         return Str;
      }

      /// <summary>
      /// Save whole CSV to a file
      /// </summary>
      /// <param name="fileName">File ename</param>
      /// <returns>True if successful</returns>
      public bool SaveToFile(string fileName, bool append)
      {
         try
         {
            // Pokud chce vytvorit novy soubor, zkontroluju, zda uz soubor neexistuje
            if (!append && File.Exists(fileName))
            {
               return false;
            }

            // Zformatuju vsechny radky
            FormatLines();

            // Otevru soubor a zapisu vsechny radky
            using (FileStream fileStream = new FileStream(fileName, FileMode.Append))
            {
               using (StreamWriter writer = new StreamWriter(fileStream, encoding))
               {
                  foreach (List<string> line in linesList)
                  {
                     writer.WriteLine(MakeCsvLine(line));
                  }
                  // Stream neni treba rucne zavirat, o vse s postara using {}
               }
            }
         }
         catch
         {
            return false;
         }

         return true;
      }

       public byte[] SaveToBytes()
       {
           byte[] bytes = null;
           using (MemoryStream memoryStream = new MemoryStream())
           {  
               using (StreamWriter writer = new StreamWriter(memoryStream, encoding))
               {
                   foreach (List<string> line in linesList)
                   {
                       writer.WriteLine(MakeCsvLine(line));
                   }
               }
               bytes = memoryStream.ToArray();
           }
           return bytes;
       }

       /// <summary>
      /// Save whole CSV to a file
      /// </summary>
      /// <param name="fileName">File ename</param>
      /// <returns>True if successful</returns>
      public bool SaveToFile(string fileName)
      {
         return SaveToFile(fileName, false);
      }


   }
}
