﻿/**
* Copyright 2008-2009 The Asagao Project. All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions 
* are met:
*
*  1. Redistributions of source code must retain the above copyright 
*     notice, this list of conditions and the following disclaimer.
*  2. Redistributions in binary form must reproduce the above copyright 
*     notice, this list of conditions and the following disclaimer in 
*     the documentation and/or other materials provided with the 
*     distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE ASAGAO PROJECT ``AS IS'' AND ANY 
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ASAGAO PROJECT OR 
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
* PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
* USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* 
* The views and conclusions contained in the software and documentation 
* are those of the authors and should not be interpreted as representing 
* official policies, either expressed or implied, of the Asagao Project.
* 
* http://sourceforge.net/projects/asagao/
* </summary>
* <summary> 
* \file jtagdll.cs
* \Author Suikan
*  Basic interface for FTDI's DLL.
* 
* Contains basic interface for FTDI's DLL. This file is generated from the FTCJTAG.h 
* file which is delivered at http://www.ftdichip.com/Projects/MPSSE/FTCJTAG.htm. 
* To use this interface file, you should also plase FTCJTAG.DLL into /windows/system32 
* directory. that DLL is distributed together with FTCJTAG.h. 
* 
* Note that original FTCJTAG.h has several typo about the pin name. After few minutes
* concern, I decided to correct these bugs. So, some name is not compatible with original
* name. 
* 
* Also, functionames of each DLL function is changed. I just thought FtcJtag.Open() is 
* much better than FtcJtag.JTAG_Open(). 
* 
*/

using System.Runtime.InteropServices;
using Utilities;

namespace Ftdi.Jtag
{
   /// <summary>
   /// interface class for FTCJTAG.DLL.
   ///  
   ///  This class works as wrapper of FTCJTAG.DLL for C#. All interface is delivered from
   ///  FTCJTAG.h but re-written in C# manner. 
   /// </summary>
   public static class FtcJtag
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="T:System.Object"/> class.
      /// </summary>
      static FtcJtag()
      {
         DllLoader.Load("FTCJTAG.dll");
      }

      /// <summary>
      ///  Count up FT2232 devices.
      /// 
      /// This function provides the information how many FT2232 devices are in the system.
      /// After you know the number of device, you will scan all devices's name and its unique
      /// location ID by JTAG.GetDeviceNameLocID() function.
      /// <param name="lpdwNumDevice"> Returns how many devices are in the system</param>
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_GetNumDevices")]
      public static extern JtagStatus
         GetNumDevices(
         out int lpdwNumDevice // return how many devices are in system
         );


      /// <summary>
      ///  Detect name and location ID of specified devices. 
      /// 
      /// This functions is useful to enumulate all FT2232 devices in your system. the returned value
      /// for lpdwLocationID is used to open the device. Before calling this function, you should check
      /// how many devices are in system, by calling JTAG.GetNumDevices() function.
      /// </summary>
      /// <param name="dwDeviceNameIndex"> 0 origin device number. Must be smaller than value which is given by GetNumDevices() function. </param>
      /// <param name="lpDeviceNameBuffer"> Returns name of specified device.</param>
      /// <param name="dwBufferSize"> Specify the length of lpDevicesNameBuffer.</param>
      /// <param name="lpdwLocationId"> Returns unique location ID which is used by JTAG.OpenEx() function.</param>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_GetDeviceNameLocID")]
      public static extern JtagStatus
         GetDeviceNameLocID(
         int dwDeviceNameIndex, // specify device index number. 0 origin.
         System.Text.StringBuilder lpDeviceNameBuffer, // name of speicified device
         int dwBufferSize, // length of name buffer.
         out int lpdwLocationId); // location ID of specified devices.


      /// <summary>
      ///  Open the device by name and location id.
      /// <param name="lpDeviceName"> device Name given by GetDeviceNameLocID</param>
      /// <param name="dwLocationId"> location ID given by GetDeviceNameLocID</param>
      /// <param name="pftHandle"> handle to opened device</param>
      /// 
      /// open a device specified by name and location ID. The name and Location ID must be the
      /// one which is obtained by GetDeviceNameLocID() funciton.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_OpenEx")]
      public static extern JtagStatus
         OpenEx(
         string lpDeviceName, // device Name given by GetDeviceNameLocID
         int dwLocationId, // location ID given by GetDeviceNameLocID
         out int pftHandle // handle to opened device
         );

      /// <summary>
      ///  Open a device when that is only one in the system.
      /// <param name="pftHandle"> handle to opened device</param>
      /// 
      /// If you have only one FT2232 device in your system, you can open it by this function.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_Open")]
      public static extern JtagStatus
         Open(
         out int pftHandle // handle to opened device
         );

      /// <summary>
      ///  Close the opened device
      /// <param name="pftHandle"> specify device</param>
      /// 
      /// Close the specified device.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_Close")]
      public static extern JtagStatus
         Close(
         int pftHandle // specify device
         );


      /// <summary>
      ///  Initialize JTAG clock by given devisor
      /// <param name="pftHandle"> specify device</param>
      /// <param name="dwClockDivisor"> specify devisor</param>
      /// 
      /// Set the clock frequency of TCLK. The clock frequency[MHz] is 6/(1+dwClockDivisor).
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_InitDevice")]
      public static extern JtagStatus
         InitDevice(
         int pftHandle, // specify device
         int dwClockDivisor // specify devisor
         );

      /// <summary>
      ///  calc actual clock from devisor
      /// <param name="dwClockDivisor"> specify devisor</param>
      /// <param name="lpdwClockFrequencyHz"> obtain freq from devisor</param>
      /// 
      /// Calcurate JTAG clock frequency from devisor.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_GetClock")]
      public static extern JtagStatus
         GetClock(
         int dwClockDivisor, // specify devisor
         ref int lpdwClockFrequencyHz // obtain freq from devisor
         );

      /// <summary>
      ///  set JTAG clock by given devisor
      /// <param name="pftHandle"> specify device</param>
      /// <param name="dwClockDivisor"> specify devisor</param>
      /// <param name="lpdwClockFrequencyHz"> obtain freq from devisor</param>
      /// 
      /// Set clock divisor and obtain result frequency.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_SetClock")]
      public static extern JtagStatus
         SetClock(
         int pftHandle, // specify device
         int dwClockDivisor, // specify devisor
         ref int lpdwClockFrequencyHz // obtain freq from devisor
         );

      /// <summary>
      ///  set JTAG Loopback mode
      /// <param name="pftHandle"> specify device</param>
      /// <param name="bLoopbackState"> true to set loopback mode</param>
      /// 
      /// Is this internal test of FTCJTAG.DLL?
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_SetLoopback")]
      public static extern JtagStatus
         SetLoopback(
         int pftHandle, // specify device
         bool bLoopbackState // true to set loopback mode
         );


      /// <summary>
      ///  Set GPIO pin
      /// <param name="ftHandle"> specify device </param>
      /// <param name="bControlLowInputOutputPins">?</param>
      /// <param name="pLowInputOutputPinsData">Direction and Data?</param>
      /// <param name="bControlHighInputOutputPins">?</param>
      /// <param name="pHighInputOutputPinsData">Direction and Data?</param>
      /// 
      /// This function sets the GPIO pin state based on the given parameter.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_SetGPIOs")]
      public static extern JtagStatus
         SetGPIOs(
         int ftHandle,
         bool bControlLowInputOutputPins,
         ref InputOutputPins pLowInputOutputPinsData,
         bool bControlHighInputOutputPins,
         ref InputOutputPins pHighInputOutputPinsData
         );

      /// <summary>
      ///  Get GPIO pin status.
      /// <param name="ftHandle"> specify device</param>
      /// <param name="bControlLowInputOutputPins">?</param>
      /// <param name="pLowInputPinsData"> return GPIO status.</param>
      /// <param name="bControlHighInputOutputPins">?</param>
      /// <param name="pHighInputPinsData"> return GPIO status.</param>
      /// 
      /// Return the GPIO pin status into parameter.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_GetGPIOs")]
      public static extern JtagStatus
         GetGPIOs(
         int ftHandle,
         bool bControlLowInputOutputPins,
         out InputPins pLowInputPinsData,
         bool bControlHighInputOutputPins,
         out InputPins pHighInputPinsData
         );


      /// <summary>
      ///  Write data stream to JTAG
      /// <param name="ftHandle"> handle given by Open or OpenEx</param>
      /// <param name="bInstructionTestData">true:Instruction register, false:Test register</param>
      /// <param name="dwNumBitsToWrite"> The numbers of data to be read [bits]</param>
      /// <param name="pWriteDataBuffer"> buffer which contains data to be sent</param>
      /// <param name="dwNumBytesToWrite"> Length of data buffer? [BYTE]</param>
      /// <param name="dwTapControllerState">     </param>
      /// A write command.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_Write")]
      public static extern JtagStatus
         Write(
         int ftHandle,
         JtagRegType bInstructionTestData,
         int dwNumBitsToWrite,
         byte[] pWriteDataBuffer,
         int dwNumBytesToWrite,
         JtagTapState dwTapControllerState
         );


      /// <summary>
      ///  Read data stream from JTAG
      /// <param name="ftHandle"> handle given by Open or OpenEx</param>
      /// <param name="bInstructionTestData">true:Instruction register, false:Test register</param>
      /// <param name="dwNumBitsToRead"> The numbers of data to be read [bits]</param>
      /// <param name="pReadDataBuffer"> buffer which contains data to be read</param>
      /// <param name="lpdwNumBytesReturned"> the data length which are read [BYTE]</param>
      /// <param name="dwTapControllerState">     </param>
      /// A read command.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_Read")]
      public static extern JtagStatus
         Read(
         int ftHandle,
         JtagRegType bInstructionTestData,
         int dwNumBitsToRead,
         byte[] pReadDataBuffer,
         out int lpdwNumBytesReturned,
         JtagTapState dwTapControllerState
         );

      /// <summary>
      ///  Read & Wriite data stream from JTAG
      /// <param name="ftHandle"> handle given by Open or OpenEx</param>
      /// <param name="bInstructionTestData">true:Instruction register, false:Test register</param>
      /// <param name="dwNumBitsToWrite"> The numbers of data to be read [bits]</param>
      /// <param name="pWriteDataBuffer"> buffer which contains data to be sent</param>
      /// <param name="dwNumBytesToWrite"> Length of data buffer? [BYTE]</param>
      /// <param name="pReadDataBuffer"> buffer which contains data to be read</param>
      /// <param name="lpdwNumBytesReturned"> the data length which are read [BYTE]</param>
      /// <param name="dwTapControllerState">     </param>
      /// A read and write command.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_WriteRead")]
      public static extern JtagStatus
         WriteRead(
         int ftHandle,
         JtagRegType bInstructionTestData,
         int dwNumBitsToWrite,
         byte[] pWriteDataBuffer,
         int dwNumBytesToWrite,
         byte[] pReadDataBuffer,
         out int lpdwNumBytesReturned,
         JtagTapState dwTapControllerState
         );

      /// <summary>
      ///  Toggle TCLK
      /// <param name="ftHandle"> handle given by Open or OpenEx</param>
      /// <param name="dwNumClockPulses">true:Instruction register, false:Test register</param>
      /// 
      /// Create pluse on TCLK as dwNumClockPulses times.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_GenerateClockPulses")]
      public static extern JtagStatus
         GenerateClockPulses(
         int ftHandle,
         int dwNumClockPulses
         );

      /// <summary>
      ///  Clear command sequence
      /// 
      /// Clear the commands assembled by AddWriteCmd and AddReadCmd
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_ClearCmdSequence")]
      public static extern JtagStatus
         ClearCmdSequence();


      /// <summary>
      ///  Clear command sequence
      /// <param name="bInstructionTestData">true:Instruction register, false:Test register</param>
      /// <param name="dwNumBitsToWrite"> The numbers of data to be read [bits]</param>
      /// <param name="pWriteDataBuffer"> buffer which contains data to be sent</param>
      /// <param name="dwNumBytesToWrite"> Length of data buffer? [BYTE]</param>
      /// <param name="dwTapControllerState">     </param>
      /// Add write command to command sequence
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_AddWriteCmd")]
      public static extern JtagStatus
         AddWriteCmd(
         JtagRegType bInstructionTestData,
         int dwNumBitsToWrite,
         byte[] pWriteDataBuffer,
         int dwNumBytesToWrite,
         JtagTapState dwTapControllerState
         );

      /// <summary>
      ///  Read data stream from JTAG
      /// <param name="bInstructionTestData">true:Instruction register, false:Test register</param>
      /// <param name="dwNumBitsToRead"> The numbers of data to be read [bits]</param>
      /// <param name="dwTapControllerState">     </param>
      /// Add read command to command sequence
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_AddReadCmd")]
      public static extern JtagStatus
         AddReadCmd(
         JtagRegType bInstructionTestData,
         int dwNumBitsToRead,
         JtagTapState dwTapControllerState
         );


      /// <summary>
      ///  Read & Wriite data stream from JTAG
      /// <param name="bInstructionTestData">true:Instruction register, false:Test register</param>
      /// <param name="dwNumBitsToWriteRead"> The numbers of data to be read [bits]</param>
      /// <param name="pWriteDataBuffer"> buffer which contains data to be sent</param>
      /// <param name="dwNumBytesToWriteRead"> Length of data buffer? [BYTE]</param>
      /// <param name="dwTapControllerState">     </param>
      /// A read and write command.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_AddWriteReadCmd")]
      public static extern JtagStatus
         AddWriteReadCmd(
         JtagRegType bInstructionTestData,
         int dwNumBitsToWriteRead,
         byte[] pWriteDataBuffer,
         int dwNumBytesToWriteRead,
         JtagTapState dwTapControllerState
         );


      /// <summary>
      ///  Clear command sequence
      /// <param name="ftHandle"> handle given by Open or OpenEx</param>
      /// 
      /// Clear the commands assembled by AddWriteCmd and AddReadCmd
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_ClearDeviceCmdSequence")]
      public static extern JtagStatus
         ClearDeviceCmdSequence(
         int ftHandle
         );


      /// <summary>
      ///  Clear command sequence
      /// <param name="ftHandle"> handle given by Open or OpenEx</param>
      /// <param name="bInstructionTestData">true:Instruction register, false:Test register</param>
      /// <param name="dwNumBitsToWrite"> The numbers of data to be read [bits]</param>
      /// <param name="pWriteDataBuffer"> buffer which contains data to be sent</param>
      /// <param name="dwNumBytesToWrite"> Length of data buffer? [BYTE]</param>
      /// <param name="dwTapControllerState">     </param>
      /// Add write command to command sequence
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_AddDeviceWriteCmd")]
      public static extern JtagStatus
         AddDeviceWriteCmd(
         int ftHandle,
         JtagRegType bInstructionTestData,
         int dwNumBitsToWrite,
         byte[] pWriteDataBuffer,
         int dwNumBytesToWrite,
         JtagTapState dwTapControllerState
         );

      /// <summary>
      ///  Read data stream from JTAG
      /// <param name="ftHandle"> handle given by Open or OpenEx</param>
      /// <param name="bInstructionTestData">true:Instruction register, false:Test register</param>
      /// <param name="dwNumBitsToRead"> The numbers of data to be read [bits]</param>
      /// <param name="dwTapControllerState">     </param>
      /// Add read command to command sequence
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_AddDeviceReadCmd")]
      public static extern JtagStatus
         AddDeviceReadCmd(
         int ftHandle,
         JtagRegType bInstructionTestData,
         int dwNumBitsToRead,
         JtagTapState dwTapControllerState
         );

      /// <summary>
      ///  Read & Wriite data stream from JTAG
      /// <param name="ftHandle"> handle given by Open or OpenEx</param>
      /// <param name="bInstructionTestData">true:Instruction register, false:Test register</param>
      /// <param name="dwNumBitsToWriteRead"> The numbers of data to be read [bits]</param>
      /// <param name="pWriteDataBuffer"> buffer which contains data to be sent</param>
      /// <param name="dwNumBytesToWriteRead"> Length of data buffer? [BYTE]</param>
      /// <param name="dwTapControllerState">     </param>
      /// A read and write command.
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_AddDeviceWriteReadCmd")]
      public static extern JtagStatus
         AddDeviceWriteReadCmd(
         int ftHandle,
         JtagRegType bInstructionTestData,
         int dwNumBitsToWriteRead,
         byte[] pWriteDataBuffer,
         int dwNumBytesToWriteRead,
         JtagTapState dwTapControllerState
         );

      /// <summary>
      ///  Start to execute the command sequence by AddDeviceReadCmd and AddDeviceWriteCmd
      /// <param name="ftHandle"> handle given by Open or OpenEx</param>
      /// <param name="pReadCmdSequenceDataBuffer"> Buffer to receive the data</param>
      /// <param name="lpdwNumBytesReturned"> The numbers of data transfered [BYTE]</param>
      ///
      /// This command starts to execute the command sequence in a command buffer.  
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_ExecuteCmdSequence")]
      public static extern JtagStatus
         ExecuteCmdSequence(
         int ftHandle,
         byte[] pReadCmdSequenceDataBuffer,
         out int lpdwNumBytesReturned
         );


      /// <summary>
      ///  convert error number to massage string
      /// <param name="lpLanguage"> Language. Use "en"</param>
      /// <param name="statusCode"> given error code</param>
      /// <param name="lpErrorMessageBuffer"> Obtain error message </param>
      /// <param name="dwBufferSize"> length of message buffer</param>
      /// 
      /// generate the message string from given Status Code
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_GetErrorCodeString")]
      public static extern JtagStatus
         GetErrorCodeString(
         string lpLanguage, // Language. Use "en"
         JtagStatus statusCode, // given error code
         System.Text.StringBuilder lpErrorMessageBuffer, // Obtain error message 
         int dwBufferSize // length of message buffer
         );

      /// <summary>
      ///  get DLL Version
      /// <param name="lpDllVersionBuffer"> Obtain version string</param>
      /// <param name="dwBufferSize"> length of string</param>
      /// </summary>
      [DllImport("FTCJTAG.dll", EntryPoint = "JTAG_GetDllVersion")]
      public static extern JtagStatus
         GetDllVersion(
         System.Text.StringBuilder lpDllVersionBuffer, // Obtain version string 
         int dwBufferSize // length of string
         );
   } // JTAG
}