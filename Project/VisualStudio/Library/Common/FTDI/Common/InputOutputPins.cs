﻿using System.Runtime.InteropServices;

namespace Ftdi
{
   /// <summary>
   /// Specify state of each pin at beginning of transfering
   ///
   /// Caution. The original FTCJTAG.h had bug on the name of each member function.
   /// The name bPin4InputOutputState, is wrong name. It should be bPin3InputOutputState.
   /// (Tha bPinxInputOutputState where x must be zero origin, instead of one origin ).
   /// Below definition corrected this mistake. 
   /// </summary>
   [StructLayout(LayoutKind.Sequential)]
   public struct InputOutputPins
   {
      /// <summary>
      /// Input/Output Direction
      /// </summary>
      public PinDirection Pin0_Dir;

      /// <summary>
      /// H or L
      /// </summary>
      public LogicLevel Pin0;


      /// <summary>
      /// Input/Output Direction
      /// </summary>
      public PinDirection Pin1_Dir;

      /// <summary>
      /// H or L
      /// </summary>
      public LogicLevel Pin1;

      /// <summary>
      /// Input/Output Direction
      /// </summary>
      public PinDirection Pin2_Dir;

      /// <summary>
      /// H or L
      /// </summary>
      public LogicLevel Pin2;

      /// <summary>
      /// Input/Output Direction
      /// </summary>
      public PinDirection Pin3_Dir;

      /// <summary>
      /// H or L
      /// </summary>
      public LogicLevel Pin3;
   };
}