﻿using Desktop.Client.MessageService.Interface;
using Common.Desktop.Presentation;

namespace Desktop.Client.MainWindow.Interface
{
   public interface IMainWindow : IView, IMessageService
   {
   }
}
