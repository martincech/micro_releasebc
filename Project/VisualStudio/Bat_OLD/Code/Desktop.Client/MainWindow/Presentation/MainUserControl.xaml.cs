﻿using Common.Desktop.Applications;
using Common.Desktop.Presentation;
using Desktop.Client.ModelViews.Interface;
using Desktop.Client.NavigationService.Aplications;
using Desktop.Client.NavigationService.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop.Client.MainWindow.Presentation
{
   /// <summary>
   /// Interaction logic for Weighing.xaml
   /// </summary>
   public partial class MainUserControl : IView
   {

       private ICommand selectViewCommand;

      public MainUserControl()
      {
         InitializeComponent();
         viewListBox.SelectionChanged += viewListBox_SelectionChanged;
      }

      void viewListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
          if (viewListBox.SelectedItem == null)
          {
              DispatcherHelper.RunAsync(() =>
              {   
                  viewListBox.SelectedIndex = 0;
              });
          }
      }

     


      public ICommand SelectViewCommand
      {
          get
          {
              if (selectViewCommand == null)
              {
                  selectViewCommand = new RelayCommand<INavigationNode>(node =>
                  {

                      NavigationNode gvn = (NavigationNode)node;
                      viewListBox.SelectedItem = node;
                  },
                  node => true);
              }
              return selectViewCommand;
          }
      }
   }
}
