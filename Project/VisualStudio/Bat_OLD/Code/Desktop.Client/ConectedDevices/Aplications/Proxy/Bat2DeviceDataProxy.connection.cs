﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ServiceModel.Channels;
using System.Threading;
using Common.Services.Client;
using Connection.Interface.Contract;
using Connection.Interface.Domain;
using DataExtensions = ViewModels.DataExtensions;
using Timer = System.Timers.Timer;
using System.Collections.ObjectModel;
using ViewModels;
using ArchiveItem = Connection.Interface.Domain.ArchiveItem;
using Curve = Connection.Interface.Domain.Curve;
using Contact = Connection.Interface.Domain.Contact;
using WeighingPlan = Connection.Interface.Domain.WeighingPlan;
using WeighingConfiguration = Connection.Interface.Domain.WeighingConfiguration;

namespace Desktop.Client.ConectedDevices.Aplications.Proxy
{
   /// <summary>
   /// Implementation of communication with physical device
   /// </summary>
   public partial class Bat2DeviceDataProxy :
      IBat2DataReadEventsContract,
      IBat2DataWriteEventsContract,
      IBat2ActionEventsContract
   {
      #region Private fields

      private readonly IChannelFactory<IBat2DataCommandContract> clientFactory;
      private readonly Bat2DeviceData origin;
      private readonly Dictionary<string, bool> isCommunicatingStates;
      private readonly AutoResetEvent isLoadingIsFree;
      private readonly Timer responseTimer;
      private const int READ_TIMEOUT = 5000;
      private PropertyInfo rwProperty;
      private bool isDirty;

      #endregion

      #region Bat2DeviceData proxy properties

      /// <summary>
      /// Device configuration
      /// </summary>
      internal Configuration Configuration
      {
         get { return origin.Configuration; }
         set
         {
            origin.Configuration = value;
            MapModelFromOrigin();
            SetIsLoading(false);
         }
      }

      /// <summary>
      /// Device context
      /// </summary>
      internal Context Context
      {
         get { return origin.Context; }
         set
         {
            origin.Context = value;
            MapModelFromOrigin();
            SetIsLoading(false);
         }
      }

      /// <summary>
      /// Archive of sttistics
      /// </summary>
      internal IEnumerable<ArchiveItem> Archive
      {
         get { return origin.Archive; }
         set
         {
            origin.Archive = value;
            MapModelFromOrigin();
            SetIsLoading(false);
         }
      }

      /// <summary>
      /// List of growth curves
      /// </summary>
      internal IEnumerable<Curve> GrowthCurves
      {
         get { return origin.GrowthCurves; }
         set
         {
            origin.GrowthCurves = value;
            MapModelFromOrigin();
            SetIsLoading(false);
         }
      }

      /// <summary>
      /// List of  correction curves
      /// </summary>
      internal IEnumerable<Curve> CorrectionCurves
      {
         get { return origin.CorrectionCurves; }
         set
         {
            origin.CorrectionCurves = value;
            MapModelFromOrigin();
            SetIsLoading(false);
         }
      }

      /// <summary>
      /// List of contacts
      /// </summary>
      internal IEnumerable<Contact> Contacts
      {
         get { return origin.Contacts; }
         set
         {
            origin.Contacts = value;
            MapModelFromOrigin();
            SetIsLoading(false);
         }
      }

      /// <summary>
      /// List of weighing plans
      /// </summary>
      internal IEnumerable<WeighingPlan> WeighingPlans
      {
         get { return origin.WeighingPlans; }
         set
         {
            origin.WeighingPlans = value;
            MapModelFromOrigin();
            SetIsLoading(false);
         }
      }

      /// <summary>
      /// List of predefined weighing configurations
      /// </summary>
      internal IEnumerable<WeighingConfiguration> PredefinedWeighings
      {
         get { return origin.PredefinedWeighings; }
         set
         {
            origin.PredefinedWeighings = value;
            MapModelFromOrigin();
            SetIsLoading(false);
         }
      }

      #endregion

      #region Status methods & properties

      public bool IsBusy
      {
         get
         {
            lock (isCommunicatingStates)
            {
               return isCommunicatingStates.Any(kv => kv.Value);
            }
         }
      }

      public bool IsDirty
      {
         get { return isDirty; }
         private set { SetProperty(ref isDirty, value); }
      }

      internal void SetIsLoading<T>(bool value, Expression<Func<T>> expression)
      {
         Debug.Assert(expression != null);
         Debug.Assert(expression.Body is MemberExpression);
         SetIsLoading(value, (expression.Body as MemberExpression).Member.Name);
      }

      internal void SetIsLoading(bool value, [CallerMemberName] string propName = null)
      {
         Debug.Assert(propName != null);
         lock (isCommunicatingStates)
         {
            if (!isCommunicatingStates.ContainsKey(propName))
            {
               isCommunicatingStates.Add(propName, false);
            }
            if (value == isCommunicatingStates[propName])
            {
               // same values, do nothing
               return;
            }
         }
         if (value)
         {
            // want to set lock
            isLoadingIsFree.WaitOne();
            lock (isCommunicatingStates)
            {
               isCommunicatingStates[propName] = true;
            }
         }
         else
         {
            lock (isCommunicatingStates)
            {
               isCommunicatingStates[propName] = false;
            }
            // want to release lock
            isLoadingIsFree.Set();
         }
         RaisePropertyChanged(() => IsBusy);
      }

      internal bool GetIsLoading<T>(Expression<Func<T>> expression)
      {
         Debug.Assert(expression != null);
         Debug.Assert(expression.Body is MemberExpression);
         return GetIsLoading((expression.Body as MemberExpression).Member.Name);
      }

      internal bool GetIsLoading([CallerMemberName] string propName = null)
      {
         Debug.Assert(propName != null);
         if (propName.EndsWith("IsLoading"))
         {
            propName = propName.Replace("IsLoading", "");
         }
         if (propName.EndsWith("IsLoaded"))
         {
            propName = propName.Replace("IsLoaded", "");
         }
         lock (isCommunicatingStates)
         {
            if (!isCommunicatingStates.ContainsKey(propName))
            {
               isCommunicatingStates.Add(propName, false);
            }
            return isCommunicatingStates[propName];
         }
      }

      internal bool GetIsLoaded<T>(Expression<Func<T>> expression)
      {
         Debug.Assert(expression != null);
         Debug.Assert(expression.Body is MemberExpression);
         return GetIsLoaded((expression.Body as MemberExpression).Member.Name);
      }

      internal bool GetIsLoaded([CallerMemberName] string propName = null)
      {
         Debug.Assert(propName != null, "PropName is null!");
         if (propName.EndsWith("IsLoading"))
         {
            propName = propName.Replace("IsLoading", "");
         }
         if (propName.EndsWith("IsLoaded"))
         {
            propName = propName.Replace("IsLoaded", "");
         }
         var prop = GetType().GetProperty(propName, BindingFlags.Instance | BindingFlags.NonPublic);
         Debug.Assert(prop != null, string.Format("Non existent property {0}", propName));
         return prop.GetMethod.Invoke(this, null) != null;
      }

      #endregion

      #region Loading methods

      public void LoadProperty<T>(Expression<Func<T>> expression, bool forceRead = false)
      {
         Debug.Assert(expression != null);
         Debug.Assert(expression.Body is MemberExpression);
         LoadProperty(expression.Body as MemberExpression, forceRead);
      }

      internal void LoadProperty(string property, bool forceRead = false)
      {
         var param = Expression.Parameter(typeof (Bat2DeviceDataProxy));
         LoadProperty(Expression.Property(param, property), forceRead);
      }

      private void LoadProperty(MemberExpression memberExpression, bool forceRead)
      {
         Debug.Assert(memberExpression != null, "memberExpression", "The expression argument must not be null!");
         var propInfo = memberExpression.Member as PropertyInfo;
         Debug.Assert(propInfo != null);
         Action<IBat2DataCommandContract> callback;
         switch (propInfo.Name)
         {
            case "Configuration":
               callback = reader => reader.ReadConfig(origin);
               break;
            case "Context":
               callback = reader => reader.ReadContext(origin);
               break;
            case "Archive":
               callback = reader => reader.ReadArchive(origin);
               break;
            case "GrowthCurves":
               callback = reader => reader.ReadGrowthCurves(origin);
               break;
            case "CorrectionCurves":
               callback = reader => reader.ReadCorrectionCurves(origin);
               break;
            case "Contacts":
               callback = reader => reader.ReadContactList(origin);
               break;
            case "WeighingPlans":
               callback = reader => reader.ReadWeighingPlans(origin);
               break;
            case "PredefinedWeighings":
               callback = reader => reader.ReadPredefinedWeighings(origin);
               break;
            default:
               throw new InvalidOperationException(string.Format("LoadProperty for property {0} not supported!",
                  propInfo.Name));
         }
         DoAsyncRw(propInfo, callback, !forceRead);
      }

      #endregion

      #region Saving methods

      public void SaveProperty<T>(Expression<Func<T>> expression)
      {
         Debug.Assert(expression != null);
         Debug.Assert(expression.Body is MemberExpression);
         SaveProperty(expression.Body as MemberExpression);
      }

      internal void SaveProperty(string property)
      {
         var param = Expression.Parameter(typeof (Bat2DeviceDataProxy));
         SaveProperty(Expression.Property(param, property));
      }

      private void SaveProperty(MemberExpression memberExpression)
      {
         Debug.Assert(memberExpression != null, "memberExpression", "The expression argument must not be null!");
         var propInfo = memberExpression.Member as PropertyInfo;
         Debug.Assert(propInfo != null);
         Action<IBat2DataCommandContract> callback;
         switch (propInfo.Name)
         {
            case "Configuration":
               DataExtensions.Map(Configuration, Bat2Identification,Country,WeightUnit,Calibration,
                  DisplayConfiguration,WeighingConfiguration,Rs485Configuration,ModbusConfiguration,MegaviConfiguration);
               callback = reader => reader.WriteConfig(origin);
               break;
            case "GrowthCurves":
               origin.GrowthCurves = origin.GrowthCurves.Map(GrowthCurveList);
               callback = reader => reader.WriteGrowthCurves(origin);
               break;
            case "CorrectionCurves":
               origin.CorrectionCurves = origin.CorrectionCurves.Map(CorrectionCurveList);
               callback = reader => reader.WriteCorrectionCurves(origin);
               break;
            case "Contacts":
               origin.Contacts = origin.Contacts.Map(ContactList);
               callback = reader => reader.WriteContactList(origin);
               break;
            case "WeighingPlans":
               origin.WeighingPlans = origin.WeighingPlans.Map(WeighingPlanList);
               callback = reader => reader.WriteWeighingPlans(origin);
               break;
            case "PredefinedWeighings":
               origin.PredefinedWeighings = origin.PredefinedWeighings.Map(PredefinedWeighingList);
               callback = reader => reader.WritePredefinedWeighings(origin);
               break;
            default:
               throw new InvalidOperationException(string.Format("LoadProperty for property {0} not supported!",
                  propInfo.Name));
         }
         DoAsyncRw(propInfo, callback, false);
      }

      #endregion

      #region Data communication

      private void DoAsyncRw(PropertyInfo propInfo, Action<IBat2DataCommandContract> callback, bool isReading)
      {
         ThreadPool.QueueUserWorkItem(a =>
         {  
            if ((GetIsLoaded(propInfo.Name) && isReading)  || GetIsLoading(propInfo.Name))
            {
               return;
            }
            SetIsLoading(true, propInfo.Name); // will lock single action
            rwProperty = propInfo;
            var action = a as Action<IBat2DataCommandContract>;
            Debug.Assert(action != null);
            try
            {
               clientFactory.RemoteAction(action);
            }
            catch (Exception e)
            {
               SetIsLoading(false, propInfo.Name);
               OnStatusMessageHandler(string.Format("Can't execute the read action!\n{0}\n{1}", e.Message,
                  e.InnerException != null ? e.InnerException.Message : ""));
               return;
            }

            responseTimer.Start();
         }, callback);
      }

      #endregion

      #region Implementation of IBat2DataReadEventsContract, IBat2DataWriteEventsContract, IBat2ActionEventsContract

      /// <summary>
      /// Method is invoked when new data were read from device.
      /// </summary>
      /// <param name="newData">Data read from device</param>
      [MethodImpl(MethodImplOptions.Synchronized)]
      public void DataRead(Bat2DeviceData newData)
      {
         responseTimer.Stop();
         KeyValuePair<string, bool> loadProp;
         lock (isCommunicatingStates)
         {
            loadProp = isCommunicatingStates.FirstOrDefault(value => value.Value);
         }
         if (rwProperty == null || rwProperty.Name != loadProp.Key)
         {
            return;
         }
         var prop = typeof (Bat2DeviceData).GetProperty(loadProp.Key);
         Debug.Assert(prop != null);
         rwProperty.SetMethod.Invoke(this, new[] {prop.GetMethod.Invoke(newData, null)});
      }

      /// <summary>
      /// Method is invoked when new data were writen to device
      /// </summary>
      /// <param name="data">Data writen to device</param>
      public void DataWriten(Bat2DeviceData data)
      {
         DataRead(data);
      }

      /// <summary>
      /// Method is invoked when new action invoked on device
      /// </summary>
      /// <param name="actionString"> string to inform through message handler</param>
      private void ActionInvoked(string actionString)
      {
         Context = null;
         LoadProperty(() => Context);
         OnStatusMessageHandler(actionString);
      }

      public void TimeGet(Bat2DeviceData deviceData, DateTime time)
      {
         ActionInvoked(string.Format("device inner time: {0}", time));
      }

      public void TimeSet(Bat2DeviceData deviceData, DateTime time)
      {
         ActionInvoked(string.Format("device inner time set to: {0}", time));
      }

      public void WeighingStarted(Bat2DeviceData deviceData)
      {
         ActionInvoked("weighing started");
      }

      public void WeighingPlanned(Bat2DeviceData deviceData, DateTime at)
      {
         ActionInvoked(string.Format("weighing planned at {0}", at));
      }

      public void WeighingStopped(Bat2DeviceData deviceData)
      {
         ActionInvoked("weighing stopped");
      }

      public void WeighingSuspended(Bat2DeviceData deviceData)
      {
         ActionInvoked("weighing suspended");
      }

      public void WeighingReleased(Bat2DeviceData deviceData)
      {
         ActionInvoked("weighing released");
      }

      #endregion
   }
}