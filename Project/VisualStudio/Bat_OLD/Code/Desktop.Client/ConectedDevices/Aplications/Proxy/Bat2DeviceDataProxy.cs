using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.Channels;
using System.Threading;
using Common.Library.Observable;
using Common.Library.Messenger;
using Connection.Interface.Contract;
using Connection.Interface.Domain;
using ViewModels;
using Desktop.Client.Server;
using ArchiveItem = ViewModels.ArchiveItem;
using Contact = ViewModels.Contact;
using Country = ViewModels.Country;
using DisplayConfiguration = ViewModels.DisplayConfiguration;
using GsmMessage = ViewModels.GsmMessage;
using Timer = System.Timers.Timer;
using WeighingConfiguration = ViewModels.WeighingConfiguration;
using WeighingPlan = ViewModels.WeighingPlan;
using System.Reflection;

namespace Desktop.Client.ConectedDevices.Aplications.Proxy
{
   public partial class Bat2DeviceDataProxy : ValidatableObservableObject //RecursiveValidatableObservableObject
   {
      #region Private fields

      private Country country;
      private WeightUnit weightUnit;
      private DisplayConfiguration displayConfiguration;
      private GsmMessage gsmMessage;
      private Bat2Identification bat2Identification;
      private EthernetConfiguration ethernetConfiguration;
      private Calibration calibration;
      private Weighing weighing;
      private WeighingConfiguration weighingConfiguration;
      private ObservableCollection<Rs485Configuration> rs485Configuration;
      private ModbusConfiguration modbusConfiguration;
      private MegaviConfiguration megaviConfiguration;
      private SmsGateConfiguration smsGateConfiguration;
      private DacsConfiguration dacsConfiguration;
      private ObservableCollection<ArchiveItem> archiveItems;
      private ObservableCollection<GrowthCurve> growthCurveList;
      private ObservableCollection<CorrectionCurve> correctionCurveList;
      private ObservableCollection<Contact> contactList;
      private ObservableCollection<WeighingConfiguration> predefinedWeighingList;
      private ObservableCollection<WeighingPlan> weighingPlanList;

      #endregion

      public Bat2DeviceDataProxy(Bat2DeviceData origin,
         IChannelFactory<IBat2DataCommandContract> clientFactory)
      {
         this.clientFactory = clientFactory;
         this.origin = origin;
         isCommunicatingStates = new Dictionary<string, bool>();
         isLoadingIsFree = new AutoResetEvent(true);
         responseTimer = new Timer(READ_TIMEOUT);
         responseTimer.Elapsed += (sender, args) => 
            DataRead(origin);

         // register physical device messages
         Messenger.Default.Register<Bat2DataReadEventsContractMessage>(this,
            origin.Configuration.VersionInfo.SerialNumber, msg => msg.ActionToInvoke(this));
         Messenger.Default.Register<Bat2DataWriteEventsContractMessage>(this,
            origin.Configuration.VersionInfo.SerialNumber, msg => msg.ActionToInvoke(this));
         Messenger.Default.Register<Bat2ActionEventsContractMessage>(this, origin.Configuration.VersionInfo.SerialNumber,
            msg => msg.ActionToInvoke(this));

         InitProperties();
         MapModelFromOrigin("");
      
         IsChanged = false;      
      }

      public bool IsChanged { get; set; }

      public uint SerialNumber
      {
         get { return origin.Configuration.VersionInfo.SerialNumber; }
      }

      public event EventHandler<String> StatusMessageHandler;

      #region Models properties

      public Country Country
      {
         get { return country; }
         private set { SetProperty(ref country, value); }
      }

      public WeightUnit WeightUnit
      {
         get { return weightUnit; }
         private set { SetProperty(ref weightUnit, value); }
      }

      public DisplayConfiguration DisplayConfiguration
      {
         get { return displayConfiguration; }
         private set { SetProperty(ref displayConfiguration, value); }
      }

      public GsmMessage GsmMessage
      {
         get { return gsmMessage; }
         private set { SetProperty(ref gsmMessage, value); }
      }   

      public EthernetConfiguration EthernetConfiguration
      {
         get { return ethernetConfiguration; }
         private set { SetProperty(ref ethernetConfiguration, value); }
      }

      public Bat2Identification Bat2Identification
      {
         get { return bat2Identification; }
         private set { SetProperty(ref bat2Identification, value); }
      }

      public Calibration Calibration
      {
         get { return calibration; }
         private set { SetProperty(ref calibration, value); }
      }

      public Weighing Weighing
      {
         get { return weighing; }
         private set { SetProperty(ref weighing, value); }
      }

      public WeighingConfiguration WeighingConfiguration
      {
         get { return weighingConfiguration; }
         private set { SetProperty(ref weighingConfiguration, value); }
      }

      public ObservableCollection<Rs485Configuration> Rs485Configuration
      {
         get { return rs485Configuration; }
         private set { SetProperty(ref rs485Configuration, value); }
      }

      public ModbusConfiguration ModbusConfiguration
      {
         get { return modbusConfiguration; }
         private set { SetProperty(ref modbusConfiguration, value); }
      }

      public MegaviConfiguration MegaviConfiguration
      {
         get { return megaviConfiguration; }
         private set { SetProperty(ref megaviConfiguration, value); }
      }

      public SmsGateConfiguration SmsGateConfiguration
      {
         get { return smsGateConfiguration; }
         private set { SetProperty(ref smsGateConfiguration, value); }
      }

      public DacsConfiguration DacsConfiguration
      {
         get { return dacsConfiguration; }
         private set { SetProperty(ref dacsConfiguration, value); }
      }

      public ObservableCollection<ArchiveItem> ArchiveItems
      {
         get { return archiveItems; }
         private set { SetProperty(ref archiveItems, value); }
      }

      public ObservableCollection<GrowthCurve> GrowthCurveList
      {
         get { return growthCurveList; }
         private set { SetProperty(ref growthCurveList, value); }
      }

      public ObservableCollection<CorrectionCurve> CorrectionCurveList
      {
         get { return correctionCurveList; }
         private set { SetProperty(ref correctionCurveList, value); }
      }

      public ObservableCollection<Contact> ContactList
      {
         get { return contactList; }
         private set { SetProperty(ref contactList, value); }
      }     

      public ObservableCollection<WeighingPlan> WeighingPlanList
      {
         get { return weighingPlanList; }
         private set { SetProperty(ref weighingPlanList, value); }
      }

      public ObservableCollection<WeighingConfiguration> PredefinedWeighingList
      {
         get { return predefinedWeighingList; }
         private set { SetProperty(ref predefinedWeighingList, value); }
      }

      #endregion

   }
}