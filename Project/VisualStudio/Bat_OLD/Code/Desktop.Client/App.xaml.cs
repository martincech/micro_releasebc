﻿using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Windows;
using Connection;
using Connection.Interface.Contract;
using Common.Desktop.Applications;
using Desktop.Client.ConectedDevices.Aplications;
using Desktop.Client.ConectedDevices.Aplications.Proxy;
using Desktop.Client.ConectedDevices.Interface;
using Desktop.Client.MainWindow.Aplications;
using Desktop.Client.MessageService.Interface;
using Desktop.Client.NavigationService.Aplications;
using Desktop.Client.NavigationService.Interface;
using Desktop.Client.ToolBarService.Interface;
using Common.Desktop.Presentation;
using Ninject;
using System;
using Desktop.Client.Server;
using System.ServiceModel.Description;

namespace Desktop.Client
{
   public partial class App
   {
      private IKernel ninjectKernel;

      protected override void OnStartup(StartupEventArgs e)
      {
         base.OnStartup(e);
         ninjectKernel = new StandardKernel();
         AddBindings();

          
         // Show Main window
         DispatcherHelper.Initialize();
         MainWindow = ninjectKernel.Get<Window>();
         Debug.Assert(MainWindow != null);
         MainWindow.Show();
      }

      private void AddBindings()
      {
         // Load modules
         //ninjectKernel.Load(new[] {new ConnectedDevicesModule()});

         ninjectKernel.Bind<INavigationService, NavigationServiceViewModel>()
            .To<NavigationNodeViewModel>()
            .InSingletonScope();
         ninjectKernel.Bind<ViewModel<Bat2DeviceDataProxy>>().To<ConnectedDevicesViewModel>().InSingletonScope();
         ninjectKernel.Bind<ISelectDeviceService>().To<SelectDeviceService>();

         ninjectKernel.Bind<IToolBarService, ViewModel>().To<MainWindowViewModel>().InSingletonScope()
            .OnActivation(x =>
            {
               x.ConnectedDevices = ninjectKernel.Get<ViewModel<Bat2DeviceDataProxy>>();
               x.NavigationNodes = ninjectKernel.Get<NavigationServiceViewModel>();
            });
         ninjectKernel.Bind<IMessageService, Window>().To<MainWindow.Presentation.MainWindow>().InSingletonScope()
            .OnActivation(x => x.DataContext = ninjectKernel.Get<ViewModel>());

         ninjectKernel.Bind<IChannelFactory<IBat2CommandContract>, ChannelFactory<IBat2CommandContract>>()
            .ToConstant(new ChannelFactory<IBat2CommandContract>("IBat2CommandContract_netPipe"));
         ninjectKernel.Bind<IChannelFactory<IBat2DataCommandContract>, ChannelFactory<IBat2DataCommandContract>>()
            .ToConstant(new ChannelFactory<IBat2DataCommandContract>("IBat2DataCommandContract_netPipe"));
      }
   }
}
