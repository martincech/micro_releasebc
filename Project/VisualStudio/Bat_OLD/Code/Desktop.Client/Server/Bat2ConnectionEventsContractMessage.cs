﻿using System;
using Connection.Interface.Contract;

namespace Desktop.Client.Server
{
   internal class Bat2ConnectionEventsContractMessage
   {

      public Bat2ConnectionEventsContractMessage(Action<IBat2ConnectionEventsContract> actionToInvoke)
      {
         ActionToInvoke = actionToInvoke;
      }
      public Action<IBat2ConnectionEventsContract> ActionToInvoke { get; private set; }
   }
}
