﻿using System;
using Connection.Interface.Contract;

namespace Desktop.Client.Server
{
   internal class Bat2DataWriteEventsContractMessage
   {

      public Bat2DataWriteEventsContractMessage(Action<IBat2DataWriteEventsContract> actionToInvoke)
      {
         ActionToInvoke = actionToInvoke;
      }
      public Action<IBat2DataWriteEventsContract> ActionToInvoke { get; private set; }
   }
}
