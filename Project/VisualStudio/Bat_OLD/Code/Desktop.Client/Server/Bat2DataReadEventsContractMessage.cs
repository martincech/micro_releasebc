﻿using System;
using Connection.Interface.Contract;

namespace Desktop.Client.Server
{
   internal class Bat2DataReadEventsContractMessage
   {

      public Bat2DataReadEventsContractMessage(Action<IBat2DataReadEventsContract> actionToInvoke)
      {
         ActionToInvoke = actionToInvoke;
      }
      public Action<IBat2DataReadEventsContract> ActionToInvoke { get; private set; }
   }
}
