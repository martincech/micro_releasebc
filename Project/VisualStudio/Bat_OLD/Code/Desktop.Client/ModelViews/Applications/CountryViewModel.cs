﻿using System;
using System.ComponentModel;
using System.Globalization;
using Bat2Library;
using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Applications
{
   public class CountryViewModel : ViewModel<Country>
   {
      #region Private fields

      private string dateExample;
      private string timeExample;
      private CountryLocale prevModelLocale;

      #endregion

      #region Public interface

      #region Constructors

      public CountryViewModel(
         ICountryDetailView view)
         : base(view)
      {
         PropertyChanged += CheckModelChanged;
      }

      private void CheckModelChanged(object sender, PropertyChangedEventArgs e)
      {
         if (e.PropertyName == "Model")
         {
            if (sender == this)
            {
               if (prevModelLocale != null)
               {
                  prevModelLocale.PropertyChanged -= ModelLocalePropertyChanged;
               }
               if (Model != null)
               {
                  Model.Locale.PropertyChanged += ModelLocalePropertyChanged;
                  prevModelLocale = Model.Locale;
                  UpdateDateExample();
                  UpdateTimeExample();
               }
            }
            UpdateCommands();
         }
         if (e.PropertyName == "View")
         {
            UpdateCommands();
         }
      }

      private void ModelLocalePropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if (e.PropertyName == "LocaleTimeSeparator" || e.PropertyName == "TimeFormat" || e.PropertyName == "Separators")
         {
            UpdateTimeExample();
         }
         if (e.PropertyName == "LocaleDateSeparator1" || e.PropertyName == "LocaleDateSeparator2"
            || e.PropertyName == "DateFormat" || e.PropertyName == "Separators")
         {
            UpdateDateExample();
         }
      }

      #endregion

      /// <summary>
      /// Example of date with current settings.
      /// </summary>
      public String DateFormater
      {
         get { return dateExample; }
         private set
         {
            SetProperty(ref dateExample, value);
            RaisePropertyChanged(() => DateTimeFormater);
         }
      }

      /// <summary>
      /// Example of time with current settings.
      /// </summary>
      public String TimeFormater
      {
         get { return timeExample; }
         private set
         {
            SetProperty(ref timeExample, value);
            RaisePropertyChanged(() => DateTimeFormater);
         }
      }

      /// <summary>
      /// Concatenation of
      /// <see cref="DateFormater"/> <see cref="TimeFormater"/> with ' '(space) in between
      ///  </summary>
      public String DateTimeFormater
      {
         get { return dateExample + " " + timeExample; }
      }

      #endregion

      #region Private helpers

      #region Command helpers

      private void UpdateCommands()
      {
      }

      #endregion

      /// <summary>
      /// Update example date string
      /// </summary>
      private void UpdateDateExample()
      {
         if (Model == null || Model.Locale == null)
         {
            return;
         }
         var fSep = "'" + Model.Locale.LocaleDateSeparator1.ToString(CultureInfo.InvariantCulture) + "'";
         var sSep = "'" + Model.Locale.LocaleDateSeparator2.ToString(CultureInfo.InvariantCulture) + "'";
         switch (Model.Locale.DateFormat)
         {
            case DateFormatE.DATE_FORMAT_DDMMYYYY:
               DateFormater = "dd" + fSep + "MM" + sSep + "yy";
               break;
            case DateFormatE.DATE_FORMAT_MMDDYYYY:
               DateFormater = "MM" + fSep + "dd" + sSep + "yy";
               break;
            case DateFormatE.DATE_FORMAT_YYYYMMDD:
               DateFormater = "yy" + fSep + "MM" + sSep + "dd";
               break;
            case DateFormatE.DATE_FORMAT_YYYYDDMM:
               DateFormater = "yy" + fSep + "dd" + sSep + "MM";
               break;
            case DateFormatE.DATE_FORMAT_DDMMMYYYY:
               DateFormater = "dd" + fSep + "MMM" + sSep + "yy";
               break;
            case DateFormatE.DATE_FORMAT_MMMDDYYYY:
               DateFormater = "MMM" + fSep + "dd" + sSep + "yy";
               break;
            case DateFormatE.DATE_FORMAT_YYYYMMMDD:
               DateFormater = "yy" + fSep + "MMM" + sSep + "dd";
               break;
            case DateFormatE.DATE_FORMAT_YYYYDDMMM:
               DateFormater = "yy" + fSep + "dd" + sSep + "MMM";
               break;
         }
      }

      /// <summary>
      /// update example time string
      /// </summary>
      private void UpdateTimeExample()
      {
         if (Model == null || Model.Locale == null)
         {
            return;
         }
         var sep = "'" + Model.Locale.LocaleTimeSeparator.ToString(CultureInfo.InvariantCulture) + "'";
         switch (Model.Locale.TimeFormat)
         {
            case TimeFormatE.TIME_FORMAT_12:
               TimeFormater = "hh" + sep + "mmtt";
               break;
            case TimeFormatE.TIME_FORMAT_24:
               TimeFormater = "HH" + sep + "mm";
               break;
         }
      }

      #endregion
   }
}