﻿using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.ComponentModel.Composition;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<Rs485Configuration>))]
   public class Rs485ConfigurationViewModel : ViewModel<Rs485Configuration>
   {
      #region Private fields

      private ModbusConfiguration modbusConfiguration;
      private MegaviConfiguration megaviConfiguration;
      private SmsGateConfiguration smsGateConfiguration;
      private DacsConfiguration dacsConfiguration;

      #endregion

      #region Public interfaces

      #region Constructors

      [ImportingConstructor]
      public Rs485ConfigurationViewModel(IRs485ConfigurationView view = null)
         : base(view)
      {       
      }

      #endregion

      #region Selected model properties

      public ModbusConfiguration ModbusConfiguration
      {
         get { return modbusConfiguration; }
         set { SetProperty(ref modbusConfiguration, value); }
      }

      public MegaviConfiguration MegaviConfiguration
      {
         get { return megaviConfiguration; }
         set { SetProperty(ref megaviConfiguration, value); }
      }

      public SmsGateConfiguration SmsGateConfiguration
      {
         get { return smsGateConfiguration; }
         set { SetProperty(ref smsGateConfiguration, value); }
      }

      public DacsConfiguration DacsConfiguration
      {
         get { return dacsConfiguration; }
         set { SetProperty(ref dacsConfiguration, value); }
      }

      #endregion

      #endregion
   }
}
