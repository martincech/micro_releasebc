﻿using System.ComponentModel.Composition;
using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.Windows.Input;
using System.Collections.Generic;





namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<ArchiveItem>))]
   public class ArchiveItemViewModel : ViewModel<ArchiveItem>
   {
      #region Private fields
     
      private RelayCommand addModelCommand;
      private RelayCommand<object> deleteModelCommand;

      #endregion

      #region Public interfaces

      #region Constructors

      [ImportingConstructor]
      public ArchiveItemViewModel(
         IArchiveItemView view)
         : base(view)
      {
      }

      #endregion

      #region Commands for models

      /// <summary>
      /// Add new <see cref="ViewModels.ArchiveItem"/> to <see cref="Models"/> collection and map this object properties to newly created object.
      /// <see cref="ViewModels.ArchiveItem"/> will be set tho newly created object.
      /// </summary>
      public ICommand AddModelCommand
      {
         get
         {
            if (addModelCommand == null)
            {
               addModelCommand = new RelayCommand(
                  () =>
                  {
                     var conf = new ArchiveItem();
                     AddModelWithOptionalSelect(conf, true);
                     Validate();
                  });
            }
            return addModelCommand;
         }
      }

      /// <summary>
      /// Delete <see cref="ViewModels.ArchiveItem"/> or collection of <see cref="ViewModels.ArchiveItem"/> from <see cref="Models"/> collection. When parameter is null than <see cref="Model"/>
      /// is deleted (have to be non-null), when parameter is non-null than this object or objects are deleted (have to be one of <see cref="Models"/> items).
      /// </summary>
      public ICommand DeleteModelCommand
      {
         get
         {
            if (deleteModelCommand == null)
            {
               deleteModelCommand = new RelayCommand<object>(
                  c =>
                  {
                     IEnumerable<ArchiveItem> selList = null;
                     if (c == null)
                     {
                        selList = new List<ArchiveItem>(SelectedModels);
                     }
                     else if (c is IEnumerable<ArchiveItem>)
                     {
                        selList = c as IEnumerable<ArchiveItem>;
                     }

                     if (selList != null)
                     {
                        foreach (var o in selList)
                        {
                           DeleteModelWithOptionalMove(o, true);
                        }
                     }
                     else
                     {
                        DeleteModelWithOptionalMove(c as ArchiveItem, true);
                     }
                     Validate();
                  },
                  c =>
                  {
                     if (c == null)
                     {
                        return Model != null;
                     }
                     return Models.Contains(c as ArchiveItem);
                  });
            }
            return deleteModelCommand;
         }
      }

      #endregion

      #endregion
   }
}
