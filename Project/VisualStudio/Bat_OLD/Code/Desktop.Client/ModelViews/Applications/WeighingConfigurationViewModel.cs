﻿using Bat2Library;
using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Windows.Input;
using System;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<WeighingConfiguration>))]
   public class WeighingConfigurationViewModel : ViewModel<WeighingConfiguration>
   {
      #region Private fields

      private RelayCommand addModelCommand;
      private RelayCommand<object> deleteModelCommand;

      private RelayCommand copyPredefinedWeighingCommand;
      private bool isPredefinedWeighing;

      //Support for WeighingDaysMaskE enum multiselect
      private ObservableCollection<WeighingConfigurationMenuMaskE> selectedMenuMask;
      private bool recalcCollection;

      private ObservableCollection<WeighingPlan> weighingPlans;
      private ObservableCollection<CorrectionCurve> correctionCurves;     
      private ObservableCollection<GrowthCurve> growthCurves;
      private ObservableCollection<WeighingConfiguration> predefinedWeighing;

      #endregion

      #region Public interface

      #region Constructos

      [ImportingConstructor]
      public WeighingConfigurationViewModel(IWeighingConfigurationView view)
         : base(view)
      {
         PropertyChanged += WeighingConfigurationViewModel_PropertyChanged;
      
         SelectedMenuMask = new ObservableCollection<WeighingConfigurationMenuMaskE>();
         SelectedMenuMask.CollectionChanged += SelectedMenuMaskChanged;
         recalcCollection = true;

         IsPredefinedWeighing = true;       
      }

      [ImportingConstructor]
      public WeighingConfigurationViewModel(
          IWeighingConfigurationView view, 
          IEnumerable<WeighingPlan> WeighingPlans,
          IEnumerable<Curve> CorrectionCurves,
          IEnumerable<Curve> GrowthCurves,
          IEnumerable<WeighingConfiguration> PredefinedWeighing)
          : base(view)
      {
          PropertyChanged += WeighingConfigurationViewModel_PropertyChanged;

          SelectedMenuMask = new ObservableCollection<WeighingConfigurationMenuMaskE>();
          SelectedMenuMask.CollectionChanged += SelectedMenuMaskChanged;
          recalcCollection = true;

          this.CorrectionCurves = CorrectionCurves as ObservableCollection<CorrectionCurve>;
          this.GrowthCurves = GrowthCurves as ObservableCollection<GrowthCurve>;
          this.WeighingPlans = WeighingPlans as ObservableCollection<WeighingPlan>;
          this.PredefinedWeighing = PredefinedWeighing as ObservableCollection<WeighingConfiguration>;

          IsPredefinedWeighing = true;       
      }

      #endregion

      #region Selected model properties

      public WeighingConfigurationMenuMaskE MenuMask
      {
         get { return Model == null? 0 : Model.MenuMask; }
         set
         {
            if (Model == null)
            {
               return;
            }
            Model.MenuMask = value;
            if (recalcCollection)
            {
               SelectedMenuMask.CollectionChanged -= SelectedMenuMaskChanged;
               SelectedMenuMask.Clear();
               foreach (WeighingConfigurationMenuMaskE ev in typeof(WeighingConfigurationMenuMaskE).GetEnumValues())
               {
                  if (MenuMask.HasFlag(ev))
                  {
                     SelectedMenuMask.Add(ev);
                  }
               }
               SelectedMenuMask.CollectionChanged += SelectedMenuMaskChanged;
            }
         }
      }

      /// <summary>
      /// Menu mask as collection
      /// </summary>
      public ObservableCollection<WeighingConfigurationMenuMaskE> SelectedMenuMask
      {
         get { return selectedMenuMask; }
         private set { SetProperty(ref selectedMenuMask, value); }
      }   

      public ObservableCollection<WeighingPlan> WeighingPlans
      {
          get { return weighingPlans; }
          set { SetProperty(ref weighingPlans, value); }
      }

      public ObservableCollection<CorrectionCurve> CorrectionCurves
      {
          get { return correctionCurves; }
          set { SetProperty(ref correctionCurves, value); }
      }

      public ObservableCollection<GrowthCurve> GrowthCurves
      {
         get { return growthCurves; }
         set { SetProperty(ref growthCurves, value); }
      }

      public ObservableCollection<WeighingConfiguration> PredefinedWeighing
      {
          get { return predefinedWeighing; }
          set { SetProperty(ref predefinedWeighing, value); }
      }

      public bool IsPredefinedWeighing
      {
         get { return isPredefinedWeighing; }
         set { SetProperty(ref isPredefinedWeighing, value); }
      }

      #endregion

      #region Commands for models

      /// <summary>
      /// Add new <see cref="ViewModels.WeighingConfiguration"/> to <see cref="Models"/> collection and map this object properties to newly created object.
      /// <see cref="ViewModels.WeighingConfiguration"/> will be set tho newly created object.
      /// </summary>
      public ICommand AddModelCommand
      {
         get
         {
            if (addModelCommand == null)
            {
               addModelCommand = new RelayCommand(
                  () =>
                  {
                     var conf = new WeighingConfiguration();
                     AddModelWithOptionalSelect(conf, true);
                     Validate();
                  });
            }
            return addModelCommand;
         }
      }

      /// <summary>
      /// Delete <see cref="ViewModels.WeighingConfiguration"/> or collection of <see cref="ViewModels.WeighingConfiguration"/> from <see cref="Models"/> collection. When parameter is null than <see cref="Model"/>
      /// is deleted (have to be non-null), when parameter is non-null than this object or objects are deleted (have to be one of <see cref="Models"/> items).
      /// </summary>
      public ICommand DeleteModelCommand
      {
         get
         {
            if (deleteModelCommand == null)
            {
               deleteModelCommand = new RelayCommand<object>(
                  c =>
                  {
                     IEnumerable<WeighingConfiguration> selList = null;
                     if (c == null)
                     {
                        selList = new List<WeighingConfiguration>(SelectedModels);
                     }
                     else if (c is IEnumerable<WeighingConfiguration>)
                     {
                        selList = c as IEnumerable<WeighingConfiguration>;
                     }

                     if (selList != null)
                     {
                        foreach (var o in selList)
                        {
                           DeleteModelWithOptionalMove(o, true);
                        }
                     }
                     else
                     {
                        DeleteModelWithOptionalMove(c as WeighingConfiguration, true);
                     }
                     Validate();
                  },
                  c =>
                  {
                     return true;
                     //if (c == null)
                     //{
                     //   return Model != null;
                     //}
                     //return Models.Contains(c as WeighingConfiguration);
                  });
            }
            return deleteModelCommand;
         }
      }

      #endregion

      #region Copy weighing command

      public ICommand CopyPredefinedWeighingCommand
      {
         get
         {
            if (copyPredefinedWeighingCommand == null)
            {
               copyPredefinedWeighingCommand = new RelayCommand(
                  c =>
                  {  
                     //Copy values from predefined weighing to actual model                   
                     int index = Model.PredefinedIndex;
                     //Model = PredefinedWeighing[index];

                     Model.FlockName = PredefinedWeighing[index].FlockName;
                     Model.Name = PredefinedWeighing[index].Name;
                     Model.InitialDayNumber = PredefinedWeighing[index].InitialDayNumber;
                     Model.DayStart = PredefinedWeighing[index].DayStart;
                     Model.CorrectionCurveIndex = PredefinedWeighing[index].CorrectionCurveIndex;
                     Model.Planning = PredefinedWeighing[index].Planning;
                     Model.WeighingPlanIndex = PredefinedWeighing[index].WeighingPlanIndex;                    
                     Model.MenuMask = PredefinedWeighing[index].MenuMask;

                     SelectedMenuMask.CollectionChanged -= SelectedMenuMaskChanged;
                     SelectedMenuMask.Clear();
                     foreach (WeighingConfigurationMenuMaskE ev in typeof(WeighingConfigurationMenuMaskE).GetEnumValues())
                     {
                        if (MenuMask.HasFlag(ev))
                        {
                           SelectedMenuMask.Add(ev);
                        }
                     }
                     SelectedMenuMask.CollectionChanged += SelectedMenuMaskChanged;

                     Model.PredefinedIndex = PredefinedWeighing[index].PredefinedIndex;

                     //Model.Detection = PredefinedWeighing[index].Detection;
                     Model.Detection.Step = PredefinedWeighing[index].Detection.Step;
                     Model.Detection.StabilizationRange = PredefinedWeighing[index].Detection.StabilizationRange;
                     Model.Detection.StabilizationTime = PredefinedWeighing[index].Detection.StabilizationTime;
                     Model.Detection.Filter = PredefinedWeighing[index].Detection.Filter;
                     
                     //Model.TargetWeight = PredefinedWeighing[index].TargetWeight;
                     Model.TargetWeight.OnlineAdjustment = PredefinedWeighing[index].TargetWeight.OnlineAdjustment;
                     Model.TargetWeight.Sex = PredefinedWeighing[index].TargetWeight.Sex;
                     Model.TargetWeight.SexDifferentiation = PredefinedWeighing[index].TargetWeight.SexDifferentiation;
                     Model.TargetWeight.Growth = PredefinedWeighing[index].TargetWeight.Growth;
                     Model.TargetWeight.Mode = PredefinedWeighing[index].TargetWeight.Mode;

                     //Model.MaleGender = PredefinedWeighing[index].MaleGender;
                     Model.MaleGender.InitialWeight = PredefinedWeighing[index].MaleGender.InitialWeight;
                     Model.MaleGender.GrowthCurveIndex = PredefinedWeighing[index].MaleGender.GrowthCurveIndex;
                     Model.MaleGender.StandardCurveIndex = PredefinedWeighing[index].MaleGender.StandardCurveIndex;
                     Model.MaleGender.Acceptance.MarginAbove = PredefinedWeighing[index].MaleGender.Acceptance.MarginAbove;
                     Model.MaleGender.Acceptance.MarginBelow = PredefinedWeighing[index].MaleGender.Acceptance.MarginBelow;

                     Model.FemaleGender.InitialWeight = PredefinedWeighing[index].FemaleGender.InitialWeight;
                     Model.FemaleGender.GrowthCurveIndex = PredefinedWeighing[index].FemaleGender.GrowthCurveIndex;
                     Model.FemaleGender.StandardCurveIndex = PredefinedWeighing[index].FemaleGender.StandardCurveIndex;
                     Model.FemaleGender.Acceptance.MarginAbove = PredefinedWeighing[index].FemaleGender.Acceptance.MarginAbove;
                     Model.FemaleGender.Acceptance.MarginBelow = PredefinedWeighing[index].FemaleGender.Acceptance.MarginBelow;

                     Model.Statistics.HourlyType = PredefinedWeighing[index].Statistics.HourlyType;
                     Model.Statistics.HourlyPeriod = PredefinedWeighing[index].Statistics.HourlyPeriod;
                     Model.Statistics.UniformityRange = PredefinedWeighing[index].Statistics.UniformityRange;
                     Model.Statistics.HistogramConfiguration.Step = PredefinedWeighing[index].Statistics.HistogramConfiguration.Step;
                     Model.Statistics.HistogramConfiguration.Range = PredefinedWeighing[index].Statistics.HistogramConfiguration.Range;
                     Model.Statistics.HistogramConfiguration.Mode = PredefinedWeighing[index].Statistics.HistogramConfiguration.Mode;
                  },
                  c =>
                  {
                     if (Model != null || Models != null)
                     {
                        return true;
                     }
                     return false;
                  });
            }
            return copyPredefinedWeighingCommand;
         }
      }

      #endregion

      #endregion

      #region Private helpers
      /// <summary>
      /// Property changed event handler
      /// </summary>
      /// <param name="sender">this</param>
      /// <param name="e"></param>
      private void WeighingConfigurationViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         //Debug.Assert(sender == this);
         if (e.PropertyName == "View" || e.PropertyName == "Model")
         {
            if (Model != null)
            {
               MenuMask = Model.MenuMask;
            }          
         }
      }

      private void SelectedMenuMaskChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
      {
         WeighingConfigurationMenuMaskE mask = default(WeighingConfigurationMenuMaskE);
         foreach (WeighingConfigurationMenuMaskE ev in SelectedMenuMask)
         {
            mask |= ev;
         }
         recalcCollection = false;
         MenuMask = mask;
         recalcCollection = true;
      }
      #endregion
   }
}
