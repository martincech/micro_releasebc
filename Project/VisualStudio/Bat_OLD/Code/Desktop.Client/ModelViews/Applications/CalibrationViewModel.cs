﻿using Common.Desktop.Applications;
using ViewModels;
using Desktop.Client.ModelViews.Interface;
using System.ComponentModel.Composition;

namespace Desktop.Client.ModelViews.Applications
{
   [Export, Export(typeof(ViewModel)), Export(typeof(ViewModel<Calibration>))]
   public class CalibrationViewModel : ViewModel<Calibration>
   {
      #region Public interfaces

      #region Constructors

      [ImportingConstructor]
      public CalibrationViewModel(
         ICalibrationView view)
         : base(view)
      {
      }  

      #endregion

      #endregion
   }
}
