﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for WeighingPlanListView.xaml
   /// </summary>
   public partial class WeighingPlanListView : IWeighingPlanListView
   {
      public WeighingPlanListView()
      {
         InitializeComponent();
      }
   }
}
