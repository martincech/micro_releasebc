﻿using System.ComponentModel.Composition;
using OxyPlot;
using OxyPlot.Axes;
using System;
using OxyPlot.Annotations;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;
using System.Collections.ObjectModel;
using Desktop.Client.ModelViews.Interface;
using ViewModels;
using System.Windows.Media;
using System.Windows.Controls;

namespace Desktop.Client.ModelViews.Presentation
{
    /// <summary>
    /// Interaction logic for TimeView.xaml
    /// </summary>
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class TimeView : Common.Desktop.Presentation.IView
    {
        #region Fields and properties

        public PlotModel GraphModel { get; private set; }     
        public bool HasError { get; private set; }

        private ObservableCollection<RectangleAnnotation> annotations; //list of all annotations    
        private RectangleAnnotation range;  //actual inserted annotation
        private double startx;
        private bool annotationClicked;
        private bool editAnnotation;
        private bool splitAnnotation;
        private int editAnnotationIndex;
        private double startXEdit; //original x coordinate before editing
        private bool initialize;

        private static OxyColor colorNormal = OxyColor.FromAColor(120, OxyColors.Blue);
        private static Brush defaultBorder = new SolidColorBrush(Color.FromRgb(227, 233, 239));

        // time constants
        private const int minute = 60;
        private const int hour = 60 * minute;
        private const int day = hour * 24 - 1;

        #endregion

        #region Public interface

        #region Constructors

        public TimeView()
        {
            HasError = false;
            initialize = true;
            GraphModel = new PlotModel();
            annotations = new ObservableCollection<RectangleAnnotation>();
            var dt = DataContext;
            range = new RectangleAnnotation { Fill = colorNormal, MinimumX = 0, MaximumX = 0 };
            startx = double.NaN;
            GraphModel.Annotations.Add(range);
            annotationClicked = false;
            splitAnnotation = false;

            InitAxis();
            InitializeComponent();
            var brushConvertor = new BrushConverter();
           
            // mouse events
            GraphModel.MouseDown += GraphModel_MouseDown;
            GraphModel.MouseMove += GraphModel_MouseMove;
            GraphModel.MouseUp += GraphModel_MouseUp;
           
            GraphModel.Axes[0].TransformChanged += TimeView_AxisChanged;
        }

        #endregion

        #endregion

        #region Mouse events

        private void GraphModel_MouseDown(object sender, OxyMouseDownEventArgs e)
        {
            if (e.ChangedButton == OxyMouseButton.Left && annotationClicked == false)
            {
                if (!GraphModel.Annotations.Contains(range))
                {
                    GraphModel.Annotations.Add(range);
                }
                startx = range.InverseTransform(e.Position).X;

                // round to actual minor step of x-axis     
                startx = RoundToStep(startx);
                range.MinimumX = startx;
                range.MaximumX = startx;
                GraphModel.InvalidatePlot(true);
                e.Handled = true;
            }
        }

        private void GraphModel_MouseMove(object sender, OxyMouseEventArgs e)
        {
            if (!double.IsNaN(startx))
            {
                var x = range.InverseTransform(e.Position).X;
                double minX = RoundToStep(Math.Min(x, startx));
                double maxX = RoundToStep(Math.Max(x, startx));

                range.MinimumX = (minX < 0.0) ? 0.0 : minX;
                range.MaximumX = (maxX > day) ? day : maxX;

                TimeSpan from = TimeSpan.FromSeconds(range.MinimumX);
                TimeSpan to = TimeSpan.FromSeconds(range.MaximumX);
                range.Text = String.Format("{0}\n{1}", from.ToString(@"hh\:mm"), to.ToString(@"hh\:mm"));

                GraphModel.InvalidatePlot(true);
                e.Handled = true;
            }

            if (annotationClicked == true && editAnnotationIndex < annotations.Count)
            {  // Editing annotation
                var x = annotations[editAnnotationIndex].InverseTransform(e.Position).X;
                double newX = RoundToStep(x);
                if (newX > day)
                {
                    newX = day;
                }
                if (newX < 0.0)
                {
                    newX = 0.0;
                }

                //click on last segment in annotation = edit
                if (editAnnotation == true)
                {
                    if (newX < annotations[editAnnotationIndex].MinimumX)
                    {
                        annotations[editAnnotationIndex].MaximumX = annotations[editAnnotationIndex].MinimumX;
                    }
                    else if (startXEdit != x)
                    {
                        annotations[editAnnotationIndex].MaximumX = newX;
                    }

                    TimeSpan from = TimeSpan.FromSeconds(annotations[editAnnotationIndex].MinimumX);
                    TimeSpan to = TimeSpan.FromSeconds(annotations[editAnnotationIndex].MaximumX);
                    annotations[editAnnotationIndex].Text = String.Format("{0}\n{1}", from.ToString(@"hh\:mm"), to.ToString(@"hh\:mm"));
                }

                //click on non-last segment, if move left, delete segment and create new annotation
                if (editAnnotation == false &&
                    newX <= (startXEdit - (GraphModel.Axes[0].ActualMinorStep * 0.75)) &&
                    newX >= annotations[editAnnotationIndex].MinimumX)
                {
                    if (splitAnnotation == true)
                    {  //new annotation behind editing place
                        RectangleAnnotation ann = new RectangleAnnotation
                        {
                            Fill = colorNormal,
                            MinimumX = RoundToStep(startXEdit),
                            MaximumX = annotations[editAnnotationIndex].MaximumX,
                        };
                        TimeSpan fromNew = TimeSpan.FromSeconds(ann.MinimumX);
                        TimeSpan toNew = TimeSpan.FromSeconds(ann.MaximumX);
                        ann.Text = String.Format("{0}\n{1}", fromNew.ToString(@"hh\:mm"), toNew.ToString(@"hh\:mm"));

                        AddNewAnnotation(ann);
                        AddModel(fromNew, toNew);

                        splitAnnotation = false;
                    }

                    //old reduced annotation 
                    annotations[editAnnotationIndex].MaximumX = newX;
                    TimeSpan from = TimeSpan.FromSeconds(annotations[editAnnotationIndex].MinimumX);
                    TimeSpan to = TimeSpan.FromSeconds(annotations[editAnnotationIndex].MaximumX);
                    annotations[editAnnotationIndex].Text = String.Format("{0}\n{1}", from.ToString(@"hh\:mm"), to.ToString(@"hh\:mm"));
                }

                GraphModel.InvalidatePlot(true);
            }
        }

        private void GraphModel_MouseUp(object sender, OxyMouseEventArgs e)
        {
            if (annotationClicked == true &&
               (annotations[editAnnotationIndex].MaximumX == annotations[editAnnotationIndex].MinimumX))
            {
                GraphModel.Annotations.Remove(annotations[editAnnotationIndex]);
                annotations.RemoveAt(editAnnotationIndex);
            }

            annotationClicked = false;
            editAnnotation = false;

            if (startx.Equals(double.NaN) || (range.MinimumX == range.MaximumX))
            {
                GraphModel.Annotations.Remove(range);
                CheckJoinAnnotations();
                SynchronizeModels();
                GraphModel.InvalidatePlot(true);
                startx = double.NaN;
                return;
            }

            AddNewAnnotation(null);

            TimeSpan from = TimeSpan.FromSeconds(range.MinimumX);
            TimeSpan to = TimeSpan.FromSeconds(range.MaximumX);
            AddModel(from, to);

            startx = double.NaN;
            GraphModel.Annotations.Remove(range);

            CheckJoinAnnotations();
            SynchronizeModels();
            GraphModel.InvalidatePlot(true);
        }

        private void ExistingAnnotations_MouseDown(object sender, OxyMouseDownEventArgs e)
        {
            if (e.ChangedButton == OxyMouseButton.Left)
            {
                annotationClicked = true;

                editAnnotationIndex = annotations.IndexOf((RectangleAnnotation)sender);
                startXEdit = annotations[editAnnotationIndex].InverseTransform(e.Position).X;

                if (startXEdit <= annotations[editAnnotationIndex].MaximumX &&
                   startXEdit >= (annotations[editAnnotationIndex].MaximumX - GraphModel.Axes[0].ActualMinorStep))
                {
                    editAnnotation = true;
                }

                splitAnnotation = true;
            }
        }

        /// <summary>
        /// Change graph's scale when graph is zoom in/out.
        /// </summary>
        /// object sender, EventArgs e
        /// object sender, AxisChangedEventArgs e
        private void TimeView_AxisChanged(object sender, EventArgs e)
        {
            int hourConst = 45080;
            int halHourConst = 12600;
            int quarterConst = 10350;
            int tenMinutesConst = 8000;
            int fiveMinutesConst = 2000;

            double diff = GraphModel.Axes[0].ActualMaximum - GraphModel.Axes[0].ActualMinimum;
            double step = hour;

            if (diff > hourConst)
            {
                step = hour;
            }
            else if (diff <= hourConst && diff > halHourConst)
            {
                step = minute * 30;
            }
            else if (diff <= halHourConst && diff > quarterConst)
            {
                step = minute * 15;
            }
            else if (diff <= quarterConst && diff > tenMinutesConst)
            {
                step = minute * 10;
            }
            else if (diff <= tenMinutesConst && diff > fiveMinutesConst)
            {
                step = minute * 5;
            }
            else if (diff <= fiveMinutesConst)
            {
                step = minute;
            }

            GraphModel.Axes[0].MinorStep = step;
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Test for join more annotations to one bigger 
        /// </summary>
        private void CheckJoinAnnotations()
        {
            for (int i = 1; i < annotations.Count; i++)
            {
                RectangleAnnotation before = annotations[i - 1];
                RectangleAnnotation after = annotations[i];

                if (before.MaximumX >= after.MinimumX)
                {
                    if (before.MaximumX < after.MaximumX)
                    {
                        annotations[i - 1].MaximumX = after.MaximumX;
                    }
                    TimeSpan from = TimeSpan.FromSeconds(annotations[i - 1].MinimumX);
                    TimeSpan to = TimeSpan.FromSeconds(annotations[i - 1].MaximumX);
                    annotations[i - 1].Text = String.Format("{0}\n{1}", from.ToString(@"hh\:mm"), to.ToString(@"hh\:mm"));

                    GraphModel.Annotations.Remove(annotations[i]);
                    annotations.RemoveAt(i);
                    i -= 1;
                }
            }
        }


        private void AddNewAnnotation(RectangleAnnotation insertedAnnotation)
        {
            if (insertedAnnotation == null)
            {
                insertedAnnotation = range;
            }

            RectangleAnnotation newAnnotation = new RectangleAnnotation
            {
                Fill = colorNormal,
                MinimumX = insertedAnnotation.MinimumX,
                MaximumX = insertedAnnotation.MaximumX,
                Text = insertedAnnotation.Text,
            };

            //insert new annotation to ordered list => find index where be located
            int i;
            if (annotations.Count > 0 && insertedAnnotation.MinimumX < annotations[0].MinimumX)
            {
                i = 0;
            }
            else
            {
                for (i = 1; i < annotations.Count; i++)
                {
                    RectangleAnnotation before = annotations[i - 1];
                    RectangleAnnotation after = annotations[i];

                    if (before.MinimumX <= insertedAnnotation.MinimumX &&
                        after.MinimumX > insertedAnnotation.MinimumX)
                    {
                        break;
                    }
                }
            }

            if (i >= annotations.Count)
            {
                annotations.Add(newAnnotation);
                annotations[annotations.Count - 1].MouseDown += ExistingAnnotations_MouseDown;
                GraphModel.Annotations.Add(annotations[annotations.Count - 1]);
            }
            else
            {
                annotations.Insert(i, newAnnotation);
                annotations[i].MouseDown += ExistingAnnotations_MouseDown;
                GraphModel.Annotations.Insert(i, annotations[i]);
            }

            /*
            //add triangle annotation for all annotations to last segment
            foreach (RectangleAnnotation ann in annotations)
            {
               double x = ann.MaximumX;
               double step = GraphModel.Axes[0].ActualMinorStep;
         

               PolygonAnnotation polygon = new PolygonAnnotation 
               { 
                  Fill = OxyColors.DarkBlue,
               };
               polygon.Points.AddRange(new[] { new DataPoint(x - step, 0), new DataPoint(x, 8), new DataPoint(x, 0) });          
          

               GraphModel.Annotations.Add(polygon);
            }
            */
        }

        /// <summary>
        /// Round to selected step
        /// </summary>
        /// <param name="number">number which will be round</param>
        /// <param name="stepParameter">if null, step use actual minor step of x axis</param>
        /// <returns>rounded number</returns>
        private double RoundToStep(double number, double? stepParameter = null)
        {
            double step = stepParameter ?? GraphModel.Axes[0].ActualMinorStep;
            double rest = number % step;
            if (rest < (step * 0.75))
            {
                number -= rest;
            }
            else
            {
                number += (step - rest);
            }

            return number;
        }


        private void InitAxis()
        {
            if (GraphModel == null)
            {
                return;
            }
            // x-axis
            GraphModel.Axes.Add(new TimeSpanAxis
            {
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Dot,
                Position = AxisPosition.Bottom,
                StringFormat = "h:mm",
                AbsoluteMinimum = 0,
                AbsoluteMaximum = hour * 24,
                Maximum = hour * 24,
                MinimumRange = minute * 10,
                MinorStep = hour
            });

            // y-axis (invisible)
            GraphModel.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Left,
                AbsoluteMaximum = 100,
                AbsoluteMinimum = 0,
                IsZoomEnabled = false,
                IsAxisVisible = false
            });
        }

        /// <summary>
        /// Click event on add new time model.
        /// </summary>
        private void AddTimeModel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            TimeSpan from = new TimeSpan(timePickerFrom.Value.Value.Hours, timePickerFrom.Value.Value.Minutes, 0);
            TimeSpan to = new TimeSpan(timePickerTo.Value.Value.Hours, timePickerTo.Value.Value.Minutes, 0);
            int annotationsCount = annotations.Count;

            if (!ViewModelHasErrors())
            {
                AddNewAnnotation(CreateAnnotation(from, to));
                CheckJoinAnnotations();
                GraphModel.InvalidatePlot(true);

                // if annotations count changed, add new model, else synchronize model's properties
                if (annotationsCount < annotations.Count)
                {
                    AddModel(from, to);
                }
                else
                {
                    SynchronizeModels();
                }
            }
        }

        /// <summary>
        /// Click event on delete time model.
        /// </summary>
        private void DeleteTimeModel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            TimeSpan delTimeFrom = new TimeSpan(timePickerFrom.Value.Value.Hours, timePickerFrom.Value.Value.Minutes, 0);
            TimeSpan delTimeTo = new TimeSpan(timePickerTo.Value.Value.Hours, timePickerTo.Value.Value.Minutes, 0);

            double delFrom = delTimeFrom.TotalSeconds;
            double delTo = delTimeTo.TotalSeconds;

            if (!ViewModelHasErrors())
            {
                for (int i = annotations.Count - 1; i >= 0; i--)
                {
                    if (annotations[i].MaximumX <= delTo && annotations[i].MinimumX >= delFrom)
                    {  //delete whole annotation
                        GraphModel.Annotations.Remove(annotations[i]);
                        annotations.RemoveAt(i);
                    }
                    else if (annotations[i].MaximumX > delTo && annotations[i].MinimumX < delTo)
                    {
                        //edit end of current annotation
                        double min = annotations[i].MinimumX;
                        annotations[i].MinimumX = delTo;
                        annotations[i].Text = String.Format("{0}\n{1}", delTimeTo.ToString(@"hh\:mm"), TimeSpan.FromSeconds(annotations[i].MaximumX).ToString(@"hh\:mm"));

                        if (min < delFrom)
                        {  //delete middle of annotation => create new annotation
                            annotations.Insert(i, CreateAnnotation(TimeSpan.FromSeconds(min), delTimeFrom));
                            AddModel(TimeSpan.FromSeconds(min), delTimeFrom);
                            annotations[i].MouseDown += ExistingAnnotations_MouseDown;
                            GraphModel.Annotations.Insert(i, annotations[i]);
                        }
                    }
                    else if (annotations[i].MinimumX < delFrom)
                    {  //delete end part of current annotation
                        annotations[i].MaximumX = delFrom;
                        annotations[i].Text = String.Format("{0}\n{1}", TimeSpan.FromSeconds(annotations[i].MinimumX).ToString(@"hh\:mm"), delTimeFrom.ToString(@"hh\:mm"));
                    }
                }

                SynchronizeModels();
                GraphModel.InvalidatePlot(true);
            }
        }

        /// <summary>
        /// Create new annotation from selected time period.
        /// </summary>
        /// <param name="from">start value</param>
        /// <param name="to">stop value</param>
        /// <returns>created annotation</returns>
        private RectangleAnnotation CreateAnnotation(TimeSpan from, TimeSpan to)
        {
            //SetTimePropetiesInViewModel(from, to);
            double fromNumber = from.TotalSeconds;
            double toNumber = to.TotalSeconds;

            RectangleAnnotation ann = new RectangleAnnotation
            {
                Fill = colorNormal,
                MinimumX = fromNumber,
                MaximumX = toNumber,
            };
            ann.Text = String.Format("{0}\n{1}", from.ToString(@"hh\:mm"), to.ToString(@"hh\:mm"));
            return ann;
        }

        private void DataContextAddItem<T>(TimeSpan from, TimeSpan to) where T : Time, new()
        {
            ObservableCollection<T> list = DataContext as ObservableCollection<T>;
            T newTime = new T();
            newTime.From = new DateTime(1, 1, 1, from.Hours, from.Minutes, 0);
            newTime.To = new DateTime(1, 1, 1, to.Hours, to.Minutes, 0);

            list.Add(newTime);
            DataContext = list;
        }

        /// <summary>
        /// Call method in view model to add new model.
        /// </summary>
        private void AddModel(TimeSpan from, TimeSpan to)
        {         
            if (DataContext is ObservableCollection<ViewModels.GsmPowerOptionTime>)
            {             
                DataContextAddItem<ViewModels.GsmPowerOptionTime>(from, to);
            }
            else if (DataContext is ObservableCollection<ViewModels.WeighingTime>)
            {
                DataContextAddItem<ViewModels.WeighingTime>(from, to);
            }
        }
      
        private void SynchronizeModels()
        {
            List<DateTime> listFrom = new List<DateTime>();
            List<DateTime> listTo = new List<DateTime>();
            GetTimesLists(ref listFrom, ref listTo);         

            if (DataContext is ObservableCollection<ViewModels.GsmPowerOptionTime>)
            {
                ObservableCollection<ViewModels.GsmPowerOptionTime> list = DataContext as ObservableCollection<ViewModels.GsmPowerOptionTime>;
                int i;
                for (i = 0; i < listFrom.Count; i++)
                {   //update list of times by annotations in graph
                    list[i].From = listFrom[i];
                    list[i].To = listTo[i];
                }

                //delete redundant times
                while (i < list.Count)
                {
                    list.RemoveAt(i);                  
                }

                DataContext = list;
            }
            else if (DataContext is ObservableCollection<ViewModels.WeighingTime>)
            {
                ObservableCollection<ViewModels.WeighingTime> list = DataContext as ObservableCollection<ViewModels.WeighingTime>;
                int i;
                for (i = 0; i < listFrom.Count; i++)
                {   //update list of times by annotations in graph
                    list[i].From = listFrom[i];
                    list[i].To = listTo[i];
                }

                //delete redundant times
                while (i < list.Count)
                {
                    list.RemoveAt(i);
                }

                DataContext = list;
            }

        }

        /// <summary>
        /// Return lists of time interval used in annotations.
        /// </summary>
        private void GetTimesLists(ref List<DateTime> listFrom, ref List<DateTime> listTo)
        {
            listFrom.Clear();
            listTo.Clear();

            foreach (RectangleAnnotation ann in annotations)
            {
                TimeSpan timeFrom = TimeSpan.FromSeconds(ann.MinimumX);
                DateTime dateFrom = new DateTime(1, 1, 1, timeFrom.Hours, timeFrom.Minutes, 0);

                TimeSpan timeTo = TimeSpan.FromSeconds(ann.MaximumX);
                DateTime dateTo = new DateTime(1, 1, 1, timeTo.Hours, timeTo.Minutes, 0);

                listFrom.Add(dateFrom);
                listTo.Add(dateTo);
            }
        }

        /// <summary>
        /// Get flag presents view model validation result.
        /// </summary>
        /// <returns>true - error in validation; false - no errors</returns>      
        private bool ViewModelHasErrors()
        {        
            DateTime testFrom = new DateTime(1, 1, 1, timePickerFrom.Value.Value.Hours, timePickerFrom.Value.Value.Minutes, 0);
            DateTime testTo = new DateTime(1, 1, 1, timePickerTo.Value.Value.Hours, timePickerTo.Value.Value.Minutes, 0);

            if (DateTime.Compare(testFrom, testTo) < 0)
            {
                HasError = false;
                return false;
            }
            else
            {
                HasError = true;
                return true;
            }
        }      

        /// <summary>
        /// Load initialize time range values to graph.
        /// </summary>
        private void TimeViewGraph_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            if (initialize == false)
            {  //Change model => delete graph and load initialize data
               GraphModel.Annotations.Clear();
               annotations.Clear();
               //return;
            }
            initialize = false;

            //get list of time range
            ObservableCollection<ViewModels.GsmPowerOptionTime> time1 = e.NewValue as ObservableCollection<ViewModels.GsmPowerOptionTime>;                      
            ObservableCollection<ViewModels.WeighingTime> time2 = e.NewValue as ObservableCollection<ViewModels.WeighingTime>;

            ObservableCollection<ViewModels.Time> list = new ObservableCollection<Time>();

            if (time1 == null && time2 != null)
            {
                foreach(Time t in time2)
                {
                    list.Add(t);
                }
            }
            else if (time1 != null)
            {
                foreach (Time t in time1)
                {
                    list.Add(t);
                }
            }

            //add time to graph
            RectangleAnnotation ann = new RectangleAnnotation();
            ann.Fill = colorNormal;
      
            foreach(Time time in list)
            {            
                TimeSpan fromTime = new TimeSpan(time.From.Hour, time.From.Minute, time.From.Second);
                TimeSpan toTime = new TimeSpan(time.To.Hour, time.To.Minute, time.To.Second);

                ann.MinimumX = fromTime.TotalSeconds;
                ann.MaximumX = toTime.TotalSeconds;
                ann.Text = String.Format("{0}\n{1}", fromTime.ToString(@"hh\:mm"), toTime.ToString(@"hh\:mm"));

                AddNewAnnotation(ann);
            }

            GraphModel.InvalidatePlot(true);
        }

        private void timePicker_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<object> e)
        {
            if (timePickerTo == null || timePickerFrom == null)
            {
                return;
            }

            if (ViewModelHasErrors())
            {
                timePickerFrom.BorderBrush = Brushes.Red;
                timePickerTo.BorderBrush = Brushes.Red;

                ToolTip toolTipErr = new ToolTip();
                toolTipErr.Content = "Time From is later than To!";
                timePickerFrom.ToolTip = toolTipErr;
                timePickerTo.ToolTip = toolTipErr;
            }
            else
            {
                timePickerFrom.BorderBrush = defaultBorder;
                timePickerTo.BorderBrush = defaultBorder;

                timePickerFrom.ToolTip = null;
                timePickerTo.ToolTip = null;
            }
        }

        #endregion
    }
}
