﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for Rs485ConfigurationView.xaml
   /// </summary>
   public partial class Rs485ConfigurationView : IRs485ConfigurationDetailView
   {
      public Rs485ConfigurationView()
      {
         InitializeComponent();
      }
   }
}
