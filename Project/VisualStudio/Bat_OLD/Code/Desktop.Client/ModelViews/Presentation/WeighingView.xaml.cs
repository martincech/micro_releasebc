﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for WeighingView.xaml
   /// </summary>
   public partial class WeighingView : IWeighingDetailView
   {
      public WeighingView()
      {
         InitializeComponent();
      }
   }
}
