﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for MegaviConfigurationView.xaml
   /// </summary>
   public partial class MegaviConfigurationView : IMegaviConfigurationDetailView
   {
      public MegaviConfigurationView()
      {
         InitializeComponent();
      }
   }
}
