﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for WeighingConfigurationListView.xaml
   /// </summary>
   public partial class WeighingConfigurationListView : IWeighingConfigurationListView
   {
      public WeighingConfigurationListView()
      {
         InitializeComponent();
      }
   }
}
