﻿using Desktop.Client.ModelViews.Interface;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for Bat2IdentificationView.xaml
   /// </summary>
   public partial class Bat2IdentificationView : IBat2IdentificationDetailView
   {
      public Bat2IdentificationView()
      {
         InitializeComponent();
      }
   }
}
