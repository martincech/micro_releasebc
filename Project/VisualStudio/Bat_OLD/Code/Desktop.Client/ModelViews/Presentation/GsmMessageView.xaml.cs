﻿using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using System;
using System.Linq;
using System.Collections.ObjectModel;

namespace Desktop.Client.ModelViews.Presentation
{
   /// <summary>
   /// Interaction logic for GsmMessageView.xaml
   /// </summary>
   public partial class GsmMessageView : IGsmMessageDetailView
   {
      public GsmMessageView()
      {
         InitializeComponent();            
      }

      private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
      {
         GsmMessageViewModel vm = DataContext as GsmMessageViewModel;
         vm.Model = vm.Models.FirstOrDefault();
      }     
   }
}
