﻿using System;
using System.Windows.Data;

namespace Desktop.Client.ModelViews.Converters
{
   public class EditIndexConverter : IValueConverter
   {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         int index;
         if (!int.TryParse(value.ToString(), out index) || index == 255)
         {
            return 0;
         }

         return index + 1;        
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         int index;
         if (!int.TryParse(value.ToString(), out index) || index < 0)
         {
            return -1;
         }

         return index - 1;      
      }
   }
}
