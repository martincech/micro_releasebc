﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IDisplayConfigurationView : IView
   {
   }

   /// <summary>
   /// DisplayConfiguration settings detail view
   /// </summary>
   [InheritedExport]
   public interface IDisplayConfigurationDetailView : IDisplayConfigurationView, IDetailView
   {
   }
}
