﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IWeightUnitView : IView
   {
   }

   [InheritedExport]
   public interface IWeightUnitDetailView : IWeightUnitView, IDetailView
   {
   }
}
