﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IEthernetConfigurationView : IView
   {
   }

   [InheritedExport]
   public interface IEthernetConfigurationDetailView : IEthernetConfigurationView, IDetailView
   {
   }
}
