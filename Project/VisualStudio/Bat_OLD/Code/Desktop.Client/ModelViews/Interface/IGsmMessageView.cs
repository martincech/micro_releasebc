﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IGsmMessageView : IView
   {
   }

   [InheritedExport]
   public interface IGsmMessageDetailView : IGsmMessageView, IDetailView
   {
   }
}
