﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface IArchiveItemView : IView
   {
   }

   [InheritedExport]
   public interface IArchiveItemDetailView : IArchiveItemView, IDetailView
   {
   }

   [InheritedExport]
   public interface IArchiveItemListView : IArchiveItemView, IListView
   {
   }
}
