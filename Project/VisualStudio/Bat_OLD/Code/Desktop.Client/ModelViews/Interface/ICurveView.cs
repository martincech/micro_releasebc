﻿using System.ComponentModel.Composition;
using Common.Desktop.Presentation;

namespace Desktop.Client.ModelViews.Interface
{
   [InheritedExport]
   public interface ICurveView : IView
   {
   }

   [InheritedExport]
   public interface ICurveDetailView : ICurveView, IDetailView
   {
   }

   [InheritedExport]
   public interface ICurveListView : ICurveView, IListView
   {
   }
}
