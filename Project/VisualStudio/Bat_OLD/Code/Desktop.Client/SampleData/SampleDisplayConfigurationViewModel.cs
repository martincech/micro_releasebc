﻿using Common.Desktop.Presentation;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using System;
using System.ComponentModel.Composition;
using System.Linq;
using ViewModels;

namespace Desktop.Client.SampleData
{
   public class SampleDisplayConfigurationViewModel : DisplayConfigurationViewModel
   {
      public SampleDisplayConfigurationViewModel()
         : base(new MockIDisplayConfigurationView())
      {
         var confs = SampleDataProvider.CreateDisplayConfigurations();
         var conf = confs.First();

         conf.Contrast = Bat2Library.DisplayConfigurationC.DISPLAY_CONTRAST_DEFAULT;
         conf.Mode = Bat2Library.DisplayModeE.DISPLAY_MODE_ADVANCED;
         conf.Backlight.Duration = Bat2Library.BacklightConfigurationC.DURATION_DEFAULT;
         conf.Backlight.Intensity = Bat2Library.BacklightConfigurationC.INTENSITY_DEFAULT;
         conf.Backlight.Mode = Bat2Library.BacklightModeE.BACKLIGHT_MODE_AUTO;

         Model = conf;

         foreach (var c in confs)
         {
            Models.Add(c);
         }
      }

   }

   [PartNotDiscoverable]
   internal class MockIDisplayConfigurationView : MockView, IDisplayConfigurationDetailView
   {
   }
}
