﻿using ViewModels;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using Common.Desktop.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace Desktop.Client.SampleData
{
   public class SampleArchiveItemViewModel : ArchiveItemViewModel
   {
      public SampleArchiveItemViewModel()
         : base(new MockIArchiveItemView())
      {
         var archives = SampleDataProvider.CreateArchiveItems();
         var archive = archives.First();

         archive.MaleTargetWeight = 1100;
         archive.FemaleTargetWeight = 1000;
         archive.ZoneId = 3;
         archive.StatisticOriginator = 1;
         archive.Day = 4;
         archive.Timestamp = new DateTime(2014, 12, 1, 7, 50, 0);
         archive.Average = 155;
         archive.Count = 10;
         archive.Gain = 20;
         archive.Sigma = 2;
         archive.Uniformity = 1;
         archive.From = new DateTime(1, 1, 1, 7, 0, 0);
         archive.To = new DateTime(1, 1, 1, 14, 0, 0);
         archive.Sex = Bat2Library.SexE.SEX_MALE;

         archive.Histogram = new List<HistogramSlot>();
         for (int i = 0; i < 5; i++)
         {
            var slot = new HistogramSlot { Value = (byte)(i + 2) };
            archive.Histogram.Add(slot);
         }

         Model = archive;

         foreach (var c in archives)
         {
            Models.Add(c);
         }
      }

   }

   [PartNotDiscoverable]
   internal class MockIArchiveItemView : MockView, IArchiveItemDetailView
   {
   }
}
