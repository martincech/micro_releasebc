﻿using ViewModels;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using Common.Desktop.Presentation;
using System;
using System.ComponentModel.Composition;
using System.Linq;

namespace Desktop.Client.SampleData
{
   public class SampleWeighingPlanViewModel : WeighingPlanViewModel
   {
      public SampleWeighingPlanViewModel()
         : base(new MockIWeighingPlanView())
      {
         var plans = SampleDataProvider.CreateWeighingPlans();
         var plan = plans.First();

         plan.SyncWithDayStart = true;
         plan.Name = "plan name";
         plan.WeighingDays.Mode = Bat2Library.WeighingDaysModeE.WEIGHING_DAYS_MODE_DAY_OF_WEEK;
         plan.WeighingDays.DaysOfWeek = Bat2Library.WeighingDaysMaskE.WEIGHING_DAYS_THURSDAY;

         WeighingTime time1 = new WeighingTime();
         time1.From = new DateTime(1, 1, 1, 10, 10, 0);
         time1.To = new DateTime(1, 1, 1, 14, 0, 0);
         WeighingTime time2 = new WeighingTime();
         time2.From = new DateTime(1, 1, 1, 18, 30, 0);
         time2.To = new DateTime(1, 1, 1, 20, 5, 0);
         plan.WeighingTime.Clear();
         plan.WeighingTime.Add(time1);
         plan.WeighingTime.Add(time2);

         //WeighingTimeViewColl.Add(time1);
         //WeighingTimeViewColl.Add(time2);
       
         Model = plan;

         foreach (var p in plans)
         {
            Models.Add(p);
         }

         SelectedModels.Clear();
         SelectedModels.Add(Model);
      }
   }

   [PartNotDiscoverable]
   internal class MockIWeighingPlanView : MockView, IWeighingPlanDetailView
   {
   }
}
