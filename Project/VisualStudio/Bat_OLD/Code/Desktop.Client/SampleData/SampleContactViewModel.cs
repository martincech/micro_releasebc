﻿using ViewModels;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using Common.Desktop.Presentation;
using System.ComponentModel.Composition;
using System.Linq;

namespace Desktop.Client.SampleData
{
   public class SampleContactViewModel : ContactsViewModel
   {
      public SampleContactViewModel()
         : base(new MockIContactView())
      {
         var contacts = SampleDataProvider.CreateContacts();
         var contact = contacts.First();

         contact.Phone = "+420 123 456 789";
         contact.Name = "Jožin z Bažin";
         contact.SmsFormat = Bat2Library.GsmSmsFormatE.GSM_SMS_FORMAT_MOBILE_PHONE;
         contact.Statistics = true;
         contact.Events = true;
         contact.SendHistogram = true;
         contact.Commands = false;
         contact.RemoteControl = false;

         Model = contact;

         foreach (var c in contacts)
         {
            Models.Add(c);
         }

         SelectedModels.Clear();
         SelectedModels.Add(Model);
      }

   }

   [PartNotDiscoverable]
   internal class MockIContactView : MockView, IContactDetailView
   {
   }
}
