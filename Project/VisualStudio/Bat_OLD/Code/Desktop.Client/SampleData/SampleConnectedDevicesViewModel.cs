﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.ServiceModel;
using Bat2Library;
using Connection.Interface.Contract;
using Connection.Interface.Domain;
using Desktop.Client.ConectedDevices.Aplications;
using Desktop.Client.ConectedDevices.Aplications.Proxy;
using Desktop.Client.ConectedDevices.Interface;
using Desktop.Client.MessageService.Interface;
using Ploeh.AutoFixture;
using ArchiveItem = Connection.Interface.Domain.ArchiveItem;
using Common.Desktop.Presentation;

namespace Desktop.Client.SampleData
{
   public class SampleConnectedDevicesViewModel : ConnectedDevicesViewModel
   {
      public SampleConnectedDevicesViewModel() :
         base(
         new MockISelectDeviceService(),
         new MockIMessageService(),
         new ChannelFactory<IBat2DataCommandContract>(),
         new ChannelFactory<IBat2CommandContract>())
      {
         DispatcherHelper.Initialize();
          
         var fixture = new Fixture { RepeatCount = 5 };
         fixture.Customize(new MultipleCustomization());
         fixture.Customize<ArchiveItem>(composer =>
            composer.Without(m => m.HourFrom).Without(m => m.HourTo));
         var stats = Enum.GetValues(typeof (WeighingStatusE));
         var classes = Enum.GetValues(typeof(DeviceClassE));
         var rand = new Random((int) DateTime.Now.ToBinary());
         fixture.Customize<WeighingContext>(composer => composer.With(c => c.Status, (byte)stats.Cast<WeighingStatusE>().ElementAt(rand.Next(stats.Length))));
         fixture.Customize<VersionInfo>(composer => composer.With(c => c.Class, (byte)classes.Cast<DeviceClassE>().ElementAt(rand.Next(classes.Length))));
         //fixture.Customize<Context>(composer => composer.Without(c => c.WeighingContext));
         var models = new List<Bat2DeviceData>();
         fixture.AddManyTo(models);
         var i = 0;
         foreach (var model in models)
         {
            Models.Add(new Bat2DeviceDataProxy(model, null));
            i = i < models.Count - 1 ? i + 1 : 0;
         }

         Model = Models.First();
      }
   }

   [PartNotDiscoverable]
   public class MockISelectDeviceService : ISelectDeviceService
   {
      #region Implementation of ISelectDeviceService

      public Bat2DeviceDataProxy SelectedDevice { set; private get; }
      public IEnumerable<Bat2DeviceDataProxy> SelectedDevices { set; private get; }

      #endregion
   }

   [PartNotDiscoverable]
   public class MockIMessageService : IMessageService
   {
      #region Implementation of IMessageService

      /// <summary>
      /// Shows the message.
      /// </summary>
      /// <param name="owner">The window that owns this Message Window.</param>
      /// <param name="message">The message.</param>
      public void ShowMessage(string message)
      {
      }

      /// <summary>
      /// Shows the message as warning.
      /// </summary>
      /// <param name="owner">The window that owns this Message Window.</param>
      /// <param name="message">The message.</param>
      public void ShowWarning(string message)
      {
      }

      /// <summary>
      /// Shows the message as error.
      /// </summary>
      /// <param name="owner">The window that owns this Message Window.</param>
      /// <param name="message">The message.</param>
      public void ShowError(string message)
      {
      }

      /// <summary>
      /// Shows the specified question.
      /// </summary>
      /// <param name="owner">The window that owns this Message Window.</param>
      /// <param name="message">The question.</param>
      /// <returns><c>true</c> for yes, <c>false</c> for no and <c>null</c> for cancel.</returns>
      public bool? ShowQuestion(string message)
      {
         return null;
      }

      /// <summary>
      /// Shows the specified yes/no question.
      /// </summary>
      /// <param name="owner">The window that owns this Message Window.</param>
      /// <param name="message">The question.</param>
      /// <returns><c>true</c> for yes and <c>false</c> for no.</returns>
      public bool ShowYesNoQuestion(string message)
      {
         return false;
      }

      #endregion
   } 
}