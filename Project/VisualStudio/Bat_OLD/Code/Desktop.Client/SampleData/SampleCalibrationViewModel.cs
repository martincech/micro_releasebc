﻿using ViewModels;
using Desktop.Client.ModelViews.Applications;
using Desktop.Client.ModelViews.Interface;
using Common.Desktop.Presentation;
using System;
using System.ComponentModel.Composition;
using System.Linq;

namespace Desktop.Client.SampleData
{
   public class SampleCalibrationViewModel : CalibrationViewModel
   {
      public SampleCalibrationViewModel()
         : base(new MockICalibrationView())
      {
         var calibration = SampleDataProvider.CreateCalibrations().First();

         calibration.Delay = 2;
         calibration.Duration = 10;

         Random num = new Random();
         calibration.CalibrationPoints.Clear();
         for (int i = 0; i < 5; i++)
         {
            CalibrationPoint point = new CalibrationPoint();
            point.Weight = num.Next(1, 20);
            calibration.CalibrationPoints.Add(point);
         }

         Model = calibration;
      }
   }

   [PartNotDiscoverable]
   internal class MockICalibrationView : MockView, ICalibrationDetailView
   {
   }
}
