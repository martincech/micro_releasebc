﻿using Bat2Library;
using Common.Library.Observable;

namespace ViewModels.Desktop
{
   public class IdentificationContext : ObservableObject
   {
      private Weighing activeWeighing;
      private Bat2Identification identification;

      public Bat2Identification Identification
      {
         get { return identification; }
         set { SetProperty(ref identification, value, "Identification"); }
      }

      public Weighing ActiveWeighing
      {
         get { return activeWeighing; }
         set
         {
            SetProperty(ref activeWeighing, value, "ActiveWeighing");
            RaisePropertyChanged(() => IsWeighing);
         }
      }

      public bool IsWeighing
      {
         get
         {
            return
               (ActiveWeighing != null) &&
               (ActiveWeighing.Status == WeighingStatusE.WEIGHING_STATUS_WEIGHING);
         }
      }
   }
}