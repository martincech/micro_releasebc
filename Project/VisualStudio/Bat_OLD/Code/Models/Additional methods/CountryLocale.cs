﻿namespace ViewModels
{
   public partial class CountryLocale
   {
      /// <summary>
      /// Separator of time (between hours and minutes and between minutes and seconds)
      /// </summary>
      public char LocaleTimeSeparator
      {
         get { return string.IsNullOrEmpty(Separators) ? ' ' : Separators[0]; }
         set
         {
            Separators = new string(new[]
            {
               value,
               LocaleDateSeparator1,
               LocaleDateSeparator2
            });
            RaisePropertyChanged();
         }
      }

      /// <summary>
      /// Date separator between first two items 
      /// </summary>
      public char LocaleDateSeparator1
      {
         get { return string.IsNullOrEmpty(Separators) ? ' ' : Separators[1]; }
         set
         {
            Separators = new string(new[]
            {
               LocaleTimeSeparator,
               value,
               LocaleDateSeparator2
            });
            RaisePropertyChanged();
         }
      }

      /// <summary>
      /// Date separator between second two items 
      /// </summary>
      public char LocaleDateSeparator2
      {
         get { return string.IsNullOrEmpty(Separators) ? ' ' : Separators[2]; }
         set
         {
            Separators = new string(new[]
            {
               LocaleTimeSeparator,
               LocaleDateSeparator1,
               value
            });
            RaisePropertyChanged();
         }
      }
   }
}
