﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using Bat2Library;
using Connection.Interface.Domain;
using ViewModels.Properties;
using System.Collections.ObjectModel;

namespace ViewModels
{
   public static class DataExtensions
   {
      #region Map methods

      public static void Map(
         this Configuration configuration,
         Bat2Identification identification,
         Country country,
         WeightUnit weightUnit,
         Calibration calibration,
         DisplayConfiguration displayConfiguration,
         WeighingConfiguration weighingConfiguration,
         IEnumerable<Rs485Configuration> rsConfigs,
         ModbusConfiguration modbusConfig,
         MegaviConfiguration megaviConfig
         )
      {
         configuration.VersionInfo.Map(identification);
         configuration.DeviceInfo.Map(identification);
         configuration.Country.Map(country);
         configuration.WeightUnits.Map(weightUnit);
         configuration.PlatformCalibration.Map(calibration);
         configuration.DisplayConfiguration.Map(displayConfiguration);
         configuration.WeighingConfiguration.Map(weighingConfiguration);
         configuration.Rs485Options.Map(rsConfigs);
         configuration.ModbusOptions.Map(modbusConfig);
         configuration.MegaviOptions.Map(megaviConfig);
      }

      public static IEnumerable<Connection.Interface.Domain.Curve> Map(this IEnumerable<Connection.Interface.Domain.Curve> contacts, IEnumerable<GrowthCurve> existingGrowthCurves)
      {
         ICollection<Connection.Interface.Domain.Curve> dataList = new Collection<Connection.Interface.Domain.Curve>(); 
         foreach (GrowthCurve curve in existingGrowthCurves) 
         {
            Connection.Interface.Domain.Curve data = new Connection.Interface.Domain.Curve();
            data.Map(curve);
            dataList.Add(data);
         }
         return dataList;
      }

      public static IEnumerable<Connection.Interface.Domain.Curve> Map(this IEnumerable<Connection.Interface.Domain.Curve> contacts, IEnumerable<CorrectionCurve> existingGrowthCurves)
      {
         ICollection<Connection.Interface.Domain.Curve> dataList = new Collection<Connection.Interface.Domain.Curve>();
         foreach (CorrectionCurve curve in existingGrowthCurves)
         {
            Connection.Interface.Domain.Curve data = new Connection.Interface.Domain.Curve();
            data.Map(curve);
            dataList.Add(data);
         }
         return dataList;
      }

      public static IEnumerable<Connection.Interface.Domain.Contact> Map(this IEnumerable<Connection.Interface.Domain.Contact> contacts, IEnumerable<Contact> existingContacts)
      {
         ICollection<Connection.Interface.Domain.Contact> dataList = new Collection<Connection.Interface.Domain.Contact>();
         foreach (Contact contact in existingContacts)
         {
            Connection.Interface.Domain.Contact data = new Connection.Interface.Domain.Contact();
            data.Map(contact);
            dataList.Add(data);
         }
         return dataList;
      }

      public static IEnumerable<Connection.Interface.Domain.WeighingPlan> Map(this IEnumerable<Connection.Interface.Domain.WeighingPlan> contacts, IEnumerable<WeighingPlan> existingWeighingPlans)
      {
         ICollection<Connection.Interface.Domain.WeighingPlan> dataList = new Collection<Connection.Interface.Domain.WeighingPlan>();
         foreach (WeighingPlan weighingPlan in existingWeighingPlans)
         {
            Connection.Interface.Domain.WeighingPlan data = new Connection.Interface.Domain.WeighingPlan();
            data.Map(weighingPlan);
            dataList.Add(data);
         }
         return dataList;
      }

      public static IEnumerable<Connection.Interface.Domain.WeighingConfiguration> Map(this IEnumerable<Connection.Interface.Domain.WeighingConfiguration> contacts, IEnumerable<WeighingConfiguration> existingWeighingConfigurations)
      {
         ICollection<Connection.Interface.Domain.WeighingConfiguration> dataList = new Collection<Connection.Interface.Domain.WeighingConfiguration>();
         foreach (WeighingConfiguration weighingConfiguration in existingWeighingConfigurations)
         {
            Connection.Interface.Domain.WeighingConfiguration data = new Connection.Interface.Domain.WeighingConfiguration();
            data.Map(weighingConfiguration);
            dataList.Add(data);
         }
         return dataList;
      }

      public static void Map(this List<Connection.Interface.Domain.Rs485Options> rs485ConfigurationList, IEnumerable<Rs485Configuration> existingRs485ConfigurationList)
      {
         rs485ConfigurationList.Clear();
         foreach (Rs485Configuration rs485Configuration in existingRs485ConfigurationList) 
         {
            Connection.Interface.Domain.Rs485Options rs = new Connection.Interface.Domain.Rs485Options();
            rs.Map(rs485Configuration);
            rs485ConfigurationList.Add(rs);
         }
      }

      private static void Map(this Connection.Interface.Domain.Country country,
         Country existingCountry)
      {
         Debug.Assert(existingCountry != null, "Map - Country is null!");
         if (country == null)
         {
            throw new ArgumentNullException("country", Resources.Country_can_t_be_null);
         }
         country.Language = (byte)existingCountry.Language;
         country.CountryCode = (byte)existingCountry.CountryCode;

         country.CodePage = (byte)existingCountry.Locale.CodePage;
         country.DateFormat = (byte)existingCountry.Locale.DateFormat;
         country.DaylightSavingType = (byte)existingCountry.Locale.DaylightSavingType;

         country.DateSeparator1 = existingCountry.Locale.Separators[0];
         country.DateSeparator2 = existingCountry.Locale.Separators[1];
         country.TimeSeparator = existingCountry.Locale.Separators[2];
         country.TimeFormat = (byte)existingCountry.Locale.TimeFormat;
      }

      private static void Map(this Connection.Interface.Domain.WeightUnits weightUnits, WeightUnit existingWeightUnit)
      {
         if (weightUnits == null)
         {
            throw new ArgumentNullException("weightUnits", Resources.WeightUnit_can_t_be_null);
         }

         weightUnits.Division = Convert.ToInt16(existingWeightUnit.Division);
         weightUnits.Units = (byte)existingWeightUnit.Units;
         weightUnits.Range = ConvertCapacity(existingWeightUnit.Units, existingWeightUnit.Capacity);
      }

      private static void Map(this Connection.Interface.Domain.DisplayConfiguration configuration,
         DisplayConfiguration existingConfiguration)
      {
         Debug.Assert(existingConfiguration != null, "Map - DisplayConfiguration is null!");
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.DisplayConfiguration_can_t_be_null);
         }

         configuration.Contrast = existingConfiguration.Contrast;
         configuration.Mode = (byte)existingConfiguration.Mode;
         configuration.BacklightDuration = existingConfiguration.Backlight.Duration;
         configuration.BacklightIntensity = existingConfiguration.Backlight.Intensity;
         configuration.BacklightMode = (byte)existingConfiguration.Backlight.Mode;
      }

      private static void Map(this Connection.Interface.Domain.GsmMessage message,
        GsmMessage existingMessage)
      {
         Debug.Assert(existingMessage != null, "Map - GsmMessage is null!");
         if (message == null)
         {
            throw new ArgumentNullException("message", Resources.GsmMessage_can_t_be_null);
         }

         //gsm message
         message.SendAt = existingMessage.Statistics.SendAt;
         message.AcceleratedPeriod = existingMessage.Statistics.AcceleratedPeriod;
         message.AccelerateFromDay = existingMessage.Statistics.AccelerateFromDay;
         message.Period = existingMessage.Statistics.Period;
         message.StartFromDay = existingMessage.Statistics.StartFromDay;
         message.CommandsExpiration = existingMessage.Commands.Expiration;
         message.CommandsCheckPhoneNumber = existingMessage.Commands.CheckPhoneNumber;
         message.CommandsEnabled = existingMessage.Commands.Enabled;
         message.RemoteControlCheckPhoneNumber = existingMessage.RemoteControl.CheckPhoneNumber;
         message.RemoteControlEnabled = existingMessage.RemoteControl.Enabled;
         message.EventMask = (byte)existingMessage.Events.EventMask;

         //gsm power configuration
         message.SwitchOnDuration = existingMessage.PowerConfiguration.SwitchOnDuration;
         message.SwitchOnPeriod = existingMessage.PowerConfiguration.SwitchOnPeriod;
         message.Mode = (byte)existingMessage.PowerConfiguration.Mode;

         //gsm power option time
         message.SwitchOnTimes.Clear();
         foreach (var t in existingMessage.GsmPowerOptionTimes)
         {
            var time = new TimeRange();
            time.From = t.From;
            time.To = t.To;
            message.SwitchOnTimes.Add(time);
         }
      }

      private static void Map(this Connection.Interface.Domain.Contact contact, 
         Contact existingContact)
      {
         Debug.Assert(existingContact != null, "Map - Contact is null!");
         if (contact == null)
         {
            throw new ArgumentNullException("contact", Resources.Contact_can_t_be_null);
         }

         contact.SendHistogram = existingContact.SendHistogram;
         contact.RemoteControl = existingContact.RemoteControl;
         contact.Events = existingContact.Events;
         contact.Commands = existingContact.Commands;
         contact.Statistics = existingContact.Statistics;
         contact.SmsFormat = (byte)existingContact.SmsFormat;
         contact.Name = existingContact.Name;
         contact.PhoneNumber = existingContact.Phone;
      }

      private static void Map(this Connection.Interface.Domain.VersionInfo version, Bat2Identification existingBat2)
      {
         Debug.Assert(version != null, "Map - VersionInfo is null!");
         if (existingBat2 == null)
         {
            throw new ArgumentNullException("existingBat2", "Bat2Identification can't be null!");
         }

         version.HardwareBuild = existingBat2.Version.HardwareBuild;
         version.HardwareMinor = existingBat2.Version.HardwareMinor;
         version.HardwareMajor = existingBat2.Version.HardwareMajor;
         version.SoftwareBuild = existingBat2.Version.SoftwareBuild;
         version.SoftwareMinor = existingBat2.Version.SoftwareMinor;
         version.SoftwareMajor = existingBat2.Version.SoftwareMajor;
         version.Modification = (byte)existingBat2.Version.Modification;
         version.Class = (byte)existingBat2.Version.Class;
         version.SerialNumber = Convert.ToUInt32(existingBat2.SerialNumber);

      }

      private static void Map(this DeviceInfo device, Bat2Identification existingBat2)
      {
         Debug.Assert(device != null, "Map - VersionInfo is null!");
         if (existingBat2 == null)
         {
            throw new ArgumentNullException("existingBat2", "Bat2Identification can't be null!");
         }

         device.Name = existingBat2.Name;
         device.Password = existingBat2.Password;
      }

      private static void Map(this EthernetOptions configuration, 
         EthernetConfiguration existingConfiguration)
      {
         Debug.Assert(existingConfiguration != null, "Map - EthernetConfiguration is null!");
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.EthernetOptions_can_t_be_null);
         }

         configuration.Port = existingConfiguration.Port;
         configuration.Address = existingConfiguration.Address;
         configuration.Enabled = existingConfiguration.Enabled;
      }

      private static void Map(this Rs485Options configuration,
         Rs485Configuration existingConfiguration)
      {
         Debug.Assert(existingConfiguration != null, "Map - Rs485Configuration is null!");
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.Rs485Options_can_t_be_null);
         }

         configuration.Mode = (byte)existingConfiguration.Mode;
         configuration.Enabled = existingConfiguration.Enabled;
         configuration.PhysicalDeviceAddress = existingConfiguration.PhysicalInterfaceAddress;
      }

      private static void Map(this SmsGateOptions configuration, 
         SmsGateConfiguration existingConfiguration)
      {
         Debug.Assert(existingConfiguration != null, "Map - SmsGateConfiguration is null!");
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.SmsGateOptions_can_t_be_null);
         }

         configuration.Parity = (byte)existingConfiguration.Parity;
         configuration.DataBits = (byte)existingConfiguration.DataBits;
         configuration.BaudRate = existingConfiguration.BaudRate;
      }

      private static void Map(this DacsOptions configuration, 
         DacsConfiguration existingConfiguration)
      {
         Debug.Assert(existingConfiguration != null, "Map - DacsConfiguration is null!");
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.DacsOptions_can_t_be_null);
         }

         configuration.Address = existingConfiguration.Address;
         configuration.Version = (byte)existingConfiguration.Version;
      }

      private static void Map(this  MegaviOptions configuration, 
         MegaviConfiguration existingConfiguration)
      {
         Debug.Assert(existingConfiguration != null, "Map - MegaviConfiguration is null!");
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.MegaviOptions_can_t_be_null);
         }

         configuration.Code = existingConfiguration.ModuleCode;
         configuration.Address = existingConfiguration.ModuleAddress;
      }

      private static void Map(this ModbusOptions configuration,
         ModbusConfiguration existingConfiguration)
      {
         Debug.Assert(existingConfiguration != null, "Map - ModbusConfiguration is null!");
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.ModbusConfiguration_can_t_be_null);
         }

         configuration.ReplyDelay = existingConfiguration.ReplyDelay;
         configuration.Parity = (byte)existingConfiguration.Parity;
         configuration.DataBits = (byte)existingConfiguration.DataBits;
         configuration.BaudRate = existingConfiguration.BaudRate;
         configuration.Address = existingConfiguration.Address;
         configuration.Mode = (byte)existingConfiguration.Mode;
      }

      private static void Map(this PlatformCalibration calibration, Calibration existingCalibration)
      {
         Debug.Assert(existingCalibration != null, "Map - Calibration is null!");
         if (calibration == null)
         {
            throw new ArgumentNullException("calibration", Resources.PlatformCalibration_can_t_be_null);
         }
         // Calibration
         calibration.Delay = existingCalibration.Delay;
         calibration.Duration = existingCalibration.Duration;

         // CalibrationPoint
         calibration.Points.Clear();
         foreach (var point in existingCalibration.CalibrationPoints)
         {
            calibration.Points.Add(point.Weight);
         }
      }

      private static void Map(this WeighingContext weighing, Weighing existingWeighing)
      {
         Debug.Assert(existingWeighing != null, "Map - Weighing is null!");
         if (weighing == null)
         {
            throw new ArgumentNullException("weighing", Resources.WeighingContext_can_t_be_null);
         }
         // Weighing
         weighing.Status = (byte)existingWeighing.Status;
         weighing.LastStatus = (byte)existingWeighing.PrevStatus;
         weighing.DayActive = Convert.ToByte(existingWeighing.DayActive);
         weighing.Day = existingWeighing.Day;
         weighing.TargetWeightFemale = existingWeighing.TargetWeightFemale;
         weighing.TargetWeight = existingWeighing.TargetWeightMale;
         weighing.Correction = existingWeighing.Correction;
         weighing.DayCloseAt = existingWeighing.DayCloseAt;
         weighing.StartAt = existingWeighing.StartAt;
         weighing.Id = existingWeighing.Id;
         weighing.DayDuration = existingWeighing.DayDuration;

         // WeighingPlan
         weighing.WeighingPlan.Map(existingWeighing.WeighingPlan);

         // CorrectionCurve
         weighing.CorrectionCurve.Map(existingWeighing.CorrectionCurve);

         // GrowthCurve
         weighing.GrowthCurveFemale.Map(existingWeighing.GrowthCurveFemale);
          weighing.GrowthCurveMale.Map(existingWeighing.GrowthCurveMale);
      }

      private static void Map(this Connection.Interface.Domain.WeighingPlan plan, 
         WeighingPlan existingPlan)
      {
         Debug.Assert(existingPlan != null, "Map - WeighingPlan is null!");
         if (plan == null)
         {
            throw new ArgumentNullException("plan", Resources.WeighingPlan_can_t_be_null);
         }

         plan.SyncWithDayStart = existingPlan.SyncWithDayStart;
         plan.Name = existingPlan.Name;
         plan.WeighingDaysStartDay = existingPlan.WeighingDays.StartDay;
         plan.WeighingDaysSuspendedDays = existingPlan.WeighingDays.SuspendedDays;
         plan.WeighingDaysOnlineDays = existingPlan.WeighingDays.OnlineDays;
         plan.WeighingDaysDays = (byte)existingPlan.WeighingDays.DaysOfWeek;
         plan.WeighingDaysMode = (byte)existingPlan.WeighingDays.Mode;

         // WeighingTime
         var tmpList = new List<TimeRange>();
         foreach (var t in existingPlan.WeighingTime)
         {
            var time = new TimeRange();
            time.From = t.From;
            time.To = t.To;
            tmpList.Add(time);
         }
         plan.WeighingTimes = tmpList;
      }

      private static void Map(this Connection.Interface.Domain.Curve curve, 
         CorrectionCurve existingCurve)
      {
         Debug.Assert(existingCurve != null, "Map - CorrectionCurve is null!");
         if (curve == null)
         {
            throw new ArgumentNullException("curve", Resources.Curve_can_t_be_null);
         }

         curve.Name = existingCurve.Name;
         curve.Points.Clear();
         foreach (var point in existingCurve.Points)
         {
            var p = new Connection.Interface.Domain.CurvePoint();
            p.ValueX = point.ValueX;
            p.ValueY = point.ValueY;
            curve.Points.Add(p);
         }
      }

      private static void Map(this Connection.Interface.Domain.Curve curve, 
         GrowthCurve existingCurve)
      {
         Debug.Assert(existingCurve != null, "Map - GrowthCurve is null!");
         if (curve == null)
         {
            throw new ArgumentNullException("curve", Resources.Curve_can_t_be_null);
         }

         curve.Name = existingCurve.Name;
         curve.Points.Clear();
         foreach (var point in existingCurve.Points)
         {
            var p = new Connection.Interface.Domain.CurvePoint();
            p.ValueX = point.ValueX;
            p.ValueY = point.ValueY;
            curve.Points.Add(p);
         }
      }

      private static void Map(this Connection.Interface.Domain.ArchiveItem item, 
         ArchiveItem existingItem)
      {
         Debug.Assert(existingItem != null, "Map - ArchiveItem is null!");
         if (item == null)
         {
            throw new ArgumentNullException("item", Resources.ArchiveItem_can_t_be_null);
         }

         item.TargetWeight = existingItem.MaleTargetWeight;
         item.TargetWeightFemale = existingItem.FemaleTargetWeight;
         item.ZoneIdentifier = existingItem.ZoneId;
         item.StatisticCalculate = existingItem.StatisticOriginator;
         item.Day = existingItem.Day;
         item.Timestamp = existingItem.Timestamp;
         item.Average = existingItem.Average;
         item.Count = existingItem.Count;
         item.Gain = existingItem.Gain;
         item.Sigma = existingItem.Sigma;
         item.Uniformity = existingItem.Uniformity;
         item.HourFrom = (byte)existingItem.From.Hour;
         item.HourTo = (byte)existingItem.To.Hour;
         item.Sex = (byte)existingItem.Sex;

         //HistogramSlot
         var tmpList = new List<byte>();
         foreach (var slot in existingItem.Histogram)
         {
            tmpList.Add(slot.Value);
         }
         item.Histogram = tmpList;
      }

      private static void Map(this Connection.Interface.Domain.WeighingConfiguration configuration, 
         WeighingConfiguration existingConfiguration)
      {
         Debug.Assert(existingConfiguration != null, "Map - WeighingConfiguration is null!");
         if (configuration == null)
         {
            throw new ArgumentNullException("configuration", Resources.WeighingConfiguration_can_t_be_null);
         }

         configuration.InitialDay = existingConfiguration.InitialDayNumber;
         configuration.MenuMask = Convert.ToUInt32(existingConfiguration.MenuMask);
         configuration.Flock = existingConfiguration.FlockName;
         configuration.Name = existingConfiguration.Name;
         configuration.CorrectionCurveIndex = existingConfiguration.CorrectionCurveIndex;
         configuration.PredefinedIndex = existingConfiguration.PredefinedIndex;
         configuration.DayStart = new DateTime(existingConfiguration.DayStart.Ticks);
         configuration.OnlineAdjustment = (byte)existingConfiguration.TargetWeight.OnlineAdjustment;
         configuration.Sex = (byte)existingConfiguration.TargetWeight.Sex;
         configuration.SexDifferentiation = (byte)existingConfiguration.TargetWeight.SexDifferentiation;
         configuration.Growth = (byte)existingConfiguration.TargetWeight.Growth;
         configuration.Mode = (byte)existingConfiguration.TargetWeight.Mode;
         configuration.Step = (byte)existingConfiguration.Detection.Step;
         configuration.StabilizationRange = existingConfiguration.Detection.StabilizationRange;
         configuration.StabilizationTime = existingConfiguration.Detection.StabilizationTime;
         configuration.MaleInitialWeight = existingConfiguration.MaleGender.InitialWeight;
         configuration.MaleGrowthCurveIndex = existingConfiguration.MaleGender.GrowthCurveIndex;
         configuration.MaleStandardCurveIndex = existingConfiguration.MaleGender.StandardCurveIndex;
         configuration.MaleMarginBelow = existingConfiguration.MaleGender.Acceptance.MarginBelow;
         configuration.MaleMarginAbove = existingConfiguration.MaleGender.Acceptance.MarginAbove;
         configuration.FemaleInitialWeight = existingConfiguration.FemaleGender.InitialWeight;
         configuration.FemaleGrowthCurveIndex = existingConfiguration.FemaleGender.GrowthCurveIndex;
         configuration.FemaleStandardCurveIndex = existingConfiguration.FemaleGender.StandardCurveIndex;
         configuration.FemaleMarginBelow = existingConfiguration.FemaleGender.Acceptance.MarginBelow;
         configuration.FemaleMarginAbove = existingConfiguration.FemaleGender.Acceptance.MarginAbove;
         configuration.ShortType = (byte)existingConfiguration.Statistics.HourlyType;
         configuration.ShortPeriod = existingConfiguration.Statistics.HourlyPeriod;
         configuration.UniformityRange = existingConfiguration.Statistics.UniformityRange;
         configuration.HistogramStep = existingConfiguration.Statistics.HistogramConfiguration.Step;
         configuration.HistogramRange = existingConfiguration.Statistics.HistogramConfiguration.Range;
         configuration.HistogramMode = (byte)existingConfiguration.Statistics.HistogramConfiguration.Mode;
         configuration.WeighingPlanIndex = existingConfiguration.WeighingPlanIndex;
         configuration.Planning = existingConfiguration.Planning;
      }

      #endregion

      #region Private helpers

      private static int ConvertCapacity(WeightUnitsE units, WeightCapacityE capacity)
      {
         switch (units)
         {
            case WeightUnitsE.WEIGHT_UNITS_G:
               return capacity == WeightCapacityE.WEIGHT_CAPACITY_EXTENDED
                  ? (WeightUnitsC.G_EXT_RANGE)
                  : (WeightUnitsC.G_RANGE);

            case WeightUnitsE.WEIGHT_UNITS_LB:
               return capacity == WeightCapacityE.WEIGHT_CAPACITY_EXTENDED
                  ? (WeightUnitsC.LB_EXT_RANGE)
                  : (WeightUnitsC.LB_RANGE);

            case WeightUnitsE.WEIGHT_UNITS_KG:
               return capacity == WeightCapacityE.WEIGHT_CAPACITY_EXTENDED
                  ? (WeightUnitsC.KG_EXT_RANGE)
                  : (WeightUnitsC.KG_RANGE);
         }
         return 0;
      }

      #endregion
   }
}