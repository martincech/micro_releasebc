﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using ViewModels.Properties;

namespace ViewModels
{
   public partial class GrowthCurve
   {
      public void Map(Connection.Interface.Domain.Curve curve)
      {
         if (curve == null)
         {
            throw new ArgumentNullException("curve", Resources.Curve_can_t_be_null);
         }

         Name = curve.Name;
         Points = new ObservableCollection<CurvePoint>();
         if (curve.Points == null) return;
         foreach (var p in curve.Points.Select(point => new CurvePoint {ValueX = point.ValueX, ValueY = point.ValueY}))
         {
            Points.Add(p);
         }
      }
   }
}
