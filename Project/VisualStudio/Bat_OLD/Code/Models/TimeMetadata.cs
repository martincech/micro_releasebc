//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ViewModels
{
   using System;
   using System.ComponentModel.DataAnnotations;
   
   [MetadataType(typeof(TimeMetadata))]
   public partial class Time
   {
      internal sealed class TimeMetadata
   	{
      
        [DataType(DataType.DateTime)]
        [Display(Name = "To")]
      	public System.DateTime To { get; set; }
   
        [DataType(DataType.DateTime)]
        [Display(Name = "From")]
      	public System.DateTime From { get; set; }
   
   	}
   }
}
