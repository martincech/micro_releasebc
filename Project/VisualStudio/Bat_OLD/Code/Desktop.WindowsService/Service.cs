﻿using System.ComponentModel;
using System.ServiceModel;
using System.ServiceProcess;
using System.Configuration;
using System.Configuration.Install;
using System.Linq;
using Connection.Server;
using Connection.Interface.Contract;
using System.ServiceModel.Channels;
using Connection.Interface.Domain;
using Connection.Manager.Interface;
using Common.Services.Client;
using System;
using Connection.Manager;
using System.Threading;
using System.Diagnostics;
using System.ServiceModel.Description;
using Connection;
using Ninject;
using Connection.Interface;

namespace Desktop.WindowsService
{
    //TODO: Change service name, default starup mode, description atd... 
    public class Bat2CommandService : ServiceBase
    {
        public ServiceHost serviceFileHost = null;
        private IKernel ninjectKernel;
        private string serviceName = "Bat2WCFService";

        private ConnectedDevicesController cdController;

        public bool IsOpened
        {
            get { return cdController.IsRunning; }
        }

        public Bat2CommandService()
        {
            ServiceName = serviceName;
        }

        public static void Main()
        {
            ServiceBase.Run(new Bat2CommandService());
        }

        protected override void OnStart(string[] args)
        {
            ninjectKernel = new StandardKernel();
            ninjectKernel.Load(new[] { new ConnectedDevicesModule() });             
            cdController = ninjectKernel.Get<ConnectedDevicesController>();
            serviceFileHost = new ServiceHost(typeof(Bat2FileCommandContractService));
            serviceFileHost.Open();
            cdController.Run();
            Bat2ConnectionManager.Instance().DeviceConnected += SocketSendArchive.SocketSendArchive_DeviceConnected;
        }

        protected override void OnStop()
        {
            Bat2ConnectionManager.Instance().DeviceConnected -= SocketSendArchive.SocketSendArchive_DeviceConnected;
            if (cdController.IsRunning)
            {
                cdController.Shutdown();
                //serviceHost = null;
            }
            if (serviceFileHost.State == CommunicationState.Opened)
            {
                serviceFileHost.Close();
                serviceFileHost = null;
            }
             
        }
    }

    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public ProjectInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new ServiceInstaller();
            service.ServiceName = "Bat2WCFService";
            service.Description = "Service support for Bat2 device.";
            service.StartType = ServiceStartMode.Automatic;
            Installers.Add(process);
            Installers.Add(service);
            service.AfterInstall += Service_AfterInstall;
        }

        private void Service_AfterInstall(object sender, InstallEventArgs e)
        {
            ServiceController sc = new ServiceController(service.ServiceName);
            if (sc.Status == ServiceControllerStatus.Stopped && service.StartType == ServiceStartMode.Automatic)
            {
                sc.Start();
            }
        }
    }
}
