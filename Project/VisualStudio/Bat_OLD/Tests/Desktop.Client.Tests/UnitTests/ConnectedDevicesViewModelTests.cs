﻿using System.Collections.Generic;
using Connection.Interface.Domain;
using Desktop.Client.ConectedDevices.Aplications;
using Desktop.Client.ConectedDevices.Aplications.Proxy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ploeh.AutoFixture;

namespace Desktop.Client.Tests.UnitTests
{
   [TestClass]
   public class ConnectedDevicesViewModelTests
   {
      private ConnectedDevicesViewModel viewModel;

      [TestInitialize]
      public void Init()
      {
         // TODO tests with mock
         viewModel = new ConnectedDevicesViewModel(null,null,null,null);
         var dataList = new List<Bat2DeviceData>();
         var fixture = new Fixture {RepeatCount = 5};
         fixture.Customize(new MultipleCustomization());
         fixture.Customize<ArchiveItem>(composer =>
            composer.Without(m => m.HourFrom).Without(m => m.HourTo)
            );

         //fixture.Customizations.Add(
         //   );
         fixture.AddManyTo(dataList);
         foreach (var bat2DeviceData in dataList)
         {
            viewModel.Models.Add(new Bat2DeviceDataProxy(bat2DeviceData, null));
         }
      }

      //[TestMethod]
      //public void TimeGet_CanExecute()
      //{
      //   viewModel.Model = viewModel.Models.First();
      //   foreach (var value in (WeighingStatusE[])typeof(WeighingStatusE).GetEnumValues())
      //   {
      //      viewModel.Model.Context.WeighingContext.Status = (byte) value;
      //      Assert.IsTrue(viewModel.ExecuteCommand.CanExecute(null));
      //   }
      //}

      //[TestMethod]
      //public void TimeSet_CanExecute()
      //{
      //   viewModel.Model = viewModel.Models.First();
      //   foreach (var value in (WeighingStatusE[])typeof(WeighingStatusE).GetEnumValues())
      //   {
      //      viewModel.Model.Context.WeighingContext.Status = (byte)value;

      //      Assert.IsFalse(viewModel.TimeSetCommand.CanExecute(null));
      //      Assert.IsTrue(viewModel.TimeSetCommand.CanExecute(DateTime.Now));
      //   }
      //}

      //[TestMethod]
      //public void WeighingStart_CanExecute()
      //{
      //   viewModel.Model = viewModel.Models.First();
      //   foreach (var value in (WeighingStatusE[]) typeof (WeighingStatusE).GetEnumValues())
      //   {
      //      viewModel.Model.Context.WeighingContext.Status = (byte) value;
      //      if (value == WeighingStatusE.WEIGHING_STATUS_STOPPED ||
      //         value == WeighingStatusE.WEIGHING_STATUS_WAIT)
      //      {
      //         Assert.IsTrue(viewModel.WeighingStartCommand.CanExecute(null));
      //         Assert.IsTrue(viewModel.WeighingStartCommand.CanExecute(DateTime.Now));
      //         continue;
      //      }
      //      Assert.IsFalse(viewModel.WeighingStartCommand.CanExecute(null));
      //      Assert.IsFalse(viewModel.WeighingStartCommand.CanExecute(DateTime.Now));
      //   }
      //}

      //[TestMethod]
      //public void WeighingStop_CanExecute()
      //{
      //   viewModel.Model = viewModel.Models.First();
      //   foreach (var value in (WeighingStatusE[])typeof(WeighingStatusE).GetEnumValues())
      //   {
      //      viewModel.Model.Context.WeighingContext.Status = (byte)value;
      //      if (value != WeighingStatusE.WEIGHING_STATUS_STOPPED)
      //      {
      //         Assert.IsTrue(viewModel.WeighingStopCommand.CanExecute(null));
      //         Assert.IsTrue(viewModel.WeighingStopCommand.CanExecute(DateTime.Now));
      //         continue;
      //      }
      //      Assert.IsFalse(viewModel.WeighingStopCommand.CanExecute(null));
      //      Assert.IsFalse(viewModel.WeighingStopCommand.CanExecute(DateTime.Now));
      //   }
      //}

      //[TestMethod]
      //public void WeighingSuspend_CanExecute()
      //{
      //   viewModel.Model = viewModel.Models.First();
      //   foreach (var value in (WeighingStatusE[])typeof(WeighingStatusE).GetEnumValues())
      //   {
      //      viewModel.Model.Context.WeighingContext.Status = (byte)value;
      //      if (value == WeighingStatusE.WEIGHING_STATUS_WEIGHING ||
      //          value == WeighingStatusE.WEIGHING_STATUS_RELEASED)
      //      {
      //         Assert.IsTrue(viewModel.WeighingSuspendCommand.CanExecute(null));
      //         Assert.IsTrue(viewModel.WeighingSuspendCommand.CanExecute(DateTime.Now));
      //         continue;
      //      }
      //      Assert.IsFalse(viewModel.WeighingSuspendCommand.CanExecute(null));
      //      Assert.IsFalse(viewModel.WeighingSuspendCommand.CanExecute(DateTime.Now));
      //   }
      //}

      //[TestMethod]
      //public void WeighingRelease_CanExecute()
      //{
      //   viewModel.Model = viewModel.Models.First();
      //   foreach (var value in (WeighingStatusE[])typeof(WeighingStatusE).GetEnumValues())
      //   {
      //      viewModel.Model.Context.WeighingContext.Status = (byte)value;
      //      if (value == WeighingStatusE.WEIGHING_STATUS_SUSPENDED)
      //      {
      //         Assert.IsTrue(viewModel.WeighingReleaseCommand.CanExecute(null));
      //         Assert.IsTrue(viewModel.WeighingReleaseCommand.CanExecute(DateTime.Now));
      //         continue;
      //      }
      //      Assert.IsFalse(viewModel.WeighingReleaseCommand.CanExecute(null));
      //      Assert.IsFalse(viewModel.WeighingReleaseCommand.CanExecute(DateTime.Now));
      //   }
      //}
   }
}