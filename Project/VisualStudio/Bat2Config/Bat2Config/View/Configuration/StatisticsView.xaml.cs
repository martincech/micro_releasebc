﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for StatisticsView.xaml
   /// </summary>
   public partial class StatisticsView : IStatisticsView
   {
    
      public StatisticsView()
      {
         InitializeComponent();     
      }

      public StatisticsView(bool readOnly)
      {
         InitializeComponent();     
         HourlyPanel.Visibility = Visibility.Collapsed;
         Margin = new Thickness(0,5,0,0);
         Layout.IsEnabled = !readOnly;
      }


      public void Show()
      {
         Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         Visibility = Visibility.Collapsed;
      }
   }

   public interface IStatisticsView : IView
   {
   }
}
