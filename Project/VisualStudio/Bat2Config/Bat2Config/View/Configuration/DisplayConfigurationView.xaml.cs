﻿using System.Windows;
using Desktop.Wpf.Presentation;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for DisplayConfiguration.xaml
   /// </summary>
   public partial class DisplayConfigurationView : IDisplayConfigurationView
   {    
      public DisplayConfigurationView()
      {  //Bat2
         InitializeComponent();    
         StackPanelBat2OnlyProperty.Visibility = Visibility.Visible;
      }

      public DisplayConfigurationView(bool readOnly)
      {  //Bat1
         InitializeComponent();
         Layout.IsEnabled = !readOnly;
      }


      public void Show()
      {
         DisplayControl.Visibility = Visibility.Visible;
      }

      public void Hide()
      {
         DisplayControl.Visibility = Visibility.Collapsed;
      }
   }

   public interface IDisplayConfigurationView : IView
   {
   }
}
