﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Bat2Config.Convertors;
using Desktop.Wpf.Applications;

namespace Bat2Config.View.Configuration
{
   /// <summary>
   /// Interaction logic for ChangePasswordView.xaml
   /// </summary>
   public partial class ChangePasswordView : Window
   {
      #region Field and properties

      private Command cancelCommand;
      private Command okCommand;
      private Command newPasswordCommand;
      private Command insertPasswordCommand;

      private ImageToBitmapConverter convertor;
      private ImageSource nullImg;
      private List<bool> images;

      #endregion

      #region Public interfaces

      #region Constructors

      public ChangePasswordView()
      {
         InitializeComponent();
         convertor = new ImageToBitmapConverter();
         nullImg = (ImageSource)convertor.Convert(Properties.Resources.KeyNull, typeof(ImageSource), null, null);
         images = new List<bool> { false, false, false, false };
      }

      #endregion

      public ICommand CancelCommand
      {
         get
         {
            if (cancelCommand == null)
            {
               cancelCommand = new RelayCommand(
                  //Execute
                  Close,
                  // CanExecute
                  () => true
               );
            }
            return cancelCommand;
         }
      }

      public ICommand NewPasswordCommand
      {
         get
         {
            if (newPasswordCommand == null)
            {
               newPasswordCommand = new RelayCommand(
                  //Execute
                  ClearPassword,
                  // CanExecute
                  () => true
               );
            }
            return newPasswordCommand;
         }
      }

      public ICommand OkCommand
      {
         get
         {
            if (okCommand == null)
            {
               okCommand = new RelayCommand(
                  //Execute
                  () =>
                  {
                     //TODO: save password to some public property
                     Close();
                  },
                  // CanExecute
                  () => true
               );
            }
            return okCommand;
         }
      }

      public ICommand InsertPasswordCommand
      {
         get
         {
            if (insertPasswordCommand == null)
            {
               insertPasswordCommand = new RelayCommand(
                  //Execute
                  (o) =>
                  {
                     var img = Properties.Resources.KeyNull;
                     switch (o.ToString())
                     {
                        case "esc":
                           img = Properties.Resources.KeyEsc;
                           break;
                        case "up":
                           img = Properties.Resources.KeyUp;
                           break;
                        case "enter":
                           img = Properties.Resources.KeyEnter;
                           break;
                        case "left":
                           img = Properties.Resources.KeyLeft;
                           break;
                        case "down":
                           img = Properties.Resources.KeyDown;
                           break;
                        case "right":
                           img = Properties.Resources.KeyRight;
                           break;
                     }
                     var control = GetImageControl();
                     if (control != null)
                     {
                        control.Source = (ImageSource)convertor.Convert(img, typeof(ImageSource), null, null);
                     }

                  },
                  // CanExecute
                  (o) => true
               );
            }
            return insertPasswordCommand;
         }
      }

      #endregion

      #region Private helpers

      private void ClearPassword()
      {
         PassKey1Image.Source = nullImg;
         PassKey2Image.Source = nullImg;
         PassKey3Image.Source = nullImg;
         PassKey4Image.Source = nullImg;
         images = new List<bool> { false, false, false, false };
      }

      private System.Windows.Controls.Image GetImageControl()
      {
         if (images[0] == false)
         {
            images[0] = true;
            return PassKey1Image;
         }
         if (images[1] == false)
         {
            images[1] = true;
            return PassKey2Image;
         }
         if (images[2] == false)
         {
            images[2] = true;
            return PassKey3Image;
         }
         if (images[3] == false)
         {
            images[3] = true;
            return PassKey4Image;
         }

         return null;
      }

      #endregion
   }
}
