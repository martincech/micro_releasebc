﻿using System.Collections.ObjectModel;
using System.Linq;
using Bat2Library.Connection.Interface.Domain;
using CurvePoint = Bat2Library.Connection.Interface.Domain.CurvePoint;

namespace Bat2Config.Map
{
   public static class MapModelCurveToDataCurve
   {
      public static Models.Curve MapTo(this Curve curve)
      {
         var points = new ObservableCollection<Models.CurvePoint>();
         foreach (var point in curve.Points)
         {
            points.Add(new Models.CurvePoint
            {
               ValueX = point.ValueX,
               ValueY = point.ValueY
            });
         }

         return new Models.Curve
         {
            Name = curve.Name,
            Points = points
         };
      }

      public static Curve MapFrom(this Models.Curve curve)
      {
         var points = curve.Points.Select(point => new CurvePoint
         {
            ValueX = point.ValueX, ValueY = point.ValueY
         }).ToList();

         return new Curve
         {
            Name = curve.Name,
            Points = points
         };
      }
   }
}
