﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Windows.Input;
using Bat2Config.Models;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Microsoft.VisualBasic.FileIO;
using Microsoft.Win32;
using Utilities.Observable;

namespace Bat2Config.ViewModel
{
   public class CurveViewModel : ObservableObject
   {
      private ObservableCollection<Curve> growthCurve;
      private ObservableCollection<Curve> correctionCurve;

      private Curve activeItem;
      private string unitHeader;

      private ICommand addCommand;
      private ICommand deleteCommand;
      private ICommand loadCommand;

      public CurveViewModel(IView view)
      {
         view.DataContext = this;
         GrowthCurve = new ObservableCollection<Curve>();
         CorrectionCurve = new ObservableCollection<Curve>();
         UnitHeader = "gg";
         GrowthCurve.CollectionChanged += GrowthCurveOnCollectionChanged;
      }

      private void GrowthCurveOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
      {
         if (e.Action == NotifyCollectionChangedAction.Add)
         {
            foreach (Curve item in e.NewItems)
            {
               //item.PropertyChanged += PointOnPropertyChanged;
               foreach (var point in item.Points)
               {
                  point.PropertyChanged += PointOnPropertyChanged;
               }
            }
         }

         if (e.Action == NotifyCollectionChangedAction.Remove && e.NewItems != null)
         {
            foreach (Curve item in e.NewItems)
            {
               //item.PropertyChanged -= PointOnPropertyChanged;
               foreach (var point in item.Points)
               {
                  point.PropertyChanged -= PointOnPropertyChanged;
               }
            }
         }
      }

      private void PointOnPropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         var point = sender as CurvePoint;
         // if changed point is used for initial weight then update value
         if (point != null)
         {
            foreach (var weighing in StoreVM.Instance.PredefinedWeigingsVM.PredefinedWeighings)
            {
               weighing.UpdateInitialWeight(point);
            }      
            StoreVM.Instance.ConfigurationVM.WeighingConfigurationVM.UpdateInitialWeight(point);
         }
      }


      public ObservableCollection<Curve> GrowthCurve
      {
         get { return growthCurve; }
         set { SetProperty(ref growthCurve, value); }
      }

      public ObservableCollection<Curve> CorrectionCurve
      {
         get { return correctionCurve; }
         set { SetProperty(ref correctionCurve, value); }
      }

      public Curve ActiveItem
      {
         get { return activeItem; }
         set
         {
            SetProperty(ref activeItem, value);
            if (GrowthCurve.Contains(value))
            {
               UnitHeader = "Weight (g)";
            }

            if (CorrectionCurve.Contains(value))
            {
               UnitHeader = "Correction (%)";
            }

            UpdateCommands();
         }
      }

      public string UnitHeader
      {
         get { return unitHeader; }
         set { SetProperty(ref unitHeader, value); }
      }

      public ICommand AddCommand
      {
         get
         {
            return addCommand ??
                   (addCommand =
                      new RelayCommand(
                         (curves) =>
                         {
                            var curveList = ((ObservableCollection<Curve>)curves);
                            curveList.Add(new Curve()
                            {
                               Name = "New curve",
                               Points = new ObservableCollection<CurvePoint>()
                            });
                         }));
         }
      }

      public ICommand DeleteCommand
      {
         get
         {
            return deleteCommand ?? (deleteCommand = new RelayCommand((curves) =>
            {
               ((ObservableCollection<Curve>)curves).Remove(ActiveItem);
               UpdateCommands();
            }, (curves) => curves != null && ((ObservableCollection<Curve>)curves).Contains(ActiveItem)));
         }
      }

      public ICommand LoadCommand
      {
         get
         {
            return loadCommand ?? (loadCommand = new RelayCommand((curves) =>
            {
               OpenFileDialog dialog = new OpenFileDialog();
               dialog.Filter = ".csv|*.csv";
               var result = dialog.ShowDialog();
               if (result.HasValue && result.Value)
               {
                  Curve c = new Curve();
                  var fileName = Path.GetFileName(dialog.FileName);
                  c.Points = new ObservableCollection<CurvePoint>();
                  if (fileName != null)
                     c.Name = fileName.Split(new[]{"."},StringSplitOptions.None)[0];

                  TextFieldParser parser = new TextFieldParser(dialog.FileName);
                  parser.Delimiters = new[] {","};
                  while (!parser.EndOfData)
                  {
                     string[] fields = parser.ReadFields();
                     if (fields != null && fields.Length == 2)
                     {
                        int day;
                        double value;
                        if (Int32.TryParse(fields[0], out day) && Double.TryParse(fields[1],NumberStyles.Float,CultureInfo.InvariantCulture, out value))
                        {
                           c.Points.Add(new CurvePoint(){ValueX = day, ValueY = Convert.ToInt32(value*1000 * 10)});
                        }
                     }
                  }
                  ((ObservableCollection<Curve>)curves).Add(c);
               }
            }));
         }
      }

      private void UpdateCommands()
      {
         ((RelayCommand)AddCommand).RaiseCanExecuteChanged();
         ((RelayCommand)DeleteCommand).RaiseCanExecuteChanged();
      }
   }
}
