﻿using System;
using System.ComponentModel;
using Utilities.Observable;
// ReSharper disable InconsistentNaming

namespace Bat2Config.ViewModel
{
  
   public class StoreVM : ObservableObject
   {
      private static StoreVM _instance;

      private FileSelectViewModel fileSelectViewModel;
      private ContactListViewModel contactListViewModel;
      private CurveViewModel curveViewModel;
      private PredefinedWeigingsViewModel predefinedWeigingsViewModel;
      private WeighingPlansViewModel weighingPlansViewModel;
      private ConfigurationViewModel configurationViewModel;
      private CommandViewModel commandViewModel;

      private StoreVM()
      {        
      }

      public static StoreVM Instance
      {
         get { return _instance ?? (_instance = new StoreVM()); }
      }

      public FileSelectViewModel FileSelectVM
      {
         get { return fileSelectViewModel; }
         set { SetProperty(ref fileSelectViewModel, value); }
      }

      public ContactListViewModel ContactListVM
      {
         get { return contactListViewModel; }
         set { SetProperty(ref contactListViewModel, value); }
      }

      public CurveViewModel CurveVM
      {
         get { return curveViewModel; }
         set { SetProperty(ref curveViewModel, value); }
      }

      public PredefinedWeigingsViewModel PredefinedWeigingsVM
      {
         get { return predefinedWeigingsViewModel; }
         set { SetProperty(ref predefinedWeigingsViewModel, value); }
      }

      public WeighingPlansViewModel WeighingPlansVM
      {
         get { return weighingPlansViewModel; }
         set { SetProperty(ref weighingPlansViewModel, value); }
      }

      public ConfigurationViewModel ConfigurationVM
      {
         get { return configurationViewModel; }
         set { SetProperty(ref configurationViewModel, value); }
      }

      public CommandViewModel CommandVM
      {
         get { return commandViewModel; }
         set { SetProperty(ref commandViewModel, value); }
      }
   }
}
