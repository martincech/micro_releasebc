﻿using System.Windows.Input;
using Bat2Config.View.Configuration;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel.Configuration
{
   public class PasswordViewModel : ObservableObject
   {
      #region Private fields

      private string password;

      private Command changePasswordCommand;

      #endregion

      #region Public interface

      #region Constructors

      public PasswordViewModel(IView view, DeviceInfo device)
      {
         Password = device.Password;
         view.DataContext = this;
      }

      #endregion

      public ICommand ChangePasswordCommand
      {
         get
         {
            if (changePasswordCommand == null)
            {
               changePasswordCommand = new RelayCommand(
                  //Execute
                  () =>
                  {
                     var passwordView = new ChangePasswordView();
                     passwordView.DataContext = this;
                     passwordView.ShowDialog();

                  },
                  // CanExecute
                  () => true
               );
            }
            return changePasswordCommand;
         }
      }

      #region Properties

      public string Password
      {
         get
         {
            return password;
         }
         set
         {
            SetProperty(ref password, value);
         }
      }


      #endregion 

      #endregion
   }
}
