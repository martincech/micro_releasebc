﻿using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel.Configuration
{
   public class DisplayConfigurationViewModel : ObservableObject
   {
      #region Private fields

      private byte contrast;
      private DisplayModeE mode;
      private bool savePower;

      private short backlightDuration;
      private byte backlightIntensity;
      private BacklightModeE backlightMode;

      #endregion

      #region Public interface

      #region Constructors

      public DisplayConfigurationViewModel(IView view, DisplayConfiguration device)
      {
         Contrast = device.Contrast;
         Mode = device.Mode;
         BacklightDuration = device.BacklightDuration;
         BacklightIntensity = device.BacklightIntensity;
         BacklightMode = device.BacklightMode;
         SavePower = device.SavePower;
         view.DataContext = this;
      }

      #endregion

      #region Properties

      public byte Contrast { get { return contrast; } set { SetProperty(ref contrast, value); } }
      public DisplayModeE Mode { get { return mode; } set { SetProperty(ref mode, value); } }
      public bool SavePower { get { return savePower; } set { SetProperty(ref savePower, value); } }

      public short BacklightDuration { get { return backlightDuration; } set { SetProperty(ref backlightDuration, value); } }
      public byte BacklightIntensity { get { return backlightIntensity; } set { SetProperty(ref backlightIntensity, value); } }
      public BacklightModeE BacklightMode { get { return backlightMode; } set { SetProperty(ref backlightMode, value); } }

      #endregion

      #endregion
   }
}
