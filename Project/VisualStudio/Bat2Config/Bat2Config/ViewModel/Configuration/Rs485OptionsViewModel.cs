﻿using Bat2Library;
using Bat2Library.Connection.Interface.Domain;
using Utilities.Observable;

namespace Bat2Config.ViewModel.Configuration
{
   public class Rs485OptionsViewModel : ObservableObject
   {
      #region Private fields

      private Rs485ModeE mode;
      private bool enabled;

      #endregion

      #region Public interface

      #region Constructors

      public Rs485OptionsViewModel(Rs485Options option)
      {
         Mode = (Rs485ModeE)option.Mode;
         Enabled = option.Enabled;
      }     

      #endregion

      #region Properties

      public Rs485ModeE Mode { get { return mode; } set { SetProperty(ref mode, value); } }
      public bool Enabled { get { return enabled; } set { SetProperty(ref enabled, value); } }

      #endregion 

      #endregion
   }
}
