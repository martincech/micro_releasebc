﻿using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Bat2Config.Map;
using Bat2Config.Properties;
using Bat2Config.ViewModel.Configuration;
using Bat2Library.Bat2Old;
using Bat2Library.Bat2Old.Map;
using Bat2Library.Configuration;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Applications;
using Desktop.Wpf.Presentation;
using Microsoft.Win32;
using Utilities.Observable;

namespace Bat2Config.ViewModel
{
   public class FileSelectViewModel : ObservableObject
   {
      private Bat2DeviceData data;
      private string filePath;
      private BitmapImage messageImage;
      private string message;
      private bool createNew;

      private ICommand loadCommnad;

      public Bat2DeviceData Data
      {
         get { return data; }
         set { SetProperty(ref data, value); }
      }

      public string FilePath
      {
         get { return filePath; }
         set { SetProperty(ref filePath, value); }
      }

      public FileSelectViewModel(IView view)
      {
         view.DataContext = this;
         MessageImage = ToBitmapImage(Resources.Info);
         Message = "Please select Bat2 hardcopy *.bin file";
      }

      public ICommand LoadCommnad
      {
         get
         {
            return loadCommnad ?? (loadCommnad = new RelayCommand(() =>
            {
               var fileDialog = new OpenFileDialog {Filter = "Bat2 hardcopy|*.bin"};
               var showDialog = fileDialog.ShowDialog();
               if (showDialog != null && (bool)showDialog)
               {
                  FilePath = fileDialog.FileName;

                  var oldData = Bat2Conversion.LoadFlash(File.ReadAllBytes(FilePath));
                  if (oldData != null)
                  {
                     Data = MapOldToNew.Map(oldData, new FileReader("",0).LoadDefault());
                     LoadData();
                     MessageImage = ToBitmapImage(Resources.KeyEnter);
                     Message = "Selected file succesfuly loaded";
                  }
                  else
                  {
                     MessageImage = ToBitmapImage(Resources.KeyEsc);
                     Message = "Selected file is not Bat2 file";
                  }
               }
            }));
         }
      }

      public BitmapImage MessageImage
      {
         get { return messageImage; }
         set { SetProperty(ref messageImage, value); }
      }

      public string Message
      {
         get { return message; }
         set { SetProperty(ref message, value); }
      }

      public bool CreateNew
      {
         get { return createNew; }
         set
         {
            SetProperty(ref createNew, value);
            if (value)
            {
               Data = new FileReader("", 0).LoadDefault();
               LoadData();
            }
            else
            {
               Data = null;
               MessageImage = ToBitmapImage(Resources.Info);
               Message = "Please select Bat2 hardcopy *.bin file";
               FilePath = "";
            }
         }
      }

      private void LoadData()
      {
         if (Data == null || StoreVM.Instance.ContactListVM == null)
         {
            return;
         }
         StoreVM.Instance.ContactListVM.ContatList = new ObservableCollection<Contact>(Data.Contacts);

         StoreVM.Instance.CurveVM.GrowthCurve.Clear();
         foreach (var curve in Data.GrowthCurves)
         {
            StoreVM.Instance.CurveVM.GrowthCurve.Add(curve.MapTo());
         }
         StoreVM.Instance.CurveVM.CorrectionCurve.Clear();
         foreach (var curve in Data.CorrectionCurves)
         {
            StoreVM.Instance.CurveVM.CorrectionCurve.Add(curve.MapTo());
         }

         StoreVM.Instance.WeighingPlansVM.WeighignPlans = new ObservableCollection<WeighingPlan>(Data.WeighingPlans);
         StoreVM.Instance.PredefinedWeigingsVM.PredefinedWeighings = new ObservableCollection<WeighingConfigurationViewModel>(
            Data.PredefinedWeighings.Select(x => new WeighingConfigurationViewModel(x, false)));

         StoreVM.Instance.ConfigurationVM.WeighingConfigurationVM.GrowthCurvesNames =
            StoreVM.Instance.CurveVM.GrowthCurve;
         StoreVM.Instance.ConfigurationVM.WeighingConfigurationVM.CorrectionCurvesNames =
           StoreVM.Instance.CurveVM.CorrectionCurve;
         StoreVM.Instance.ConfigurationVM.WeighingConfigurationVM.WeighingPlansNames =
           StoreVM.Instance.WeighingPlansVM.WeighignPlans;
         StoreVM.Instance.ConfigurationVM.WeighingConfigurationVM.PredefinedConfigurationList =
         StoreVM.Instance.PredefinedWeigingsVM.PredefinedWeighings;
         StoreVM.Instance.CommandVM.Name = data.Configuration.DeviceInfo.Name;

         // after load data, set initial weight from growth curve, if prediction mode set to growth curve
         foreach (var weighings in StoreVM.Instance.PredefinedWeigingsVM.PredefinedWeighings)
         {
            weighings.GrowthCurvesNames = StoreVM.Instance.CurveVM.GrowthCurve;
            weighings.CorrectionCurvesNames = StoreVM.Instance.CurveVM.CorrectionCurve;
            weighings.SetInitialWeights();
         }
      }

      private BitmapImage ToBitmapImage(Bitmap image)
      {
         MemoryStream ms = new MemoryStream();
         BitmapImage bi = new BitmapImage();
         image.Save(ms, ImageFormat.Png);
         bi.BeginInit();
         bi.StreamSource = ms;
         bi.EndInit();
         return bi;
      }
   }
}
