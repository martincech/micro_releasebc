﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Bat2Config.Map;
using Bat2Config.Properties;
using Bat2Config.View.Configuration;
using Bat2Config.ViewModel.Configuration;
using Bat2Library.Connection.Interface.Domain;
using Desktop.Wpf.Presentation;
using Utilities.Observable;

namespace Bat2Config.ViewModel
{
   public class ConfigurationViewModel : ObservableObject
   {
      private ObservableCollection<ViewHolder> views;

      private Bat2IdentificationViewModel bat2IdentificationViewModel;
      private CountryViewModel countryViewModel;
      private DisplayConfigurationViewModel displayConfigurationViewModel;
      private EthernetViewModel ethernetViewModel;
      private GsmMessageViewModel gsmMessageViewModel;
      private PasswordViewModel passwordViewModel;
      private Rs485OptionsListViewModel rs485OptionsListViewModel;
      private WeighingConfigurationViewModel weighingConfigurationViewModel;
      private WeightUnitViewModel weightUnitViewModel;
      private CellularDataViewModel cellularDataViewModel;
      private DataPublicationViewModel dataPublicationViewModel;

      public ConfigurationViewModel(IView view)
      {
         Views = new ObservableCollection<ViewHolder>();
         StoreVM.Instance.FileSelectVM.PropertyChanged += FileSelectViewModelObject_PropertyChanged;
         view.DataContext = this;
      }

      void FileSelectViewModelObject_PropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if (e.PropertyName == "Data")
         {
            var data = StoreVM.Instance.FileSelectVM.Data;
            if (data != null)
            {
               Views.Clear();
               //Views.Add(Resources.TitleListBat2Info, new Bat2IdentificationView());
               bat2IdentificationViewModel = new Bat2IdentificationViewModel(new Bat2IdentificationView(), data);

               Views.Add(new ViewHolder(){ Key = Resources.TitleListCountry, Value = new CountryView(false)});
               countryViewModel = new CountryViewModel(Views.Last().Value, data.Configuration.Country);

               //Views.Add("Cellular data", new CellularDataView());
               Views.Add(new ViewHolder() { Key = "Cellular data", Value = new CellularDataView() });
               cellularDataViewModel = new CellularDataViewModel(Views.Last().Value, data.Configuration.CellularData);

               //Views.Add("Data publication", new DataPublicationView());
               Views.Add(new ViewHolder() { Key = "Data publication", Value = new DataPublicationView() });
               dataPublicationViewModel = new DataPublicationViewModel(Views.Last().Value, data.Configuration.DataPublication);

               //Views.Add(Resources.TitleListDisplay, new DisplayConfigurationView(false));
               Views.Add(new ViewHolder() { Key = Resources.TitleListDisplay, Value = new DisplayConfigurationView(false) });
               displayConfigurationViewModel = new DisplayConfigurationViewModel(Views.Last().Value, data.Configuration.DisplayConfiguration);

               //Views.Add(Resources.TitleListEthernet, new EthernetView());
               Views.Add(new ViewHolder() { Key = Resources.TitleListEthernet, Value = new EthernetView() });
               ethernetViewModel = new EthernetViewModel(Views.Last().Value, data.Configuration.Ethernet);

               //Views.Add(Resources.TitleListGsm, new GsmMessageView());
               Views.Add(new ViewHolder() { Key = Resources.TitleListGsm, Value = new GsmMessageView() });
               gsmMessageViewModel = new GsmMessageViewModel(Views.Last().Value, data.Configuration.GsmMessage);

               //Views.Add(Resources.TitleListPassword, new PasswordView(false));
               //passwordViewModel = new PasswordViewModel(Views.Last().Value, data.Configuration.DeviceInfo);

               //Views.Add(Resources.TitleListRs485, new Rs485OptionsListView());
               Views.Add(new ViewHolder() { Key = Resources.TitleListRs485, Value = new Rs485OptionsListView() });
               rs485OptionsListViewModel = new Rs485OptionsListViewModel(
                  Views.Last().Value,
                  data.Configuration.Rs485Options.FirstOrDefault(),
                  data.Configuration.Rs485Options.LastOrDefault(),
                  data.Configuration.ModbusOptions,
                  data.Configuration.MegaviOptions,
                  data.Configuration.DacsOptions
                  );

               weighingConfigurationViewModel =
                  new WeighingConfigurationViewModel(data.Configuration.WeighingConfiguration,true);


               //Views.Add(Resources.TitleListWeighingConfig, new WeighingConfigurationView() { DataContext = weighingConfigurationViewModel });
               Views.Add(new ViewHolder() { Key = Resources.TitleListWeighingConfig, Value = new WeighingConfigurationView() { DataContext = weighingConfigurationViewModel } });

               //Views.Add(Resources.TitleListWeighingUnit, new WeightUnitView(false));
               Views.Add(new ViewHolder() { Key = Resources.TitleListWeighingUnit, Value = new WeightUnitView(false) });
               weightUnitViewModel = new WeightUnitViewModel(Views.Last().Value, data.Configuration.WeightUnits);

               //RaisePropertyChanged("Views");
            }
         }
      }

      public ObservableCollection<ViewHolder> Views
      {
         get { return views; }
         set { SetProperty(ref views, value); }
      }

      public Bat2IdentificationViewModel Bat2IdentificationVM
      {
         get { return bat2IdentificationViewModel; }
      }

      public Country CountryModel
      {
         get { return countryViewModel.Map(); }
      }
      public DisplayConfiguration DisplayConfigurationModel
      {
         get { return displayConfigurationViewModel.Map(); }
      }
      public Ethernet EthernetModel
      {
         get { return ethernetViewModel.Map(); }
      }
      public GsmMessage GsmMessageModel
      {
         get { return gsmMessageViewModel.Map(); }
      }
      public CellularData CellularDataModel
      {
         get { return cellularDataViewModel.Map(); }
      }
      public IEnumerable<Rs485Options> Rs485OptionsModel
      {
         get { return rs485OptionsListViewModel.Map(); }
      }

      public DacsOptions DacsOptionsModel
      {
         get { return rs485OptionsListViewModel.DacsOptions.Map(); }
      }

      public MegaviOptions MegaviOptionsModel
      {
         get { return rs485OptionsListViewModel.MegaviOptions.Map(); }
      }

      public ModbusOptions ModbusOptionsModel
      {
         get { return rs485OptionsListViewModel.ModbusOptions.Map(); }
      }

      public WeighingConfiguration WeighingConfigurationModel
      {
         get { return weighingConfigurationViewModel.Map(); }
      }

      // ReSharper disable once InconsistentNaming
      public WeighingConfigurationViewModel WeighingConfigurationVM
      {
         get { return weighingConfigurationViewModel; }
      }

      public WeightUnits WeightUnitModel
      {
         get { return weightUnitViewModel.Map(); }
      }

      public DataPublication DataPublicationModel
      {
         get { return dataPublicationViewModel.Map(); }
      }

      public DeviceInfo DeviceInfoModel
      {
         get
         {
            return new DeviceInfo()
               {
                  Name = bat2IdentificationViewModel.Name
               };
         }
      }

   }

   public class ViewHolder
   {
      public string Key { get; set; }
      public IView Value { get; set; }
   }
}
