﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Bat2Config.Convertors
{
   public class DecimalPointConverter : IValueConverter
   {
      #region Implementation of IValueConverter

      /// <summary>
      /// Converts a value. 
      /// </summary>
      /// <returns>
      /// A converted value. If the method returns null, the valid null value is used.
      /// </returns>
      /// <param name="value">The value produced by the binding source.</param><param name="targetType">The type of the binding target property.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
      public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
      {
         try
         {
            int param = int.Parse((string)parameter);
            int val = (int)value;
            var exp = Math.Pow(10, param);
            return val * exp;
         }
         catch {}
         return 0;
      }

      /// <summary>
      /// Converts a value. 
      /// </summary>
      /// <returns>
      /// A converted value. If the method returns null, the valid null value is used.
      /// </returns>
      /// <param name="value">The value that is produced by the binding target.</param><param name="targetType">The type to convert to.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
      public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
      {
          try
         {
         int param = int.Parse((string)parameter);
         double val = double.Parse(value.ToString());
         var exp = Math.Pow(10, param * -1);
         return System.Convert.ToInt32(val * exp);
         }
          catch { }
          return 0;
      }


      #endregion
   }
}
