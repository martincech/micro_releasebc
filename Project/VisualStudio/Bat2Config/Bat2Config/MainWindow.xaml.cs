﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Bat2Config.View;
using Bat2Config.ViewModel;
using Bat2Config.ViewModel.Shared;

namespace Bat2Config
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      public MainWindow()
      {
         InitializeComponent();
         var selectFile = new Navigation()
         {
            Title = Properties.Resources.WindowTitleImport,
            TitleText = Properties.Resources.WindowTitleTextImport,
            View = new FileSelectView(),
            CanNext = () => StoreVM.Instance.FileSelectVM.Data != null
         };
         StoreVM.Instance.FileSelectVM = new FileSelectViewModel(selectFile.View);
         StoreVM.Instance.FileSelectVM.PropertyChanged += Update;
         navigationView.NavigationViewModel.NavigationList.Add(selectFile);

         var contacts = new Navigation()
         {
            Title = "Contacts",
            TitleText = "List of contacts",
            View = new ContactListView() { }
         };
         StoreVM.Instance.ContactListVM = new ContactListViewModel(contacts.View);
         navigationView.NavigationViewModel.NavigationList.Add(contacts);


         var curves = new Navigation()
         {
            Title = "Curves",
            TitleText = "Curves settings",
            View = new CurveView() { }

         };
         StoreVM.Instance.CurveVM = new CurveViewModel(curves.View);
         navigationView.NavigationViewModel.NavigationList.Add(curves);

         var weighingPlans = new Navigation()
         {
            Title = "Plans",
            TitleText = "Weighing plans",
            View = new WeighingPlansView() { }
         };
         StoreVM.Instance.WeighingPlansVM = new WeighingPlansViewModel(weighingPlans.View);
         navigationView.NavigationViewModel.NavigationList.Add(weighingPlans);


         var predefinedWeighings = new Navigation()
         {
            Title = "Predefined",
            TitleText = "Predefined weighings",
            View = new PredefinedWeigingsView() { }
         };
         StoreVM.Instance.PredefinedWeigingsVM = new PredefinedWeigingsViewModel(predefinedWeighings.View);
         navigationView.NavigationViewModel.NavigationList.Add(predefinedWeighings);

         var config = new Navigation()
         {
            Title = Properties.Resources.WindowTitleConfig,
            TitleText = Properties.Resources.WindowTitleTextConfig,
            View = new ConfigurationView() { }
         };
         StoreVM.Instance.ConfigurationVM = new ConfigurationViewModel(config.View);
         navigationView.NavigationViewModel.NavigationList.Add(config);

         var commands = new Navigation()
         {
            Title = Properties.Resources.WindowTitleActions,
            TitleText = Properties.Resources.WindowTitleTextActions,
            View = new CommandView()
         };
         StoreVM.Instance.CommandVM = new CommandViewModel(commands.View)
         {
            Views = new ObservableCollection<ModelExport>()
                  {
                    new ModelExport(){View = Properties.Resources.ExportConfig, Export = true},
                    new ModelExport(){View = Properties.Resources.ExportGrowthC, Export = true},//"Growth curves"
                    new ModelExport(){View = Properties.Resources.ExportCorrectionC, Export = true},//"Correction curves"
                    new ModelExport(){View = Properties.Resources.ExportContacts, Export = true},
                    new ModelExport(){View = Properties.Resources.ExportWeighingPlans, Export = true},
                    new ModelExport(){View = Properties.Resources.ExportPredefinedWeighings, Export = true}
                  }
         };
         navigationView.NavigationViewModel.NavigationList.Add(commands);
         navigationView.NavigationViewModel.ActiveItem = navigationView.NavigationViewModel.NavigationList.First();
         StoreVM.Instance.FileSelectVM.CreateNew = true;
      }

      private void Update(object sender, PropertyChangedEventArgs e)
      {
         navigationView.NavigationViewModel.UpdateCommnads();
      }
   }
}
