﻿using System;
using System.Windows;

namespace Bat2Config
{
   /// <summary>
   /// Interaction logic for App.xaml
   /// </summary>
   public partial class App : Application
   {
      protected override void OnStartup(StartupEventArgs e)
      {
         base.OnStartup(e);
         AppDomain.CurrentDomain.UnhandledException += HandlerMethod;
      }
      private void HandlerMethod(object sender, UnhandledExceptionEventArgs e)
      {
         var ex = (Exception)e.ExceptionObject;
         MessageBox.Show(ex.Message + ex.StackTrace,
            "Fatal Error", MessageBoxButton.OK, MessageBoxImage.Stop);
      }
   }
}
