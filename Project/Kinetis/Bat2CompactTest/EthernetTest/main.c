#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"
#include "Ethernet/EthernetLibrary.h"
#include "Communication/Communication.h"

#define IP_STRING_SIZE  20

void main(void)
{
byte Ip[IP_STRING_SIZE + 1];
   CpuInit();
   SysInit();
   BacklightInit();
   GInit();
   GSetFont( TAHOMA16);
   BacklightTest(BACKLIGHT_INTENSITY_MAX);
   PowerPortInit();
   EthernetLibraryInitNetworkInterface();

   Ethernet.Dhcp = YES;

   forever {
      SysDelay(1000);
      GClear();
      GFlush();
      SysDelay(50);
      GTextAt(0, 0);

      EthernetLibraryExecute();

      cprintf("Rx %d/Tx %d\n", EthernetLibraryRxPacketCount(), EthernetLibraryTxPacketCount());
      EthernetLibraryIPv4( Ip, IP_STRING_SIZE + 1);
      cprintf("IP %s\n", Ip);
      GFlush();
   }
}

void SysTimerExecute( void) {}