#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Graphic/Graphic.h"
#include "Display/Backlight.h"
#include "Display/DisplayConfiguration.h"
#include "Graphic/Gpu.h"
#include "Fonts.h"
#include "Console/conio.h"
#include "Power/Power.h"
#include "Usb/Usb.h"
#include "Multitasking/MUltitasking.h"
#include "Rtc/Rtc.h"

void main(void)
{
   CpuInit();
   SysInit();
   BacklightInit();
   GInit();
   GSetFont( TAHOMA16);
   BacklightTest(BACKLIGHT_INTENSITY_MAX);
   PowerInit();
   UsbInit();
   MultitaskingInit();
   RtcInit();

   UsbDevicePowerEnable();
   UsbEnumerationEnable( YES);

   forever {
      
      SysDelay(1000);
      GClear();
      GFlush();
      GTextAt(0, 0);


      UsbExecute();
      cputs("USB: ");

      switch(UsbStatusGet()) {
         case USB_STATE_OFF:
            cputs("OFF");
            break;

         case USB_STATE_DEVICE_CHARGER_DETECTION:
            cputs("Charger detection");
            break;

         case USB_STATE_CHARGER:
            cputs("Charger");
            break;

         case USB_STATE_DEVICE_ATTACHED:
            cputs("Attached");
            break;

         case USB_STATE_DEVICE_CONNECTED:
            cputs("Connected");
            break;

         default:
            cputs("Unknown");
            break;
      }

      cputch('\n');
      GFlush();
   }
}

void SysTimerExecute( void) {}