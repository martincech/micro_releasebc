//*****************************************************************************
//
//    Hardware.h   Bat2 wired platform hardware descriptions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Cpu/uMKL02Z32VFM4.h"
#include "Cpu/Io.h"
#include "Unisys/Uni.h"


#define OPTION_SIGMA_DELTA

//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

#define F_CPU      21000000L                      // Main clock by synthesizer [Hz]
#define F_SYSTEM   21000000L
#define F_BUS      21000000L

#define _K_SYSTEM_BASE            1    // skip K_NULL
#include "System/SystemKey.h"          // system events

//-----------------------------------------------------------------------------
// System timer
//-----------------------------------------------------------------------------

#define TIMER_WAKE_UP_PERIOD  1000
#define TIMER_PERIOD      1                      // System timer period [ms]
#define TIMER_FAST_PERIOD 50                     // System timer 'fast' tick period [ms]
#define TIMER_SLOW_PERIOD 500                    // System timer 'slow' tick period [ms]
#define TIMER_FLASH1      300                    // Flash 1 delay [ms]
#define TIMER_FLASH2      200                    // Flash 2 delay [ms]
#define TIMER_TIMEOUT     10                     // Inactivity timeout [s]
#define TIMER_SEC_PERIOD  1000
//-----------------------------------------------------------------------------
// System
//-----------------------------------------------------------------------------

#define SYSTEM_CLOCK
//#define SLEEP_ENABLE

//-----------------------------------------------------------------------------
// UART common
//-----------------------------------------------------------------------------

#define UART_PORTS_COUNT   1
#define UART_MODBUS        1

#define UART0_PRIORITY       1         // UART0 IRQ

//-----------------------------------------------------------------------------
// UART0
//-----------------------------------------------------------------------------

#define UART0_RTS_PIN         KINETIS_PIN_PORTB00

#define Uart0PortInit()       PinFunction( KINETIS_PIN_UART0_TX_PORTB02, KINETIS_PIN_UART0_TX_PORTB02_FUNCTION); \
                              PinFunction( KINETIS_PIN_UART0_RX_PORTB01, KINETIS_PIN_UART0_RX_PORTB01_FUNCTION); \
                              PinPullup(KINETIS_PIN_UART0_RX_PORTB01); \
                              PinFunction( UART0_RTS_PIN, KINETIS_GPIO_FUNCTION); \
                              GpioOutput( UART0_RTS_PIN); \
                              GpioClr( UART0_RTS_PIN)

#define Uart0PortDeinit()     PinFunction( KINETIS_PIN_UART0_TX_PORTB02, KINETIS_GPIO_FUNCTION); \
                              PinFunction( KINETIS_PIN_UART0_RX_PORTB01, KINETIS_GPIO_FUNCTION)

#define UART0_BAUD           9600           // baud rate
#define UART0_FORMAT         UART_8BIT      // default format
#define UART0_TIMEOUT        10             // intercharacter timeout [ms]

#define UART0_TX_ENABLE_REGISTER   &GpioRegisterSet( UART0_RTS_PIN)
#define UART0_TX_DISABLE_REGISTER  &GpioRegisterClr( UART0_RTS_PIN)
#define UART0_TX_ENABLE_MASK       GpioMask( UART0_RTS_PIN)


//-----------------------------------------------------------------------------
// Ads1232
//-----------------------------------------------------------------------------

#define PowerAdcEn()

#define ADC_INTERRUPT_HANDLER       PORTB_IRQHandler
#define ADC_INTERRUPT_NUMBER        PORTB_IRQn
#define ADC_INTERRUPT_PRIORITY      2

#define AdcInterruptHandler()       void __irq ADC_INTERRUPT_HANDLER( void)
#define AdcInitInt()                CpuIrqAttach(ADC_INTERRUPT_NUMBER, ADC_INTERRUPT_PRIORITY, ADC_INTERRUPT_HANDLER); \
                                    CpuIrqEnable(ADC_INTERRUPT_NUMBER)
#define AdcClearInt()               EintClearFlag( ADC_DOUT_RDY)
#define AdcEnableInt()              EintEnable( ADC_DOUT_RDY, EINT_SENSE_FALLING_EDGE)
#define AdcDisableInt()             EintDisable( ADC_DOUT_RDY)

#define ADC_SCLK       KINETIS_PIN_PORTB07
#define ADC_DOUT_RDY   KINETIS_PIN_PORTB06
#define ADC_POWERDOWN  KINETIS_PIN_PORTA03

#define AdcPortInit()      PinFunction(ADC_SCLK, KINETIS_GPIO_FUNCTION); \
                           PinFunction(ADC_DOUT_RDY, KINETIS_GPIO_FUNCTION); \
                           PinFunction(ADC_POWERDOWN, KINETIS_GPIO_FUNCTION); \
                           GpioOutput(ADC_SCLK); \
                           GpioOutput(ADC_POWERDOWN); \
                           GpioInput(ADC_DOUT_RDY);

#define AdcSetSCLK()     GpioSet(ADC_SCLK)
#define AdcClrSCLK()     GpioClr(ADC_SCLK)

#define AdcPowerDownDisable()     GpioSet(ADC_POWERDOWN)
#define AdcPowerDownEnable()     GpioClr(ADC_POWERDOWN)

#define AdcGetDOUT()     GpioGet(ADC_DOUT_RDY)
#define AdcGetRDY()      !GpioGet(ADC_DOUT_RDY)

//-----------------------------------------------------------------------------
// ADC filtering
//-----------------------------------------------------------------------------

#define FILTER_MAX_AVERAGING  50       // max. width of averaging window
#define FILTER_SLOW_RESTART    1       // slow stabilisation after restart

// Basic data types :
typedef byte TSamplesCount;            // samples counter

//------------------------------------------------------------------------------
// PWM
//------------------------------------------------------------------------------

#define PwmPortInit()
#define PwmOn()         StatusLedBOn()
#define PwmOff()        StatusLedBOff()

//-----------------------------------------------------------------------------
// Samples FIFO
//-----------------------------------------------------------------------------

#define FIFO_SIZE       128            // samples FIFO capacity

//------------------------------------------------------------------------------
//  Power control
//------------------------------------------------------------------------------

#define PowerAdcOn()
#define PowerAdcOff()

//-----------------------------------------------------------------------------

#define LED       

#define LedInit()      /*PinFunction(LED, KINETIS_GPIO_FUNCTION); \
                   GpioOutput(LED)*/

#define LedOn()         //GpioClr(LED)
#define LedOff()        //GpioSet(LED)
#define LedToggle()     //GpioToggle(LED)


#define IAdcChannelsInit()   PinFunction(KINETIS_PIN_PORTB05, 0); // revert from NMI function to default function

#define TEMPERATURE_IADC_CHANNEL 0
#define HUMIDITY_IADC_CHANNEL    1
#define CO2_IADC_CHANNEL         16

#endif
