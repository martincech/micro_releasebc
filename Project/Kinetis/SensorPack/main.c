#include "Hardware.h"
#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Filter/FilterRelative.h"
#include "Ads1232/Ads1232.h"
#include "Uart/Uart.h"
#include "Sensors/Weight.h"
#include "Sensors/Sensors.h"
#include "Weight/WeightDef.h"
#include "Timer/Timer.h"
#include "mb.h"
#include <string.h>


#define BUFFER_SIZE  128 // must be power of 2

TWeightSample Buffer[BUFFER_SIZE];

TWeightSample *BufferPtr;

TWeightSample *BufferPtrWrite;

int SampleId = 0;

void main( void) {
   CpuInit();
   LedInit();
   SysInit();
   SensorsInit();
   memset(Buffer, 0xFF, sizeof(Buffer));
   AdcInit();

   eMBInit( 
      MB_RTU, 
      1,  
      UART_UART0,
      19200, 
      MB_PAR_EVEN);
   // Enable the Modbus Protocol Stack.
   eMBEnable(  );
 
   BufferPtrWrite = &Buffer[0];

   AdcStart();

   _SysTimerEnable();
   forever {
      InterruptDisable();
      eMBPoll(  );
      InterruptEnable();
   }
   eMBDisable();
}

static int LedNextOnTick;
static int LedNextOffTick;

void SysTimerExecute( void) {
   extern void vMBPortTimerExecute( void );
   vMBPortTimerExecute();

   int Now = SysTimer();

   if(TimerAfter(Now, LedNextOnTick)) {
      LedOn();
      LedNextOnTick = Now + 2000;
      LedNextOffTick = Now + 150;
   } else if(TimerAfter(Now, LedNextOffTick)) {
      LedOff();
   }
}

void FilterNextSample( TRawWeight Sample)
// Process next sample
{
   BufferPtrWrite->Weight = Sample;
   BufferPtrWrite->Id = SampleId++;
   BufferPtrWrite++;

   if(SampleId == 0xFFFF) {
      SampleId = 0;
   }

   if(BufferPtrWrite == &Buffer[BUFFER_SIZE]) {
      BufferPtrWrite = &Buffer[0];
   }
}

void AdcCallback( void)
// User implemented callback function
{}

void FilterStart( void)
// User implemented callback function
{}

void FilterStop( void)
// User implemented callback function
{}


void WeightRead(int i, TWeightSample *sample)
// read last ith sample from samples buffer 
{
   if(i >= BUFFER_SIZE){
      return;
   }
   TWeightSample *BufferPtrRead;
   BufferPtrRead = BufferPtrWrite - i - 1;

   if(BufferPtrRead < &Buffer[0]) {
      BufferPtrRead += &Buffer[BUFFER_SIZE] - &Buffer[0];
   }
   memcpy(sample, BufferPtrRead, sizeof(TWeightSample));
}

uint16 GetWeightSamplesRate()
// get samples rate in Hz
{
  return 10;
}