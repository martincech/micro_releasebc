//******************************************************************************
//
//    Main.c        HID bootloader
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Cpu/Cpu.h"
#include "Bootloader.h"
#include "System/System.h"

void main(void)
{
   CpuInit();
   SysTimerStart();
   _SysTimerEnable();
   BootloaderInit();
   forever {
      BootloaderExecute();
      WatchDog();
   }
}

void SysTimerExecute( void) {
   BootloaderTimer();
}