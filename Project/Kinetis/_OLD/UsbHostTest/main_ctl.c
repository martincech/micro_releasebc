#include "Multitasking/Multitasking.h"
#include "Hardware.h"

int main(void)
{
   CpuInit();
   SysInit();
   WatchDogInit();                     // start watchdog
   RtcInit();                          // Realtime clock
   UsbInit();
   PowerPortInit();

            forever {
               SysDelay(100);
               if(!EfsInit()) {
                  continue;
               }
               EfsSwitchOff();
               
            }
}

void SysTimerExecute( void)
// Timer user executive
{
} // SysTimerExecute

#include <ctl.h>

void ctl_handle_error(CTL_ERROR_CODE_t e) {}