//*****************************************************************************
//
//    Hardware.h   
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Bat2Hardware.h"

//------------------------------------------------------------------------------
//   CPU parameters
//------------------------------------------------------------------------------

#define F_USB     48000000ull
#define F_CPU     42000000ull
#define F_SYSTEM  42000000ull
#define F_BUS     42000000ull

//------------------------------------------------------------------------------
//   Board test
//------------------------------------------------------------------------------

#define TEST_MAIN_UART  UART_UART1     // main control UART for board tests
#define UCONIO_UART     TEST_MAIN_UART // UART conio destination

//------------------------------------------------------------------------------
//   System parameters
//------------------------------------------------------------------------------

//#define WATCHDOG_PERIOD   2000         // Watchdog period [ms]
#define TIMER_PERIOD      1            // System timer period [ms]
#define TIMER_FAST_PERIOD 50           // System timer 'fast' tick period [ms]
#define TIMER_SLOW_PERIOD 500          // System timer 'slow' tick period [ms]
#define TIMER_FLASH1      300          // Flash 1 delay [ms]
#define TIMER_FLASH2      200          // Flash 2 delay [ms]
#define TIMER_TIMEOUT     180          // Inactivity timeout [s]

//#define WATCHDOG_ENABLE 1              // Enable watchdog
//#define SLEEP_ENABLE    1              // Enable power save

#define TIMER_SOUND       1            // Enable sound trigger
#define TIMER_KBD         1            // Enable keyboard trigger
#define TIMER_NO_TIMEOUT  1            // Disable inactivity timeout processing

// Interrupt priority :
#define SYS_TIMER_PRIORITY   10         // System timer IRQ
#define UART0_PRIORITY       11         // UART0 IRQ
#define UART1_PRIORITY       12         // UART1 IRQ
#define UART2_PRIORITY       13         // UART1 IRQ
#define UART3_PRIORITY       14         // UART1 IRQ

//-----------------------------------------------------------------------------
// UART common
//-----------------------------------------------------------------------------

// conditional compilation :
#define UART_BINARY  1                      // enable binary mode
#define UART_ASCII   1                      // enable ASCII mode
#define UART_NATIVE  1                      // enable native mode

//-----------------------------------------------------------------------------
// ADC filtering
//-----------------------------------------------------------------------------

#define FILTER_MAX_AVERAGING  50       // max. width of averaging window
#define FILTER_SLOW_RESTART    1       // slow stabilisation after restart

// Basic data types :
typedef byte TSamplesCount;            // samples counter

//-----------------------------------------------------------------------------
// Samples FIFO
//-----------------------------------------------------------------------------

#define FIFO_SIZE       1024            // samples FIFO capacity

//-----------------------------------------------------------------------------
// Diagnostic
//-----------------------------------------------------------------------------

#define DIAGNOSTIC_FRAME_COUNT  10    // Frame FIFO capacity


#define __MENU_WEIGHING__
//-----------------------------------------------------------------------------
// Sleep
//-----------------------------------------------------------------------------

#define WAKE_UP_PERIOD        1 // seconds

#define ADC_WAKEUP_PIN        ADC_DOUT_RDY_PIN

// LLWU pin 4 - PTA13
#define LlwuAdcInit()              LLWU->PE2 = LLWU_PE2_WUPE4(2)
#define LlwuKeyboardInit()         LLWU->PE1 = LLWU_PE1_WUPE2(2) // falling edge
#define LlwuKeyboardForDeepSleepInit() LLWU->PE1 = LLWU_PE1_WUPE2(2)
#define LlwuPowerConnectInit()     LLWU->PE4 = LLWU_PE4_WUPE14(2) | LLWU_PE4_WUPE15(2)
#define LlwuKeyboardFlag()        (LLWU->F1 & (LLWU_F1_WUF0_MASK | LLWU_F1_WUF1_MASK | LLWU_F1_WUF2_MASK))
#define LlwuAdcFlag()             (LLWU->F1 & LLWU_F1_WUF4_MASK)
#define LlwuPowerConnectFlag()    (LLWU->F2 & (LLWU_F2_WUF14_MASK | LLWU_F2_WUF15_MASK))
#define LlwuKeyboardFlagClear()   (LLWU->F1 = LLWU_F1_WUF0_MASK | LLWU_F1_WUF1_MASK | LLWU_F1_WUF2_MASK)
#define LlwuAdcFlagClear()        (LLWU->F1 = LLWU_F1_WUF4_MASK)
#define LlwuPowerConnectFlagClear()  (LLWU->F2 = LLWU_F2_WUF14_MASK | LLWU_F2_WUF15_MASK)

//-----------------------------------------------------------------------------
// Platform Display
//-----------------------------------------------------------------------------

// chicken icon :
#define DisplayWeighingOn()
#define DisplayWeighingOff()

// accumulator icon :
#define DisplayChargerOn()
#define DisplayChargerOff()

// antenna icon :
#define DisplaySignalOn()
#define DisplaySignalOff()

// load icon :
#define DisplayCalibrationOn()
#define DisplayCalibrationOff()

// all icon off :
#define DisplayModeOff()

// red LED :
#define DisplayErrorOn()
#define DisplayErrorOff()

// green LED :
#define DisplayOkOn()
#define DisplayOkOff()

// blue LED :
#define DisplayCommunicationOn()
#define DisplayCommunicationOff()

#define DisplayStatusOff()

//-----------------------------------------------------------------------------

#include "DisplayLayout.h"
#include "Country/Country.h"

//-----------------------------------------------------------------------------

//------------------------------------------------------------------------------
//   USB
//------------------------------------------------------------------------------

#define USB_PLL                  0
#define USB_CLOCK                F_PLL0
#define USB_INTERRUPT_NUMBER     USB0_IRQn

//------------------------------------------------------------------------------
//   One wire
//------------------------------------------------------------------------------

#define ONE_WIRE_PIN          KINETIS_PIN_PORTC11

#define OneWirePortInit()      PinFunction(ONE_WIRE_PIN, KINETIS_GPIO_FUNCTION); \
                               GpioClr( ONE_WIRE_PIN)
                  
#define OneWireSetOutput( Channel)  GpioOutput( ONE_WIRE_PIN)

#define OneWireSetInput( Channel)   GpioInput( ONE_WIRE_PIN)

#define OneWireRead( Channel)       GpioGet( ONE_WIRE_PIN)
//------------------------------------------------------------------------------
//   DS2788
//------------------------------------------------------------------------------

#define DS2788_ONE_WIRE_CHANNEL   0

#endif
