//*****************************************************************************
//
//    Hardware.h   Default hardware definitions
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Bat2Hardware.h"

//------------------------------------------------------------------------------
//   CPU parameters
//------------------------------------------------------------------------------

#include "Clock/Pll.h"

static inline void ClockInit( void) {
	SIM->CLKDIV1 = ( 0
			| SIM_CLKDIV1_OUTDIV1(0)
			| SIM_CLKDIV1_OUTDIV2(0)
			| SIM_CLKDIV1_OUTDIV3(1)
			| SIM_CLKDIV1_OUTDIV4(1));

   // p�i MCG_C4_DRST_DRS == 3 za chv�li hod� hardfault, ostatn� volby jdou
   MCG->C4 &= ~MCG_C4_DRST_DRS_MASK;
   MCG->C4 |= MCG_C4_DRST_DRS(1);
   while((MCG->C4 & MCG_C4_DRST_DRS_MASK) != MCG_C4_DRST_DRS(1));



	/* Initialize PLL0 */
	/* PLL0 will be the source for MCG CLKOUT so the core, system, FlexBus, and flash clocks are derived from it */ 
	pll_init(OSCINIT,   /* Initialize the oscillator circuit */
			12000000,  /* CLKIN0 frequency */
			LOW_POWER,     /* Set the oscillator for low power mode */
			CRYSTAL,     /* Crystal or canned oscillator clock input */
			6,    /* PLL predivider value */
			24,     /* PLL multiplier */
			PLL_ONLY);       /* Use the output from this PLL as the MCGOUT */
}

#define F_USB     48000000ull
#define F_CPU     42000000ull
#define F_SYSTEM  42000000ull
#define F_BUS     42000000ull

static inline void UsbFsClockStart() {

}

static inline void UsbFsClockStop() {

}

//-----------------------------------------------------------------------------
// System timer
//-----------------------------------------------------------------------------

#define TIMER_PERIOD       1                    // System timer period [ms]
#define TIMER_PERIOD_SLEEP 1000                    // System timer period [ms]
#define TIMER_FAST_PERIOD 50                     // System timer 'fast' tick period [ms]
#define TIMER_SLOW_PERIOD 500                    // System timer 'slow' tick period [ms]
#define TIMER_FLASH1      300                    // Flash 1 delay [ms]
#define TIMER_FLASH2      200                    // Flash 2 delay [ms]
#define TIMER_TIMEOUT     10                     // Inactivity timeout [s]

//-----------------------------------------------------------------------------
// Interrupt priority
//-----------------------------------------------------------------------------

#define UART0_PRIORITY       3
#define UART1_PRIORITY       3
#define UART2_PRIORITY       3
#define UART3_PRIORITY       3

//-----------------------------------------------------------------------------
// Keyboard / Events
//-----------------------------------------------------------------------------

#define _K_SYSTEM_BASE   0

#include "System/SystemKey.h"

//-----------------------------------------------------------------------------
// UART common
//-----------------------------------------------------------------------------

// conditional compilation :
#define UART_BINARY  1                      // enable binary mode
#define UART_ASCII   1                      // enable ASCII mode
#define UART_NATIVE  1                      // enable native mode

//-----------------------------------------------------------------------------
// UART0
//-----------------------------------------------------------------------------

#define UART0_RTS_PORT       0
#define UART0_CTS_PORT       0

#define Uart0PortInit()       PinFunction( KINETIS_PIN_UART0_RX_PORTD06, KINETIS_PIN_UART0_RX_PORTD06_FUNCTION);\
                              PinFunction( KINETIS_PIN_UART0_TX_PORTD07, KINETIS_PIN_UART0_TX_PORTD07_FUNCTION)

#define UART0_BAUD           9600           // baud rate
#define UART0_FORMAT         UART_8BIT      // default format
#define UART0_TIMEOUT        10             // intercharacter timeout [ms]

#define UART0_TX_ENABLE_REGISTER   &GpioRegisterSet( UART0_RTS_PORT)
#define UART0_TX_DISABLE_REGISTER  &GpioRegisterClr( UART0_RTS_PORT)
#define UART0_TX_ENABLE_MASK       GpioMask( UART0_RTS_PORT)

//-----------------------------------------------------------------------------
// UART1
//-----------------------------------------------------------------------------

#define UART1_RTS_PORT       0
#define UART1_CTS_PORT       0

#define Uart1PortInit()

#define UART1_BAUD           9600           // baud rate
#define UART1_FORMAT         UART_8BIT      // default format
#define UART1_TIMEOUT        10             // intercharacter timeout [ms]
#define UART1_REPLY_TIMEOUT  3000           // reply timeout [ms]

#define UART1_TX_ENABLE_REGISTER     &GpioRegisterSet( UART2_RTS_PORT)
#define UART1_TX_DISABLE_REGISTER    &GpioRegisterClr( UART2_RTS_PORT)
#define UART1_TX_ENABLE_MASK         GpioMask( UART2_RTS_PORT)

//-----------------------------------------------------------------------------
// UART2
//-----------------------------------------------------------------------------

#define UART2_RTS_PORT       KINETIS_PIN_UART0_RTS_b_PORTD04

#define Uart2PortInit()       PinFunction( KINETIS_PIN_UART2_RX_PORTD02, KINETIS_PIN_UART2_RX_PORTD02_FUNCTION);\
                              PinFunction( KINETIS_PIN_UART2_TX_PORTD03, KINETIS_PIN_UART2_TX_PORTD03_FUNCTION);\
                              PinFunction( UART2_RTS_PORT, KINETIS_GPIO_FUNCTION)

#define UART2_BAUD           9600           // baud rate
#define UART2_FORMAT         UART_8BIT      // default format
#define UART2_TIMEOUT        10             // intercharacter timeout [ms]

#define UART2_TX_ENABLE_REGISTER   &GpioRegisterSet( UART2_RTS_PORT)
#define UART2_TX_DISABLE_REGISTER  &GpioRegisterClr( UART2_RTS_PORT)
#define UART2_TX_ENABLE_MASK       GpioMask( UART2_RTS_PORT)

//-----------------------------------------------------------------------------
// UART3
//-----------------------------------------------------------------------------

#define Uart3PortInit()      

#define UART3_BAUD           9600           // baud rate
#define UART3_FORMAT         UART_8BIT      // default format
#define UART3_TIMEOUT        10             // intercharacter timeout [ms]

//-----------------------------------------------------------------------------
// Console
//-----------------------------------------------------------------------------

#include "Uart/Uart.h"
#define CONSOLE_CHANNEL    UART_UART2
#define CONSOLE_BAUD       115200

//-----------------------------------------------------------------------------
// LED
//-----------------------------------------------------------------------------

#define LED_ORANGE         KINETIS_PIN_PORTA11
#define LedOrangeInit()    PinFunction(LED_ORANGE, KINETIS_GPIO_FUNCTION); GpioOutput(LED_ORANGE)
#define LedOrangeOn()      GpioClr(LED_ORANGE)
#define LedOrangeToggle()  GpioToggle(LED_ORANGE)
#define LedOrangeOff()     GpioSet(LED_ORANGE)

#define LED_YELLOW         KINETIS_PIN_PORTA28
#define LedYellowInit()    PinFunction(LED_YELLOW, KINETIS_GPIO_FUNCTION); GpioOutput(LED_YELLOW)
#define LedYellowOn()      GpioClr(LED_YELLOW)
#define LedYellowToggle()  GpioToggle(LED_YELLOW)
#define LedYellowOff()     GpioSet(LED_YELLOW)

#define LED_GREEN          KINETIS_PIN_PORTA29
#define LedGreenInit()     PinFunction(LED_GREEN, KINETIS_GPIO_FUNCTION); GpioOutput(LED_GREEN)
#define LedGreenOn()       GpioClr(LED_GREEN)
#define LedGreenToggle()   GpioToggle(LED_GREEN)
#define LedGreenOff()      GpioSet(LED_GREEN)

#define LED_BLUE           KINETIS_PIN_PORTA10
#define LedBlueInit()      PinFunction(LED_BLUE, KINETIS_GPIO_FUNCTION); GpioOutput(LED_BLUE)
#define LedBlueOn()        GpioClr(LED_BLUE)
#define LedBlueToggle()    GpioToggle(LED_BLUE)
#define LedBlueOff()       GpioSet(LED_BLUE)

//------------------------------------------------------------------------------
//   USB
//------------------------------------------------------------------------------

#define USB_PLL                  0
#define USB_CLOCK                F_PLL0
#define USB_INTERRUPT_NUMBER     USB0_IRQn

#endif