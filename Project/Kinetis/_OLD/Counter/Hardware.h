//*****************************************************************************
//
//    Hardware.h   
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "Cpu/uMK20DN512VLL10.h"
#include "Cpu/Io.h"
#include "Unisys/Uni.h"

//------------------------------------------------------------------------------
//   CPU parameters
//------------------------------------------------------------------------------

#define F_USB     48000000ull
#define F_CPU     42000000ull
#define F_SYSTEM  42000000ull
#define F_BUS     42000000ull

//------------------------------------------------------------------------------
//   Board test
//------------------------------------------------------------------------------

#define TEST_MAIN_UART  UART_UART1     // main control UART for board tests
#define UCONIO_UART     TEST_MAIN_UART // UART conio destination

//------------------------------------------------------------------------------
//   System parameters
//------------------------------------------------------------------------------

#define WATCHDOG_PERIOD   4000         // Watchdog period [ms]
//#define WATCHDOG_ENABLE
#define TIMER_PERIOD      1            // System timer period [ms]
#define TIMER_FAST_PERIOD 50           // System timer 'fast' tick period [ms]
#define TIMER_SLOW_PERIOD 500          // System timer 'slow' tick period [ms]
#define TIMER_FLASH1      300          // Flash 1 delay [ms]
#define TIMER_FLASH2      200          // Flash 2 delay [ms]
#define TIMER_TIMEOUT     25           // Inactivity timeout [s]

#define TIMER_SOUND       1            // Enable sound trigger
#define TIMER_KBD         1            // Enable keyboard trigger
#define TIMER_NO_TIMEOUT  1            // Disable inactivity timeout processing

// Interrupt priority :
#define SYS_TIMER_PRIORITY   10         // System timer IRQ
#define UART0_PRIORITY       1         // UART0 IRQ
#define UART1_PRIORITY       2         // UART1 IRQ
#define UART2_PRIORITY       3         // UART1 IRQ
#define UART3_PRIORITY       4         // UART1 IRQ

// keyboard/event codes :
#define _K_SYSTEM_BASE            1    // skip K_NULL
#include "System/SystemKey.h"          // system events

//------------------------------------------------------------------------------
// Display controller ST7529
//------------------------------------------------------------------------------

// pins :
#define GPU_DATA_OFFSET  KINETIS_PIN_PORTC00   // LSB bus D0..D7
#define GPU_CS           KINETIS_PIN_PORTC13   // /CS
#define GPU_A0           KINETIS_PIN_PORTC12   //  A0/RS
#define GPU_WR           KINETIS_PIN_PORTC09   // /WR
#define GPU_RES          KINETIS_PIN_PORTC08   // /RES  08

#define GpuPortInit()   PinFunction(GPU_CS, KINETIS_GPIO_FUNCTION); \
                        PinFunction(GPU_A0, KINETIS_GPIO_FUNCTION); \
                        PinFunction(GPU_WR, KINETIS_GPIO_FUNCTION); \
                        PinFunction(GPU_RES, KINETIS_GPIO_FUNCTION); \
                        GpioOutput( GPU_CS); GpioOutput( GPU_A0); \
                        GpioOutput( GPU_WR); GpioOutput( GPU_RES)

#define GpuClearAll()   GpuResClr(); GpuDeselect(); GpuWrSet(); GpuRsSet()

// chipselect :
#define _GpuSelect()     GpioClr( GPU_CS)
#define _GpuDeselect()   GpioSet( GPU_CS)
// control signals :
#define _GpuResSet()     GpioSet( GPU_RES)
#define _GpuResClr()     GpioClr( GPU_RES)
#define _GpuWrSet()      GpioSet( GPU_WR)
#define _GpuWrClr()      GpioClr( GPU_WR)
#define _GpuRsSet()      GpioSet( GPU_A0)
#define _GpuRsClr()      GpioClr( GPU_A0)

// Settings :
#define GPU_LF                 0x00                   // line cycles
#define GPU_EC_BASE            0x120                  // electronic contrast base
#define GPU_ANASET1            GPU_ANASET1_193
#define GPU_ANASET2            GPU_ANASET2_3K
#define GPU_ANASET3            GPU_ANASET3_12
#define GPU_DATSDR1            GPU_DATSDR1_CI
#define GPU_DATSDR2            GPU_DATSDR2_CLR

#define CPU_COLUMN_OFFSSET     5

// Color palette (31..0) :
#define G_INTENSITY_BLACK      31
#define G_INTENSITY_DARKGRAY   20
#define G_INTENSITY_LIGHTGRAY  10
#define G_INTENSITY_WHITE      0

//------------------------------------------------------------------------------
// Graphics
//------------------------------------------------------------------------------

#define G_WIDTH          240           // display width (X)
#define G_HEIGHT         160           // display height (Y)
#define G_PLANES           2           // color planes count

#define GRAPHIC_CONIO            1     // enable conio.h
#define PRINTF_BUFFER            1     // use bprintf
#define GRAPHIC_LETTER_CENTERING 1     // fixed text centering
#define GRAPHIC_TEXT_INDENTATION 1     // indentation

//-----------------------------------------------------------------------------
// UART common
//-----------------------------------------------------------------------------

// conditional compilation :
#define UART_BINARY  1                      // enable binary mode
#define UART_ASCII   1                      // enable ASCII mode
#define UART_NATIVE  1                      // enable native mode

//-----------------------------------------------------------------------------
// Counter
//-----------------------------------------------------------------------------

#define COUNTER_PIN     KINETIS_PIN_PORTE01

#define CounterPinInit()   PinFunction( COUNTER_PIN, KINETIS_GPIO_FUNCTION); \
                           PinPullup( COUNTER_PIN); \
                           GpioInput( COUNTER_PIN); \
                           EintEnable( COUNTER_PIN, EINT_SENSE_FALLING_EDGE)

#define CounterClearInt()  EintClearFlag( COUNTER_PIN)

#define CounterInt()       EintGetFlag( COUNTER_PIN)

#define RESET_PIN     KINETIS_PIN_PORTE02

#define ResetInit()        PinFunction( RESET_PIN, KINETIS_GPIO_FUNCTION); \
                           PinPullup( RESET_PIN); \
                           GpioInput( RESET_PIN); \

#define Reset()            !GpioGet( RESET_PIN)

#endif
