#include "Hardware.h"
#include "System/System.h"
#include "Cpu/Cpu.h"
#include "Graphic/Graphic.h"      // Graphic display
#include "Console/conio.h"
#include "Fonts.h"
#include "Uart/Uart.h"            // UART services

int main(void)
{
int Events = 0;
   CpuInit();
   SysInit();
   GInit();
   GSetFont( TAHOMA16);
   
   CounterPinInit();
   ResetInit();

   forever {
      if(CounterInt()) {
         SysDelay(10);
         CounterClearInt();
         Events++;
         GClear();
         GTextAt(0, 0);
         cprintf("%d", Events);
         GFlush();
      }

      if(Reset()) {
         Events = 0;
         GClear();
         GFlush();
      }

   }
}

//------------------------------------------------------------------------------
//   Timer executive
//------------------------------------------------------------------------------

void SysTimerExecute( void)
// Timer user executive
{
} // SysTimerExecute