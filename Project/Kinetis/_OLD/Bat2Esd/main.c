//*****************************************************************************
//
//    Main.c       Bat2 test main module
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "Cpu/Cpu.h"              // CPU control
#include "System/System.h"        // Operating system
#include "Kbd/Kbd.h"              // Keyboard services
#include "Graphic/Graphic.h"      // Graphic display
#include "Display/Backlight.h"    // Display backlight control
#include "Display/Contrast.h"     // Contrast control
#include "Rtc/Rx8025.h"           // RTC controller
#include "Rtc/RtcTest.h"          // RTC test
#include "Memory/Nvm.h"           // Nonvolatile memory
#include "Uart/Uart.h"            // UART services
#include "Config/Config.h"        // Project configuration
#include "Compact/Power.h"        // Power management
#include "Fonts.h"

#include "Display/BacklightPwm.h"    // Display backlight control
#include "Fram/Fram.h"
#include "Flash/At25dfxx.h"      
#include "File/Efs.h"
#include <string.h>

#define TEST_COUNT   9

#define BACKLIGHT    1

static void TestInit( void);

static void RunTest( void);

static TYesNo FlashReadTest( void);

static TYesNo FlashWriteReadTest( void);

static TYesNo FramReadTest( void);

static TYesNo FramWriteReadTest( void);

static TYesNo EfsTest( void);

static TYesNo AdcTest( void);

static TYesNo KeyboardTest( void);

static TYesNo LcdTest( void);

static TYesNo ClockTest( void);

int TestId = 0;

static int GetRandomNumber( void) {
   return FTM2->CNT;
}

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
   CpuInit();
   SysInit();
   WatchDogInit();                     // start watchdog

   PowerPortInit();

   KbdInit();   
   PowerInit();
   GInit();
   GSetFont( TAHOMA16);
   
   RtcInit();                          // Realtime clock
#ifdef OPTION_BAT2_COMPACT
   AdcInit();
#endif

   BacklightPwmInit();
   BacklightPwmOn(BACKLIGHT);

   GTextAt(0, 0);
   
   TestInit();

   cputs("Reset caused by: ");

   byte ResetCause = RCM->SRS0;

   if(RCM->SRS0 & RCM_SRS0_POR_MASK) {
      cputs("Power on reset");
   } else if(RCM->SRS0 & RCM_SRS0_PIN_MASK) {
      cputs("Pin reset");
   } else if(RCM->SRS0 & RCM_SRS0_WDOG_MASK) {
      cputs("Watchdog");
   } else if(RCM->SRS0 & RCM_SRS0_LOL_MASK) {
      cputs("Loss of lock");
   } else if(RCM->SRS0 & RCM_SRS0_LOC_MASK) {
      cputs("Loss of clock");
   } else if(RCM->SRS0 & RCM_SRS0_LVD_MASK) {
      cputs("Low voltage");
   } else if(RCM->SRS0 & RCM_SRS0_WAKEUP_MASK) {
      cputs("Wake up");
   } else if(RCM->SRS1 & RCM_SRS1_SACKERR_MASK) {
      cputs("Stop mode acknowledge");
   } else if(RCM->SRS1 & RCM_SRS1_EZPT_MASK) {
      cputs("EzPort");
   } else if(RCM->SRS1 & RCM_SRS1_MDM_AP_MASK) {
      cputs("MDM-AP");
   } else if(RCM->SRS1 & RCM_SRS1_SW_MASK) {
      cputs("Software");
   } else if(RCM->SRS1 & RCM_SRS1_LOCKUP_MASK) {
      cputs("Core lockup");
   } else if(RCM->SRS1 & RCM_SRS1_JTAG_MASK) {
      cputs("JTAG");
   } else {
      cputs("Unknown");
   }

   GFlush();
   SysDelay(2000);

   GClear();
   // main menu :
   forever {
      int Key;
   
      CpuIdleMode();
      WatchDog();
      if( PowerFailure()){
         return( K_SHUTDOWN);             // power failure, stop all activity
      }
      Key = SysSchedulerDefault();
      switch( Key){
         case K_TIMER_FAST :
            break;

         case K_TIMER_SLOW :
            break;

         case K_ENTER :
            RunTest();
            break;

         case K_LEFT :
         case K_ESC :
         case K_RIGHT :
            break;

         case K_UP :
            TestId++;
            if(TestId >= TEST_COUNT) {
               TestId = 0;
            }
            break;

         case K_DOWN :
            TestId--;
            if(TestId < 0) {
               TestId = TEST_COUNT - 1;
            }
            break;

         default :
            break;
      }

         GClear();
         GTextAt(0, 0);

         switch(TestId) {
            case 0:
               cputs("FLASH read test");
               break;

            case 1:
               cputs("FLASH write & read test");
               break;

            case 2:
               cputs("FRAM read test");
               break;

            case 3:
               cputs("FRAM write & read test");
               break;

            case 4:
               cputs("Efs test");
               break;

            case 5:
               cputs("Keyboard test");
               break;

            case 6:
               cputs("LCD test");
               break;

            case 7:
               cputs("ADC test");
               break;

            case 8:
               cputs("RTC test");
               break;
         }

      int Now = SysClock();
      UDateTime DateTime;
      uDateTime( &DateTime, Now);

      GTextAt(0, 30);
      cprintf("%02d-%02d-%04d %02d:%02d:%02d\n", (int)DateTime.Date.Day, (int)DateTime.Date.Month, (int)DateTime.Date.Year, (int)DateTime.Time.Hour, (int)DateTime.Time.Min, (int)DateTime.Time.Sec);
      GFlush();

   }
} // main

//------------------------------------------------------------------------------
//   Test
//------------------------------------------------------------------------------

static void TestInit( void) {
   FramInit();
   FlashInit();
   AdcInit();


}

#define PROGRESS_COUNT  4
byte ProgressChar[PROGRESS_COUNT] = {'-', '/', '-', '\\'};
dword LastTime = 0;
int Progress = 0;

static void ShowProgress( int Num) {
   dword Now = SysClock();

   WatchDog();

   if(Now != LastTime) {
      Progress++;
      if(Progress >= PROGRESS_COUNT) {
         Progress = 0;
      }
      GClear();
      GTextAt(0, 0);
      cputch(ProgressChar[Progress]);
      cputch(ProgressChar[Progress]);
      cputch(ProgressChar[Progress]);
      cputch(ProgressChar[Progress]);
      cputch(ProgressChar[Progress]);
      cputch('\n');
      cprintf("%d\n", Num);
      GFlush();
      LastTime = Now;
   }
}

static void RunTest() {
   GClear();
   switch(TestId) {
      case 0: 
         if(FlashReadTest()) {
            cputs("OK");
         } else {
            cputs("Error");
         }
         break;

      case 1: 
         if(FlashWriteReadTest()) {
            cputs("OK");
         } else {
            cputs("Error");
         }
         break;

      case 2: 
         if(FramReadTest()) {
            cputs("OK");
         } else {
            cputs("Error");
         }
         break;

      case 3: 
         if(FramWriteReadTest()) {
            cputs("OK");
         } else {
            cputs("Error");
         }
         break;

      case 4: 
         if(EfsTest()) {
            cputs("OK");
         } else {
            cputs("Error");
         }
         break;

       case 5: 
         if(KeyboardTest()) {
            cputs("OK");
         } else {
            cputs("Error");
         }
         break;

      case 6: 
         if(LcdTest()) {
            cputs("OK");
         } else {
            cputs("Error");
         }
         break;
         
      case 7: 
         if(AdcTest()) {
            cputs("OK");
         } else {
            cputs("Error");
         }
         break;

      case 8: 
         if(ClockTest()) {
            cputs("OK");
         } else {
            cputs("Error");
         }
         break;
   }

   cprintf("\nPress ENTER to continue\n");
   GFlush();
   KbdGet();
   SysDelay(100);
   while(KbdGet() != K_ENTER) {
      WatchDog();
   }

   GFlush();
}

#define MAGIC_OFFSET     2
#define FLASH_TEST_SIZE  (128 * 1024)
#define PATTERN 0x55

static TYesNo FlashReadTest( void) {
dword i, j;
byte  Value;

   WatchDogStop();
   ShowProgress(0);

   cprintf( "Erasing, wait\n");
   GFlush();
   if( !FlashEraseAll()){
      cprintf( "FLASH erase timeout\n");
      return( NO);
   }

   ShowProgress(0);

   cprintf( "Filling with pattern, wait\n");
   GFlush();
   // write pattern :
   FlashFill( 0, PATTERN, FLASH_TEST_SIZE);

   ShowProgress(0);
   WatchDogInit();

   // read pattern :
   forever {
      for( i = 0; i < FLASH_TEST_SIZE; i += FLASH_WRITE_SIZE){
            ShowProgress(i);


         FlashBlockReadStart( i);
         for( j = 0; j < FLASH_WRITE_SIZE; j++){
            Value = FlashBlockReadData();
            if( Value != PATTERN){
               FlashBlockReadStop();
               cprintf( "FLASH error @%04x : %02x (%02x)\n", i, Value, PATTERN);
               SysDelay(100);
               FlashBlockReadStart( i + j);
               Value = FlashBlockReadData();
               FlashBlockReadStop();
               cprintf( "FLASH error @%04x : %02x (%02x)\n", i, Value, PATTERN);
               return( NO);
            }

            if(KbdGet() == K_ESC) {
               cprintf( "ESC pressed, aborted\n");
               return YES;
            }
         }
         FlashBlockReadStop();
      }
   }
   return( YES);
}


#define CHUNK_SIZE   128

static TYesNo FlashWriteReadTest( void) {
dword i, j;
byte  Value;

   forever {
      WatchDogStop();

      ShowProgress(0);
      cprintf( "Erasing, wait\n");
      GFlush();
      if( !FlashEraseAll()){
         cprintf( "FLASH erase timeout\n");
         return( NO);
      }
      WatchDogInit();

      // write pattern :
      for( i = 0; i < FLASH_TEST_SIZE; i += FLASH_WRITE_SIZE){
         if(KbdGet() == K_ESC) {
            cprintf( "ESC pressed, aborted\n");
            return YES;
         }

         ShowProgress(i);

         FlashBlockWriteStart( i);
         for( j = 0; j < FLASH_WRITE_SIZE; j++){
            FlashBlockWriteData( (i + MAGIC_OFFSET) & 0xFF);
         }
         ShowProgress(i);
         FlashBlockWritePerform();
         if( !FlashWaitForReady()){
            cprintf( "FLASH timeout\n");
            return( NO);
         }
      }
      // read pattern :
      for( i = 0; i < FLASH_TEST_SIZE; i += FLASH_WRITE_SIZE){
         FlashBlockReadStart( i);
         for( j = 0; j < FLASH_WRITE_SIZE; j++){
            if(KbdGet() == K_ESC) {
               cprintf( "ESC pressed, aborted\n");
               return YES;
            }

            ShowProgress(FLASH_TEST_SIZE + i); // rozlišení mezi read write

            Value = FlashBlockReadData();
            if( Value != ((i + MAGIC_OFFSET) & 0xFF)){
               FlashBlockReadStop();
               cprintf( "FLASH error @%04x : %02x (%02x)\n", i, Value, PATTERN);
               SysDelay(100);
               FlashBlockReadStart( i + j);
               Value = FlashBlockReadData();
               FlashBlockReadStop();
               cprintf( "FLASH error @%04x : %02x (%02x)\n", i, Value, PATTERN);
               return( NO);
            }
         }
         FlashBlockReadStop();
      }
   }

   return( YES);
}


#define START_ADDRESS 0
#define TEST_SIZE     FRAM_SIZE


static TYesNo FramReadTest( void) {
   int i;
   byte  Value;

   ShowProgress(0);

   WatchDogStop();
   cprintf( "Filling with pattern, wait\n");
   GFlush();
   FramFill( START_ADDRESS, PATTERN, TEST_SIZE);
   WatchDogInit();

   forever {
      FramBlockReadStart( START_ADDRESS);
      for(i = 0 ; i < TEST_SIZE ; i++) {
         ShowProgress(i);
         Value = FramBlockReadData();
         if( Value != PATTERN){
            FramBlockReadStop();
            cprintf( "FRAM error @%04x : %02x (%02x)\n", i, Value, PATTERN);
            SysDelay(100);
            FramBlockReadStart( i);
            Value = FramBlockReadData();
            FramBlockReadStop();
            cprintf( "FRAM error @%04x : %02x (%02x)\n", i, Value, PATTERN);
            return( NO);
         }

         if(KbdGet() == K_ESC) {
            cprintf( "ESC pressed, aborted\n");
            return YES;
         }
      }
      FramBlockReadStop();
   }
   return YES;
}

#define CHUNK_SIZE   128

static TYesNo FramWriteReadTest( void) {
   int Addr;
   int i;
   byte Value;

   forever {
      Addr = START_ADDRESS;

      while((Addr + CHUNK_SIZE < FRAM_SIZE)) {
         // write
         FramPageWriteStart(Addr);
         for(i = 0 ; i < CHUNK_SIZE ; i++) {
            FramPageWriteData(PATTERN);
         }
         FramPageWritePerform();

         ShowProgress(Addr);

         // read
         FramBlockReadStart( Addr);
         for(i = 0 ; i < CHUNK_SIZE ; i++) {
            Value = FramBlockReadData();
            if( Value != PATTERN){
               FramBlockReadStop();
               cprintf( "FRAM error @%04x : %02x (%02x)\n", i + Addr, Value, PATTERN);
               SysDelay(100);
               FramBlockReadStart( i + Addr);
               Value = FramBlockReadData();
               FramBlockReadStop();
               cprintf( "FRAM error @%04x : %02x (%02x)\n", i + Addr, Value, PATTERN);
               return( NO);
            }
         }
         FramBlockReadStop();

         ShowProgress(Addr);

         // erase
         FramPageWriteStart(Addr);
         for(i = 0 ; i < CHUNK_SIZE ; i++) {
            FramPageWriteData(0);
         }
         FramPageWritePerform();

         ShowProgress(Addr);

         // blank check
         i = CHUNK_SIZE;
         FramBlockReadStart( Addr);
         for(i = 0 ; i < CHUNK_SIZE ; i++) {
            Value = FramBlockReadData();
            if( Value != 0){
               FramBlockReadStop();
               cprintf( "FRAM error @%04x : %02x (%02x)\n", i + Addr, Value, 0);
               return( NO);
            }
         }
         FramBlockReadStop();

         Addr += CHUNK_SIZE;

         ShowProgress(Addr);

         if(KbdGet() == K_ESC) {
            cprintf( "ESC pressed, aborted\n");
            return YES;
         }
      }
   }

   return YES;
}

#define FILENAME     "TEST.TXT"
#define DATA         "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

static TYesNo EfsTest( void) {
   int Index = 0;

   WatchDogStop();

   if(!EfsInit()) {
      cputs("No drive detected.\n");
      return NO;
   }


   if(!EfsFileCreate(FILENAME, DATA)) {
      cputs("Error creating file\n");
      EfsSwitchOff();
      return NO;
   }

   ShowProgress(0);

   if(EfsFileWrite(DATA, strlen(DATA)) != strlen(DATA)) {
      cputs("Error writing data\n");
      EfsSwitchOff();
      return NO;
   }

   ShowProgress(0);

   EfsFileClose();

   
   forever {
      if(!EfsFileOpen(FILENAME)) {
         cputs("Unable open file.\n");
         EfsSwitchOff();
         return NO;
      }

      ShowProgress(Index);

      byte Buffer[strlen(DATA)];

      if(EfsFileRead(Buffer, strlen(DATA)) != strlen(DATA)) {
         cputs("File read error.\n");
         EfsSwitchOff();
         return NO;
      }
   
      ShowProgress(Index);

      if(memcmp(Buffer, DATA, strlen(DATA))) {
         cputs("File verification error.\n");
         EfsSwitchOff();
         return NO;
      }

      #define NAME_MAX     50

      byte FileName[NAME_MAX];
      sprintf(FileName, "F%d.txt", Index);

      if(!EfsFileCreate(FileName, 0)) {
         cprintf("Can't create file %d.\n", Index);
         EfsSwitchOff();
         return NO;
      }

      ShowProgress(Index);

#define DATA_SIZE    4096

      int Random = GetRandomNumber();
      int Size = Random % DATA_SIZE;
      byte BufferWrite[DATA_SIZE];
      memset(BufferWrite, Random, Size);

      if(EfsFileWrite(BufferWrite, Size) != Size) {
         cprintf("Error writing file %d.\n", Index);
         EfsSwitchOff();
         return NO;
      }

      ShowProgress(Index);

      EfsFileClose();

      if(!EfsFileOpen(FileName)) {
         cprintf("Can't open file %d.\n", Index);
         EfsSwitchOff();
         return NO;
      }

      ShowProgress(Index);

      byte BufferRead[DATA_SIZE];

      if(EfsFileRead(BufferRead, Size) != Size) {
         cprintf("Can't read file %d.\n", Index);
         EfsSwitchOff();
         return NO;
      }

      ShowProgress(Index);

      if(memcmp(BufferWrite, BufferRead, Size)) {
         cprintf("Verification error file %d.\n", Index);
         EfsSwitchOff();
         return NO;
      }

      EfsFileClose();

      ShowProgress(Index);

      if(KbdGet() == K_ESC) {
         cprintf( "ESC pressed, aborted\n");
         break;
      }

      Index++;
   }

   EfsSwitchOff();
}

static TYesNo KeyboardTest( void) {
   int LastKey;
   int EscPressed = 0;

   forever {
      ShowProgress(0);
      int Key = KbdGet();
      switch(Key) {
         case K_ENTER :
            cputs("ENTER ");
            break;
         case K_LEFT :
            cputs("LEFT ");
            break;
         case K_ESC :
            cputs("ESC ");
            break;
         case K_RIGHT :
            cputs("RIGHT ");
            break;
         case K_UP :
            cputs("UP ");
            break;
         case K_DOWN :
            cputs("DOWN ");
            break;
         default :
            break;
      }
      int KK = Key;
      switch(KK) {
         case K_ENTER:
         case K_LEFT:
         case K_DOWN:
         case K_RIGHT:
         case K_UP:
         case K_ESC:
            if(LastKey == Key) {
               EscPressed++;
               if(EscPressed == 3) {
                  cprintf( "Triple press, aborted\n");
                  return YES;
               }
            } else {
               EscPressed = 0;
            }

            LastKey = Key;
            break;

         default:
            break;
      }
      GFlush();
      
   }

   return YES;
}

static TYesNo LcdTest( void) {
   int i;

#define RECTANGLE_WIDTH 20

   forever {
      for(i = 0 ; i < G_WIDTH - RECTANGLE_WIDTH ; i++) {
         GClear();
         GBox( 0 + i, 0, RECTANGLE_WIDTH, G_HEIGHT - 1);
         GFlush();
         SysDelay(2);
         WatchDog();   
            
         if(KbdGet() == K_ESC) {
            cprintf( "ESC pressed, aborted\n");
            return YES;
         }
      }
   }

   return YES;
}

volatile int AdcValue;
volatile TYesNo NewAdcValue;

void AdcCallback( void) {
   AdcValue = AdcRawRead();
   NewAdcValue = YES;
}

static TYesNo AdcTest( void) {
   AdcStart();
   AdcCallbackEnable(YES);

   forever {
      if(NewAdcValue) {
         GClear();
         GTextAt(0, 0);
         InterruptDisable();
         cprintf("%d\n", AdcValue);
         NewAdcValue = NO;
         InterruptEnable();
         GFlush();
      }

      WatchDog();   
            
      if(KbdGet() == K_ESC) {
         cprintf( "ESC pressed, aborted\n");
         AdcStop();
         return YES;
      }
   }

   return YES;
}

static TYesNo ClockTest( void) {
   forever {
      int Now = SysClock();
      UDateTime DateTime;
      uDateTime( &DateTime, Now);

      GClear();
      GTextAt(0, 0);
      cprintf("%02d-%02d-%04d %02d:%02d:%02d\n", (int)DateTime.Date.Day, (int)DateTime.Date.Month, (int)DateTime.Date.Year, (int)DateTime.Time.Hour, (int)DateTime.Time.Min, (int)DateTime.Time.Sec);
      GFlush();

      WatchDog();   
            
      if(KbdGet() == K_ESC) {
         cprintf( "ESC pressed, aborted\n");
         AdcStop();
         return YES;
      }
   }

   return YES;
}

//------------------------------------------------------------------------------
//   Timer executive
//------------------------------------------------------------------------------

void SysTimerExecute( void)
// Timer user executive
{
   UartTimer();
} // SysTimerExecute




void __irq NMI_Handler(void) {
   GClear();
   GTextAt(0, 0);
   cputs("NMI_Handler");
   GFlush();
   forever;
}

void __irq HardFault_Handler(void) {
   GClear();
   GTextAt(0, 0);
   cputs("HardFault_Handler");
   GFlush();
   forever;
}

void __irq MemManage_Handler(void) {
   GClear();
   GTextAt(0, 0);
   cputs("MemManage_Handler");
   GFlush();
   forever;
}

void __irq BusFault_Handler(void) {
   GClear();
   GTextAt(0, 0);
   cputs("BusFault_Handler");
   GFlush();
   forever;
}

void __irq UsageFault_Handler(void) {
   GClear();
   GTextAt(0, 0);
   cputs("UsageFault_Handler");
   GFlush();
   forever;
}