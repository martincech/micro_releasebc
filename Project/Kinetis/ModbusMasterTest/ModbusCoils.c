//******************************************************************************
//
//   ModbusCoils.c     Modbus process reply - coils commands
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "mb.h"
#include "Modbus/Slave/ModbusReg.h"

eMBErrorCode eMBRegCoilsCB( UCHAR *pucRegBuffer, USHORT usAddress, USHORT usNCoils, eMBRegisterMode eMode)
{
word i;
  //TODO coils in stack
   return MB_ENOREG;

   for( i = 0; i <usNCoils; i++){
      *pucRegBuffer++ = 0x00;                           // currently no coils
   }
   return MB_ENOERR;
}
