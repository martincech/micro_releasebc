#include "Hardware.h"
#include "Cpu/Cpu.h"
#include "System/System.h"
#include "Filter/FilterRelative.h"
#include "Ads1232/Ads1232.h"
#include "Uart/Uart.h"
#include "Sensors/Weight.h"
#include "Weight/WeightDef.h"
#include "mb.h"
#include "IAdc/Iadc.h"
#include "mbutils.h"
#include <string.h>

static int time;
#define WRITE_AR_LEN  3
#define READ_TIME 2000
#define SLAVE_ADDR 1
#define COIL_COUNT 18
static USHORT values[WRITE_AR_LEN];
#define BUFFER_SIZE  128

USHORT Buffer[BUFFER_SIZE];
UCHAR CoilsBuffer[BUFFER_SIZE]= {0x55,0x55,0X6};

void main( void) {
   time = READ_TIME;
   CpuInit();
   SysInit();
   if(eMBInit( 
      MB_RTU_MASTER, 
      1,  
      UART_UART0,
      19200, 
      MB_PAR_EVEN) != MB_ENOERR){

      while(1);
   }
   // Enable the Modbus Protocol Stack.
   eMBEnable(  );

   forever {
      eMBPoll(  );
      // TODO - zm�na bufferu navzorkovan�ch vah pouze zde, aby v pr�b�hu �ten�
      // p�es modbus nedo�lo k posunu bufferu
   }
   eMBDisable();
}

void SysTimerExecute( void) {
   extern void vMBPortTimerExecute( void );
   vMBPortTimerExecute();
   
   if(time == 0){
      for(int i = 0; i < WRITE_AR_LEN; i++){
         values[i] = (values[i] + 1) % (4+i);
      }
//      eMBMReadHoldingRegisters(SLAVE_ADDR, 0, 5);
//      eMBMWriteSingleRegister(SLAVE_ADDR, 0, values[0]);
//      eMBMWriteMultipleRegisters(SLAVE_ADDR, 1, WRITE_AR_LEN,values);
//      eMBMReadWriteMultipleRegisters(SLAVE_ADDR, 1, WRITE_AR_LEN, values, 1, WRITE_AR_LEN);
//      eMBMReadInputRegisters(SLAVE_ADDR, 0, 5);
//      eMBMReadCoils(SLAVE_ADDR, 2, COIL_COUNT);
//      eMBMWriteSingleCoil(SLAVE_ADDR, 1, FALSE);
//      eMBMWriteCoils(SLAVE_ADDR, 5, 3, CoilsBuffer);
      eMBMReadDiscreteInputs(SLAVE_ADDR, 2, COIL_COUNT);
//      eMBMReportSlaveID(SLAVE_ADDR);

      time = READ_TIME;
   }else{
      time--;
   }
}



void eMBMRegHoldingCB(USHORT regs[], USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode)
{   
   if(eMode == MB_REG_READ){
      for(int i = 0; i < usNRegs && i < BUFFER_SIZE; i++){
      
            Buffer[i] = ENDIAN_FROM_BIG_WORD(regs[i]);
      
      }
   } else {
      //writen to slave, confirm

   }
}

void eMBMRegInputCB(USHORT *regs, USHORT usNRegs)
{
   for(int i = 0; i < usNRegs && i < BUFFER_SIZE; i++){
   
         Buffer[i] = ENDIAN_FROM_BIG_WORD(regs[i]);
   
   }
}
void eMBMRegCoilsCB( UCHAR * pucRegBuffer, USHORT usAddress, UCHAR usByteCount, eMBRegisterMode eMode )
{
   if(eMode == MB_REG_READ){
      int n = COIL_COUNT;
      for(int i = 0; i < usByteCount && i < BUFFER_SIZE && n > 0; i++){
            CoilsBuffer[i] = xMBUtilGetBits(&pucRegBuffer[i], 0, n > 8 ? 8 : n);
            n-=8;
      }
   } else {
      //writen to slave, confirm

   }
}

void eMBMRegDiscreteCB( UCHAR * pucRegBuffer, UCHAR usByteCount)
{
   int n = COIL_COUNT;
   for(int i = 0; i < usByteCount && i < BUFFER_SIZE && n > 0; i++){
         CoilsBuffer[i] = xMBUtilGetBits(&pucRegBuffer[i], 0, n > 8 ? 8 : n);
         n-=8;
   }
}

void eMBMSlaveIdCB( UCHAR * pucRegBuffer, UCHAR usByteCount)
{

}

void eMBMExceptionCB(UCHAR ucRcvAddress, UCHAR ucFunctionCode, eMBException status)
{
   memset(values, 0, sizeof(USHORT) * WRITE_AR_LEN);
}

void FilterNextSample( TRawWeight Sample)
// Process next sample
{

}

void AdcCallback( void)
// User implemented callback function
{}

void FilterStop( void)
// User implemented callback function
{}



uint8 TemperatureRead( void) {
 return 0;
}

uint8 HumidityRead( void) {
return 0;
}


#define CO2_IADC_CHANNEL    0
#define CO2_MIN    0
#define CO2_MAX    10000

uint16 CO2Read( void) {
   return 0;
}

void WeightRead(int i, TWeightSample *sample)
// read last ith sample from samples buffer 
{
   sample->Id = 0xFFFF;
}

uint16 GetWeightSamplesRate()
// get samples rate in Hz
{
  return 0;
}
