//******************************************************************************
//
//   ModbusHoldingRegisters.c     Modbus process reply - Holding registers commands
//   Version 1.0                 (c) VEIT Electronics
//
//******************************************************************************

#include "mb.h"
#include "ModbusReg.h"
#include <string.h>

//-----------------------------------------------------------------------------
// Read holding register(s)
//-----------------------------------------------------------------------------

eMBErrorCode eMBRegHoldingCB( USHORT * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode )
{
word i;
word Data;

   switch ( eMode )
   {
      case MB_REG_READ:
         if( !ModbusRegReadIsCorrectNum(usAddress) ||
             !ModbusRegReadIsCorrectNum(usAddress + usNRegs - 1)){
            return MB_ENOREG;
         }
         for( i = 0; i < usNRegs; i++){
            Data = ModbusRegRead(usAddress + i);
            pucRegBuffer[i] = ENDIAN_TO_BIG_WORD(Data);
         }
      break;

      case MB_REG_WRITE:
         if( !ModbusRegWriteIsCorrectNum(usAddress) ||
             !ModbusRegReadIsCorrectNum(usAddress + usNRegs - 1)){
            return MB_ENOREG;
         }

         for( i = 0; i < usNRegs; i++){
            Data = ENDIAN_FROM_BIG_WORD(pucRegBuffer[i]);
            if( !ModbusRegWrite( usAddress + i, Data)){
               return MB_EIO;
            }
         }
      break;
   }

   return MB_ENOERR;
}
