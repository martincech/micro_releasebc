//******************************************************************************
//
//    Platform.c     Platform services
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Platform/Platform.h"
#include "Config/Bat2WireDef.h"
#include <string.h>

TPlatformStatus      PlatformStatus;

TPlatformCommunication       PlatformCommunication;        // unused in compact
TPlatformCommunicationStatus PlatformCommunicationStatus;  // unused in compact

//------------------------------------------------------------------------------
//  Communication
//------------------------------------------------------------------------------

uConstDeclare( TPlatformVersion) pPlatformVersion = {
   /* Device */ {
      /* Class */         0,
      /* Modification */  0,
      /* Software */      BAT2_SW_VERSION,
      /* Hardware */      BAT2_HW_VERSION,
      /* SerialNumber */  0L
   },
   /* WeightMax */        BAT2_WEIGHT_MAX,
   /* SamplesCount */     BAT2_SAMPLES_MAX,
   /* SensorInversion */  0
};

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void PlatformInit( void)
// Initialize platform
{
   memset( &PlatformStatus,              0, sizeof( TPlatformStatus));
} // PlatformInit
