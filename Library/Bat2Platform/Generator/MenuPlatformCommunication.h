//******************************************************************************
//
//   MenuPlatformCommunication.h  Platform communication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformCommunication_H__
   #define __MenuPlatformCommunication_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformCommunication( void);
// Menu platform communication

#endif
