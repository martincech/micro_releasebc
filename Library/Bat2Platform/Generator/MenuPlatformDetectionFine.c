//******************************************************************************
//
//   MenuPlatformDetectionFine.c  Platform detection fine menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformDetectionFine.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform.h"


static DefMenu( PlatformDetectionFineMenu)
   STR_AVERAGING_WINDOW,
   STR_STABLE_WINDOW,
   STR_ABSOLUTE_RANGE,
   STR_SWITCHOVER_RANGE,
EndMenu()

typedef enum {
   MI_AVERAGING_WINDOW,
   MI_STABLE_WINDOW,
   MI_ABSOLUTE_RANGE,
   MI_SWITCHOVER_RANGE
} EPlatformDetectionFineMenu;

// Local functions :

static void PlatformDetectionFineParameters( int Index, int y, TPlatformDetectionFine *Parameters);
// Draw platform detection fine parameters

//------------------------------------------------------------------------------
//  Menu PlatformDetectionFine
//------------------------------------------------------------------------------

void MenuPlatformDetectionFine( void)
// Edit platform detection fine parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_DETECTION_FINE, PlatformDetectionFineMenu, (TMenuItemCb *)PlatformDetectionFineParameters, &PlatformDetectionFine, &MData)){
         ConfigPlatformDetectionFineSave();
         return;
      }
      switch( MData.Item){
         case MI_AVERAGING_WINDOW :
            i = PlatformDetectionFine.AveragingWindow;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_DETECTION_FINE_AVERAGING_WINDOW_MIN, PLATFORM_DETECTION_FINE_AVERAGING_WINDOW_MAX, 0)){
               break;
            }
            PlatformDetectionFine.AveragingWindow = (byte)i;
            break;

         case MI_STABLE_WINDOW :
            i = PlatformDetectionFine.StableWindow;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_DETECTION_FINE_STABLE_WINDOW_MIN, PLATFORM_DETECTION_FINE_STABLE_WINDOW_MAX, 0)){
               break;
            }
            PlatformDetectionFine.StableWindow = (byte)i;
            break;

         case MI_ABSOLUTE_RANGE :
            i = PlatformDetectionFine.AbsoluteRange;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformDetectionFine.AbsoluteRange = (TWeightGauge)i;
            break;

         case MI_SWITCHOVER_RANGE :
            i = PlatformDetectionFine.SwitchoverRange;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformDetectionFine.SwitchoverRange = (TWeightGauge)i;
            break;

      }
   }
} // MenuPlatformDetectionFine

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformDetectionFineParameters( int Index, int y, TPlatformDetectionFine *Parameters)
// Draw platform detection fine parameters
{
   switch( Index){
      case MI_AVERAGING_WINDOW :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->AveragingWindow, 0);
         break;

      case MI_STABLE_WINDOW :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->StableWindow, 0);
         break;

      case MI_ABSOLUTE_RANGE :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->AbsoluteRange);
         break;

      case MI_SWITCHOVER_RANGE :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->SwitchoverRange);
         break;

   }
} // PlatformDetectionFineParameters
