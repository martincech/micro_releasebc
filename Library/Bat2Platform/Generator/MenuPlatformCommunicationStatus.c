//******************************************************************************
//
//   MenuPlatformCommunicationStatus.c  Platform communication status menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformCommunicationStatus.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform.h"


static DefMenu( PlatformCommunicationStatusMenu)
   STR_FRAME_COUNT,
   STR_WRONG_CHARACTER_COUNT,
   STR_WRONG_FRAME_COUNT,
   STR_OTHER_ERRORS,
EndMenu()

typedef enum {
   MI_FRAME_COUNT,
   MI_WRONG_CHARACTER_COUNT,
   MI_WRONG_FRAME_COUNT,
   MI_OTHER_ERRORS
} EPlatformCommunicationStatusMenu;

// Local functions :

static void PlatformCommunicationStatusParameters( int Index, int y, TPlatformCommunicationStatus *Parameters);
// Draw platform communication status parameters

//------------------------------------------------------------------------------
//  Menu PlatformCommunicationStatus
//------------------------------------------------------------------------------

void MenuPlatformCommunicationStatus( void)
// Edit platform communication status parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_COMMUNICATION_STATUS, PlatformCommunicationStatusMenu, (TMenuItemCb *)PlatformCommunicationStatusParameters, &PlatformCommunicationStatus, &MData)){
         ConfigPlatformCommunicationStatusSave();
         return;
      }
      switch( MData.Item){
         case MI_FRAME_COUNT :
            i = PlatformCommunicationStatus.FrameCount;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_COMMUNICATION_STATUS_FRAME_COUNT_MIN, PLATFORM_COMMUNICATION_STATUS_FRAME_COUNT_MAX, 0)){
               break;
            }
            PlatformCommunicationStatus.FrameCount = (word)i;
            break;

         case MI_WRONG_CHARACTER_COUNT :
            i = PlatformCommunicationStatus.WrongCharacterCount;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_COMMUNICATION_STATUS_WRONG_CHARACTER_COUNT_MIN, PLATFORM_COMMUNICATION_STATUS_WRONG_CHARACTER_COUNT_MAX, 0)){
               break;
            }
            PlatformCommunicationStatus.WrongCharacterCount = (word)i;
            break;

         case MI_WRONG_FRAME_COUNT :
            i = PlatformCommunicationStatus.WrongFrameCount;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_COMMUNICATION_STATUS_WRONG_FRAME_COUNT_MIN, PLATFORM_COMMUNICATION_STATUS_WRONG_FRAME_COUNT_MAX, 0)){
               break;
            }
            PlatformCommunicationStatus.WrongFrameCount = (word)i;
            break;

         case MI_OTHER_ERRORS :
            i = PlatformCommunicationStatus.OtherErrors;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_COMMUNICATION_STATUS_OTHER_ERRORS_MIN, PLATFORM_COMMUNICATION_STATUS_OTHER_ERRORS_MAX, 0)){
               break;
            }
            PlatformCommunicationStatus.OtherErrors = (word)i;
            break;

      }
   }
} // MenuPlatformCommunicationStatus

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformCommunicationStatusParameters( int Index, int y, TPlatformCommunicationStatus *Parameters)
// Draw platform communication status parameters
{
   switch( Index){
      case MI_FRAME_COUNT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->FrameCount, 0);
         break;

      case MI_WRONG_CHARACTER_COUNT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->WrongCharacterCount, 0);
         break;

      case MI_WRONG_FRAME_COUNT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->WrongFrameCount, 0);
         break;

      case MI_OTHER_ERRORS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->OtherErrors, 0);
         break;

   }
} // PlatformCommunicationStatusParameters
