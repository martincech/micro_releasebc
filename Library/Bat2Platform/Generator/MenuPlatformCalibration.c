//******************************************************************************
//
//   MenuPlatformCalibration.c  Platform calibration menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuPlatformCalibration.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Platform.h"


static DefMenu( PlatformCalibrationMenu)
   STR_FULL_RANGE,
   STR_DELAY,
   STR_DURATION,
EndMenu()

typedef enum {
   MI_FULL_RANGE,
   MI_DELAY,
   MI_DURATION
} EPlatformCalibrationMenu;

// Local functions :

static void PlatformCalibrationParameters( int Index, int y, TPlatformCalibration *Parameters);
// Draw platform calibration parameters

//------------------------------------------------------------------------------
//  Menu PlatformCalibration
//------------------------------------------------------------------------------

void MenuPlatformCalibration( void)
// Edit platform calibration parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_CALIBRATION, PlatformCalibrationMenu, (TMenuItemCb *)PlatformCalibrationParameters, &PlatformCalibration, &MData)){
         ConfigPlatformCalibrationSave();
         return;
      }
      switch( MData.Item){
         case MI_FULL_RANGE :
            i = PlatformCalibration.FullRange;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            PlatformCalibration.FullRange = (TWeightGauge)i;
            break;

         case MI_DELAY :
            i = PlatformCalibration.Delay;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_CALIBRATION_DELAY_MIN, PLATFORM_CALIBRATION_DELAY_MAX, "ms")){
               break;
            }
            PlatformCalibration.Delay = (word)i;
            break;

         case MI_DURATION :
            i = PlatformCalibration.Duration;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, PLATFORM_CALIBRATION_DURATION_MIN, PLATFORM_CALIBRATION_DURATION_MAX, "ms")){
               break;
            }
            PlatformCalibration.Duration = (word)i;
            break;

      }
   }
} // MenuPlatformCalibration

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void PlatformCalibrationParameters( int Index, int y, TPlatformCalibration *Parameters)
// Draw platform calibration parameters
{
   switch( Index){
      case MI_FULL_RANGE :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Parameters->FullRange);
         break;

      case MI_DELAY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->Delay, "ms");
         break;

      case MI_DURATION :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->Duration, "ms");
         break;

   }
} // PlatformCalibrationParameters
