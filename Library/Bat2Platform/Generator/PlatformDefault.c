TPlatformVersion PlatformVersion;
TPlatformCommunication PlatformCommunication;
TPlatformCommunicationStatus PlatformCommunicationStatus;
TPlatformStatus PlatformStatus;
TPlatformDiagnostics PlatformDiagnostics;
TPlatformPicostrain PlatformPicostrain;
TPlatformSigmaDelta PlatformSigmaDelta;
TPlatformDetection PlatformDetection;
TPlatformDetectionFine PlatformDetectionFine;
TPlatformDetectionCoarse PlatformDetectionCoarse;
TPlatformAcceptance PlatformAcceptance;
TPlatformCalibration PlatformCalibration;

//------------------------------------------------------------------------------
//  Platform version
//------------------------------------------------------------------------------

const TPlatformVersion PlatformVersionDefault = {
   /* Deviceversion */ DeviceVersion?,
   /* WeightMax */ PLATFORM_VERSION_WEIGHT_MAX_DEFAULT,
   /* CalibrationTime */ PLATFORM_VERSION_CALIBRATION_TIME_DEFAULT,
   /* SamplesCount */ PLATFORM_VERSION_SAMPLES_COUNT_DEFAULT,
   /* SensorInversion */ NO,
   /* _Dummy */ 0
};

//------------------------------------------------------------------------------
//  Platform communication
//------------------------------------------------------------------------------

const TPlatformCommunication PlatformCommunicationDefault = {
   /* BaudRate */ PLATFORM_COMMUNICATION_BAUD_RATE_DEFAULT,
   /* BusAddress */ PLATFORM_COMMUNICATION_BUS_ADDRESS_DEFAULT,
   /* Format */ SERIAL_FORMAT_8_BIT_NO_PARITY,
   /* IntercharacterTimeout */ PLATFORM_COMMUNICATION_INTERCHARACTER_TIMEOUT_DEFAULT,
   /* TransmitterDelay */ PLATFORM_COMMUNICATION_TRANSMITTER_DELAY_DEFAULT,
   /* _Dummy */ 0
};

//------------------------------------------------------------------------------
//  Platform communication status
//------------------------------------------------------------------------------

const TPlatformCommunicationStatus PlatformCommunicationStatusDefault = {
   /* FrameCount */ PLATFORM_COMMUNICATION_STATUS_FRAME_COUNT_DEFAULT,
   /* WrongCharacterCount */ PLATFORM_COMMUNICATION_STATUS_WRONG_CHARACTER_COUNT_DEFAULT,
   /* WrongFrameCount */ PLATFORM_COMMUNICATION_STATUS_WRONG_FRAME_COUNT_DEFAULT,
   /* OtherErrors */ PLATFORM_COMMUNICATION_STATUS_OTHER_ERRORS_DEFAULT
};

//------------------------------------------------------------------------------
//  Platform status
//------------------------------------------------------------------------------

const TPlatformStatus PlatformStatusDefault = {
   /* Operation */ PLATFORM_OPERATION_UNDEFINED,
   /* Error */ PLATFORM_ERROR_OK,
   /* Power */ PLATFORM_POWER_OK,
   /* _Dummy1 */ 0,
   /* SamplesCount */ PLATFORM_STATUS_SAMPLES_COUNT_DEFAULT,
   /* _Dummy2 */ PLATFORM_STATUS__DUMMY2_DEFAULT,
   /* Weight */ PLATFORM_STATUS_WEIGHT_DEFAULT
};

//------------------------------------------------------------------------------
//  Platform diagnostics
//------------------------------------------------------------------------------

const TPlatformDiagnostics PlatformDiagnosticsDefault = {
   /* SampleSize */ PLATFORM_DIAGNOSTICS_SAMPLE_SIZE_DEFAULT,
   /* BurstCount */ PLATFORM_DIAGNOSTICS_BURST_COUNT_DEFAULT,
   /* Averaging */ PLATFORM_DIAGNOSTICS_AVERAGING_DEFAULT,
   /* FrameCount */ PLATFORM_DIAGNOSTICS_FRAME_COUNT_DEFAULT
};

//------------------------------------------------------------------------------
//  Platform picostrain
//------------------------------------------------------------------------------

const TPlatformPicostrain PlatformPicostrainDefault = {
   /* Rate */ PLATFORM_PICOSTRAIN_RATE_DEFAULT,
   /* Prefilter */ PLATFORM_PICOSTRAIN_PREFILTER_DEFAULT,
   /* Filter */ PICOSTRAIN_FILTER_SINC5,
   /* Mode */ PICOSTRAIN_MODE_MODE_CONTINUOUS,
   /* AccuracySwitch */ NO,
   /* _Dummy1 */ 0,
   /* AccuracyFine */ PLATFORM_PICOSTRAIN_ACCURACY_FINE_DEFAULT,
   /* AccuracyCoarse */ PLATFORM_PICOSTRAIN_ACCURACY_COARSE_DEFAULT,
   /* _Dummy2 */ 0
};

//------------------------------------------------------------------------------
//  Platform sigma delta
//------------------------------------------------------------------------------

const TPlatformSigmaDelta PlatformSigmaDeltaDefault = {
   /* Rate */ PLATFORM_SIGMA_DELTA_RATE_DEFAULT,
   /* Prefilter */ PLATFORM_SIGMA_DELTA_PREFILTER_DEFAULT,
   /* Filter */ SIGMA_DELTA_FILTER_SINC4,
   /* Chop */ NO,
   /* _Dummy */ 0
};

//------------------------------------------------------------------------------
//  Platform detection
//------------------------------------------------------------------------------

const TPlatformDetection PlatformDetectionDefault = {
   /* Detectiondata */ DetectionData?,
   /* Detectiondata */ DetectionData?
};

//------------------------------------------------------------------------------
//  Platform detection fine
//------------------------------------------------------------------------------

const TPlatformDetectionFine PlatformDetectionFineDefault = {
   /* AveragingWindow */ PLATFORM_DETECTION_FINE_AVERAGING_WINDOW_DEFAULT,
   /* StableWindow */ PLATFORM_DETECTION_FINE_STABLE_WINDOW_DEFAULT,
   /* _Dummy */ 0,
   /* AbsoluteRange */ PLATFORM_DETECTION_FINE_ABSOLUTE_RANGE_DEFAULT,
   /* SwitchoverRange */ PLATFORM_DETECTION_FINE_SWITCHOVER_RANGE_DEFAULT
};

//------------------------------------------------------------------------------
//  Platform detection coarse
//------------------------------------------------------------------------------

const TPlatformDetectionCoarse PlatformDetectionCoarseDefault = {
   /* AveragingWindow */ PLATFORM_DETECTION_COARSE_AVERAGING_WINDOW_DEFAULT,
   /* StableWindow */ PLATFORM_DETECTION_COARSE_STABLE_WINDOW_DEFAULT,
   /* _Dummy */ 0,
   /* AbsoluteRange */ PLATFORM_DETECTION_COARSE_ABSOLUTE_RANGE_DEFAULT,
   /* SwitchoverRange */ PLATFORM_DETECTION_COARSE_SWITCHOVER_RANGE_DEFAULT
};

//------------------------------------------------------------------------------
//  Platform acceptance
//------------------------------------------------------------------------------

const TPlatformAcceptance PlatformAcceptanceDefault = {
   /* Mode */ ACCEPTANCE_MODE_SINGLE,
   /* Sex */ SEX_UNDEFINED,
   /* Step */ ACCEPTANCE_STEP_BOTH,
   /* _Dummy */ 0,
   /* LowLimit */ PLATFORM_ACCEPTANCE_LOW_LIMIT_DEFAULT,
   /* HighLimit */ PLATFORM_ACCEPTANCE_HIGH_LIMIT_DEFAULT,
   /* LowLimitFemale */ PLATFORM_ACCEPTANCE_LOW_LIMIT_FEMALE_DEFAULT,
   /* HighLimitFemale */ PLATFORM_ACCEPTANCE_HIGH_LIMIT_FEMALE_DEFAULT
};

//------------------------------------------------------------------------------
//  Platform calibration
//------------------------------------------------------------------------------

const TPlatformCalibration PlatformCalibrationDefault = {
   /* FullRange */ PLATFORM_CALIBRATION_FULL_RANGE_DEFAULT,
   /* Delay */ PLATFORM_CALIBRATION_DELAY_DEFAULT,
   /* Duration */ PLATFORM_CALIBRATION_DURATION_DEFAULT
};

