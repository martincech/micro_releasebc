//******************************************************************************
//
//   MenuPlatformDetectionFine.h  Platform detection fine menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformDetectionFine_H__
   #define __MenuPlatformDetectionFine_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Platform_H__
   #include "Platform.h"
#endif


void MenuPlatformDetectionFine( void);
// Menu platform detection fine

#endif
