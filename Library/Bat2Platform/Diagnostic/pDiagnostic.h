//******************************************************************************
//
//   pDiagnostic.h   Platform diagnostic
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef __pDiagnostic_H__
   #define __pDiagnostic_H__

#ifndef __DiagnosticDef_H__
   #include "Diagnostic/DiagnosticDef.h"
#endif

void pDiagnosticInit( void);
// Init

void pDiagnosticStart( void);
// Start

void pDiagnosticStop( void);
// Stop

void pDiagnosticBurstCountSet( byte BurstCount);
// Sets <BurstCount> samples per one frame

TYesNo pDiagnosticFrameGet( dword IdIn, dword *IdOut, TDiagnosticFrame *Frame);
// Get frame

#endif
