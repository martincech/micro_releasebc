//******************************************************************************
//
//   Status.c     Device status handling
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Status.h"
#include "System/System.h"        // Operating system
#include "Platform/Platform.h"    // Platform data

typedef enum {
   SIGNAL_OFF,
   SIGNAL_OK_PERMANENT,
   SIGNAL_ERROR_PERMANENT,
   SIGNAL_OK_FLASH,
   SIGNAL_ERROR_FLASH,
   SIGNAL_ALTERNATE,
   SIGNAL_FLASH_BOTH
} ESignal;

static byte _Signal;

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

void StatusInit( void)
// Initialize
{
   _Signal = SIGNAL_OFF;
} // StatusInit

//------------------------------------------------------------------------------
// Executive
//------------------------------------------------------------------------------

void StatusExecute( void)
// Main executive
{
static byte Toggle = 0;

   Toggle = !Toggle;
   switch( _Signal){
      case SIGNAL_OFF :
         DisplayErrorOff();
         DisplayOkOff();
         break;

      case SIGNAL_OK_PERMANENT :
         DisplayErrorOff();
         DisplayOkOn();
         break;

      case SIGNAL_ERROR_PERMANENT :
         DisplayErrorOn();
         DisplayOkOff();
         break;

      case SIGNAL_OK_FLASH :
         DisplayErrorOff();
         if( Toggle){
            DisplayOkOff();
         } else {
            DisplayOkOn();
         }
         break;

      case SIGNAL_ERROR_FLASH :
         DisplayOkOff();
         if( Toggle){
            DisplayErrorOff();
         } else {
            DisplayErrorOn();
         }
         break;

      case SIGNAL_ALTERNATE :
         if( Toggle){
            DisplayOkOff();
            DisplayErrorOn();
         } else {
            DisplayOkOn();
            DisplayErrorOff();
         }
         break;

      case SIGNAL_FLASH_BOTH :
         if( Toggle){
            DisplayOkOn();
            DisplayErrorOn();
         } else {
            DisplayOkOff();
            DisplayErrorOff();
         }
         break;

      default :
         DisplayErrorOff();
         DisplayOkOff();
         break;
   }
} // StatusExecute

//------------------------------------------------------------------------------
// Set error
//------------------------------------------------------------------------------

void StatusErrorSet( byte Error)
// Set <Error> state
{
   PlatformErrorSet( Error);
   DisplayErrorOn();
   _Signal = SIGNAL_ERROR_PERMANENT;
} // StatusErrorSet


//------------------------------------------------------------------------------
// Set operation
//------------------------------------------------------------------------------

void StatusOperationSet( byte Operation)
// Set current <Operation>
{
   DisplayModeOff();                             // all indications off
   PlatformOperationSet( Operation);
   if( PlatformError() != PLATFORM_ERROR_OK){
      if( Operation != PLATFORM_OPERATION_MATCHING){
         return;                                 // error state signalized
      }
      PlatformErrorSet( PLATFORM_ERROR_OK);      // clear error
   }      
   _Signal = SIGNAL_OFF;
   switch( Operation){
      case PLATFORM_OPERATION_STOP :
         // operation stopped
         break;

      case PLATFORM_OPERATION_WEIGHING :
         // weighing active
         DisplayWeighingOn();
         break;

      case PLATFORM_OPERATION_CALIBRATION :
         // calibration in progress
         DisplayCalibrationOn();
         break;

      case PLATFORM_OPERATION_DIAGNOSTICS :
         // ADC diagnostics
         DisplaySignalOn();
         DisplayWeighingOn();
         break;

      case PLATFORM_OPERATION_SLEEP :
         // low power sleep
         break;
		 
      case PLATFORM_OPERATION_MATCHING :
         // platform communication matching
         _Signal = SIGNAL_ALTERNATE;
         break;

      default :
         break;
   }
} // StatusOperationSet

//------------------------------------------------------------------------------
// Display status
//------------------------------------------------------------------------------

void StatusDisplay( byte Status)
// Display temporary status
{
   switch( Status){
      case STATUS_DISPLAY_START :
         // flash at start
         DisplayWeighingOn();
         SysDelay( 1000);
         DisplayWeighingOff();
         SysDelay( 250);
         _Signal = SIGNAL_OFF;
         break;

      case STATUS_DISPLAY_ZERO :                 
         // wait for zero load
         DisplayErrorOn();
         _Signal = SIGNAL_ERROR_FLASH;
         break;

      case STATUS_DISPLAY_ZERO_WEIGHING :
         // wait for zero weighing,
         DisplayErrorOn();
         _Signal = SIGNAL_ERROR_PERMANENT;
         break;

      case STATUS_DISPLAY_LOAD :
         // wait for full range load
         DisplayOkOn();
         _Signal = SIGNAL_OK_FLASH;
         break;

      case STATUS_DISPLAY_LOAD_WEIGHING :
         // wait for full range weighing
         DisplayOkOn();
         _Signal = SIGNAL_OK_PERMANENT;
         break;

      case STATUS_DISPLAY_CALIBRATION_OK :
         // calibration succesfully saved
         DisplayStatusOff();           // all status off
         SysDelay( 500);

         DisplayOkOff();
         DisplayErrorOn();
         SysDelay( 200);
         DisplayOkOn();
         DisplayErrorOff();
         SysDelay( 200);

         DisplayOkOff();
         DisplayErrorOn();
         SysDelay( 200);
         DisplayOkOn();
         DisplayErrorOff();
         SysDelay( 200);

         DisplayStatusOff();           // all status off
         _Signal = SIGNAL_OFF;
         break;

      case STATUS_DISPLAY_CALIBRATION_ERROR :
         // wrong calibration
         DisplayStatusOff();           // all status off
         SysDelay( 500);

         DisplayErrorOn();
         SysDelay( 2000);
         DisplayStatusOff();           // all status off
         _Signal = SIGNAL_OFF;
         break;

      case STATUS_DISPLAY_CALIBRATION_STOP :
         // stop calibration
         DisplayStatusOff();           // all status off
         DisplayModeOff();             // all indications off
         DisplayWeighingOn();
         SysDelay( 500);
         DisplayWeighingOff();
         SysDelay( 500);
         _Signal = SIGNAL_OFF;
         break;

      case STATUS_DISPLAY_OFF :
         // terminate display
         DisplayStatusOff();
         _Signal = SIGNAL_OFF;
         break;

      default :
         break;
   }
} // StatusDisplay
