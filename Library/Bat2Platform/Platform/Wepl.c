//******************************************************************************
//
//   Wepl.c       Weighing platform services
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Platform/Wepl.h"
#include "System/System.h"             // Operating system
#include "Platform/Platform.h"         // Platform data
#include "Weighing/pWeighing.h"         // Weighing utility
#include "Fifo/Fifo.h"                 // Samples FIFO
#include "Diagnostic/pDiagnostic.h"

#ifdef OPTION_SIMULATION
   #define StatusOperationSet( Operation)   PlatformOperationSet( Operation)
#else   
   #include "Config/Configuration.h"      // Serial Number only
   #include "Calibration/Calibration.h"   // Calibration
   #include "Device/Status.h"             // Status display
#endif

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

TYesNo WeplInit( void)
// Initialize
{
   PlatformInit();                     // set device defaults
   return( YES);
} // WeplInit

//------------------------------------------------------------------------------
// Address
//------------------------------------------------------------------------------

void WeplAddressSet( TPlatformAddress Address)
// Set current platform <Address>
{
} // Address

//------------------------------------------------------------------------------
// Version
//------------------------------------------------------------------------------

TYesNo WeplVersionGet( TPlatformVersion *Version)
// Get device version info
{
   uConstCopy( Version, &pPlatformVersion, sizeof( TPlatformVersion));
#if !defined( OPTION_SIMULATION) && defined( OPTION_PICOSTRAIN)
   //Version->Device.SerialNumber = ConfigurationSerialNumber();
#endif   
#if !defined( OPTION_SIMULATION) && defined( OPTION_SIGMA_DELTA)
   Version->CalibrationTime     = CalibrationTime();
   //Version->Device.SerialNumber = ConfigurationSerialNumber();
   Version->SensorInversion     = CalibrationInversion();
#endif
   return( YES);
} // WeplVersionGet

//------------------------------------------------------------------------------
// Status
//------------------------------------------------------------------------------

TYesNo WeplStatusGet( TPlatformStatus *Status)
// Get device status
{
   *Status = PlatformStatus;
   Status->Weight       = WeighingWeight();
   Status->SamplesCount = FifoCount();
   return( YES);
} // WeplStatusGet

//------------------------------------------------------------------------------
// Clock
//------------------------------------------------------------------------------

TYesNo WeplClockSet( TPlatformClock Clock)
// Set device clock
{
#ifndef OPTION_BAT2_COMPACT
   SysClockSet( Clock);
#endif
   return( YES);
} // WeplClockSet

#ifdef OPTION_SIGMA_DELTA
//------------------------------------------------------------------------------
// Sigma/delta
//------------------------------------------------------------------------------

TYesNo WeplSigmaDeltaSet( TPlatformSigmaDelta *SigmaDelta)
// Set sigma-delta configuration
{
   pWeighingStop();
   PlatformSigmaDelta = *SigmaDelta;
   StatusOperationSet( PLATFORM_OPERATION_STOP);
   return( YES);
} // WeplSigmaDeltaSet

#endif // OPTION_SIGMA_DELTA

#ifdef OPTION_PICOSTRAIN
//------------------------------------------------------------------------------
// Picostrain
//------------------------------------------------------------------------------
   
TYesNo WeplPicostrainSet( TPlatformPicostrain *Picostrain)
// Set picostrain configuration
{
   pWeighingStop();
   PlatformPicostrain = *Picostrain;
   StatusOperationSet( PLATFORM_OPERATION_STOP);
   return( YES);
} // WeplPicostrainSet

#endif // OPTION_PICOSTRAIN

//------------------------------------------------------------------------------
// Detection
//------------------------------------------------------------------------------

TYesNo WeplDetectionSet( TPlatformDetection *Detection)
// Set detection parameters
{
   pWeighingStop();
   PlatformDetection = *Detection;
   StatusOperationSet( PLATFORM_OPERATION_STOP);
   return( YES);
} // WeplDetectionSet

//------------------------------------------------------------------------------
// Acceptance
//------------------------------------------------------------------------------

TYesNo WeplAcceptanceSet( TPlatformAcceptance *Acceptance)
// Set acceptance parameters
{
   pWeighingStop();
   PlatformAcceptance = *Acceptance;
   StatusOperationSet( PLATFORM_OPERATION_STOP);
   return( YES);
} // WeplAcceptanceSet

//------------------------------------------------------------------------------
// Calibration
//------------------------------------------------------------------------------

TYesNo WeplCalibrationSet( TPlatformCalibration *Calibration)
// Set calibration parameters
{
   pWeighingStop();
   PlatformCalibration = *Calibration;
   StatusOperationSet( PLATFORM_OPERATION_CALIBRATION);
   return( YES);
} // WeplCalibrationSet

//------------------------------------------------------------------------------
// Weighing start
//------------------------------------------------------------------------------

TYesNo WeplWeighingStart( void)
// Start weighing
{
   pWeighingStop();
   pWeighingStart();
   StatusOperationSet( PLATFORM_OPERATION_WEIGHING);
   return( YES);
} // WeplWeighingStart

//------------------------------------------------------------------------------
// Diagnostics start
//------------------------------------------------------------------------------

TYesNo WeplDiagnosticStart( void)
// Start diagnostics
{
   pWeighingStop();
   WeighingDiagnosticStart();
   StatusOperationSet( PLATFORM_OPERATION_DIAGNOSTICS);
   return( YES);
} // WeplDiagnosticsStart

//------------------------------------------------------------------------------
// Diagnostics get
//------------------------------------------------------------------------------

TYesNo WeplDiagnosticWeightGet( dword IdIn, dword *IdOut, TDiagnosticFrame *Frame)
// Get diagnostic data
{
   return pDiagnosticFrameGet(IdIn, IdOut, Frame);
} // WeplDiagnosticWeightGet

//------------------------------------------------------------------------------
// Stop
//------------------------------------------------------------------------------

TYesNo WeplStop( void)
// Stop weighing/diagnostics
{
   pWeighingStop();
   StatusOperationSet( PLATFORM_OPERATION_STOP);
   return( YES);
} // WeplStop

//------------------------------------------------------------------------------
// Saved weight
//------------------------------------------------------------------------------

TYesNo WeplSavedWeightGet( word *Count, TPlatformWeight *Sample)
// Returns <ActualWeight> and saved <Sample> with <Count>
{
word SamplesCount;
word i;

   // get FIFO samples :
   SamplesCount = FifoCount();         // get total samples count
   if( SamplesCount > PLATFORM_SAVED_WEIGHT_COUNT){
      SamplesCount = PLATFORM_SAVED_WEIGHT_COUNT;
   }
   FifoMark( 0);                       // mark zero items for failure
   *Count = 0;                         // empty list for failure
   for( i = 0; i < SamplesCount; i++){
      if( !FifoGet( i, &Sample[ i])){
         return( NO);                  // FIFO read failure
      }
   }
   *Count = (byte)SamplesCount;        // items count
   FifoMark( SamplesCount);            // mark items for read
   return( YES);
} // WeplSavedWeightGet

//------------------------------------------------------------------------------
// Saved confirm
//------------------------------------------------------------------------------

TYesNo WeplSavedWeightConfirm( word Count)
// Confirm <Count> saved weights
{
   return( FifoRemove( Count));
} // WeplSavedWeightConfirm

//------------------------------------------------------------------------------
//   Communication set
//------------------------------------------------------------------------------

TYesNo WeplCommunicationSet( TPlatformCommunication *Communication)
// Set communication parameters (warning : parameters are set immediately)
{
   pWeighingStop();
   PlatformCommunication = *Communication;
   StatusOperationSet( PLATFORM_OPERATION_STOP);
#ifndef OPTION_SIMULATION
   ConfigSave();
   //!!! wait for send, change UART parameters
#endif
   return( YES);
} // WeplCommunicationSet

//------------------------------------------------------------------------------
//   Communication status
//------------------------------------------------------------------------------

TYesNo WeplCommunicationStatusGet( TPlatformCommunicationStatus *CommunicationStatus)
// Get communication status
{
   *CommunicationStatus = PlatformCommunicationStatus;
   return( YES);
} // WeplCommunicationStatusGet
