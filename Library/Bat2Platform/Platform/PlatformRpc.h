//******************************************************************************
//
//   PlatformRpc.h  Platform remote procedure call
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __PlatformRcp_H__
   #define __PlatformRcp_H__

#ifndef __PlatformDef_H__
   #include "Platform/PlatformDef.h"
#endif

#ifndef __DiagnosticDef_H__
   #include "Diagnostic/DiagnosticDef.h"
#endif

#ifndef __CommonCommandDef_H__
   #include "Device/CommonCommandDef.h"
#endif

#define PLATFORM_SAVED_WEIGHT_COUNT 13      // ZigBee packet size is 84 bytes only

typedef byte TPlatformAddress;              // platform bus address

//------------------------------------------------------------------------------
//  Command code
//------------------------------------------------------------------------------

typedef enum {
   PLATFORM_CMD_UNDEFINED,
   PLATFORM_CMD_VERSION_GET,
   PLATFORM_CMD_STATUS_GET,
   PLATFORM_CMD_CLOCK_SET,
   PLATFORM_CMD_PICOSTRAIN_SET,
   PLATFORM_CMD_SIGMA_DELTA_SET,
   PLATFORM_CMD_DETECTION_SET,
   PLATFORM_CMD_ACCEPTANCE_SET,
   PLATFORM_CMD_CALIBRATION_SET,
   PLATFORM_CMD_WEIGHING_START,
   PLATFORM_CMD_DIAGNOSTIC_START,
   PLATFORM_CMD_DIAGNOSTIC_WEIGHT_GET,
   PLATFORM_CMD_STOP,
   PLATFORM_CMD_SAVED_WEIGHT_GET,
   PLATFORM_CMD_SAVED_WEIGHT_CONFIRM,
   PLATFORM_CMD_COMMUNICATION_SET,
   PLATFORM_CMD_COMMUNICATION_STATUS_GET,
   _PLATFORM_CMD_LAST
} EPlatformCommand;

//------------------------------------------------------------------------------
//  Command data
//------------------------------------------------------------------------------

typedef struct {
   byte Count;
} TPlatformSavedWeightConfirm;

typedef struct {
   dword Id;
} TPlatformDiagnosticWeightGet;

//------------------------------------------------------------------------------
//  Command Data union
//------------------------------------------------------------------------------
   
typedef union {
   TPlatformClock               Clock;
   TPlatformPicostrain          Picostrain;
   TPlatformSigmaDelta          SigmaDelta;
   TPlatformDetection           Detection;
   TPlatformAcceptance          Acceptance;
   TPlatformCalibration         Calibration;
   TPlatformCommunication       Communication;
   TPlatformSavedWeightConfirm  Confirm;
   TPlatformDiagnosticWeightGet Diagnostic;
   TCommonAddressAssign         AddressAssign;
} TPlatformCommandUnion;

//------------------------------------------------------------------------------
//  Command
//------------------------------------------------------------------------------

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

typedef struct {
   byte                  Command;
   TPlatformCommandUnion Data;
} __packed TPlatformCommand;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif

#define PlatformSimpleCommandSize() (1)
#define PlatformCommandSize( Item)  (1 + sizeof( TPlatform##Item))

//******************************************************************************

//------------------------------------------------------------------------------
//  Reply code
//------------------------------------------------------------------------------

// almost same as command :
#define PlatformReply( Command)   ((Command) | 0x80)

//------------------------------------------------------------------------------
//  Reply data
//------------------------------------------------------------------------------

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

// saved weight list :
typedef struct {
   byte            Count;              // saved weights count
   TPlatformWeight Weight[ PLATFORM_SAVED_WEIGHT_COUNT];
} __packed TPlatformSavedWeightGet;

#define PlatformSavedWeightReplySize( Count) (1 + 1 + sizeof( TPlatformWeight) * (Count))

// saved weight list :
typedef struct {
   dword            Id;              // saved weights count
   TDiagnosticFrame Frame;
} __packed TDiagnosticWeightGet;

#define PlatformDiagnosticWeightReplySize( Frame)   (1 + 4 + DiagnosticFrameSize(Frame))

//------------------------------------------------------------------------------
//  Reply Data union
//------------------------------------------------------------------------------

typedef union {
   TPlatformVersion               Version;
   TPlatformStatus                Status;
   TPlatformSavedWeightGet        SavedWeight;
   TPlatformCommunicationStatus   CommunicationStatus;
   TDiagnosticWeightGet           Diagnostic;
   TCommonDiscoverReply           Discover;
} __packed TPlatformReplyUnion;

//------------------------------------------------------------------------------
//  Reply 
//------------------------------------------------------------------------------

typedef struct {
   byte                Reply;
   TPlatformReplyUnion Data;
} __packed TPlatformReply;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif

#define PlatformSimpleReplySize() (1)
#define PlatformReplySize( Item)  (1 + sizeof( TPlatform##Item))

#endif
