//******************************************************************************
//
//   Acceptance.h Weighing acceptance
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Acceptance_H__
   #define __Acceptance_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightFlagDef.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif


TYesNo AcceptWeight( TWeightGauge *ActualWeight, byte *WeightFlags);
// <ActualWeight> acceptance, returns <WeightFlags>, may update <ActualWeight>

#ifdef __cplusplus
   }
#endif

#endif
