//*****************************************************************************
//
//    Rcs.c        Remote control server
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Rcs.h"
#include "System/System.h"             // Operating system
#include "Uart/Uart.h"                 // NATIVE_DATA_SIZE
#include "Uart/NativeDef.h"            // NATIVE_DATA_SIZE
#include "Remote/RcsCommand.h"         // Remote commands processing
#include "Platform/PlatformRpc.h"      // Platform remote calls
#include "Platform/Wepl.h"             // Weighing platform utility
#include "Platform/Platform.h"         // Platform data

//#define RCS_ERROR_TRACE 1            // Trace Rx errors

#define BUFFER_SIZE    NATIVE_DATA_SIZE

static byte _BufferAl[ BUFFER_SIZE + 3] __attribute__ ((aligned (4))) ;

static byte *_Buffer = _BufferAl + 3;
static byte _Context;

typedef enum {
   RCS_START,
   RCS_RECEIVE,
   RCS_SEND,
} ERcsContext;

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void RcsInit( void)
// Initialize
{
   UartInit( RCS_CHANNEL);
   UartSetup( RCS_CHANNEL, RCS_BAUD_RATE, RCS_FORMAT);
   UartModeSet( RCS_CHANNEL, UART_MODE_NATIVE_SLAVE);
   UartTimeoutSet( RCS_CHANNEL, RCS_TIMEOUT, RCS_INTERCHARACTER_TIMEOUT);
   UartSendDelaySet( RCS_CHANNEL, RCS_SEND_DELAY);
   UartSendAddressSet( RCS_CHANNEL, PlatformCommunication.BusAddress);
   UartBufferSet( RCS_CHANNEL, _Buffer, BUFFER_SIZE);
   WeplInit();
   _Context = RCS_START;
} // RcsInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void RcsExecute( void)
// Server executive
{
#ifdef RCS_ERROR_TRACE
   byte Status;
#endif
word ReplySize;
   switch( _Context){
      //-----------------------------------------------------------------------
      case RCS_START :
         UartReceive( RCS_CHANNEL);
         _Context = RCS_RECEIVE;
         break;

      //-----------------------------------------------------------------------
      case RCS_RECEIVE :
         switch( UartReceiveStatus( RCS_CHANNEL)){
            case UART_RECEIVE_ACTIVE :
               // receive in progress               
               break;                  

            case UART_RECEIVE_FRAME :
               // execute command :
               if( !RcsCommand( UartReceiveAddress( RCS_CHANNEL), (TPlatformCommand *)_Buffer, UartReceiveSize( RCS_CHANNEL), 
                                (TPlatformReply *)_Buffer,  &ReplySize)){
                  UartReceive( RCS_CHANNEL);                // no reply, receive again
                  _Context = RCS_RECEIVE;
                  break;
               }
               // send reply :
               UartSend( RCS_CHANNEL, _Buffer, ReplySize);
               _Context = RCS_SEND;
               break;

            default :
               // receive error :
#ifdef RCS_ERROR_TRACE
               Status = UartReceiveStatus( RCS_CHANNEL);
               _Buffer[ 0] = 0xFF;
               _Buffer[ 1] = 0xFF;
               _Buffer[ 2] = Status;
               UartSend( RCS_CHANNEL, _Buffer, 3);
               _Context = RCS_SEND;
               break;
#else			
               UartReceive( RCS_CHANNEL);
               break;
#endif			   
         }
         break;

      //-----------------------------------------------------------------------
      case RCS_SEND :
         switch(UartSendStatus( RCS_CHANNEL)) {
            case UART_SEND_DONE:
               _Context = RCS_RECEIVE;
               break;

            default:
               break;
         }
         break;

      //-----------------------------------------------------------------------
      default :
         _Context = RCS_START;
         break;
   }
} // RcsExecute



void RcsCaEnable( TYesNo Enable) {
   if(Enable) {
      UartSendDelaySet( RCS_CHANNEL, RCS_SEND_DELAY + 55);
   } else {
      UartSendDelaySet( RCS_CHANNEL, RCS_SEND_DELAY + 55);
   }
}