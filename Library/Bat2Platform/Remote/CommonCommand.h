//*****************************************************************************
//
//    CommonCommand.h  Remote control command processing
//    Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __CommonCommand_H__
   #define __CommonCommand_H__

#ifndef __CommonCommandDef_H__
   #include "Device/CommonCommandDef.h"
#endif   

#ifdef __cplusplus
   extern "C" {
#endif

//------------------------------------------------------------------------------
//  Command
//------------------------------------------------------------------------------

void CommonCommandInit( void);
// Initialize

TYesNo CommonCommand( TCommonCommand *Command, word CommandSize, TCommonReply *Reply, word *ReplySize);
// Execute received command

//------------------------------------------------------------------------------

#ifdef __cplusplus
   }
#endif
  
#endif
