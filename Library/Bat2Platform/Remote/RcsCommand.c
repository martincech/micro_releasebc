//*****************************************************************************
//
//    RcsCommand.c  Remote control command processing
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#include "RcsCommand.h"
#include "CommonCommand.h"
#include "PlatformCommand.h"

static byte _Address;

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void RcsCommandInit( void)
// Initialize
{
   CommonCommandInit();
   PlatformCommandInit();
} // RcsCommandInit

//-----------------------------------------------------------------------------
// Command
//-----------------------------------------------------------------------------

TYesNo RcsCommand( byte Address, void *Command, word CommandSize, void *Reply, word *ReplySize)
// Execute received command
{
static TCommonCommand *CommonCmd;
static TCommonReply *CommonReply;
   if(Address != BROADCAST_ADDRESS && Address != _Address) {
      return NO;
   }
   CommonCmd = (TCommonCommand *)Command;
   CommonReply = (TCommonReply *)Reply;
   if(CommonCmd->Command >= _COMMON_CMD_FIRST) {
      if( !CommonCommand( CommonCmd, CommandSize, CommonReply, ReplySize)){
         return NO;
      }
      return YES;
   } else {
      if( !PlatformCommand( (TPlatformCommand *)CommonCmd, CommandSize, (TPlatformReply *)CommonReply, ReplySize)){
         return NO;
      }
   }
   if(Address == BROADCAST_ADDRESS) {
      return NO;
   }
   return YES;
}

//-----------------------------------------------------------------------------
// Assign address
//-----------------------------------------------------------------------------

void RcsAddressAssign( byte Address)
// Communication address assignment
{
   _Address = Address;
} // RcsAddressAssign


