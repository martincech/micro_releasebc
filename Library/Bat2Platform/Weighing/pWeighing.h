//******************************************************************************
//
//   pWeighing.h    Weighing utilities
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __pWeighing_H__
   #define __pWeighing_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

//------------------------------------------------------------------------------
// Basic functions
//------------------------------------------------------------------------------

void pWeighingInit( void);
// Initialize weighing

void pWeighingStart( void);
// Power up start weighing

void pWeighingStop( void);
// Power down stop weighing

void WeighingDiagnosticStart( void);
// Start diagnostics

void pWeighingExecute( void);
// Execute automatic weighing

TRawWeight WeighingRaw( void);
// Get actual raw value

//------------------------------------------------------------------------------
// Display data
//------------------------------------------------------------------------------

TWeightGauge WeighingWeight( void);
// Returns actual weight

//------------------------------------------------------------------------------

#ifdef __cplusplus
   }
#endif

#endif
