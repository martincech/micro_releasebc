//******************************************************************************
//
//   Weighing.c   SigmaDelta weighing utilities
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Weighing/pWeighing.h"
#include "System/System.h"            // Operating system
#include "Calibration/Calibration.h"  // Calibration
#include "Filter/FilterRelative.h"    // Samples filter
#include "Ad7192/Ad7192f.h"           // A/D convertor
#include "Platform/Platform.h"        // Platform data
#include "Fifo/Fifo.h"                // Samples FIFO
#include "Platform/Acceptance.h"      // Acceptance
#include "Diagnostic/pDiagnostic.h"    // Diagnostic utility
#include "Timer/Timer.h"
#include "Multitasking/Multitasking.h"

#define WEIGHING_DRIFT 30             // drift filter (units LSB)

// Local data :
static TWeightGauge _ActualWeight;         // currently measured weight

// Local functions :
static void SetupFilter( void);
// Setup weight filering

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

void pWeighingInit( void)
// Initialize weighing
{
   AdcInit();
   FifoInit();
   _ActualWeight = 0;
} // WeighingInit

//------------------------------------------------------------------------------
// Start
//------------------------------------------------------------------------------

void pWeighingStart( void)
// Power up start weighing
{
   _ActualWeight = 0;
   FifoInit();                         // clear samples FIFO
   SetupFilter();                      // set parameters
   AdcStart();                         // start conversion
} // WeighingStart

//------------------------------------------------------------------------------
// Stop
//------------------------------------------------------------------------------

void pWeighingStop( void)
// Power down stop weighing
{
   AdcStop();
   pDiagnosticStop();                   // stop diagnostic mode
   _ActualWeight = 0;
} // WeighingStop

//------------------------------------------------------------------------------
// Diagnostic
//------------------------------------------------------------------------------

void WeighingDiagnosticStart( void)
// Start diagnostics
{
   _ActualWeight = 0;
   pDiagnosticStart();                 // start diagnostic mode
   SetupFilter();                      // set parameters
   AdcCallbackEnable(YES);
   AdcStart();                         // start conversion
} // WeighingDiagnosticStart

//------------------------------------------------------------------------------
// Execute
//------------------------------------------------------------------------------

void pWeighingExecute( void)
// Execute automatic weighing
{
TRawWeight   RawWeight;
TWeightGauge Weight;
byte         Flags;

   if( PlatformOperation() != PLATFORM_OPERATION_WEIGHING && PlatformOperation() != PLATFORM_OPERATION_DIAGNOSTICS){
      return;
   }
   // set actual value :
   RawWeight     = AdcLowPassRead();
   Weight        = CalibrationWeight( RawWeight);
   _ActualWeight = Weight;
   if( PlatformOperation() != PLATFORM_OPERATION_WEIGHING){
      return;
   }
   // check for stable value :
   if( !AdcRead( &RawWeight)){
      return;                                     // no data (unstable value)
   }
   Weight = CalibrationWeight( RawWeight);  
   if( !AcceptWeight( &Weight, &Flags)){
      return;
   }
   FifoPut( Weight, Flags);
} // WeighingExecute

//------------------------------------------------------------------------------
// Raw weight
//------------------------------------------------------------------------------

TRawWeight WeighingRaw( void)
// Get actual value
{

TYesNo     Inversion;
TRawWeight RawWeight;
word delay = PlatformCalibration.Delay;
int Delay;

   Inversion = FilterRecord.Inversion; // save inversion status
   FilterRecord.Inversion = NO;        // disable ADC data inversion
   AdcStart();
   Delay = SysTimer() + PlatformCalibration.Delay;
   while(!TimerAfter( SysTimer(), Delay)){
      MultitaskingReschedule();
   }
//   SysDelay( PlatformCalibration.Delay);
   AdcAverageRead();                   // clear averaging
   Delay = SysTimer() + PlatformCalibration.Duration;
   while(!TimerAfter( SysTimer(), Delay)){
      MultitaskingReschedule();
   }
//   SysDelay( PlatformCalibration.Duration);
   RawWeight = AdcAverageRead();       // read average
   AdcStop();
   FilterRecord.Inversion = Inversion; // restore inversion status
   return( RawWeight);
} // WeighingRaw

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

TWeightGauge WeighingWeight( void)
// Returns actual weight
{
   return( _ActualWeight);
} // WeighingWeight

//******************************************************************************

//------------------------------------------------------------------------------
// Setup filter
//------------------------------------------------------------------------------

static void SetupFilter( void)
// Setup weight filering
{
TWeightGauge StableRange;

   AdcStop();                          // stop measuring
   FilterRecord.Inversion       = CalibrationInversion();
   FilterRecord.AveragingWindow = PlatformDetection.Fine.AveragingWindow;
   FilterRecord.StableWindow    = PlatformDetection.Fine.StableWindow;
   FilterRecord.ZeroWeight      = CalibrationRawWeight( 0);
   // calculate stable range :
   StableRange  = CalibrationRawWeight( PlatformDetection.Fine.AbsoluteRange);
   StableRange -= CalibrationRawWeight( 0);
   FilterRecord.StableRange     = StableRange;
   FilterRecord.MaxWeight       = CalibrationRawWeight( uConstDwordGet( pPlatformVersion.WeightMax));
   AdcDataRateSet( PlatformSigmaDelta.Rate);
   AdcSinc3Set( PlatformSigmaDelta.Filter == PLATFORM_SIGMA_DELTA_FILTER_SINC3);
   AdcChopSet( PlatformSigmaDelta.Chop);
} // SetupFilter
