//*****************************************************************************
//
//    Kbx.c        Keyboard matrix services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Kbd/Kbx.h"         // Keyboard matrix

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void KbxInit( void)
// Initialization
{
   ButtonInit();
} // KbdInit

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

int KbxRead( void)
// Read keyboard matrix
{
   if( ButtonRedPressed()){
      return( K_KEY_RED);
   }
   if( ButtonBlackPressed()){
      return( K_KEY_BLACK);
   }
   return( K_RELEASED);
} // KbxRead
