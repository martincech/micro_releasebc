//******************************************************************************
//
//   Configuration.h  Configuration utilities
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Configuration_H__
   #define __Configuration_H__

#ifndef __ConfigurationDef_H__
   #include "ConfigurationDef.h"
#endif

TYesNo ConfigLoad( void);
// Load configuration from EEPROM

void ConfigSave( void);
// Save configuration to EEPROM

dword ConfigurationSerialNumber( void);
// Returns device serial number

void ConfigCalibrationSave( void);
// Save calibration

void ConfigCalibrationLoad( void);
// LoadCalibration

#endif
