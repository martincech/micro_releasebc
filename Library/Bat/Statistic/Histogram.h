//*****************************************************************************
//
//    Histogram.h   -  Histogram calculations
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Histogram_H__
   #define __Histogram_H__

#ifndef __StatisticDef_H__
   #include "Statistic/StatisticDef.h"
#endif

void HistogramClear( THistogram *Histogram);
// Clear data

void HistogramAppend( THistogram *Histogram, THistogramValue Value);
// Append <Value>

int HistogramSlot( THistogram *Histogram, THistogramValue Value);
// Find slot index by <Value>

void HistogramCenterSet( THistogram *Histogram, THistogramValue Center);
// Set histogram <Center>

void HistogramRangeSet( THistogram *Histogram, byte Range);
// Set histogram step by <Range> [%]

void HistogramStepSet( THistogram *Histogram, THistogramValue Step);
// Set histogram <Step>

TYesNo HistogramEmpty( THistogram *Histogram);
// Returns YES on empty <Histogram>

THistogramValue HistogramMaximum( THistogram *Histogram);
// Get maximum slot value

int HistogramNormalize( THistogram *Histogram, THistogramValue MaxValue, int Index);
// Returns histogram value normalized by <MaxValue>

THistogramValue HistogramValue( THistogram *Histogram, int Index);
// Returns column value by slot <Index>

#endif
