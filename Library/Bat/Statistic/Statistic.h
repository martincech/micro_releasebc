//*****************************************************************************
//
//    Statistic.h   Statistics utility
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Statistic_H__
   #define __Statistic_H__

#ifndef __StatisticDef_H__
   #include "Statistic/StatisticDef.h"
#endif

//-----------------------------------------------------------------------------

void StatisticClear( TStatisticGauge *Statistic);
// Clear <Statistic>

void StatisticAppend( TStatisticGauge *Statistic, TStatisticNumber Number);
// Append <Number> to <Statistic>

TStatisticCount StatisticCount( TStatisticGauge *Statistic);
// Returns samples count

TStatisticNumber StatisticAverage( TStatisticGauge *Statistic);
// Returns average value

TStatisticNumber StatisticSigma( TStatisticGauge *Statistic);
// Returns standard deviation

TStatisticNumber StatisticVariation( TStatisticNumber Average, TStatisticNumber Sigma);
// Returns variation (percent of Sigma)

//-----------------------------------------------------------------------------

void StatisticUniformityClear( TStatisticGauge *Statistic, TStatisticUniformityRange Range);
// Clear & initialize uniformity with <Range> percent

void StatisticUniformityAppend( TStatisticGauge *Statistic, TStatisticNumber Number);
// Append <Number> to uniformity

TStatisticNumber StatisticUniformity( TStatisticGauge *Statistic);
// Returns uniformity

//-----------------------------------------------------------------------------

#endif
