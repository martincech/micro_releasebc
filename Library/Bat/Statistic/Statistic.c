//*****************************************************************************
//
//    Statistic.c   Statistics utility
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Statistic.h"
#include <math.h>

#define AVERAGE_MIN        1e-03         // approximately zero
#define SIGMA_MIN          1e-03         // approximately zero
#define UNIFORMITY_DEFAULT 100           // default uniformity (error value)

//-----------------------------------------------------------------------------
// Clear
//-----------------------------------------------------------------------------

void StatisticClear( TStatisticGauge *Statistic)
// Clear <Statistic>
{
   Statistic->XSuma    = 0;
   Statistic->X2Suma   = 0;
   Statistic->UniMin   = 0;
   Statistic->UniMax   = 0;
   Statistic->Count    = 0;
   Statistic->CountIn  = 0;
   Statistic->CountOut = 0;
} // StatisticClear

//-----------------------------------------------------------------------------
// Append
//-----------------------------------------------------------------------------

void StatisticAppend( TStatisticGauge *Statistic, TStatisticNumber Number)
// Append <Number> to <Statistic>
{
   Statistic->XSuma  += Number;
   Statistic->X2Suma += Number * Number;
   Statistic->Count++;
} // StatisticAppend

//-----------------------------------------------------------------------------
// Count
//-----------------------------------------------------------------------------

TStatisticCount StatisticCount( TStatisticGauge *Statistic)
// Returns samples count
{
   return( Statistic->Count);
} // StatisticCount

//-----------------------------------------------------------------------------
// Average
//-----------------------------------------------------------------------------

TStatisticNumber StatisticAverage( TStatisticGauge *Statistic)
// Returns average value
{
   if( Statistic->Count == 0){
      return( 0);
   }
   return( Statistic->XSuma / Statistic->Count);
} // StatisticAverage

//-----------------------------------------------------------------------------
// Standard deviation
//-----------------------------------------------------------------------------

TStatisticNumber StatisticSigma( TStatisticGauge *Statistic)
// Returns standard deviation
{
TStatisticNumber Tmp;

   if (Statistic->Count <= 1) {
      return( 0);
   }
   Tmp = Statistic->XSuma * Statistic->XSuma / Statistic->Count;
   Tmp = Statistic->X2Suma - Tmp;
   if( Tmp <= SIGMA_MIN){
      return( 0);
   }
   return( sqrt( 1/(TStatisticNumber)(Statistic->Count - 1) * Tmp));
} // StatisticSigma

//-----------------------------------------------------------------------------
// Variation
//-----------------------------------------------------------------------------

TStatisticNumber StatisticVariation( TStatisticNumber Average, TStatisticNumber Sigma)
// Returns variation (percent of Sigma)
{
   if( Average < AVERAGE_MIN){
      return( 0);         // "deleni nulou"
   }
   return( Sigma / Average * 100);
} // StatisticVariation

//-----------------------------------------------------------------------------
// Uniformity
//-----------------------------------------------------------------------------

void StatisticUniformityClear( TStatisticGauge *Statistic, TStatisticUniformityRange Range)
// Clear & initialize uniformity with <Range> percent
{
TStatisticNumber Min, Max;
TStatisticNumber Average;

   // get average :
   Average = StatisticAverage( Statistic);
   // get absolute range  :
   Min     = Average * (TStatisticNumber)(100 - Range) / 100.0;
   Max     = Average * (TStatisticNumber)(100 + Range) / 100.0;
   // initialize uniformity data :
   Statistic->UniMin   = Min;
   Statistic->UniMax   = Max;
   Statistic->CountIn  = 0;
   Statistic->CountOut = 0;
}  // StatisticUniformityClear

void StatisticUniformityAppend( TStatisticGauge *Statistic, TStatisticNumber Number)
// Append <Number> to uniformity
{
   if( Number < Statistic->UniMin || Number > Statistic->UniMax) {
     Statistic->CountOut++;            // sample out of range
     return;
   }
   Statistic->CountIn++;               // sample in range
} // StatisticUniformityAppend

TStatisticNumber StatisticUniformity( TStatisticGauge *Statistic)
// Returns uniformity
{
TStatisticNumber Uniformity;

   if( Statistic->CountIn == 0 && Statistic->CountOut == 0) {
      return( UNIFORMITY_DEFAULT);     // unable calculate uniformity
   }
   Uniformity = (TStatisticNumber)(100 * Statistic->CountIn) / (TStatisticNumber)(Statistic->CountIn + Statistic->CountOut);
   return( Uniformity);
} // StatisticUniformity
