//******************************************************************************
//
//   FilterRelative.h Relative weight fitering
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef __FilterRelative_H__
   #define __FilterRelative_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

// Filter status :
typedef enum {
   FILTER_STOP,              // weighing disabled
   FILTER_WAIT,              // wait for step
   FILTER_STABLE,            // weighing done
   _FILTER_LAST
} TFilterStatus;

// Data record :
typedef struct {
   byte          Status;               //  filter status
   byte          Ready;                //  weighing done
   byte          Inversion;            //  reverse polarity
   TYesNo        NotFirstStable;       //  first stable flag
   TSamplesCount AveragingWindow;      //* moving average window
   TSamplesCount StableWindow;         //* stability window
   TRawWeight    LowPass;              //  low pass filter value
   TRawWeight    HighPass;             //  high pass filter value
   TRawWeight    RawWeight;            //  filter input
   TRawWeight    ZeroWeight;           //* zero weight - fifo initials
   TWeightGauge  StableRange;          //* absolute stabilisation range
   TRawWeight    MaxWeight;            //* overload value
   TRawWeight    LastStableWeight;     //  last stabilised value
   TRawWeight    Weight;               //  stable value
} TFilterRecord;

// Global data :
extern TFilterRecord FilterRecord;

#ifdef __cplusplus
   extern "C" {
#endif

//******************************************************************************
// Functions
//******************************************************************************

void FilterStop( void);
// Stop filtering

void FilterStart( void);
// Initialize & start filtering

TYesNo FilterRead( TRawWeight *Weight);
// Read filtered value

void FilterNextSample( TRawWeight Sample);
// Process next sample

#ifdef __cplusplus
   }
#endif

#endif
