//******************************************************************************
//
//   Menu$NAME$.c  $DESCRIPTION$
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Menu$NAME$.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

$INCLUDE$

static DefMenu( $NAME$Menu)
$STRINGS$
EndMenu()

typedef enum {
$ENUM$
} E$NAME$Menu;

// Local functions :

static void $DNAME$Parameters( int Index, int y, T$DNAME$ *Parameters);
// Draw $LNAME$ parameters

//------------------------------------------------------------------------------
//  Menu $NAME$
//------------------------------------------------------------------------------

void Menu$NAME$( void)
// Edit $LNAME$ parameters
{
TMenuData MData;
int       i;
$DATA_DECLARATION$

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_$UNAME$, $NAME$Menu, (TMenuItemCb *)$DNAME$Parameters, &$DNAME$, &MData)){
         Config$DNAME$Save();
         return;
      }
      switch( MData.Item){
$CASE$
      }
   }
} // Menu$NAME$

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void $DNAME$Parameters( int Index, int y, T$DNAME$ *Parameters)
// Draw $LNAME$ parameters
{
   switch( Index){
$PARAMETERS$
   }
} // $DNAME$Parameters
