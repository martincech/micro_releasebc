$GETTER$   // $REG_NAME$
   for( i = 0; i < $COUNT$; i++){
     int _ai = ($INDEX3$ + i >> 3) + $INDEX1$;
     int _bi = 1 << ($INDEX4$ - ((i+1) & 7) & 7);
     if( WriteValuesBuffer.$GROUP_CCNAME$Change[ _ai] & _bi){
$LOCAL_GETTER$        $VALUE$ = WriteValuesBuffer.$GROUP_CCNAME$[ i + $INDEX2$];
$LOCAL_SETTER$        WriteValuesBuffer.$GROUP_CCNAME$Change[ _ai] &= ~_bi;
     }
   }
$SETTER$