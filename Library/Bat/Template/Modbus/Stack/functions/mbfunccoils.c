/* 
 * FreeModbus Libary: A portable Modbus implementation for Modbus ASCII/RTU.
 * Copyright (c) 2006 Christian Walter <wolti@sil.at>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * File: $Id: mbfunccoils.c,v 1.8 2007/02/18 23:47:16 wolti Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include "stdlib.h"
#include "string.h"

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbframe.h"
#include "mbproto.h"
#include "mbconfig.h"

/* ----------------------- Defines ------------------------------------------*/
#define MB_PDU_FUNC_READ_ADDR_OFF           ( MB_PDU_DATA_OFF )
#define MB_PDU_FUNC_READ_COILCNT_OFF        ( MB_PDU_DATA_OFF + 2 )
#define MB_PDU_FUNC_READ_SIZE               ( 4 )
#define MB_PDU_FUNC_READ_COILCNT_MAX        ( 0x07D0 )

#define MBM_PDU_FUNC_READ_BYTE_COUNT_OFF    ( MB_PDU_DATA_OFF + 0)
#define MBM_PDU_FUNC_READ_DATA_OFF          ( MBM_PDU_FUNC_READ_BYTE_COUNT_OFF + 1)
#define MBM_PDU_FUNC_READ_SIZE_MIN          ( MB_PDU_SIZE_MIN + 1 )

#define MB_PDU_FUNC_WRITE_ADDR_OFF          ( MB_PDU_DATA_OFF )
#define MB_PDU_FUNC_WRITE_VALUE_OFF         ( MB_PDU_DATA_OFF + 2 )
#define MB_PDU_FUNC_WRITE_SIZE              ( 4 )

#define MB_PDU_FUNC_WRITE_MUL_ADDR_OFF      ( MB_PDU_DATA_OFF )
#define MB_PDU_FUNC_WRITE_MUL_COILCNT_OFF   ( MB_PDU_DATA_OFF + 2 )
#define MB_PDU_FUNC_WRITE_MUL_BYTECNT_OFF   ( MB_PDU_DATA_OFF + 4 )
#define MB_PDU_FUNC_WRITE_MUL_VALUES_OFF    ( MB_PDU_DATA_OFF + 5 )
#define MB_PDU_FUNC_WRITE_MUL_SIZE_MIN      ( 5 )
#define MB_PDU_FUNC_WRITE_MUL_COILCNT_MAX   ( 0x07B0 )
#define MB_PDU_FUNC_WRITE_MUL_COILCNT_MIN   ( 1 )

#define MB_PDU_COIL_ON  0xFF
#define MB_PDU_COIL_OFF 0x00
/* ----------------------- Static functions ---------------------------------*/
eMBException    prveMBError2Exception( eMBErrorCode eErrorCode );

/* ----------------------- Start implementation -----------------------------*/

#if MB_FUNC_READ_COILS_ENABLED > 0

eMBException
eMBFuncReadCoils( UCHAR * pucFrame, USHORT * usLen )
{
    USHORT          usRegAddress;
    USHORT          usCoilCount;
    UCHAR           ucNBytes;
    UCHAR          *pucFrameCur;

    eMBException    eStatus = MB_EX_NONE;
    eMBErrorCode    eRegStatus;

    if( *usLen == ( MB_PDU_FUNC_READ_SIZE + MB_PDU_SIZE_MIN ) )
    {
        usRegAddress = ( USHORT )( pucFrame[MB_PDU_FUNC_READ_ADDR_OFF] << 8 );
        usRegAddress |= ( USHORT )( pucFrame[MB_PDU_FUNC_READ_ADDR_OFF + 1] );
        

        usCoilCount = ( USHORT )( pucFrame[MB_PDU_FUNC_READ_COILCNT_OFF] << 8 );
        usCoilCount |= ( USHORT )( pucFrame[MB_PDU_FUNC_READ_COILCNT_OFF + 1] );

        /* Check if the number of registers to read is valid. If not
         * return Modbus illegal data value exception. 
         */
        if( ( usCoilCount >= 1 ) &&
            ( usCoilCount < MB_PDU_FUNC_READ_COILCNT_MAX ) )
        {
            /* Set the current PDU data pointer to the beginning. */
            pucFrameCur = &pucFrame[MB_PDU_FUNC_OFF];
            *usLen = MB_PDU_FUNC_OFF;

            /* First byte contains the function code. */
            *pucFrameCur++ = MB_FUNC_READ_COILS;
            *usLen += 1;

            /* Test if the quantity of coils is a multiple of 8. If not last
             * byte is only partially field with unused coils set to zero. */
            if( ( usCoilCount & 0x0007 ) != 0 )
            {
                ucNBytes = ( UCHAR )( usCoilCount / 8 + 1 );
            }
            else
            {
                ucNBytes = ( UCHAR )( usCoilCount / 8 );
            }
            *pucFrameCur++ = ucNBytes;
            *usLen += 1;

            eRegStatus =
                eMBRegCoilsCB( pucFrameCur, usRegAddress, usCoilCount,
                               MB_REG_READ );

            /* If an error occured convert it into a Modbus exception. */
            if( eRegStatus != MB_ENOERR )
            {
                eStatus = prveMBError2Exception( eRegStatus );
            }
            else
            {
                /* The response contains the function code, the starting address
                 * and the quantity of registers. We reuse the old values in the 
                 * buffer because they are still valid. */
                *usLen += ucNBytes;
            }
        }
        else
        {
            eStatus = MB_EX_ILLEGAL_DATA_VALUE;
        }
    }
    else
    {
        /* Can't be a valid read coil register request because the length
         * is incorrect. */
        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
    }
    return eStatus;
}

#if MB_MASTER_ENABLED
eMBErrorCode eMBMReadCoils (UCHAR ucSlaveAddress, USHORT usCoilStartAddress, USHORT usNCoils)
{
UCHAR   ucMBFrame[MB_PDU_FUNC_READ_SIZE + 1]; 

   if(usNCoils > MB_PDU_FUNC_READ_COILCNT_MAX || ucSlaveAddress == MB_ADDRESS_BROADCAST){
      return MB_EINVAL;
   }
   ucMBFrame[MB_PDU_FUNC_OFF] = MB_FUNC_READ_COILS;
   ucMBFrame[MB_PDU_FUNC_READ_ADDR_OFF] = (UCHAR)(usCoilStartAddress >> 8);
   ucMBFrame[MB_PDU_FUNC_READ_ADDR_OFF + 1] = (UCHAR) usCoilStartAddress;
   ucMBFrame[MB_PDU_FUNC_READ_COILCNT_OFF] = (UCHAR)(usNCoils >> 8);
   ucMBFrame[MB_PDU_FUNC_READ_COILCNT_OFF+1] = (UCHAR) usNCoils;
   return SendFrame(ucSlaveAddress, ucMBFrame, MB_PDU_FUNC_READ_SIZE + 1);
}

eMBException    eMBMFuncReadCoils( UCHAR * pucFrame, USHORT * usLen )
{
   UCHAR    usByteCount;

   if( *usLen < MBM_PDU_FUNC_READ_SIZE_MIN  )
   {
      return MB_EX_ILLEGAL_DATA_VALUE;
   }
   usByteCount = pucFrame[MBM_PDU_FUNC_READ_BYTE_COUNT_OFF];
   if(*usLen - MBM_PDU_FUNC_READ_SIZE_MIN != usByteCount){
      /* Can't be a valid request because the length is incorrect. */
      return MB_EX_ILLEGAL_DATA_VALUE;
   }     
    /* Make callback to update the value. */        
   eMBMRegCoilsCB(&pucFrame[MBM_PDU_FUNC_READ_DATA_OFF], 0, usByteCount, MB_REG_READ);    
   return MB_ENOERR;
}

#endif

#endif

#if MB_FUNC_WRITE_COIL_ENABLED > 0
eMBException
eMBFuncWriteCoil( UCHAR * pucFrame, USHORT * usLen )
{
    USHORT          usRegAddress;
    UCHAR           ucBuf[2];

    eMBException    eStatus = MB_EX_NONE;
    eMBErrorCode    eRegStatus;

    if( *usLen == ( MB_PDU_FUNC_WRITE_SIZE + MB_PDU_SIZE_MIN ) )
    {
        usRegAddress = ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_ADDR_OFF] << 8 );
        usRegAddress |= ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_ADDR_OFF + 1] );
        

        if( ( pucFrame[MB_PDU_FUNC_WRITE_VALUE_OFF + 1] == MB_PDU_COIL_OFF ) &&
            ( ( pucFrame[MB_PDU_FUNC_WRITE_VALUE_OFF] == MB_PDU_COIL_ON ) ||
              ( pucFrame[MB_PDU_FUNC_WRITE_VALUE_OFF] == MB_PDU_COIL_OFF ) ) )
        {
            ucBuf[1] = 0;
            if( pucFrame[MB_PDU_FUNC_WRITE_VALUE_OFF] == MB_PDU_COIL_ON )
            {
                ucBuf[0] = 1;
            }
            else
            {
                ucBuf[0] = 0;
            }
            eRegStatus =
                eMBRegCoilsCB( &ucBuf[0], usRegAddress, 1, MB_REG_WRITE );

            /* If an error occured convert it into a Modbus exception. */
            if( eRegStatus != MB_ENOERR )
            {
                eStatus = prveMBError2Exception( eRegStatus );
            }
        }
        else
        {
            eStatus = MB_EX_ILLEGAL_DATA_VALUE;
        }
    }
    else
    {
        /* Can't be a valid write coil register request because the length
         * is incorrect. */
        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
    }
    return eStatus;
}

#if MB_MASTER_ENABLED
eMBErrorCode eMBMWriteSingleCoil (UCHAR ucSlaveAddress, USHORT usOutputAddress, BOOL bOn)
{
UCHAR   ucMBFrame[MB_PDU_FUNC_WRITE_SIZE + 1]; 

   ucMBFrame[MB_PDU_FUNC_OFF] = MB_FUNC_WRITE_SINGLE_COIL;
   ucMBFrame[MB_PDU_FUNC_WRITE_ADDR_OFF] = (UCHAR)(usOutputAddress >> 8);
   ucMBFrame[MB_PDU_FUNC_WRITE_ADDR_OFF + 1] = (UCHAR) usOutputAddress;
   ucMBFrame[MB_PDU_FUNC_WRITE_VALUE_OFF] = (UCHAR)(bOn ? MB_PDU_COIL_ON : MB_PDU_COIL_OFF );
   ucMBFrame[MB_PDU_FUNC_WRITE_VALUE_OFF+1] = (UCHAR) 0;
   return SendFrame(ucSlaveAddress, ucMBFrame, MB_PDU_FUNC_WRITE_SIZE + 1);
}

eMBException    eMBMFuncWriteCoil( UCHAR * pucFrame, USHORT * usLen )
{
   USHORT          usOutputAddress;  
   UCHAR          coil;

   if( *usLen != ( MB_PDU_FUNC_WRITE_SIZE + MB_PDU_SIZE_MIN ) )
   {
      return MB_EX_ILLEGAL_DATA_VALUE;
   }
   usOutputAddress = ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_ADDR_OFF] << 8 );
   usOutputAddress |= ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_ADDR_OFF + 1] );
   coil = pucFrame[MB_PDU_FUNC_WRITE_VALUE_OFF] == MB_PDU_COIL_ON? 0x01 : 0x00;
   /* Make callback to update the value. */
   eMBMRegCoilsCB(&coil, usOutputAddress, 1, MB_REG_WRITE);    
   return MB_EX_NONE;
}
#endif
#endif

#if MB_FUNC_WRITE_MULTIPLE_COILS_ENABLED > 0
eMBException
eMBFuncWriteMultipleCoils( UCHAR * pucFrame, USHORT * usLen )
{
    USHORT          usRegAddress;
    USHORT          usCoilCnt;
    UCHAR           ucByteCount;
    UCHAR           ucByteCountVerify;

    eMBException    eStatus = MB_EX_NONE;
    eMBErrorCode    eRegStatus;

    if( *usLen > ( MB_PDU_FUNC_WRITE_SIZE + MB_PDU_SIZE_MIN ) )
    {
        usRegAddress = ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_MUL_ADDR_OFF] << 8 );
        usRegAddress |= ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_MUL_ADDR_OFF + 1] );
        

        usCoilCnt = ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_MUL_COILCNT_OFF] << 8 );
        usCoilCnt |= ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_MUL_COILCNT_OFF + 1] );

        ucByteCount = pucFrame[MB_PDU_FUNC_WRITE_MUL_BYTECNT_OFF];

        /* Compute the number of expected bytes in the request. */
        if( ( usCoilCnt & 0x0007 ) != 0 )
        {
            ucByteCountVerify = ( UCHAR )( usCoilCnt / 8 + 1 );
        }
        else
        {
            ucByteCountVerify = ( UCHAR )( usCoilCnt / 8 );
        }

        if( ( usCoilCnt >= 1 ) &&
            ( usCoilCnt <= MB_PDU_FUNC_WRITE_MUL_COILCNT_MAX ) &&
            ( ucByteCountVerify == ucByteCount ) )
        {
            eRegStatus =
                eMBRegCoilsCB( &pucFrame[MB_PDU_FUNC_WRITE_MUL_VALUES_OFF],
                               usRegAddress, usCoilCnt, MB_REG_WRITE );

            /* If an error occured convert it into a Modbus exception. */
            if( eRegStatus != MB_ENOERR )
            {
                eStatus = prveMBError2Exception( eRegStatus );
            }
            else
            {
                /* The response contains the function code, the starting address
                 * and the quantity of registers. We reuse the old values in the 
                 * buffer because they are still valid. */
                *usLen = MB_PDU_FUNC_WRITE_MUL_BYTECNT_OFF;
            }
        }
        else
        {
            eStatus = MB_EX_ILLEGAL_DATA_VALUE;
        }
    }
    else
    {
        /* Can't be a valid write coil register request because the length
         * is incorrect. */
        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
    }
    return eStatus;
}

#define MB_PDU_FUNC_WRITE_MUL_ADDR_OFF      ( MB_PDU_DATA_OFF )
#define MB_PDU_FUNC_WRITE_MUL_COILCNT_OFF   ( MB_PDU_DATA_OFF + 2 )
#define MB_PDU_FUNC_WRITE_MUL_BYTECNT_OFF   ( MB_PDU_DATA_OFF + 4 )
#define MB_PDU_FUNC_WRITE_MUL_VALUES_OFF    ( MB_PDU_DATA_OFF + 5 )
#define MB_PDU_FUNC_WRITE_MUL_SIZE_MIN      ( 5 )
#define MB_PDU_FUNC_WRITE_MUL_COILCNT_MAX   ( 0x07B0 )

#if MB_MASTER_ENABLED
eMBErrorCode eMBMWriteCoils (UCHAR ucSlaveAddress, USHORT usCoilStartAddress, USHORT usNCoils, const UCHAR arubCoilsIn[])
{
UCHAR byteCount = usNCoils/8 + (usNCoils & 0x7 ? 1 : 0);
int size = MB_PDU_FUNC_WRITE_MUL_SIZE_MIN + 1 + byteCount;
UCHAR   ucMBFrame[size]; 
int i;

   if(usNCoils > MB_PDU_FUNC_WRITE_MUL_COILCNT_MAX || usNCoils < MB_PDU_FUNC_WRITE_MUL_COILCNT_MIN){
      return MB_EINVAL;
   }
   ucMBFrame[MB_PDU_FUNC_OFF] = MB_FUNC_WRITE_MULTIPLE_COILS;
   ucMBFrame[MB_PDU_FUNC_WRITE_MUL_ADDR_OFF] = (UCHAR)(usCoilStartAddress >> 8);
   ucMBFrame[MB_PDU_FUNC_WRITE_MUL_ADDR_OFF + 1] = (UCHAR) usCoilStartAddress;
   ucMBFrame[MB_PDU_FUNC_WRITE_MUL_COILCNT_OFF] = (UCHAR)(usNCoils >> 8);
   ucMBFrame[MB_PDU_FUNC_WRITE_MUL_COILCNT_OFF + 1] = (UCHAR) usNCoils;
   //Quantity of Outputs / 8, if the remainder is different of 0 => N = N+1
   ucMBFrame[MB_PDU_FUNC_WRITE_MUL_BYTECNT_OFF] = (UCHAR) byteCount;
   for(i = 0; i < byteCount; i++){
      ucMBFrame[MB_PDU_FUNC_WRITE_MUL_VALUES_OFF + i] = arubCoilsIn[i];
   }
   return SendFrame(ucSlaveAddress, ucMBFrame, size);
}

eMBException    eMBMFuncWriteMultipleCoils( UCHAR * pucFrame, USHORT * usLen )
{
   USHORT          usRegAddress;
   USHORT          usRegCount;

   if( *usLen < (MB_PDU_FUNC_WRITE_SIZE + MB_PDU_SIZE_MIN ) )
   {
      return MB_EX_ILLEGAL_DATA_VALUE;
   }
   usRegAddress = ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_MUL_ADDR_OFF] << 8 );
   usRegAddress |= ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_MUL_ADDR_OFF + 1] );        

   usRegCount = ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_MUL_COILCNT_OFF] << 8 );
   usRegCount |= ( USHORT )( pucFrame[MB_PDU_FUNC_WRITE_MUL_COILCNT_OFF + 1] );

   eMBMRegCoilsCB(NULL, usRegAddress, usRegCount, MB_REG_WRITE);    

   return MB_EX_NONE;   
}
#endif
#endif
