//******************************************************************************
//
//   $NAME$.c      $DESCRIPTION$
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "$NAME$.h"
$INCLUDE$

$GLOBALS$

// Locals :
$LOCAL_DECLARATIONS$

$FUNCTIONS$

$LOCAL_DEFINITIONS$
