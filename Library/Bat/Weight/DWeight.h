//******************************************************************************
//
//   DWeight.h      Display weight
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DWeight_H__
   #define __DWeight_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#define DWEIGHT_MAX_LENGTH   12      // max. size of weight string with units

void DWeight( TWeightGauge Weight);
// Display weight

void DWeightLeft( TWeightGauge Weight);
// Display weight left aligned

void DWeighingUnits( void);
// Display current units

void DWeightWithUnits( TWeightGauge Weight);
// Display weight with units

void DWeightWithUnitsNarrow( int x, int y, TWeightGauge Weight);
// Display weight with units left aligned

void DWeightWithUnitsNarrowLeft( int x, int y, TWeightGauge Weight);
// Display weight with units left aligned

void DWeightWithUnitsNarrowRight( int x, int y, TWeightGauge Weight);
// Display weight with units right aligned

void DWeightWithUnitsNarrowCentered( int x, int xWidth, int y, int yWidth, TWeightGauge Weight);
// Display weight with units left aligned and centered

void DWeightNarrow( int x, int y, TWeightGauge Weight);
// Display weight with narrow decimal dot

void BWeightWithUnits( char *Buffer, TWeightGauge Weight);
// Format <Weight> with units to <Buffer>

TYesNo DInputWeight( TUniStr Caption, TUniStr Text, TWeightGauge *Weight);
// Input weight

TYesNo DEditWeight( int x, int y, TWeightGauge *Weight);
// Edit weight

TYesNo DInputWeightRange( TUniStr Caption, TUniStr Text, TWeightGauge *Weight, NWeight LoLimit, NWeight HiLimit);
// Input weight

TYesNo DEditWeightRange( int x, int y, TWeightGauge *Weight, NWeight LoLimit, NWeight HiLimit);
// Edit weight

int DWeightWidth( void);
// Returns weight width (pixels)

int DWeightWidthNarrow( void);
// Returns weight width with narrow decimal dot (pixels)

#endif
