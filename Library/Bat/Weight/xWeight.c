//******************************************************************************
//                                                                            
//   xWeight.c      putchar based weight formatting
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "xWeight.h"
#include "Weight/Weight.h"        // Weight internal representation
#include "Console/xprint.h"       // Display
#include "Convert/uBcd.h"         // Display
#include "Str.h"                  // Strings

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

void xWeight( TPutchar *xPutchar, TWeightGauge Weight)
// Display weight
{
int     Width;
NWeight Number;

   Number = WeightNumber( Weight);
   Width = uBinaryWidth( NWEIGHT_MAX);
   if( !WeightUnits.Decimals){
      xprintdec( xPutchar, Number, Width);
      return;
   }
   xfloat( xPutchar, Number, Width + 1, WeightUnits.Decimals, 0);
} // xWeight

//------------------------------------------------------------------------------
// Weight width
//------------------------------------------------------------------------------

int xWeightWidth( void)
// Weight width (chars)
{
   return( uBinaryWidth( NWEIGHT_MAX) + 1); // always with dot
} // xWeightWidth

//------------------------------------------------------------------------------
// Weight left
//------------------------------------------------------------------------------

void xWeightLeft( TPutchar *xPutchar, TWeightGauge Weight)
// Display weight left aligned
{
NWeight Number;

   Number = WeightNumber( Weight);
   if( !WeightUnits.Decimals){
      xprintdec( xPutchar, Number, 0);
      return;
   }
   xfloat( xPutchar, Number, 0, WeightUnits.Decimals, 0);
} // xWeightLeft

//------------------------------------------------------------------------------
// Units
//------------------------------------------------------------------------------

void xWeighingUnits( TPutchar *xPutchar)
// Display current units
{
   xputs( xPutchar, StrGet( ENUM_WEIGHT_UNITS + WeightUnits.Units));
} // xWeighingUnits

//------------------------------------------------------------------------------
// Weight & Units
//------------------------------------------------------------------------------

void xWeightWithUnits( TPutchar *xPutchar, TWeightGauge Weight)
// Display weight with units
{
   xWeight( xPutchar, Weight); 
   (*xPutchar)(' ');
   xWeighingUnits( xPutchar);
} // xWeightWithUnits

//------------------------------------------------------------------------------
// Weight & Units
//------------------------------------------------------------------------------

void xWeightWithUnitsLeft( TPutchar *xPutchar, TWeightGauge Weight)
// Display weight with units left aligned
{
   xWeightLeft( xPutchar, Weight); 
   (*xPutchar)(' ');
   xWeighingUnits( xPutchar);
} // xWeightWithUnitsLeft
