//******************************************************************************
//                                                                            
//   Weight.h       Weight utility
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Weight_H__
   #define __Weight_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

//------------------------------------------------------------------------------

extern       TWeightUnits WeightUnits;          // weight units descriptor
extern const TWeightUnits WeightUnitsG;         // weight units [g]  defaults
extern const TWeightUnits WeightUnitsKg;        // weight units [kg] defaults
extern const TWeightUnits WeightUnitsLb;        // weight units [lb] defaults

#define WeightUnitsDefault WeightUnitsKg

//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif

void WeightInit( void);
// Initialize

void WeightUnitsSet( byte Units);
// Set current <Units>

NWeight WeightNumber( TWeightGauge Weight);
// Returns current units number of <Weight>

NWeight WeightNumberDivision( TWeightGauge Weight);
// Returns current units number of <Weight> rounded to division

TWeightGauge WeightGauge( NWeight Number);
// Returns internal representation of <Number> units

TWeightGauge WeightGaugeDivision( TWeightGauge Weight);
// Returns internal representation rounded to division

TWeightGauge WeightNice( TWeightGauge Weight);
// Returns nice number of <Weight> by current units

#ifdef __cplusplus
   }
#endif
#endif
