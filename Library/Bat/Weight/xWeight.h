//******************************************************************************
//                                                                            
//   xWeight.h      putchar based weight formatting
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __xWeight_H__
   #define __xWeight_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifndef __PutcharDef_H__
   #include "Console/PutcharDef.h"
#endif

//------------------------------------------------------------------------------

void xWeight( TPutchar *xPutchar, TWeightGauge Weight);
// Display weight

int xWeightWidth( void);
// Weight width (chars)

void xWeightLeft( TPutchar *xPutchar, TWeightGauge Weight);
// Display weight left aligned

void xWeighingUnits( TPutchar *xPutchar);
// Display current units

void xWeightWithUnits( TPutchar *xPutchar, TWeightGauge Weight);
// Display weight with units

void xWeightWithUnitsLeft( TPutchar *xPutchar, TWeightGauge Weight);
// Display weight with units left aligned

#endif
