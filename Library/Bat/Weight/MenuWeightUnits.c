//******************************************************************************
//                                                                            
//   MenuWeightUnits.c   Weighing units menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeightUnits.h"
//#include "Graphic/Graphic.h"    // graphic
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/Weight.h"        // Weighing units
#include "Weight/DWeight.h"       // Display weight
#include "Config/Config.h"        // Configuration
#include "Str.h"                  // Strings

static DefMenu( WeightUnitsMenu)
   STR_WEIGHT_UNITS,
   STR_WEIGHT_DIVISION,
   STR_WEIGHT_RANGE,
EndMenu()

typedef enum {
   MI_UNITS,
   MI_DIVISION,
   MI_RANGE
} EWeightUnitsMenuEnum;

// Local functions :

static void WeightUnitsParameters( int Index, int y, void *UserData);
// Menu weighing units parameters

static void _SetUnits( int Units);
// Set weighing units

static int _EnumToCapacity( int Code);
// Decode capacity from <Code>

static int _CapacityToEnum( int Capacity);
// Encode <Capacity> to enum

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuWeightUnits( void)
// Menu weighing units
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_WEIGHING, WeightUnitsMenu, WeightUnitsParameters, 0, &MData)){
         ConfigWeightUnitsSave();
         return;
      }
      switch( MData.Item){
         case MI_UNITS :
            i = WeightUnits.Units;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_WEIGHT_UNITS, _WEIGHT_UNITS_COUNT)){
               break;
            }
            _SetUnits( i);
            break;

         case MI_DIVISION :
            i = WeightUnits.Division;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, WeightUnits.Decimals, WEIGHT_UNITS_DIVISION_MIN,
                              WeightUnits.DivisionMax, ENUM_WEIGHT_UNITS + WeightUnits.Units)){
               break;
            }
            WeightUnits.Division = i;
            break;

         case MI_RANGE :
            i = _CapacityToEnum( WeightUnits.Range);
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_WEIGHT_CAPACITY, _WEIGHT_CAPACITY_COUNT)){
               break;
            }
            WeightUnits.Range = _EnumToCapacity( i);
            break;

      }
   }
} // MenuWeightUnits

//******************************************************************************

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void WeightUnitsParameters( int Index, int y, void *UserData)
// Menu weighing units parameters
{
   switch( Index){
      case MI_UNITS :
         DLabelEnum( WeightUnits.Units, ENUM_WEIGHT_UNITS, DMENU_PARAMETERS_X, y);
         break;

      case MI_DIVISION :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, WeightGauge( WeightUnits.Division));
         break;

      case MI_RANGE :
         DLabelEnum( _CapacityToEnum( WeightUnits.Range), ENUM_WEIGHT_CAPACITY, DMENU_PARAMETERS_X, y);
         break;
   }
} // WeightUnitsParameters

//------------------------------------------------------------------------------
//   Units
//------------------------------------------------------------------------------

static void _SetUnits( int Units)
// Set weighing units
{
int Capacity;

   Capacity = _CapacityToEnum( WeightUnits.Range);    // save old capacity as enum
   WeightUnitsSet( Units);
   WeightUnits.Range = _EnumToCapacity( Capacity);    // set new capacity
} // _SetUnits

//------------------------------------------------------------------------------
//   Enum to capacity
//------------------------------------------------------------------------------

static int _EnumToCapacity( int Code)
// Decode capacity from <Code>
{
   switch( WeightUnits.Units){
      case WEIGHT_UNITS_KG :
         if( Code == WEIGHT_CAPACITY_EXTENDED){
            return( WEIGHT_UNITS_KG_EXT_RANGE);
         } else {
            return( WEIGHT_UNITS_KG_RANGE);
         }
      case WEIGHT_UNITS_G :
         if( Code == WEIGHT_CAPACITY_EXTENDED){
            return( WEIGHT_UNITS_G_EXT_RANGE);
         } else {
            return( WEIGHT_UNITS_G_RANGE);
         }
      case WEIGHT_UNITS_LB :
         if( Code == WEIGHT_CAPACITY_EXTENDED){
            return( WEIGHT_UNITS_LB_EXT_RANGE);
         } else {
            return( WEIGHT_UNITS_LB_RANGE);
         }
   }
   return( 0);
} // _EnumToCapacity

//------------------------------------------------------------------------------
//   Capacity to enum
//------------------------------------------------------------------------------

static int _CapacityToEnum( int Capacity)
// Encode <Capacity> to enum
{
   switch( WeightUnits.Units){
      case WEIGHT_UNITS_KG :
         if( Capacity == WEIGHT_UNITS_KG_EXT_RANGE){
            return( WEIGHT_CAPACITY_EXTENDED);
         } else {
            return( WEIGHT_CAPACITY_NORMAL);
         }
      case WEIGHT_UNITS_G :
         if( Capacity == WEIGHT_UNITS_G_EXT_RANGE){
            return( WEIGHT_CAPACITY_EXTENDED);
         } else {
            return( WEIGHT_CAPACITY_NORMAL);
         }
      case WEIGHT_UNITS_LB :
         if( Capacity == WEIGHT_UNITS_LB_EXT_RANGE){
            return( WEIGHT_CAPACITY_EXTENDED);
         } else {
            return( WEIGHT_CAPACITY_NORMAL);
         }
   }
   return( 0);
} // _CapacityToEnum
