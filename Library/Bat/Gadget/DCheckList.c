//******************************************************************************
//                                                                            
//  DCheckList.c   Display check list
//  Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "DCheckList.h"
#include "DLabel.h"
#include "DEvent.h"
#include "Sound/Beep.h"
#include "DLayout.h"
#include "Graphic/Graphic.h"
#include "Console/conio.h"
#include "Str.h"
#include "Fonts.h"
#include "Bitmap.h"

#define DCHECKLIST_FONT           DMENU_FONT       // same as menu
#define DCHECKLIST_MAX_ROWS       32               // rows limit

#define DCHECKLIST_ITEM_HEIGHT    19               // menu item height
#define DCHECKLIST_TEXT_Y         24               // first item y

#define DCHECKLIST_HEIGHT  (G_HEIGHT - DCHECKLIST_TEXT_Y - DLAYOUT_STATUS_H) // items area height
#define DCHECKLIST_COUNT   (DCHECKLIST_HEIGHT / DCHECKLIST_ITEM_HEIGHT)      // items per page

#define DCHECKLIST_NUMBER_W       50               // line number width

#define DCHECKLIST_NORMAL_COLOR   DCOLOR_DEFAULT   // standard menu item
#define DCHECKLIST_NUMBER_COLOR   COLOR_LIGHTGRAY  // line number

#define DCHECKLIST_CHECKBOX_X     5
#define DCHECKLIST_TEXT_X        (13 + 12)
// Local functions :

static int CheckListRows( const TUniStr *CheckList);
// Returns number of rows

static void CheckListDraw( const TUniStr *CheckList, int FirstRow, int LastCursorRow, int CursorRow, dword Options);
// Draw check list

//------------------------------------------------------------------------------
//  CheckList
//------------------------------------------------------------------------------

TYesNo DCheckList( TUniStr Caption, const TUniStr *CheckList, dword *Options)
// Displays <CheckList>, returns <Options>
{
int   RowsCount;              // all visible menu rows
int   FirstRow;               // page offset
int   PageRows;               // menu rows at page
int   LastItem;
int   CursorItem;
char  Buffer[ 16];
dword _Options;
TYesNo Checked;

   SetFont( DCHECKLIST_FONT);
   GClear();
   DLayoutTitle( Caption);
   DLayoutStatus( STR_BTN_EXIT, STR_BTN_SELECT, 0);
   RowsCount  = CheckListRows( CheckList);    // enabled rows
   if( RowsCount <= DCHECKLIST_COUNT){
      PageRows = RowsCount;                           // all items visible - nonscrollable list
   } else {
      PageRows = DCHECKLIST_COUNT;                    // scrollable list
   }
   _Options   = *Options;
   FirstRow   = 0;
   CursorItem = 0;
   LastItem   = -1;
   Checked    = NO;
   forever {
      if( (LastItem != CursorItem) || Checked){
         // redraw list
         CheckListDraw( CheckList, FirstRow, LastItem, CursorItem, _Options);
         if( LastItem != -1){
            GFlush();                  // redraw cursor area only
         } // else all menu changed
         // clear line number :
         GSetColor( DCOLOR_TITLE_BG);
         GBox( G_WIDTH - DCHECKLIST_NUMBER_W, 0, DCHECKLIST_NUMBER_W, DLAYOUT_TITLE_H);
         // show line number :
         GSetColor( DCHECKLIST_NUMBER_COLOR);
         bprintf( Buffer, "%d/%d", CursorItem + FirstRow + 1, RowsCount);
         DLabelRight( Buffer, G_WIDTH, 2);
         GSetColor( DCOLOR_DEFAULT);
         GFlush();
         LastItem = CursorItem;        // remember last
         Checked  = NO;
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CursorItem == 0){
               if( FirstRow == 0){
                  BeepError();
                  break;
               }
               BeepKey();
               FirstRow--;
               LastItem = -1;          // redraw menu
               break;
            }
            BeepKey();
            CursorItem--;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CursorItem == PageRows - 1){
               if( FirstRow >= RowsCount - PageRows){
                  BeepError();
                  break;
               }
               BeepKey();
               FirstRow++;
               LastItem = -1;          // redraw menu
               break;
            }
            BeepKey();
            CursorItem++;
            break;

         case K_ENTER :
            BeepKey();
            Checked = YES;
            if( _Options & (1 << (FirstRow + CursorItem))){
               // checked
               _Options &= ~(1 << (FirstRow + CursorItem));
            } else {
               // unchecked
               _Options |=  (1 << (FirstRow + CursorItem));
            }
            break;

         case K_TIMEOUT :
         case K_ESC :
            BeepKey();
            *Options = _Options;
            return( YES);
      }     
   }
} // DCheckList

//******************************************************************************

//------------------------------------------------------------------------------
//   List rows count
//------------------------------------------------------------------------------

static int CheckListRows( const TUniStr *CheckList)
// Returns number of rows
{
int Count;

   Count = 0;
   while( *CheckList){
      Count++;
      CheckList++;
   }
   return( Count);
} // CheckListRows

//------------------------------------------------------------------------------
//  List Draw
//------------------------------------------------------------------------------

static void CheckListDraw( const TUniStr *CheckList, int FirstRow, int LastCursorRow, int CursorRow, dword Options)
// Draw check list
{
int Count;
int y;

   if( LastCursorRow == -1){
      // clear all list area
      GSetColor( DCOLOR_BACKGROUND);
      GBox( 0, DLAYOUT_TITLE_H, G_WIDTH, G_HEIGHT - DLAYOUT_TITLE_H - DLAYOUT_STATUS_H);
   }
   // draw items
   CheckList += FirstRow;
   Count      = 0;
   y          = DCHECKLIST_TEXT_Y;
   while( *CheckList){
      if( LastCursorRow != -1){
         // draw partialy
         if( Count != LastCursorRow && Count != CursorRow){
            // dont't redraw other rows
            y += DCHECKLIST_ITEM_HEIGHT;
            CheckList++;
            Count++;
            continue;
         }
         // erase row
         GSetColor( DCOLOR_BACKGROUND);
         GBox( 0, y, G_WIDTH, DCHECKLIST_ITEM_HEIGHT);
      }
      GSetColor( DCHECKLIST_NORMAL_COLOR);            // current text color
      if( Count == CursorRow){
         // selected item
         GSetColor( DCOLOR_CURSOR);                   // cursor block color
         GBoxRound( 0, y, G_WIDTH, DCHECKLIST_ITEM_HEIGHT, DCURSOR_R, DCURSOR_R);
         GSetColor( DCOLOR_BACKGROUND);               // cursor text color
      }
      DLabel( *CheckList, DCHECKLIST_TEXT_X, y);
      if( Options & (1 << (FirstRow + Count))){
         GBitmap( DCHECKLIST_CHECKBOX_X, y + 3, &BmpCheckboxChecked);
      } else {
         GBitmap( DCHECKLIST_CHECKBOX_X, y + 3, &BmpCheckboxUnchecked);
      }
      y += DCHECKLIST_ITEM_HEIGHT;
      CheckList++;
      Count++;
      if( Count == DCHECKLIST_COUNT){
         break;                        // page done
      }
   }
   GSetColor( DCOLOR_DEFAULT);         // restore default color
} // CheckListDraw
