//******************************************************************************
//
//   DDirectory.c  Directory viewer
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "DDirectory.h"
#include "Graphic/Graphic.h"              // Graphic display
#include "Console/conio.h"                // Console
#include "Sound/Beep.h"                   // Sounds
#include "Gadget/DCursor.h"               // List Cursor
#include "Gadget/DList.h"                 // List utility
#include "Gadget/DEvent.h"                // Event manager
#include "Gadget/DLayout.h"               // Display layout
#include "Gadget/DMsg.h"                  // Display message
#include "Str.h"                          // project directory strings
#include "Fonts.h"                        // Project fonts
#include "Bitmap.h"                       // Bitmaps


// Local functions :

static void DisplayPage( const UDirectory *Directory, TListCursor *Cursor, TUniStr SpecialItem);
// Display directory page

//------------------------------------------------------------------------------
//  Directory select
//------------------------------------------------------------------------------

UDirectoryIndex DDirectorySelect( TUniStr Title, const UDirectory *Directory, UDirectoryIndex InitialIndex, TUniStr SpecialItem)
// Select item from <Directory>, move cursor at <InitialIndex> item. <SpecialItem> added at end of list
{
TListCursor     Cursor;
int             Count;
UDirectoryIndex Index;

   Count = uDirectoryCount( (UDirectory *)Directory);
   // check for list items count :
   if( Count == 0 && !SpecialItem){
      DMsgOk( Title, STR_NO_ITEMS_FOUND, STR_LIST_IS_EMPTY);
      return( UDIRECTORY_INDEX_INVALID);
   }
   if( SpecialItem){
      Count++;
   }
   DCursorInit( &Cursor, Count, DLIST_ROWS_COUNT_MAX);
   DCursorIndexSet( &Cursor, InitialIndex);       // move cursor at initial position
   DCursorUseGridSet( &Cursor, NO);               // don't display horizontal lines
   forever {
      if( DCursorPageChanged( &Cursor)){
         // redraw page
         GClear();                                // clear display
         DLayoutTitle( Title);                    // display title
         DLayoutStatus( STR_BTN_CANCEL, STR_BTN_SELECT, 0); // display status line
      }
      if( DCursorRowChanged( &Cursor)){
         DisplayPage( Directory, &Cursor, SpecialItem);
         DCursorRowUpdate( &Cursor);
         GFlush();                               // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            DCursorRowUp( &Cursor);
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            DCursorRowDown( &Cursor);
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            DCursorPageDown( &Cursor);
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            DCursorPageUp( &Cursor);
            break;

         case K_ENTER :
            Index = DCursorIndex( &Cursor);
            if( !SpecialItem){
               return( Index);
            }
            if( Index == DCursorCount( &Cursor) - 1){
               return( DDIRECTORY_INDEX_SPECIAL);     // last item is special
            }
            return( Index);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( UDIRECTORY_INDEX_INVALID);
      }
   }
} // DDirectorySelect

//******************************************************************************

//------------------------------------------------------------------------------
//   Display page
//------------------------------------------------------------------------------

#define DIRECTORY_NAME_X   3

static void DisplayPage( const UDirectory *Directory, TListCursor *Cursor, TUniStr SpecialItem)
// Display directory page
{
int  RowY;
int  i;
char Name[ UDIRECTORY_NAME_SIZE + 1];
dword Index;

   DListClear( Cursor);                // clear list area
   // draw points
   for( i = 0; i < DCursorRowCount( Cursor); i++){
      RowY = DListY( i);
      if( !DListCursor( Cursor, i)){
         continue;
      }
      // get item data :
      Index = DCursorPage( Cursor) + i;
      if( SpecialItem && (Index == DCursorCount( Cursor) - 1)){
         GTextAt( DIRECTORY_NAME_X, RowY);
         cputs( SpecialItem);
         continue;
      }
      uDirectoryItemName( (UDirectory *)Directory, Index, Name);
      // item name :
      GTextAt( DIRECTORY_NAME_X, RowY);
      cputs( Name);
   }
   GFlush();                           // redraw rows
} // DisplayPage
