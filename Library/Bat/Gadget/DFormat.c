//******************************************************************************
//                                                                            
//   DFormat.c      Display formats
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DFormat.h"
#include "Console/xprint.h"
#include "Console/sputchar.h"

//------------------------------------------------------------------------------
//  Format Range
//------------------------------------------------------------------------------

void DFormatRange( char *Buffer, int Decimals, int LoLimit, int HiLimit, TUniStr Units)
// Format range data
{
dword Format;

   if( Decimals){
      // with decimal point
      Format = FmtPrecision( 1, Decimals);  // width plus dot
   } else {
      // integer number
      Format = FmtSetWidth( 0);             // width only
   }
   sputcharbuffer( Buffer);
   sputchar( '[');
   xprintdec( sputchar, LoLimit, Format);
   sputchar( ' ');
   sputchar( '.');
   sputchar( '.');
   sputchar( ' ');
   xprintdec( sputchar, HiLimit, Format);
   sputchar( ']');
   if( Units){
      sputchar( ' ');
      Units = StrGet( Units);
      while( *Units){
         sputchar( *Units);
         Units++;
      }
   }
   sputchar( 0);                            // string terminator
} // DFormatRange
