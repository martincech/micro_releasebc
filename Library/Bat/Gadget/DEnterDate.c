//******************************************************************************
//
//   DEnterDate.c   Display enter date
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Gadget/DEnter.h"
#include "Graphic/Graphic.h"
#include "Console/conio.h"
#include "Convert/uBcd.h"
#include "Sound/Beep.h"
#include "Gadget/DEvent.h"
#include "Gadget/DLabel.h"
#include "Country/Country.h"
#include "Country/xTime.h"         // Month name only
#include "System/System.h"

// Local functions :

static unsigned DateToBcd( UDate *Date);
// Returns BCD time

static void BcdToDate( UDate *Date, unsigned Bcd);
// Converts <Bcd> to <Date>

static int DateMaxField( void);
// Returns highest possible field index

static int DateGetOffset( int Field);
// Returns character offset of the field

static int DateGetNumber( int Field);
// Returns limit on <Field>

static TYesNo ValidDate( UDate *Date, int *Field);
// Check for date validity. Returns the wrong <Field>

static void DatePrint( int Value);
// Print BCD <Value> date

//------------------------------------------------------------------------------
//  Date
//------------------------------------------------------------------------------

// date fields  :
typedef enum {
   FLD_1YEAR,
   FLD_10YEAR,
   FLD_1MONTH,
   FLD_10MONTH,
   FLD_1DAY,
   FLD_10DAY,
   _FLD_LAST
} TDateField;

TYesNo DEnterDate( UDate *Date, int x, int y)
// Enter date
{
int    Field;         // active field
int    FieldMax;      // max field
int    Value;
int    OldValue;
TYesNo ShowCursor;    // show/hide cursor
TYesNo FlashCursor;   // draw/skip cursor
int    TmpValue;
int    Event;
TYesNo AutoMove;
int    AutoMoveTimeout;

   if( uDateValid( Date) != DATE_OK){
      // repair garbage
      Date->Day   = 1;
      Date->Month = 1;
      Date->Year  = 0;
   }
   Field       = DateMaxField();       // edit MSB
   FieldMax    = Field;                // max field
   Value       = DateToBcd( Date);
   OldValue    = -1;
   ShowCursor  = YES;                  // show cursor
   FlashCursor = NO;                   // no cursor
   SysKeyAscii( SYS_KEY_NUMBERS);
   AutoMove = YES;
   AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
   forever {
      if( Value != OldValue){          // value changed - redraw
         // clear area :
         GSetColor( DCOLOR_ENTER_BG);
         GBox( x, y,  DEnterDateWidth(), DENTER_H);
         GSetColor( DCOLOR_ENTER);
         // draw text :
         GTextAt( x, y);
         GSetFixedPitch();             // set nonproportional font
         DatePrint( Value);
         GSetNormalPitch();            // restore font setting
         // draw cursor :
         if( !ShowCursor || FlashCursor){
            GSetMode( GMODE_XOR);
            GSetColor( DCOLOR_ENTER_CURSOR);
            GBox( x + DateGetOffset( Field) * GCharWidth(), y, GCharWidth() - 1, DENTER_H);
            GSetMode( GMODE_REPLACE);
         }
         GSetColor( DCOLOR_DEFAULT);
         OldValue = Value;             // remember value
         GFlush();                     // redraw
      }
      Event = DEventWait();
      switch( Event){
         case K_UP | K_REPEAT :
         case K_UP :
            AutoMove = YES;
            ShowCursor = NO;                               // disable cursor
            TmpValue = uBcdDigitGet( Value, Field);        // select digit
            if( TmpValue >= DateGetNumber( Field)){
               TmpValue = 0;
            } else {
               TmpValue++;
            }
            BeepKey();
            Value = uBcdDigitSet( Value, Field, TmpValue); // compose digit
            break;

         case K_DOWN | K_REPEAT :
         case K_DOWN :
            AutoMove = YES;
            ShowCursor = NO;                               // disable cursor
            TmpValue = uBcdDigitGet( Value, Field);        // select digit
            if( TmpValue == 0){
               TmpValue = DateGetNumber( Field);
            } else {
               TmpValue--;
            }
            BeepKey();
            Value = uBcdDigitSet( Value, Field, TmpValue); // compose digit
            break;

         case K_FLASH1 :
            if( !AutoMove){
               AutoMoveTimeout--;
            }

            FlashCursor = YES;
            OldValue    = -1;       // force redraw
            if( AutoMoveTimeout == 0){
               AutoMove = YES;
               SysKeyAscii( SYS_KEY_NUMBERS);
            } else {
               break;
            }
         while(AutoMove){
            default:
               if( Event & K_ASCII){
                  if(!(Event & K_REPEAT) && !AutoMove){
                     AutoMove = YES;
                  }else {
                     AutoMove = NO;
                     AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
                     if( (Event & ~K_ASCII & ~K_REPEAT) == ' '){
                        break;
                     }
                     Value = uBcdDigitSet( Value, Field, (Event & ~K_ASCII & ~K_REPEAT) - '0');
                     ShowCursor = YES;
                     break; // cycle break
                  }
               }else {
                  if (Event & K_RELEASED){
                     ShowCursor = YES;       // key released, redraw cursor
                     OldValue   = -1;        // force redraw
                     break;
                  }else if( Event != K_FLASH1){
                     break; // cycle break
                  }else{
                     AutoMove = NO;
                     AutoMoveTimeout = TIMER_KEY_POS_SHIFT;
                  }
               }
            case K_RIGHT | K_REPEAT :
            case K_RIGHT :
               ShowCursor = NO;                               // disable cursor
               if( Field == 0){
                  if( AutoMove) continue; // cycle continue/break
                  else {
                     BeepError();
                     break;
                  }
               }
               BeepKey();
               Field--;
               OldValue   = -1;        // force redraw
               if( AutoMove) continue; // cycle continue/break
               else break;
         }
            if( (Event & ~K_REPEAT) == K_RIGHT || (Event == K_FLASH1)){
               AutoMove = YES;
            }
            if( Event == K_FLASH1){
               ShowCursor = YES;
            }
            break; // case break

         case K_LEFT | K_REPEAT :
         case K_LEFT :
            AutoMove = YES;
            ShowCursor = NO;                               // disable cursor
            if( Field == FieldMax){
               BeepError();
               break;
            }
            BeepKey();
            Field++;
            OldValue   = -1;        // force redraw
            break;

         case K_RELEASED :
            ShowCursor = YES;       // key released, redraw cursor
            OldValue   = -1;        // force redraw
            break;


         case K_FLASH2 :
            FlashCursor = NO;
            OldValue    = -1;       // force redraw
            break;

         case K_ENTER :
            BcdToDate( Date, Value);
            if( ValidDate( Date, &Field)){
               BeepKey();
               SysKeyAscii( SYS_KEY_DEFAULT);
               return( YES);
            }
            BeepError();
            OldValue   = -1;           // force redraw
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            SysKeyAscii( SYS_KEY_DEFAULT);
            return( NO);
      }
   }
} // DEnterDate

#ifdef DENTER_YEAR_YYYY
//------------------------------------------------------------------------------
//  Date width
//------------------------------------------------------------------------------

int DEnterDateWidth( void)
// Returns widht of date field
{
   if( CountryDateFormat() >= DATE_FORMAT_DDMMMYYYY){
      return( 11 * GCharWidth());
   } else {
      return( 10 * GCharWidth());
   }
} // DEnterDateWidth

#else // DENTER_YEAR_YY
//------------------------------------------------------------------------------
//  Date width
//------------------------------------------------------------------------------

int DEnterDateWidth( void)
// Returns widht of date field
{
   if( CountryDateFormat() >= DATE_FORMAT_DDMMMYYYY){
      return( 9 * GCharWidth());
   } else {
      return( 8 * GCharWidth());
   }
} // DEnterDateWidth
#endif // DENTER_YEAR_YY

//******************************************************************************

//------------------------------------------------------------------------------
//  Date to BCD
//------------------------------------------------------------------------------

static unsigned DateToBcd( UDate *Date)
// Returns BCD time
{
   switch( CountryDateFormat()){
      case DATE_FORMAT_DDMMYYYY :
         return( uBinaryToBcd( Date->Day   * 10000 + Date->Month * 100 + Date->Year));
      case DATE_FORMAT_MMDDYYYY :
         return( uBinaryToBcd( Date->Month * 10000 + Date->Day   * 100 + Date->Year));
      case DATE_FORMAT_YYYYMMDD :
         return( uBinaryToBcd( Date->Year  * 10000 + Date->Month * 100 + Date->Day));
      case DATE_FORMAT_YYYYDDMM :
         return( uBinaryToBcd( Date->Year  * 10000 + Date->Day   * 100 + Date->Month));

      case DATE_FORMAT_DDMMMYYYY :
         return( uBinaryToBcd( Date->Day   * 1000 + Date->Year)      | (Date->Month  - 1) << 8);
      case DATE_FORMAT_MMMDDYYYY :
         return( uBinaryToBcd( Date->Day   * 100  + Date->Year)      | (Date->Month  - 1) << 16);
      case DATE_FORMAT_YYYYMMMDD :
         return( uBinaryToBcd( Date->Year  * 1000 + Date->Day)       | (Date->Month  - 1) << 8);
      case DATE_FORMAT_YYYYDDMMM :
         return( uBinaryToBcd( Date->Year  * 1000 + Date->Day * 10)  | (Date->Month  - 1));
   }
   return( 0);
} // DateToBcd

static void BcdToDate( UDate *Date, unsigned Bcd)
// Converts <Bcd> to <Date>
{
   switch( CountryDateFormat()){
      case DATE_FORMAT_DDMMYYYY :
         Bcd = uBcdToBinary( Bcd);
         Date->Year  = Bcd % 100;
         Bcd /= 100;
         Date->Month = Bcd % 100;
         Bcd /= 100;
         Date->Day   = Bcd;
         break;

      case DATE_FORMAT_MMDDYYYY :
         Bcd = uBcdToBinary( Bcd);
         Date->Year  = Bcd % 100;
         Bcd /= 100;
         Date->Day   = Bcd % 100;
         Bcd /= 100;
         Date->Month = Bcd;
         break;

      case DATE_FORMAT_YYYYMMDD :
         Bcd = uBcdToBinary( Bcd);
         Date->Day   = Bcd % 100;
         Bcd /= 100;
         Date->Month = Bcd % 100;
         Bcd /= 100;
         Date->Year  = Bcd;
         break;

      case DATE_FORMAT_YYYYDDMM :
         Bcd = uBcdToBinary( Bcd);
         Date->Month = Bcd % 100;
         Bcd /= 100;
         Date->Day   = Bcd % 100;
         Bcd /= 100;
         Date->Year  = Bcd;
         break;

      case DATE_FORMAT_DDMMMYYYY :
         Date->Month = ((Bcd >> 8)  & 0x0F) + 1;
         Bcd &= ~(0x0F << 8);
         Bcd = uBcdToBinary( Bcd);
         Date->Year  = Bcd % 100;
         Bcd /= 1000;
         Date->Day   = Bcd;
         break;

      case DATE_FORMAT_MMMDDYYYY :
         Date->Month = ((Bcd >> 16) & 0x0F) + 1;
         Bcd &= ~(0x0F << 16);
         Bcd = uBcdToBinary( Bcd);
         Date->Year  = Bcd % 100;
         Bcd /= 100;
         Date->Day   = Bcd;
         break;

      case DATE_FORMAT_YYYYMMMDD :
         Date->Month = ((Bcd >> 8)  & 0x0F) + 1;
         Bcd &= ~(0x0F << 8);
         Bcd = uBcdToBinary( Bcd);
         Date->Day   = Bcd % 100;
         Bcd /= 1000;
         Date->Year   = Bcd;
         break;

      case DATE_FORMAT_YYYYDDMMM :
         Date->Month = ( Bcd        & 0x0F) + 1;
         Bcd &= ~0x0F;
         Bcd = uBcdToBinary( Bcd);
         Bcd /= 10;
         Date->Day  = Bcd % 100;
         Bcd /= 100;
         Date->Year = Bcd;
         break;
   }
} // DateToBcd

//------------------------------------------------------------------------------
//  Max field
//------------------------------------------------------------------------------

static int DateMaxField( void)
// Returns highest possible field index
{
   if( CountryDateFormat() >= DATE_FORMAT_DDMMMYYYY){
      return( FLD_10DAY - 1);
   } else {
      return( FLD_10DAY);
   }
} // DateMaxField

//------------------------------------------------------------------------------
//  Date offset
//------------------------------------------------------------------------------

// date offsets :
#ifdef DENTER_YEAR_YYYY
static const byte _Offset[ _DATE_FORMAT_COUNT][ _FLD_LAST] = {
/* DATE_FORMAT_DDMMYYYY */  /* 1year  */  {9, 8, 4, 3, 1, 0}, /* 10 day   */
/* DATE_FORMAT_MMDDYYYY */  /* 1year  */  {9, 8, 4, 3, 1, 0}, /* 10 month */
/* DATE_FORMAT_YYYYMMDD */  /* 1day   */  {9, 8, 6, 5, 3, 2}, /* 10 year  */
/* DATE_FORMAT_YYYYDDMM */  /* 1month */  {9, 8, 6, 5, 3, 2}, /* 10 year  */

/* DATE_FORMAT_DDMMMYYYY */ /* 1year  */  {10, 9, 3, 1, 0, 0}, /* 10 day  */
/* DATE_FORMAT_MMMDDYYYY */ /* 1year  */  {10, 9, 5, 4, 0, 0}, /* 10 day  */
/* DATE_FORMAT_YYYYMMMDD */ /* 1day   */  {10, 9, 5, 3, 2, 0}, /* 10 year */
/* DATE_FORMAT_YYYYDDMMM */ /* 1month */  {8,  6, 5, 3, 2, 0}, /* 10 year */
};
#else // DENTER_YEAR_YY
static const byte _Offset[ _DATE_FORMAT_COUNT][ _FLD_LAST] = {
/* DATE_FORMAT_DDMMYYYY */  /* 1year  */  {7, 6, 4, 3, 1, 0}, /* 10 day   */
/* DATE_FORMAT_MMDDYYYY */  /* 1year  */  {7, 6, 4, 3, 1, 0}, /* 10 month */
/* DATE_FORMAT_YYYYMMDD */  /* 1day   */  {7, 6, 4, 3, 1, 0}, /* 10 year  */
/* DATE_FORMAT_YYYYDDMM */  /* 1month */  {7, 6, 4, 3, 1, 0}, /* 10 year  */

/* DATE_FORMAT_DDMMMYYYY */ /* 1year  */  {8, 7, 3, 1, 0, 0}, /* 10 day  */
/* DATE_FORMAT_MMMDDYYYY */ /* 1year  */  {8, 7, 5, 4, 0, 0}, /* 10 day  */
/* DATE_FORMAT_YYYYMMMDD */ /* 1day   */  {8, 7, 3, 1, 0, 0}, /* 10 year */
/* DATE_FORMAT_YYYYDDMMM */ /* 1month */  {6, 4, 3, 1, 0, 0}, /* 10 year */
};
#endif // DENTER_YEAR_YY

static int DateGetOffset( int Field)
// Returns character offset of the field
{
   return( _Offset[ CountryDateFormat()][ Field]);
} // DateGetOffset

//------------------------------------------------------------------------------
//  Date number
//------------------------------------------------------------------------------

// date limits :
static const byte _Limit[ _DATE_FORMAT_COUNT][ _FLD_LAST] = {
/* DATE_FORMAT_DDMMYYYY */  /* 1year  */   {9, 9, 9, 1, 9, 3}, /* 10day   */
/* DATE_FORMAT_MMDDYYYY */  /* 1year  */   {9, 9, 9, 3, 9, 1}, /* 10month */
/* DATE_FORMAT_YYYYMMDD */  /* 1day   */   {9, 3, 9, 1, 9, 9}, /* 10year  */
/* DATE_FORMAT_YYYYDDMM */  /* 1month */   {9, 1, 9, 3, 9, 9}, /* 10year  */

/* DATE_FORMAT_DDMMMYYYY */  /* 1year  */   {9, 9, 0x0B, 9, 3, 0},  /* 10day  */
/* DATE_FORMAT_MMMDDYYYY */  /* 1year  */   {9, 9, 9, 3, 0x0B, 0},  /* 1month */
/* DATE_FORMAT_YYYYMMMDD */  /* 1day   */   {9, 3, 0x0B, 9, 9, 0},  /* 1year  */
/* DATE_FORMAT_YYYYDDMMM */  /* 1month */   {0x0B, 9, 3, 9, 9, 0},  /* 1year  */
};

static int DateGetNumber( int Field)
// Returns limit on <Field>
{
   return( _Limit[ CountryDateFormat()][ Field]);
} // DateGetNumber


//------------------------------------------------------------------------------
//  Valid date
//------------------------------------------------------------------------------


static const byte _Field[ _DATE_FORMAT_COUNT][ 3] = {
                            // day, month, year
/* DATE_FORMAT_DDMMYYYY */  { 5, 3, 1},
/* DATE_FORMAT_MMDDYYYY */  { 3, 5, 1},
/* DATE_FORMAT_YYYYMMDD */  { 1, 3, 5},
/* DATE_FORMAT_YYYYDDMM */  { 3, 1, 5},

/* DATE_FORMAT_DDMMMYYYY */ { 4, 2, 1},
/* DATE_FORMAT_MMMDDYYYY */ { 3, 4, 1},
/* DATE_FORMAT_YYYYMMMDD */ { 1, 2, 4},
/* DATE_FORMAT_YYYYDDMMM */ { 2, 0, 4},
};

static TYesNo ValidDate( UDate *Date, int *Field)
// Check for date validity. Returns the wrong <Field>
{
int Result;

   Result = uDateValid( Date);
   if( Result == DATE_OK){
      return( YES);
   }
   Result -= DATE_WRONG_DAY;
   *Field  = _Field[ CountryDateFormat()][ Result];
   return( NO);
} // ValidDate

#ifdef DENTER_YEAR_YYYY
//------------------------------------------------------------------------------
//  Date print
//------------------------------------------------------------------------------

static void DatePrint( int Value)
// Print BCD <Value> date
{
int  f0, f1, f2;
char Separator1;
char Separator2;

   Separator1 = CountryDateSeparator1();
   Separator2 = CountryDateSeparator2();
   if( CountryDateFormat() < DATE_FORMAT_DDMMMYYYY){
      f0 = (Value >> 16)  & 0xFF;
      f1 = (Value >>  8)  & 0xFF;
      f2 =  Value         & 0xFF;
      switch( CountryDateFormat()){
         case DATE_FORMAT_DDMMYYYY :
         case DATE_FORMAT_MMDDYYYY :
            cprintf( "%02x%c%02x%c%04x", f0, Separator1, f1, Separator2, f2 | 0x2000);
            break;
         case DATE_FORMAT_YYYYMMDD :
         case DATE_FORMAT_YYYYDDMM :
            cprintf( "%04x%c%02x%c%02x", f0 | 0x2000, Separator1, f1, Separator2, f2);
            break;
      }
      return;
   }
   f0 = (Value >> 12)  & 0xFF;
   f1 = (Value >>  8)  & 0xFF;
   f2 =  Value         & 0xFF;
   switch( CountryDateFormat()){
      case DATE_FORMAT_DDMMMYYYY :
         cprintf( "%02x%c%s%c%04x", f0, Separator1, xDateMonth( (f1 & 0x0F) + 1), Separator2, f2 | 0x2000);
         break;
      case DATE_FORMAT_MMMDDYYYY :
         cprintf( "%s%c%02x%c%04x", xDateMonth( ((Value >> 16) & 0x0F) + 1), Separator1, f1, Separator2, f2 | 0x2000);
         break;
      case DATE_FORMAT_YYYYMMMDD :
         cprintf( "%04x%c%s%c%02x", f0 | 0x2000, Separator1, xDateMonth( (f1 & 0x0F) + 1), Separator2, f2);
         break;
      case DATE_FORMAT_YYYYDDMMM :
         cprintf( "%04x%c%02x%c%s", f0 | 0x2000, Separator1, (Value >> 4) & 0xFF, Separator2, xDateMonth( (f2 & 0x0F) + 1));
         break;
   }
} // DatePrint

#else // DENTER_YEAR_YY
//------------------------------------------------------------------------------
//  Date print
//------------------------------------------------------------------------------

static void DatePrint( int Value)
// Print BCD <Value> date
{
int  f0, f1, f2;
char Separator1;
char Separator2;

   Separator1 = CountryDateSeparator1();
   Separator2 = CountryDateSeparator2();
   if( CountryDateFormat() < DATE_FORMAT_DDMMMYYYY){
      f0 = (Value >> 16)  & 0xFF;
      f1 = (Value >>  8)  & 0xFF;
      f2 =  Value         & 0xFF;
      switch( CountryDateFormat()){
         case DATE_FORMAT_DDMMYYYY :
         case DATE_FORMAT_MMDDYYYY :
            cprintf( "%02x%c%02x%c%02x", f0, Separator1, f1, Separator2, f2);
            break;
         case DATE_FORMAT_YYYYMMDD :
         case DATE_FORMAT_YYYYDDMM :
            cprintf( "%02x%c%02x%c%02x", f0, Separator1, f1, Separator2, f2);
            break;
      }
      return;
   }
   f0 = (Value >> 12)  & 0xFF;
   f1 = (Value >>  8)  & 0xFF;
   f2 =  Value         & 0xFF;
   switch( CountryDateFormat()){
      case DATE_FORMAT_DDMMMYYYY :
         cprintf( "%02x%c%s%c%02x", f0, Separator1, xDateMonth( (f1 & 0x0F) + 1), Separator2, f2);
         break;
      case DATE_FORMAT_MMMDDYYYY :
         cprintf( "%s%c%02x%c%02x", xDateMonth( ((Value >> 16) & 0x0F) + 1), Separator1, f1, Separator2, f2);
         break;
      case DATE_FORMAT_YYYYMMMDD :
         cprintf( "%02x%c%s%c%02x", f0, Separator1, xDateMonth( (f1 & 0x0F) + 1), Separator2, f2);
         break;
      case DATE_FORMAT_YYYYDDMMM :
         cprintf( "%02x%c%02x%c%s", f0, Separator1, (Value >> 4) & 0xFF, Separator2, xDateMonth( (f2 & 0x0F) + 1));
         break;
   }
} // DatePrint
#endif // DENTER_YEAR_YY
