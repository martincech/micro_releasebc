//******************************************************************************
//                                                                            
//  DMsg.c         Display text message box
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DMsg.h"
#include "DLabel.h"
#include "DEvent.h"
#include "DLayout.h"                 // Display layout
#include "Graphic/Graphic.h"
#include "Str.h"                     // strings from project directory
#include "Fonts.h"                   // project fonts
#include "Bitmap.h"
#include <string.h>

// local functions :
static void FormatText( TUniStr Text, TUniStr Text2);
// Draw texts

//------------------------------------------------------------------------------
//  Info
//------------------------------------------------------------------------------

void DMsgOk( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays <Text> with title <Caption> with OK button
{
   SetFont( DMSG_FONT);
   GClear();
   DLayoutTitle( Caption);
   FormatText( Text, Text2);
   DLayoutStatus( 0, STR_BTN_OK, 0);
   GFlush();
   // wait for user response :
   DEventWaitForEnter();
} // DMsgOk

//------------------------------------------------------------------------------
//  Nowait info
//------------------------------------------------------------------------------

void DMsgCancel( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays window with Cancel button. Warning : doesn't wait for a key
{
   SetFont( DMSG_FONT);
   GClear();
   DLayoutTitle( Caption);
   FormatText( Text, Text2);
   DLayoutStatus( STR_BTN_CANCEL, 0, 0);
   GFlush();
} // DMsgCancel

//------------------------------------------------------------------------------
//  Confirmation
//------------------------------------------------------------------------------

TYesNo DMsgOkCancel( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays <Text> with title <Caption> with OK/Cancel buttons
{
   SetFont( DMSG_FONT);
   GClear();
   DLayoutTitle( Caption);
   FormatText( Text, Text2);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // wait for user response :
   return( DEventWaitForEnterEsc());
} // DMsgOkCancel

//------------------------------------------------------------------------------
//  Selection
//------------------------------------------------------------------------------

TYesNo DMsgYesNo( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays <Text>/<Text2> with title <Caption> with Yes/No buttons
{
   SetFont( DMSG_FONT);
   GClear();
   DLayoutTitle( Caption);
   FormatText( Text, Text2);
   DLayoutStatus( STR_BTN_NO, STR_BTN_YES, 0);
   GFlush();
   // wait for user response :
   return( DEventWaitForEnterEsc());
} // DMsgYesNo

//------------------------------------------------------------------------------
// Wait
//------------------------------------------------------------------------------

void DMsgWait( void)
// Display waiting
{
   GClear();
   GBitmap( 104, 55, &BmpWait);
   GSetColor( DCOLOR_DEFAULT);
   SetFont( TAHOMA16);
   DLabelCenter( STR_BOX_WAIT, 0, 122, G_WIDTH, 0);
   GFlush();
} // DMsgWait

//------------------------------------------------------------------------------
//  Format text
//------------------------------------------------------------------------------

#define ROW_HEIGHT   20

static void FormatText( TUniStr Text, TUniStr Text2)
// Draw texts
{
int   Rows;
const char *p;
int   y;

   // get number of rows :
   Rows = 1;                           // at least one row
   p = StrGet( Text);
   while( *p){
      if( *p == '\n'){
         Rows++;
      }
      p++;
   }
   // draw <Text> rows :
   y = G_HEIGHT / 2 - (Rows * ROW_HEIGHT) / 2;
   p = StrGet( Text);
   do {
      DLabelCenter( p,  0, y, G_WIDTH, 0);
      p  = strchr( p, '\n');           // line break
      p++;                             // start of the new line
      y += ROW_HEIGHT;      
   } while( --Rows);
   // draw <Text2> row :
   if( Text2){
      DLabelCenter( Text2,  0, y, G_WIDTH, 0);
   }
} // FormatText
