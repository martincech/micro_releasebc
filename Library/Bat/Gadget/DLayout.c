//******************************************************************************
//
//  DLayout.c      Display layout
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DLayout.h"
#include "DLabel.h"
#include "Graphic/Graphic.h"
#include "Console/conio.h"
#include "Bitmap.h"
#include "Fonts.h"
#include "Str.h"

#define RIGHT_SPACE         3                              // right side icon space
#define STATUS_ICON_X       3                              // left icon
#define STATUS_TEXT_X       30                             // left text
#define STATUS_LINE_Y       (G_HEIGHT - DLAYOUT_STATUS_H)  // status line
#define STATUS_BOX_H        (DLAYOUT_STATUS_H - 1)         // status box width
#define STATUS_ICON_Y       (STATUS_LINE_Y + 3)            // status icon
#define STATUS_TEXT_Y       (STATUS_LINE_Y + 3)            // status text


static int DShowLayoutStatus( TUniStr Left, TUniStr Right, const TBitmap *RightIcon);
// show layout status with left and right texts and buttons, return position of the right button x

//------------------------------------------------------------------------------
//  Title
//------------------------------------------------------------------------------

void DLayoutTitle( TUniStr Caption)
// Display window <Title>
{
   SetFont( DLAYOUT_FONT);
   // draw frame
   GSetColor( DCOLOR_TITLE_BG);
   GBox( 0, 0, G_WIDTH, DLAYOUT_TITLE_H);
   // draw title
   if( Caption){
      GSetColor( DCOLOR_TITLE);
      DLabelCenter( Caption, 0, 0, G_WIDTH, DLAYOUT_TITLE_H);
   }
   GSetColor( DCOLOR_DEFAULT);
} // DLayoutTitle

void DLayoutTitleLTRT( TUniStr Center, TUniStr Left, TUniStr Right)
// Display window <Title> with left and right texts next to it
{
   DLayoutTitle(Center);
   GSetColor( DCOLOR_TITLE);
   // draw left
   if( Left){
      DLabel( Left, RIGHT_SPACE, 0);
   }
   // draw right
   if( Right){
      DLabelRight( Right, RIGHT_SPACE, 0);
   }
   GSetColor( DCOLOR_DEFAULT);
}

void DLayoutTitleLTRI( TUniStr Caption, TUniStr Left, const TBitmap *RightIcon)
// Display window <Title> with left text and right icon next to it
{
   DLayoutTitleLTRT(Caption, Left, 0);
   GSetColor( DCOLOR_DEFAULT);
   // draw right icon
   if( RightIcon){
      GBitmap( G_WIDTH - RightIcon->Width - RIGHT_SPACE, RIGHT_SPACE, RightIcon);
   }
}

void DLayoutTitleLIRI( TUniStr Caption, const TBitmap *LeftIcon, const TBitmap *RightIcon)
// Display window <Title> with left icon and right icon next to it
{
   DLayoutTitleLTRI(Caption, 0, RightIcon);
   if( LeftIcon){
      GBitmap( RIGHT_SPACE, RIGHT_SPACE, LeftIcon);
   }
}

void DLayoutTitleLIRT( TUniStr Caption, const TBitmap *LeftIcon, TUniStr Right)
// Display window <Title> with left icon and right text next to it
{
   DLayoutTitleLTRT(Caption, 0, Right);
   if( LeftIcon){
      GBitmap( RIGHT_SPACE, RIGHT_SPACE, LeftIcon);
   }
}
//------------------------------------------------------------------------------
//  Status line
//------------------------------------------------------------------------------

void DLayoutStatus( TUniStr Left, TUniStr Right, const TBitmap *RightIcon)
// Draw status line
{
   DShowLayoutStatus(Left, Right, RightIcon);
} // DLayoutStatus

static int DShowLayoutStatus( TUniStr Left, TUniStr Right, const TBitmap *RightIcon)
{
int Width;
int RightX;

   SetFont( DLAYOUT_FONT);
   // draw frame
   GSetColor( DCOLOR_STATUS_LINE);
   GLine( 0, STATUS_LINE_Y, G_WIDTH, STATUS_LINE_Y);
   GSetColor( DCOLOR_STATUS_BG);
   GBox( 0, STATUS_LINE_Y + 1, G_WIDTH, STATUS_BOX_H);
   // left button
   GSetColor( DCOLOR_STATUS);
   if( Left){
      GBitmap( STATUS_ICON_X,  STATUS_ICON_Y, &BmpButtonCancel);
      GTextAt( STATUS_TEXT_X, STATUS_TEXT_Y);
      cputs( Left);
   }
   // right bitmap
   if( !RightIcon){
      RightIcon = &BmpButtonOk;        // default button
   }
   // right button
   if( Right){
      Width = GTextWidth( StrGet( Right));
      RightX = G_WIDTH - Width - 3 - RightIcon->Width - 2 * RIGHT_SPACE;
      GBitmap( RightX, STATUS_ICON_Y, RightIcon);
      GTextAt( G_WIDTH - Width + 21  - RightIcon->Width - 2 * RIGHT_SPACE, STATUS_TEXT_Y);
      cputs( Right);
   }
   GSetColor( DCOLOR_DEFAULT);
   return RightX;
}

void DLayoutStatusWithVertical( TUniStr Left, TUniStr Right, const TBitmap *RightIcon)
// Draw status line with right and left images and texts + cursor keys (vertical)
{
int RightX;

   RightX = DShowLayoutStatus(Left, Right, RightIcon);

   // left/right button
   GSetColor( DCOLOR_STATUS);
   GBitmap( RightX - (BmpButtonLeftRight.Width + RIGHT_SPACE),  STATUS_ICON_Y, &BmpButtonLeftRight);
   GSetColor( DCOLOR_DEFAULT);
}

//------------------------------------------------------------------------------
//  Status line with cursor
//------------------------------------------------------------------------------

void DLayoutStatusMove( void)
// Draw status line with all cursor keys
{
   SetFont( DLAYOUT_FONT);
   // draw frame
   GSetColor( DCOLOR_STATUS_LINE);
   GLine( 0, STATUS_LINE_Y, G_WIDTH, STATUS_LINE_Y);
   GSetColor( DCOLOR_STATUS_BG);
   GBox( 0, STATUS_LINE_Y + 1, G_WIDTH, STATUS_BOX_H);
   // left button
   GSetColor( DCOLOR_STATUS);
   GBitmap( STATUS_ICON_X,  STATUS_ICON_Y, &BmpButtonCancel);
   GTextAt( STATUS_TEXT_X, STATUS_TEXT_Y);
   cputs( STR_BTN_CANCEL);
   // right buttons
   GBitmap( G_WIDTH - 20, STATUS_ICON_Y, &BmpButtonRight);
   GBitmap( G_WIDTH - 40, STATUS_ICON_Y, &BmpButtonLeft);
   GBitmap( G_WIDTH - 60, STATUS_ICON_Y, &BmpButtonDown);
   GBitmap( G_WIDTH - 80, STATUS_ICON_Y, &BmpButtonUp);
   GSetColor( DCOLOR_DEFAULT);
} // DLayoutStatusMove

//------------------------------------------------------------------------------
//  Status line with vertical cursor
//------------------------------------------------------------------------------

void DLayoutStatusMoveVertical( void)
// Draw status line with vertical cursor keys
{
   SetFont( DLAYOUT_FONT);
   // draw frame
   GSetColor( DCOLOR_STATUS_LINE);
   GLine( 0, STATUS_LINE_Y, G_WIDTH, STATUS_LINE_Y);
   GSetColor( DCOLOR_STATUS_BG);
   GBox( 0, STATUS_LINE_Y + 1, G_WIDTH, STATUS_BOX_H);
   // left button
   GSetColor( DCOLOR_STATUS);
   GBitmap( STATUS_ICON_X,  STATUS_ICON_Y, &BmpButtonCancel);
   GTextAt( STATUS_TEXT_X, STATUS_TEXT_Y);
   cputs( STR_BTN_CANCEL);
   // right buttons
   GBitmap( G_WIDTH - 20, STATUS_ICON_Y, &BmpButtonDown);
   GBitmap( G_WIDTH - 40, STATUS_ICON_Y, &BmpButtonUp);
   GSetColor( DCOLOR_DEFAULT);
} // DLayoutStatusMoveVertical
