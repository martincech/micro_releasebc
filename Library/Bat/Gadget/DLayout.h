//******************************************************************************
//
//  DGrid.h        Display grid
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DLayout_H__
   #define __DLayout_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#ifndef __Graphic_H__
   #include "Graphic/Graphic.h"
#endif

#define DLAYOUT_TITLE_H   20
#define DLAYOUT_STATUS_H  21

void DLayoutTitle( TUniStr Caption);
// Display window <Title>

void DLayoutTitleLTRT( TUniStr Center, TUniStr Left, TUniStr Right);
// Display window <Title> with left and right texts next to it

void DLayoutTitleLTRI( TUniStr Caption, TUniStr Left, const TBitmap *RightIcon);
// Display window <Title> with left text and right icon next to it

void DLayoutTitleLIRI( TUniStr Caption, const TBitmap *LeftIcon, const TBitmap *RightIcon);
// Display window <Title> with left icon and right icon next to it

void DLayoutTitleLIRT( TUniStr Caption, const TBitmap *LeftIcon, TUniStr Right);
// Display window <Title> with left icon and right text next to it

void DLayoutStatus( TUniStr Left, TUniStr Right, const TBitmap *RightIcon);
// Draw status line

void DLayoutStatusWithVertical( TUniStr Left, TUniStr Right, const TBitmap *RightIcon);
// Draw status line with right and left images and texts + cursor keys (vertical)

void DLayoutStatusMove( void);
// Draw status line with all cursor keys

void DLayoutStatusMoveVertical( void);
// Draw status line with vertical cursor keys

#endif
