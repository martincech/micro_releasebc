//******************************************************************************
//
//   DTimeRange.h     Display time range selector and edit
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "DTimeRange.h"
#include "Console/conio.h"        // Console
#include "Gadget/DMsg.h"          // Confirmation
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DCursor.h"       // List Cursor
#include "Gadget/DList.h"         // List utility
#include "Gadget/DTime.h"         // Event manager
#include "Gadget/DLayout.h"       // Display layout
#include "Gadget/DInput.h"        // Display input
#include "Gadget/DEvent.h"        // Event manager
#include "Sound/Beep.h"                  // Sounds
#include "Str.h"                  // Strings

static DefMenu( TimeRangeEditMenu)
   STR_FROM,
   STR_TO,
EndMenu()

typedef enum {
   MI_FROM,
   MI_TO
} EWeighingTimeMenu;

// Local functions
static byte _TimeSelect(TUniStr Caption, TTimeRange **Ranges, int TimesCount);
// Select item from WeighingTimes list
static void _DisplayRange( TListCursor *Cursor, TTimeRange *Ranges);
// Display contact page
static void TimeParameters( int Index, int y, TTimeRange *Parameters);
// Draw weighing time parameters

#define TIMES_COUNT_MAX    20

//------------------------------------------------------------------------------
//  Edit
//------------------------------------------------------------------------------

void DTimeRangeEdit( TUniStr Caption, TTimeRange *Times, int TimesCount)
// Show list of time ranges with edit capability
{
byte Index;
TMenuData  MData;
UTime      Time;

   DMenuClear( MData);
   forever{
      Index = _TimeSelect( Caption, &Times, TimesCount);
      if( Index > TimesCount){
         return;
      }

      // selection :
      if( !DMenu( Caption, TimeRangeEditMenu, (TMenuItemCb *)TimeParameters, &Times[Index], &MData)){
         continue;
      }
      switch( MData.Item){
         case MI_FROM :
            uTime( &Time, Times[Index].From);
            if( !DInputTime( STR_ENTER_TIME, STR_FROM, &Time)){
               break;
            }
            Times[Index].From = uTimeGauge( &Time);
            break;

         case MI_TO :
            uTime( &Time, Times[Index].To);
            if( !DInputTime( STR_ENTER_TIME, STR_TO, &Time)){
               break;
            }
            Times[Index].To = uTimeGauge( &Time);
            break;
      }
   }
}

//------------------------------------------------------------------------------
//  Delete
//------------------------------------------------------------------------------

void DTimeRangeDelete(TUniStr Caption, TTimeRange *Times, int *TimesCount)
// Show list of time ranges with delete capability, return selected delete index or -1 when cancel
{
byte Index;
TTimeRange *ActiveIndices[TIMES_COUNT_MAX];
// #warning Je nutne vytvaret kopii ?
int i;

   for(i = 0; i < *TimesCount; i++){
      ActiveIndices[i] = &Times[i];
   }
   forever{
      if(*TimesCount == 0){
         return;
      }
      Index = _TimeSelect( Caption, ActiveIndices, *TimesCount);
      if( (Index > *TimesCount)){
         return;
      }
      if( !DMsgYesNo( STR_CONFIRMATION, STR_REALLY_DELETE, 0)){
         continue;
      }
      if( Index <= *TimesCount - 1){
         memmove( &ActiveIndices[Index], &ActiveIndices[Index+1], (*TimesCount - Index) * (sizeof(TTimeRange*)));
      }
      (*TimesCount)--;
   }
}

//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------

void DTimeRangeCreate( TTimeRange *Time)
// Create new time, Time will be unmodified when cancel selected
{
UTime      nTime;
TTimeRange TimeCopy;

   memcpy( &TimeCopy, Time, sizeof( TTimeRange));
   forever{
      uTime( &nTime, Time->From);
      if( !DInputTime( STR_ENTER_TIME, STR_FROM, &nTime)){
         break;
      }
      Time->From = uTimeGauge( &nTime);

      uTime( &nTime, Time->To);
      if( !DInputTime( STR_ENTER_TIME, STR_TO, &nTime)){
         continue;
      }
      Time->To = uTimeGauge( &nTime);
      return;
   }
   memcpy( Time, &TimeCopy, sizeof( TTimeRange));
}

//------------------------------------------------------------------------------
//   Select
//------------------------------------------------------------------------------

static byte _TimeSelect( TUniStr Caption, TTimeRange **Ranges, int TimesCount)
// Select item from WeighingTimes list
{
TListCursor Cursor;

   DCursorInit( &Cursor, TimesCount, DLIST_ROWS_COUNT_MAX);
   DCursorIndexSet( &Cursor, 0);                  // move cursor at initial position
   DCursorUseGridSet( &Cursor, NO);               // don't display horizontal lines
   forever {
      if( DCursorPageChanged( &Cursor)){
         // redraw page
         GClear();                                          // clear display
         DLayoutTitle( Caption);                            // display title
         DLayoutStatus( STR_BTN_CANCEL, STR_BTN_SELECT, 0); // display status line
      }
      if( DCursorRowChanged( &Cursor)){
         _DisplayRange( &Cursor, *Ranges);
         DCursorRowUpdate( &Cursor);
         GFlush();                                // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            DCursorRowUp( &Cursor);
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            DCursorRowDown( &Cursor);
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            DCursorPageDown( &Cursor);
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            DCursorPageUp( &Cursor);
            break;

         case K_ENTER :
            return( (byte)DCursorIndex( &Cursor));

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( TimesCount + 1);
      }
   }
} // _WeighingTimeSelect

//------------------------------------------------------------------------------
//   Display page
//------------------------------------------------------------------------------

#define TIME_FROM_X    10
#define TIME_SPACE_X   60
#define TIME_TO_X      80

static void _DisplayRange( TListCursor *Cursor, TTimeRange *Ranges)
// Display contact page
{
int           RowY;
int           i;
byte          Index;

   DListClear( Cursor);                // clear list area
   // draw points
   for( i = 0; i < DCursorRowCount( Cursor); i++){
      RowY = DListY( i);
      if( !DListCursor( Cursor, i)){
         continue;
      }
      // get item data :
      Index = DCursorPage( Cursor) + i;
      // from / to :
      DTimeShort( Ranges[Index].From, TIME_FROM_X, RowY);
      GTextAt( TIME_SPACE_X, RowY);
      cputch( '-');
      DTimeShort( Ranges[Index].To,   TIME_TO_X,   RowY);
   }
} // DisplayPage

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void TimeParameters( int Index, int y, TTimeRange *Parameters)
// Draw weighing time parameters
{
   switch( Index){
      case MI_FROM :
         DTimeShortRight( Parameters->From, DMENU_PARAMETERS_X, y);
         break;

      case MI_TO :
         DTimeShortRight( Parameters->To, DMENU_PARAMETERS_X, y);
         break;

   }
} // WeighingTimeParameters
