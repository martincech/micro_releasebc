//******************************************************************************
//
//   DTimeRange.h     Display time range selector and edit
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DTimeRange_H__
#define __DTimeRange_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#include "Time/uTime.h"

void DTimeRangeEdit( TUniStr Caption, TTimeRange *Times, int TimesCount);
// Show list of time ranges with edit capability

void DTimeRangeDelete(TUniStr Caption, TTimeRange *Times, int *TimesCount);
// Show list of time ranges with delete capability, <TimesCount> is I/O

void DTimeRangeCreate(TTimeRange *Time);
// Create new time, Time will be unmodified when cancel selected

#endif // __DTimeRange_H__


