//******************************************************************************
//
//  bTime.c        Print Date/Time to buffer
//  Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "bTime.h"
#include "Console/conio.h"         // putchar
#include "Country/xTime.h"         // date & time
#include "Console/sputchar.h"

//------------------------------------------------------------------------------
//  Time
//------------------------------------------------------------------------------

void bTime( char *Buffer, UTimeGauge Now)
// Display time only
{
   sputcharbuffer( Buffer);
   xTime( sputchar, Now);
   sputchar( '\0');                    // string terminator
} // bTime

//------------------------------------------------------------------------------
//  Time short
//------------------------------------------------------------------------------

void bTimeShort( char *Buffer, UTimeGauge Now)
// Display time without seconds
{
   sputcharbuffer( Buffer);
   xTimeShort( sputchar, Now);
   sputchar( '\0');                    // string terminator
} // bTimeShort

//------------------------------------------------------------------------------
//  Date
//------------------------------------------------------------------------------

void bDateFormated(char *Buffer, UDateGauge Now, EDateFormat dateFormat, char Separator1, char Separator2)
{
   sputcharbuffer( Buffer);
   xDateFormated( sputchar, Now, dateFormat, Separator1, Separator2);
   sputchar( '\0');                    // string terminator

}

void bDate( char *Buffer, UDateGauge Now)
// Display date only
{
   bDateFormated(Buffer, Now, CountryDateFormat(), CountryDateSeparator1(), CountryDateSeparator2());
} // bDate

//------------------------------------------------------------------------------
//  Date & Time
//------------------------------------------------------------------------------

void bDateTime( char *Buffer, UDateTimeGauge Now)
// Display date and time
{
   sputcharbuffer( Buffer);
   xDateTime( sputchar, Now);
   sputchar( '\0');                    // string terminator
} // bDateTime

//------------------------------------------------------------------------------
//  Date & Time short
//------------------------------------------------------------------------------

void bDateTimeShort( char *Buffer, UDateTimeGauge Now)
// Display date and time without seconds
{
   sputcharbuffer( Buffer);
   xDateTime( sputchar, Now);
   sputchar( '\0');                    // string terminator
} // bDateTimeShort
