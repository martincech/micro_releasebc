//******************************************************************************
//
//   CountrySet.h  Country set definitions
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef __CountrySet_H__
   #ifndef _MANAGED
   #define __CountrySet_H__
   #endif

// Country :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class CountryE : byte{
#else
typedef enum {
#endif
   COUNTRY_INTERNATIONAL,
   COUNTRY_ALBANIA,
   COUNTRY_ALGERIA,
   COUNTRY_ARGENTINA,
   COUNTRY_AUSTRALIA,
   COUNTRY_AUSTRIA,
   COUNTRY_BANGLADESH,
   COUNTRY_BELARUS,
   COUNTRY_BELGIUM,
   COUNTRY_BOLIVIA,
   COUNTRY_BRAZIL,
   COUNTRY_BULGARIA,
   COUNTRY_CANADA,
   COUNTRY_CHILE,
   COUNTRY_CHINA,
   COUNTRY_COLOMBIA,
   COUNTRY_CYPRUS,
   COUNTRY_CZECH,
   COUNTRY_DENMARK,
   COUNTRY_ECUADOR,
   COUNTRY_EGYPT,
   COUNTRY_ESTONIA,
   COUNTRY_FINLAND,
   COUNTRY_FRANCE,
   COUNTRY_GERMANY,
   COUNTRY_GREECE,
   COUNTRY_HUNGARY,
   COUNTRY_INDIA,
   COUNTRY_INDONESIA,
   COUNTRY_IRAN,
   COUNTRY_IRELAND,
   COUNTRY_ISRAEL,
   COUNTRY_ITALY,
   COUNTRY_JAPAN,
   COUNTRY_JORDAN,
   COUNTRY_LATVIA,
   COUNTRY_LEBANON,
   COUNTRY_LITHUANIA,
   COUNTRY_LUXEMBOURG,
   COUNTRY_MALAYSIA,
   COUNTRY_MALTA,
   COUNTRY_MEXICO,
   COUNTRY_MONGOLIA,
   COUNTRY_MOROCCO,
   COUNTRY_NEPAL,
   COUNTRY_NETHERLANDS,
   COUNTRY_NEW_ZEALAND,
   COUNTRY_NIGERIA,
   COUNTRY_NORWAY,
   COUNTRY_PAKISTAN,
   COUNTRY_PARAGUAY,
   COUNTRY_PERU,
   COUNTRY_PHILIPPINES,
   COUNTRY_POLAND,
   COUNTRY_PORTUGAL,
   COUNTRY_ROMANIA,
   COUNTRY_RUSSIA,
   COUNTRY_SLOVAKIA,
   COUNTRY_SLOVENIA,
   COUNTRY_SOUTH_AFRICA,
   COUNTRY_SOUTH_KOREA,
   COUNTRY_SPAIN,
   COUNTRY_SWEDEN,
   COUNTRY_SWITZERLAND,
   COUNTRY_SYRIA,
   COUNTRY_THAILAND,
   COUNTRY_TUNISIA,
   COUNTRY_TURKEY,
   COUNTRY_UKRAINE,
   COUNTRY_UK,
   COUNTRY_USA,
   COUNTRY_URUGUAY,
   COUNTRY_VENEZUELA,
   COUNTRY_VIETNAM,
#ifndef _MANAGED
   _COUNTRY_COUNT
}ECountry;
#else
   };
}
#endif

// Language :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class LanguageE{
#else
typedef enum {
#endif
   LNG_CZECH,
   LNG_DUTCH,
   LNG_ENGLISH,
   LNG_FINNISH,
   LNG_FRENCH,
   LNG_GERMAN,
   LNG_HUNGARIAN,
   LNG_ITALIAN,
   LNG_JAPANESE,
   LNG_POLISH,
   LNG_PORTUGUESE,
   LNG_RUSSIAN,
   LNG_SPANISH,
   LNG_TURKISH,
#ifndef _MANAGED
   _LNG_COUNT
}ELanguage;
#else
   };
}
#endif

// Code pages :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class CodePageE{
#else
typedef enum {
#endif
   CPG_LATIN,
   CPG_JAPAN,
#ifndef _MANAGED
   _CPG_COUNT
}ECodePage;
#else
   };
}
#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "CountrySet.h"
   #define _MANAGED
#endif

#endif // __CountrySet_H__
