//******************************************************************************
//
//  xTime.h        putchar based display date & time
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __xTime_H__
   #define __xTime_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __uDateTime_H__
   #include "Time/uDateTime.h"
#endif

#ifndef __CountryDef_H__
   #include "Country/CountryDef.h"
#endif

#ifndef __PutcharDef_H__
   #include "Console/PutcharDef.h"
#endif

#define XDATE_MONTH_SIZE   3        // month shortcut size

void xTime( TPutchar *xputchar, UTimeGauge Now);
// Display time only

int xTimeWidth( void);
// Returns character width of time

void xTimeShort( TPutchar *xputchar, UTimeGauge Now);
// Display time without seconds

int xTimeShortWidth( void);
// Returns character width of time

const char *xTimeSuffix( ETimeSuffix Suffix);
// Returns suffix by <Suffix> enum

void xDateFormated( TPutchar *xputchar, UDateGauge Now, EDateFormat dateFormat, char Separator1, char Separator2);
// Display date only by specific format

void xDate(TPutchar *xputchar, UDateGauge Now);
// Display date only

int xDateWidth( void);
// Returns character width of date

void xDateTime( TPutchar *xputchar, UDateTimeGauge Now);
// Display date and time

int xDateTimeWidth( void);
// Returns character width of date time

void xDateTimeShort( TPutchar *xputchar, UDateTimeGauge Now);
// Display date and time without seconds

int xDateTimeShortWidth( void);
// Returns character width of date time

const char *xDateMonth( UMonth Month);
// Returns <Month> shortcut

#endif
