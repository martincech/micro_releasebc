//******************************************************************************
//
//  bTime.h        Print Date/Time to buffer
//  Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __bTime_H__
   #define __bTime_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __uDateTime_H__
   #include "Time/uDateTime.h"
#endif

void bTime( char *Buffer, UTimeGauge Now);
// Display time only

void bTimeShort( char *Buffer, UTimeGauge Now);
// Display time without seconds

void bDate( char *Buffer, UDateGauge Now);
// Display date only formated by Country settings

void bDateFormated(char *Buffer, UDateGauge Now, EDateFormat dateFormat, char Separator1, char Separator2);
// Display date only formated by specific format


void bDateTime( char *Buffer, UDateTimeGauge Now);
// Display date and time

void bDateTimeShort( char *Buffer, UDateTimeGauge Now);
// Display date and time without seconds

#endif
