//*****************************************************************************
//
//    uconio.c     UART simple formatted output
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Hardware.h"           // conditional compilation
#include "uconio.h"
#include "xprint.h"
#include "Uart/Uart.h"

#include <stdarg.h>


//-----------------------------------------------------------------------------
// Character
//-----------------------------------------------------------------------------

void uputch( char ch)
// character output
{
   uputchar( ch);
} // uputch

//-----------------------------------------------------------------------------
// String
//-----------------------------------------------------------------------------

void uputs( const char *String)
// string output
{
   xputs( uputchar, String);
} // uputs

//-----------------------------------------------------------------------------
// Hexadecimal
//-----------------------------------------------------------------------------

void uprinthex( dword x, dword Flags)
// hexadecimal output
{
   xprinthex( uputchar, x, Flags);
} // uprinthex

//-----------------------------------------------------------------------------
// Decadic
//-----------------------------------------------------------------------------

void uprintdec( int32 x, dword Flags)
// decadic number output
{
   xprintdec( uputchar, x, Flags);
} // uprintdec

//-----------------------------------------------------------------------------
// Float
//-----------------------------------------------------------------------------

void ufloat( int x, int w, int d, dword Flags)
// floating number output
{
   xfloat( uputchar, x, w, d, Flags);
} // ufloat

//-----------------------------------------------------------------------------
// Formatted
//-----------------------------------------------------------------------------

void uprintf( const char *Format, ...)
// simple formatted output
{
va_list Arg;

   va_start( Arg, Format);
   xvprintf( uputchar, Format, Arg);
   va_end( Arg);
} // uprintf

//------------------------------------------------------------------------------
// uputchar
//------------------------------------------------------------------------------

int uputchar( int ch)
// output character
{
   if( ch == '\n'){
      UartTxChar( UCONIO_UART, '\r');  // add carriage return
   }
   UartTxChar( UCONIO_UART, ch);
   return( ch);   
} // uputchar
