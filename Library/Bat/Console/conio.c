//*****************************************************************************
//
//    conio.c - simple formatted output
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"           // podminena kompilace
#include "conio.h"
#include "xprint.h"

#ifdef PRINTF_BUFFER
   #include "sputchar.h"
#endif

#ifdef CONIO_FLUSH
   #include "Graphic/Graphic.h"
   #define Flush()  GFlush()
#else
   #define Flush()
#endif

//-----------------------------------------------------------------------------
// Znak
//-----------------------------------------------------------------------------

void cputch( char ch)
// vystup znaku <ch>
{
   cputchar( ch);
   Flush();
} // cputch

//-----------------------------------------------------------------------------
// Retezec
//-----------------------------------------------------------------------------

void cputs( const char *string)
// vystup retezce <string>
{
   xputs( cputchar, string);
   Flush();
} // cputs

//-----------------------------------------------------------------------------
// Retezec
//-----------------------------------------------------------------------------

void cputsn( const char *string)
// vystup retezce <string> az po LF
{
   xputsn( cputchar, string);
   Flush();
} // cputs

//-----------------------------------------------------------------------------
// Hexadecimalni cislo
//-----------------------------------------------------------------------------

void cprinthex( dword x, dword flags)
// vystup hexa cisla <x>, <flags> koduje sirku
{
   xprinthex( cputchar, x, flags);
   Flush();
} // cprinthex

//-----------------------------------------------------------------------------
// Dekadicke cislo
//-----------------------------------------------------------------------------

void cprintdec( int32 x, dword flags)
// vystup dekadickeho cisla <x>, <flags> koduje sirku
{
   xprintdec( cputchar, x, flags);
   Flush();
} // cprintdec

//-----------------------------------------------------------------------------
// Float
//-----------------------------------------------------------------------------

void cfloat( int x, int w, int d, dword flags)
// Tiskne binarni cislo <x> jako float s celkovou sirkou <w>
// a <d> desetinnymi misty. <flags> jsou doplnujici atributy
// (FMT_PLUS)
{
   xfloat( cputchar, x, w, d, flags);
   Flush();
} // cfloat

//-----------------------------------------------------------------------------
// Formatovany vystup
//-----------------------------------------------------------------------------

void cprintf( const char *Format, ...)
// jednoduchy formatovany vystup
{
va_list Arg;

   va_start( Arg, Format);
   xvprintf( cputchar, Format, Arg);
   va_end( Arg);
   Flush();
} // cprintf

#ifdef PRINTF_BUFFER
//-----------------------------------------------------------------------------
// Formatovany vystup do bufferu
//-----------------------------------------------------------------------------

void bprintf( char *Buffer, const char *Format, ...)
// formatovany vystup do <Bufferu>
{
va_list Arg;

   va_start( Arg, Format);
   bvprintf( Buffer, Format, Arg);
   va_end( Arg);
} // sprintf

void bvprintf( char *Buffer, const char *Format, va_list Arg)
// formatovany vystup do <Buffer>
{
   sputcharbuffer( Buffer);
   xvprintf( sputchar, Format, Arg);
   sputchar( '\0');                    // string terminator
} // bvprintf

#endif
