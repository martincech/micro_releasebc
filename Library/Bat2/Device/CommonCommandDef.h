//******************************************************************************
//
//   CommonCommandDef.h    Common cmds
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#ifndef __CommonCommandDef_H__
   #define __CommonCommandDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __VersionDef_H__
   #include "Device/VersionDef.h"
#endif

#define BROADCAST_ADDRESS     0xFF

//------------------------------------------------------------------------------
//  Command code
//------------------------------------------------------------------------------

typedef enum {
   COMMON_CMD_DISCOVER = 0xFF,
   COMMON_CMD_ADDRESS_ASSIGN = 0xFE,
   _COMMON_CMD_FIRST = 0xFE
} ECommonCommand;   

//------------------------------------------------------------------------------
//  Command data
//------------------------------------------------------------------------------

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

typedef struct {
   TDeviceVersion Version;
   byte           Address;
} __packed TCommonAddressAssign;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif

//------------------------------------------------------------------------------
//  Command Data union
//------------------------------------------------------------------------------
   
typedef union {
   TCommonAddressAssign AddressAssign;
} TCommonCommandUnion;

//------------------------------------------------------------------------------
//  Command
//------------------------------------------------------------------------------

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

typedef struct {
   byte                Command;
   TCommonCommandUnion Data;
} __packed TCommonCommand;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif

#define CommonSimpleCommandSize() (1)
#define CommonCommandSize( Item)  (1 + sizeof( TCommon##Item))

//******************************************************************************

//------------------------------------------------------------------------------
//  Reply code
//------------------------------------------------------------------------------

// almost same as command :
#define CommonReply( Command)   ((Command) | 0x80)

//------------------------------------------------------------------------------
//  Reply data
//------------------------------------------------------------------------------

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

typedef struct {
   TDeviceVersion Version;
} __packed TCommonDiscoverReply;

//------------------------------------------------------------------------------
//  Reply Data union
//------------------------------------------------------------------------------

typedef union {
   TCommonDiscoverReply       Discover;
} __packed TCommonReplyUnion;

//------------------------------------------------------------------------------
//  Reply 
//------------------------------------------------------------------------------

typedef struct {
   byte                Reply;
   TCommonReplyUnion Data;
} __packed TCommonReply;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif

#define CommonSimpleReplySize() (1)
#define CommonReplySize( Item)  (1 + sizeof( TCommon##Item))

#endif
