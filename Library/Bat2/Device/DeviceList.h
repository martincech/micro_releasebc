//******************************************************************************
//
//   DeviceList.h    Device list
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef __DeviceList_H__
   #define __DeviceList_H__

#ifndef __Device_H__
   #include "Device.h"
#endif

void DeviceListInit( void);
// Initialize

byte DeviceListCount( void);
// Returns actual list items count

TDeviceIndex DeviceListCapacity( void);
// Returns maximum list items count

TDeviceIndex DeviceListAdd( TDevice *Device);
// Add <Device> returns index

void DeviceListDelete( TDeviceIndex Index);
// Delete by <Index>

void DeviceListLoad( TDevice *Device, TDeviceIndex Index);
// Load <Device> by <Index>

void DeviceListSave( TDevice *Device, TDeviceIndex Index);
// Save <Device> at <Index>

#endif
