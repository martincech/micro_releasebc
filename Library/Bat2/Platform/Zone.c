//******************************************************************************
//
//   Zone.c       Weighing zone control
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Platform/Zone.h"
#include "System/System.h"
#include "Platform/Wepl.h"
#include "Storage/Sample.h"
#include "Statistic/Calculate.h"
#include "Config/Config.h"
#include "Weighing/WeighingConfiguration.h"
#include "Weighing/Statistics.h"
#include "Weighing/Correction.h"
#include "Device/DeviceList.h"
#if defined(OPTION_BAT2_COMPACT) && !defined(OPTION_SIMULATION)
#include "Weighing/pWeighing.h"
#endif

#define PLATFORM_ADDRESS        0x02   // current platform communication address
#define __PLATFORM_STOP_FLUSH__ 1      // flush data before stop

typedef enum {
   ZONE_STATUS_UNDEFINED,
   ZONE_STATUS_STOP,
   ZONE_STATUS_WEIGHING,
   ZONE_STATUS_CALIBRATION,
   ZONE_STATUS_DIAGNOSTICS,
   _ZONE_SATUS_LAST
} EZoneStatus;

static byte             _ZoneStatus;
static TPlatformStatus  _Status;
static TWeightGauge     _ActualWeight;
static TWeightGauge     _LastWeight;
static byte             _LastFlag;

static TPlatformWeight  _Samples[ PLATFORM_SAVED_WEIGHT_COUNT];

static byte DeviceCount;
static byte CurrentDevice;

static TCalculate _Calculate;
static TCalculate _CalculateFemale;
static TStatistic _Statistic;
static TStatistic _StatisticFemale;
static TYesNo     _HasFemale;
static TYesNo     _HasMale;

//------------------------------------------------------------------------------



//------------------------------------------------------------------------------

// Local functions :

static void _WeighingRestore( void);
// Restore weighing

static void _PlatformConnect( void);
// Prepare data for platform connection

static TYesNo _PlatformStart( void);
// Restart weighing

static TYesNo _PlatformStop( void);
// Stop weighing

static TYesNo _PlatformCalibration( void);
// Start calibration

static TYesNo _PlatformSamplesGet( void);
// Get weighing samples

static TYesNo _PlatformStatusGet( void);
// Get platform status

static void _PlatformParameters( void);
// Calculate platform parameters

static TYesNo _DiagnosticDataGet( void);
// Get diagnostic data

static void _OnlineStatisticClear( void);
// Clear on-line statistic

static void _OnlineStatisticRecalculate( void);
// Recalculate on-line statistic

static void _OnlineStatisticAppend( TWeightGauge Weight, TSampleFlag SampleFlag);
// Append sample with <Weight> and <SampleFlag> to statistic

static void _OnlineStatisticUpdate( void);
// Update statistics after append

static void _SampleWeightCorrection( TSample *Sample);
// Calculates <Sample> weight correction

static TYesNo _IsFemale( TSampleFlag Flag);
// Check for female storage

//------------------------------------------------------------------------------
//  Initialization
//------------------------------------------------------------------------------

void ZoneInit( void)
// Initialize
{
#ifndef __WIN32__
   WeplInit();                         // initialize platform communication
#endif   
#if defined(OPTION_BAT2_COMPACT) && !defined(OPTION_SIMULATION)
   pWeighingInit(); // platform weighing
#endif
   SampleInit();                       // restore samples FIFO
   _ActualWeight = 0;                  // set actual weight
   _PlatformConnect();                 // prepare for connection
   _ZoneStatus = ZONE_STATUS_STOP;
} // ZoneInit

//------------------------------------------------------------------------------
//   Resume
//------------------------------------------------------------------------------

void ZoneResume( void)
// Resume weighing data after power on
{
   _WeighingRestore();                 // restore last sample & statistics
   _PlatformParameters();              // recalculate platform parameters
} // ZoneResume

//------------------------------------------------------------------------------
//   Execute
//------------------------------------------------------------------------------

void ZoneExecute( void)
// Execute weighing
{
#if defined(OPTION_BAT2_COMPACT) && !defined(OPTION_SIMULATION)
   pWeighingExecute();
#endif
   if( _ZoneStatus == ZONE_STATUS_UNDEFINED){
      return;                          // no command yet
   }
   // check for calibration :
   if( _ZoneStatus == ZONE_STATUS_CALIBRATION){
      _Status.Operation = PLATFORM_OPERATION_CALIBRATION;
      _Status.Error     = PLATFORM_ERROR_OK;
      _ActualWeight     = 0;
      return;                          // don't communicate
   }
   // read device status :
   if( !_PlatformStatusGet()){
      return;                          // communication break
   }
   // check for stopped state :
   if( _ZoneStatus == ZONE_STATUS_STOP){
      // check for platform state :
      if( _Status.Operation != PLATFORM_OPERATION_STOP){
         _PlatformStop();              // try stop weighing
      }
      return;                          // weighing stopped
   }
   if( _ZoneStatus != ZONE_STATUS_WEIGHING && _ZoneStatus != ZONE_STATUS_DIAGNOSTICS){
      return;                          //!!! other states
   }
   // check for platform state :
   if( _Status.Operation != PLATFORM_OPERATION_WEIGHING && _Status.Operation != PLATFORM_OPERATION_DIAGNOSTICS){
      _PlatformStart();
      return;                          // wait for start
   }
   // check for diagnostics :
   if( _ZoneStatus == ZONE_STATUS_DIAGNOSTICS){
      _DiagnosticDataGet();
      return;
   }
   // get actual weight & samples :
   if( _Status.SamplesCount == 0){
      return;                          // no actual samples
   }
   if( !_PlatformSamplesGet()){
      return;
   }
} // ZoneExecute

//------------------------------------------------------------------------------
//   Start
//------------------------------------------------------------------------------

TYesNo ZoneStart( void)
// Start weighing
{
   _ZoneStatus = ZONE_STATUS_WEIGHING;
   CurrentDevice = 0;
   return YES;
} // ZoneStart

//------------------------------------------------------------------------------
//   Stop
//------------------------------------------------------------------------------

TYesNo ZoneStop( void)
// Stop weighing
{
   _ZoneStatus = ZONE_STATUS_STOP;
   return YES;
} // ZoneStop

//------------------------------------------------------------------------------
//   Calibration
//------------------------------------------------------------------------------

TYesNo ZoneCalibration( void)
// Start calibration
{
   _ZoneStatus = ZONE_STATUS_CALIBRATION;
   return( _PlatformCalibration());
} // ZoneCalibration

//------------------------------------------------------------------------------
//  Diagnostics
//------------------------------------------------------------------------------

TYesNo ZoneDiagnostics( void)
// Run diagnostics
{
   _ZoneStatus = ZONE_STATUS_DIAGNOSTICS;
   return YES;
} // ZoneDiagnostics

//------------------------------------------------------------------------------
//  Clear
//------------------------------------------------------------------------------

void ZoneClear( void)
// Clear samples FIFO and statistics
{
TSamples Samples;
   while( !SampleOpen( &Samples));
   SampleClear( &Samples);                      // clear samples FIFO
   SampleClose( &Samples);
   _OnlineStatisticClear();            // clear statistics
   // clear last weight :
   _LastWeight       = 0;
   _LastFlag         = 0;
} // ZoneClear

//------------------------------------------------------------------------------
//  Operation
//------------------------------------------------------------------------------

byte ZoneOperation( void)
// Returns platform current operation
{
   return( _Status.Operation);
} // WeighingOperation

//------------------------------------------------------------------------------
//  Error
//------------------------------------------------------------------------------

byte ZoneError( void)
// Returns platform last error
{
   return( _Status.Error);
} // WeighingError

//------------------------------------------------------------------------------
//  Weight
//------------------------------------------------------------------------------

TWeightGauge ZoneActualWeight( void)
// Returns platform actual weight
{
   return( _ActualWeight);
} // WeighingActualWeight

//------------------------------------------------------------------------------
//  Samples count
//------------------------------------------------------------------------------

int ZoneSamplesCount( void)
// Returns platform saved samples count
{
   return( SampleCount());
} // WeighingSamplesCount

//------------------------------------------------------------------------------
//  Last weight
//------------------------------------------------------------------------------

TWeightGauge ZoneLastWeight( void)
// Returns platform last saved weight
{
   return( _LastWeight);
} // WeighingLastWeight

//------------------------------------------------------------------------------
//  Last flag
//------------------------------------------------------------------------------

byte ZoneLastFlag( void)
// Returns platform last saved weight flag
{
   return( _LastFlag);
} // WeighingLastFlag

//------------------------------------------------------------------------------
//  Statistic
//------------------------------------------------------------------------------

TStatistic *ZoneStatistic( void)
// Returns current statistics
{
   return( &_Statistic);
} // WeighingStatistic

//------------------------------------------------------------------------------
//  Histogram
//------------------------------------------------------------------------------

THistogram *ZoneHistogram( void)
// Returns current histogram
{
   return( CalculateHistogram( &_Calculate));
} // WeighingHistogram

//------------------------------------------------------------------------------
//  Statistic female
//------------------------------------------------------------------------------

TStatistic *ZoneStatisticFemale( void)
// Returns current statistics
{
   return( &_StatisticFemale);
} // WeighingStatisticFemale

//------------------------------------------------------------------------------
//  Histogram female
//------------------------------------------------------------------------------

THistogram *ZoneHistogramFemale( void)
// Returns current histogram
{
   return( CalculateHistogram( &_CalculateFemale));
} // ZoneHistogramFemale

//******************************************************************************

//------------------------------------------------------------------------------
//  Weighing restore
//------------------------------------------------------------------------------

static void _WeighingRestore( void)
// Restore weighing
{
TSampleIndex Index;
TSample      Sample;
TSamples Samples;
   _LastWeight   = 0;
   _LastFlag     = 0;
   _OnlineStatisticRecalculate();           // recalculate statistics
   if( SampleCount() == 0){
      return;                               // no samples saved
   }
   // get last sample :
   while( !SampleOpen( &Samples));
   Index = SampleCount() - 1;
   SampleGet( &Samples, Index, &Sample);
   SampleClose( &Samples);
   _LastWeight = SampleWeight( &Sample);
   _LastFlag   = SampleFlag( &Sample);
} // _WeighingRestore

//------------------------------------------------------------------------------
//  Connect
//------------------------------------------------------------------------------

static void _PlatformConnect( void)
// Prepare data for platform connection
{
   _Status.Operation = PLATFORM_OPERATION_UNDEFINED;
   _Status.Error     = PLATFORM_ERROR_BREAK;
   _ActualWeight     = 0;
} // _PlatformConnect

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

static TYesNo _PlatformStart( void)
// Restart weighing
{
   // stop weighing :
   if( !WeplStop()){
      _PlatformConnect();
      return( NO);
   }
   // get version data :
   if( !WeplVersionGet( &PlatformVersion)){
      _PlatformConnect();
      return( NO);
   }
   // calculate parameters :
   _PlatformParameters();
   // set parameters :
//   if( PlatformVersion.Device.Modification == DEVICE_MODIFICATION_PICOSTRAIN){
//      if( !WeplPicostrainSet( &PlatformPicostrain)){
//         _PlatformConnect();
//         return( NO);
//      }
//   } else {
      if( !WeplSigmaDeltaSet( &PlatformSigmaDelta)){
         _PlatformConnect();
         return( NO);
      }
//   }
   if( !WeplDetectionSet( &PlatformDetection)){
      _PlatformConnect();
      return( NO);
   }
   if( !WeplAcceptanceSet( &PlatformAcceptance)){
      _PlatformConnect();
      return( NO);
   }
   if( !WeplClockSet( SysClock())){
      _PlatformConnect();
      return( NO);
   }
   // start weighing :
   if(_ZoneStatus == ZONE_STATUS_DIAGNOSTICS) {
      if( !WeplDiagnosticStart()){
         _PlatformConnect();
         return( NO);
      }
   } else {
      if( !WeplWeighingStart()){
         _PlatformConnect();
         return( NO);
      }
   }
   //SysDelay( 800);                     // wait for operation change
   return( YES);
} // _PlatformStart

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

static TYesNo _PlatformStop( void)
// Stop weighing
{
   if(_ZoneStatus == ZONE_STATUS_WEIGHING) {
#ifdef __PLATFORM_STOP_FLUSH__
      // flush samples :
      if( !_PlatformSamplesGet()){
         _PlatformConnect();
         return( NO);
      }
#endif // __PLATFORM_STOP_FLUSH__
   }
   // stop weighing :
   _ActualWeight = 0;
   if( !WeplStop()){
      _PlatformConnect();
      return( NO);
   }
   return( YES);
} // _PlatformStop

//------------------------------------------------------------------------------
//  Calibration
//------------------------------------------------------------------------------

static TYesNo _PlatformCalibration( void)
// Start calibration
{
   if( !WeplCalibrationSet( &PlatformCalibration)){
      _PlatformConnect();
      return( NO);
   }
   return( YES);
} // _PlatformCalibration

//------------------------------------------------------------------------------
//  Samples get
//------------------------------------------------------------------------------

static TYesNo _PlatformSamplesGet( void)
// Get weighing samples
{
TSample Sample;
TSamples Samples;
int     i;
word    Count;
   forever {
      if( !WeplSavedWeightGet( &Count, _Samples)){
         _PlatformConnect();
         return( NO);
      }
      if( !WeplSavedWeightConfirm( Count)){
         _PlatformConnect();
         return( NO);
      }
      // check for samples count :
      if( Count == 0){
         return( YES);                    // no samples
      }
      // get samples :
      while( !SampleOpen( &Samples));
      for( i = 0; i < Count; i++){
         /*if(SampleTimestamp( &_Samples[ i]) < WeighingDayCloseAt() - WeighingDayDuration()) {
            continue;
         }
         if(SampleTimestamp( &_Samples[ i]) > WeighingDayCloseAt()) {
            continue;
         }*/
         Sample.Origin    = PLATFORM_ADDRESS;
         Sample.Timestamp = _Samples[ i].Timestamp;
         Sample.Weight    = _Samples[ i].Weight;
         _SampleWeightCorrection( &Sample);
         SampleAppend( &Samples, &Sample);
         _OnlineStatisticAppend( SampleWeight( &Sample), SampleFlag( &Sample));
      }
      SampleClose( &Samples);
      // show last sample data :
      _LastWeight = SampleWeight( &Sample);
      _LastFlag   = SampleFlag( &Sample);
      _OnlineStatisticUpdate();
      if(Count != PLATFORM_SAVED_WEIGHT_COUNT) {
         return YES;
      }
   }
} // _PlatformSamplesGet

//------------------------------------------------------------------------------
//  Status get
//------------------------------------------------------------------------------

static TYesNo _PlatformStatusGet( void)
// Get platform status
{
   // read device status :
   if( !WeplStatusGet( &_Status)){
      _PlatformConnect();
      return( NO);
   }
   _ActualWeight = _Status.Weight;
   return( YES);
} // _PlatformStatusGet

//------------------------------------------------------------------------------
//   Diagnostic data
//------------------------------------------------------------------------------

#include "Diagnostic/Diagnostic.h"

static TYesNo _DiagnosticDataGet( void)
// Get diagnostic data
{
static dword IdIn = -1;
byte Count;
TDiagnosticFrame Frame;

   if(!WeplDiagnosticWeightGet( IdIn, &IdIn, &Frame)) {
      return NO;
   }

   DiagnosticAppend(&Frame);
   return YES;
} // _DiagnosticDataGet

//------------------------------------------------------------------------------
//   Platform parameters
//------------------------------------------------------------------------------

static void _PlatformParameters( void)
// Calculate platform parameters
{
TWeightGauge Mid;
TWeightGauge WeightMale;
TWeightGauge WeightFemale;

   // ADC paramaters :
   PlatformSigmaDelta = PlatformSigmaDeltaDefault;
   PlatformPicostrain = PlatformPicostrainDefault;
   // Fine detection parameters :
   PlatformDetection.Fine.AveragingWindow  =  WeighingConfiguration.Detection.Filter;
   PlatformDetection.Fine.AbsoluteRange    = (WeighingConfiguration.Detection.StabilizationRange * WeighingContext.TargetWeight) / 1000;
   if( PlatformDetection.Fine.AbsoluteRange < PLATFORM_DETECTION_ABSOLUTE_RANGE_MIN){
      PlatformDetection.Fine.AbsoluteRange = PLATFORM_DETECTION_ABSOLUTE_RANGE_MIN;
   }
   PlatformDetection.Fine.StableWindow      = WeighingConfiguration.Detection.StabilizationTime;
   PlatformDetection.Fine.SwitchoverRange   = WEIGHT_UNITS_KG_EXT_RANGE * WEIGHT_UNITS_FACTOR; // fake maximum value
   // Coarse detection parameters :
   PlatformDetection.Coarse = PlatformDetection.Fine;  // fake same as Fine
   PlatformDetection.Coarse.SwitchoverRange = 0;       // fake minimum value
   // Acceptance parameters :
   PlatformAcceptance.Mode = PLATFORM_ACCEPTANCE_MODE_SINGLE;
   switch( WeighingSexDifferentation()){
      case SEX_DIFFERENTIATION_NO :
         PlatformAcceptance.Mode = PLATFORM_ACCEPTANCE_MODE_SINGLE;
         break;

      case SEX_DIFFERENTIATION_YES :
         PlatformAcceptance.Mode = PLATFORM_ACCEPTANCE_MODE_MIXED;
         break;

      case SEX_DIFFERENTIATION_STEP_ONLY :
         PlatformAcceptance.Mode = PLATFORM_ACCEPTANCE_MODE_STEP;
         break;

   }
   PlatformAcceptance.Sex             =  WeighingConfiguration.TargetWeights.Sex;
   PlatformAcceptance.Step            =  WeighingConfiguration.Detection.Step;
   WeightMale   = CorrectionWeightSet( WeighingContext.TargetWeight);
   PlatformAcceptance.LowLimit        = (WeightMale   * (100 - WeighingConfiguration.Acceptance.Male.MarginBelow)) / 100;
   PlatformAcceptance.HighLimit       = (WeightMale   * (100 + WeighingConfiguration.Acceptance.Male.MarginAbove)) / 100;
   WeightFemale = CorrectionWeightSet( WeighingContext.TargetWeightFemale);
   PlatformAcceptance.LowLimitFemale  = (WeightFemale * (100 - WeighingConfiguration.Acceptance.Female.MarginBelow)) / 100;
   PlatformAcceptance.HighLimitFemale = (WeightFemale * (100 + WeighingConfiguration.Acceptance.Female.MarginAbove)) / 100;
   // update inner limits for mixed mode :
   if( WeighingHasFemale()){
      if( PlatformAcceptance.LowLimit < PlatformAcceptance.HighLimitFemale){
         // male/female overlapping
         Mid  = WeightMale + WeightFemale;
         Mid /= 2;
         // set boundary at mid of area :
         PlatformAcceptance.LowLimit        = Mid;
         PlatformAcceptance.HighLimitFemale = Mid;
      }
   }
} // _PlatformParameters

//------------------------------------------------------------------------------
//   Statistic clear
//------------------------------------------------------------------------------

static void _OnlineStatisticClear( void)
// Clear on-line statistic
{
   // clear online calculation :
   CalculateInit( &_Calculate, WeighingSexCurrent(), WeighingTarget(), StatisticsLastAverage( WeighingSexCurrent()), 0, 23);
   CalculateClear( &_Calculate);                                // clear statictics
   CalculateStatistic( &_Calculate, &_Statistic);               // update statistic
   // clear online calculation female :
   CalculateInit( &_CalculateFemale, SEX_FEMALE, WeighingTargetFemale(), StatisticsLastAverage( SEX_FEMALE), 0, 23);
   CalculateClear( &_CalculateFemale);                          // clear statictics
   CalculateStatistic( &_CalculateFemale, &_StatisticFemale);   // update statistic
   _HasMale   = NO;
   _HasFemale = NO;
} // _OnlineStatisticClear

//------------------------------------------------------------------------------
//   Statistic recalculate
//------------------------------------------------------------------------------

static void _OnlineStatisticRecalculate( void)
// Recalculate on-line statistic
{
   // update statistics :
   CalculateInit( &_Calculate, WeighingSexCurrent(), WeighingTarget(), StatisticsLastAverage( WeighingSexCurrent()), 0, 23);
   CalculateAll( &_Calculate);                        // recalculate statistics
   CalculateStatistic( &_Calculate, &_Statistic);     // update statistic
   // update statistics female :
   if( WeighingHasFemale()){
      CalculateInit( &_CalculateFemale, SEX_FEMALE, WeighingTargetFemale(), StatisticsLastAverage(SEX_FEMALE), 0, 23);
      CalculateAll( &_CalculateFemale);                         // recalculate statictics
      CalculateStatistic( &_CalculateFemale, &_StatisticFemale);// update statistic
   }
   _HasMale   = NO;
   _HasFemale = NO;
} // _OnlineStatisticRecalculate

//------------------------------------------------------------------------------
//   Statistic append
//------------------------------------------------------------------------------

static void _OnlineStatisticAppend( TWeightGauge Weight, TSampleFlag SampleFlag)
// Append sample with <Weight> and <SampleFlag> to statistic
{
   if( WeighingSexDifferentation() == SEX_DIFFERENTIATION_STEP_ONLY){
      return;                          // disable statistics
   }
   if( _IsFemale( SampleFlag)){
      CalculateAppend( &_CalculateFemale, Weight);
      _HasFemale = YES;                // recalculate female statistic
   } else {
      CalculateAppend( &_Calculate, Weight);
      _HasMale   = YES;                // recalculate male statistic
   }
} // _OnlineStatisticAppend

//------------------------------------------------------------------------------
//   Statistic update
//------------------------------------------------------------------------------

static void _OnlineStatisticUpdate( void)
// Update statistics after append
{
   if( WeighingSexDifferentation() == SEX_DIFFERENTIATION_STEP_ONLY){
      return;                          // disable statistics
   }
   if( _HasMale){
      CalculateUpdate( &_Calculate);
      CalculateStatistic( &_Calculate, &_Statistic);            // update male statistic
   }
   if( _HasFemale){
      CalculateUpdate( &_CalculateFemale);
      CalculateStatistic( &_CalculateFemale, &_StatisticFemale);// update female statistic
   }
   // cler recalculation flags :
   _HasMale   = NO;
   _HasFemale = NO;
} // _OnlineStatisticUpdate

//------------------------------------------------------------------------------
//   Weight correction
//------------------------------------------------------------------------------

static void _SampleWeightCorrection( TSample *Sample)
// Calculates <Sample> weight correction
{
TSampleFlag Flag;
TWeightGauge Weight;

   // decompose weight & flag :
   Flag   = SampleFlag( Sample);
   Weight = SampleWeight( Sample);
   // recalculate weight :
   Weight = CorrectionWeight( Weight);
   // compose sample :
   SampleWeightSet( Sample, Weight, Flag);
} // _SampleWeightCorrection

//------------------------------------------------------------------------------
//   Test female
//------------------------------------------------------------------------------


static TYesNo _IsFemale( TSampleFlag Flag)
// Check for female storage
{
   if( !WeighingHasFemale()){
      return( NO);                     // single sex mode
   }
   // mixed mode, check for sex :
   if( Flag & WEIGHT_FLAG_FEMALE){
      return( YES);
   }
   return( NO);
} // _IsFemale
