//******************************************************************************
//                                                                            
//   MenuPredefinedWeighing.h Predefined weighing menu
//   Version 1.0              (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPredefinedWeighing_H__
   #define __MenuPredefinedWeighing_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#ifndef __PredefinedWeighingDef_H__
   #include "Predefined/PredefinedWeighingDef.h"
#endif

#ifndef __PredefinedList_H__
   #include "Predefined/PredefinedList.h"
#endif

void MenuPredefinedWeighing( TPredefinedList *PredefinedList);
// Display predefined weighing menu

TYesNo MenuPredefinedWeighingSelect( TPredefinedList *PredefinedList, TUniStr Title, TPredefinedWeighingIndex *Identifier, TUniStr SpecialItem);
// Select predefined weighing, add <SpecialItem> at end of list

#endif
