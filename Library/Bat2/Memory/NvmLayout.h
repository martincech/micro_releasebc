//*****************************************************************************
//
//   NvmLayout.h  Bat2 nonvolatile memory layout
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __NvmLayout_H__
   #define __NvmLayout_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __NvmInternal_H__
   #include "Memory/NvmInternal.h"
#endif

// Warning : logical section start must match flash sector erase boundary

//------------------------------------------------------------------------------
//   Physical memory
//------------------------------------------------------------------------------

// direct FRAM memory :
#define NVM_FRAM_START            0x80000000
#define NVM_FRAM_SIZE             FRAM_SIZE
#define NVM_FRAM_SIZE             FRAM_SIZE
#define NVM_FRAM_END             (NVM_FRAM_START + NVM_FRAM_SIZE - 1)
#define NVM_FRAM_MODE             NVM_SECTION_CACHE_ONLY   // FRAM without caching

// direct Flash memory :
#define NVM_FLASH_START           0xC0000000
#define NVM_FLASH_SIZE            FLASH_SIZE
#define NVM_FLASH_END            (NVM_FLASH_START + NVM_FLASH_SIZE - 1)
#define NVM_FLASH_MODE            NVM_SECTION_STANDARD   // cache

// cached Flash memory :
#define NVM_MEMORY_START          0
#define NVM_MEMORY_SIZE           FLASH_SIZE
#define NVM_MEMORY_END           (NVM_MEMORY_START + NVM_MEMORY_SIZE - 1)

// RAM memory :
#define NVM_RAM_START             0xD0000000
#define NVM_RAM_SIZE              RAM_SIZE
#define NVM_RAM_END              (NVM_RAM_START + NVM_RAM_SIZE - 1)
#define NVM_RAM_MODE              NVM_SECTION_RAM

//------------------------------------------------------------------------------
//   Logical sections
//------------------------------------------------------------------------------

// device configuration :
#define NVM_CONFIG_START          NVM_MEMORY_START
#define NVM_CONFIG_SIZE          (64 * 1024)
#define NVM_CONFIG_END           (NVM_CONFIG_START + NVM_CONFIG_SIZE - 1)
#define NVM_CONFIG_MODE           NVM_SECTION_COMMIT_ONLY  // FRAM with Flash backup

// device context :
#define NVM_CONTEXT_START        (NVM_CONFIG_END + 1)
#define NVM_CONTEXT_SIZE         (8 * 1024)
#define NVM_CONTEXT_END          (NVM_CONTEXT_START + NVM_CONTEXT_SIZE - 1)
#define NVM_CONTEXT_MODE          NVM_SECTION_COMMIT_ONLY  // FRAM with Flash backup

// Sandbox :
#define NVM_SANDBOX_START        (NVM_CONTEXT_END + 1)
#define NVM_SANDBOX_SIZE         (64 * 1024)
#define NVM_SANDBOX_END          (NVM_SANDBOX_START + NVM_SANDBOX_SIZE - 1)
#define NVM_SANDBOX_MODE          NVM_SECTION_CACHE_ONLY

// diagnostic FIFO data :
#define NVM_DIAGNOSTIC_START     (NVM_SANDBOX_END + 1)
#define NVM_DIAGNOSTIC_SIZE    (((1 * 1024 * 1024)) - NVM_SANDBOX_SIZE)
#define NVM_DIAGNOSTIC_END       (NVM_DIAGNOSTIC_START + NVM_DIAGNOSTIC_SIZE - 1)
#define NVM_DIAGNOSTIC_MODE       NVM_SECTION_STANDARD   // FIFO with paging

// samples FIFO data :
#define NVM_SAMPLES_START        (NVM_DIAGNOSTIC_END + 1)
#define NVM_SAMPLES_SIZE        ((1 * 1024 * 1024) - NVM_CONFIG_SIZE - NVM_CONTEXT_SIZE)
#define NVM_SAMPLES_END          (NVM_SAMPLES_START + NVM_SAMPLES_SIZE - 1)
#define NVM_SAMPLES_MODE          NVM_SECTION_WRITE_ONLY   // FIFO with paging

// samples FIFO data :
#define NVM_SAMPLES_BACKUP_START   (NVM_SAMPLES_END + 1)
#define NVM_SAMPLES_BACKUP_SIZE     NVM_SAMPLES_SIZE
#define NVM_SAMPLES_BACKUP_END     (NVM_SAMPLES_BACKUP_START + NVM_SAMPLES_BACKUP_SIZE - 1)
#define NVM_SAMPLES_BACKUP_MODE     NVM_SECTION_WRITE_ONLY   // FIFO with paging

// archive FIFO data :
#define NVM_ARCHIVE_START        (NVM_SAMPLES_BACKUP_END + 1)
#define NVM_ARCHIVE_SIZE        ((5 * 1024 * 1024) - NVM_LOG_SIZE - NVM_FIFO_SIZE - NVM_SERVICE_SIZE)
#define NVM_ARCHIVE_END          (NVM_ARCHIVE_START + NVM_ARCHIVE_SIZE - 1)
#define NVM_ARCHIVE_MODE          NVM_SECTION_WRITE_ONLY   // FIFO with paging

// firmware update data :
#define NVM_FIRMWARE_START        NVM_FLASH_END + 1 - NVM_FIRMWARE_SIZE
#define NVM_FIRMWARE_SIZE         (1 * 1024 * 1024)
#define NVM_FIRMWARE_END          NVM_FLASH_END

// log FIFO data :
#define NVM_LOG_START            (NVM_ARCHIVE_END + 1)
#define NVM_LOG_SIZE            ((40 * 1024))
#define NVM_LOG_END              (NVM_LOG_START + NVM_LOG_SIZE - 1)
#define NVM_LOG_MODE              NVM_SECTION_WRITE_ONLY   // FIFO with paging

// FIFO pointers :
#define NVM_FIFO_START           (NVM_LOG_END + 1)
#define NVM_FIFO_SIZE            (4 * 1024)
#define NVM_FIFO_END             (NVM_FIFO_START + NVM_FIFO_SIZE - 1)
#define NVM_FIFO_MODE             NVM_SECTION_COMMIT_ONLY  // FRAM with Flash backup

// FIFO pointer addresses :
#define NVM_FIFO_POINTER_SIZE     32                       // UFIFO_POINTER_SIZE with spare

// ROM service data :
#define NVM_SERVICE_START        (NVM_FIFO_END + 1)
#define NVM_SERVICE_SIZE         (4 * 1024)
#define NVM_SERVICE_END          (NVM_SERVICE_START + NVM_SERVICE_SIZE - 1)
#define NVM_SERVICE_MODE          NVM_SECTION_NOCACHE      // Flash without caching

// RAM state data :
#define NVM_STATE_START          (NVM_RAM_START)
#define NVM_STATE_SIZE           (32)
#define NVM_STATE_END            (NVM_STATE_START + NVM_STATE_SIZE - 1)
#define NVM_STATE_MODE            NVM_SECTION_RAM

extern byte * const NvmState;

//------------------------------------------------------------------------------
//   Cache data
//------------------------------------------------------------------------------

#define NVM_PAGE_SIZE            (4 * 1024)                // minimum Flash block erase size

#define NVM_PAGE_CONFIG_START    0
#define NVM_PAGE_CONFIG_SIZE     NvmPageLayoutSize( NVM_CONFIG_SIZE)
#define NVM_PAGE_CONFIG_PAGE     NvmPageSizeSet( NVM_CONFIG_SIZE)
#define NVM_PAGE_CONFIG_END     (NVM_PAGE_CONFIG_START + NVM_PAGE_CONFIG_SIZE - 1)

#define NVM_PAGE_CONTEXT_START  (NVM_PAGE_CONFIG_END + 1)
#define NVM_PAGE_CONTEXT_SIZE    NvmPageLayoutSize( NVM_CONTEXT_SIZE)
#define NVM_PAGE_CONTEXT_PAGE    NvmPageSizeSet( NVM_CONTEXT_SIZE)
#define NVM_PAGE_CONTEXT_END    (NVM_PAGE_CONTEXT_START + NVM_PAGE_CONTEXT_SIZE - 1)

#define NVM_PAGE_SANDBOX_START  (NVM_PAGE_CONTEXT_END + 1)
#define NVM_PAGE_SANDBOX_SIZE    NvmPageLayoutSize( NVM_SANDBOX_SIZE)
#define NVM_PAGE_SANDBOX_PAGE    NvmPageSizeSet( NVM_SANDBOX_SIZE)
#define NVM_PAGE_SANDBOX_END    (NVM_PAGE_SANDBOX_START + NVM_PAGE_SANDBOX_SIZE - 1)

#define NVM_PAGE_DIAGNOSTIC_START  (NVM_PAGE_SANDBOX_END + 1)
#define NVM_PAGE_DIAGNOSTIC_SIZE    NvmPageLayoutSize( NVM_PAGE_SIZE)
#define NVM_PAGE_DIAGNOSTIC_PAGE    NvmPageSizeSet( NVM_PAGE_SIZE)
#define NVM_PAGE_DIAGNOSTIC_END    (NVM_PAGE_DIAGNOSTIC_START + NVM_PAGE_DIAGNOSTIC_SIZE - 1)

#define NVM_PAGE_SAMPLES_START  (NVM_PAGE_DIAGNOSTIC_END + 1)
#define NVM_PAGE_SAMPLES_SIZE    NvmPageLayoutSize( NVM_PAGE_SIZE)
#define NVM_PAGE_SAMPLES_PAGE    NvmPageSizeSet( NVM_PAGE_SIZE)
#define NVM_PAGE_SAMPLES_END    (NVM_PAGE_SAMPLES_START + NVM_PAGE_SAMPLES_SIZE - 1)

#define NVM_PAGE_SAMPLES_BACKUP_START         (NVM_PAGE_SAMPLES_END + 1)
#define NVM_PAGE_SAMPLES_BACKUP_SIZE           NvmPageLayoutSize( NVM_PAGE_SIZE)
#define NVM_PAGE_SAMPLES_BACKUP_PAGE           NvmPageSizeSet( NVM_PAGE_SIZE)
#define NVM_PAGE_SAMPLES_BACKUP_END           (NVM_PAGE_SAMPLES_BACKUP_START + NVM_PAGE_SAMPLES_BACKUP_SIZE - 1)

#define NVM_PAGE_ARCHIVE_START  (NVM_PAGE_SAMPLES_BACKUP_END + 1)
#define NVM_PAGE_ARCHIVE_SIZE    NvmPageLayoutSize( NVM_PAGE_SIZE)
#define NVM_PAGE_ARCHIVE_PAGE    NvmPageSizeSet( NVM_PAGE_SIZE)
#define NVM_PAGE_ARCHIVE_END    (NVM_PAGE_ARCHIVE_START + NVM_PAGE_ARCHIVE_SIZE - 1)

#define NVM_PAGE_LOG_START      (NVM_PAGE_ARCHIVE_END + 1)
#define NVM_PAGE_LOG_SIZE        NvmPageLayoutSize( NVM_PAGE_SIZE)
#define NVM_PAGE_LOG_PAGE        NvmPageSizeSet( NVM_PAGE_SIZE)
#define NVM_PAGE_LOG_END        (NVM_PAGE_LOG_START + NVM_PAGE_LOG_SIZE - 1)

#define NVM_PAGE_FIFO_START     (NVM_PAGE_LOG_END + 1)
#define NVM_PAGE_FIFO_SIZE       NvmPageLayoutSize( NVM_FIFO_SIZE)
#define NVM_PAGE_FIFO_PAGE       NvmPageSizeSet( NVM_FIFO_SIZE)
#define NVM_PAGE_FIFO_END       (NVM_PAGE_FIFO_START + NVM_PAGE_FIFO_SIZE - 1)

#define NVM_PAGE_FLASH_START     (NVM_PAGE_FIFO_END + 1)
#define NVM_PAGE_FLASH_SIZE       NvmPageLayoutSize( NVM_PAGE_SIZE)
#define NVM_PAGE_FLASH_PAGE       NvmPageSizeSet( NVM_PAGE_SIZE)
#define NVM_PAGE_FLASH_END       (NVM_PAGE_FLASH_START + NVM_PAGE_FLASH_SIZE - 1)

#endif
