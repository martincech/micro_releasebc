//******************************************************************************
//                                                                            
//   MenuStatisticParameters.h  Statistic Parameters menu
//   Version 1.0                (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuStatistic_H__
   #define __MenuStatistic_H__

#ifndef __StatisticDef_H__
   #include "Statistic/StatisticDef.h"
#endif

void MenuStatisticParameters( TStatisticParameters *StatisticParameters);
// Edit statistic parameters

#endif
