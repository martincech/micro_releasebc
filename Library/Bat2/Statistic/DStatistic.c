//******************************************************************************
//                                                                            
//    DStatistic.c   Display statistics
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "DStatistic.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Weight/DWeight.h"       // Display weight
#include "Bitmap.h"               // Bitmaps
#include "Fonts.h"                // Project fonts

// Local functions :

static void DrawGrid( void);
// Draw screen grid

static void DrawData( TStatistic *StatisticCalculate);
// Draw statistic column

//------------------------------------------------------------------------------
//   Statistics
//------------------------------------------------------------------------------

#define LEFT_X    36
#define RIGHT_X  (G_WIDTH / 2 + 30)

void DStatistic( TStatistic *Statistic)
// Display <Statistic>. Returns YES on Enter
{
   // title icon :
   GSetColor( DCOLOR_TITLE);
   switch( Statistic->Sex){
      case SEX_MALE :
         GBitmap( G_WIDTH - 18, 2, &BmpIconMale);
         break;

      case SEX_FEMALE :
         GBitmap( G_WIDTH - 18, 2, &BmpIconFemale);
         break;

      default :
         break;
   }
   GSetColor( DCOLOR_DEFAULT);
   DrawGrid();
   DrawData( Statistic);
} // DStatistic

//------------------------------------------------------------------------------
//   Grid
//------------------------------------------------------------------------------

#define SS_TITLE_X  4

static void DrawGrid( void)
// Draw screen grid
{
   // background
   GSetColor( COLOR_LIGHTGRAY);
   GBox( 0, 59, G_WIDTH, 19);
   GBox( 0, 97, G_WIDTH, 19);

   // grid
   GLine( 0, 39, G_WIDTH, 39);
   GLine( 29, 22, 29, 135); 
   GSetMode( GMODE_REPLACE);

   // symbols
   GSetColor( DCOLOR_DEFAULT);
   GBitmap( 6, 40,  &BmpStatisticsCount);
   GBitmap( 8, 60,  &BmpStatisticsWeight);
   GBitmap( 9, 80,  &BmpStatisticsSigma);
   GBitmap( 2, 100, &BmpStatisticsCv);
   GBitmap( 2, 120, &BmpStatisticsUni);

   GBitmap( G_WIDTH / 2, 41,   &BmpStatisticsTarget);
   GBitmap( G_WIDTH / 2, 61,   &BmpStatisticsLastAverage);
   GBitmap( G_WIDTH / 2, 81,   &BmpStatisticsGain);
} // DrawGrid

//------------------------------------------------------------------------------
//   Column
//------------------------------------------------------------------------------

static void DrawData( TStatistic *Statistic)
// Draw statistics column
{
   // count
   GSetFont( TAHOMA16);
   GTextAt( LEFT_X, 40);
   cprintf( "%d", Statistic->Count);
   // average
   GTextAt( LEFT_X, 60);
   DWeightLeft( Statistic->Average);
   // sigma
   GTextAt( LEFT_X, 80);
   DWeightLeft( Statistic->Sigma);
   // variation
   GTextAt( LEFT_X, 100);
   cprintf( "%3.1f", Statistic->Cv);
   // uniformity
   GTextAt( LEFT_X, 120);
   cprintf( "%3.1f", Statistic->Uniformity);

   // target
   GTextAt( RIGHT_X, 40);
   DWeightLeft( Statistic->Target);
   // last average
   GTextAt( RIGHT_X, 60);
   if( Statistic->LastAverage == WEIGHT_INVALID){
      DWeightLeft( 0);
   } else {
      DWeightLeft( Statistic->LastAverage);
   }
   // gain
   GTextAt( RIGHT_X, 80);
   DWeightLeft( Statistic->Gain);
} // DrawData
