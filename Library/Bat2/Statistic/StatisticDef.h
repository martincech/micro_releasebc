//*****************************************************************************
//
//    StatisticDef.h  Statistic data definition
//    Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __StatisticDef_H__
   #ifndef _MANAGED
      #define __StatisticDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifndef __SexDef_H__
   #include "Weight/SexDef.h"
#endif

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------
#ifndef _MANAGED
#define HISTOGRAM_MIN_STEP       1      // min. step in THistogramValue
#define HISTOGRAM_SLOTS          39     // columns count even/odd (max. 254)
#define HISTOGRAM_NORM           1000   // normalized value by max column
#define HISTOGRAM_RANGE_MIN      1
#define HISTOGRAM_RANGE_MAX      99

#define STATISTIC_SHORT_PERIOD_MIN      0
#define STATISTIC_SHORT_PERIOD_MAX      12
#define STATISTIC_SHORT_PERIOD_DEFAULT  0

#define STATISTIC_UNIFORMITY_RANGE_MIN  1
#define STATISTIC_UNIFORMITY_RANGE_MAX  99

#define STATISTIC_HISTOGRAM_STEP_MIN     10
#define STATISTIC_HISTOGRAM_STEP_MAX   2000

//------------------------------------------------------------------------------
// Data types
//------------------------------------------------------------------------------

// Statistic data types :
typedef double TStatisticNumber;            // statistic number
typedef dword  TStatisticCount;             // statistic samples count
typedef byte   TStatisticUniformityRange;   // uniformity range [%]

// Histogram data types :
typedef word         THistogramCount;       // samples count
typedef TWeightGauge THistogramValue;       // sample value, must be signed

//------------------------------------------------------------------------------
// Statistic internal representation
//------------------------------------------------------------------------------

#include "Statistic/StatisticGaugeDef.h"

//------------------------------------------------------------------------------
// Statistic
//------------------------------------------------------------------------------

typedef struct {
   TWeightGauge    Target;             // target weight
   word            Sex;                // sex
   TStatisticCount Count;              // samples count
   TWeightGauge    Average;            // average weight
   TWeightGauge    LastAverage;        // yesterday average weight
   TWeightGauge    Gain;               // daily gain (sometimes negative)
   TWeightGauge    Sigma;              // standard deviation
   word            Cv;                 // variance [0.1%]
   word            Uniformity;         // uniformity [0.1%]
} TStatistic;

//------------------------------------------------------------------------------
// Histogram
//------------------------------------------------------------------------------

#include "Statistic/HistogramDef.h"
#endif
//------------------------------------------------------------------------------
// Parameters
//------------------------------------------------------------------------------

// Histogram mode :
#ifdef _MANAGED
namespace Bat2Library{
   public enum class HistogramModeE{
#else
typedef enum {
#endif
   HISTOGRAM_MODE_RANGE,               // histogram by range
   HISTOGRAM_MODE_STEP,                // histogram by absolute step
#ifndef _MANAGED
   _HISTOGRAM_MODE_COUNT
} EHistogramMode;
#else
   };
}
#endif

#ifdef _MANAGED
namespace Bat2Library{
   public enum class StatisticShortTypeE{
#else
typedef enum {
#endif
   STATISTIC_SHORT_TYPE_INTERVAL,
   STATISTIC_SHORT_TYPE_CUMMULATIVE,
#ifndef _MANAGED
   _STATISTIC_SHORT_TYPE_COUNT
} EStatisticShortType;
#else
   };
}
#endif

#ifndef _MANAGED
// Histogram parameters
typedef struct {
   byte              Mode;             // EHistogramMode - mode of histogram
   byte              Range;	       // Histogram step in +/- percents of average weight
   TWeightShortGauge Step;	       // Histogram step in +/- kg
} THistogramParameters;

// Parameters for statistic calculations
typedef struct {
   TStatisticUniformityRange UniformityRange; // Uniformity range in +/- percents
   byte                      ShortPeriod;
   byte                      ShortType;
   byte                      _Spare;
   THistogramParameters      Histogram;       // Histogram parameters
} TStatisticParameters;
#endif

//------------------------------------------------------------------------------
#ifdef _MANAGED
   #undef _MANAGED
   #include "StatisticDef.h"
   #define _MANAGED
   namespace Bat2Library{
      public ref class StatisticParametersC abstract sealed{
      public:
         literal int HIST_MIN_STEP = HISTOGRAM_MIN_STEP;
         literal int HIST_SLOTS = HISTOGRAM_SLOTS;
         literal int HIST_RANGE_MIN = HISTOGRAM_RANGE_MIN;
         literal int HIST_RANGE_MAX = HISTOGRAM_RANGE_MAX;

         literal int SHORT_PERIOD_MIN = STATISTIC_SHORT_PERIOD_MIN;
         literal int SHORT_PERIOD_MAX = STATISTIC_SHORT_PERIOD_MAX;
         literal int SHORT_PERIOD_DEFAULT = STATISTIC_SHORT_PERIOD_DEFAULT;

         literal int UNIFORMITY_RANGE_MIN = STATISTIC_UNIFORMITY_RANGE_MIN;
         literal int UNIFORMITY_RANGE_MAX = STATISTIC_UNIFORMITY_RANGE_MAX;
      };
   }
#endif

#endif
