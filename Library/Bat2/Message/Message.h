//******************************************************************************
//
//   Message.h      GSM messaging
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Message_H__
#ifndef _MANAGED
	#define __Message_H__
#endif

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __GsmMessageDef_H__
   #include "Message/GsmMessageDef.h"
#endif



//------------------------------------------------------------------------------
//  Binary SMS data type
//------------------------------------------------------------------------------

#include "Statistic/StatisticDef.h"            // Weighing statistics

#ifdef _WIN32
#pragma pack( push, 1)                   // byte alignment
#endif

#ifdef _MANAGED
namespace Bat2Library{
	public ref class GenderStats{
#else
typedef struct
{
#endif
#ifdef _MANAGED
public:
	property
#endif
		TWeightGauge    Target;             // target weight
#ifdef _MANAGED
	property
#endif
		TStatisticCount Count;              // samples count
#ifdef _MANAGED
	property
#endif
		TWeightGauge    Average;            // average weight
#ifdef _MANAGED
	property
#endif
		TWeightGauge    Gain;               // daily gain (sometimes negative)
#ifdef _MANAGED
	property
#endif
		TWeightGauge    Sigma;              // standard deviation
#ifdef _MANAGED
	property
#endif
		word            Cv;                 // variance [0.1%]
#ifdef _MANAGED
	property
#endif
		word            Uniformity;         // uniformity [0.1%]
#ifndef _MANAGED
} __packed TBinaryGenderStats;
#else
};
}
#endif


#ifdef _MANAGED
namespace Bat2Library{
	public ref class PublishedData{
#else
typedef struct
{
#endif
#ifdef _MANAGED
public:
	property
#endif
		dword                   SerialNumber;
#ifdef _MANAGED
	property DateTime
#else
	UDateTimeGauge
#endif
										DateTime;
#ifdef _MANAGED
	property
#endif
		TDayNumber              Day;
#ifdef _MANAGED
	  GenderStats ^
#else
	TBinaryGenderStats
#endif
										Male;
#ifndef _MANAGED
	union{
#endif
#ifdef _MANAGED
		GenderStats ^
#else
		TBinaryGenderStats
#endif
										Female;
#ifndef _MANAGED
		word							Crc16;
	} Female;
	word								Crc16;
} __packed TBinaryStatistics;
#else
	};
}
#endif

#ifdef _WIN32
#pragma pack( pop)                       // original alignment
#endif
//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void MessageInit( void);
// Initialize

void MessageExecute( void);
// Execute

void MessageResume( void);
// Resume weighing after power on

void MessageStart( void);
// Start weighing

void MessageDayClose( void);
// Close day

void MessageStop( void);
// Stop weighing

#ifdef _MANAGED
#undef _MANAGED
#include "Message.h"
#define _MANAGED
#endif

#endif
