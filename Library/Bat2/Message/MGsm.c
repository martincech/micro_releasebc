//******************************************************************************
//
//   MGsm.c       Managed GSM services
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "MGsm.h"
#include "Hardware.h"
#include "Console/conio.h"
#include "System/System.h"
#include "Message/GsmMessage.h"             // GSM messaging configuration
#include "Gsm/Gsm.h"                        // GSM services
#include "Protothreads/pt-sem.h"            // Protothread mutexes
#include "Multitasking/Multitasking.h"              // normal tasks
#include "Power/Power.h"                    // Power management
#include "Str.h"
#include "Device/VersionDef.h"
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

static TMutex              _ThMutex;    // interthread mutex
static struct pt           _MGsmPt;
static byte                _MGsmTimer;
static TYesNo              _GsmValidPin;
static EMGsmStatus         _GsmStatus;
static byte                _ForcedOn;
static char                _Operator[ GSM_OPERATOR_SIZE + 1];
#define GSM_READ_ADDRESS     0              // address of incomming SMS slot


#define MGSM_INSTRUCTION_TIMEOUT                      TimerSlowCount(15)
#define MGSM_REGISTERED_TIMEOUT                       TimerSlowCount(45)
#define MGSM_CHECK_REGISTERED_PERIOD_TIMEOUT          TimerSlowCount(80)
#define MGSM_CHECK_REGISTERED_SHORT_PERIOD_TIMEOUT    TimerSlowCount(5)
#define MGSM_POWER_ON_TIMEOUT                         TimerSlowCount(12)

#define MGsmTimerIsSet()      (_MGsmTimer != 0)
#define MGsmTimerFinished()   (!MGsmTimerIsSet())
#define MGsmTimerSet(period)  _MGsmTimer = period
#define MGsmTimerTick()       if( MGsmTimerIsSet()) {_MGsmTimer--;}

#define MutexTryLock()      MultitaskingMutexTrySet( &_ThMutex)
#define MutexGetLock()     MultitaskingMutexSet(&_ThMutex);
#define MutexFreeLock()    MultitaskingMutexRelease(&_ThMutex)

#define PTRestartWithStatus(s)   \
            _PowerOff(); \
            _GsmStatus = s; \
            PT_RESTART(&_MGsmPt)

// Local functions :
static TYesNo _PowerOnExecute( void);
// Check for power options
static TYesNo _PowerOffExecute( void);
// Check for power options
static void _PowerOff( void);
// Power off
static void _PowerOn(void);
// Power on
#ifndef __WIN32__
   static int fnet_netif_3G_stats( struct fnet_netif *netif, struct fnet_netif_statistics *statistics );
   // fnet interface statistic function
   static int fnet_netif_3G_is_connected( struct fnet_netif *);
   // fnet interface connected function

   // values for fnet network interface
   //inteface specific
   static fnet_3G_if_t iface3G ={
      NO,
      0,
      0
   };

   //api
   static fnet_netif_api_t fnet_3G_api = {
      .type = FNET_NETIF_TYPE_OTHER,
      .is_connected = &fnet_netif_3G_is_connected,
      .get_statistics = &fnet_netif_3G_stats
   };

   //interface itself
   fnet_netif_t fnet_3G_if = {
      .name = "EHS5-E",
      .if_ptr = (void *)&iface3G,
      .api = &fnet_3G_api
   };

#endif
//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void MGsmInit( void)
// Initialize
{
   PT_INIT(&_MGsmPt);
   _MGsmTimer = 0;
   _GsmValidPin = NO;
   _ForcedOn = 0;
   _GsmStatus = MGSM_STATUS_OFF;
   _PowerOff();
   MultitaskingMutexInit( &_ThMutex);
   strcpy( _Operator, "?");
   MutexGetLock();                        // MUTEX lock released in Execute routine
} // MGsmInit

//------------------------------------------------------------------------------
//  MGSM Execute
//------------------------------------------------------------------------------

PT_THREAD(MGsmExecute( void))
{
TYesNo JobDone = NO;
TYesNo CheckRegistration = NO;

   MGsmTimerTick();

   PT_BEGIN(&_MGsmPt);
   while(1){
      // Power on?
      PT_WAIT_UNTIL(&_MGsmPt, _PowerOnExecute());
      // turn module on
     _PowerOn();
      // Wait for power on
      MGsmTimerSet(MGSM_POWER_ON_TIMEOUT);
      PT_WAIT_UNTIL(&_MGsmPt, MGsmTimerFinished());
      GsmInit();

      //-----------------------------------------------
      // Registration
      MGsmTimerSet(MGSM_INSTRUCTION_TIMEOUT);
      PT_WAIT_UNTIL(&_MGsmPt,  ((JobDone = GsmReset()) == YES) || MGsmTimerFinished() || _PowerOffExecute());
      if( !JobDone){
         // Can't reset, timeout or turned off by request
         if( MGsmTimerFinished()){
            PTRestartWithStatus(MGSM_STATUS_ERROR);
         } else {
            PTRestartWithStatus(MGSM_STATUS_OFF);
         }
      }
      _GsmStatus = MGSM_STATUS_NO_PIN;
      MGsmTimerSet(MGSM_REGISTERED_TIMEOUT);
      PT_WAIT_UNTIL(&_MGsmPt,  ((JobDone = GsmPinReady()) == YES) || MGsmTimerFinished() || _PowerOffExecute());
      if( !JobDone){
         _GsmStatus = MGSM_STATUS_WAIT_PIN;
         PT_WAIT_UNTIL(&_MGsmPt,  _GsmValidPin || _PowerOffExecute());
         if( !_GsmValidPin){
            PTRestartWithStatus(MGSM_STATUS_OFF);
         }
      }
      _GsmValidPin = YES;
      _GsmStatus = MGSM_STATUS_ON;
      // Network registration
      MGsmTimerSet(MGSM_REGISTERED_TIMEOUT);
      PT_WAIT_UNTIL(&_MGsmPt, ((JobDone = GsmRegistered()) == YES) || MGsmTimerFinished()  || _PowerOffExecute());
      if( !JobDone){
         if( MGsmTimerFinished()){
            PTRestartWithStatus(MGSM_STATUS_ERROR);
         } else {
            PTRestartWithStatus(MGSM_STATUS_OFF);
         }
      }
      // get operator
      MGsmTimerSet(MGSM_INSTRUCTION_TIMEOUT);
      PT_WAIT_UNTIL(&_MGsmPt, ((JobDone = GsmOperator( _Operator)) == YES) || MGsmTimerFinished()  || _PowerOffExecute());
      if( !JobDone){
         if( MGsmTimerFinished()){
            PTRestartWithStatus(MGSM_STATUS_ERROR);
         } else {
            PTRestartWithStatus(MGSM_STATUS_OFF);
         }
      }
      // init sms service
      MGsmTimerSet(MGSM_INSTRUCTION_TIMEOUT);
      PT_WAIT_UNTIL(&_MGsmPt, ((JobDone = GsmSmsInit()) == YES) || MGsmTimerFinished() || _PowerOffExecute());
      if( !JobDone){
         if( MGsmTimerFinished()){
            PTRestartWithStatus(MGSM_STATUS_ERROR);
         } else {
            PTRestartWithStatus(MGSM_STATUS_OFF);
         }
      }
      #ifndef __WIN32__
      if (BAT2_HAS_GSM_MODULE(Bat2Version.Modification)){
        // init data service
        MGsmTimerSet(MGSM_INSTRUCTION_TIMEOUT);
        PT_WAIT_UNTIL(&_MGsmPt, ((JobDone = GsmNetInit()) == YES) || MGsmTimerFinished() || _PowerOffExecute());
        if( !JobDone){
           if( MGsmTimerFinished()){
              PTRestartWithStatus(MGSM_STATUS_ERROR);
           } else {
              PTRestartWithStatus(MGSM_STATUS_OFF);
           }
        }
      }
      #endif
      //-----------------------------------------------
      //registration complete - check periodically if still registered
      // permanently check registration or power off
      _GsmStatus = MGSM_STATUS_READY;
      MutexFreeLock();                                                     // MUTEX unlock from init
      CheckRegistration = YES;
      JobDone = YES;
      while( CheckRegistration){
         if( JobDone){
            MGsmTimerSet(MGSM_CHECK_REGISTERED_PERIOD_TIMEOUT);
         } else {
            MGsmTimerSet(MGSM_CHECK_REGISTERED_SHORT_PERIOD_TIMEOUT);
         }
         PT_WAIT_UNTIL(&_MGsmPt, MGsmTimerFinished() || _PowerOffExecute());
         MutexGetLock();                                  // MUTEX lock
         if( !MGsmTimerFinished()){
            PTRestartWithStatus(MGSM_STATUS_OFF);
         }
         //check for registration
         _GsmStatus = MGSM_STATUS_CHECK_REGISTERED;
         MGsmTimerSet(MGSM_REGISTERED_TIMEOUT);
         PT_WAIT_UNTIL(&_MGsmPt, ((JobDone = GsmRegistered()) == YES) || MGsmTimerFinished()  || _PowerOffExecute());
         if( !JobDone){
            if( MGsmTimerFinished()){
               _GsmStatus = MGSM_STATUS_NOT_REGISTERED;
               MutexFreeLock();                                   // MUTEX unlock
               CheckRegistration = YES;
               continue;
            } else {
               PTRestartWithStatus(MGSM_STATUS_OFF);
            }
         }else {
            _GsmStatus = MGSM_STATUS_READY;
         }
         // get operator
         if( MGsmRegistered()){
            MGsmTimerSet(MGSM_INSTRUCTION_TIMEOUT);
            PT_WAIT_UNTIL(&_MGsmPt, ((JobDone = GsmOperator( _Operator)) == YES) || MGsmTimerFinished()  || _PowerOffExecute());
            if( !JobDone){
               if( !MGsmTimerFinished()){
                  PTRestartWithStatus(MGSM_STATUS_OFF);
               }
            }
         }
         MutexFreeLock();                                   // MUTEX unlock
         CheckRegistration = YES;
      }
      //-----------------------------------------------
   }

   PT_END(&_MGsmPt);
}

void MGsmFree( void)
{
   MultitaskingMutexRelease( &_ThMutex);
}

//------------------------------------------------------------------------------
//  Send sms
//------------------------------------------------------------------------------

TYesNo MGsmSend( char *PhoneNumber, char *Message)
// GSM send SMS <Message> to <PhoneNumber>
{
TYesNo send;

   MutexGetLock(); // wait for GSM not used
   _GsmStatus = MGSM_STATUS_SEND;
   send = GsmSmsSend( PhoneNumber, Message);
   _GsmStatus = MGSM_STATUS_READY;
   MutexFreeLock();
   return( send);
} // MGsmSend

//------------------------------------------------------------------------------
//  Receive sms
//------------------------------------------------------------------------------

TYesNo MGsmReceive( char *PhoneNumber, char *Message, UClockGauge *ReceivedTime)
{
TSmsReadStatus Status;

   if(!MutexTryLock()){
      // wait for GSM not used
      return NO;
   }
   Status = GsmSmsRead( PhoneNumber, Message, ReceivedTime);
   MutexFreeLock();
   return( (Status != GSM_SMS_READ_EMPTY) && (Status != GSM_SMS_READ_ERROR));
} // MGsmReceive

//------------------------------------------------------------------------------
//   Registered
//------------------------------------------------------------------------------

TYesNo MGsmRegistered( void)
// Returns YES on modem registered in network
{
   return MGsmStatus() > MGSM_STATUS_NOT_REGISTERED;
} // MGsmRegistered

//------------------------------------------------------------------------------
//   Operator
//------------------------------------------------------------------------------

TYesNo MGsmOperator( char *Name)
// Returns operator <Name>
{
   strcpy(Name, _Operator);
   return YES;
} // MGsmOperator

//------------------------------------------------------------------------------
//   Signal
//------------------------------------------------------------------------------

byte MGsmSignalStrength( void)
// Returns relative signal strength
{
byte RetVal = GSM_SIGNAL_UNDEFINED;

   if( !MutexTryLock()){
      return( RetVal);  // wait for GSM not used
   }
   RetVal = GsmSignalStrength();
   MutexFreeLock();
   return( RetVal);
} // MGsmSignalStrength


//------------------------------------------------------------------------------
//   Phone number
//------------------------------------------------------------------------------

TYesNo MGsmPhoneNumber( char *PhoneNumber)
// Returns own <PhoneNumber>
{
TYesNo RetVal;

   if( !MutexTryLock()){
      return( NO);  // wait for GSM not used
   }
   RetVal = GsmPhoneNumber( PhoneNumber);
   MutexFreeLock();
   return( RetVal);
} // MGsmPhoneNumber

//------------------------------------------------------------------------------
//   Pin ready
//------------------------------------------------------------------------------

TYesNo MGsmPinReady( void)
// Returns YES on valid PIN
{
   return( _GsmValidPin);
} // MGsmPinReady

//------------------------------------------------------------------------------
//   Pin enter
//------------------------------------------------------------------------------

TYesNo MGsmPinEnter( char *Pin)
// Enters <Pin>
{
TYesNo RetVal = NO;
TYesNo LockedByExecutive = NO;

   if( !MutexTryLock()){
      if( _GsmStatus != MGSM_STATUS_WAIT_PIN){
         return RetVal;
      }
      LockedByExecutive = YES;                   // locked by executive, must enter pin
   }

   RetVal = GsmPinEnter( Pin);
   if( RetVal){
      _GsmValidPin = YES;
   }
   if( !LockedByExecutive){
      MutexFreeLock();
   }
   return( RetVal);
} // MGsmPinEnter

#ifdef _SOCKET_IF_SMS_
//------------------------------------------------------------------------------
//   SMS Channel init
//-----------------------------------------------------------------------------
static TYesNo SmsSocketInited = NO;
TYesNo MGsmSmsSocketInit()
// Sms channel service init
{
   MGsmPowerOn();
   MutexGetLock();
   GsmSmsChannelInit();
   SmsSocketInited = YES;
   MutexFreeLock();
}

//------------------------------------------------------------------------------
//   SMS Channel free
//------------------------------------------------------------------------------

TYesNo MGsmSmsSocketFree()
// Sms channel service free
{
   MutexGetLock();
   MGsmPowerResume();
   GsmSmsChannelDeinit();
   SmsSocketInited = NO;
   MutexFreeLock();
}

//------------------------------------------------------------------------------
//   SMS Channel executive
//------------------------------------------------------------------------------

void MGsmSmsSocketExecute()
// Sms channel service executive
{
   MutexGetLock();
   GsmSmsChannelExecutive();
   MutexFreeLock();
}

#endif

TYesNo MGsmSendBinary( char *PhoneNumber, char *Message, int32 size)
// GSM send SMS <Message> to <PhoneNumber> as binary sms
{
TYesNo ret;

   MutexGetLock()
#ifdef _SOCKET_IF_SMS_
   while( !SmsSocketInited){
      MutexFreeLock();
      MultitaskingReschedule();
#endif
      MutexGetLock();
#ifdef _SOCKET_IF_SMS_
   }
   ret = (GsmSmsChannelSendAndDel( PhoneNumber, Message, size) != SMS_INVALID_CHANNEL);
#else
   ret = GsmSmsSendBinary(PhoneNumber, Message, size);
#endif
   MutexFreeLock();
   return ret;
}
//------------------------------------------------------------------------------
//   Power on
//------------------------------------------------------------------------------

void MGsmPowerOn( void)
// Instant power on
{
   _ForcedOn++;
} // MGsmPowerOn

//------------------------------------------------------------------------------
//   Power resume
//------------------------------------------------------------------------------

void MGsmPowerResume( void)
// Resume power mode
{
   if( _ForcedOn){
      _ForcedOn--;
   }
} // MGsmPowerResume

//------------------------------------------------------------------------------
//  Status
//------------------------------------------------------------------------------

EMGsmStatus MGsmStatus( void)
// Returns actual GSM status
{
   return( _GsmStatus);
} // MessageStatus

//------------------------------------------------------------------------------
//  Power period?
//------------------------------------------------------------------------------
TYesNo MGsmIsPowerPeriod( void)
// return true if GSM is switched on by planned activity (not forced by other events)
{
UClockGauge Now, From, To;
UTime Time;
int i;

   switch( GsmMessage.PowerOptions.Mode){
      case GSM_POWER_MODE_OFF :
         break;

      case GSM_POWER_MODE_ON :
         return YES;

      case GSM_POWER_MODE_PERIODIC :
         // check for active interval
         Now   = SysTime();
         From  = Now;
         From /= GsmMessage.PowerOptions.SwitchOnPeriod * TIME_MIN;
         From *= GsmMessage.PowerOptions.SwitchOnPeriod * TIME_MIN;
         To    = From + GsmMessage.PowerOptions.SwitchOnDuration * TIME_MIN;
         if( Now >= From && Now <= To){
            return YES;
         }
         break;
      case GSM_POWER_MODE_TIME_PLAN :
         // check for active time
         uTime( &Time, SysClock());
         Now = (UClockGauge) uTimeGauge( &Time);
         for( i = 0; i < GsmMessage.PowerOptions.TimesCount; i++){
            From  = GsmMessage.PowerOptions.Times[i].From;
            To  = GsmMessage.PowerOptions.Times[i].To;
            if( Now >= From && Now <= To){
               return YES;
            }
         }
         break;
   }
   return NO;
}

//------------------------------------------------------------------------------
//  Power on execute?
//------------------------------------------------------------------------------

static TYesNo _PowerOnExecute( void)
// Check for power options
{
   if( _ForcedOn){
      return( YES);
   }
   if(_GsmStatus >= MGSM_STATUS_SEND){
      return( YES);                   // wait for SMS send
   }

   return MGsmIsPowerPeriod();
} // _PowerExecute

//------------------------------------------------------------------------------
//  Power off execute?
//------------------------------------------------------------------------------

static TYesNo _PowerOffExecute( void)
// Check for power options
{
   return (!_PowerOnExecute());
}

//------------------------------------------------------------------------------
//  Gsm module and uart port power off
//------------------------------------------------------------------------------

static void _PowerOff( void)
// Power off
{
   if( MGsmStatus() > MGSM_STATUS_ON){
      GsmShutdown();
   }
   GsmDeinit();
   GsmPowerOff();
   _GsmValidPin = NO;
} // _PowerOff

//------------------------------------------------------------------------------
//  Gsm module power on
//------------------------------------------------------------------------------

static void _PowerOn(void)
// Power on
{
   GsmPowerOn();
}


//------------------------------------------------------------------------------
// Internet service commands
#ifndef __WIN32__
TYesNo MGsmSocketOpen(SOCKET sock, const struct sockaddr_in *foreignAddr, byte protocol, fnet_tcp_sockopt_t *tcpOpt)
// Open client socket to <foreignAddr>
{
TYesNo status;
char portStr[6];
char addrStr[FNET_IP4_ADDR_STR_SIZE];

   assert( foreignAddr);

   if( !MutexTryLock()){
      return NO;
   }
   status = (protocol == FNET_IP_PROTOCOL_TCP);
   sprintf(portStr, "%d", foreignAddr->sin_port);
   fnet_inet_ntoa(foreignAddr->sin_addr, addrStr);
   if(status && tcpOpt){
      // tcp with options
      status = GsmNetSocketSetup( sock, addrStr, portStr, status, tcpOpt->keep_idle, tcpOpt->keep_cnt, tcpOpt->keep_intvl);
   } else {
      status = GsmNetSocketSetup( sock, addrStr, portStr, status, 0, 0, 0);
   }
   if(!status){
      MutexFreeLock();
      return NO;
   }
   if( !GsmNetSocketOpen( sock)){
      MutexFreeLock();
      return NO;
   }

   MutexFreeLock();
   return YES;
}

TYesNo MGsmSocketClose(SOCKET sock)
// close socket
{
TYesNo ret;

   MutexGetLock();
   ret = GsmNetSocketClose(sock);
   MutexFreeLock();
   return ret;
}

TYesNo MGsmSocketAddress(SOCKET sock, struct sockaddr_in *localAddr)
// return socket address
{
char portStr[6];
char addrStr[FNET_IP4_ADDR_STR_SIZE];

   MutexGetLock();
   if( !GsmNetGetSocketAddress( sock, addrStr, portStr)){
      MutexFreeLock();
      return NO;
   }
   localAddr->sin_family = AF_INET;
   fnet_inet_aton( addrStr, &localAddr->sin_addr);
   localAddr->sin_port = (unsigned short)strtol( portStr, NULL, 0);
   MutexFreeLock();
   return YES;
}

fnet_socket_state_t MGsmSocketState(SOCKET sock)
// get socket state
{
TGsmNetSocketState   state;
fnet_socket_state_t  sstate;

   if( !MutexTryLock()){
      return SS_CONNECTING;
   }
   sstate = SS_CONNECTING;
   state = GsmNetSocketState( sock);
   switch(state){
      case STATE_UP :
         sstate = SS_CONNECTED;
         break;
      case STATE_CLOSING :
      case STATE_DOWN :
      case STATE_ALOCATED :
         sstate = SS_UNCONNECTED;
         break;
      default :
         break;
   }

   MutexFreeLock();
   return sstate;
}

int MGsmSocketSend(SOCKET sock, byte *buf, int len)
// send data to socket
{
int size;

   if( !MutexTryLock()){
      return 0;
   }
   size = GsmNetWrite( sock, buf, len);
   MutexFreeLock();
   return size;
}

int MGsmSocketReceive(SOCKET sock, byte *buf, int maxlen)
// read data from socket
{
int size;
int curSize;

   if( !MutexTryLock()){
         return 0;
   }

   size  = curSize = 0;
   do{
      curSize = GsmNetRead( sock, buf, maxlen - size);
      if( curSize < 0){
         MutexFreeLock();
         return size == 0? curSize : size;
      }
      size += curSize;
   } while( curSize != 0 && size < maxlen);
   MutexFreeLock();
   return size;
}

static int fnet_netif_3G_stats( struct fnet_netif *netif, struct fnet_netif_statistics *statistics )
{
   if( netif != &fnet_3G_if){
      return FNET_ERR;
   }
   statistics->rx_packet = iface3G.RxCount;
   statistics->tx_packet = iface3G.TxCount;
   return FNET_OK;
}

static int fnet_netif_3G_is_connected( struct fnet_netif *netif)
{
   return iface3G.connected;
}
#endif
