//******************************************************************************
//
//   MenuGsmPowerOptions.h  GSM power options menu
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmPowerOptions_H__
   #define __MenuGsmPowerOptions_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuGsmPowerOptions( void);
// Menu gsm power options

#endif
