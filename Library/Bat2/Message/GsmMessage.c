//******************************************************************************
//
//   GsmMessage.c  GSM messages configuration
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "GsmMessage.h"

TGsmMessage GsmMessage;

const TGsmMessage GsmMessageDefault = {
   //  Commands
   {
      /* Enabled */           NO,
      /* CheckPhoneNumber */  YES,
      /* Expiration */        GSM_COMMANDS_EXPIRATION_DEFAULT
   },
   //  GsmEvents
   {
      /* EventMask */         0
   },
   //  GsmPowerOptions
   {
      /* Mode */              GSM_POWER_MODE_OFF,
      /* SwitchOnPeriod */    GSM_POWER_OPTIONS_SWITCH_ON_PERIOD_DEFAULT,
      /* SwitchOnDuration */  GSM_POWER_OPTIONS_SWITCH_ON_DURATION_DEFAULT,
      /* Times */             {TIME_RANGE_FROM_DEFAULT, TIME_RANGE_TO_DEFAULT},
      /* TimesCount */        1
   },
};

//------------------------------------------------------------------------------
//  GSM contact
//------------------------------------------------------------------------------

const TGsmContact GsmContactDefault = {
   /* Name */           GSM_CONTACT_NAME_DEFAULT,
   /* PhoneNumber */    GSM_CONTACT_PHONE_NUMBER_DEFAULT,
   /* SmsFormat */      GSM_SMS_FORMAT_MOBILE_PHONE,
   /* Statistics */     NO,
   /* Commands */       NO,
   /* Events */         NO,
   /* SendHistogram */  NO
};
