//******************************************************************************
//
//   MenuGsmContact.h  GSM contact menu
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmContact_H__
   #define __MenuGsmContact_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __GsmMessageDef_H__
   #include "Message/GsmMessageDef.h"
#endif


void MenuGsmContact( TGsmContact *Contact);
// Menu gsm contact

#endif
