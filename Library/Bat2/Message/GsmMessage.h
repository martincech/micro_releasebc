//******************************************************************************
//
//   GsmMessage.h  GSM messages configuration
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GsmMessage_H__
   #define __GsmMessage_H__

#ifndef __GsmMessageDef_H__
   #include "Message/GsmMessageDef.h"
#endif

extern       TGsmMessage GsmMessage;
extern const TGsmMessage GsmMessageDefault;

extern const TGsmContact GsmContactDefault;

#endif
