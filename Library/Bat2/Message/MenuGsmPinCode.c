//******************************************************************************
//
//   MenuGsmPinCode.c  GSM PIN enter
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmPinCode.h"
#include "Sound/Beep.h"           // Beep
#include "Gadget/DInput.h"        // Input text
#include "Gadget/DMsg.h"          // Message box
#include "Gadget/DEvent.h"        // Display events
#include "Message/MGsm.h"         // GSM module services
#include "Gsm/GsmDef.h"           // GSM constants

#include "Str.h"                  // Strings
#include <string.h>

//------------------------------------------------------------------------------
//  Menu GSM PIN
//------------------------------------------------------------------------------

void MenuGsmPinCode( void)
// Menu GSM PIN enter
{
char        PinCode[ GSM_PIN_SIZE_MAX + 1];
EMGsmStatus Status;
TYesNo      Redraw;

   MGsmPowerOn();
   DMsgWait();
   Redraw = YES;
   forever {
      if( Redraw){
         Status = MGsmStatus();
         switch( Status){
            case MGSM_STATUS_ON :
            case MGSM_STATUS_RECEIVE :
               break;

            case MGSM_STATUS_READY :
               DMsgOk( STR_PIN_CODE, STR_PIN_CODE_ALREADY_ENTERED, 0);
               MGsmPowerResume();
               return;

            default :
            case MGSM_STATUS_OFF :
            case MGSM_STATUS_SEND :
               break;
            case MGSM_STATUS_ERROR :
               MGsmPowerResume();
               if( !DMsgOkCancel( STR_PIN_CODE, "STR_GSM_COMMUNICATION_ERROR", "STR_RETRY")){
                  return;
               }
               MGsmPowerOn();
               break;
            case MGSM_STATUS_WAIT_PIN :
               strcpy( PinCode, "1234");
               if( !DInputText( STR_PIN_CODE, STR_PIN_CODE, PinCode, GSM_PIN_SIZE_MAX, NO)){
                  MGsmPowerResume();
                  return;
               }
               DMsgWait();
               if( !MGsmPinEnter( PinCode)){
                  DMsgOk( STR_PIN_CODE, STR_UNABLE_SET_PIN, 0);
                  MGsmPowerResume();
                  return;
               }
               DMsgOk( STR_PIN_CODE, STR_PIN, STR_VALID);
               MGsmPowerResume();
               return;
         }
         Redraw = NO;
      }
      switch( DEventWait()){
         case K_FLASH1 :
            Redraw = YES;
            break;

         case K_ENTER :
         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            MGsmPowerResume();         // resume by current power mode
            return;
      }
   }
} // MenuGsmPinCode
