//******************************************************************************
//
//   ContactList.h  Contacts list
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ContactList_H__
   #define __ContactList_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __GsmMessageDef_H__
   #include "Message/GsmMessageDef.h"
#endif

#ifndef __uList_H__
   #include "Data/uList.h"
#endif

typedef UList  TContactList;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif
TYesNo ContactListRemoteLoad( void);
// Load from remote device

TYesNo ContactListRemoteSave( void);
// Save to remote device

void ContactListInit( void);
// Initialize

TYesNo ContactListOpen( TContactList *ContactList);
// Open contact list

void ContactListClose( TContactList *ContactList);
// Close contact list

TContactIndex ContactListCount( TContactList *ContactList);
// Returns actual list items count

TContactIndex ContactListCapacity( void);
// Returns maximum list items count

TContactIndex ContactListAdd( TContactList *ContactList, TGsmContact *Contact);
// Add <Contact> returns index

void ContactListDelete( TContactList *ContactList, TContactIndex Index);
// Add <Contact> returns index

void ContactListLoad( TContactList *ContactList, TGsmContact *Contact, TContactIndex Index);
// Load <Contact> by <Index>

void ContactListSave( TContactList *ContactList, TGsmContact *Contact, TContactIndex Index);
// Save <Contact> at <Index>

TYesNo ContactListLoadByPhoneNumber( TContactList *ContactList, TGsmContact *Contact, char *PhoneNumber);
// Load <Contact> by <ContactListLoadByPhoneNumber>

//------------------------------------------------------------------------------

TYesNo ContactListCommandEnabled( TContactList *ContactList, char *PhoneNumber);
// Returns YES on enabled command by <PhoneNumber>

EGsmSmsFormat ContactListSmsFormat( TContactList *ContactList, char *PhoneNumber);
// Returns EGsmSmsFormat of contact with PhoneNumber, if no contact with this PhoneNumber then _GSM_SMS_FORMAT_LAST is returned

TYesNo ContactListStatisticEnabled( TContactList *ContactList, TContactIndex Index);
// Returns YES for enabled statitistics at <Index>

TYesNo ContactListEventEnabled( TContactList *ContactList, TContactIndex Index);
// Returns YES for enabled events at <Index>

void ContactListPhoneNumber( TContactList *ContactList, char *PhoneNumber, TContactIndex Index);
// Returns <PhoneNumber> at <Index>

#ifdef __cplusplus
   }
#endif

#endif
