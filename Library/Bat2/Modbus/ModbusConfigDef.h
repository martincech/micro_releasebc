//******************************************************************************
//
//   ModbusConfigDef.h     MOdbus protocol configuration options
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusConfigDef_H__
   #define __ModbusConfigDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

typedef struct {
   byte                    Mode;
   byte                    Address;
   dword                   BaudRate;
   byte                    Parity;
} TModbusModuleOptions;


#endif // __ModbusConfigDef_H__


