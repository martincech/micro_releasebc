//******************************************************************************
//
//   ModbusPredefinedWeighingsGroup.h  Modbus Predefined weighings register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusPredefinedWeighingsGroup_H__
   #define __ModbusPredefinedWeighingsGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadPredefinedWeighings( EModbusRegNum R);
// Read Predefined weighings register group


TYesNo ModbusRegWritePredefinedWeighings( EModbusRegNum R, word D);
// Write Predefined weighings register group



#endif
