//******************************************************************************
//
//   ModbusRegRangeCheck.c      Modbus write registers range check functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusRegRangeCheck.h"




// Locals :


//------------------------------------------------------------------------------
//  Configuration
//------------------------------------------------------------------------------

TYesNo ModbusRegRangeCheckConfiguration( EModbusRegNum R, word D)
// check range for Configuration registers
{
   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_10){
      if( D >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_MIN && D <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_MAX){
         return YES;
      }
      return NO;
   }

   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_MIN_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_MIN_10){
      if( D >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_MIN_MIN && D <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_MIN_MAX){
         return YES;
      }
      return NO;
   }

   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_HOUR_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_HOUR_10){
      if( D >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_HOUR_MIN && D <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_HOUR_MAX){
         return YES;
      }
      return NO;
   }

   if( R >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_MIN_1 && R <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_MIN_10){
      if( D >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_MIN_MIN && D <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_TO_MIN_MAX){
         return YES;
      }
      return NO;
   }

   switch ( R){

      case MODBUS_REG_CONFIGURATION_SCALE_NAME :
         if(( D >= MODBUS_REG_CONFIGURATION_SCALE_NAME_MIN) && ( D <= MODBUS_REG_CONFIGURATION_SCALE_NAME_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_DISPLAY_BACKLIGHT :
         if(( D >= MODBUS_REG_CONFIGURATION_DISPLAY_BACKLIGHT_MIN) && ( D <= MODBUS_REG_CONFIGURATION_DISPLAY_BACKLIGHT_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_DISPLAY_CONTRAST :
         if(( D >= MODBUS_REG_CONFIGURATION_DISPLAY_CONTRAST_MIN) && ( D <= MODBUS_REG_CONFIGURATION_DISPLAY_CONTRAST_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_DISPLAY_MODE :
         if(( D >= MODBUS_REG_CONFIGURATION_DISPLAY_MODE_MIN) && ( D <= MODBUS_REG_CONFIGURATION_DISPLAY_MODE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_DISPLAY_SAVE :
         if(( D >= MODBUS_REG_CONFIGURATION_DISPLAY_SAVE_MIN) && ( D <= MODBUS_REG_CONFIGURATION_DISPLAY_SAVE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_WEIGHING_UNITS :
         if(( D >= MODBUS_REG_CONFIGURATION_WEIGHING_UNITS_MIN) && ( D <= MODBUS_REG_CONFIGURATION_WEIGHING_UNITS_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_WEIGHING_DIVISION :
         if(( D >= MODBUS_REG_CONFIGURATION_WEIGHING_DIVISION_MIN) && ( D <= MODBUS_REG_CONFIGURATION_WEIGHING_DIVISION_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_WEIGHING_RANGE :
         if(( D >= MODBUS_REG_CONFIGURATION_WEIGHING_RANGE_MIN) && ( D <= MODBUS_REG_CONFIGURATION_WEIGHING_RANGE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_WEIGHING_SAVE :
         if(( D >= MODBUS_REG_CONFIGURATION_WEIGHING_SAVE_MIN) && ( D <= MODBUS_REG_CONFIGURATION_WEIGHING_SAVE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_LOCALIZATION_LANGUAGE :
         if(( D >= MODBUS_REG_CONFIGURATION_LOCALIZATION_LANGUAGE_MIN) && ( D <= MODBUS_REG_CONFIGURATION_LOCALIZATION_LANGUAGE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_LOCALIZATION_COUNTRY :
         if(( D >= MODBUS_REG_CONFIGURATION_LOCALIZATION_COUNTRY_MIN) && ( D <= MODBUS_REG_CONFIGURATION_LOCALIZATION_COUNTRY_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_FORMAT :
         if(( D >= MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_FORMAT_MIN) && ( D <= MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_FORMAT_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_SEPARATOR_FIRST :
         if(( D >= MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_SEPARATOR_FIRST_MIN) && ( D <= MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_SEPARATOR_FIRST_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_SEPARATOR_SECOND :
         if(( D >= MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_SEPARATOR_SECOND_MIN) && ( D <= MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_SEPARATOR_SECOND_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_LOCALIZATION_TIME_FORMAT :
         if(( D >= MODBUS_REG_CONFIGURATION_LOCALIZATION_TIME_FORMAT_MIN) && ( D <= MODBUS_REG_CONFIGURATION_LOCALIZATION_TIME_FORMAT_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_LOCALIZATION_TIME_SEPARATOR :
         if(( D >= MODBUS_REG_CONFIGURATION_LOCALIZATION_TIME_SEPARATOR_MIN) && ( D <= MODBUS_REG_CONFIGURATION_LOCALIZATION_TIME_SEPARATOR_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_LOCALIZATION_DAYLIGHT_SAVING :
         if(( D >= MODBUS_REG_CONFIGURATION_LOCALIZATION_DAYLIGHT_SAVING_MIN) && ( D <= MODBUS_REG_CONFIGURATION_LOCALIZATION_DAYLIGHT_SAVING_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_LOCALIZATION_SAVE :
         if(( D >= MODBUS_REG_CONFIGURATION_LOCALIZATION_SAVE_MIN) && ( D <= MODBUS_REG_CONFIGURATION_LOCALIZATION_SAVE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_DATE_TIME_DAY :
         if(( D >= MODBUS_REG_CONFIGURATION_DATE_TIME_DAY_MIN) && ( D <= MODBUS_REG_CONFIGURATION_DATE_TIME_DAY_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_DATE_TIME_MONTH :
         if(( D >= MODBUS_REG_CONFIGURATION_DATE_TIME_MONTH_MIN) && ( D <= MODBUS_REG_CONFIGURATION_DATE_TIME_MONTH_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_DATE_TIME_YEAR :
         if(( D >= MODBUS_REG_CONFIGURATION_DATE_TIME_YEAR_MIN) && ( D <= MODBUS_REG_CONFIGURATION_DATE_TIME_YEAR_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_DATE_TIME_HOUR :
         if(( D >= MODBUS_REG_CONFIGURATION_DATE_TIME_HOUR_MIN) && ( D <= MODBUS_REG_CONFIGURATION_DATE_TIME_HOUR_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_DATE_TIME_MIN :
         if(( D >= MODBUS_REG_CONFIGURATION_DATE_TIME_MIN_MIN) && ( D <= MODBUS_REG_CONFIGURATION_DATE_TIME_MIN_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_DATE_TIME_SAVE :
         if(( D >= MODBUS_REG_CONFIGURATION_DATE_TIME_SAVE_MIN) && ( D <= MODBUS_REG_CONFIGURATION_DATE_TIME_SAVE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_GSM_POWER_OPTIONS :
         if(( D >= MODBUS_REG_CONFIGURATION_GSM_POWER_OPTIONS_MIN) && ( D <= MODBUS_REG_CONFIGURATION_GSM_POWER_OPTIONS_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_PERIOD :
         if(( D >= MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_PERIOD_MIN) && ( D <= MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_PERIOD_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_DURATION :
         if(( D >= MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_DURATION_MIN) && ( D <= MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_DURATION_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_GSM_POWER_TIMES_COUNT :
         if(( D >= MODBUS_REG_CONFIGURATION_GSM_POWER_TIMES_COUNT_MIN) && ( D <= MODBUS_REG_CONFIGURATION_GSM_POWER_TIMES_COUNT_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_GSM_EVENTS_EVENT_MASK :
         if(( D >= MODBUS_REG_CONFIGURATION_GSM_EVENTS_EVENT_MASK_MIN) && ( D <= MODBUS_REG_CONFIGURATION_GSM_EVENTS_EVENT_MASK_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_CONFIGURATION_GSM_SAVE :
         if(( D >= MODBUS_REG_CONFIGURATION_GSM_SAVE_MIN) && ( D <= MODBUS_REG_CONFIGURATION_GSM_SAVE_MAX)){
            return YES;
         }
         return NO;

      default :
         return NO;
   }


} // ModbusRegRangeCheckConfiguration

//------------------------------------------------------------------------------
//  Gsm Contacts
//------------------------------------------------------------------------------

TYesNo ModbusRegRangeCheckGsmContacts( EModbusRegNum R, word D)
// check range for Gsm Contacts registers
{
   switch ( R){

      case MODBUS_REG_GSM_CONTACTS_INDEX :
         if(( D >= MODBUS_REG_GSM_CONTACTS_INDEX_MIN) && ( D <= MODBUS_REG_GSM_CONTACTS_INDEX_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_GSM_CONTACTS_NAME :
         if(( D >= MODBUS_REG_GSM_CONTACTS_NAME_MIN) && ( D <= MODBUS_REG_GSM_CONTACTS_NAME_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_GSM_CONTACTS_PHONE_NUMBER :
         if(( D >= MODBUS_REG_GSM_CONTACTS_PHONE_NUMBER_MIN) && ( D <= MODBUS_REG_GSM_CONTACTS_PHONE_NUMBER_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_GSM_CONTACTS_SMS_FORMAT :
         if(( D >= MODBUS_REG_GSM_CONTACTS_SMS_FORMAT_MIN) && ( D <= MODBUS_REG_GSM_CONTACTS_SMS_FORMAT_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_GSM_CONTACTS_ALLOW_STATISTICS :
         if(( D >= MODBUS_REG_GSM_CONTACTS_ALLOW_STATISTICS_MIN) && ( D <= MODBUS_REG_GSM_CONTACTS_ALLOW_STATISTICS_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_GSM_CONTACTS_ALLOW_COMMANDS :
         if(( D >= MODBUS_REG_GSM_CONTACTS_ALLOW_COMMANDS_MIN) && ( D <= MODBUS_REG_GSM_CONTACTS_ALLOW_COMMANDS_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_GSM_CONTACTS_ALLOW_EVENTS :
         if(( D >= MODBUS_REG_GSM_CONTACTS_ALLOW_EVENTS_MIN) && ( D <= MODBUS_REG_GSM_CONTACTS_ALLOW_EVENTS_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_GSM_CONTACTS_SEND_HISTOGRAM :
         if(( D >= MODBUS_REG_GSM_CONTACTS_SEND_HISTOGRAM_MIN) && ( D <= MODBUS_REG_GSM_CONTACTS_SEND_HISTOGRAM_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_GSM_CONTACTS_CREATE :
         if(( D >= MODBUS_REG_GSM_CONTACTS_CREATE_MIN) && ( D <= MODBUS_REG_GSM_CONTACTS_CREATE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_GSM_CONTACTS_SAVE :
         if(( D >= MODBUS_REG_GSM_CONTACTS_SAVE_MIN) && ( D <= MODBUS_REG_GSM_CONTACTS_SAVE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_GSM_CONTACTS_DELETE :
         if(( D >= MODBUS_REG_GSM_CONTACTS_DELETE_MIN) && ( D <= MODBUS_REG_GSM_CONTACTS_DELETE_MAX)){
            return YES;
         }
         return NO;

      default :
         return NO;
   }


} // ModbusRegRangeCheckGsmContacts

//------------------------------------------------------------------------------
//  SMS Gate
//------------------------------------------------------------------------------

TYesNo ModbusRegRangeCheckSmsGate( EModbusRegNum R, word D)
// check range for SMS Gate registers
{
   switch ( R){

      case MODBUS_REG_SMS_GATE_SMS_SLOT_NUMBER :
         if(( D >= MODBUS_REG_SMS_GATE_SMS_SLOT_NUMBER_MIN) && ( D <= MODBUS_REG_SMS_GATE_SMS_SLOT_NUMBER_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_SMS_GATE_SMS_TEXT :
         if(( D >= MODBUS_REG_SMS_GATE_SMS_TEXT_MIN) && ( D <= MODBUS_REG_SMS_GATE_SMS_TEXT_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_SMS_GATE_SMS_PHONE_NUMBER :
         if(( D >= MODBUS_REG_SMS_GATE_SMS_PHONE_NUMBER_MIN) && ( D <= MODBUS_REG_SMS_GATE_SMS_PHONE_NUMBER_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_SMS_GATE_SEND :
         if(( D >= MODBUS_REG_SMS_GATE_SEND_MIN) && ( D <= MODBUS_REG_SMS_GATE_SEND_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_SMS_GATE_SENDTO :
         if(( D >= MODBUS_REG_SMS_GATE_SENDTO_MIN) && ( D <= MODBUS_REG_SMS_GATE_SENDTO_MAX)){
            return YES;
         }
         return NO;

      default :
         return NO;
   }


} // ModbusRegRangeCheckSmsGate

//------------------------------------------------------------------------------
//  Predefined Growth Curves
//------------------------------------------------------------------------------

TYesNo ModbusRegRangeCheckPredefinedGrowthCurves( EModbusRegNum R, word D)
// check range for Predefined Growth Curves registers
{
   if( R >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_DAY_1 && R <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_DAY_30){
      if( D >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_DAY_MIN && D <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_DAY_MAX){
         return YES;
      }
      return NO;
   }

   if( R >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_1 && R <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_30){
      if( D >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_MIN && D <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_MAX){
         return YES;
      }
      return NO;
   }

   switch ( R){

      case MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_INDEX :
         if(( D >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_INDEX_MIN) && ( D <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_INDEX_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_NAME :
         if(( D >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_NAME_MIN) && ( D <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_NAME_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_GROWTH_CURVES_CREATE :
         if(( D >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CREATE_MIN) && ( D <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CREATE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_GROWTH_CURVES_SAVE :
         if(( D >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_SAVE_MIN) && ( D <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_SAVE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_GROWTH_CURVES_DELETE :
         if(( D >= MODBUS_REG_PREDEFINED_GROWTH_CURVES_DELETE_MIN) && ( D <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_DELETE_MAX)){
            return YES;
         }
         return NO;

      default :
         return NO;
   }


} // ModbusRegRangeCheckPredefinedGrowthCurves

//------------------------------------------------------------------------------
//  Predefined Correction Curves
//------------------------------------------------------------------------------

TYesNo ModbusRegRangeCheckPredefinedCorrectionCurves( EModbusRegNum R, word D)
// check range for Predefined Correction Curves registers
{
   if( R >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_DAY_1 && R <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_DAY_10){
      if( D >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_DAY_MIN && D <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_DAY_MAX){
         return YES;
      }
      return NO;
   }

   if( R >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_1 && R <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_10){
      if( D >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_MIN && D <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_MAX){
         return YES;
      }
      return NO;
   }

   switch ( R){

      case MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_INDEX :
         if(( D >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_INDEX_MIN) && ( D <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_INDEX_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_NAME :
         if(( D >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_NAME_MIN) && ( D <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_NAME_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CREATE :
         if(( D >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CREATE_MIN) && ( D <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CREATE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_CORRECTION_CURVES_SAVE :
         if(( D >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_SAVE_MIN) && ( D <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_SAVE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_CORRECTION_CURVES_DELETE :
         if(( D >= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_DELETE_MIN) && ( D <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_DELETE_MAX)){
            return YES;
         }
         return NO;

      default :
         return NO;
   }


} // ModbusRegRangeCheckPredefinedCorrectionCurves

//------------------------------------------------------------------------------
//  Predefined Weighing Plans
//------------------------------------------------------------------------------

TYesNo ModbusRegRangeCheckPredefinedWeighingPlans( EModbusRegNum R, word D)
// check range for Predefined Weighing Plans registers
{
   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_20){
      if( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_MIN && D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_MAX){
         return YES;
      }
      return NO;
   }

   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_MIN_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_MIN_20){
      if( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_MIN_MIN && D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_MIN_MAX){
         return YES;
      }
      return NO;
   }

   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_HOUR_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_HOUR_20){
      if( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_HOUR_MIN && D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_HOUR_MAX){
         return YES;
      }
      return NO;
   }

   if( R >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_1 && R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_20){
      if( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_MIN && D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_MAX){
         return YES;
      }
      return NO;
   }

   switch ( R){

      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_PLAN_INDEX :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_PLAN_INDEX_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_PLAN_INDEX_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_NAME :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_NAME_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_NAME_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TYPE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TYPE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TYPE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_OF_WEEK :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_OF_WEEK_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_OF_WEEK_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_WEIGHING :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_WEIGHING_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_WEIGHING_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_SUSPEND :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_SUSPEND_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_SUSPEND_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_START :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_START_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_START_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_CREATE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_CREATE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_CREATE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_SAVE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_SAVE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_SAVE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DELETE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DELETE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DELETE_MAX)){
            return YES;
         }
         return NO;

      default :
         return NO;
   }


} // ModbusRegRangeCheckPredefinedWeighingPlans

//------------------------------------------------------------------------------
//  Statistics
//------------------------------------------------------------------------------

TYesNo ModbusRegRangeCheckStatistics( EModbusRegNum R, word D)
// check range for Statistics registers
{
   switch ( R){

      case MODBUS_REG_STATISTICS_DAYNUMBER :
         if(( D >= MODBUS_REG_STATISTICS_DAYNUMBER_MIN) && ( D <= MODBUS_REG_STATISTICS_DAYNUMBER_MAX)){
            return YES;
         }
         return NO;

      default :
         return NO;
   }


} // ModbusRegRangeCheckStatistics

//------------------------------------------------------------------------------
//  Predefined weighings
//------------------------------------------------------------------------------

TYesNo ModbusRegRangeCheckPredefinedWeighings( EModbusRegNum R, word D)
// check range for Predefined weighings registers
{
   switch ( R){

      case MODBUS_REG_PREDEFINED_WEIGHINGS_PREDEF_ID :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_PREDEF_ID_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_PREDEF_ID_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_NAME :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_NAME_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_NAME_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_FLOCK_NAME :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_FLOCK_NAME_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_FLOCK_NAME_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_MENU_CHANGES :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_MENU_CHANGES_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_MENU_CHANGES_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALDAYNUMBER :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALDAYNUMBER_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALDAYNUMBER_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_DAYSTART :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_DAYSTART_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_DAYSTART_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_USEBOTHGENDERS :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_USEBOTHGENDERS_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_USEBOTHGENDERS_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALWFEMALES :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALWFEMALES_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALWFEMALES_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALWMALES :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALWMALES_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALWMALES_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_USECURVES :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_USECURVES_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_USECURVES_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_MODE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_MODE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_MODE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_WEIGH_PLAN_PREDEFINED :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_WEIGH_PLAN_PREDEFINED_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_WEIGH_PLAN_PREDEFINED_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_F_PREDEFINED :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_F_PREDEFINED_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_F_PREDEFINED_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_M_PREDEFINED :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_M_PREDEFINED_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_M_PREDEFINED_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_FILTER :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_FILTER_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_FILTER_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_STABILIZATIONRANGE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_STABILIZATIONRANGE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_STABILIZATIONRANGE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_STABILIZATIONTIME :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_STABILIZATIONTIME_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_STABILIZATIONTIME_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_JUMPMODE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_JUMPMODE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_JUMPMODE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_FEMALES :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_FEMALES_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_FEMALES_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_MALES :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_MALES_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_MALES_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_FEMALES :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_FEMALES_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_FEMALES_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_MALES :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_MALES_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_MALES_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_UNIFORMITYRANGE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_UNIFORMITYRANGE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_UNIFORMITYRANGE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_HISTOGRAMRANGE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_HISTOGRAMRANGE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_HISTOGRAMRANGE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_CREATE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_CREATE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_CREATE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_SAVE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_SAVE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_SAVE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_DELETE :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_DELETE_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_DELETE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_SET_DEFAULT :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_SET_DEFAULT_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_SET_DEFAULT_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_PREDEFINED_WEIGHINGS_DISABLE_DEFAULT :
         if(( D >= MODBUS_REG_PREDEFINED_WEIGHINGS_DISABLE_DEFAULT_MIN) && ( D <= MODBUS_REG_PREDEFINED_WEIGHINGS_DISABLE_DEFAULT_MAX)){
            return YES;
         }
         return NO;

      default :
         return NO;
   }


} // ModbusRegRangeCheckPredefinedWeighings

//------------------------------------------------------------------------------
//  Weighing
//------------------------------------------------------------------------------

TYesNo ModbusRegRangeCheckWeighing( EModbusRegNum R, word D)
// check range for Weighing registers
{
   switch ( R){

      case MODBUS_REG_WEIGHING_PREDEF_ID :
         if(( D >= MODBUS_REG_WEIGHING_PREDEF_ID_MIN) && ( D <= MODBUS_REG_WEIGHING_PREDEF_ID_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_START_AT_DAY :
         if(( D >= MODBUS_REG_WEIGHING_START_AT_DAY_MIN) && ( D <= MODBUS_REG_WEIGHING_START_AT_DAY_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_START_AT_MONTH :
         if(( D >= MODBUS_REG_WEIGHING_START_AT_MONTH_MIN) && ( D <= MODBUS_REG_WEIGHING_START_AT_MONTH_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_START_AT_YEAR :
         if(( D >= MODBUS_REG_WEIGHING_START_AT_YEAR_MIN) && ( D <= MODBUS_REG_WEIGHING_START_AT_YEAR_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_START_AT_HOUR :
         if(( D >= MODBUS_REG_WEIGHING_START_AT_HOUR_MIN) && ( D <= MODBUS_REG_WEIGHING_START_AT_HOUR_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_START_AT_MIN :
         if(( D >= MODBUS_REG_WEIGHING_START_AT_MIN_MIN) && ( D <= MODBUS_REG_WEIGHING_START_AT_MIN_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_START :
         if(( D >= MODBUS_REG_WEIGHING_START_MIN) && ( D <= MODBUS_REG_WEIGHING_START_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_START_AT :
         if(( D >= MODBUS_REG_WEIGHING_START_AT_MIN) && ( D <= MODBUS_REG_WEIGHING_START_AT_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_START_DIAGNOSTICS :
         if(( D >= MODBUS_REG_WEIGHING_START_DIAGNOSTICS_MIN) && ( D <= MODBUS_REG_WEIGHING_START_DIAGNOSTICS_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_START_CALIBRATION :
         if(( D >= MODBUS_REG_WEIGHING_START_CALIBRATION_MIN) && ( D <= MODBUS_REG_WEIGHING_START_CALIBRATION_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_PAUSE :
         if(( D >= MODBUS_REG_WEIGHING_PAUSE_MIN) && ( D <= MODBUS_REG_WEIGHING_PAUSE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_UNPAUSE :
         if(( D >= MODBUS_REG_WEIGHING_UNPAUSE_MIN) && ( D <= MODBUS_REG_WEIGHING_UNPAUSE_MAX)){
            return YES;
         }
         return NO;

      case MODBUS_REG_WEIGHING_STOP :
         if(( D >= MODBUS_REG_WEIGHING_STOP_MIN) && ( D <= MODBUS_REG_WEIGHING_STOP_MAX)){
            return YES;
         }
         return NO;

      default :
         return NO;
   }


} // ModbusRegRangeCheckWeighing

//------------------------------------------------------------------------------
//  Character buffer
//------------------------------------------------------------------------------

TYesNo ModbusRegRangeCheckCharacterBuffer( EModbusRegNum R, word D)
// check range for Character buffer registers
{
   if( R >= MODBUS_REG_CHARACTER_BUFFER_CHAR_0_1 && R <= MODBUS_REG_CHARACTER_BUFFER_CHAR_126_127){
      if( D >= MODBUS_REG_CHARACTER_BUFFER_CHAR_MIN && D <= MODBUS_REG_CHARACTER_BUFFER_CHAR_MAX){
         return YES;
      }
      return NO;
   }


   return NO;
} // ModbusRegRangeCheckCharacterBuffer




