//******************************************************************************
//
//   ModbusWeighingGroup.h  Modbus Weighing register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusWeighingGroup_H__
   #define __ModbusWeighingGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadWeighing( EModbusRegNum R);
// Read Weighing register group


TYesNo ModbusRegWriteWeighing( EModbusRegNum R, word D);
// Write Weighing register group



#endif
