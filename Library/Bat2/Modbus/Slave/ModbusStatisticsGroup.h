//******************************************************************************
//
//   ModbusStatisticsGroup.h  Modbus Statistics register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusStatisticsGroup_H__
   #define __ModbusStatisticsGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadStatistics( EModbusRegNum R);
// Read Statistics register group


TYesNo ModbusRegWriteStatistics( EModbusRegNum R, word D);
// Write Statistics register group



#endif
