//******************************************************************************
//
//   ModbusReg.c      Modbus register map and access functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusReg.h"
#include "ModbusConfigurationGroup.h"
#include "ModbusGsmContactsGroup.h"
#include "ModbusSmsGateGroup.h"
#include "ModbusPredefinedGrowthCurvesGroup.h"
#include "ModbusPredefinedCorrectionCurvesGroup.h"
#include "ModbusPredefinedWeighingPlansGroup.h"
#include "ModbusStatisticsGroup.h"
#include "ModbusCalibrationGroup.h"
#include "ModbusPredefinedWeighingsGroup.h"
#include "ModbusWeighingGroup.h"
#include "ModbusCharacterBufferGroup.h"
#include <string.h>


TModbusRWBuffer WriteValuesBuffer;
TModbusVersion ModbusVersion = { 1, 0};


// Locals :


//------------------------------------------------------------------------------
//  Register init
//------------------------------------------------------------------------------
void ModbusRegsInit( void)
// init modbus registers
{
memset( &WriteValuesBuffer, 0, sizeof(WriteValuesBuffer));
}

//------------------------------------------------------------------------------
//  Check correct read register number
//------------------------------------------------------------------------------
TYesNo ModbusRegReadIsCorrectNum( EModbusRegNum R)
// checks read register number and return YES/NO whether its number is correct or not
{
   if(         
      // Configuration
      (R < MODBUS_REG_CONFIGURATION_VERSION ||
       R > MODBUS_REG_CONFIGURATION_SCALE_NAME) && 
      (R < MODBUS_REG_CONFIGURATION_DISPLAY_BACKLIGHT ||
       R > MODBUS_REG_CONFIGURATION_DISPLAY_MODE) && 
      (R < MODBUS_REG_CONFIGURATION_WEIGHING_UNITS ||
       R > MODBUS_REG_CONFIGURATION_WEIGHING_RANGE) && 
      (R < MODBUS_REG_CONFIGURATION_LOCALIZATION_LANGUAGE ||
       R > MODBUS_REG_CONFIGURATION_LOCALIZATION_DAYLIGHT_SAVING) && 
      (R < MODBUS_REG_CONFIGURATION_RS485_ADDRESS ||
       R > MODBUS_REG_CONFIGURATION_RS485_PROTOCOL) && 
      (R < MODBUS_REG_CONFIGURATION_DATE_TIME_DAY ||
       R > MODBUS_REG_CONFIGURATION_DATE_TIME_MIN) && 
      (R < MODBUS_REG_CONFIGURATION_GSM_INFO_PIN_STATE ||
       R > MODBUS_REG_CONFIGURATION_GSM_INFO_SIGNAL_STRENGTH) && 
      (R < MODBUS_REG_CONFIGURATION_GSM_POWER_OPTIONS ||
       R > MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_DURATION) && 
      (R < MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_1 ||
       R > MODBUS_REG_CONFIGURATION_GSM_EVENTS_EVENT_MASK) && 
      // Gsm Contacts
      (R < MODBUS_REG_GSM_CONTACTS_INDEX ||
       R > MODBUS_REG_GSM_CONTACTS_SEND_HISTOGRAM) && 
      // SMS Gate
      (R < MODBUS_REG_SMS_GATE_SMS_SLOT_NUMBER ||
       R > MODBUS_REG_SMS_GATE_SMS_STATUS) && 
      // Predefined Growth Curves
      (R < MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_INDEX ||
       R > MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_30) && 
      // Predefined Correction Curves
      (R < MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_INDEX ||
       R > MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_10) && 
      // Predefined Weighing Plans
      (R < MODBUS_REG_PREDEFINED_WEIGHING_PLANS_PLAN_INDEX ||
       R > MODBUS_REG_PREDEFINED_WEIGHING_PLANS_NAME) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TYPE ||
       R > MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_START) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_1 ||
       R > MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_20) && 
      // Statistics
      (R < MODBUS_REG_STATISTICS_DAYNUMBER ||
       R > MODBUS_REG_STATISTICS_DAYNUMBER) && 
      (R < MODBUS_REG_STATISTICS_STAT_F_COUNT ||
       R > MODBUS_REG_STATISTICS_REALUNIFORMITY_M) && 
      (R < MODBUS_REG_STATISTICS_HIST_F_CENTER_HI ||
       R > MODBUS_REG_STATISTICS_HIST_M_COUNT_39) && 
      // Calibration
      (R < MODBUS_REG_CALIBRATION_ZEROCAL_HI ||
       R > MODBUS_REG_CALIBRATION_DIVISION) && 
      // Predefined weighings
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_PREDEF_ID ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_MENU_CHANGES) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALDAYNUMBER ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_MODE) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_WEIGH_PLAN_PREDEFINED ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_WEIGH_PLAN_PREDEFINED) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_F_PREDEFINED ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_M_PREDEFINED) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_FILTER ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_JUMPMODE) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_FEMALES ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_MALES) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_UNIFORMITYRANGE ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_HISTOGRAMRANGE) && 
      // Weighing
      (R < MODBUS_REG_WEIGHING_PREDEF_ID ||
       R > MODBUS_REG_WEIGHING_PREDEF_ID) && 
      (R < MODBUS_REG_WEIGHING_WEIGHING_STATE ||
       R > MODBUS_REG_WEIGHING_WEIGHING_STATE) && 
      (R < MODBUS_REG_WEIGHING_START_AT_DAY ||
       R > MODBUS_REG_WEIGHING_START_AT_MIN) && 
      // Character buffer
      (R < MODBUS_REG_CHARACTER_BUFFER_CHAR_0_1 ||
       R > MODBUS_REG_CHARACTER_BUFFER_CHAR_126_127)

     ){
     return NO;
   }
   return YES;
}

word ModbusRegRead( EModbusRegNum R)
// read register value
{
   if( R <= MODBUS_REG_CONFIGURATION_GSM_EVENTS_EVENT_MASK){
       return ModbusRegReadConfiguration( R); 
   }
   if( R <= MODBUS_REG_GSM_CONTACTS_SEND_HISTOGRAM){
       return ModbusRegReadGsmContacts( R); 
   }
   if( R <= MODBUS_REG_SMS_GATE_SMS_STATUS){
       return ModbusRegReadSmsGate( R); 
   }
   if( R <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_30){
       return ModbusRegReadPredefinedGrowthCurves( R); 
   }
   if( R <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_10){
       return ModbusRegReadPredefinedCorrectionCurves( R); 
   }
   if( R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_20){
       return ModbusRegReadPredefinedWeighingPlans( R); 
   }
   if( R <= MODBUS_REG_STATISTICS_HIST_M_COUNT_39){
       return ModbusRegReadStatistics( R); 
   }
   if( R <= MODBUS_REG_CALIBRATION_DIVISION){
       return ModbusRegReadCalibration( R); 
   }
   if( R <= MODBUS_REG_PREDEFINED_WEIGHINGS_HISTOGRAMRANGE){
       return ModbusRegReadPredefinedWeighings( R); 
   }
   if( R <= MODBUS_REG_WEIGHING_START_AT_MIN){
       return ModbusRegReadWeighing( R); 
   }
   if( R <= MODBUS_REG_CHARACTER_BUFFER_CHAR_126_127){
       return ModbusRegReadCharacterBuffer( R); 
   }
   return 0;
}

//------------------------------------------------------------------------------
//  Check correct write register number
//------------------------------------------------------------------------------
TYesNo ModbusRegWriteIsCorrectNum( EModbusRegNum R)
// checks write register number and return YES/NO whether its number is correct or not
{
   if(         
      // Configuration
      (R < MODBUS_REG_CONFIGURATION_SCALE_NAME ||
       R > MODBUS_REG_CONFIGURATION_SCALE_NAME) && 
      (R < MODBUS_REG_CONFIGURATION_DISPLAY_BACKLIGHT ||
       R > MODBUS_REG_CONFIGURATION_DISPLAY_MODE) && 
      (R < MODBUS_REG_CONFIGURATION_DISPLAY_SAVE ||
       R > MODBUS_REG_CONFIGURATION_DISPLAY_SAVE) && 
      (R < MODBUS_REG_CONFIGURATION_WEIGHING_UNITS ||
       R > MODBUS_REG_CONFIGURATION_WEIGHING_RANGE) && 
      (R < MODBUS_REG_CONFIGURATION_WEIGHING_SAVE ||
       R > MODBUS_REG_CONFIGURATION_WEIGHING_SAVE) && 
      (R < MODBUS_REG_CONFIGURATION_LOCALIZATION_LANGUAGE ||
       R > MODBUS_REG_CONFIGURATION_LOCALIZATION_COUNTRY) && 
      (R < MODBUS_REG_CONFIGURATION_LOCALIZATION_DATE_FORMAT ||
       R > MODBUS_REG_CONFIGURATION_LOCALIZATION_DAYLIGHT_SAVING) && 
      (R < MODBUS_REG_CONFIGURATION_LOCALIZATION_SAVE ||
       R > MODBUS_REG_CONFIGURATION_LOCALIZATION_SAVE) && 
      (R < MODBUS_REG_CONFIGURATION_DATE_TIME_DAY ||
       R > MODBUS_REG_CONFIGURATION_DATE_TIME_MIN) && 
      (R < MODBUS_REG_CONFIGURATION_DATE_TIME_SAVE ||
       R > MODBUS_REG_CONFIGURATION_DATE_TIME_SAVE) && 
      (R < MODBUS_REG_CONFIGURATION_GSM_POWER_OPTIONS ||
       R > MODBUS_REG_CONFIGURATION_GSM_POWER_SWITCH_ON_DURATION) && 
      (R < MODBUS_REG_CONFIGURATION_GSM_POWER_TIME_FROM_HOUR_1 ||
       R > MODBUS_REG_CONFIGURATION_GSM_POWER_TIMES_COUNT) && 
      (R < MODBUS_REG_CONFIGURATION_GSM_EVENTS_EVENT_MASK ||
       R > MODBUS_REG_CONFIGURATION_GSM_SAVE) && 
      // Gsm Contacts
      (R < MODBUS_REG_GSM_CONTACTS_INDEX ||
       R > MODBUS_REG_GSM_CONTACTS_SEND_HISTOGRAM) && 
      (R < MODBUS_REG_GSM_CONTACTS_CREATE ||
       R > MODBUS_REG_GSM_CONTACTS_DELETE) && 
      // SMS Gate
      (R < MODBUS_REG_SMS_GATE_SMS_SLOT_NUMBER ||
       R > MODBUS_REG_SMS_GATE_SMS_SLOT_NUMBER) && 
      (R < MODBUS_REG_SMS_GATE_SMS_TEXT ||
       R > MODBUS_REG_SMS_GATE_SMS_PHONE_NUMBER) && 
      (R < MODBUS_REG_SMS_GATE_SEND ||
       R > MODBUS_REG_SMS_GATE_SENDTO) && 
      // Predefined Growth Curves
      (R < MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_INDEX ||
       R > MODBUS_REG_PREDEFINED_GROWTH_CURVES_CURVE_WEIGHT_30) && 
      (R < MODBUS_REG_PREDEFINED_GROWTH_CURVES_CREATE ||
       R > MODBUS_REG_PREDEFINED_GROWTH_CURVES_DELETE) && 
      // Predefined Correction Curves
      (R < MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_INDEX ||
       R > MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CURVE_FACTOR_10) && 
      (R < MODBUS_REG_PREDEFINED_CORRECTION_CURVES_CREATE ||
       R > MODBUS_REG_PREDEFINED_CORRECTION_CURVES_DELETE) && 
      // Predefined Weighing Plans
      (R < MODBUS_REG_PREDEFINED_WEIGHING_PLANS_PLAN_INDEX ||
       R > MODBUS_REG_PREDEFINED_WEIGHING_PLANS_NAME) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TYPE ||
       R > MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DAY_TECHNOLOGICAL_START) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_FROM_HOUR_1 ||
       R > MODBUS_REG_PREDEFINED_WEIGHING_PLANS_TIME_TO_MIN_20) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHING_PLANS_CREATE ||
       R > MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DELETE) && 
      // Statistics
      (R < MODBUS_REG_STATISTICS_DAYNUMBER ||
       R > MODBUS_REG_STATISTICS_DAYNUMBER) && 
      // Predefined weighings
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_PREDEF_ID ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_MENU_CHANGES) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_INITIALDAYNUMBER ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_MODE) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_WEIGH_PLAN_PREDEFINED ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_WEIGH_PLAN_PREDEFINED) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_F_PREDEFINED ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_CURVE_M_PREDEFINED) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_FILTER ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_JUMPMODE) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_ABOVE_FEMALES ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_MARGIN_BELOW_MALES) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_UNIFORMITYRANGE ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_HISTOGRAMRANGE) && 
      (R < MODBUS_REG_PREDEFINED_WEIGHINGS_CREATE ||
       R > MODBUS_REG_PREDEFINED_WEIGHINGS_DISABLE_DEFAULT) && 
      // Weighing
      (R < MODBUS_REG_WEIGHING_PREDEF_ID ||
       R > MODBUS_REG_WEIGHING_PREDEF_ID) && 
      (R < MODBUS_REG_WEIGHING_START_AT_DAY ||
       R > MODBUS_REG_WEIGHING_START_AT_MIN) && 
      (R < MODBUS_REG_WEIGHING_START ||
       R > MODBUS_REG_WEIGHING_STOP) && 
      // Character buffer
      (R < MODBUS_REG_CHARACTER_BUFFER_CHAR_0_1 ||
       R > MODBUS_REG_CHARACTER_BUFFER_CHAR_126_127)

     ){
     return NO;
   }
   return YES;
}

TYesNo ModbusRegWrite( EModbusRegNum R, word D)
// write register value
{
   if( R <= MODBUS_REG_CONFIGURATION_GSM_SAVE){
       return ModbusRegWriteConfiguration( R, D); 
   }
   if( R <= MODBUS_REG_GSM_CONTACTS_DELETE){
       return ModbusRegWriteGsmContacts( R, D); 
   }
   if( R <= MODBUS_REG_SMS_GATE_SENDTO){
       return ModbusRegWriteSmsGate( R, D); 
   }
   if( R <= MODBUS_REG_PREDEFINED_GROWTH_CURVES_DELETE){
       return ModbusRegWritePredefinedGrowthCurves( R, D); 
   }
   if( R <= MODBUS_REG_PREDEFINED_CORRECTION_CURVES_DELETE){
       return ModbusRegWritePredefinedCorrectionCurves( R, D); 
   }
   if( R <= MODBUS_REG_PREDEFINED_WEIGHING_PLANS_DELETE){
       return ModbusRegWritePredefinedWeighingPlans( R, D); 
   }
   if( R <= MODBUS_REG_STATISTICS_DAYNUMBER){
       return ModbusRegWriteStatistics( R, D); 
   }
   if( R <= MODBUS_REG_PREDEFINED_WEIGHINGS_DISABLE_DEFAULT){
       return ModbusRegWritePredefinedWeighings( R, D); 
   }
   if( R <= MODBUS_REG_WEIGHING_STOP){
       return ModbusRegWriteWeighing( R, D); 
   }
   if( R <= MODBUS_REG_CHARACTER_BUFFER_CHAR_126_127){
       return ModbusRegWriteCharacterBuffer( R, D); 
   }
   return NO;
}




