//******************************************************************************
//
//   ModbusWeighingGroup.c      Modbus Weighing register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusWeighingGroup.h"
#include "Predefined/PredefinedWeighingDef.h"
#include "Predefined/PredefinedList.h"
#include "Data/uNamedList.h"
#include "Weighing/WeighingConfiguration.h"
#include "System/System.h"
#include "Scheduler/WeighingScheduler.h"
#include "Time/uDateTime.h"
#include "ModbusRegRangeCheck.h"


static TPredefinedWeighingIndex   _WeighingPredefId = 0; // Predefined weighing numeric identifier - 0 means no predefined weighing
static UDateTime StartAt;


// Locals :


//------------------------------------------------------------------------------
//  Read Weighing register
//------------------------------------------------------------------------------
word ModbusRegReadWeighing( EModbusRegNum R)
// Read Weighing register group
{
   switch ( R){
      case MODBUS_REG_WEIGHING_WEIGHING_STATE :
      {
         return WeighingStatus();
      }
      case MODBUS_REG_WEIGHING_START_AT_DAY :
      {
         return StartAt.Date.Day;
      }
      case MODBUS_REG_WEIGHING_START_AT_MONTH :
      {
         return StartAt.Date.Month;
      }
      case MODBUS_REG_WEIGHING_START_AT_YEAR :
      {
         return StartAt.Date.Year;
      }
      case MODBUS_REG_WEIGHING_START_AT_HOUR :
      {
         return StartAt.Time.Hour;
      }
      case MODBUS_REG_WEIGHING_START_AT_MIN :
      {
         return StartAt.Time.Min;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write Weighing register
//------------------------------------------------------------------------------
TYesNo ModbusRegWriteWeighing( EModbusRegNum R, word D)
// Write Weighing register group
{
TPredefinedList PredefList;
UDateTimeGauge Local;

   // Check value correctness
   if( !ModbusRegRangeCheckWeighing( R, D)){
      return NO;
   }
   // Command START
   if( R == MODBUS_REG_WEIGHING_START){
      if( !(_WeighingPredefId > 0)){
         return NO;
      }
      while( PredefinedListOpen( &PredefList));
      PredefinedListIdentifierLoad( &PredefList, &WeighingConfiguration, _WeighingPredefId);
      PredefinedListClose( &PredefList);
      WeighingSchedulerStart();

      return YES;
   }
   // Command START_AT
   if( R == MODBUS_REG_WEIGHING_START_AT){
      if( !(_WeighingPredefId > 0)){
         return NO;
      }
      while( PredefinedListOpen( &PredefList));
      PredefinedListIdentifierLoad( &PredefList, &WeighingConfiguration, _WeighingPredefId);
      PredefinedListClose( &PredefList);
      Local = uDateTimeGauge( &StartAt);
      WeighingSchedulerStartAt( uDateTimeClock( Local));

      return YES;
   }
   // Command START_DIAGNOSTICS
   if( R == MODBUS_REG_WEIGHING_START_DIAGNOSTICS){
      WeighingSchedulerDiagnostics();

      return YES;
   }
   // Command START_CALIBRATION
   if( R == MODBUS_REG_WEIGHING_START_CALIBRATION){
      WeighingSchedulerCalibration();

      return YES;
   }
   // Command PAUSE
   if( R == MODBUS_REG_WEIGHING_PAUSE){
      WeighingSchedulerSuspend();

      return YES;
   }
   // Command UNPAUSE
   if( R == MODBUS_REG_WEIGHING_UNPAUSE){
      WeighingSchedulerRelease();

      return YES;
   }
   // Command STOP
   if( R == MODBUS_REG_WEIGHING_STOP){
      WeighingSchedulerStop();

      return YES;
   }
   // Global variable _WeighingPredefId
   if( R == MODBUS_REG_WEIGHING_PREDEF_ID){
      if( !(D < uDirectoryCount( uNamedListDirectory( &PredefList)))){
         return NO;
      }
      _WeighingPredefId = D;
      return YES;
   }
   // R/W registers 
   // register START_AT_DAY
   if( R == MODBUS_REG_WEIGHING_START_AT_DAY){    
            
      StartAt.Date.Day = D;      
      
      return YES;
   }
   // register START_AT_MONTH
   if( R == MODBUS_REG_WEIGHING_START_AT_MONTH){    
            
      StartAt.Date.Month = D;      
      
      return YES;
   }
   // register START_AT_YEAR
   if( R == MODBUS_REG_WEIGHING_START_AT_YEAR){    
            
      StartAt.Date.Year = D;      
      
      return YES;
   }
   // register START_AT_HOUR
   if( R == MODBUS_REG_WEIGHING_START_AT_HOUR){    
            
      StartAt.Time.Hour = D;      
      
      return YES;
   }
   // register START_AT_MIN
   if( R == MODBUS_REG_WEIGHING_START_AT_MIN){    
            
      StartAt.Time.Min = D;      
      
      return YES;
   }
   return NO;
}




