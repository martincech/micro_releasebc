//******************************************************************************
//
//   ModbusGsmContactsGroup.c      Modbus Gsm Contacts register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusGsmContactsGroup.h"
#include "Message/GsmMessageDef.h"
#include "Message/ContactList.h"
#include "ModbusRegRangeCheck.h"
#include <string.h>


static TContactIndex   _GsmContactsIndex = 0; // This contact index(0 - no contact)
static TGsmContact Contact;


// Locals :


//------------------------------------------------------------------------------
//  Read Gsm Contacts register
//------------------------------------------------------------------------------
word ModbusRegReadGsmContacts( EModbusRegNum R)
// Read Gsm Contacts register group
{
TContactList ContactList;

   switch ( R){
      case MODBUS_REG_GSM_CONTACTS_SMS_FORMAT :
      {
         return Contact.SmsFormat;
      }
      case MODBUS_REG_GSM_CONTACTS_ALLOW_STATISTICS :
      {
         return Contact.Statistics;
      }
      case MODBUS_REG_GSM_CONTACTS_ALLOW_COMMANDS :
      {
         return Contact.Commands;
      }
      case MODBUS_REG_GSM_CONTACTS_ALLOW_EVENTS :
      {
         return Contact.Events;
      }
      case MODBUS_REG_GSM_CONTACTS_SEND_HISTOGRAM :
      {
         return Contact.SendHistogram;
      }
      case MODBUS_REG_GSM_CONTACTS_NAME :
      {
         strncpy( (void *)WriteValuesBuffer.CharacterBuffer, Contact.Name, GSM_CONTACT_NAME_SIZE);
         return 1;
      }
      case MODBUS_REG_GSM_CONTACTS_PHONE_NUMBER :
      {
         strncpy( (void *)WriteValuesBuffer.CharacterBuffer, Contact.PhoneNumber, GSM_PHONE_NUMBER_SIZE);
         return 1;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write Gsm Contacts register
//------------------------------------------------------------------------------
TYesNo ModbusRegWriteGsmContacts( EModbusRegNum R, word D)
// Write Gsm Contacts register group
{
TContactList ContactList;

   // Check value correctness
   if( !ModbusRegRangeCheckGsmContacts( R, D)){
      return NO;
   }
   // String NAME
   if( R == MODBUS_REG_GSM_CONTACTS_NAME){
      strncpy( Contact.Name, (void *)WriteValuesBuffer.CharacterBuffer, GSM_CONTACT_NAME_SIZE);
      return YES;
   }
   // String PHONE_NUMBER
   if( R == MODBUS_REG_GSM_CONTACTS_PHONE_NUMBER){
      strncpy( Contact.PhoneNumber, (void *)WriteValuesBuffer.CharacterBuffer, GSM_PHONE_NUMBER_SIZE);
      return YES;
   }
   // Command CREATE
   if( R == MODBUS_REG_GSM_CONTACTS_CREATE){
      if( !(ContactListCount(&ContactList) < ContactListCapacity())){
         return NO;
      }
      while( ContactListOpen( &ContactList));
      _GsmContactsIndex = ContactListAdd( &ContactList, &Contact);
      ContactListClose( &ContactList);

      return YES;
   }
   // Command SAVE
   if( R == MODBUS_REG_GSM_CONTACTS_SAVE){
      if( !(_GsmContactsIndex > 0)){
         return NO;
      }
      while( ContactListOpen( &ContactList));
      ContactListSave(  &ContactList, &Contact, _GsmContactsIndex);
      ContactListClose( &ContactList);

      return YES;
   }
   // Command DELETE
   if( R == MODBUS_REG_GSM_CONTACTS_DELETE){
      if( !(_GsmContactsIndex > 0)){
         return NO;
      }
      while( ContactListOpen( &ContactList));
      ContactListDelete(  &ContactList, _GsmContactsIndex);
      ContactListClose(&ContactList);

      return YES;
   }
   // Global variable _GsmContactsIndex
   if( R == MODBUS_REG_GSM_CONTACTS_INDEX){
      if( !(D < ContactListCount( &ContactList))){
         return NO;
      }
      _GsmContactsIndex = D;
      while( ContactListOpen( &ContactList));
      ContactListLoad( &ContactList, &Contact, _GsmContactsIndex);
      ContactListClose( &ContactList);
      return YES;
   }
   // R/W registers 
   // register SMS_FORMAT
   if( R == MODBUS_REG_GSM_CONTACTS_SMS_FORMAT){    
            
      Contact.SmsFormat = D;      
      
      return YES;
   }
   // register ALLOW_STATISTICS
   if( R == MODBUS_REG_GSM_CONTACTS_ALLOW_STATISTICS){    
            
      Contact.Statistics = D;      
      
      return YES;
   }
   // register ALLOW_COMMANDS
   if( R == MODBUS_REG_GSM_CONTACTS_ALLOW_COMMANDS){    
            
      Contact.Commands = D;      
      
      return YES;
   }
   // register ALLOW_EVENTS
   if( R == MODBUS_REG_GSM_CONTACTS_ALLOW_EVENTS){    
            
      Contact.Events = D;      
      
      return YES;
   }
   // register SEND_HISTOGRAM
   if( R == MODBUS_REG_GSM_CONTACTS_SEND_HISTOGRAM){    
            
      Contact.SendHistogram = D;      
      
      return YES;
   }
   return NO;
}




