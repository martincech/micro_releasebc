//******************************************************************************
//
//   ModbusCharacterBufferGroup.c      Modbus Character buffer register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusCharacterBufferGroup.h"
#include "ModbusRegRangeCheck.h"




// Locals :


//------------------------------------------------------------------------------
//  Read Character buffer register
//------------------------------------------------------------------------------
word ModbusRegReadCharacterBuffer( EModbusRegNum R)
// Read Character buffer register group
{
int i;
   if( R >= MODBUS_REG_CHARACTER_BUFFER_CHAR_0_1 && R <= MODBUS_REG_CHARACTER_BUFFER_CHAR_126_127){
      i = R - MODBUS_REG_CHARACTER_BUFFER_CHAR_0_1;
      return WriteValuesBuffer.CharacterBuffer[ i];
   }
   return 0;
}

//------------------------------------------------------------------------------
//  Write Character buffer register
//------------------------------------------------------------------------------
TYesNo ModbusRegWriteCharacterBuffer( EModbusRegNum R, word D)
// Write Character buffer register group
{
int i;
   // Check value correctness
   if( !ModbusRegRangeCheckCharacterBuffer( R, D)){
      return NO;
   }
   if( R >=  MODBUS_REG_CHARACTER_BUFFER_CHAR_0_1 && R <= MODBUS_REG_CHARACTER_BUFFER_CHAR_126_127){
     i = R - MODBUS_REG_CHARACTER_BUFFER_CHAR_0_1; 
     WriteValuesBuffer.CharacterBuffer[ i] = D;
     return YES;
   }
   return NO;
}




