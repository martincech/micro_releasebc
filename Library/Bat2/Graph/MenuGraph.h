//******************************************************************************
//
//   MenuGraph.h  Bat2 graph viewer page
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGraph_H__
#define __MenuGraph_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuGraph( void);
// Display samples memory

#endif // __MenuGraph_H__
