//******************************************************************************
//
//   SmsGateDef.h    Bat2 sms gate definitions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __SmsGateDef_H__
   #ifndef _MANAGED
      #define __SmsGateDef_H__
   #endif


#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Uart_H__
   #include "Uart/Uart.h"
#endif

//-----------------------------------------------------------------------------
// Configuration parameters for RS485 mode
//-----------------------------------------------------------------------------

#ifdef _MANAGED
namespace Bat2Library{
   public enum class SmsGateParityE{
#else
typedef enum {
#endif
   SMS_GATE_PARITY_NONE,
   SMS_GATE_PARITY_EVEN,
   SMS_GATE_PARITY_ODD,
#ifndef _MANAGED
   _SMS_GATE_PARITY_LAST
} ESmsGateParity;
#else
   };
}
#endif

#ifdef _MANAGED
namespace Bat2Library{
   public enum class SmsGateDataBitsE{
#else
typedef enum {
#endif
   SMS_GATE_DATA_BITS_7,
   SMS_GATE_DATA_BITS_8,
#ifndef _MANAGED
   _SMS_GATE_DATA_BITS_LAST
} ESmsGateDataBits;
#else
   };
}
#endif

#ifndef _MANAGED
#define SMS_GATE_BAUD_RATE_MIN           1200
#define SMS_GATE_BAUD_RATE_MAX           115200
#define SMS_GATE_BAUD_RATE_DEFAULT       19200

#define SMS_GATE_PARITY_DEFAULT           SMS_GATE_PARITY_NONE

#define SMS_GATE_DATA_BITS_DEFAULT        SMS_GATE_DATA_BITS_8

//-----------------------------------------------------------------------------
// SMS send slot number wrong response
//-----------------------------------------------------------------------------

#define SMS_GATE_PHONEBOOK_ERROR    0xFD     // phonebook error unable send broadcast
#define SMS_GATE_PHONENUMBER_ERROR  0xFE     // unknown target/invalid phone number
#define SMS_GATE_SLOT_ERROR         0xFF     // unable get SMS slot

typedef byte      TSmsGateSlotNumber;
typedef byte      TSmsGateSmsStatus;

// SMS status codes :
typedef enum {
   SMS_GATE_SMS_STATUS_EMPTY,       // SMS slot empty
   SMS_GATE_SMS_STATUS_PENDING,     // SMS pending for send
   SMS_GATE_SMS_STATUS_SENDED,      // SMS already sended
   SMS_GATE_SMS_STATUS_ERROR = 0xFF, // unable deliver SMS
} ESmsGateSmsStatus;


#include "Gsm/GsmDef.h"
#include "Message/GsmMessageDef.h"

#define SMS_GATE_SMS_SLOTS         10

typedef struct {
   char SmsText[GSM_SMS_SIZE + 1];
   char SmsPhoneNumber[GSM_PHONE_NUMBER_SIZE + 1];
   byte _Dummy1;

   byte SlotStatus;
   TContactIndex Contact;
   byte Broadcast;
   byte _Dummy2;

   dword SlotReadStartTime;
   word BinarySize;
   word _Dummy3;
} TSmsSlot;

typedef struct {
   TSmsSlot SmsSlots[SMS_GATE_SMS_SLOTS];
   dword    ActiveSlot;
} TSmsQueue;

#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "SmsGateDef.h"
   #define _MANAGED
   namespace Bat2Library{
      public ref class SmsGateConfigurationC abstract sealed{
      public:
         literal int BAUD_RATE_MIN = SMS_GATE_BAUD_RATE_MIN;
         literal int BAUD_RATE_MAX = SMS_GATE_BAUD_RATE_MAX;
         literal int BAUD_RATE_DEFAULT = SMS_GATE_BAUD_RATE_DEFAULT;
      };
   }
#endif
#endif // __SmsGateDef_H__

