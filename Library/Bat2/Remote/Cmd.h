//*****************************************************************************
//
//    Cmd.h        Remote commands
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Cmd_H__
   #define __Cmd_H__

#ifndef __RcDef_H__
   #include "RcDef.h"
#endif

#include "Remote/SocketDef.h"

#define CMD_FILE_DATA_MAX    RC_FILE_DATA_MAX

#ifdef __cplusplus
   extern "C" {
#endif

//******************************************************************************
// Open
//******************************************************************************

TYesNo CmdFileOpen( TSocket *Socket, byte Name, byte Mode);
// File open

TYesNo ReplyFileOpen( TSocket *Socket);
// File open

TYesNo CmdAndReplyFileOpen( TSocket *Socket, byte Name, byte Mode);
// File open

//******************************************************************************
// Close
//******************************************************************************

TYesNo CmdFileClose( TSocket *Socket, TFile *File);
// File close

TYesNo ReplyFileClose( TSocket *Socket);
// File close

TYesNo CmdAndReplyFileClose( TSocket *Socket, TFile *File);
// File close

//******************************************************************************
// Save
//******************************************************************************

TYesNo CmdFileSave( TSocket *Socket, TFile *File, TFileAddress Address, void *Buffer, int Size);
// File save

TYesNo ReplyFileSave( TSocket *Socket);
// File save

TYesNo CmdAndReplyFileSave( TSocket *Socket, TFile *File, TFileAddress Address, void *Buffer, int Size);
// File save

//******************************************************************************
// Load
//******************************************************************************

TYesNo CmdFileLoad( TSocket *Socket, TFile *File, TFileAddress Address, int Size);
// File load

TYesNo ReplyFileLoad( TSocket *Socket, void *Buffer, int Size);
// File load

TYesNo CmdAndReplyFileLoad( TSocket *Socket, TFile *File, TFileAddress Address, void *Buffer, int Size);
// File load

//******************************************************************************
// Match
//******************************************************************************

//******************************************************************************
// Fill
//******************************************************************************

TYesNo CmdFileFill(TSocket *Socket, TFile *File, TFileAddress Address, byte Pattern, int Size);
// File Fill

TYesNo ReplyFileFill(TSocket *Socket);
// File Fill

TYesNo CmdAndReplyFileFill(TSocket *Socket, TFile *File, TFileAddress Address, byte Pattern, int Size);
// File Fill

//******************************************************************************
// Sum
//******************************************************************************

TYesNo CmdFileSum(TSocket *Socket, TFile *File, TFileAddress Address, int Size);
// File sum

TYesNo ReplyFileSum(TSocket *Socket, TNvmSum *Sum);
// File sum

TYesNo CmdAndReplyFileSum(TSocket *Socket, TFile *File, TFileAddress Address, int Size, TNvmSum *Sum);
// File sum

//******************************************************************************
// Move
//******************************************************************************

TYesNo CmdFileMove(TSocket *Socket, TFile *File, TFileAddress ToAddress, TFileAddress FromAddress, int Size);
// File Move

TYesNo ReplyFileMove(TSocket *Socket);
// File Move

TYesNo CmdAndReplyFileMove(TSocket *Socket, TFile *File, TFileAddress ToAddress, TFileAddress FromAddress, int Size);
// File Move

//******************************************************************************
// Commit
//******************************************************************************

TYesNo CmdFileCommit( TSocket *Socket, TFile *File);
// File commit

TYesNo ReplyFileCommit( TSocket *Socket);
// File commit

TYesNo CmdAndReplyFileCommit( TSocket *Socket, TFile *File);
// File commit

//******************************************************************************
// Action
//******************************************************************************

TYesNo CmdAction( TSocket *Socket,  TActionCmd *Action, int ActionSize);
// Action execute

TYesNo ReplyAction( TSocket *Socket, TActionReply *Reply, int *ReplySize);
// Action execute

TYesNo CmdAndReplyAction( TSocket *Socket, TActionCmd *Action, int ActionSize, TActionReply *Reply, int *replyIoSize);
// Action execute

#ifdef __cplusplus
   }
#endif

#endif
