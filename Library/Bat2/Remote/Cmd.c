//*****************************************************************************
//
//    Cmd.c        Remote commands
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "Cmd.h"
#include "Remote/Socket.h"
#include <string.h>

static TYesNo Send( TSocket *Socket, void *Buffer, int Size)
{
   if(!SocketSend( Socket, Buffer, Size)) {
      return NO;
   }
   while(SocketState( Socket) == SOCKET_STATE_SEND_ACTIVE);

   if(SocketState( Socket) != SOCKET_STATE_SEND_DONE) {
      return NO;
   }

   return YES;
}

static TYesNo Receive( TSocket *Socket, void *Reply, int *Size, int DesiredReplySize)
{
   if(!SocketReceive( Socket, Reply, sizeof(TRcReply))) {
      return NO;
   }
   while(SocketState( Socket) == SOCKET_STATE_RECEIVE_ACTIVE);

   if(SocketState( Socket) != SOCKET_STATE_RECEIVE_DONE) {
      return NO;
   }

   *Size = SocketReceiveSize( Socket);
   if(DesiredReplySize) {
      if( *Size != DesiredReplySize) {
         return NO;
      }
   }
   return YES;
}

//******************************************************************************
// Open
//******************************************************************************

TYesNo CmdFileOpen( TSocket *Socket, byte Name, byte Mode)
// File open
{
TRcCmd Cmd;
   Cmd.Cmd = RC_CMD_FILE_OPEN;
   Cmd.Data.FileOpen.FileName = Name;
   Cmd.Data.FileOpen.Mode = Mode;
   if(!Send( Socket, &Cmd, RcCmdSize( FileOpen))) {
      return NO;
   }
   return YES;
}

TYesNo ReplyFileOpen( TSocket *Socket)
// File open
{
TRcReply Reply;
int Size;
byte Des;
   if(!(SocketPermission(Socket) & FILE_MODE_READ_ONLY)) {
      return YES;
   }
   if(!Receive(Socket, &Reply, &Size, RcReplySimpleSize())) {
      return NO;
   }
   Des = RcReply(RC_CMD_FILE_OPEN, YES);
   if(Reply.Reply != Des) {
      return NO;
   }
   return YES;
}

TYesNo CmdAndReplyFileOpen( TSocket *Socket, byte Name, byte Mode)
// File open
{
   if(!CmdFileOpen( Socket, Name, Mode)) {
      return NO;
   }
   if(!ReplyFileOpen( Socket)) {
      return NO;
   }
   return YES;
}

//******************************************************************************
// Close
//******************************************************************************

TYesNo CmdFileClose( TSocket *Socket, TFile *File)
// File close
{
TRcCmd Cmd;
   Cmd.Cmd = RC_CMD_FILE_CLOSE;
   Cmd.Data.FileName = File->Item.Name;
   if(!Send( Socket, &Cmd, RcCmdSize( FileClose))) {
      return NO;
   }
   return YES;
}

TYesNo ReplyFileClose( TSocket *Socket)
// File close
{
TRcReply Reply;
int Size;
   if(!(SocketPermission(Socket) & FILE_MODE_READ_ONLY)) {
      return YES;
   }
   if(!Receive(Socket, &Reply, &Size, RcReplySimpleSize())) {
      return NO;
   }
   if(Reply.Reply != RcReply(RC_CMD_FILE_CLOSE, YES)) {
      return NO;
   }
   return YES;
}

TYesNo CmdAndReplyFileClose( TSocket *Socket, TFile *File)
// File close
{
   if(!CmdFileClose( Socket, File)) {
      return NO;
   }
   if(!ReplyFileClose( Socket)) {
      return NO;
   }
   return YES;
}

//******************************************************************************
// Save
//******************************************************************************

TYesNo CmdFileSave( TSocket *Socket, TFile *File, TFileAddress Address, void *Buffer, int Size)
// File save
{
TRcCmd Cmd;
   if(!(SocketPermission(Socket) & FILE_MODE_WRITE_ONLY)) {
      return NO;
   }

   if( Size > CMD_FILE_DATA_MAX) {
      return NO;
   }

   Cmd.Cmd = RC_CMD_FILE_SAVE;
   Cmd.Data.FileName = File->Item.Name;
   Cmd.Data.FileSave.Address = Address;
   Cmd.Data.FileSave.Size = Size;
   memcpy(Cmd.Data.FileSave.Data, Buffer, Size);

   if(!Send(Socket, &Cmd, RcCmdFileSaveSize( Size))) {
      return NO;
   }
   return YES;
}

TYesNo ReplyFileSave( TSocket *Socket)
// File save
{
TRcReply Reply;
int Size;
   if(!(SocketPermission(Socket) & FILE_MODE_READ_ONLY)) {
      return YES;
   }
   if(!Receive(Socket, &Reply, &Size, RcReplySimpleSize())) {
      return NO;
   }
   if(Reply.Reply != RcReply(RC_CMD_FILE_SAVE, YES)) {
      return NO;
   }
   return YES;
}

TYesNo CmdAndReplyFileSave( TSocket *Socket, TFile *File, TFileAddress Address, void *Buffer, int Size)
// File save
{
   if(!CmdFileSave( Socket, File, Address, Buffer, Size)) {
      return NO;
   }
   if(!ReplyFileSave( Socket)) {
      return NO;
   }
   return YES;
}

//******************************************************************************
// Load
//******************************************************************************

TYesNo CmdFileLoad( TSocket *Socket, TFile *File, TFileAddress Address, int Size)
// File load
{
TRcCmd Cmd;
   if(!(SocketPermission(Socket) & FILE_MODE_WRITE_ONLY)) {
      return NO;
   }

   if( Size > CMD_FILE_DATA_MAX) {
      return NO;
   }

   Cmd.Cmd = RC_CMD_FILE_LOAD;
   Cmd.Data.FileName = File->Item.Name;
   Cmd.Data.FileSave.Address = Address;
   Cmd.Data.FileSave.Size = Size;

   if(!Send(Socket, &Cmd, RcCmdSize( FileLoad))) {
      return NO;
   }

   return YES;
}

TYesNo ReplyFileLoad( TSocket *Socket, void *Buffer, int Size)
// File load
{
TRcReply Reply;
int ReplySize;
   if(!(SocketPermission(Socket) & FILE_MODE_READ_ONLY)) {
      return YES;
   }
   if(!Receive(Socket, &Reply, &ReplySize, RcReplyFileLoadSize( Size))) {
      return NO;
   }
   if(Reply.Reply != RcReply(RC_CMD_FILE_LOAD, YES)) {
      return NO;
   }
   memcpy(Buffer, Reply.Data.FileLoad.Data, Size);
   return YES;
}

TYesNo CmdAndReplyFileLoad( TSocket *Socket, TFile *File, TFileAddress Address, void *Buffer, int Size)
// File load
{
   if(!CmdFileLoad( Socket, File, Address, Size)) {
      return NO;
   }
   if(!ReplyFileLoad( Socket, Buffer, Size)) {
      return NO;
   }
   return YES;
}


//******************************************************************************
// Fill
//******************************************************************************

TYesNo CmdFileFill(TSocket *Socket, TFile *File, TFileAddress Address, byte Pattern, int Size)
// File Fill
{
TRcCmd Cmd;

   Cmd.Cmd = RC_CMD_FILE_FILL;
   Cmd.Data.FileFill.FileName = File->Item.Name;
   Cmd.Data.FileFill.Address = Address;
   Cmd.Data.FileFill.Pattern = Pattern;
   Cmd.Data.FileFill.Size = Size;
   if (!Send(Socket, &Cmd, RcCmdSize(FileFill))){
      return NO;
   }
   return YES;
}

TYesNo ReplyFileFill(TSocket *Socket)
// File Fill
{
   TRcReply Reply;
   int Size;

   if (!Receive(Socket, &Reply, &Size, RcReplySimpleSize())) {
      return NO;
   }
   if (Reply.Reply != RcReply(RC_CMD_FILE_FILL, YES)) {
      return NO;
   }
   return YES;
}

TYesNo CmdAndReplyFileFill(TSocket *Socket, TFile *File, TFileAddress Address, byte Pattern, int Size)
// File Fill
{
   if (!CmdFileFill(Socket, File, Address, Pattern, Size)) {
      return NO;
   }
   if (!ReplyFileFill(Socket)) {
      return NO;
   }
   return YES;
}


//******************************************************************************
// Sum
//******************************************************************************

TYesNo CmdFileSum(TSocket *Socket, TFile *File, TFileAddress Address, int Size)
// File sum
{
TRcCmd Cmd;

   Cmd.Cmd = RC_CMD_FILE_SUM;
   Cmd.Data.FileSum.FileName = File->Item.Name;
   Cmd.Data.FileSum.Address = Address;
   Cmd.Data.FileSum.Size = Size;
   if (!Send(Socket, &Cmd, RcCmdSize(FileSum))){
      return NO;
   }
   return YES;
}

TYesNo ReplyFileSum(TSocket *Socket, TNvmSum *Sum)
// File sum
{
TRcReply Reply;
int Size;

   if (!Receive(Socket, &Reply, &Size, RcReplySize(FileSum))) {
      return NO;
   }
   if (Reply.Reply != RcReply(RC_CMD_FILE_SUM, YES)) {
      return NO;
   }
   *Sum = Reply.Data.FileSum.Sum;
   return YES;
}

TYesNo CmdAndReplyFileSum(TSocket *Socket, TFile *File, TFileAddress Address, int Size, TNvmSum *Sum)
// File sum
{
   if (!CmdFileSum(Socket, File, Address, Size)) {
      return NO;
   }
   if (!ReplyFileSum(Socket, Sum)) {
      return NO;
   }
   return YES;
}

//******************************************************************************
// Move
//******************************************************************************

TYesNo CmdFileMove(TSocket *Socket, TFile *File, TFileAddress ToAddress, TFileAddress FromAddress, int Size)
// File Move
{
   TRcCmd Cmd;

   Cmd.Cmd = RC_CMD_FILE_MOVE;
   Cmd.Data.FileMove.FileName = File->Item.Name;
   Cmd.Data.FileMove.ToAddress = ToAddress;
   Cmd.Data.FileMove.FromAddress = FromAddress;
   Cmd.Data.FileMove.Size = Size;
   if (!Send(Socket, &Cmd, RcCmdSize(FileMove))){
      return NO;
   }
   return YES;
}

TYesNo ReplyFileMove(TSocket *Socket)
// File Move
{
   TRcReply Reply;
   int Size;

   if (!Receive(Socket, &Reply, &Size, RcReplySimpleSize())) {
      return NO;
   }
   if (Reply.Reply != RcReply(RC_CMD_FILE_MOVE, YES)) {
      return NO;
   }
   return YES;
}

TYesNo CmdAndReplyFileMove(TSocket *Socket, TFile *File, TFileAddress ToAddress, TFileAddress FromAddress, int Size)
// File Move
{
   if (!CmdFileMove(Socket, File,ToAddress, FromAddress, Size)) {
      return NO;
   }
   if (!ReplyFileMove(Socket)) {
      return NO;
   }
   return YES;
}

//******************************************************************************
// Commit
//******************************************************************************

TYesNo CmdFileCommit( TSocket *Socket, TFile *File)
// File commit
{
TRcCmd Cmd;
   Cmd.Cmd = RC_CMD_FILE_COMMIT;
   Cmd.Data.FileName = File->Item.Name;
   if(!Send( Socket, &Cmd, RcCmdSize( FileCommit))) {
      return NO;
   }
   return YES;
}

TYesNo ReplyFileCommit( TSocket *Socket)
// File commit
{
TRcReply Reply;
int Size;
   if(!(SocketPermission(Socket) & FILE_MODE_READ_ONLY)) {
      return YES;
   }
   if(!Receive(Socket, &Reply, &Size, RcReplySimpleSize())) {
      return NO;
   }
   if(Reply.Reply != RcReply(RC_CMD_FILE_COMMIT, YES)) {
      return NO;
   }
   return YES;
}

TYesNo CmdAndReplyFileCommit( TSocket *Socket, TFile *File)
// File commit
{
   if(!CmdFileCommit( Socket, File)) {
      return NO;
   }
   if(!ReplyFileCommit( Socket)) {
      return NO;
   }
   return YES;
}

//******************************************************************************
// Action
//******************************************************************************

TYesNo CmdAction( TSocket *Socket,  TActionCmd *Action, int ActionSize)
// Action execute
{
TRcCmd RcCmd;

   RcCmd.Cmd = RC_CMD_ACTION;
   memcpy(&RcCmd.Data.Action.Action, Action, ActionSize);
   if(!Send( Socket, &RcCmd, RcActionToRcSize( ActionSize))) {
      return NO;
   }
   return YES;
}

TYesNo ReplyAction(TSocket *Socket, TActionReply *Reply, int *ReplySize)
// Action execute
{
TRcReply RcReply;

   if (!Receive(Socket, &RcReply, ReplySize, RcActionToRcSize(*ReplySize))) {
      return NO;
   }
   if(*ReplySize == 0) {
      return NO;
   }
   *ReplySize = RcRcToActionSize( *ReplySize);
   memcpy(Reply, &RcReply.Data.Action.Action, *ReplySize);
   return YES;
}

TYesNo CmdAndReplyAction(TSocket *Socket, TActionCmd *Action, int ActionSize, TActionReply *Reply, int *replyIoSize)
// Action execute
{
   if(!CmdAction( Socket, Action, ActionSize)) {
      return NO;
   }
   if (!ReplyAction(Socket, Reply, replyIoSize)) {
      return NO;
   }
   return YES;
}
