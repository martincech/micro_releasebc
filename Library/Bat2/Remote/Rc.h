//*****************************************************************************
//
//    Rc.h         Remote commands
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Rc_H__
   #define __Rc_H__

#ifndef __RcDef_H__
   #include "RcDef.h"
#endif

#ifndef __Remote_H__
   #include "Remote/Remote.h"
#endif

#define RC_SESSION_ID_INVALID      0

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
   byte SessionId;
   byte InterfaceId;
} TRcId;

void RcInit( void);
// Init

byte RcSessionStart( void);
// Start session - check for RC_SESSION_ID_INVALID 

TYesNo RcSessionIsActive( byte SessionId);
// Checks for session validity

void RcSessionTerminate( byte SessionId);
// Terminates session 

void RcSessionTerminateForce();
// Terminate session by user intervention

void RcExecute( void);
// Execute - call periodically

TYesNo RcCommand(TRcId *RcId, TRcCmd *Cmd, word CmdSize, TRcReply *Reply, word *ReplySize);
// Execute command

#ifdef __cplusplus
   }
#endif

#ifndef __WIN32__
TYesNo RcNoSessionCommand(TRcCmd *Cmd, word CmdSize, TRcReply *Reply, word *ReplySize);
// Execute command without the need of session
#endif

#endif
