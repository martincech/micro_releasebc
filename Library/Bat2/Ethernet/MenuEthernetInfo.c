//******************************************************************************
//
//   MenuEthernetInfo.c  Ethernet Info menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuEthernetInfo.h"
#include "System/System.h"             // Timer only
#include "Gadget/DEvent.h"             // Display events
#include "Gadget/DLayout.h"            // Screen layout
#include "Gadget/DLabel.h"             // Labels
#include "Console/conio.h"             // Console
#include "Bitmap.h"                    // arrows
#include "Fonts.h"                     // Project fonts
#include "Str.h"                       // Strings
#include "Ethernet/EthernetLibrary.h"  // ethernet library functions
#include "Sound/Beep.h"
#include <string.h>

#define ETHERNET_INFO_PERIOD    1

#if ETHERNET_INFO_PERIOD < 1
   #define ETHERNET_INFO_COUNTER  1
#else
   #define ETHERNET_INFO_COUNTER  TimerSlowCount( ETHERNET_INFO_PERIOD)
#endif

#define EthernetInfoTimerSet()    _EthernetInfoTimer = ETHERNET_INFO_COUNTER
#define EthernetInfoTimerStart()  _EthernetInfoTimer = 1   // execute immediately
#define EthernetInfoTimerTick()  (--_EthernetInfoTimer == 0)
static byte _EthernetInfoTimer;

#define INFO_X          3
#define INFO_Y          20
#define CHAR_H          17
#define PAGE_COUNT      1
#define PAGE_NUMBER_W   50               // page number width
#define INFO_BUFFER_SIZE 50

static void InfoLine(int y, TUniStr Name, char *info);
// print one info line with name : info
static void PrintPage(int num);
// how page info

//------------------------------------------------------------------------------
//  Menu EthernetInfo
//------------------------------------------------------------------------------

void MenuEthernetInfo( void)
// Menu ethernet Info
{
TYesNo   Redraw;
int      actPage;

   EthernetInfoTimerStart();
   Redraw = YES;
   actPage = 1;
   forever {
      if( Redraw){
         GClear();
         DLayoutStatus( STR_BTN_CANCEL, "", &BmpButtonLeftRight);
         PrintPage(actPage);
         GFlush();
         Redraw = NO;
      }
       switch( DEventWait()){
          case K_LEFT :
             if( actPage == 1){
                BeepKey();
                break;
             }
             actPage--;
             Redraw = YES;
             break;
          case K_RIGHT :
             if( actPage == PAGE_COUNT){
                BeepKey();
                break;
             }
             actPage++;
             Redraw = YES;
             break;
          case K_TIMER_SLOW :
             if( !EthernetInfoTimerTick()){
                break;
             }
             EthernetInfoTimerSet();
             Redraw = YES;
             break;

          case K_ENTER :
          case K_ESC :
             BeepKey();
          case K_TIMEOUT :
             return;
       }      
   }
} // MenuEthernetInfo

//------------------------------------------------------------------------------
//  print info line
//------------------------------------------------------------------------------

static void InfoLine(int y, TUniStr Name, char *info)
// print one info line with name : info
{
   if( Name){
      DLabel( Name, INFO_X, y);
      GTextAt(INFO_X + GTextWidth( StrGet(Name)), y);
      cputs(" : ");
   }
   if(info){
     DLabelRight( info, DMENU_EDIT_X, y);
   }
}

//------------------------------------------------------------------------------
//  print info line
//------------------------------------------------------------------------------

static void PrintPage(int num)
{
char   StringBuffer[INFO_BUFFER_SIZE];
int    y;
int    len;
dword  upTime;

   StringBuffer[0] = '\0';
   // info :
   SetFont( TAHOMA16);
   y = INFO_Y;
   switch(num){
      case 1 :
         // Interface and IPv4 info, frame :
         len = strlen( StrGet(STR_INTERFACE)) + 1 ;
         strcpy( StringBuffer, StrGet(STR_INTERFACE));
         strcat( StringBuffer, " ");
         EthernetLibraryEthName( StringBuffer + len, INFO_BUFFER_SIZE - len);
         DLayoutTitle( StringBuffer);
         // IPv4
         EthernetLibraryIPv4( StringBuffer, INFO_BUFFER_SIZE);
         InfoLine(y, STR_IPV4_ADDRESS, 0);
         y += CHAR_H;
         InfoLine(y, 0, StringBuffer);
         y += CHAR_H;
         //#warning Lepe naimplementovat status internetu
         // connection status
         EthernetLibraryLinkStatus( StringBuffer, INFO_BUFFER_SIZE);
         InfoLine(y, STR_STATUS, StringBuffer);
         y += CHAR_H;
         // connection time
         upTime = EthernetLibraryUpTime();
         bprintf(StringBuffer, "%02d:%02d:%02d", upTime/3600, (upTime%3600)/60, (upTime%3600)%60);
         InfoLine(y, STR_ETHERNET_UP_TIME, StringBuffer);
         y += CHAR_H;
         // rx/tx statistics
         InfoLine(y, STR_PACKETS_COUNT, "");
         y += CHAR_H;
         bprintf( StringBuffer, "%d/%d", EthernetLibraryRxPacketCount(), EthernetLibraryTxPacketCount());
         InfoLine(y, 0, StringBuffer);
         y += CHAR_H;
         break;
      default :
         break;
   }

   // show page number :
   GSetColor( DCOLOR_TITLE_BG);
   GBox( G_WIDTH - PAGE_NUMBER_W, 0, PAGE_NUMBER_W, DLAYOUT_TITLE_H);
   GSetColor( COLOR_LIGHTGRAY);
   bprintf( StringBuffer, "%d/%d", num, PAGE_COUNT);
   DLabelRight( StringBuffer, G_WIDTH, 2);
   GSetColor( DCOLOR_DEFAULT);
}
