//******************************************************************************
//
//   Curve.c        Weighing curve utility
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Curve.h"
#include "Curve/GrowthCurve.h"
#include "Curve/CurveList.h"
#include "Weighing/WeighingConfiguration.h"
#include "Math/uGeometry.h"

// Local functions :

static TWeightGauge _PointInterpolation( byte Sex, TDayNumber Day, byte LowIndex, byte HighIndex);
// Returns interpolated value for <Day> between <LowIndex>..<HighIndex> points

static TGrowthCurve *_CurveGet( byte Sex);
// Get current curve by <Sex>

//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void CurveInit( void)
// Initialize
{
   CurveListInit();
   CurveStart();                       // reload curve
} // CurveInit

//------------------------------------------------------------------------------
//   Resume
//------------------------------------------------------------------------------

void CurveResume( void)
// Reload current curve
{
   if( !WeighingHasFemale()){
      GrowthCurveDefault( &WeighingContext.GrowthCurveFemale);
      return;
   }
} // CurveResume

//------------------------------------------------------------------------------
//   Start
//------------------------------------------------------------------------------

void CurveStart( void)
// Initialization after weighing start
{
   CurveResume();                      // load active curve
} // CurveStart

//------------------------------------------------------------------------------
//   Points count
//------------------------------------------------------------------------------

TDayNumber CurvePointsCount( byte Sex)
// Growth curve points count
{
TGrowthCurve *Curve;

   Curve = _CurveGet( Sex);
   return( Curve->Count);
} // CurvePointsCount

//------------------------------------------------------------------------------
//   Has day
//------------------------------------------------------------------------------

TYesNo CurveHasDay( byte Sex, TDayNumber Day)
// Returns YES if the <Day> is included in the curve range
{
TGrowthCurve *Curve;

   Curve = _CurveGet( Sex);
   // check for curve start :
   if( Day <= Curve->Point[ 0].Day){
      return( YES);
   }
   // check for curve end :
   if( Day <= Curve->Point[ Curve->Count - 1].Day){
      return( YES);
   }
   return( NO);                        // past end of curve
} // CurveHasDay

//------------------------------------------------------------------------------
//   Weight
//------------------------------------------------------------------------------

TWeightGauge CurveWeight( byte Sex, TDayNumber Day)
// Returns weight for <Day>
{
TGrowthCurve *Curve;
byte          PointIndex;

   Curve = _CurveGet( Sex);
   if( Curve->Count == 0){
      return( 0);                      // empty curve
   }
   // check for short curve :
   if( Curve->Count < 2){
      return( Curve->Point[ 0].Weight);
   }
   // check for curve start :
   if( Day <= Curve->Point[ 0].Day){
      return( Curve->Point[ 0].Weight);
   } // else Day > Point[ 0].Day
   // check for interval :
   for( PointIndex = 1; PointIndex < Curve->Count; PointIndex++){
      if( Curve->Point[ PointIndex].Day >= Day){
         return( _PointInterpolation( Sex, Day, PointIndex - 1, PointIndex));
      }
   }
   // end of curve :
   return( Curve->Point[ Curve->Count - 1].Weight); // last point
} // CurveWeight

//******************************************************************************

//------------------------------------------------------------------------------
//   Interpolation
//------------------------------------------------------------------------------

static TGrowthCurve *_CurveGet( byte Sex)
// Get current curve by <Sex>
{
   if( Sex != SEX_FEMALE){
      return( &WeighingContext.GrowthCurveMale);
   }
   return( &WeighingContext.GrowthCurveFemale);
} // _CurveGet

//------------------------------------------------------------------------------
//   Interpolation
//------------------------------------------------------------------------------

static TWeightGauge _PointInterpolation( byte Sex, TDayNumber Day, byte LowIndex, byte HighIndex)
// Returns interpolated value for <Day> between <LowIndex>..<HighIndex> points
{
TGrowthCurve *GrowthCurve;
TWeightGauge  LowWeight, HighWeight;
TDayNumber    LowDay, HighDay;

   GrowthCurve = _CurveGet( Sex);
   LowWeight   = GrowthCurve->Point[ LowIndex].Weight;
   HighWeight  = GrowthCurve->Point[ HighIndex].Weight;
   LowDay      = GrowthCurve->Point[ LowIndex].Day;
   HighDay     = GrowthCurve->Point[ HighIndex].Day;
   return( (TWeightGauge)uInterpolation( LowDay, HighDay, LowWeight, HighWeight, Day));
} // _PointInterpolation
