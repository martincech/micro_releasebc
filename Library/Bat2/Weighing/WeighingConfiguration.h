//******************************************************************************
//
//   WeighingConfiguration.h  Weighing Configuration
//   Version 1.0              (c) VEIT Electronics
//
//******************************************************************************

#ifndef __WeighingConfiguration_H__
   #define __WeighingConfiguration_H__

#ifndef __WeighingConfigurationDef_H__
   #include "Weighing/WeighingConfigurationDef.h"
#endif

#ifndef __PredefinedWeighingDef_H__
   #include "Predefined/PredefinedWeighingDef.h"
#endif

#include "Predefined/PredefinedList.h"


extern       TWeighingConfiguration WeighingConfiguration;
extern const TWeighingConfiguration WeighingConfigurationDefault;

extern       TWeighingContext       WeighingContext;
extern const TWeighingContext       WeighingContextDefault;

TPredefinedWeighingIdentifier DefaultWeighingGet( void);
// Get default predefined weighing identifier

void DefaultWeighingSet( TPredefinedList *PredefinedList, TPredefinedWeighingIdentifier Identifier);
// Set default predefined weighing <Identifier>
//------------------------------------------------------------------------------
//  Macros
//------------------------------------------------------------------------------
#include "Log/Logger.h"

#define WeighingSexDifferentation()  (WeighingConfiguration.TargetWeights.SexDifferentiation)
#define WeighingSex()                (WeighingConfiguration.TargetWeights.Sex)    // single mode only

#define WeighingStatus()             (WeighingContext.Status)
#define WeighingLastStatus()         (WeighingContext.LastStatus)
#define WeighingDay()                (WeighingContext.Day)
#define WeighingDayActive()          (WeighingContext.DayActive)
#define WeighingTarget()             (WeighingContext.TargetWeight)
#define WeighingTargetFemale()       (WeighingContext.TargetWeightFemale)
#define WeighingCorrection()         (WeighingContext.Correction)
#define WeighingCorrectionFemale()   (WeighingContext.CorrectionFemale)
#define WeighingStartTime()          (WeighingContext.StartAt)
#define WeighingDayCloseAt()         (WeighingContext.DayCloseAt)
#define WeighingDayDuration()        (WeighingContext.DayDuration)
#define WeighingDayZero()            (WeighingContext.DayZero)
// setter :
#define WeighingStatusSet( NewStatus)    {\
   if( WeighingContext.Status != NewStatus){LogWeighingStatusChanged(WeighingContext.Status, NewStatus);}\
   WeighingContext.Status      = NewStatus;                          \
   }
#define WeighingStatusSave()             WeighingContext.LastStatus  = WeighingContext.Status
#define WeighingStatusRestore()          WeighingContext.Status      = WeighingContext.LastStatus
#define WeighingDaySet( NewDay)          WeighingContext.Day         = NewDay
#define WeighingDayActiveSet( YesNo)     WeighingContext.DayActive   = YesNo
#define WeighingStartTimeSet( NewTime)   WeighingContext.StartAt     = NewTime
#define WeighingDayCloseAtSet( NewTime)  WeighingContext.DayCloseAt  = NewTime
#define WeighingDayDurationSet( NewTime) WeighingContext.DayDuration = NewTime
#define WeighingDayZeroSet( NewTime)     WeighingContext.DayZero     = NewTime

#define WeighingFlock()              (WeighingConfiguration.Flock)
#define WeighingPredictionMode()     (WeighingConfiguration.TargetWeights.Mode)
#define WeighingPredictionGrowth()   (WeighingConfiguration.TargetWeights.Growth)
#define WeighingDayStart()           (WeighingConfiguration.DayStart)
#define WeighingInitialDay()         (WeighingConfiguration.InitialDay)

#define WeighingHasFemale()          (WeighingSexDifferentation() == SEX_DIFFERENTIATION_YES)
// Returns YES on female weighing active

byte WeighingSexCurrent( void);
// Returns current sex for male position

byte WeighingHourNormalize( UTimeGauge Time);
// Normalize weighing hour

byte WeighingHour( void);
// Get hour number

#endif
