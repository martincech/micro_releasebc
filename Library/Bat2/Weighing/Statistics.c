//******************************************************************************
//
//   Statistics.c   Weighing statistics
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Weighing/Statistics.h"
#include "Weighing/WeighingConfiguration.h"
#include "Storage/Sample.h"
#include "Archive/Archive.h"
#include "Statistic/Calculate.h"

#include "Platform/Zone.h"
#include <string.h>

static TArchiveIndex _LastIndex;
static TArchiveIndex _LastFemaleIndex;
static TArchiveIndex _LastDayIndex;
static byte _NextShortStatisticMale;
static byte _NextShortStatisticFemale;

// Local functions :

static void _DayNew( void);
// Start a new day

TArchiveIndex _SearchLast( byte Sex);
// Find last possible record by <Sex>

static TArchiveIndex Statistic( byte HourFrom, byte HourTo, byte Sex);
// Calculates current days statistic <HourFrom> till <HourTo> for <Sex>, returns archive index

static void ShortStatisticsCalculate( byte HourTo, byte Sex);
// Calculates current days statistics till <HourTo> for <Sex>

static void ShortStatisticsExecute( byte Hour);
// Execute short statistics

static byte _ShortStatisticSearchLast( byte Sex, byte *Hour);
// Search last short statistic for current day and <Sex>, returns success and <Hour> of last statistic

static byte ShortStatisticsResume( byte Sex);
// Resumes short statistics for <Sex>, returns next calculation hour

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void StatisticsInit( void)
// Initialize
{
   _LastIndex       = ARCHIVE_INDEX_INVALID;
   _LastFemaleIndex = ARCHIVE_INDEX_INVALID;
   _LastDayIndex    = ARCHIVE_INDEX_INVALID;
   _NextShortStatisticMale = 24;
   _NextShortStatisticFemale = 24;
} // StatisticsInit

//------------------------------------------------------------------------------
//  Resume
//------------------------------------------------------------------------------

void StatisticsResume( void)
// Resume weighing statistics after power on
{
TDayNumber Day;
TArchive Archive;
   // check for last average :
   Day = WeighingDay();

   _NextShortStatisticMale = ShortStatisticsResume( WeighingSexCurrent());
   if( WeighingHasFemale()){
      _NextShortStatisticFemale = ShortStatisticsResume( SEX_FEMALE);
   }

   while(!ArchiveOpen( &Archive));
   _LastDayIndex = ArchiveSearch( &Archive, Day);     // get active day marker
   ArchiveClose( &Archive);
   if( Day <= WeighingInitialDay()){
      return;                               // empty archive
   }
   Day--;                                   // already closed day
   // find last average records :
   _LastIndex = _SearchLast( WeighingSexCurrent());
   if( !WeighingHasFemale()){
      return;
   }
   // find male last average :
   _LastFemaleIndex = _SearchLast( SEX_FEMALE);
} // StatisticsResume

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void StatisticsStart( void)
// Start weighing statistics
{
   _LastIndex       = ARCHIVE_INDEX_INVALID;     // previous statistic is empty
   _LastFemaleIndex = ARCHIVE_INDEX_INVALID;     // previous statistic is empty
   _LastDayIndex    = ARCHIVE_INDEX_INVALID;     // previous day is empty
   _NextShortStatisticMale = WeighingConfiguration.Statistic.ShortPeriod - 1;
   _NextShortStatisticFemale = WeighingConfiguration.Statistic.ShortPeriod - 1;
} // StatisticsStart

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

void StatisticsExecute( void)
// Execute short weighing statistics
{
   ShortStatisticsExecute( WeighingHour());
} // StatisticsExecute

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void StatisticsDayClose( void)
// Close day
{
TArchiveIndex Index;
   ShortStatisticsExecute( 55);
   Index = Statistic( 0, 23, WeighingSexCurrent());
   if(Index != ARCHIVE_INDEX_INVALID) {
      _LastIndex = Index;
   }
   // save female item :
   if( WeighingHasFemale()){
      Index = Statistic( 0, 23, SEX_FEMALE);
      if(Index != ARCHIVE_INDEX_INVALID) {
         _LastFemaleIndex = Index;
      }
   }
} // StatisticsDayClose

//------------------------------------------------------------------------------
//  Next
//------------------------------------------------------------------------------

void StatisticsDayNew( void)
// Prepare a new day
{
   _DayNew();                          // save new day marker
   _NextShortStatisticMale = WeighingConfiguration.Statistic.ShortPeriod - 1;
   _NextShortStatisticFemale = WeighingConfiguration.Statistic.ShortPeriod - 1;
} // StatisticsDayNew

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

void StatisticsStop( void)
// Stop weighing statistics
{
TArchiveIndex Index;

   // check for last day closed :
   Index = StatisticsFind( WeighingDay(), WeighingSexCurrent());
   if( Index != ARCHIVE_INDEX_INVALID){
      return;                          // already closed
   }
   StatisticsDayClose();               // close day
} // StatisticsStop

//------------------------------------------------------------------------------
//  Find
//------------------------------------------------------------------------------

TArchiveIndex StatisticsFind( TDayNumber Day, byte Sex)
// Find day record by <Day>
{
TArchiveIndex Index;
TArchiveItem  Item;
TArchive Archive;
   while(!ArchiveOpen( &Archive));
   // check for day marker :
   Index = ArchiveSearch( &Archive, Day);
   if( Index == ARCHIVE_INDEX_INVALID){
      ArchiveClose( &Archive);
      return( ARCHIVE_INDEX_INVALID);  // no such day
   }
   Index++;                            // skip marker
   while( ArchiveGet( &Archive, Index, &Item)){
      if( Item.Data.Day != Day){
         break;
      }
      // check for hour :
      if( !ArchiveIsDaily( &Item.Data)){
         Index++;
         continue;
      }
      // check for sex :
      if( Sex != SEX_INVALID){
         if( Item.Data.Sex != Sex){
            Index++;
            continue;
         }
      }
      ArchiveClose( &Archive);
      return( Index);
   }
   ArchiveClose( &Archive);
   return( ARCHIVE_INDEX_INVALID);     // not found
} // StatisticsFind

//------------------------------------------------------------------------------
//  Last day
//------------------------------------------------------------------------------

TArchiveIndex StatisticsLastDay( void)
// Returns last day marker index
{
   return( _LastDayIndex);
} // StatisticsLastDay

//------------------------------------------------------------------------------
//  Last average
//------------------------------------------------------------------------------

TYesNo StatisticsLast( TArchiveItem *Item)
// Returns (male) last <Statistic>
{
TArchive Archive;
TYesNo Success;
   if( _LastIndex == ARCHIVE_INDEX_INVALID){
      return( NO);
   }
   while(!ArchiveOpen( &Archive));
   Success = ArchiveGet( &Archive, _LastIndex, Item);
   ArchiveClose(  &Archive);
   return Success;
} // StatisticsLast

//------------------------------------------------------------------------------
//  Last average female
//------------------------------------------------------------------------------

TYesNo StatisticsLastFemale( TArchiveItem *Item)
// Returns female last <Statistic>
{
TArchive Archive;
TYesNo Success;
   if( _LastFemaleIndex == ARCHIVE_INDEX_INVALID){
      return( NO);
   }
   while(!ArchiveOpen( &Archive));
   Success = ArchiveGet( &Archive, _LastFemaleIndex, Item);
   ArchiveClose(  &Archive);
   return Success;
} // StatisticLastFemale

//------------------------------------------------------------------------------
//  Last average
//------------------------------------------------------------------------------

TWeightGauge StatisticsLastAverage( byte Sex)
// Returns last average weight or WEIGHT_INVALID on empty archive
{
TArchiveItem Item;

   if(Sex == SEX_FEMALE) {
      if( !StatisticsLastFemale( &Item)){
         return( WEIGHT_INVALID);
      }
      return( Item.Data.Average);
   }

   if( !StatisticsLast( &Item)){
      return( WEIGHT_INVALID);
   }
   return( Item.Data.Average);
} // StatisticsLastAverage

//******************************************************************************

//------------------------------------------------------------------------------
//  New day
//------------------------------------------------------------------------------

static void _DayNew( void)
// Start a new day
{
TArchiveItem Marker;
TArchive Archive; 
   
   memset(&Marker, 0, sizeof(TArchiveItem));
   Marker.Marker.Day                = WeighingDay();
   Marker.Marker.TargetWeight       = WeighingTarget();
   Marker.Marker.TargetWeightFemale = WeighingTargetFemale();
   while(!ArchiveOpen( &Archive));
   ArchiveDayStart( &Archive, &Marker);
   _LastDayIndex = ArchiveCount() - 1; // remember index of marker
   ArchiveClose( &Archive);
} // _DayNew

//------------------------------------------------------------------------------
//  Search last
//------------------------------------------------------------------------------

TArchiveIndex _SearchLast( byte Sex)
// Find last possible record by <Sex>
{
TArchiveIndex Index;
TArchiveItem  Item;
TArchive      Archive;
   while(!ArchiveOpen( &Archive));
   Index = ArchiveCount();
   forever {
      if(Index == 0) {
         ArchiveClose( &Archive);
         return ARCHIVE_INDEX_INVALID;
      }
      Index--;
      if( !ArchiveGet( &Archive, Index, &Item)){
         continue;
      }
      // check for marker :
      if( ArchiveIsMarker( &Item)){
         continue;                          // skip marker
      }
      // check for sex :
      if( Sex != SEX_INVALID){
         if( Item.Data.Sex != Sex){
            continue;                       // skip item with different sex
         }
      }
      // check for samples count :
      /*if( Item.Data.Count == 0){
         continue;                          // skip item without samples
      }*/
      // check for hour :
      if( !ArchiveIsDaily( &Item.Data)){
         continue;
      }

      ArchiveClose( &Archive);
      return( Index);                       // item found
   }
} // _SearchLast

//------------------------------------------------------------------------------
//  Calculate statistic
//------------------------------------------------------------------------------

static TArchiveIndex Statistic( byte HourFrom, byte HourTo, byte Sex)
// Calculates current days statistic <HourFrom> to <HourTo> for <Sex>, returns archive index
{
TArchive Archive;
TArchiveItem Item;
TArchiveIndex LastIndex;
TCalculate Calculate;
TStatistic Statistic;
TWeightGauge Target;

   if(Sex == SEX_FEMALE) {
      Target = WeighingTargetFemale();
   } else {
      Target = WeighingTarget();
   }

   CalculateInit( &Calculate, Sex, WeighingTarget(), StatisticsLastAverage( Sex), HourFrom, HourTo);
   CalculateAll( &Calculate);                        // recalculate statistics
   CalculateStatistic( &Calculate, &Statistic);      // update statistic
   ArchiveStatisticSet( &Item, &Statistic);
   ArchiveHistogramSet( &Item, CalculateHistogram( &Calculate));

   if(Item.Data.Count == 0) {
      return ARCHIVE_INDEX_INVALID;
   }

   Item.Data.HourFrom = HourFrom;
   Item.Data.HourTo = HourTo;
   Item.Data.Day  = WeighingDay();
   Item.Data.Sex = Sex;
   while( !ArchiveOpen(&Archive));
   ArchiveAppend( &Archive, &Item);                        // save (male) archive item
   LastIndex = ArchiveCount() - 1;
   ArchiveClose( &Archive);
   return LastIndex;
} // CalculateStatistic

//------------------------------------------------------------------------------
//  Calculate short statistic
//------------------------------------------------------------------------------

static void ShortStatisticsCalculate( byte HourTo, byte Sex)
// Calculates current days statistics to <HourTo> for <Sex>
{
byte HourFrom;
   if(!WeighingConfiguration.Statistic.ShortPeriod) {
      return;
   }

   switch(WeighingConfiguration.Statistic.ShortType) {
      case STATISTIC_SHORT_TYPE_INTERVAL:
         HourFrom = HourTo - WeighingConfiguration.Statistic.ShortPeriod + 1;
         break;

      case STATISTIC_SHORT_TYPE_CUMMULATIVE:
      default:
         if(HourTo == 23) { // will be calculated in daily statistics
            return;
         }
         HourFrom = 0;
         break;

   }
   Statistic( HourFrom, HourTo, Sex);
} // CalculateShortStatistics

//------------------------------------------------------------------------------
//  Execute short statistic
//------------------------------------------------------------------------------

static void ShortStatisticsExecute( byte Hour)
// Execute short statistics
{
byte HourTo;

   if(Hour > _NextShortStatisticMale) {
      HourTo = _NextShortStatisticMale;
      if(HourTo > 23) {
         HourTo = 23;
      }
      ShortStatisticsCalculate( HourTo, WeighingSexCurrent());
      _NextShortStatisticMale = HourTo + WeighingConfiguration.Statistic.ShortPeriod;
   }

   if( WeighingHasFemale()) {
      if(Hour > _NextShortStatisticFemale) {
         HourTo = _NextShortStatisticFemale;
         if(HourTo > 23) {
            HourTo = 23;
         }
         ShortStatisticsCalculate( HourTo, SEX_FEMALE);
         _NextShortStatisticFemale  = HourTo + WeighingConfiguration.Statistic.ShortPeriod;
      }
   }
} // ShortStatisticsExecute

//------------------------------------------------------------------------------
//  Search last short statistic
//------------------------------------------------------------------------------

static byte _ShortStatisticSearchLast( byte Sex, byte *Hour)
// Search last short statistic for current day and <Sex>, returns success and <Hour> of last statistic
{
TArchiveIndex Index;
TArchiveItem  Item;
TArchive      Archive;
TDayNumber Day = WeighingDay();
TYesNo Result = NO;
   while(!ArchiveOpen( &Archive));
   Index = ArchiveCount();
   forever {
      if(Index == 0) {
         break;
      }
      Index--;
      if( !ArchiveGet( &Archive, Index, &Item)){
         break;
      }
      // check for marker :
      if( ArchiveIsMarker( &Item)){
         break;
      }
      if( Item.Data.Day != Day){
         break;
      }
      // check for sex :
      if( Sex != SEX_INVALID){
         if( Item.Data.Sex != Sex){
            continue;                       // skip item with different sex
         }
      }
      *Hour = Item.Data.HourTo;
      Result = YES;
      break;
   }
   ArchiveClose( &Archive);
   return Result;
} // _ShortStatisticSearchLast

//------------------------------------------------------------------------------
//  Resume short statistic
//------------------------------------------------------------------------------

static byte ShortStatisticsResume( byte Sex)
// Resumes short statistics for <Sex>, returns next calculation hour
{
byte HourTo;
byte WeighingHourNow =  WeighingHour();

   if(_ShortStatisticSearchLast(Sex, &HourTo)) {
      if(HourTo == 23) {
         HourTo = 24;
      } else {
         HourTo += WeighingConfiguration.Statistic.ShortPeriod;
      }
   } else {
      HourTo = WeighingConfiguration.Statistic.ShortPeriod - 1;
   }

   forever {
      if(WeighingHourNow < HourTo) {
         return HourTo;
      }
      ShortStatisticsCalculate( HourTo, Sex);
      HourTo += WeighingConfiguration.Statistic.ShortPeriod;
   }
} // ResumeShortStatistics
