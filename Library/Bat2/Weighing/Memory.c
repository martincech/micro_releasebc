//******************************************************************************
//
//   Memory.c       External memory
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Weighing/Memory.h"
#include "Diagnostic/Diagnostic.h"
#include "File/Efs.h"
#include <string.h>
#include <stdarg.h>
#include "Console/conio.h"
#include "Console/xprint.h"
#include "Console/sputchar.h"
#include "System/System.h"
#include "Remote/Socket.h"
#include "Usb/Usb.h"

#define FILE_DIAGNOSTIC_NAME  "Diagnostics.csv"

#define TEXT_BUFFER_SIZE      255

#define DEFAULT_DIR_NAME      "Default"

typedef enum {
   DIAGNOSTIC_IDLE,
   DIAGNOSTIC_START,
   DIAGNOSTIC_RUNNING
} EDiagnosticState;

static byte DiagnosticState = DIAGNOSTIC_IDLE;

// Local functions :

static void _DiagnosticStartAndWrite( void);
// Start new file and write samples

static void _DiagnosticWrite( void);
// Write samples

static void _DiagnosticPrintSample(TDiagnosticSample Sample);
// Diagnostic print single sample

static void _DiagnosticPrint( void);
// Diagnostic print samples

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void MemoryInit( void)
// Initialize
{
} // MemoryInit

//------------------------------------------------------------------------------
//  Resume
//------------------------------------------------------------------------------

void MemoryResume( void)
// Resume weighing after power on
{
} // MemoryResume

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void MemoryStart( void)
// Start weighing
{
   MemoryMsdDump(NULL);
} // MemoryStart

//------------------------------------------------------------------------------
//  Next
//------------------------------------------------------------------------------

void MemoryDayClose( void)
// Close day
{
   MemoryMsdDump(NULL);
} // MemoryDayClose

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

void MemoryStop( void)
// Stop weighing
{
   MemoryMsdDump(NULL);
} // MemoryStop

//------------------------------------------------------------------------------
//  Memory diagnostic start
//------------------------------------------------------------------------------

static dword DiagnosticSamples;
static dword DiagnosticLostSamples;

void MemoryDiagnosticStart( void) {
   DiagnosticState = DIAGNOSTIC_START;
   DiagnosticSamples = 0;
   DiagnosticLostSamples = 0;
} // MemoryDiagnosticStart

//------------------------------------------------------------------------------
//  Memory diagnostic stop
//------------------------------------------------------------------------------

void MemoryDiagnosticStop( void) {
   _DiagnosticWrite();
   DiagnosticState = DIAGNOSTIC_IDLE;
} // MemoryDiagnosticStop

//------------------------------------------------------------------------------
//  Memory diagnostic write
//------------------------------------------------------------------------------

void MemoryDiagnosticWrite( void) {
   switch(DiagnosticState) {
      case DIAGNOSTIC_START:
         _DiagnosticStartAndWrite();
         DiagnosticState = DIAGNOSTIC_RUNNING;
         break;

      case DIAGNOSTIC_RUNNING:
         _DiagnosticWrite();
         break;

      default:
         break;
   }
} // MemoryDiagnosticWrite

dword MemoryDiagnosticSamples( void) {
   return DiagnosticSamples;
}

dword MemoryDiagnosticLostSamples( void) {
   return DiagnosticLostSamples;
}

//*****************************************************************************

//------------------------------------------------------------------------------
//   Diagnostic start and write
//------------------------------------------------------------------------------

#define DIAGNOSTIC_OPERATION_TIMEOUT     5000
#define _DiagnosticOperationTimeoutSet() _DiagnosticOperationStart = SysTime() + DIAGNOSTIC_OPERATION_TIMEOUT;
#define _DiagnosticOperationTimeout()    TimeAfter(SysTime(), _DiagnosticOperationStart)
static dword _DiagnosticOperationStart;

static void _DiagnosticStartAndWrite( void)
// Start new file and write samples
{
char DirName[EFS_NAME_MAX + 1];
   _DiagnosticOperationTimeoutSet();
   /*if(!EfsInit()) {
      return;
   }
   DirectoryName(DirName);
   _BackupExistingDirectory( DirName);
   EfsDirectoryCreate( DirName);
   EfsFileCreate( FILE_DIAGNOSTIC_NAME, 0);
   _DiagnosticPrint();
   EfsSwitchOff();*/
} // _DiagnosticStartAndWrite

//------------------------------------------------------------------------------
//   Diagnostic write
//------------------------------------------------------------------------------

static void _DiagnosticWrite( void)
// Write samples
{
char DirName[EFS_NAME_MAX + 1];
   _DiagnosticOperationTimeoutSet();
   /*if(!EfsInit()) {
      return;
   }
   DirectoryName(DirName);
   EfsDirectoryCreate(DirName);
   if(!EfsFileOpen( FILE_DIAGNOSTIC_NAME)) {
      EfsFileCreate( FILE_DIAGNOSTIC_NAME, 0);
   } else {
      EfsFileSeek(0, EFS_SEEK_END);
   }

   _DiagnosticPrint();
   EfsSwitchOff();*/
} // _DiagnosticWrite

//------------------------------------------------------------------------------
//   Diagnostic print
//------------------------------------------------------------------------------

static void _DiagnosticPrint( void)
// Diagnostic print samples
{
TDiagnosticSample Sample;
int i = 0;
   while(DiagnosticRead( &Sample)) {
      _DiagnosticPrintSample(Sample);
      i++;
      //#warning Dodelat timeout zapisu diagnostiky na flash
      /*if(_DiagnosticOperationTimeout() && i > 1000) {
         return;
      }*/
   }
} // _DiagnosticPrint

//------------------------------------------------------------------------------
//   Diagnostic print sample
//------------------------------------------------------------------------------

static void _DiagnosticPrintSample(TDiagnosticSample Sample)
// Diagnostic print single sample
{
static dword Timestamp;

   switch(Sample.Type) {
      case DIAGNOSTIC_WEIGHT:
         //_EfsPrintf( "%d,%d\r\n", Timestamp, Sample.Weight);
         DiagnosticSamples++;
         break;
      case DIAGNOSTIC_TIMESTAMP:
         Timestamp = Sample.Timestamp;
         break;
      case DIAGNOSTIC_LOST_SAMPLES:
         //_EfsPrintf( "X,%d\r\n", Sample.LostSamples);
         DiagnosticLostSamples += Sample.LostSamples;
         break;
      default:
         break;
   }
} // PrintSample

/*static void _DiagnosticPrintSample(TDiagnosticSample Sample)
// Diagnostic print single sample
{
   switch(Sample.Type) {
      case DIAGNOSTIC_WEIGHT:
         _EfsPrintf( "W,%d\r\n", Sample.Weight);
         break;
      case DIAGNOSTIC_TIMESTAMP:
         _EfsPrintf( "T,%d\r\n", Sample.Timestamp);
         break;
      case DIAGNOSTIC_LOST_SAMPLES:
         _EfsPrintf( "L,%d\r\n", Sample.LostSamples);
         break;
      default:
         break;
   }
} // PrintSample*/
