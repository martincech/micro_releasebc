//******************************************************************************
//
//   WeighingConfiguration.c  Weighing Configuration
//   Version 1.0              (c) VEIT Electronics
//
//******************************************************************************

#include "WeighingConfiguration.h"
#include "Config/Config.h"        // Project configuration
#include "Predefined/PredefinedList.h"

TWeighingConfiguration WeighingConfiguration;
TWeighingContext       WeighingContext;

//------------------------------------------------------------------------------
//   Default weighing
//------------------------------------------------------------------------------

TPredefinedWeighingIdentifier DefaultWeighingGet( void)
// Get default predefined weighing identifier
{
   if( WeighingConfiguration.Predefined == ULIST_IDENTIFIER_INVALID){
      return( ULIST_IDENTIFIER_INVALID);         // predefined weighing not set (same as selectable)
   }
   if( WeighingConfiguration.MenuMask & (WCMM_TYPE)){
      return( WeighingConfiguration.Predefined); // fixed configuration
   }
   return( ULIST_IDENTIFIER_INVALID);            // selectable predefined weighing
} // _DefaultWeighing

//------------------------------------------------------------------------------
//   Set default weighing
//------------------------------------------------------------------------------

void DefaultWeighingSet( TPredefinedList *PredefinedList, TPredefinedWeighingIdentifier Identifier)
// Set default predefined weighing <Identifier>
{
   if( Identifier != ULIST_IDENTIFIER_INVALID){
      // fixed predefined weighing
      PredefinedListIdentifierLoad( PredefinedList, &WeighingConfiguration, Identifier);
      WeighingConfiguration.MenuMask |= (WCMM_TYPE);  // disable predefined weighing selection
      ConfigWeighingConfigurationSave();                   // save weighing configuration
      return;
   }
   // selectable predefined weighing
   WeighingConfiguration.MenuMask &= ~(WCMM_TYPE);    // enable predefined weighing selection
   ConfigWeighingConfigurationSave();
} // _DefaultWeighingSet

//------------------------------------------------------------------------------
//  Weighing configuration
//------------------------------------------------------------------------------

const TWeighingConfiguration WeighingConfigurationDefault = {
   /* Name */                  WEIGHING_CONFIGURATION_NAME_DEFAULT,
   /* Flock */                 WEIGHING_CONFIGURATION_FLOCK_DEFAULT,
   /* MenuMask */              0,
   /* InitialDay */            WEIGHING_CONFIGURATION_INITIAL_DAY_DEFAULT,
   /* CorrectionCurve */       WEIGHING_CONFIGURATION_CORRECTION_CURVE_DEFAULT,
   /* Predefined */            WEIGHING_CONFIGURATION_PREDEFINED_DEFAULT,
   /* Male */ {
      /* InitialWeight */      WEIGHING_CONFIGURATION_INITIAL_WEIGHT_DEFAULT,
      /* GrowthCurve */        WEIGHING_CONFIGURATION_CURVE_DEFAULT,
   },
   /* Female */ {
      /* InitialWeight */      WEIGHING_CONFIGURATION_INITIAL_WEIGHT_DEFAULT,
      /* GrowthCurve */        WEIGHING_CONFIGURATION_CURVE_DEFAULT,
   },
   /* TargetWeights */ {

      /* Mode */               PREDICTION_MODE_AUTOMATIC,
      /* Growth */             PREDICTION_GROWTH_SLOW,
      /* SexDifferentiation */ SEX_DIFFERENTIATION_NO,
      /* Sex */                SEX_UNDEFINED,
      /* AdjustTargetWeights */ADJUST_TARGET_WEIGHTS_NONE
   },
   /* Detection */ {
      /* Filter */             WEIGHING_DETECTION_FILTER_DEFAULT,
      /* StabilizationTime */  WEIGHING_DETECTION_STABILIZATION_TIME_DEFAULT,
      /* StabilizationRange */ WEIGHING_DETECTION_STABILIZATION_RANGE_DEFAULT,
      /* Step */               PLATFORM_STEP_BOTH
   },
   /* Acceptance */ {
      /* Male */ {
         /* MarginAbove */       WEIGHING_ACCEPTANCE_MARGIN_ABOVE_DEFAULT,
         /* MarginBelow */       WEIGHING_ACCEPTANCE_MARGIN_BELOW_DEFAULT
      },
      /* Female */ {
         /* MarginAbove */       WEIGHING_ACCEPTANCE_MARGIN_ABOVE_DEFAULT,
         /* MarginBelow */       WEIGHING_ACCEPTANCE_MARGIN_BELOW_DEFAULT
      }
   },
   /* Statistic */ {
      /* UniformityRange */  10,
      /* Short period */ STATISTIC_SHORT_PERIOD_DEFAULT,
      /* Short type */ STATISTIC_SHORT_TYPE_INTERVAL,
      /* spare */ 0,
      { /* Histogram */
         /* Mode  */ HISTOGRAM_MODE_RANGE,
         /* Range */ 30,
         /* Step  */ 100,
      }
   },
   /* DayStart */              WEIGHING_CONFIGURATION_DAY_START_DEFAULT,
   /* Planning */              NO,
   /* WeighingPlan */          0
};

//------------------------------------------------------------------------------
//  Weighing context
//------------------------------------------------------------------------------

const TWeighingContext WeighingContextDefault = {
   /* Status */             WEIGHING_STATUS_STOPPED,
   /* LastStatus */         WEIGHING_STATUS_STOPPED,
   /* Day */                WEIGHING_CONTEXT_DAY_DEFAULT,
   /* DayActive */          NO,
   /* _Dummy1 */            0,
   /* _Dummy2 */            0,
   /* TargetWeight */       WEIGHING_CONTEXT_TARGET_DEFAULT,
   /* TargetWeightFemale */ WEIGHING_CONTEXT_TARGET_DEFAULT,
   /* Correction */         WEIGHING_CONTEXT_CORRECTION_DEFAULT,
   /* StartAt */            WEIGHING_CONTEXT_START_AT_DEFAULT,
   /* DayZero */            WEIGHING_CONTEXT_DAY_ZERO_DEFAULT,
   /* DayCloseAt */         WEIGHING_CONTEXT_DAY_CLOSE_AT_DEFAULT,
   /* DayDuration */        WEIGHING_CONTEXT_DAY_DURATION_DEFAULT
};

//------------------------------------------------------------------------------
//  Current sex
//------------------------------------------------------------------------------

byte WeighingSexCurrent( void)
// Returns current sex for male position
{
   switch( WeighingSexDifferentation()){
      case SEX_DIFFERENTIATION_NO :
         return( WeighingSex());

      case SEX_DIFFERENTIATION_YES :
         return( SEX_MALE);

      case SEX_DIFFERENTIATION_STEP_ONLY :
         return( SEX_UNDEFINED);

   }
   return( SEX_UNDEFINED);
} // WeighingSexCurrent

//------------------------------------------------------------------------------
//  Normalize weighing hour
//------------------------------------------------------------------------------

byte WeighingHourNormalize( UTimeGauge Time)
// Normalize weighing hour
{
   int Diff = Time - WeighingDayStart();
   if(Diff < 0) {
      Diff = Diff + TIME_DAY;
   }
   return Diff / TIME_HOUR;
} // WeighingHourNormalize

//------------------------------------------------------------------------------
//  Weighing hour
//------------------------------------------------------------------------------
#include "System/System.h"

byte WeighingHour( void)
// Get hour number
{
   UDateTimeGauge DateTime = uClockDateTime( SysClock());
   return WeighingHourNormalize( uDateTimeTime(DateTime));
} // WeighingHour
