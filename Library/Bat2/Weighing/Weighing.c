//******************************************************************************
//
//   Weighing.c     Weighing services
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Weighing/Weighing.h"
#include "Diagnostic/Diagnostic.h"
#include "System/System.h"
#include "Config/Config.h"
#include "Platform/Zone.h"
#include "Weighing/WeighingConfiguration.h"
#include "Weighing/Statistics.h"
#include "Weighing/Prediction.h"
#include "Weighing/Memory.h"
#include "Weighing/Correction.h"
#include "Message/Message.h"
#include "Archive/Archive.h"
#include "Predefined/PredefinedList.h"
#include "Remote/SocketIfEthernet.h"

// weighing activity :
typedef enum {
   WEIGHING_ACTIVITY_UNDEFINED,
   WEIGHING_ACTIVITY_PAUSED,
   WEIGHING_ACTIVITY_WEIGHING,
   WEIGHING_ACTIVITY_DIAGNOSTIC,
   WEIGHING_ACTIVITY_CALIBRATION
} EWeigningActivity;

static byte _WeighingActivity;

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void WeighingInit( void)
// Initialize
{
   ArchiveInit();                      // restore archive
   CorrectionInit();                   // initialize weighing correction
   PredictionInit();                   // initialize weight prediction
   StatisticsInit();                   // initialize weighing statistics
   MemoryInit();                       // initialize external memory
   ZoneInit();                         // initialize weighing zone
   PredefinedListInit();               // initialize predefined weighings
   _WeighingActivity = WEIGHING_ACTIVITY_UNDEFINED;
} // WeighingInit

//------------------------------------------------------------------------------
//  Resume
//------------------------------------------------------------------------------

void WeighingResume( void)
// Power on initialization
{
   // restore old context :
   StatisticsResume();
   CorrectionResume();
   PredictionResume();
   ZoneResume();                       // must by after statistics resumed
   MessageResume();                    // must be after statistics resumed
   _WeighingActivity = WEIGHING_ACTIVITY_UNDEFINED;
} // WeighingResume

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

void WeighingExecute( void)
// Executive
{
   ZoneExecute();                      // zone executive
   StatisticsExecute();                // execute short statistics
} // WeighingExecute

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void WeighingStart( void)
// Start weighing
{
TArchive Archive;
   ZoneClear();                        // clear samples FIFO & statistics
   if(!ArchiveOpen(&Archive)) {
      return;
   }
   ArchiveClear(&Archive);             // clear statistics archive
   ArchiveClose(&Archive);
   PredictionStart();                  // calculate prediction first
   CorrectionStart();                  // calculate weighing correction
   StatisticsStart();                  // start statistics
   MemoryStart();                      // start memory
   MessageStart();                     // start GSM messaging
   // don't start zone - controlled by scheduler :
   _WeighingActivity = WEIGHING_ACTIVITY_UNDEFINED;
} // WeighingStart

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

void WeighingStop( void)
// Stop weighing
{
   ZoneStop();                         // stop weighing immediately
   switch(_WeighingActivity) {
      case WEIGHING_ACTIVITY_DIAGNOSTIC:
         DiagnosticStop();
         _WeighingActivity = WEIGHING_ACTIVITY_UNDEFINED;
         return;

      case WEIGHING_ACTIVITY_CALIBRATION:
         _WeighingActivity = WEIGHING_ACTIVITY_UNDEFINED;
         break;
   }
   StatisticsStop();
   MemoryStop();
   MessageStop();                      // stop GSM messaging
   _WeighingActivity = WEIGHING_ACTIVITY_UNDEFINED;
} // WeighingStop

//------------------------------------------------------------------------------
//  Pause
//------------------------------------------------------------------------------

void WeighingPause( void)
// Pause weighing
{
   if( _WeighingActivity == WEIGHING_ACTIVITY_PAUSED){
      return;
   }
   _WeighingActivity = WEIGHING_ACTIVITY_PAUSED;
   ZoneStop();                         // stop weighing
} // WeighingPause

//------------------------------------------------------------------------------
//  Run
//------------------------------------------------------------------------------

void WeighingWeighing( void)
// Run weighing
{
   if( _WeighingActivity == WEIGHING_ACTIVITY_WEIGHING){
      return;
   }
   _WeighingActivity = WEIGHING_ACTIVITY_WEIGHING;
   ZoneStart();                        // start weighing
} // WeighingWeighing

//------------------------------------------------------------------------------
//  Calibration
//------------------------------------------------------------------------------

void WeighingCalibration( void)
// Start calibration
{
   if( _WeighingActivity == WEIGHING_ACTIVITY_DIAGNOSTIC){
      return;
   }
   _WeighingActivity = WEIGHING_ACTIVITY_CALIBRATION;
   ZoneCalibration();                  // start calibration
} // WeighingCalibration

//------------------------------------------------------------------------------
//  Diagnostics start
//------------------------------------------------------------------------------

void WeighingDiagnostics( void)
// Start diagnostics
{
   if( _WeighingActivity == WEIGHING_ACTIVITY_DIAGNOSTIC){
      return;
   }
   _WeighingActivity = WEIGHING_ACTIVITY_DIAGNOSTIC;
   StatisticsStop();
   DiagnosticStart();
   ZoneDiagnostics();                  // start diagnostics
} // WeighingDiagnostics

//------------------------------------------------------------------------------
//   Close day
//------------------------------------------------------------------------------
void WeighingDayClose( void)
// Close weighing day
{
   StatisticsDayClose();
   MemoryDayClose();
   MessageDayClose();
   ZoneClear();                        // clear samples FIFO & statistics
   WeighingDayActiveSet( NO);          // day closed
} // WeighingDayClose

//------------------------------------------------------------------------------
//   Next day
//------------------------------------------------------------------------------

void WeighingDayNew( void)
// Start a new day
{
   PredictionNew();                    // calculate new prediction
   CorrectionNew();                    // calculate new weighing correction
   StatisticsDayNew();                 // save new prediction
   WeighingDayActiveSet( YES);         // day active
} // WeighingDayNew
