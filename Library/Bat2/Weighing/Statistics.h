//******************************************************************************
//
//   Statistics.h   Weighing statistics
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Statistics_H__
   #define __Statistics_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

#ifndef __ArchiveDef_H__
   #include "Archive/ArchiveDef.h"
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void StatisticsInit( void);
// Initialize

void StatisticsResume( void);
// Resume weighing statistics after power on

void StatisticsStart( void);
// Start weighing statistics

void StatisticsExecute( void);
// Execute short weighing statistics

void StatisticsDayClose( void);
// Close day

void StatisticsDayNew( void);
// Prepare a new day

void StatisticsStop( void);
// Stop weighing statistics

//------------------------------------------------------------------------------

TArchiveIndex StatisticsFind( TDayNumber Day, byte Sex);
// Find record by <Day>, <Hour>

TArchiveIndex StatisticsLastDay( void);
// Returns last day marker index

TYesNo StatisticsLast( TArchiveItem *Item);
// Returns (male) last <Statistic>

TYesNo StatisticsLastFemale( TArchiveItem *Item);
// Returns female last <Statistic>

TWeightGauge StatisticsLastAverage( byte Sex);
// Returns last average weight or WEIGHT_INVALID on empty archive

#endif
