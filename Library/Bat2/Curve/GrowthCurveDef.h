//******************************************************************************
//
//   GrowthCurveDef.h  Growth curve data
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GrowthCurveDef_H__
   #define __GrowthCurveDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

#ifndef __WeightDef_H__
   #include "Weight/WeightDef.h"
#endif

#ifndef __uNamedListDef_H__
   #include "Data/uNamedListDef.h"
#endif

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define GROWTH_CURVE_NAME_SIZE   UDIRECTORY_NAME_SIZE
#define GROWTH_CURVE_POINT_COUNT 30

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef UDirectoryIndex TCurveIndex;
typedef UDirectoryIdentifier TCurveIdentifier;

//------------------------------------------------------------------------------
//  Curve point
//------------------------------------------------------------------------------

typedef struct {
   TDayNumber   Day;                                  // Technological day
   word         _Dummy;
   TWeightGauge Weight;                               // Target weight
} TCurvePoint;

//------------------------------------------------------------------------------
//  Growth curve
//------------------------------------------------------------------------------

typedef struct {
   char        Name[ GROWTH_CURVE_NAME_SIZE + 1];     // Curve name (must be first item !)
   byte        Deleted;                               // Deleted curve
   word        Count;                                 // Curve points count
   TCurvePoint Point[ GROWTH_CURVE_POINT_COUNT];      // Growth curve points
} TGrowthCurve;

//------------------------------------------------------------------------------

#ifdef _MANAGED
#undef _MANAGED
#include "GrowthCurveDef.h"
#define _MANAGED
namespace Bat2Library{
   public ref class GrowthCurveC abstract sealed{
   public:
      literal TDayNumber POINT_COUNT_MAX = GROWTH_CURVE_POINT_COUNT;
      literal byte CURVE_NAME_SIZE_MAX_LENGTH = GROWTH_CURVE_NAME_SIZE;
   };
}
#endif
#endif
