//******************************************************************************
//
//   CurveList.h  Growth curve list
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __CurveList_H__
   #define __CurveList_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __GrowthCurveDef_H__
   #include "Curve/GrowthCurveDef.h"
#endif   

#ifdef __WIN32__
   #ifndef __uStorageDef_H__
      #include "Data/uStorageDef.h"
   #endif
   DllImportExport extern const UDirectoryDescriptor _CurveListDescriptor;
#endif
typedef UNamedList   TCurveList;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif
TYesNo CurveListRemoteLoad( void);
// Load from remote device

TYesNo CurveListRemoteSave( void);
// Save to remote device

void CurveListInit( void);
// Initialize
TYesNo CurveListOpen( TCurveList *CurveList);
// open

void CurveListClose( TCurveList *CurveList);
// close

TCurveIdentifier CurveListIdentifier( TCurveList *CurveList, TCurveIndex Index);
// Returns identifier of curve at <Index>

TCurveIndex CurveListIndex( TCurveList *CurveList, TCurveIdentifier Identifier);
// Returns index of curve by curve <Identifier>

TYesNo CurveListName( TCurveList *CurveList, TCurveIdentifier Identifier, char *Name);
// Returns curve <Name>

void CurveListLoad( TCurveList *CurveList, TGrowthCurve *Curve, TCurveIndex Index);
// Load <Curve> by <Index>

void CurveListSave( TCurveList *CurveList, TGrowthCurve *Curve, TCurveIndex Index);
// Save <Curve> at <Index>

TYesNo CurveListIdentifierLoad( TCurveList *CurveList, TGrowthCurve *Curve, TCurveIdentifier Identifier);
// Load <Curve> by <Identifier>
#ifdef __cplusplus
   }
#endif

#endif
