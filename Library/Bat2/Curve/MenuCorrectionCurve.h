//******************************************************************************
//                                                                            
//   MenuCorrectionCurve.h Correction curve viewer
//   Version 1.0           (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuCorrectionCurve_H__
   #define __MenuCorrectionCurve_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#ifndef __CorrectionCurveDef_H__
   #include "Curve/CorrectionCurveDef.h"
#endif

#ifndef __CorrectionList_H__
   #include "Curve/CorrectionList.h"
#endif

void MenuCorrectionCurve( TCorrectionList *CorrectionList);
// Display correction curve

TYesNo MenuCorrectionCurveSelect( TCorrectionList *CorrectionList, TUniStr Title, TCorrectionIdentifier *Identifier, TUniStr SpecialItem);
// Select correction curve, add <SpecialItem> at end of list

void MenuCorrectionCurveEdit( TCorrectionCurve *Curve);
// Display/edit correction curve

#endif
