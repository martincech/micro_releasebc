//******************************************************************************
//                                                                            
//   MenuGrowthCurve.h  Growth curve viewer
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGrowthCurve_H__
   #define __MenuGrowthCurve_H__

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#ifndef __GrowthCurveDef_H__
   #include "Curve/GrowthCurveDef.h"
#endif

#ifndef __CurveList_H__
   #include "Curve/CurveList.h"
#endif

void MenuGrowthCurve( TCurveList *CurveList);
// Growth curve menu

TYesNo MenuGrowthCurveSelect( TCurveList *CurveList, TUniStr Title, TCurveIdentifier *Identifier, TUniStr SpecialItem);
// Select growth curve, add <SpecialItem> at end of list

void MenuGrowthCurveEdit( TGrowthCurve *Curve);
// Display/edit growth curve

#endif
