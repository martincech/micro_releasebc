//*****************************************************************************
//
//    Kbx.c        Keyboard matrix services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Kbd/Kbx.h"         // Keyboard matrix

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void KbxInit( void)
// Initialization
{
   KbdPortInit();
} // KbdInit

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

int KbxRead( void)
// Read keyboard matrix
{
	// Row 0 :
	KbdRow0Set();
   SysUDelay(1);
	if( KbdCol0()){
   	KbdRow0Release();
		return( K_ESC);
	}
	if( KbdCol1()){
   	KbdRow0Release();
		return( K_UP);
	}
	if( KbdCol2()){
   	KbdRow0Release();
		return( K_ENTER);
	}
	KbdRow0Release();
	// Row 1 :
	KbdRow1Set();
   SysUDelay(1);
	if( KbdCol0()){
   	KbdRow1Release();
		return( K_LEFT);
	}
	if( KbdCol1()){
   	KbdRow1Release();
		return( K_DOWN);
	}
	if( KbdCol2()){
   	KbdRow1Release();
		return( K_RIGHT);
	}
	KbdRow1Release();	
   return( K_RELEASED);
} // KbxRead
