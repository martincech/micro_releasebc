//*****************************************************************************
//
//    KbxBat2Rot.c   Bat2 keyboard, rotated connector
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "Kbd/Kbx.h"         // Keyboard matrix
#include "System/System.h"

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void KbxInit( void)
// Initialization
{
   KbdPortInit();
} // KbdInit

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

int KbxRead( void)
// Read keyboard
{
   SysUDelay(2);
   #warning Cekani nemusi byt asi tak dlouhe
   if( KbdK0()){
      return( K_DOWN);
   }	
   if( KbdK1()){
      return( K_LEFT);		
   }
   if( KbdK2()){
      return( K_ESC);
   }

   KbdK1Set();
   SysUDelay(2);
   if( KbdK0()){
      KbdK1Release();
      return( K_RIGHT);
   }
   KbdK1Release();

   KbdK2Set();
   SysUDelay(2);
   if( KbdK0()){	
      KbdK2Release();
      return( K_ENTER);
   }
   if( KbdK1()){
      KbdK2Release();	
	  return( K_UP);
   }
   KbdK2Release();

   return( K_RELEASED);
} // KbxRead