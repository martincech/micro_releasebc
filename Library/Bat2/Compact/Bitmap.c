//*****************************************************************************
//
//    Bitmap.c     Project bitmaps
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Bitmap.h"

//------------------------------------------------------------------------------
//  Off screen
//------------------------------------------------------------------------------

#include "Bitmap/BmpOff.c"

//------------------------------------------------------------------------------
//  Status Line
//------------------------------------------------------------------------------

#include "Bitmap/BmpBattery.c"
#include "Bitmap/BmpPC.c"
#include "Bitmap/BmpCharger.c"
#include "Bitmap/BmpUsbStick.c"

#include "Bitmap/BmpAm.c"
#include "Bitmap/BmpPm.c"

//------------------------------------------------------------------------------
//  Big accu
//------------------------------------------------------------------------------

#include "Bitmap/BmpAccu.c"

//------------------------------------------------------------------------------
//  Screen
//------------------------------------------------------------------------------

#include "Bitmap/BmpWeight.c"
#include "Bitmap/BmpSigma.c"
#include "Bitmap/BmpCount.c"
#include "Bitmap/BmpSexMale.c"
#include "Bitmap/BmpSexFemale.c"
#include "Bitmap/BmpUpArrow.c"
#include "Bitmap/BmpDownArrow.c"

//------------------------------------------------------------------------------
//  Statistics
//------------------------------------------------------------------------------

#include "Bitmap/BmpStatisticsCount.c"
#include "Bitmap/BmpStatisticsWeight.c"
#include "Bitmap/BmpStatisticsSigma.c"
#include "Bitmap/BmpStatisticsCv.c"
#include "Bitmap/BmpStatisticsUni.c"

#include "Bitmap/BmpStatisticsTotal.c"
#include "Bitmap/BmpStatisticsMale.c"
#include "Bitmap/BmpStatisticsFemale.c"

#include "Bitmap/BmpStatisticsTarget.c"
#include "Bitmap/BmpStatisticsLastAverage.c"
#include "Bitmap/BmpStatisticsGain.c"

//------------------------------------------------------------------------------
//  Histogram
//------------------------------------------------------------------------------

#include "Bitmap/BmpHistogramCount.c"
#include "Bitmap/BmpHistogramWeight.c"
#include "Bitmap/BmpHistogramCursor.c"

//------------------------------------------------------------------------------
//  Layout
//------------------------------------------------------------------------------

#include "Bitmap/BmpButtonOk.c"
#include "Bitmap/BmpButtonCancel.c"
#include "Bitmap/BmpButtonUp.c"
#include "Bitmap/BmpButtonDown.c"
#include "Bitmap/BmpButtonLeft.c"
#include "Bitmap/BmpButtonRight.c"
#include "Bitmap/BmpButtonLeftRight.c"

#include "Bitmap/BmpIconTotal.c"
#include "Bitmap/BmpIconMale.c"
#include "Bitmap/BmpIconFemale.c"

#include "Bitmap/BmpIconLight.c"
#include "Bitmap/BmpIconHeavy.c"

#include "Bitmap/BmpCheckboxChecked.c"
#include "Bitmap/BmpCheckboxUnchecked.c"

//------------------------------------------------------------------------------
//  Edit
//------------------------------------------------------------------------------

#include "Bitmap/BmpEditUp.c"
#include "Bitmap/BmpEditDown.c"

//------------------------------------------------------------------------------
//  Wait
//------------------------------------------------------------------------------

#include "Bitmap/BmpWait.c"

//------------------------------------------------------------------------------
//  GSM
//------------------------------------------------------------------------------

#include "Bitmap/BmpClock.c"
#include "Bitmap/BmpPhone.c"
#include "Bitmap/BmpFlash.c"

#include "Bitmap/BmpGsmOff.c"
#include "Bitmap/BmpGsmPin.c"
#include "Bitmap/BmpGsmNoSignal.c"
#include "Bitmap/BmpGsmOk.c"
#include "Bitmap/BmpGsmSend.c"
#include "Bitmap/BmpGsmReceive.c"

//------------------------------------------------------------------------------
//  Graph
//------------------------------------------------------------------------------

#include "Bitmap/BmpGraphCursor.c"

//------------------------------------------------------------------------------
// Main screen big icons
//------------------------------------------------------------------------------

#include "Compact/Bitmap/BmpStop.c"
#include "Compact/Bitmap/BmpPause.c"
#include "Compact/Bitmap/BmpStatisticsCO2.c"
#include "Compact/Bitmap/BmpStatisticsTemperature.c"
#include "Compact/Bitmap/BmpStatisticsHumidity.c"

