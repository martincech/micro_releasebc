//*****************************************************************************
//
//    Fonts.c      Project fonts
//    Version 1.1  (c) VEIT Electronics
//
//*****************************************************************************

#include "Fonts.h"
#include "Graphic/Graphic.h"

#include "Font/Tahoma16.c"
#include "Font/Tahoma16j.c"
#include "Font/ArialBold14.c"
#include "Font/ArialBold18.c"
#include "Font/ArialBold21.c"
#include "Font/ArialBold27n.c"
#include "Font/ArialBold45n.c"
#include "Font/ArialBold55n.c"
#include "Font/ArialBold90n.c"

const TFontDescriptor const *Fonts[] = {
   /* Tahoma16      */   &FontTahoma16,
   /* Tahoma16j     */   &FontTahoma16j,
   /* ArialBold14   */   &FontArialBold14,
   /* ArialBold18   */   &FontArialBold18,
   /* ArialBold20   */   &FontArialBold21,
   /* ArialBold27n  */   &FontArialBold27n,
   /* ArialBold45n  */   &FontArialBold45n,
   /* ArialBold55n  */   &FontArialBold55n,
   /* ArialBold90n  */   &FontArialBold90n,
   /* last          */   0
};   

//------------------------------------------------------------------------------
//  Set font
//------------------------------------------------------------------------------

void SetFont( int FontNumber)
// Set font
{
   GSetFont( FontNumber);              // direct font
} // SetFont
