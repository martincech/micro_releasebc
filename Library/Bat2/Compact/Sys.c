//******************************************************************************
//
//   Scheduler.c   Bat2 Scheduler
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Hardware.h"
#include "System/System.h"
#include "Cpu/Cpu.h"

#include "Power/Power.h"
#include "Usb/Usb.h"
#include "Rtc/Rtc.h"
#include "Log/Log.h"
#include "Config/Context.h"
#include "Config/Config.h"
#include "Multitasking/Multitasking.h"
#include "Multitasking/Tasks.h"


#include "Memory/Nvm.h"                   // Nonvolatile memory
#include "Kbd/Kbd.h"
#include "Menu/Menu.h"
#include "Bootloader/Bootloader.h"

typedef enum {
   BOOT_UNDEFINED,
   BOOT_MAIN,
   BOOT_BOOTLOADER,
   BOOT_POWER_OFF,
   BOOT_REBOOT
} EBoot;

static TYesNo ModeRequest;
static byte Mode = BOOT_UNDEFINED;

static void ModeSet( byte _Mode)
// Set <Mode>
{
   if(Mode == _Mode) {
      return;
   }
   if(ModeRequest) {
      return;
   }
   ModeRequest = YES;
   Mode = _Mode;
} // ModeSet


void SysOff( void)
// Request system off
{
   ModeSet(BOOT_POWER_OFF);
}


void SysReboot( void)
// Reboot
{
   ModeSet(BOOT_REBOOT);
}

void SysBootloader( void)
// Request bootloader
{
   ModeSet(BOOT_BOOTLOADER);
}

void SysMain( void)
// Request main application
{
   ModeSet(BOOT_MAIN);
}

void AppInit()
{
   CpuInit();
   SysInit();
   WatchDogInit();                     // start watchdog
   KbdInit();
   PowerInit();
   SysMain();

   /*if(PowerEsdEvent()) {
      SysOff();
   } else if(PowerSwitchedOnByUser()) {
      SysMain();
   } else if(PowerExternal()) {
      SysMain();
   } else {
      SysOff();
   }*/

   UsbInit();
   RtcInit();                          // Realtime clock
   NvmInit();                          // FRAM/Flash init
   LogInit();

   ConfigLoad();
   ContextLoad();
   TasksInit();

   if(!MenuContext.SwitchedOff) {
      SysMain();
   } else {
      SysOff();
   }
}



int SysScheduler( void)
// Operating system scheduler
{
static TYesNo LastPowerExt = NO;
static TYesNo LastPowerFail = NO;
TYesNo PowerExt;
TYesNo PowerFail;
int Key;

   WatchDog();

   if(ModeRequest) {
      switch(Mode) {
         case BOOT_MAIN:
            UsbEnumerationEnable( YES);
            TasksRun();
            break;
      }

      switch(Mode) {
         case BOOT_POWER_OFF:
         case BOOT_BOOTLOADER:
            TasksStop();
            UsbShutdown();
            NvmShutdown();
            break;
      }

      switch(Mode) {
         case BOOT_REBOOT:
            CpuReset();
            break;

         case BOOT_POWER_OFF:
            PowerOff();
            break;

         case BOOT_BOOTLOADER:
            BootloaderTrigger();
            break;
      }
      ModeRequest = NO;
   }


   PowerFail = PowerFailure();
   PowerExt = PowerExternal();
   if( PowerFail && !LastPowerFail){
      UsbEnumerationEnable( NO);
      Key = K_POWER_FAILURE;
   } else if(!PowerFail && LastPowerFail) {
      UsbEnumerationEnable( YES);
      Key = K_POWER_OK;
   } else if(PowerExt && !LastPowerExt) {
      Key = K_POWER_CONNECTED;
   } else {
      Key = SysSchedulerDefault();
   }
   LastPowerFail = PowerFail;
   LastPowerExt = PowerExt;

   switch(Key) {
      case K_TIMER_SLOW:
         PowerExecute();
         UsbExecute();
         break;

      default:
         break;
   }

   switch(Key) {
      case K_IDLE:
         break;

      default:
         TasksEvent( Key);
         break;
   }

   if(TasksIdle()) {
      CpuIdleMode();
   } else {
      MultitaskingReschedule();
   }
   return Key;
}
