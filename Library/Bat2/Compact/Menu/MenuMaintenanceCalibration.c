//******************************************************************************
//
//   MenuMaintenanceCalibration.c  Calibration menu
//   Version 1.0                   (c) VEIT Electronics
//
//******************************************************************************

#include "../../Menu/MenuMaintenanceCalibration.h"
#include "System/System.h"        // Operating system
#include "Graphic/Graphic.h"      // graphic
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DMsg.h"          // Message box
#include "Gadget/DInput.h"
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Context.h"        // Project configuration
#include "Platform/Zone.h"                // Platform data
#include "Scheduler/WeighingScheduler.h"  // Weighing scheduler executive
#include "Weighing/pWeighing.h"           // Weighing operations
#include "Calibration/Calibration.h"      // Calibration data
#include "Gadget/DInput.h"
#include "Config/Config.h"
#include "Gadget/DLayout.h"
#include "Gadget/DLabel.h"
#include "Gadget/DEvent.h"
#include "Weight/Weight.h"
#include "Fonts.h"

#include "Weighing/WeighingConfiguration.h"
#include "Platform/Zone.h"

#define INFO_X  3
#define INFO_Y  19
#define CHAR_H  17

static void CalibrationSteps( void);

static DefMenu( CalibrationMenu)
   STR_CALIBRATION,
EndMenu()

typedef enum {
   MI_CALIBRATION
} ECalibrationMenu;

//------------------------------------------------------------------------------
//   Main loop
//------------------------------------------------------------------------------

void MenuMaintenanceCalibration( void)
// Calibration menu
{
int y;
TYesNo Redraw = YES;
TMenuData MData;

   DMenuClear( MData);
   MData.Item = MI_CALIBRATION;

   forever {
      if(Redraw){
         DMenuNoWait(STR_CALIBRATION, CalibrationMenu, NULL, NULL, &MData);
         if(WeighingStatus() == WEIGHING_STATUS_WEIGHING || WeighingStatus() == WEIGHING_STATUS_RELEASED){
            y = G_HEIGHT - DLAYOUT_STATUS_H - GCharHeight() - 2;
            DLabel("Current weight: ", 6, y);
            DWeightWithUnitsNarrowRight( DMENU_PARAMETERS_X, y, WeightGaugeDivision( ZoneActualWeight()));
         }
         GFlush();
         Redraw = NO;
      }
      switch( DEventWait()){
         case K_FLASH1 :
            Redraw = YES;
            break;

         case K_ENTER :
            WeighingSchedulerCalibration();
            CalibrationSteps();
            DMsgWait();
            WeighingSchedulerStop();
            Redraw = YES;
            break;
         case K_ESC :
         case K_TIMEOUT :
            return;
      }
   }
} // MenuMaintenanceCalibration


static void CalibrationSteps( void) {
//#ifndef OPTION_SIMULATION
int Points;
int i;
TRawWeight   Raw[PLATFORM_CALIBRATION_POINTS_MAX];
   // Enter calibration weight
   Points = PlatformCalibration.Points;
   if( !DInputNumber( STR_CALIBRATION, "Calibration points", &Points, 0, PLATFORM_CALIBRATION_POINTS_MIN, PLATFORM_CALIBRATION_POINTS_MAX, "")){
      return;
   }
   PlatformCalibration.Points = Points;

   // Unload scale
   if(!DMsgOkCancel( STR_CALIBRATION, STR_UNLOAD_SCALE, 0)) {
      return;
   }

   DMsgWait();
   PlatformCalibration.Weight[0] = 0;
   Raw[0] = WeighingRaw();

   for(i = 1 ; i < Points ; i++) {
      // Enter calibration weight
      if( !DInputWeight( STR_CALIBRATION, STR_ENTER_WEIGHT, &PlatformCalibration.Weight[i])){
         WeighingSchedulerStop();
         return;
      }
      DMsgWait();
      Raw[i] = WeighingRaw();
   }

   if(!CalibrationUpdate( PlatformCalibration.Points, Raw, PlatformCalibration.Weight)) {
      DMsgOk( STR_CALIBRATION, "Invalid calibration. Try again.", 0);
      return;
   }

   ContextCalibrationSave();
   ContextCalibrationCoefficientsSave();

   DMsgOk( STR_CALIBRATION, STR_CALIBRATION_SAVED, 0);
//#endif
}
