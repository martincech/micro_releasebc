/* AUTOGENERATED FILE. DO NOT EDIT. */
#ifndef _MOCKPOWER_H
#define _MOCKPOWER_H

#include "Power.h"
#include "CException.h"

void MockPower_Init(void);
void MockPower_Destroy(void);
void MockPower_Verify(void);




#define PowerInit_Ignore() PowerInit_CMockIgnore()
void PowerInit_CMockIgnore(void);
#define PowerInit_Expect() PowerInit_CMockExpect(__LINE__)
void PowerInit_CMockExpect(UNITY_LINE_TYPE cmock_line);
typedef void (* CMOCK_PowerInit_CALLBACK)(int cmock_num_calls);
void PowerInit_StubWithCallback(CMOCK_PowerInit_CALLBACK Callback);
#define PowerInit_ExpectAndThrow(cmock_to_throw) PowerInit_CMockExpectAndThrow(__LINE__, cmock_to_throw)
void PowerInit_CMockExpectAndThrow(UNITY_LINE_TYPE cmock_line, CEXCEPTION_T cmock_to_throw);
#define PowerUsbCurrentLimitSet_Ignore() PowerUsbCurrentLimitSet_CMockIgnore()
void PowerUsbCurrentLimitSet_CMockIgnore(void);
#define PowerUsbCurrentLimitSet_Expect(CurrentMa) PowerUsbCurrentLimitSet_CMockExpect(__LINE__, CurrentMa)
void PowerUsbCurrentLimitSet_CMockExpect(UNITY_LINE_TYPE cmock_line, word CurrentMa);
typedef void (* CMOCK_PowerUsbCurrentLimitSet_CALLBACK)(word CurrentMa, int cmock_num_calls);
void PowerUsbCurrentLimitSet_StubWithCallback(CMOCK_PowerUsbCurrentLimitSet_CALLBACK Callback);
#define PowerUsbCurrentLimitSet_ExpectAndThrow(CurrentMa, cmock_to_throw) PowerUsbCurrentLimitSet_CMockExpectAndThrow(__LINE__, CurrentMa, cmock_to_throw)
void PowerUsbCurrentLimitSet_CMockExpectAndThrow(UNITY_LINE_TYPE cmock_line, word CurrentMa, CEXCEPTION_T cmock_to_throw);
#define PowerExecute_Ignore() PowerExecute_CMockIgnore()
void PowerExecute_CMockIgnore(void);
#define PowerExecute_Expect() PowerExecute_CMockExpect(__LINE__)
void PowerExecute_CMockExpect(UNITY_LINE_TYPE cmock_line);
typedef void (* CMOCK_PowerExecute_CALLBACK)(int cmock_num_calls);
void PowerExecute_StubWithCallback(CMOCK_PowerExecute_CALLBACK Callback);
#define PowerExecute_ExpectAndThrow(cmock_to_throw) PowerExecute_CMockExpectAndThrow(__LINE__, cmock_to_throw)
void PowerExecute_CMockExpectAndThrow(UNITY_LINE_TYPE cmock_line, CEXCEPTION_T cmock_to_throw);
#define PowerSwitchedOnByUser_IgnoreAndReturn(cmock_retval) PowerSwitchedOnByUser_CMockIgnoreAndReturn(__LINE__, cmock_retval)
void PowerSwitchedOnByUser_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, TYesNo cmock_to_return);
#define PowerSwitchedOnByUser_ExpectAndReturn(cmock_retval) PowerSwitchedOnByUser_CMockExpectAndReturn(__LINE__, cmock_retval)
void PowerSwitchedOnByUser_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, TYesNo cmock_to_return);
typedef TYesNo (* CMOCK_PowerSwitchedOnByUser_CALLBACK)(int cmock_num_calls);
void PowerSwitchedOnByUser_StubWithCallback(CMOCK_PowerSwitchedOnByUser_CALLBACK Callback);
#define PowerSwitchedOnByUser_ExpectAndThrow(cmock_to_throw) PowerSwitchedOnByUser_CMockExpectAndThrow(__LINE__, cmock_to_throw)
void PowerSwitchedOnByUser_CMockExpectAndThrow(UNITY_LINE_TYPE cmock_line, CEXCEPTION_T cmock_to_throw);
#define PowerAccuPresent_IgnoreAndReturn(cmock_retval) PowerAccuPresent_CMockIgnoreAndReturn(__LINE__, cmock_retval)
void PowerAccuPresent_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, TYesNo cmock_to_return);
#define PowerAccuPresent_ExpectAndReturn(cmock_retval) PowerAccuPresent_CMockExpectAndReturn(__LINE__, cmock_retval)
void PowerAccuPresent_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, TYesNo cmock_to_return);
typedef TYesNo (* CMOCK_PowerAccuPresent_CALLBACK)(int cmock_num_calls);
void PowerAccuPresent_StubWithCallback(CMOCK_PowerAccuPresent_CALLBACK Callback);
#define PowerAccuPresent_ExpectAndThrow(cmock_to_throw) PowerAccuPresent_CMockExpectAndThrow(__LINE__, cmock_to_throw)
void PowerAccuPresent_CMockExpectAndThrow(UNITY_LINE_TYPE cmock_line, CEXCEPTION_T cmock_to_throw);
#define PowerExternal_IgnoreAndReturn(cmock_retval) PowerExternal_CMockIgnoreAndReturn(__LINE__, cmock_retval)
void PowerExternal_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, TYesNo cmock_to_return);
#define PowerExternal_ExpectAndReturn(cmock_retval) PowerExternal_CMockExpectAndReturn(__LINE__, cmock_retval)
void PowerExternal_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, TYesNo cmock_to_return);
typedef TYesNo (* CMOCK_PowerExternal_CALLBACK)(int cmock_num_calls);
void PowerExternal_StubWithCallback(CMOCK_PowerExternal_CALLBACK Callback);
#define PowerExternal_ExpectAndThrow(cmock_to_throw) PowerExternal_CMockExpectAndThrow(__LINE__, cmock_to_throw)
void PowerExternal_CMockExpectAndThrow(UNITY_LINE_TYPE cmock_line, CEXCEPTION_T cmock_to_throw);
#define PowerFailure_IgnoreAndReturn(cmock_retval) PowerFailure_CMockIgnoreAndReturn(__LINE__, cmock_retval)
void PowerFailure_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, TYesNo cmock_to_return);
#define PowerFailure_ExpectAndReturn(cmock_retval) PowerFailure_CMockExpectAndReturn(__LINE__, cmock_retval)
void PowerFailure_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, TYesNo cmock_to_return);
typedef TYesNo (* CMOCK_PowerFailure_CALLBACK)(int cmock_num_calls);
void PowerFailure_StubWithCallback(CMOCK_PowerFailure_CALLBACK Callback);
#define PowerFailure_ExpectAndThrow(cmock_to_throw) PowerFailure_CMockExpectAndThrow(__LINE__, cmock_to_throw)
void PowerFailure_CMockExpectAndThrow(UNITY_LINE_TYPE cmock_line, CEXCEPTION_T cmock_to_throw);
#define PowerOff_Ignore() PowerOff_CMockIgnore()
void PowerOff_CMockIgnore(void);
#define PowerOff_Expect() PowerOff_CMockExpect(__LINE__)
void PowerOff_CMockExpect(UNITY_LINE_TYPE cmock_line);
typedef void (* CMOCK_PowerOff_CALLBACK)(int cmock_num_calls);
void PowerOff_StubWithCallback(CMOCK_PowerOff_CALLBACK Callback);
#define PowerOff_ExpectAndThrow(cmock_to_throw) PowerOff_CMockExpectAndThrow(__LINE__, cmock_to_throw)
void PowerOff_CMockExpectAndThrow(UNITY_LINE_TYPE cmock_line, CEXCEPTION_T cmock_to_throw);

#endif
