//******************************************************************************
//
//   FactoryDefault.h    Factory default settings
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef __FactoryDefault_H__
   #define __FactoryDefault_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

void FactoryDefault( void);
// Set factory default configuration

#ifdef __cplusplus
	}
#endif

#endif
