//******************************************************************************
//
//   Config.c     Bat2 configuration
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Config.h"
#include "ConfigDef.h"
#include "Data/uStorage.h"
#include "Country/Country.h"
#include "Display/DisplayConfiguration.h"

#include "Weight/Weight.h"
#include "Weighing/WeighingConfiguration.h"
#include "Message/GsmMessage.h"
#include "Megavi/MegaviConfig.h"
#include "Dacs/DacsConfig.h"
#include "Scheduler/WeighingPlan.h"
#include "Calibration/Calibration.h"
#include "Modbus/ModbusConfig.h"
#include "Rs485/Rs485Config.h"
#include "Communication/Communication.h"
#include "SmsGate/SmsGateConfig.h"
#include "Multitasking/Multitasking.h"
#include "FactoryDefault.h"

//------------------------------------------------------------------------------
//  Version record
//------------------------------------------------------------------------------

TDeviceVersion Bat2Version;

const TDeviceVersion Bat2VersionDefault
 = {
   /* Class */        DEVICE_SCALE_COMPACT,
#ifdef BAT2_MODIFICATION
   /* Modification */ BAT2_MODIFICATION,
#else
   /* Modification */ 0,
#endif
   /* Software */     BAT2_SW_VERSION,
   /* Hardware */     BAT2_HW_VERSION,
#if defined(DEBUG) && defined(BAT2_SERIAL_NUMBER)
   /* SerialNumber */ BAT2_SERIAL_NUMBER
#else
   /* SerialNumber */ 0
#endif
};

const TDeviceVersion Bat2VersionBootloader
#ifndef _WIN32
   __attribute__ ((section (".bootloader_version")))
#endif
;

#if defined(_WIN32) || defined(DEBUG)
const TDeviceVersion Bat2VersionBootloader
 = {
   /* Class */        0,
#ifdef BAT2_MODIFICATION
   /* Modification */ BAT2_MODIFICATION,
#else
   /* Modification */ 0,
#endif
   /* Software */     0,
   /* Hardware */     0,
   /* SerialNumber */ 0
};
#endif

//------------------------------------------------------------------------------
//   Device
//------------------------------------------------------------------------------

TBat2Device Bat2Device;

const TBat2Device Bat2DeviceDefault = {
   /* Name */     "BAT2",
   /* Password */ {0,0,0,0}
};

//------------------------------------------------------------------------------
//  Storage definition
//------------------------------------------------------------------------------

#define CONFIG_SPARE   128

uStorageStart( ConfigDescriptor, CONFIG_COUNT)
   // !!! UPDATE EStorageItem when items changed (added or rearanged)
   uStorageItem( &Bat2Version),
   uStorageItem( &Bat2Device),
   uStorageItem( &Country),
   uStorageItem( &WeightUnits),
   uStorageItem( &DisplayConfiguration),
   uStorageItem( &WeighingConfiguration),
   uStorageItem( &Rs485Options),
   uStorageItem( &ModbusOptions),
   uStorageItem( &MegaviOptions),
   uStorageItem( &DacsOptions),
   uStorageItem( &GsmMessage),
   uStorageItem( &DataPublication),
   uStorageItem( &CellularData),
   uStorageItem( &Ethernet),
   uStorageSpare( CONFIG_SPARE)
uStorageEnd()

static UStorage Storage;

//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo ConfigRemoteLoad( void)
// Load from remote device
{
   if (!uStorageCopy(&ConfigDescriptor, FILE_CONFIG_LOCAL, FILE_CONFIG_REMOTE)){
      return NO;
   }
   return YES;
} // ConfigRemoteLoad

//------------------------------------------------------------------------------
//  Remote save
//------------------------------------------------------------------------------

TYesNo ConfigRemoteSave( void)
// Save to remote device
{
   return uStorageCopy(&ConfigDescriptor, FILE_CONFIG_REMOTE, FILE_CONFIG_LOCAL);
} // ConfigRemoteSave

#ifndef _WIN32
   #include "Flash/IFlash.h"
#endif

static void ConfigVersionDefaultValues()
// set default values to
{
TYesNo SaveVersion = NO;

#ifndef _WIN32
dword  SN;

   IFlashReadOnce(60, &SN, sizeof(SN));
   // check for serial number :
   if(Bat2Version.SerialNumber != SN){
      Bat2Version.SerialNumber = SN;
      SaveVersion = YES;
   }
#endif
   // check for modification :
//   if(Bat2Version.Modification != Bat2VersionBootloader.Modification){
//      Bat2Version.Modification = Bat2VersionBootloader.Modification;
//      SaveVersion = YES;
//   }
   // check for build :
   if (DeviceBuild(Bat2Version.Software) != DeviceBuild(Bat2VersionDefault.Software)){
      Bat2Version.Software = Bat2VersionDefault.Software;      // update build
      SaveVersion = YES;
   }
   // check for HW revision :
   if (Bat2Version.Hardware != Bat2VersionDefault.Hardware){
      Bat2Version.Hardware = Bat2VersionDefault.Hardware;
      SaveVersion = YES;
   }
   // update version info :
   if (SaveVersion){
      uStorageItemSave(&Storage, CONFIG_VERSION);
   }
}

void ConfigFactory( void)
// Set factory config on demand
{
   uStorageInit(&ConfigDescriptor, FILE_CONFIG_LOCAL);
   while (!uStorageOpen(&Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageDefault(&ConfigDescriptor);
   ConfigVersionDefaultValues();
   uStorageSave(&Storage);
   uStorageClose( &Storage);
}

//------------------------------------------------------------------------------
//  Load
//------------------------------------------------------------------------------
#if defined(_WIN32) && !defined(OPTION_SIMULATION)
void ConfigLoad(void)
{
   uStorageInit(&ConfigDescriptor, FILE_CONFIG_LOCAL);
   while (!uStorageOpen(&Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   // check for storage integrity :
   if (!uStorageCheck(&Storage)) {
      uStorageRestore(&Storage);
      if (!uStorageCheck(&Storage)) {
         uStorageClose(&Storage); // close it to restore with ConfigFactory method
         ConfigFactory();
         uStorageInit(&ConfigDescriptor, FILE_CONFIG_LOCAL);
         while (!uStorageOpen(&Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
            MultitaskingReschedule();
         }
      }
   }
   uStorageLoad(&Storage);
   uStorageClose(&Storage);
}
#else
void ConfigLoad(void)
// Load configuration
{
#ifdef STORAGE_ALLWAYS_DEFAULT
   FactoryDefault();
   return;
#else

   uStorageInit(&ConfigDescriptor, FILE_CONFIG_LOCAL);
   while (!uStorageOpen(&Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }

   // check for storage integrity :
   if (!uStorageCheck(&Storage)) {
      uStorageRestore(&Storage);
      if (!uStorageCheck(&Storage)) {
         uStorageClose( &Storage); // close it to restore with ConfigFactory method
         FactoryDefault();
         return;
      }
   }
#endif
   // load storage data :
   uStorageLoad(&Storage);

   // check device class :
   if (Bat2Version.Class != Bat2VersionDefault.Class){
   // confused device class
      uStorageDefault(&ConfigDescriptor);
      uStorageSave(&Storage);
   }
   // check for main version :
   if (DeviceVersion(Bat2Version.Software) != DeviceVersion(Bat2VersionDefault.Software)){
    // wrong software version
      uStorageDefault(&ConfigDescriptor);
      uStorageSave(&Storage);
   }

   ConfigVersionDefaultValues();
   uStorageClose(&Storage);
} // ConfigLoad

#endif
//------------------------------------------------------------------------------
//  Load
//------------------------------------------------------------------------------

void ConfigSave( void)
// Save configuration
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageSave( &Storage);
   uStorageClose( &Storage);
} // ConfigSave

//------------------------------------------------------------------------------
//   Device save
//------------------------------------------------------------------------------

void ConfigDeviceSave( void)
// Save device configuration
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_DEVICE);
   uStorageClose( &Storage);
} // ConfigDeviceSave

//------------------------------------------------------------------------------
//  Country save
//------------------------------------------------------------------------------

void ConfigCountrySave( void)
// Save country configuration
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_COUNTRY);
   uStorageClose( &Storage);
} // ConfigCountrySave

//------------------------------------------------------------------------------
//   Weight units
//------------------------------------------------------------------------------

void ConfigWeightUnitsSave( void)
// Save weight units
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_WEIGHT_UNITS);
   uStorageClose( &Storage);
} // ConfigWeightUnitsSave

//------------------------------------------------------------------------------
//  Display configuration save
//------------------------------------------------------------------------------

void ConfigDisplayConfigurationSave( void)
// Save display configuration
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_DISPLAY_CONFIGURATION);
   uStorageClose( &Storage);
} // ConfigDisplayConfigurationSave

//------------------------------------------------------------------------------
//  Weighing context save
//------------------------------------------------------------------------------

void ConfigWeighingConfigurationSave( void)
// Save weighing configuration
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_WEIGHING_CONFIGURATION);
   uStorageClose( &Storage);
} // ConfigWeighingConfigurationSave

//------------------------------------------------------------------------------
//  GSM message
//------------------------------------------------------------------------------

void ConfigGsmMessageSave( void)
// Save GSM messages configuration
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_GSM_MESSAGE);
   uStorageClose( &Storage);
} // ConfigGsmMessageSave

//------------------------------------------------------------------------------
//   MEGAVI options
//------------------------------------------------------------------------------

void ConfigMegaviOptionsSave( void)
// Save Megavi settings
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_MEGAVI_OPTIONS);
   uStorageClose( &Storage);
} // ConfigMegaviOptionsSave

//------------------------------------------------------------------------------
//   RS485 options
//------------------------------------------------------------------------------

void ConfigRs485OptionsSave( void)
// Save RS485 settings
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_RS485_OPTIONS);
   uStorageClose( &Storage);
} // ConfigRs485OptionsSave

//------------------------------------------------------------------------------
//   Modbus options
//------------------------------------------------------------------------------

void ConfigModbusOptionsSave( void)
// Save Modbus settings
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_MODBUS_OPTIONS);
   uStorageClose( &Storage);
} // ConfigModbusOptionsSave


void ConfigDacsOptionsSave( void)
// Save Dacs settings
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_DACS_OPTIONS);
   uStorageClose( &Storage);
} // ConfigDacsOptionsSave


//------------------------------------------------------------------------------
//  Data publication
//------------------------------------------------------------------------------

void ConfigDataPublicationSave( void)
// Save Data publication
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_DATA_PUBLICATION);
   uStorageClose( &Storage);
} // ConfigDataPublicationSave

//------------------------------------------------------------------------------
//  Cellular data
//------------------------------------------------------------------------------

void ConfigCellularDataSave( void)
// Save Cellular data
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_CELLULAR_DATA);
   uStorageClose( &Storage);
} // ConfigCellularDataSave

//------------------------------------------------------------------------------
//  Ethernet
//------------------------------------------------------------------------------

void ConfigEthernetSave( void)
// Save Ethernet
{
   while(!uStorageOpen( &Storage, &ConfigDescriptor, FILE_CONFIG_LOCAL)){
      MultitaskingReschedule();
   }
   uStorageItemSave( &Storage, CONFIG_ETHERNET);
   uStorageClose( &Storage);
} // ConfigEthernetaSave

//------------------------------------------------------------------------------
// Serial number
//------------------------------------------------------------------------------

dword ConfigurationSerialNumber(void)
// Returns device serial number
{
   return ENDIAN_FROM_BIG_DWORD(Bat2Version.SerialNumber);
} // ConfigurationSerialNumber

