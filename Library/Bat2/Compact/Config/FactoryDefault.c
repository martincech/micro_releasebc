//******************************************************************************
//
//   FactoryDefault.c    Factory default settings
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#include "FactoryDefault.h"
#include "Curve/CurveList.h"
#include "Curve/CorrectionList.h"
#include "Predefined/PredefinedList.h"
#include "Data/uNamedList.h"
#include "Scheduler/WeighingPlan.h"
#include "Scheduler/WeighingPlanList.h"
#include "Memory/Nvm.h"
#include "Config.h"
#include "Str.h"                          // project directory strings
#include <string.h>
#define _CorrectionCurveDataAddress( c)  ((char *)c + CORRECTION_CURVE_NAME_SIZE + 1)
#define _GrowthCurveDataAddress( c)  ((char *)c + GROWTH_CURVE_NAME_SIZE + 1)
#define _PredefinedWeighingDataAddress( c)  ((char *)c + PREDEFINED_WEIGHING_NAME_SIZE + 1)

//------------------------------------------------------------------------------
//   Factory default
//------------------------------------------------------------------------------

void FactoryDefault( void)
// Set factory default configuration
{
TPredefinedList PredefinedList;
TCurveList CurveList;
TCorrectionList CorrectionList;
TWeighingPlanList PlanList;
TPredefinedWeighing Predefined;
UDirectoryIndex Index;
UNamedListIdentifier BroilersGrowthCurveIdentifier;
UNamedListIdentifier BroilersCorrectionCurveIdentifier;

TCorrectionCurve BroilersCorrectionCurve = {
   /* Name */     "",
   /* Count */    2,
   /* _dummy */   0,
   /* Point */    {
      {14,  0},
      {30,  50}
   }
};

TGrowthCurve BroilersGrowthCurve = {
   /* Name */     "",
   /* Deleted */  NO,
   /* Count */    7,
   /* Point */    {
      {0,  0, 420},
      {1,  0, 490},
      {2,  0, 590},
      {3,  0, 750},
      {4,  0, 940},
      {5,  0, 1170},
      {6,  0, 1440}
   }
};

TPredefinedWeighing PredefinedWeighingBroilers = {
   /* Name */                  "",
   /* Flock */                 "",
   /* MenuMask */              WCMM_GROWTH_CURVE | WCMM_CORRECTION_CURVE | WCMM_DETECTION | WCMM_TARGET_WEIGHTS | WCMM_ACCEPTANCE | WCMM_STATISTICS,
   /* InitialDay */            1,
   /* CorrectionCurve */       ULIST_IDENTIFIER_INVALID,
   /* Predefined */            ULIST_IDENTIFIER_INVALID,
   /* Male */ {
      /* InitialWeight */      500,
      /* Curve */              ULIST_IDENTIFIER_INVALID,
   },
   /* Female */ {
      /* InitialWeight */      WEIGHING_CONFIGURATION_INITIAL_WEIGHT_DEFAULT,
      /* Curve */              ULIST_IDENTIFIER_INVALID,
   },
   /* TargetWeights */ {
      /* Mode */               PREDICTION_MODE_GROWTH_CURVE,
      /* Growth */             PREDICTION_GROWTH_FAST,
      /* SexDifferentiation */ SEX_DIFFERENTIATION_NO,
      /* Sex */                SEX_UNDEFINED,
      /* OnlineAdjustment */   ADJUST_TARGET_WEIGHTS_NONE
   },
   /* Detection */ {
      /* Filter */             10,
      /* StabilizationTime */  5,
      /* StabilizationRange */ 30,
      /* Step */               PLATFORM_STEP_BOTH
   },
   /* Acceptance */ {
      /* Male */ {
         /* MarginAbove */       30,
         /* MarginBelow */       30
      },
      /* Female */ {
         /* MarginAbove */       30,
         /* MarginBelow */       30
      }
   },
   /* Statistic */ {
      /* UniformityRange */  10,
      /* Short period */ 0,
      /* Short type */ STATISTIC_SHORT_TYPE_INTERVAL,
      /* spare */ 0,
      { /* Histogram */
         /* Mode  */ HISTOGRAM_MODE_RANGE,
         /* Range */ 30,
         /* Step  */ 100,
      }
   },
   /* DayStart */              0,
   /* Planning */              NO,
   /* WeighingPlan */          ULIST_IDENTIFIER_INVALID
};

TPredefinedWeighing PredefinedWeighingBreeders = {
   /* Name */                  "",
   /* Flock */                 "",
   /* MenuMask */              WCMM_GROWTH_CURVE | WCMM_CORRECTION_CURVE | WCMM_DETECTION | WCMM_TARGET_WEIGHTS | WCMM_ACCEPTANCE | WCMM_STATISTICS | WCMM_DAY_START,
   /* InitialDay */            1,
   /* CorrectionCurve */       ULIST_IDENTIFIER_INVALID,
   /* Predefined */            ULIST_IDENTIFIER_INVALID,
   /* Male */ {
      /* InitialWeight */      28000,
      /* Curve */              ULIST_IDENTIFIER_INVALID,
   },
   /* Female */ {
      /* InitialWeight */      38000,
      /* Curve */              ULIST_IDENTIFIER_INVALID,
   },
   /* TargetWeights */ {

      /* Mode */               PREDICTION_MODE_AUTOMATIC,
      /* Growth */             PREDICTION_GROWTH_SLOW,
      /* SexDifferentiation */ SEX_DIFFERENTIATION_YES,
      /* Sex */                SEX_UNDEFINED,
      /* OnlineAdjustment */   ADJUST_TARGET_WEIGHTS_NONE
   },
   /* Detection */ {
      /* Filter */             10,
      /* StabilizationTime */  5,
      /* StabilizationRange */ 30,
      /* Step */               PLATFORM_STEP_BOTH
   },
   /* Acceptance */ {
      /* Male */ {
         /* MarginAbove */       15,
         /* MarginBelow */       15
      },
      /* Female */ {
         /* MarginAbove */       15,
         /* MarginBelow */       15
      }
   },
   /* Statistic */ {
      /* UniformityRange */  10,
      /* Short period */ 0,
      /* Short type */ STATISTIC_SHORT_TYPE_INTERVAL,
      /* spare */ 0,
      { /* Histogram */
         /* Mode  */ HISTOGRAM_MODE_RANGE,
         /* Range */ 15,
         /* Step  */ 100,
      }
   },
   /* DayStart */              0,
   /* Planning */              NO,
   /* WeighingPlan */          ULIST_IDENTIFIER_INVALID
};

TPredefinedWeighing PredefinedWeighingOther = {
   /* Name */                  "",
   /* Flock */                 "",
   /* MenuMask */              WCMM_GROWTH_CURVE | WCMM_CORRECTION_CURVE | WCMM_DETECTION | WCMM_TARGET_WEIGHTS | WCMM_ACCEPTANCE | WCMM_STATISTICS | WCMM_DAY_START,
   /* InitialDay */            1,
   /* CorrectionCurve */       ULIST_IDENTIFIER_INVALID,
   /* Predefined */            ULIST_IDENTIFIER_INVALID,
   /* Male */ {
      /* InitialWeight */      500,
      /* Curve */              ULIST_IDENTIFIER_INVALID,
   },
   /* Female */ {
      /* InitialWeight */      WEIGHING_CONFIGURATION_INITIAL_WEIGHT_DEFAULT,
      /* Curve */              ULIST_IDENTIFIER_INVALID,
   },
   /* TargetWeights */ {

      /* Mode */               PREDICTION_MODE_AUTOMATIC,
      /* Growth */             PREDICTION_GROWTH_SLOW,
      /* SexDifferentiation */ SEX_DIFFERENTIATION_NO,
      /* Sex */                SEX_UNDEFINED,
      /* OnlineAdjustment */   ADJUST_TARGET_WEIGHTS_NONE
   },
   /* Detection */ {
      /* Filter */             10,
      /* StabilizationTime */  5,
      /* StabilizationRange */ 30,
      /* Step */               PLATFORM_STEP_BOTH
   },
   /* Acceptance */ {
      /* Male */ {
         /* MarginAbove */       30,
         /* MarginBelow */       30
      },
      /* Female */ {
         /* MarginAbove */       30,
         /* MarginBelow */       30
      }
   },
   /* Statistic */ {
      /* UniformityRange */  10,
      /* Short period */ 0,
      /* Short type */ STATISTIC_SHORT_TYPE_INTERVAL,
      /* spare */ 0,
      { /* Histogram */
         /* Mode  */ HISTOGRAM_MODE_RANGE,
         /* Range */ 30,
         /* Step  */ 100,
      }
   },
   /* DayStart */              0,
   /* Planning */              NO,
   /* WeighingPlan */          ULIST_IDENTIFIER_INVALID
};

TPredefinedWeighing PredefinedWeighingManual = {
   /* Name */                  "",
   /* Flock */                 "",
   /* MenuMask */              0,
   /* InitialDay */            1,
   /* CorrectionCurve */       ULIST_IDENTIFIER_INVALID,
   /* Predefined */            ULIST_IDENTIFIER_INVALID,
   /* Male */ {
      /* InitialWeight */      500,
      /* Curve */              ULIST_IDENTIFIER_INVALID,
   },
   /* Female */ {
      /* InitialWeight */      WEIGHING_CONFIGURATION_INITIAL_WEIGHT_DEFAULT,
      /* Curve */              ULIST_IDENTIFIER_INVALID,
   },
   /* TargetWeights */ {

      /* Mode */               PREDICTION_MODE_AUTOMATIC,
      /* Growth */             PREDICTION_GROWTH_SLOW,
      /* SexDifferentiation */ SEX_DIFFERENTIATION_NO,
      /* Sex */                SEX_UNDEFINED,
      /* OnlineAdjustment */   ADJUST_TARGET_WEIGHTS_NONE
   },
   /* Detection */ {
      /* Filter */             10,
      /* StabilizationTime */  5,
      /* StabilizationRange */ 30,
      /* Step */               PLATFORM_STEP_BOTH
   },
   /* Acceptance */ {
      /* Male */ {
         /* MarginAbove */       30,
         /* MarginBelow */       30
      },
      /* Female */ {
         /* MarginAbove */       30,
         /* MarginBelow */       30
      }
   },
   /* Statistic */ {
      /* UniformityRange */  10,
      /* Short period */ 0,
      /* Short type */ STATISTIC_SHORT_TYPE_INTERVAL,
      /* spare */ 0,
      { /* Histogram */
         /* Mode  */ HISTOGRAM_MODE_RANGE,
         /* Range */ 30,
         /* Step  */ 100,
      }
   },
   /* DayStart */              0,
   /* Planning */              NO,
   /* WeighingPlan */          ULIST_IDENTIFIER_INVALID
};

TWeighingPlan PlanNeverEnding = {
   /* Name */             "",
   /* SyncWithDayStart */ YES,
   /* _Dummy */           0,
   /* Days */ {
      /* Mode */          WEIGHING_DAYS_MODE_DAY_OF_WEEK,
      /* Days */          WEIGHING_DAYS_DAYS_DEFAULT,
      /* WeighingDays */  WEIGHING_DAYS_WEIGHING_DAYS_DEFAULT,
      /* SuspendedDays */ WEIGHING_DAYS_SUSPENDED_DAYS_DEFAULT,
      /* StartDay */      WEIGHING_DAYS_START_DAY_DEFAULT,
   },
   /* Times */ {
      {TIME_RANGE_FROM_DEFAULT, TIME_RANGE_TO_DEFAULT}
   },
   /* TimesCount*/        1
};

TWeighingPlan PlanEverySecondDay = {
   /* Name */             "",
   /* SyncWithDayStart */ YES,
   /* _Dummy */           0,
   /* Days */ {
      /* Mode */          WEIGHING_DAYS_MODE_DAY_NUMBER,
      /* Days */          0x55,
      /* WeighingDays */  1,
      /* SuspendedDays */ 1,
      /* StartDay */      1,
   },
   /* Times */ {
      {TIME_RANGE_FROM_DEFAULT, TIME_RANGE_TO_DEFAULT}
   },
   /* TimesCount*/        1
};

TWeighingPlan PlanNoNight = {
   /* Name */             "",
   /* SyncWithDayStart */ YES,
   /* _Dummy */           0,
   /* Days */ {
      /* Mode */          WEIGHING_DAYS_MODE_DAY_OF_WEEK,
      /* Days */          WEIGHING_DAYS_DAYS_DEFAULT,
      /* WeighingDays */  WEIGHING_DAYS_WEIGHING_DAYS_DEFAULT,
      /* SuspendedDays */ WEIGHING_DAYS_SUSPENDED_DAYS_DEFAULT,
      /* StartDay */      WEIGHING_DAYS_START_DAY_DEFAULT,
   },
   /* Times */ {
      {6*3600, 20*3600} //from 6:00 to 20:00
   },
   /* TimesCount*/        1
};

   // Format
#ifdef STORAGE_ALLWAYS_DEFAULT
   NvmFormat();
#endif

   // Growth curve
   CurveListInit();   CurveListInit();
   CurveListOpen( &CurveList);
   // delete all
   while(uNamedListCount( &CurveList) != 0){
      uNamedListDelete(&CurveList, 0);
   }
   // add factory defaults
   Index = uNamedListAdd( &CurveList, StrGet(STR_BROILERS_1_WEEK), _GrowthCurveDataAddress( &BroilersGrowthCurve));
   BroilersGrowthCurveIdentifier = uDirectoryItemIdentifier( ( UDirectory *) uNamedListDirectory(&CurveList), Index);
   CurveListClose( &CurveList);

   // Correction curve
   CorrectionListInit();
   CorrectionListOpen( &CorrectionList);
   // delete all
   while(uNamedListCount( &CorrectionList) != 0){
      uNamedListDelete(&CorrectionList, 0);
   }
   // add factory defaults
   Index = uNamedListAdd( &CorrectionList, StrGet(STR_BROILERS), _CorrectionCurveDataAddress( &BroilersCorrectionCurve));
   BroilersCorrectionCurveIdentifier = uDirectoryItemIdentifier( ( UDirectory *) uNamedListDirectory(&CorrectionList), Index);
   CorrectionListClose( &CorrectionList);

   // Predefined weighing list
   PredefinedListInit();
   PredefinedListOpen( &PredefinedList);
   // delete all
   while(uNamedListCount( &PredefinedList) != 0){
      uNamedListDelete(&PredefinedList, 0);
   }
   memcpy( &Predefined, &PredefinedWeighingBroilers, sizeof(Predefined)); // copy to RAM, need to alter curves
   Predefined.Male.GrowthCurve = BroilersGrowthCurveIdentifier;
   Predefined.CorrectionCurve = BroilersCorrectionCurveIdentifier;
   uNamedListAdd( &PredefinedList, StrGet(STR_BROILERS), _PredefinedWeighingDataAddress( &Predefined));
   uNamedListAdd( &PredefinedList, StrGet(STR_BREEDERS), _PredefinedWeighingDataAddress( &PredefinedWeighingBreeders));
   uNamedListAdd( &PredefinedList, StrGet(STR_OTHER), _PredefinedWeighingDataAddress( &PredefinedWeighingOther));
   uNamedListAdd( &PredefinedList, StrGet(STR_MANUAL), _PredefinedWeighingDataAddress( &PredefinedWeighingManual));
   PredefinedListClose( &PredefinedList);

   // Weighing plans
   WeighingPlanListInit();
   WeighingPlanListOpen(&PlanList);
   // delete all
   while(WeighingPlanListCount( &PlanList) != 0){
      WeighingPlanListDelete(&PlanList, 0);
   }
   strcpy(PlanNeverEnding.Name, StrGet(STR_NEVER_ENDING));
   WeighingPlanListAdd( &PlanList, &PlanNeverEnding);

   strcpy(PlanEverySecondDay.Name, StrGet(STR_EVERY_SECOND_DAY));
   WeighingPlanListAdd( &PlanList, &PlanEverySecondDay);

   strcpy(PlanNoNight.Name, StrGet(STR_NO_NIGHTS));
   WeighingPlanListAdd( &PlanList, &PlanNoNight);
   WeighingPlanListClose(&PlanList);

   // Config default
   ConfigFactory();
   ContextFactory();
} // FactoryDefault
