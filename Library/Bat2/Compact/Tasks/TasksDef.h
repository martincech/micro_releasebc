//******************************************************************************
//
//   TasksDef.h     Bat2 tasks def
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __TasksDef_H__
   #define __TasksDef_H__

#include "Multitasking/Multitasking.h"

#define TASKS_COUNT     9

extern TTask *Tasks[TASKS_COUNT];

int GuiTaskScheduler();

#endif
