//*****************************************************************************
//
//    Bitmap.h - project bitmaps
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Bitmap_H__
   #define __Bitmap_H__

#ifndef __Graphic_H__
   #include "Graphic/Graphic.h"
#endif

//------------------------------------------------------------------------------
//  Off screen
//------------------------------------------------------------------------------

extern const TBitmap BmpOff;

//------------------------------------------------------------------------------
//   Status Line
//------------------------------------------------------------------------------

extern const TBitmap BmpBattery;
extern const TBitmap BmpPC;
extern const TBitmap BmpCharger;
extern const TBitmap BmpUsbStick;

extern const TBitmap BmpAm;
extern const TBitmap BmpPm;

//------------------------------------------------------------------------------
//  Big accu
//------------------------------------------------------------------------------

extern const TBitmap BmpAccu;

//------------------------------------------------------------------------------
//  Screen
//------------------------------------------------------------------------------

extern const TBitmap BmpWeight;
extern const TBitmap BmpSigma;
extern const TBitmap BmpCount;
extern const TBitmap BmpSexFemale;
extern const TBitmap BmpSexMale;
extern const TBitmap BmpUpArrow;
extern const TBitmap BmpDownArrow;

//------------------------------------------------------------------------------
//  Statistics
//------------------------------------------------------------------------------

extern const TBitmap BmpStatisticsCount;
extern const TBitmap BmpStatisticsWeight;
extern const TBitmap BmpStatisticsSigma;
extern const TBitmap BmpStatisticsCv;
extern const TBitmap BmpStatisticsUni;

extern const TBitmap BmpStatisticsTotal;
extern const TBitmap BmpStatisticsMale;
extern const TBitmap BmpStatisticsFemale;

extern const TBitmap BmpStatisticsTarget;
extern const TBitmap BmpStatisticsLastAverage;
extern const TBitmap BmpStatisticsGain;

//------------------------------------------------------------------------------
//  Histogram
//------------------------------------------------------------------------------

extern const TBitmap BmpHistogramCount;
extern const TBitmap BmpHistogramWeight;
extern const TBitmap BmpHistogramCursor;

//------------------------------------------------------------------------------
//  Layout
//------------------------------------------------------------------------------

extern const TBitmap BmpFileBlack;

extern const TBitmap BmpButtonOk;
extern const TBitmap BmpButtonCancel;
extern const TBitmap BmpButtonUp;
extern const TBitmap BmpButtonDown;
extern const TBitmap BmpButtonLeft;
extern const TBitmap BmpButtonRight;
extern const TBitmap BmpButtonLeftRight;

extern const TBitmap BmpIconTotal;
extern const TBitmap BmpIconMale;
extern const TBitmap BmpIconFemale;

extern const TBitmap BmpIconLight;
extern const TBitmap BmpIconHeavy;

extern const TBitmap BmpCheckboxChecked;
extern const TBitmap BmpCheckboxUnchecked;

//------------------------------------------------------------------------------
//  Edit
//------------------------------------------------------------------------------

extern const TBitmap BmpEditUp;
extern const TBitmap BmpEditDown;

//------------------------------------------------------------------------------
//  Wait
//------------------------------------------------------------------------------

extern const TBitmap BmpWait;

//------------------------------------------------------------------------------
//  GSM
//------------------------------------------------------------------------------

extern const TBitmap BmpClock;
extern const TBitmap BmpPhone;
extern const TBitmap BmpFlash;

extern const TBitmap BmpGsmOff;
extern const TBitmap BmpGsmPin;
extern const TBitmap BmpGsmNoSignal;
extern const TBitmap BmpGsmOk;
extern const TBitmap BmpGsmSend;
extern const TBitmap BmpGsmReceive;

//------------------------------------------------------------------------------
//  Graph
//------------------------------------------------------------------------------

extern const TBitmap BmpGraphCursor;

//------------------------------------------------------------------------------
// Main screen icons
//------------------------------------------------------------------------------

extern const TBitmap BmpStop;
extern const TBitmap BmpPause;
extern const TBitmap BmpStatisticsCO2;
extern const TBitmap BmpStatisticsHumidity;
extern const TBitmap BmpStatisticsTemperature;

#endif
