//******************************************************************************
//
//   InternetPublication.c
//   Version 1.0                 (c) VEIT Electronics
//
//******************************************************************************
#include "InternetPublication.h"
#include "CommunicationDef.h"
#include "http/client/HTTPClient.h"
#include "Console/conio.h"
#include "Str.h"                  // Strings
#include "Ethernet/EthernetLibrary.h"
#include "Device/VersionDef.h"
#include <string.h>
#include <stdio.h>

#define HTTP    "http://"

#define HTTP_CLIENT_ERROR_NO_PERMISSON 12345
#define HTTP_CLIENT_ERROR_INVALID_DATA HTTP_CLIENT_ERROR_NO_PERMISSON + 1
static INT32                   lastError;

static void ConcatUrl(char *Buffer, int argCount, ...)
{
va_list vl;
char *val;
int i;

   Buffer[0] = '\0';
   va_start(vl, argCount);
   val = va_arg(vl, char *);
   if(argCount >= 1){
      bprintf(Buffer, "%s", val);
   }
   for(i = 0; i < argCount - 1; i++)
   {
      val = va_arg(vl, char *);
      bprintf(Buffer, "%s/%s", Buffer, val);
   }
   va_end(vl);
}

static void FlushBody(HTTP_SESSION_HANDLE pHTTP)
{
const int bufSize = 10;
char buffer[bufSize];
#ifdef _HTTP_DEBUGGING_
HTTPDebug("FlushBody", NULL, 0, "");
#endif
UINT32 nSize;
   do
   {
      // Set the size of our buffer
      nSize = bufSize;

      // Get the data
      lastError = HTTPClientReadData(pHTTP,buffer,nSize,0,&nSize);
      // Print out the results
#ifdef __UDEBUG__
      printf("%s",buffer);
#endif
      if(lastError == HTTP_CLIENT_EOS){break;}
   }while((lastError == HTTP_CLIENT_SUCCESS) && nSize > 0);
}

static void SetLastErrorFromStatusCode(HTTP_CLIENT httpInfo){
   switch(httpInfo.HTTPStatusCode){
   case HTTP_STATUS_UNAUTHORIZED:
      lastError = HTTP_CLIENT_ERROR_AUTH_HOST;
      break;
   case HTTP_STATUS_FORBIDDEN:
      lastError = HTTP_CLIENT_ERROR_NO_PERMISSON;
      break;
   case HTTP_STATUS_NOT_ACCEPTABLE:
      lastError = HTTP_CLIENT_ERROR_INVALID_DATA;
      break;
   default:
      lastError = HTTP_CLIENT_ERROR_BAD_URL;
      break;
   }
}

static TYesNo IsInterfaceOk()
{
#ifdef CONNECTION_INTERNET_TYPE_GSM
   if (BAT2_HAS_GSM_MODULE(Bat2Version.Modification)){
   // try to publish through GSM module with gsm module TCP/IP stack
      if( !_GsmEnabled()){
        return NO;
      }
#else
   if(BAT2_HAS_ETHERNET_MODULE(Bat2Version.Modification)){
   // try to publish through ethernet/wifi with fnet TCP/IP stack
      if( !EthernetIsLink()){
        return NO;
      }
      return EthernetLibraryIpAssigned();
#endif
   }
}

#ifdef _HTTP_DEBUGGING_
VOID HTTPDebug(const CHAR* FunctionName,const CHAR *DebugDump,UINT32 iLength,CHAR *DebugDescription,...) // Requested operation
{

    va_list            pArgp;
    char               szBuffer[2048];

    memset(szBuffer,0,2048);
    va_start(pArgp, DebugDescription);
    vsprintf((char*)szBuffer, DebugDescription, pArgp); //Copy Data To The Buffer
    va_end(pArgp);

    printf("%s %s %s\n", FunctionName,DebugDump,szBuffer);
    fflush(stdout);
}
#endif

TYesNo InternetPublicationCheckConnection(InternetPublicationSettings settings)
{
HTTP_SESSION_HANDLE     pHTTP;
HTTP_CLIENT             httpInfo;

const int bufSize = HTTP_CLIENT_MAX_URL_LENGTH;
char buffer[bufSize];

if(!IsInterfaceOk()){
      lastError = HTTP_CLIENT_ERROR_SOCKET_CONNECT;
      return NO;
   }
   buffer[0]='\0';
   if(strlen(settings.url) == 0){
      lastError = HTTP_CLIENT_ERROR_BAD_URL;
      return NO;
   }
   if(strlen(settings.login) == 0 || strlen(settings.password) == 0){
      lastError = HTTP_CLIENT_ERROR_BAD_AUTH;
      return NO;
   }
   char version[10];
   bprintf(version, "%d", Bat2Version.Software);
   if(strncmp(settings.url, HTTP, strlen(HTTP)) == 0){
      ConcatUrl(buffer, 4, settings.url, BASE_ADDRESS, METHOD_ADDRESS, version);
   }else{
      ConcatUrl(buffer, 5, HTTP, settings.url, BASE_ADDRESS, METHOD_ADDRESS, version);
   }
// TODO allow port speification (Check address for port)
#ifdef __UDEBUG__
   pHTTP = HTTPClientOpenRequest(HTTP_CLIENT_FLAG_URLANDPORT);
#ifdef _HTTP_DEBUGGING_
   HTTPClientSetDebugHook(pHTTP,&HTTPDebug);
#endif
#else
   pHTTP = HTTPClientOpenRequest(0);
#endif
   do{
      // Set the Verb
      if((lastError = HTTPClientSetVerb(pHTTP,VerbGet)) != HTTP_CLIENT_SUCCESS)
      {
         break;
      }
      // Set credentials
      if((lastError = HTTPClientSetAuth(pHTTP, AuthSchemaBasic, 0)) != HTTP_CLIENT_SUCCESS)
      {
         break;
      }
      if((lastError = HTTPClientSetCredentials(pHTTP, settings.login, settings.password)) != HTTP_CLIENT_SUCCESS)
      {
         break;
      }
      // send request for server & login validation
      if((lastError = HTTPClientSendRequest(pHTTP,buffer,NULL,0,FALSE,0, 0)) != HTTP_CLIENT_SUCCESS)
      {
         break;
      }
      // Retrieve the the headers and analyze them
      if((lastError = HTTPClientRecvResponse(pHTTP,3)) != HTTP_CLIENT_SUCCESS)
      {
         break;
      }
      HTTPClientGetInfo(pHTTP, &httpInfo);
      // throw all possible data
      FlushBody(pHTTP);
      if(httpInfo.HTTPStatusCode != HTTP_STATUS_OK){
         SetLastErrorFromStatusCode(httpInfo);
         break;
      }
      HTTPClientCloseRequest(&pHTTP);
      return YES;
   }while(0);

   HTTPClientCloseRequest(&pHTTP);
   return NO;
}

TYesNo InternetPublicationPublish(InternetPublicationSettings settings, void *data, int dataSize)
{
HTTP_SESSION_HANDLE     pHTTP;
HTTP_CLIENT             httpInfo;
UINT32                  nSize,nTotal = 0;
const int bufSize = HTTP_CLIENT_MAX_URL_LENGTH;
char buffer[bufSize];

   if(!IsInterfaceOk()){
      lastError = HTTP_CLIENT_ERROR_SOCKET_CONNECT;
      return NO;
   }
   buffer[0]='\0';
   if(strlen(settings.url) == 0){
      lastError = HTTP_CLIENT_ERROR_BAD_URL;
      return NO;
   }
   if(strlen(settings.login) == 0 || strlen(settings.password) == 0){
      lastError = HTTP_CLIENT_ERROR_BAD_AUTH;
      return NO;
   }
   char version[10];
   bprintf(version, "%d", Bat2Version.Software);
   if(strncmp(settings.url, HTTP, strlen(HTTP)) == 0){
      ConcatUrl(buffer, 4, settings.url, BASE_ADDRESS, METHOD_ADDRESS, version);
   }else{
      ConcatUrl(buffer, 5, HTTP, settings.url, BASE_ADDRESS, METHOD_ADDRESS, version);
   }

#ifdef __UDEBUG__
   pHTTP = HTTPClientOpenRequest(HTTP_CLIENT_FLAG_URLANDPORT);
#else
   pHTTP = HTTPClientOpenRequest(0);
#endif
   do{
      // Set the Verb
      if((lastError = HTTPClientSetVerb(pHTTP, VerbPost)) != HTTP_CLIENT_SUCCESS)
      {
         break;
      }
      // Set credentials
      if((lastError = HTTPClientSetAuth(pHTTP, AuthSchemaBasic, 0)) != HTTP_CLIENT_SUCCESS)
      {
         break;
      }
      if((lastError = HTTPClientSetCredentials(pHTTP, settings.login, settings.password)) != HTTP_CLIENT_SUCCESS)
      {
         break;
      }
      // send request with all data
      if((lastError = HTTPClientSendRequest(pHTTP,buffer, data, dataSize, TRUE, 0, 0)) != HTTP_CLIENT_SUCCESS)
      {
         break;
      }
      // Retrieve the headers and analyze them
      if((lastError = HTTPClientRecvResponse(pHTTP,3)) != HTTP_CLIENT_SUCCESS)
      {
         break;
      }
      HTTPClientGetInfo(pHTTP, &httpInfo);
      // throw all possible data
      FlushBody(pHTTP);
      if(httpInfo.HTTPStatusCode != HTTP_STATUS_OK){
         SetLastErrorFromStatusCode(httpInfo);
         break;
      }
      HTTPClientCloseRequest(&pHTTP);
      return YES;
   }while(0);

   HTTPClientCloseRequest(&pHTTP);
   return NO;
}

void InternetPublicationGetLastError(TUniStr *text1, TUniStr *text2)
{
   *text1 = "";
   *text2 = "";
   switch(lastError)
   {
   case HTTP_CLIENT_SUCCESS:
      break;
   case HTTP_CLIENT_ERROR_SOCKET_RESOLVE:
      *text1 = STR_INVALID_HOSTNAME;
      break;
   case HTTP_CLIENT_ERROR_SOCKET_TIME_OUT:
   case HTTP_CLIENT_ERROR_SOCKET_CONNECT:
   case HTTP_CLIENT_ERROR_AUTH_MISMATCH:
   case HTTP_CLIENT_ERROR_SOCKET_BIND:
      *text1 = STR_CONNECTIONG_ERROR;
      *text2 = STR_TO_REMOTE_SERVER;
      break;
   case HTTP_CLIENT_ERROR_SOCKET_RECV:
   case HTTP_CLIENT_ERROR_SOCKET_SEND:
   case HTTP_CLIENT_ERROR_HEADER_RECV:
   case HTTP_CLIENT_ERROR_HEADER_NOT_FOUND:
   case HTTP_CLIENT_ERROR_HEADER_BIG_CLUE:
   case HTTP_CLIENT_ERROR_HEADER_NO_LENGTH :
   case HTTP_CLIENT_ERROR_CHUNK_TOO_BIG:
   case HTTP_CLIENT_EOS :
      *text1 = STR_COMMUNICATING_ERROR;
      *text2 = STR_WITH_REMOTE_SERVER;
      break;
   case HTTP_CLIENT_ERROR_AUTH_HOST:
   case HTTP_CLIENT_ERROR_AUTH_PROXY:
   case HTTP_CLIENT_ERROR_BAD_AUTH:
   case HTTP_CLIENT_ERROR_NO_DIGEST_TOKEN:
   case HTTP_CLIENT_ERROR_NO_DIGEST_ALG :
   case HTTP_CLIENT_ERROR_TLS_NEGO	:
      *text1 = STR_AUTHENTICATION_ERROR;
      *text2 = STR_CHECK_CREDENTIALS;
      break;
   case HTTP_CLIENT_ERROR_BAD_URL:
   case HTTP_CLIENT_ERROR_BAD_VERB:
   case HTTP_CLIENT_ERROR_BAD_HEADER:
      *text1 = STR_INVALID_URL;
      break;
   case HTTP_CLIENT_ERROR_NO_PERMISSON:
      *text1 = STR_UNAUTHORIZED_ACCESS;
      *text2 = STR_INSUFICIENT_PRIVILEGES;
      break;
   default:
      *text2 = STR_UNKNOWN_ERROR;
      break;
   }
}

