//******************************************************************************
//
//   MenuUsb.h       Menu USB
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuUsb_H__
   #define __MenuUsb_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuUsb( void);
// Menu USB

#endif
