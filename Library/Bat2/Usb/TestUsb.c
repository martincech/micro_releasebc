//*****************************************************************************
//
//    TestUsb.c       Usb test
//    Version 1.0     (c) Veit Electronics
//
//*****************************************************************************

#include "Unity/unity_fixture.h"
#include "Usb/Usb.h"
#include "MockPower.h"
#include "Usb/MockUsbCharger.h"
#include "Usb/MockUsbModule.h"
#include "Usb/MockUsbDevice.h"
#include "Usb/MockUsbHardware.h"
#include "IAdc/MockIAdc.h"

#define USB_ATTACHED_POWER 100 // mA

int Current;
static void CurrentSetter( int _Current) {
   Current = _Current;
}

TEST_GROUP( Usb);

TEST_SETUP( Usb)
{
   MockIAdc_Init();
   MockPower_Init();
   MockUsbCharger_Init();
   MockUsbModule_Init();
   MockUsbDevice_Init();
   MockUsbHardware_Init();
   PowerUsbCurrentLimitSet_StubWithCallback(&CurrentSetter);
}

TEST_TEAR_DOWN( Usb)
{
   MockIAdc_Verify();
   MockPower_Verify();
   MockUsbCharger_Verify();
   MockUsbModule_Verify();
   MockUsbDevice_Verify();
   MockUsbHardware_Verify();
}

TEST( Usb, Init)
{
   printf("--- Init ---\n");
   UsbModuleInit_Expect();
   UsbPortInit_Expect();
   UsbInit();
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}

TEST( Usb, UsbIsIdle)
{
   printf("--- Usb idle ---\n");
   UsbDevicePowerOn_ExpectAndReturn(NO);
   UsbDevicePowerOnChanged_ExpectAndReturn(NO);
   UsbDevicePowerOnChangedClear_Expect();
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}

TEST( Usb, UsbDisconnected)
{
   printf("--- Usb disconnected ---\n");
   UsbDevicePowerOn_ExpectAndReturn(NO);
   UsbDevicePowerOnChanged_ExpectAndReturn(YES);
   UsbDevicePowerOnChangedClear_Expect();
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}

TEST( Usb, UsbDisconnectedWhenPreviouslyAttachedOrConnected)
{
   printf("--- Usb disconnected when previously attached or connected ---\n");
   UsbDevicePowerOn_ExpectAndReturn(NO);
   UsbDevicePowerOnChanged_ExpectAndReturn(YES);
   UsbDevicePowerOnChangedClear_Expect();
   UsbDeviceDeinit_Expect();
   UsbModuleSwitchOff_Expect();
   UsbSwitchDisable_Expect();
   PowerUsbHostOff_Ignore(); // that's not mandatory
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}

TEST( Usb, UsbAttached)
{
   printf("--- Usb attached ---\n");
   UsbDevicePowerOn_ExpectAndReturn(YES);
   UsbDevicePowerOnChanged_ExpectAndReturn(YES);
   UsbDevicePowerOnChangedClear_Expect();
   UsbModuleSwitchOn_Expect();
   UsbSwitchEnable_Expect();
   UsbSwitchSelectDevice_Expect();
   UsbModulePullDownEnable_Expect(NO);
   IAdcInit_Expect();
   IAdcRead_IgnoreAndReturn(0);
   UsbChargerDetectionStart_Expect();
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}

TEST( Usb, ChargerDetectionInProgress)
{
   printf("--- Usb charger detection in progress ---\n");
   UsbDevicePowerOn_ExpectAndReturn(YES);
   UsbDevicePowerOnChanged_ExpectAndReturn(NO);
   UsbDevicePowerOnChangedClear_Expect();
   UsbChargerDetectionCompleted_ExpectAndReturn(NO);
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}

TEST( Usb, ChargerDetectionComplete)
{
   printf("--- Usb charger detection complete ---\n");
   UsbDevicePowerOn_ExpectAndReturn(YES);
   UsbDevicePowerOnChanged_ExpectAndReturn(NO);
   UsbDevicePowerOnChangedClear_Expect();
   UsbChargerDetectionCompleted_ExpectAndReturn(YES);
   UsbChargerCharger_ExpectAndReturn(CHARGER_STANDARD_HOST);
   UsbDeviceInit_Expect();
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}

TEST( Usb, UsbWaitForEnumeration)
{
   printf("--- Waiting for enumeration ---\n");
   UsbDevicePowerOn_ExpectAndReturn(YES);
   UsbDevicePowerOnChanged_ExpectAndReturn(NO);
   UsbDevicePowerOnChangedClear_Expect();
   UsbDeviceIsConnected_ExpectAndReturn(NO);
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}

TEST( Usb, UsbEnumerated)
{
   printf("--- Enumerated ---\n");
   UsbDevicePowerOn_ExpectAndReturn(YES);
   UsbDevicePowerOnChanged_ExpectAndReturn(NO);
   UsbDevicePowerOnChangedClear_Expect();
   UsbDeviceIsConnected_ExpectAndReturn(YES);
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_POWER, Current);
}

TEST( Usb, UsbIdleEnumerated)
{
   printf("--- Idle enumerated ---\n");
   UsbDevicePowerOn_ExpectAndReturn(YES);
   UsbDevicePowerOnChanged_ExpectAndReturn(NO);
   UsbDevicePowerOnChangedClear_Expect();
   UsbDeviceIsConnected_ExpectAndReturn(YES);
   UsbDeviceExecute_Expect();
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_POWER, Current);
}

TEST( Usb, UsbEnumeratedToAttached)
{
   printf("--- Enumerated to attached ---\n");
   UsbDevicePowerOn_ExpectAndReturn(YES);
   UsbDevicePowerOnChanged_ExpectAndReturn(NO);
   UsbDevicePowerOnChangedClear_Expect();
   UsbDeviceIsConnected_ExpectAndReturn(NO);
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}


TEST( Usb, UsbHostEnabledWhenDisconnected)
{
   printf("--- Host enabled when disconnected ---\n");

   UsbModuleSwitchOn_Expect();
   UsbModulePullDownEnable_Expect(YES);
   UsbSwitchSelectHost_Expect();
   UsbSwitchEnable_Expect();
   PowerUsbHostOn_Expect();
   TEST_ASSERT_TRUE(UsbHostEnable(YES));


   UsbDevicePowerOn_ExpectAndReturn(NO);
   UsbDevicePowerOnChanged_ExpectAndReturn(NO);
   UsbDevicePowerOnChangedClear_Expect();
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}

TEST( Usb, UsbHostEnabledWhenAttachedOrConnected)
{
   printf("--- Host enabled when attached or connected ---\n");

   UsbDeviceDeinit_Expect();
   UsbModuleSwitchOff_Expect();
   UsbSwitchDisable_Expect();
   PowerUsbHostOff_Ignore();
   UsbModuleSwitchOn_Expect();
   UsbModulePullDownEnable_Expect(YES);
   UsbSwitchSelectHost_Expect();
   UsbSwitchEnable_Expect();
   PowerUsbHostOn_Expect();
   TEST_ASSERT_TRUE(UsbHostEnable(YES));


   UsbDevicePowerOn_ExpectAndReturn(NO);
   UsbDevicePowerOnChanged_ExpectAndReturn(NO);
   UsbDevicePowerOnChangedClear_Expect();
   UsbExecute();
}

TEST( Usb, UsbHostDisabled)
{
   printf("--- Host disabled ---\n");

   UsbModuleSwitchOff_Expect();
   UsbSwitchDisable_Expect();
   PowerUsbHostOff_Expect();
   TEST_ASSERT_TRUE(UsbHostEnable(NO));
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}

TEST( Usb, UsbAttachedWhenHostEnable)
{
   printf("--- Attached when host enabled ---\n");

   UsbDevicePowerOn_ExpectAndReturn(YES);
   UsbDevicePowerOnChanged_ExpectAndReturn(YES);
   UsbDevicePowerOnChangedClear_Expect();
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_ATTACHED_POWER, Current);
}

TEST( Usb, UsbIsIdleDuringHostEnabledAndUsbConnected)
{
   printf("--- Idle during host enabled and usb connected ---\n");

   UsbDevicePowerOn_ExpectAndReturn(YES);
   UsbDevicePowerOnChanged_ExpectAndReturn(NO);
   UsbDevicePowerOnChangedClear_Expect();
   UsbExecute();
   TEST_ASSERT_EQUAL(USB_POWER, Current);
}

//******************************************************************************

TEST_GROUP_RUNNER( Usb)
{
   RUN_TEST_CASE( Usb, Init);
   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbAttached);
   RUN_TEST_CASE( Usb, UsbDisconnected);

   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbAttached);
   RUN_TEST_CASE( Usb, ChargerDetectionInProgress);
   RUN_TEST_CASE( Usb, ChargerDetectionInProgress);
   RUN_TEST_CASE( Usb, UsbDisconnected);

   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbAttached);
   RUN_TEST_CASE( Usb, ChargerDetectionInProgress);
   RUN_TEST_CASE( Usb, ChargerDetectionInProgress);
   RUN_TEST_CASE( Usb, ChargerDetectionComplete);
   RUN_TEST_CASE( Usb, UsbDisconnectedWhenPreviouslyAttachedOrConnected);

   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbAttached);
   RUN_TEST_CASE( Usb, ChargerDetectionInProgress);
   RUN_TEST_CASE( Usb, ChargerDetectionInProgress);
   RUN_TEST_CASE( Usb, ChargerDetectionComplete);
   RUN_TEST_CASE( Usb, UsbWaitForEnumeration);
   RUN_TEST_CASE( Usb, UsbDisconnectedWhenPreviouslyAttachedOrConnected);

   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbAttached);
   RUN_TEST_CASE( Usb, ChargerDetectionInProgress);
   RUN_TEST_CASE( Usb, ChargerDetectionComplete);
   RUN_TEST_CASE( Usb, UsbWaitForEnumeration);
   RUN_TEST_CASE( Usb, UsbWaitForEnumeration);
   RUN_TEST_CASE( Usb, UsbDisconnectedWhenPreviouslyAttachedOrConnected);

   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbAttached);
   RUN_TEST_CASE( Usb, ChargerDetectionInProgress);
   RUN_TEST_CASE( Usb, ChargerDetectionComplete);
   RUN_TEST_CASE( Usb, UsbWaitForEnumeration);
   RUN_TEST_CASE( Usb, UsbWaitForEnumeration);
   RUN_TEST_CASE( Usb, UsbEnumerated);
   RUN_TEST_CASE( Usb, UsbIdleEnumerated);
   RUN_TEST_CASE( Usb, UsbIdleEnumerated);
   RUN_TEST_CASE( Usb, UsbIdleEnumerated);
   RUN_TEST_CASE( Usb, UsbIdleEnumerated);
   RUN_TEST_CASE( Usb, UsbEnumeratedToAttached);
   RUN_TEST_CASE( Usb, UsbWaitForEnumeration);
   RUN_TEST_CASE( Usb, UsbWaitForEnumeration);
   RUN_TEST_CASE( Usb, UsbEnumerated);
   RUN_TEST_CASE( Usb, UsbIdleEnumerated);
   RUN_TEST_CASE( Usb, UsbIdleEnumerated);
   RUN_TEST_CASE( Usb, UsbDisconnectedWhenPreviouslyAttachedOrConnected);

   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbHostEnabledWhenDisconnected);
   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbAttachedWhenHostEnable);
   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbDisconnected);
   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbHostDisabled);

   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbAttached);
   RUN_TEST_CASE( Usb, ChargerDetectionInProgress);
   RUN_TEST_CASE( Usb, ChargerDetectionComplete);
   RUN_TEST_CASE( Usb, UsbHostEnabledWhenAttachedOrConnected);
   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbDisconnected);
   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbHostDisabled);

   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbAttached);
   RUN_TEST_CASE( Usb, ChargerDetectionInProgress);
   RUN_TEST_CASE( Usb, ChargerDetectionComplete);
   RUN_TEST_CASE( Usb, UsbWaitForEnumeration);
   RUN_TEST_CASE( Usb, UsbWaitForEnumeration);
   RUN_TEST_CASE( Usb, UsbEnumerated);
   RUN_TEST_CASE( Usb, UsbIdleEnumerated);
   RUN_TEST_CASE( Usb, UsbHostEnabledWhenAttachedOrConnected);
   RUN_TEST_CASE( Usb, UsbIsIdleDuringHostEnabledAndUsbConnected);
   RUN_TEST_CASE( Usb, UsbDisconnected);
   RUN_TEST_CASE( Usb, UsbIsIdle);
   RUN_TEST_CASE( Usb, UsbHostDisabled);
}

#include "Test/TestRunner.h"

void TestRunner( void) {
   RUN_TEST_GROUP( Usb);
}
