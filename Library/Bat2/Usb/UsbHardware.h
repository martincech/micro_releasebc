//******************************************************************************
//
//   UsbDevice.h    Usb Device
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __UsbHardware_H__
   #define __UsbHardware_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

void UsbPortInit( void);          

void UsbSwitchEnable( void);
void UsbSwitchDisable( void);

TYesNo UsbDevicePowerOn( void);
TYesNo UsbDevicePowerOnChanged( void);
void UsbDevicePowerOnChangedClear( void);

void UsbSwitchSelectDevice( void);
void UsbSwitchSelectHost( void);

void PowerUsbHostOn( void);
void PowerUsbHostOff( void);

#endif