//*****************************************************************************
//
//    Megavi.c       Serial port Megavi communication dummy functions for simulation
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "Megavi/Megavi.h"
#include "Debug/uDebug.h"
#include "Megavi/MegaviConfig.h"  // Messaging configuration
#include "Hardware.h"

#include <string.h>
#include <ctype.h>

#define MEGAVI_READ_PERIOD   100000

static byte _MegaviRead;
#define _MegaviReadSet()   _MegaviRead = MEGAVI_READ_PERIOD
#define _MegaviReadTick()  (--_MegaviRead == 0)

//------------------------------------------------------------------------------
//  Megavi module definitions
//------------------------------------------------------------------------------
static TMegaviStatus  _MegaviStatus;
static TMegaviCommand _MegaviRxBuffer;
static TMegaviReply   _MegaviTxBuffer;
//------------------------------------------------------------------------------
//   Initialize
//------------------------------------------------------------------------------

void MegaviInit( int uartPort)
// Initialize
{
   _MegaviReadSet();
   TRACE_INIT();
   TRACE( "MEGAVI : Init");
}// MegaviInit

//------------------------------------------------------------------------------
//   Deinitialize
//------------------------------------------------------------------------------
void MegaviDeinit( void)
// Deinitialize
{
   MegaviListenStop();
   TRACE(" MEGAVI : Deinit");
}

//------------------------------------------------------------------------------
//   Baud rate
//------------------------------------------------------------------------------

void MegaviBaudRateSet( int BaudRate)
// Set Megavi <BaudRate>
{
   TRACE( "MEGAVI : Baud rate set.");
}// MegaviBaudRateSet


//------------------------------------------------------------------------------
//   Status
//------------------------------------------------------------------------------

TMegaviStatus MegaviStatus( void)
// Check for last operation status
{
static byte type = MEGAVI_COMMAND_MALE_DATA_GET;

  // if( !_MegaviReadTick()){
      return( _MegaviStatus);
  // }
   _MegaviReadSet();
   switch( type){
      case MEGAVI_COMMAND_VERSION :
      case MEGAVI_COMMAND_MALE_DATA_GET :
      case MEGAVI_COMMAND_FEMALE_DATA_GET :
         break;

      case MEGAVI_COMMAND_SMS_SEND :
         _MegaviRxBuffer.SmsSendData.SmsTarget = 't';
         strcpy(_MegaviRxBuffer.SmsSendData.SmsText, "Megavi sms message text");
         break;

      case MEGAVI_COMMAND_SMS_STATUS_GET :
         _MegaviRxBuffer.SmsStatusData.SlotNumber = 0;
         break;

      default :
         type = MEGAVI_COMMAND_VERSION;
         return _MegaviStatus;
   }

   _MegaviRxBuffer.CommandCode = type;
   return(MEGAVI_RECEIVE_DATA_WAITING);
}// MegaviStatus

//------------------------------------------------------------------------------
//   RX reset
//------------------------------------------------------------------------------

void MegaviListenStart( void)
// Resets listening on port when error occurred
{
   _MegaviStatus = MEGAVI_RECEIVE_LISTENING;
   TRACE( "MEGAVI : Listening started");
}

//------------------------------------------------------------------------------
//   RX/TX stop
//------------------------------------------------------------------------------

void MegaviListenStop( void)
// Stops listening on port
{
   _MegaviStatus = MEGAVI_IDLE;
   TRACE( "MEGAVI : Listening stopped");
}

//------------------------------------------------------------------------------
//   Read RX buffer
//------------------------------------------------------------------------------

TMegaviCommand *MegaviReceive( void)
// Reads incoming data from receive buffer, succeed only when MEGAVI_RECEIVE_DATA_WAITING
{
   return &_MegaviRxBuffer;
}

//------------------------------------------------------------------------------
//   Send TX buffer
//------------------------------------------------------------------------------

TYesNo MegaviSend( void)
// Sends reply,  succeed only when MEGAVI_IDLE, MEGAVI_SEND_DONE, MEGAVI_RECEIVE_LISTENING or MEGAVI_RECEIVE_DATA_WAITING
{
   TRACE( "MEGAVI Send ");
   return YES;
}

//------------------------------------------------------------------------------
//   Get tx buffer pointer
//------------------------------------------------------------------------------

TMegaviReply *MegaviGetReplyBuffer()
// Returns buffer for outgoing message, should be filled before calling MegaviSend
{
    return &_MegaviTxBuffer;
}
