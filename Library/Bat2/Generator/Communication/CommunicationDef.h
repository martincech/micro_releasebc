//******************************************************************************
//
//   CommunicationDef.h  Communication menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __CommunicationDef_H__
   #define __CommunicationDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif


//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define DATA_PUBLICATION_USERNAME_SIZE 15
#define DATA_PUBLICATION_USERNAME_DEFAULT ""

#define DATA_PUBLICATION_PASSWORD_SIZE 15
#define DATA_PUBLICATION_PASSWORD_DEFAULT ""

#define DATA_PUBLICATION_URL_SIZE 255
#define DATA_PUBLICATION_URL_DEFAULT "http://www.veit.cz/asp"

#define DATA_PUBLICATION_SEND_AT_MIN 0
#define DATA_PUBLICATION_SEND_AT_MAX 0
#define DATA_PUBLICATION_SEND_AT_DEFAULT 0

#define CELLULAR_DATA_APN_SIZE 63
#define CELLULAR_DATA_APN_DEFAULT ""

#define CELLULAR_DATA_USERNAME_SIZE 15
#define CELLULAR_DATA_USERNAME_DEFAULT ""

#define CELLULAR_DATA_PASSWORD_SIZE 15
#define CELLULAR_DATA_PASSWORD_DEFAULT ""


//------------------------------------------------------------------------------
//  Data publication interface
//------------------------------------------------------------------------------

typedef enum {
   DATA_PUBLICATION_INTERFACE_DISABLED,
   DATA_PUBLICATION_INTERFACE_INTERNET,
   DATA_PUBLICATION_INTERFACE_SMS,
   DATA_PUBLICATION_INTERFACE_BOTH,
   _DATA_PUBLICATION_INTERFACE_LAST
} EDataPublicationInterface;

//------------------------------------------------------------------------------
//  Day number
//------------------------------------------------------------------------------

typedef enum {
   _DAY_NUMBER_LAST
} EDayNumber;





//------------------------------------------------------------------------------
//  Data publication
//------------------------------------------------------------------------------

typedef struct {
   byte Interface;
   char Username[ DATA_PUBLICATION_USERNAME_SIZE + 1];
   char Password[ DATA_PUBLICATION_PASSWORD_SIZE + 1];
   char Url[ DATA_PUBLICATION_URL_SIZE + 1]; // URL address
   Day Number StartFromDay; // Start sending from day
   Day Number Period; // Send with period [days]
   Day Number AccelerateFromDay; // Accelerate sending  from day
   Day Number AcceleratedPeriod; // Accelerated sending period [days]
   UTimeGauge SendAt; // Send message at this time
} TDataPublication;

//------------------------------------------------------------------------------
//  Cellular data
//------------------------------------------------------------------------------

typedef struct {
   char Apn[ CELLULAR_DATA_APN_SIZE + 1];
   char Username[ CELLULAR_DATA_USERNAME_SIZE + 1];
   char Password[ CELLULAR_DATA_PASSWORD_SIZE + 1];
} TCellularData;

//------------------------------------------------------------------------------
//  Ethernet
//------------------------------------------------------------------------------

typedef struct {
   byte Dhcp;
   dword Ip;
   dword SubnetMask;
   dword Gateway;
   dword PrimaryDns;
   dword SecondaryDns;
} TEthernet;



//------------------------------------------------------------------------------
#endif
