//******************************************************************************
//
//   MenuInternetConnection.c  Internet connection menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuInternetConnection.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "Communication.h"
#include "MenuCellularData.h"
#include "MenuEthernet.h"
#include "MenuWifi.h"


static DefMenu( InternetConnectionMenu)
   STR_CELLULAR_DATA,
   STR_ETHERNET,
   STR_WIFI,
EndMenu()

typedef enum {
   MI_CELLULAR_DATA,
   MI_ETHERNET,
   MI_WIFI
} EInternetConnectionMenu;

//------------------------------------------------------------------------------
//  Menu InternetConnection
//------------------------------------------------------------------------------

void MenuInternetConnection( void)
// Menu internet connection
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_INTERNET_CONNECTION, InternetConnectionMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_CELLULAR_DATA :
            MenuCellularData();
            break;

         case MI_ETHERNET :
            MenuEthernet();
            break;

         case MI_WIFI :
            MenuWifi();
            break;

      }
   }
} // MenuInternetConnection
