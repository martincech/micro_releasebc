TDataPublication DataPublication;
TCellularData CellularData;
TEthernet Ethernet;

//------------------------------------------------------------------------------
//  Data publication
//------------------------------------------------------------------------------

const TDataPublication DataPublicationDefault = {
   /* Interface */ DATA_PUBLICATION_INTERFACE_DISABLED,
   /* Username */ DATA_PUBLICATION_USERNAME_DEFAULT,
   /* Password */ DATA_PUBLICATION_PASSWORD_DEFAULT,
   /* Url */ DATA_PUBLICATION_URL_DEFAULT,
   /* StartFromDay */ ?,
   /* Period */ ?,
   /* AccelerateFromDay */ ?,
   /* AcceleratedPeriod */ ?,
   /* SendAt */ DATA_PUBLICATION_SEND_AT_DEFAULT
};

//------------------------------------------------------------------------------
//  Cellular data
//------------------------------------------------------------------------------

const TCellularData CellularDataDefault = {
   /* Apn */ CELLULAR_DATA_APN_DEFAULT,
   /* Username */ CELLULAR_DATA_USERNAME_DEFAULT,
   /* Password */ CELLULAR_DATA_PASSWORD_DEFAULT
};

//------------------------------------------------------------------------------
//  Ethernet
//------------------------------------------------------------------------------

const TEthernet EthernetDefault = {
   /* Dhcp */ YES,
   /* Ip */ 0xC0A80102,
   /* SubnetMask */ 0xFFFFFF00,
   /* Gateway */ 0xC0A80101,
   /* PrimaryDns */ 0xC0A80101,
   /* SecondaryDns */ 0
};

