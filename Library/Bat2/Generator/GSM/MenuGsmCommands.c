//******************************************************************************
//
//   MenuGsmCommands.c  Gsm commands menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmCommands.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "MenuGsm.h"


static DefMenu( GsmCommandsMenu)
   STR_ENABLED,
   STR_CHECK_PHONE_NUMBER,
   STR_EXPIRATION,
EndMenu()

typedef enum {
   MI_ENABLED,
   MI_CHECK_PHONE_NUMBER,
   MI_EXPIRATION
} EGsmCommandsMenu;

// Local functions :

static void GsmCommandsParameters( int Index, int y, TGsmCommands *Parameters);
// Draw gsm commands parameters

//------------------------------------------------------------------------------
//  Menu GsmCommands
//------------------------------------------------------------------------------

void MenuGsmCommands( void)
// Edit gsm commands parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_COMMANDS, GsmCommandsMenu, (TMenuItemCb *)GsmCommandsParameters, &GsmCommands, &MData)){
         ConfigGsmCommandsSave();
         return;
      }
      switch( MData.Item){
         case MI_ENABLED :
            i = GsmCommands.Enabled;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmCommands.Enabled = (byte)i;
            break;

         case MI_CHECK_PHONE_NUMBER :
            i = GsmCommands.CheckPhoneNumber;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmCommands.CheckPhoneNumber = (byte)i;
            break;

         case MI_EXPIRATION :
            i = GsmCommands.Expiration;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_COMMANDS_EXPIRATION_MIN, GSM_COMMANDS_EXPIRATION_MAX, "min")){
               break;
            }
            GsmCommands.Expiration = (word)i;
            break;

      }
   }
} // MenuGsmCommands

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void GsmCommandsParameters( int Index, int y, TGsmCommands *Parameters)
// Draw gsm commands parameters
{
   switch( Index){
      case MI_ENABLED :
         DLabelEnum( Parameters->Enabled, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_CHECK_PHONE_NUMBER :
         DLabelEnum( Parameters->CheckPhoneNumber, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_EXPIRATION :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->Expiration, "min");
         break;

   }
} // GsmCommandsParameters
