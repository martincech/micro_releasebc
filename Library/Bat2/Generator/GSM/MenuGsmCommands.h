//******************************************************************************
//
//   MenuGsmCommands.h  Gsm commands menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGsmCommands_H__
   #define __MenuGsmCommands_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MenuGsm_H__
   #include "MenuGsm.h"
#endif


void MenuGsmCommands( void);
// Menu gsm commands

#endif
