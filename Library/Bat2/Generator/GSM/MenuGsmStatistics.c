//******************************************************************************
//
//   MenuGsmStatistics.c  Gsm statistics menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsmStatistics.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "MenuGsm.h"


static DefMenu( GsmStatisticsMenu)
   STR_START_FROM_DAY,
   STR_PERIOD,
   STR_ACCELERATE_FROM_DAY,
   STR_ACCELERATED_PERIOD,
   STR_SEND_AT,
   STR_SMS_FORMAT,
   STR_SEND_HISTOGRAM,

EndMenu()

typedef enum {
   MI_START_FROM_DAY,
   MI_PERIOD,
   MI_ACCELERATE_FROM_DAY,
   MI_ACCELERATED_PERIOD,
   MI_SEND_AT,
   MI_SMS_FORMAT,
   MI_SEND_HISTOGRAM,

} EGsmStatisticsMenu;

// Local functions :

static void GsmStatisticsParameters( int Index, int y, TGsmStatistics *Parameters);
// Draw gsm statistics parameters

//------------------------------------------------------------------------------
//  Menu GsmStatistics
//------------------------------------------------------------------------------

void MenuGsmStatistics( void)
// Edit gsm statistics parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_STATISTICS, GsmStatisticsMenu, (TMenuItemCb *)GsmStatisticsParameters, &GsmStatistics, &MData)){
         ConfigGsmStatisticsSave();
         return;
      }
      switch( MData.Item){
         case MI_START_FROM_DAY :
            i = GsmStatistics.StartFromDay;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_STATISTICS_START_FROM_DAY_MIN, GSM_STATISTICS_START_FROM_DAY_MAX, 0)){
               break;
            }
            GsmStatistics.StartFromDay = (TDayNumber)i;
            break;

         case MI_PERIOD :
            i = GsmStatistics.Period;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_STATISTICS_PERIOD_MIN, GSM_STATISTICS_PERIOD_MAX, 0)){
               break;
            }
            GsmStatistics.Period = (TDayNumber)i;
            break;

         case MI_ACCELERATE_FROM_DAY :
            i = GsmStatistics.AccelerateFromDay;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_STATISTICS_ACCELERATE_FROM_DAY_MIN, GSM_STATISTICS_ACCELERATE_FROM_DAY_MAX, 0)){
               break;
            }
            GsmStatistics.AccelerateFromDay = (TDayNumber)i;
            break;

         case MI_ACCELERATED_PERIOD :
            i = GsmStatistics.AcceleratedPeriod;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, GSM_STATISTICS_ACCELERATED_PERIOD_MIN, GSM_STATISTICS_ACCELERATED_PERIOD_MAX, 0)){
               break;
            }
            GsmStatistics.AcceleratedPeriod = (TDayNumber)i;
            break;

         case MI_SEND_AT :
            uTime( &DateTime.Time, GsmStatistics.SendAt);
            if( !DInputTime( STR_, STR_ENTER_, &DateTime.Time)){
               break;
            }
            GsmStatistics.SendAt = uTimeGauge( &DateTime.Time);
            break;

         case MI_SMS_FORMAT :
            i = GsmStatistics.SmsFormat;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_GSM_SMS_FORMAT, _GSM_SMS_FORMAT_LAST)){
               break;
            }
            GsmStatistics.SmsFormat = (byte)i;
            break;

         case MI_SEND_HISTOGRAM :
            i = GsmStatistics.SendHistogram;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            GsmStatistics.SendHistogram = (byte)i;
            break;

      }
   }
} // MenuGsmStatistics

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void GsmStatisticsParameters( int Index, int y, TGsmStatistics *Parameters)
// Draw gsm statistics parameters
{
   switch( Index){
      case MI_START_FROM_DAY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->StartFromDay, 0);
         break;

      case MI_PERIOD :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->Period, 0);
         break;

      case MI_ACCELERATE_FROM_DAY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->AccelerateFromDay, 0);
         break;

      case MI_ACCELERATED_PERIOD :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->AcceleratedPeriod, 0);
         break;

      case MI_SEND_AT :
         DTimeRight( Parameters->SendAt, DMENU_PARAMETERS_X, y);
         break;

      case MI_SMS_FORMAT :
         DLabelEnum( Parameters->SmsFormat, ENUM_GSM_SMS_FORMAT, DMENU_PARAMETERS_X, y);
         break;

      case MI_SEND_HISTOGRAM :
         DLabelEnum( Parameters->SendHistogram, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

   }
} // GsmStatisticsParameters
