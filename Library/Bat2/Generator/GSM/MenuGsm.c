//******************************************************************************
//
//   MenuGsm.c  Gsm menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuGsm.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "MenuGsm.h"
#include "MenuGsmPowerOptions.h"
#include "MenuGsmContacts.h"
#include "MenuGsmCommands.h"
#include "MenuGsmEvents.h"
#include "MenuGsmInfo.h"
#include "MenuGsmPinCode.h"


static DefMenu( GsmMenu)
   STR_POWER_OPTIONS,
   STR_CONTACTS,
   STR_COMMANDS,
   STR_EVENTS,
   STR_INFO,
   STR_PIN_CODE,
EndMenu()

typedef enum {
   MI_POWER_OPTIONS,
   MI_CONTACTS,
   MI_COMMANDS,
   MI_EVENTS,
   MI_INFO,
   MI_PIN_CODE
} EGsmMenu;

//------------------------------------------------------------------------------
//  Menu Gsm
//------------------------------------------------------------------------------

void MenuGsm( void)
// Menu gsm
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_GSM, GsmMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_POWER_OPTIONS :
            MenuGsmPowerOptions();
            break;

         case MI_CONTACTS :
            MenuGsmContacts();
            break;

         case MI_COMMANDS :
            MenuGsmCommands();
            break;

         case MI_EVENTS :
            MenuGsmEvents();
            break;

         case MI_INFO :
            MenuGsmInfo();
            break;

         case MI_PIN_CODE :
            MenuGsmPinCode();
            break;

      }
   }
} // MenuGsm
