//******************************************************************************
//
//   MenuStatistics.c  Statistics menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuStatistics.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "Menu.h"
#include "MenuArchive.h"
#include "MenuGraph.h"
#include "MenuTodaysWeighings.h"


static DefMenu( StatisticsMenu)
   STR_ARCHIVE,
   STR_GRAPHS,
   STR_TODAYS_WEIGHINGS,
EndMenu()

typedef enum {
   MI_ARCHIVE,
   MI_GRAPHS,
   MI_TODAY'S_WEIGHINGS
} EStatisticsMenu;

//------------------------------------------------------------------------------
//  Menu Statistics
//------------------------------------------------------------------------------

void MenuStatistics( void)
// Menu statistics
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_STATISTICS, StatisticsMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_ARCHIVE :
            MenuArchive();
            break;

         case MI_GRAPHS :
            MenuGraph();
            break;

         case MI_TODAY'S_WEIGHINGS :
            MenuTodaysWeighings();
            break;

      }
   }
} // MenuStatistics
