//******************************************************************************
//
//   MenuConfiguration.c  Configuration menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuConfiguration.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "Menu.h"
#include "MenuConfigurationWeighing.h"
#include "MenuCommunication.h"
#include "MenuDataPublication.h"
#include "MenuDisplay.h"


static DefMenu( ConfigurationMenu)
   STR_WEIGHING,
   STR_COMMUNICATION,
   STR_DATA_PUBLICATION,
   STR_DISPLAY,
EndMenu()

typedef enum {
   MI_WEIGHING,
   MI_COMMUNICATION,
   MI_DATA_PUBLICATION,
   MI_DISPLAY
} EConfigurationMenu;

//------------------------------------------------------------------------------
//  Menu Configuration
//------------------------------------------------------------------------------

void MenuConfiguration( void)
// Menu configuration
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_CONFIGURATION, ConfigurationMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_WEIGHING :
            MenuConfigurationWeighing();
            break;

         case MI_COMMUNICATION :
            MenuCommunication();
            break;

         case MI_DATA_PUBLICATION :
            MenuDataPublication();
            break;

         case MI_DISPLAY :
            MenuDisplay();
            break;

      }
   }
} // MenuConfiguration
