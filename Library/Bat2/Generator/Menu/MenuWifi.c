//******************************************************************************
//
//   MenuWifi.c  Wifi menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWifi.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Str.h"                  // Strings

#include "Menu.h"
#include "MenuWifiNetwork.h"


static DefMenu( WifiMenu)
   STR_NETWORK,
EndMenu()

typedef enum {
   MI_NETWORK
} EWifiMenu;

//------------------------------------------------------------------------------
//  Menu Wifi
//------------------------------------------------------------------------------

void MenuWifi( void)
// Menu wifi
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_WIFI, WifiMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_NETWORK :
            MenuWifiNetwork();
            break;

      }
   }
} // MenuWifi
