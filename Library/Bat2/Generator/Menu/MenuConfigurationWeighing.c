//******************************************************************************
//
//   MenuConfigurationWeighing.c  Configuration weighing menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuConfigurationWeighing.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Menu.h"
#include "MenuPredefinedWeighings.h"
#include "MenuGrowthCurve.h"
#include "MenuCorrectionCurve.h"


static DefMenu( ConfigurationWeighingMenu)
   STR_PREDEFINED_WEIGHINGS,
   STR_DEFAULT_WEIGHING,
   STR_GROWTH_CURVES,
   STR_CORRECTION_CURVES,
EndMenu()

typedef enum {
   MI_PREDEFINED_WEIGHINGS,
   MI_DEFAULT_WEIGHING,
   MI_GROWTH_CURVES,
   MI_CORRECTION_CURVES
} EConfigurationWeighingMenu;

// Local functions :

static void ConfigurationWeighingParameters( int Index, int y, TConfigurationWeighing *Parameters);
// Draw configuration weighing parameters

//------------------------------------------------------------------------------
//  Menu ConfigurationWeighing
//------------------------------------------------------------------------------

void MenuConfigurationWeighing( void)
// Edit configuration weighing parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_WEIGHING, ConfigurationWeighingMenu, (TMenuItemCb *)ConfigurationWeighingParameters, &ConfigurationWeighing, &MData)){
         ConfigConfigurationWeighingSave();
         return;
      }
      switch( MData.Item){
         case MI_PREDEFINED_WEIGHINGS :
            MenuPredefinedWeighings();
            break;

         case MI_DEFAULT_WEIGHING :
            i = ConfigurationWeighing.DefaultWeighing;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, CONFIGURATION_WEIGHING_DEFAULT_WEIGHING_MIN, CONFIGURATION_WEIGHING_DEFAULT_WEIGHING_MAX, 0)){
               break;
            }
            ConfigurationWeighing.DefaultWeighing = (TWeighingIndex)i;
            break;

         case MI_GROWTH_CURVES :
            MenuGrowthCurve();
            break;

         case MI_CORRECTION_CURVES :
            MenuCorrectionCurve();
            break;

      }
   }
} // MenuConfigurationWeighing

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void ConfigurationWeighingParameters( int Index, int y, TConfigurationWeighing *Parameters)
// Draw configuration weighing parameters
{
   switch( Index){
      case MI_PREDEFINED_WEIGHINGS :
         break;

      case MI_DEFAULT_WEIGHING :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->DefaultWeighing, 0);
         break;

      case MI_GROWTH_CURVES :
         break;

      case MI_CORRECTION_CURVES :
         break;

   }
} // ConfigurationWeighingParameters
