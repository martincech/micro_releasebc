//******************************************************************************
//
//   MenuCellularData.c  Cellular data menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuCellularData.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Menu.h"


static DefMenu( CellularDataMenu)
   STR_APN,
   STR_USERNAME,
   STR_PASSWORD,
EndMenu()

typedef enum {
   MI_APN,
   MI_USERNAME,
   MI_PASSWORD
} ECellularDataMenu;

// Local functions :

static void CellularDataParameters( int Index, int y, TCellularData *Parameters);
// Draw cellular data parameters

//------------------------------------------------------------------------------
//  Menu CellularData
//------------------------------------------------------------------------------

void MenuCellularData( void)
// Edit cellular data parameters
{
TMenuData MData;
int       i;
char Apn[ CELLULAR_DATA_APN_SIZE + 1];
char Username[ CELLULAR_DATA_USERNAME_SIZE + 1];
char Password[ CELLULAR_DATA_PASSWORD_SIZE + 1];


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_CELLULAR_DATA, CellularDataMenu, (TMenuItemCb *)CellularDataParameters, &CellularData, &MData)){
         ConfigCellularDataSave();
         return;
      }
      switch( MData.Item){
         case MI_APN :
            strcpy( Apn, CellularData.Apn);
            if( !DInputText( STR_APN, STR_ENTER_APN, Apn, CELLULAR_DATA_APN_SIZE)){
               break;
            }
            strcpy( CellularData.Apn, Apn);
            break;

         case MI_USERNAME :
            strcpy( Username, CellularData.Username);
            if( !DInputText( STR_USERNAME, STR_ENTER_USERNAME, Username, CELLULAR_DATA_USERNAME_SIZE)){
               break;
            }
            strcpy( CellularData.Username, Username);
            break;

         case MI_PASSWORD :
            strcpy( Password, CellularData.Password);
            if( !DInputText( STR_PASSWORD, STR_ENTER_PASSWORD, Password, CELLULAR_DATA_PASSWORD_SIZE)){
               break;
            }
            strcpy( CellularData.Password, Password);
            break;

      }
   }
} // MenuCellularData

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void CellularDataParameters( int Index, int y, TCellularData *Parameters)
// Draw cellular data parameters
{
   switch( Index){
      case MI_APN :
         DLabelNarrow( Parameters->Apn, DMENU_PARAMETERS_X, y);
         break;

      case MI_USERNAME :
         DLabelNarrow( Parameters->Username, DMENU_PARAMETERS_X, y);
         break;

      case MI_PASSWORD :
         DLabelNarrow( Parameters->Password, DMENU_PARAMETERS_X, y);
         break;

   }
} // CellularDataParameters
