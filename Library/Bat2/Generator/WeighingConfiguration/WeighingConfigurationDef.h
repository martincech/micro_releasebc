//******************************************************************************
//
//   WeighingConfigurationDef.h  Bat2 weighing configuration
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __WeighingConfigurationDef_H__
   #define __WeighingConfigurationDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif


//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define WEIGHING_CONFIGURATION_TYPE_SIZE 15
#define WEIGHING_CONFIGURATION_TYPE_DEFAULT "BROILERS"

#define WEIGHING_CONFIGURATION_FLOCK_SIZE 15
#define WEIGHING_CONFIGURATION_FLOCK_DEFAULT "Flock000"

#define WEIGHING_CONFIGURATION_INITIAL_DAY_MIN 1
#define WEIGHING_CONFIGURATION_INITIAL_DAY_MAX 999
#define WEIGHING_CONFIGURATION_INITIAL_DAY_DEFAULT 1

#define WEIGHING_CONFIGURATION_INITIAL_WEIGHT_MIN 0
#define WEIGHING_CONFIGURATION_INITIAL_WEIGHT_MAX 0
#define WEIGHING_CONFIGURATION_INITIAL_WEIGHT_DEFAULT 0

#define WEIGHING_CONFIGURATION_GROWTH_CURVE_MIN 0
#define WEIGHING_CONFIGURATION_GROWTH_CURVE_MAX 100
#define WEIGHING_CONFIGURATION_GROWTH_CURVE_DEFAULT 0xFF

#define WEIGHING_CONFIGURATION_STANDARD_CURVE_MIN 0
#define WEIGHING_CONFIGURATION_STANDARD_CURVE_MAX 100
#define WEIGHING_CONFIGURATION_STANDARD_CURVE_DEFAULT 0xFF

#define WEIGHING_CONFIGURATION_CORRECTION_CURVE_MIN 0
#define WEIGHING_CONFIGURATION_CORRECTION_CURVE_MAX 100
#define WEIGHING_CONFIGURATION_CORRECTION_CURVE_DEFAULT 0xFF

#define WEIGHING_CONFIGURATION_DAY_START_MIN 0
#define WEIGHING_CONFIGURATION_DAY_START_MAX 0
#define WEIGHING_CONFIGURATION_DAY_START_DEFAULT 0

#define WEIGHING_INITIAL_WEIGHTS_MALES_MIN 0
#define WEIGHING_INITIAL_WEIGHTS_MALES_MAX 0
#define WEIGHING_INITIAL_WEIGHTS_MALES_DEFAULT 0

#define WEIGHING_INITIAL_WEIGHTS_FEMALES_MIN 0
#define WEIGHING_INITIAL_WEIGHTS_FEMALES_MAX 0
#define WEIGHING_INITIAL_WEIGHTS_FEMALES_DEFAULT 0

#define WEIGHING_GROWTH_CURVES_MALES_MIN 0
#define WEIGHING_GROWTH_CURVES_MALES_MAX 100
#define WEIGHING_GROWTH_CURVES_MALES_DEFAULT 0xFF

#define WEIGHING_GROWTH_CURVES_FEMALES_MIN 0
#define WEIGHING_GROWTH_CURVES_FEMALES_MAX 100
#define WEIGHING_GROWTH_CURVES_FEMALES_DEFAULT 0xFF

#define WEIGHING_STANDARD_CURVES_MALES_MIN 0
#define WEIGHING_STANDARD_CURVES_MALES_MAX 100
#define WEIGHING_STANDARD_CURVES_MALES_DEFAULT 0xFF

#define WEIGHING_STANDARD_CURVES_FEMALES_MIN 0
#define WEIGHING_STANDARD_CURVES_FEMALES_MAX 100
#define WEIGHING_STANDARD_CURVES_FEMALES_DEFAULT 0xFF

#define WEIGHING_ACCEPTANCE_MARGIN_ABOVE_MIN 0
#define WEIGHING_ACCEPTANCE_MARGIN_ABOVE_MAX 100
#define WEIGHING_ACCEPTANCE_MARGIN_ABOVE_DEFAULT 30

#define WEIGHING_ACCEPTANCE_MARGIN_BELOW_MIN 0
#define WEIGHING_ACCEPTANCE_MARGIN_BELOW_MAX 100
#define WEIGHING_ACCEPTANCE_MARGIN_BELOW_DEFAULT 30

#define WEIGHING_ACCEPTANCE_DATA_MARGIN_ABOVE_MIN 0
#define WEIGHING_ACCEPTANCE_DATA_MARGIN_ABOVE_MAX 100
#define WEIGHING_ACCEPTANCE_DATA_MARGIN_ABOVE_DEFAULT 30

#define WEIGHING_ACCEPTANCE_DATA_MARGIN_BELOW_MIN 0
#define WEIGHING_ACCEPTANCE_DATA_MARGIN_BELOW_MAX 100
#define WEIGHING_ACCEPTANCE_DATA_MARGIN_BELOW_DEFAULT 30

#define WEIGHING_DETECTION_FILTER_MIN 1
#define WEIGHING_DETECTION_FILTER_MAX 150
#define WEIGHING_DETECTION_FILTER_DEFAULT 10

#define WEIGHING_DETECTION_STABILIZATION_TIME_MIN 1
#define WEIGHING_DETECTION_STABILIZATION_TIME_MAX 100
#define WEIGHING_DETECTION_STABILIZATION_TIME_DEFAULT 5

#define WEIGHING_DETECTION_STABILIZATION_RANGE_MIN 1
#define WEIGHING_DETECTION_STABILIZATION_RANGE_MAX 10000
#define WEIGHING_DETECTION_STABILIZATION_RANGE_DEFAULT 30


//------------------------------------------------------------------------------
//  Prediction mode
//------------------------------------------------------------------------------

typedef enum {
   PREDICTION_MODE_AUTOMATIC,
   PREDICTION_MODE_GROWTH_CURVE,
   _PREDICTION_MODE_LAST
} EPredictionMode;

//------------------------------------------------------------------------------
//  Prediction growth
//------------------------------------------------------------------------------

typedef enum {
   PREDICTION_GROWTH_SLOW,
   PREDICTION_GROWTH_FAST,
   _PREDICTION_GROWTH_LAST
} EPredictionGrowth;

//------------------------------------------------------------------------------
//  Sex differentiation
//------------------------------------------------------------------------------

typedef enum {
   SEX_DIFFERENTIATION_NO,
   SEX_DIFFERENTIATION_YES,
   SEX_DIFFERENTIATION_STEP_ONLY,
   _SEX_DIFFERENTIATION_LAST
} ESexDifferentiation;

//------------------------------------------------------------------------------
//  Weighing status
//------------------------------------------------------------------------------

typedef enum {
   WEIGHING_STATUS_UNDEFINED,
   WEIGHING_STATUS_STOPPED,
   WEIGHING_STATUS_WAIT,
   WEIGHING_STATUS_SLEEP,
   WEIGHING_STATUS_WEIGHING,
   WEIGHING_STATUS_SUSPENDED,
   WEIGHING_STATUS_RELEASED,
   WEIGHING_STATUS_CALIBRATION,
   WEIGHING_STATUS_DIAGNOSTICS,
   _WEIGHING_STATUS_LAST
} EWeighingStatus;

//------------------------------------------------------------------------------
//  Adjust target weights
//------------------------------------------------------------------------------

typedef enum {
   ADJUST_TARGET_WEIGHTS_NONE,
   ADJUST_TARGET_WEIGHTS_SINGLE,
   ADJUST_TARGET_WEIGHTS_RECOMPUTE,
   _ADJUST_TARGET_WEIGHTS_LAST
} EAdjustTargetWeights;



//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef byte TCurveIdentifier;
typedef byte TCorrectionIdentifier;
typedef word TDayNumber;

//------------------------------------------------------------------------------
//  Weighing configuration
//------------------------------------------------------------------------------

typedef struct {
   char Type[ WEIGHING_CONFIGURATION_TYPE_SIZE + 1]; // Chicken type
   char Flock[ WEIGHING_CONFIGURATION_FLOCK_SIZE + 1]; // Flock name
   TDayNumber InitialDay; // Initial day
   TWeightGauge InitialWeight; // Initial weight
   TCurveIdentifier GrowthCurve; // Weighing curve index
   TCurveIdentifier StandardCurve; // Standard curve for comparison
   TCorrectionIdentifier CorrectionCurve; // Correction curve index
   UTimeGauge DayStart; // Day start time (almost midnight)
} TWeighingConfiguration;

//------------------------------------------------------------------------------
//  Weighing initial weights
//------------------------------------------------------------------------------

typedef struct {
   TWeightGauge Males; // Male initial weight
   TWeightGauge Females; // Female initial weight
} TWeighingInitialWeights;

//------------------------------------------------------------------------------
//  Weighing growth curves
//------------------------------------------------------------------------------

typedef struct {
   TCurveIdentifier Males; // Male weighing curve index
   TCurveIdentifier Females; // Female weighing curve index
} TWeighingGrowthCurves;

//------------------------------------------------------------------------------
//  Weighing standard curves
//------------------------------------------------------------------------------

typedef struct {
   TCurveIdentifier Males; // Standard curve for male comparison
   TCurveIdentifier Females; // Standard curve for female comparison
} TWeighingStandardCurves;

//------------------------------------------------------------------------------
//  Weighing target weights
//------------------------------------------------------------------------------

typedef struct {
   byte Mode; // Target weight prediction mode
   byte Growth; // Target weight prediction with gain
   byte SexDifferentiation; // Sex differentiation
   byte Sex; // Fixed sex
   byte AdjustTargetWeights;
} TWeighingTargetWeights;

//------------------------------------------------------------------------------
//  Weighing acceptance
//------------------------------------------------------------------------------

typedef struct {
   byte MarginAbove; // Margin above target [%]
   byte MarginBelow; // Margin below target [%]
} TWeighingAcceptance;

//------------------------------------------------------------------------------
//  Weighing acceptance data
//------------------------------------------------------------------------------

typedef struct {
   byte MarginAbove; // Margin above target [%]
   byte MarginBelow; // Margin below target [%]
} TWeighingAcceptanceData;

//------------------------------------------------------------------------------
//  Weighing detection
//------------------------------------------------------------------------------

typedef struct {
   byte Filter; // Smooth filter [0.1s]
   byte StabilizationTime; // Stabilisation Time  [0.1s]
   byte StabilizationRange; // Relative stabilization range [0.1%]
   byte Step; // Step mode
} TWeighingDetection;



//------------------------------------------------------------------------------
#endif
