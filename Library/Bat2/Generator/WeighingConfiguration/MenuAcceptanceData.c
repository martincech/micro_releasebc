//******************************************************************************
//
//   MenuAcceptanceData.c  Acceptance data menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuAcceptanceData.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "WeighingConfiguration.h"


static DefMenu( AcceptanceDataMenu)
   STR_MARGIN_ABOVE,
   STR_MARGIN_BELOW,
EndMenu()

typedef enum {
   MI_MARGIN_ABOVE,
   MI_MARGIN_BELOW
} EAcceptanceDataMenu;

// Local functions :

static void WeighingAcceptanceDataParameters( int Index, int y, TWeighingAcceptanceData *Parameters);
// Draw acceptance data parameters

//------------------------------------------------------------------------------
//  Menu AcceptanceData
//------------------------------------------------------------------------------

void MenuAcceptanceData( void)
// Edit acceptance data parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_ACCEPTANCE_DATA, AcceptanceDataMenu, (TMenuItemCb *)WeighingAcceptanceDataParameters, &WeighingAcceptanceData, &MData)){
         ConfigWeighingAcceptanceDataSave();
         return;
      }
      switch( MData.Item){
         case MI_MARGIN_ABOVE :
            i = WeighingAcceptanceData.MarginAbove;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_ACCEPTANCE_DATA_MARGIN_ABOVE_MIN, WEIGHING_ACCEPTANCE_DATA_MARGIN_ABOVE_MAX, "%")){
               break;
            }
            WeighingAcceptanceData.MarginAbove = (byte)i;
            break;

         case MI_MARGIN_BELOW :
            i = WeighingAcceptanceData.MarginBelow;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_ACCEPTANCE_DATA_MARGIN_BELOW_MIN, WEIGHING_ACCEPTANCE_DATA_MARGIN_BELOW_MAX, "%")){
               break;
            }
            WeighingAcceptanceData.MarginBelow = (byte)i;
            break;

      }
   }
} // MenuAcceptanceData

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingAcceptanceDataParameters( int Index, int y, TWeighingAcceptanceData *Parameters)
// Draw acceptance data parameters
{
   switch( Index){
      case MI_MARGIN_ABOVE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->MarginAbove, "%");
         break;

      case MI_MARGIN_BELOW :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Parameters->MarginBelow, "%");
         break;

   }
} // WeighingAcceptanceDataParameters
