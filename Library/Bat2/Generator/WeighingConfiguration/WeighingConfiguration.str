//******************************************************************************
//
//   WeighingConfiguration.str  Bat2 weighing configuration
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************
//------------------------------------------------------------------------------
//  Prediction mode enum
//------------------------------------------------------------------------------

ENUM_PREDICTION_MODE
STR_AUTOMATIC "Automatic"
STR_GROWTH_CURVE "Growth curve"

//------------------------------------------------------------------------------
//  Prediction growth enum
//------------------------------------------------------------------------------

ENUM_PREDICTION_GROWTH
STR_SLOW "Slow"
STR_FAST "Fast"

//------------------------------------------------------------------------------
//  Sex differentiation enum
//------------------------------------------------------------------------------

ENUM_SEX_DIFFERENTIATION
STR_NO "No"
STR_YES "Yes"
STR_STEP_ONLY "Step only"

//------------------------------------------------------------------------------
//  Weighing status enum
//------------------------------------------------------------------------------

ENUM_WEIGHING_STATUS
STR_UNDEFINED "Undefined"
STR_STOPPED "Stopped"
STR_WAIT "Wait"
STR_SLEEP "Sleep"
STR_WEIGHING "Weighing"
STR_SUSPENDED "Suspended"
STR_RELEASED "Released"
STR_CALIBRATION "Calibration"
STR_DIAGNOSTICS "Diagnostics"

//------------------------------------------------------------------------------
//  Adjust target weights enum
//------------------------------------------------------------------------------

ENUM_ADJUST_TARGET_WEIGHTS
STR_NONE "None"
STR_SINGLE "Single"
STR_RECOMPUTE "Recompute"

//------------------------------------------------------------------------------
//  Configuration
//------------------------------------------------------------------------------

STR_CONFIGURATION "Configuration"
STR_MENU_SELECTION "Menu selection"
STR_TYPE "Type"
STR_FLOCK "Flock"
STR_INITIAL_DAY "Initial day"
STR_INITIAL_WEIGHT "Initial weight"
STR_INITIAL_WEIGHTS "Initial weights"
STR_GROWTH_CURVE "Growth curve"
STR_GROWTH_CURVES "Growth curves"
STR_STANDARD_CURVE "Standard curve"
STR_STANDARD_CURVES "Standard curves"
STR_CORRECTION_CURVE "Correction curve"
STR_DETECTION "Detection"
STR_TARGET_WEIGHTS "Target weights"
STR_ACCEPTANCE "Acceptance"
STR_STATISTICS "Statistics"
STR_DAY_START "Day start"
STR_START_NOW "Start now"
STR_START_LATER "Start later"

//------------------------------------------------------------------------------
//  Initial weights
//------------------------------------------------------------------------------

STR_INITIAL_WEIGHTS "Initial weights"
STR_MALES "Males"
STR_FEMALES "Females"

//------------------------------------------------------------------------------
//  Growth curves
//------------------------------------------------------------------------------

STR_GROWTH_CURVES "Growth curves"
STR_MALES "Males"
STR_FEMALES "Females"

//------------------------------------------------------------------------------
//  Standard curves
//------------------------------------------------------------------------------

STR_STANDARD_CURVES "Standard curves"
STR_MALES "Males"
STR_FEMALES "Females"

//------------------------------------------------------------------------------
//  Target weights
//------------------------------------------------------------------------------

STR_TARGET_WEIGHTS "Target weights"
STR_MODE "Mode"
STR_GROWTH "Growth"
STR_SEX_DIFFERENTIATION "Sex differentiation"
STR_SEX "Sex"
STR_ADJUST_TARGET_WEIGHTS "Adjust target weights"

//------------------------------------------------------------------------------
//  Acceptance
//------------------------------------------------------------------------------

STR_ACCEPTANCE "Acceptance"
STR_MARGIN_ABOVE "Margin above"
STR_MARGIN_BELOW "Margin below"
STR_MALES "Males"
STR_FEMALES "Females"

//------------------------------------------------------------------------------
//  Acceptance data
//------------------------------------------------------------------------------

STR_ACCEPTANCE_DATA "Acceptance data"
STR_MARGIN_ABOVE "Margin above"
STR_MARGIN_BELOW "Margin below"

//------------------------------------------------------------------------------
//  Detection
//------------------------------------------------------------------------------

STR_DETECTION "Detection"
STR_FILTER "Filter"
STR_STABILIZATION_TIME "Stabilization time"
STR_STABILIZATION_RANGE "Stabilization range"
STR_STEP "Step"

