//******************************************************************************
//
//   MenuGrowthCurves.h  Growth curves menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuGrowthCurves_H__
   #define __MenuGrowthCurves_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingConfiguration_H__
   #include "WeighingConfiguration.h"
#endif


void MenuGrowthCurves( void);
// Menu growth curves

#endif
