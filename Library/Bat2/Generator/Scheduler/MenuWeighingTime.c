//******************************************************************************
//
//   MenuWeighingTime.c  Weighing time menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingTime.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Weighing Scheduler.h"


static DefMenu( WeighingTimeMenu)
   STR_WEIGHING_FROM,
   STR_WEIGHING_TO,
EndMenu()

typedef enum {
   MI_WEIGHING_FROM,
   MI_WEIGHING_TO
} EWeighingTimeMenu;

// Local functions :

static void WeighingTimeParameters( int Index, int y, TWeighingTime *Parameters);
// Draw weighing time parameters

//------------------------------------------------------------------------------
//  Menu WeighingTime
//------------------------------------------------------------------------------

void MenuWeighingTime( void)
// Edit weighing time parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      // selection :
      if( !DMenu( STR_WEIGHING_TIME, WeighingTimeMenu, (TMenuItemCb *)WeighingTimeParameters, &WeighingTime, &MData)){
         ConfigWeighingTimeSave();
         return;
      }
      switch( MData.Item){
         case MI_WEIGHING_FROM :
            uTime( &DateTime.Time, WeighingTime.WeighingFrom);
            if( !DInputTime( STR_$UITEM$, STR_ENTER_$UITEM$, &DateTime.Time)){
               break;
            }
            WeighingTime.WeighingFrom = uTimeGauge( &DateTime.Time);
            break;

         case MI_WEIGHING_TO :
            uTime( &DateTime.Time, WeighingTime.WeighingTo);
            if( !DInputTime( STR_$UITEM$, STR_ENTER_$UITEM$, &DateTime.Time)){
               break;
            }
            WeighingTime.WeighingTo = uTimeGauge( &DateTime.Time);
            break;

      }
   }
} // MenuWeighingTime

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingTimeParameters( int Index, int y, TWeighingTime *Parameters)
// Draw weighing time parameters
{
   switch( Index){
      case MI_WEIGHING_FROM :
         DTimeRight( Parameters->WeighingFrom, DMENU_PARAMETERS_X, y);
         break;

      case MI_WEIGHING_TO :
         DTimeRight( Parameters->WeighingTo, DMENU_PARAMETERS_X, y);
         break;

   }
} // WeighingTimeParameters
