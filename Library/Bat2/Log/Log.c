//******************************************************************************
//
//   Log.c           Log
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "Log.h"
#include "System/System.h"
#include "Memory/File.h"
#include "Data/uFifo.h"

// log FIFO :
uFifoAlloc( LogFifoConfiguration, LOG_POINTER_ADDRESS,
            LOG_CAPACITY, LOG_ITEM_SIZE, YES, FILE_LOG_LOCAL, FILE_FIFO_LOCAL)
uFifoAlloc( LogFifoConfigurationRemote, LOG_POINTER_ADDRESS,
            LOG_CAPACITY, LOG_ITEM_SIZE, NO,  FILE_LOG_LOCAL, FILE_FIFO_REMOTE)

static UFifo LogFifo;
static UFifoDescriptor LogFifoDescriptor;
//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------
TYesNo LogRemoteSave( void)
// Save to remote device
{
TYesNo copyState;

   uFifoClose( &LogFifo);
   LogFifo.Descriptor = NULL;
   // uFifoCopy opens file again, hence it needs to be closed
   copyState = uFifoCopy( &LogFifoConfigurationRemote, &LogFifoConfiguration);
   LogFifo.Descriptor = &LogFifoDescriptor;
   uFifoOpen( &LogFifo, LogFifo.Descriptor);
   return copyState;
} // LogRemoteSave

void LogInit( void)
// Initialize
{
   LogFifo.Descriptor = &LogFifoDescriptor;
   uFifoInit( LogFifo.Descriptor, &LogFifoConfiguration);
   uFifoOpen( &LogFifo, LogFifo.Descriptor);
} // LogInit

//------------------------------------------------------------------------------
//  Clear
//------------------------------------------------------------------------------

void LogClear( void)
// Clear all log
{
   uFifoClear( &LogFifo);
} // LogClear

//------------------------------------------------------------------------------
//  Append
//------------------------------------------------------------------------------

TYesNo LogAppend( TLogItem *Item)
// Append log record
{
   if( !LogFifo.Descriptor){
      return NO;
   }
   Item->Timestamp = SysDateTime();
   return( uFifoAppend( &LogFifo, Item));
} // LogAppend

//------------------------------------------------------------------------------
//  Count
//------------------------------------------------------------------------------

TLogIndex LogCount( void)
// Returns log records count
{
   if( !LogFifo.Descriptor){
      return NO;
   }
   return( uFifoCount( LogFifo.Descriptor));
} // LogCount

//------------------------------------------------------------------------------
//  Get
//------------------------------------------------------------------------------

TYesNo LogGet( TLogIndex Index, TLogItem *Item)
// Get log record
{
   if( !LogFifo.Descriptor){
      return NO;
   }
   return( uFifoGet( &LogFifo, Index, Item));
} // LogGet

TYesNo LogFull()
// Return <YES> when capacity of log reached, in that case oldest items are rewriten
{
   if( !LogFifo.Descriptor){
      return NO;
   }
   return uFifoFull( LogFifo.Descriptor);
}
