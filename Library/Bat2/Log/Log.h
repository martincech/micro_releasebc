//******************************************************************************
//
//   Log.h           Log
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Log_H__
   #define __Log_H__

#ifndef __Uni_H_
   #include "Unisys/Uni.h"
#endif

#ifndef __LogDef_H__
   #include "Log/LogDef.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

TYesNo LogRemoteSave( void);
// Save to remote device
#ifdef __cplusplus
   }
#endif

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void LogInit( void);
// Initialize

void LogClear( void);
// Clear all log

TYesNo LogAppend( TLogItem *Item);
// Append log record

TLogIndex LogCount( void);
// Returns log records count

TYesNo LogGet( TLogIndex Index, TLogItem *Item);
// Get log record

TYesNo LogFull();
// Return <YES> when capacity of log reached, in that case oldest items are rewriten

#endif
