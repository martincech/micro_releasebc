//******************************************************************************
//
//   Sample.h      Bat2 samples memory
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Sample_H__
   #define __Sample_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __SampleDef_H__
   #include "Storage/SampleDef.h"
#endif

#ifndef __uTime_H__
   #include "Time/uTime.h"
#endif

#ifndef __uFifo_H__
   #include "Data/uFifo.h"
#endif

typedef UFifo TSamples;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

#ifdef __cplusplus
   extern "C" {
#endif
TYesNo SampleRemoteLoad( void);
// Load from remote device

TYesNo SampleRemoteSave( void);
// Save to remote device
#ifdef __cplusplus
   }
#endif

void SampleInit( void);
// Initialize

TYesNo SampleOpen( TSamples *Samples);
// Open samples

void SampleClose( TSamples *Samples);
// Close samples

void SampleClear( TSamples *Samples);
// Clear samples memory

TYesNo SampleAppend( TSamples *Samples, TSample *Sample);
// Append <Sample>

TYesNo SampleGet( TSamples *Samples, TSampleIndex Index, TSample *Sample);
// Get <Sample> at <Index>

TSampleIndex SampleCount( void);
// Returns samples count

//------------------------------------------------------------------------------
// Get / Set
//------------------------------------------------------------------------------

TWeightGauge SampleWeight( TSample *Sample);
// Returns <Sample> weight

void SampleWeightSet( TSample *Sample, TWeightGauge Weight, TSampleFlag Flag);
// Set <Sample> <Weight>

TSampleFlag SampleFlag( TSample *Sample);
// Returns <Sample> flag

void SampleFlagSet( TSample *Sample, TSampleFlag Flag);
// Set <Sample> <Flag>

UTimeGauge SampleTimestamp( TSample *Sample);
// Returns <Sample> timestamp

TSampleOrigin SampleOrigin( TSample *Sample);
// Returns <Sample> origin

void SampleOriginSet( TSample *Sample, TSampleOrigin Origin);
// Set <Sample> <Origin>

//------------------------------------------------------------------------------

#endif
