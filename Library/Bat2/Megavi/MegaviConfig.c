//
//   MegaviConfig.c    Megavi messaging configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "MegaviConfig.h"
#include "Megavi/MegaviDef.h"
#include "Uart/Uart.h"

TMegaviModuleOptions MegaviOptions;

const TMegaviModuleOptions MegaviOptionsDefault = {
   /* ID */            0x00 | (MEGAVI_MODULE_CODE & 0x0F)
};
