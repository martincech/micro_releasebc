//
//   MegaviConfig.h    Megavi messaging configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MegaviConfig_H__
   #define __MegaviConfig_H__

#ifndef __MegaviConfigDef_H__
   #include "MegaviConfigDef.h"
#endif

extern         TMegaviModuleOptions MegaviOptions;
extern const   TMegaviModuleOptions MegaviOptionsDefault;

#endif // __MegaviConfig_H__
