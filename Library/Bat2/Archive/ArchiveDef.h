//******************************************************************************
//
//   ArchiveDef.h  Bat2 Archive data
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ArchiveDef_H__
   #define __ArchiveDef_H__

#ifndef __Bat2Def_H__
   #include "Config/Bat2Def.h"
#endif

#ifndef __StatisticDef_H__
   #include "Statistic/StatisticDef.h"
#endif

#ifndef __uClock_H__
   #include "Time/uClock.h"
#endif

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define ARCHIVE_INDEX_INVALID ((dword)-1)

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef word  TStatisticIdentifier;
typedef word  TZoneIdentifier;
typedef dword TArchiveIndex;

//------------------------------------------------------------------------------
//  Archive histogram
//------------------------------------------------------------------------------

typedef struct
{
   byte Slot[ HISTOGRAM_SLOTS];                  // Compressed histogram slots
   byte _Dummy;                                  // Dword alignment
} TArchiveHistogram;

//------------------------------------------------------------------------------
//  Archive data
//------------------------------------------------------------------------------

typedef struct
{
   TStatisticIdentifier StatisticCalculate;      // Statistic identifier (record originator)
   TDayNumber           Day;                     // Technological day
   UClockGauge          Timestamp;               // Statistic timestamp
   TStatisticCount      Count;                   // Weight samples count
   TWeightGauge         Average;                 // Average weight
   TWeightGauge         Gain;                    // Daily Gain (may be negative)
   TWeightGauge         Sigma;                   // Weight standard deviaiton
   word                 Uniformity;              // Weight uniformity [0.1%]
   byte                 HourFrom;                // Item Hour
   byte                 HourTo;
   byte                 Sex;                     // Statistic sex
   TArchiveHistogram    Histogram;               // Compressed histogram
} TArchiveData;

//------------------------------------------------------------------------------
//  Archive marker
//------------------------------------------------------------------------------

typedef struct 
{
   TZoneIdentifier Zone;                         // Zone identifier (record identifier)
   TDayNumber      Day;                          // Technological day
   UClockGauge     Timestamp;                    // Marker timestamp
   TWeightGauge    TargetWeight;                 // Target weight
   TWeightGauge    TargetWeightFemale;           // Target weight female
} TArchiveMarker;

//------------------------------------------------------------------------------
//  Archive item
//------------------------------------------------------------------------------

#define ARCHIVE_FLAG_MARKER              0x8000   // statistic/zone identifier for marker
#define _ArchiveMarkerSet( Identifier)   ((Identifier) |  ARCHIVE_FLAG_MARKER)
#define _ArchiveIsMarker( Identifier)    ((Identifier) &  ARCHIVE_FLAG_MARKER)
#define _ArchiveIdentifier( Identifier)  ((Identifier) & ~ARCHIVE_FLAG_MARKER)


typedef union
{
   word           Identifier;
   TArchiveData   Data;
   TArchiveMarker Marker;
} TArchiveItem;

#define ARCHIVE_ITEM_SIZE sizeof( TArchiveItem)

//------------------------------------------------------------------------------
#endif
