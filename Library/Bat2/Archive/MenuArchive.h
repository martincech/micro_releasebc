//******************************************************************************
//                                                                            
//   MenuArchive.h  Bat2 archive viewer
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuArchive_H__
   #define __MenuArchive_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuArchive( void);
// Display archive

#endif


