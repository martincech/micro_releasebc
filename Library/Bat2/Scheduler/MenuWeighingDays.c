//******************************************************************************
//
//   MenuWeighingDays.c  Weighing days menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingDays.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings

#include "Scheduler/WeighingPlan.h"
#include "Scheduler/MenuWeekDays.h"

static DefMenu( WeighingDaysMenu)
   STR_MODE,
   STR_DAYS,
   STR_WEIGHING_DAYS,
   STR_SUSPENDED_DAYS,
   STR_START_DAY,
EndMenu()

typedef enum {
   MI_MODE,
   MI_DAYS,
   MI_WEIGHING_DAYS,
   MI_SUSPENDED_DAYS,
   MI_START_DAY
} EWeighingDaysMenu;

// Local functions :

static void WeighingDaysParameters( int Index, int y, TWeighingDays *Parameters);
// Draw weighing days parameters

//------------------------------------------------------------------------------
//  Menu Weighing Days
//------------------------------------------------------------------------------

void MenuWeighingDays( TWeighingPlan *WeighingPlan)
// Edit weighing days parameters
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      //>>> item enable
      MData.Mask = 0;
      if( WeighingPlan->Days.Mode == WEIGHING_DAYS_MODE_DAY_OF_WEEK){
         MData.Mask = (1 << MI_WEIGHING_DAYS) | (1 << MI_SUSPENDED_DAYS) | (1 << MI_START_DAY);
      } else { // WEIGHING_DAYS_MODE_DAY_PATTERN
         MData.Mask = (1 << MI_DAYS);
      }
      //<<< item enable
      // selection :
      if( !DMenu( STR_WEIGHING_DAYS, WeighingDaysMenu, (TMenuItemCb *)WeighingDaysParameters, &WeighingPlan->Days, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_MODE :
            i = WeighingPlan->Days.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_WEIGHING_DAYS_MODE, _WEIGHING_DAYS_MODE_LAST)){
               break;
            }
            WeighingPlan->Days.Mode = (byte)i;
            break;

         case MI_DAYS :
            MenuWeekDays( &WeighingPlan->Days.Days);
            break;

         case MI_WEIGHING_DAYS :
            i = WeighingPlan->Days.WeighingDays;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_DAYS_MIN, WEIGHING_DAYS_MAX, 0)){
               break;
            }
            WeighingPlan->Days.WeighingDays = (byte)i;
            break;

         case MI_SUSPENDED_DAYS :
            i = WeighingPlan->Days.SuspendedDays;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_DAYS_MIN, WEIGHING_DAYS_MAX, 0)){
               break;
            }
            WeighingPlan->Days.SuspendedDays = (byte)i;
            break;

         case MI_START_DAY :
            i = WeighingPlan->Days.StartDay;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, WEIGHING_DAYS_START_DAY_MIN, WEIGHING_DAYS_START_DAY_MAX, 0)){
               break;
            }
            WeighingPlan->Days.StartDay = (TDayNumber)i;
            break;

      }
   }
} // MenuWeighingDays

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingDaysParameters( int Index, int y, TWeighingDays *Parameters)
// Draw weighing days parameters
{
   switch( Index){
      case MI_MODE :
         DLabelEnum( Parameters->Mode, ENUM_WEIGHING_DAYS_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_DAYS :
         break;

      case MI_WEIGHING_DAYS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->WeighingDays, 0);
         break;

      case MI_SUSPENDED_DAYS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->SuspendedDays, 0);
         break;

      case MI_START_DAY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Parameters->StartDay, 0);
         break;

   }
} // WeighingDaysParameters
