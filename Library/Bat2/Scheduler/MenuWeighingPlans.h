//******************************************************************************
//
//   MenuWeighingPlans.h     Weighing plans menu
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingPlans_H__
#define __MenuWeighingPlans_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __StrDef_H__
   #include "String/StrDef.h"
#endif

#ifndef __WeighingSchedulerDef_H__
   #include "Scheduler/WeighingSchedulerDef.h"
#endif

#ifndef __WeighingPlanList_H__
   #include "Scheduler/WeighingPlanList.h"
#endif

void MenuWeighingPlans( void);
// Menu Weighing plans


TWeighingPlanIndex MenuWeighingPlansSelect( TWeighingPlanList *WeighingPlanList, TUniStr Title, TWeighingPlanIndex Index, TUniStr SpecialItem);
// Select Weighing plan, add <SpecialItem> at end of list

#endif // __MenuWeighingPlans_H__


