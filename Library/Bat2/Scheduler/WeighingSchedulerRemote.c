//******************************************************************************
//
//   WeighingSchedulerRemote.c   Remote weighing scheduler
//   Version 1.0                 (c) VEIT Electronics
//
//******************************************************************************

#include "WeighingScheduler.h"
#include "Action/ActionRemote.h"
#include "Config/Config.h"
#include "Weighing/Weighing.h"
#include "Weighing/WeighingConfiguration.h"
#include "System/System.h"
#include "Menu/MenuExit.h"

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void WeighingSchedulerInit( void)
// Initialize
{
   WeighingInit();
   WeighingPlanListInit();
} // WeighingSchedulerInit

//------------------------------------------------------------------------------
//  Resume
//------------------------------------------------------------------------------

void WeighingSchedulerResume( void)
// Power on initialization
{
   WeighingResume();  
} // WeighingSchedulerResume

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

void WeighingSchedulerExecute( void)
// Executive
{
} // WeighingSchedulerExecute

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void WeighingSchedulerStart( void)
// Start weighing
{
   ConfigRemoteSave();
   WeighingPlanListRemoteSave();
   ContactListRemoteSave();
   PredefinedListRemoteSave();
   CurveListRemoteSave();
   CorrectionListRemoteSave();

   ActionRemoteWeighingSchedulerStart();

   ArchiveRemoteLoad();
   SampleRemoteLoad();
   ContextRemoteLoad();

   ArchiveInit();
   SampleInit();
   ContextLoad();

   WeighingResume();
} // WeighingSchedulerStart

//------------------------------------------------------------------------------
//   Start at
//------------------------------------------------------------------------------

void WeighingSchedulerStartAt( UClockGauge Time)
// Start weighing
{
   ConfigRemoteSave();
   WeighingPlanListRemoteSave();
   ContactListRemoteSave();
   PredefinedListRemoteSave();
   CurveListRemoteSave();
   CorrectionListRemoteSave();

   ActionRemoteWeighingSchedulerStartAt( Time);

   ArchiveRemoteLoad();
   SampleRemoteLoad();
   ContextRemoteLoad();

   ArchiveInit();
   SampleInit();
   ContextLoad();

   WeighingResume();
} // WeighingSchedulerStartAt

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

void WeighingSchedulerStop( void)
// Stop weighing
{
   ActionRemoteWeighingSchedulerStop();

   ArchiveRemoteLoad();
   SampleRemoteLoad();
   ContextRemoteLoad();

   ArchiveInit();
   SampleInit();
   ContextLoad();

   WeighingResume();
} // WeighingSchedulerStop

//------------------------------------------------------------------------------
//  Suspend
//------------------------------------------------------------------------------

void WeighingSchedulerSuspend( void)
// Suspend weighing
{
   ActionRemoteWeighingSchedulerSuspend();

   ArchiveRemoteLoad();
   SampleRemoteLoad();
   ContextRemoteLoad();

   ArchiveInit();
   SampleInit();
   ContextLoad();

   WeighingResume();
} // WeighingSchedulerSuspend

//------------------------------------------------------------------------------
//  Release
//------------------------------------------------------------------------------

void WeighingSchedulerRelease( void)
// Relase weighing
{
   ActionRemoteWeighingSchedulerRelease();

   ArchiveRemoteLoad();
   SampleRemoteLoad();
   ContextRemoteLoad();

   ArchiveInit();
   SampleInit();
   ContextLoad();

   WeighingResume();
} // WeighingSchedulerRelease

//------------------------------------------------------------------------------
//   Calibration
//------------------------------------------------------------------------------

void WeighingSchedulerCalibration( void)
// Start weighing calibration
{
 // unsupported
} // WeighingSchedulerCalibration

//------------------------------------------------------------------------------
//   Diagnostics
//------------------------------------------------------------------------------

void WeighingSchedulerDiagnostics( void)
// Start diagnostic weighing
{
} // WeighingSchedulerDiagnostics

//------------------------------------------------------------------------------
//   Next day
//------------------------------------------------------------------------------

void WeighingSchedulerDayNext( void)
// Next technological day
{
} // WeighingSchedulerDayNext

//------------------------------------------------------------------------------
//  Technological day start
//------------------------------------------------------------------------------

UDateTimeGauge WeighingDayStartDateTime( TDayNumber Day)
// Calculate start date time of technological <Day>
{
   return 0;
} // WeighingDayStartDateTime
