//******************************************************************************
//
//   MenuWeighingDays.h  Weighing days menu
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingDays_H__
   #define __MenuWeighingDays_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeighingSchedulerDef_H__
   #include "Scheduler/WeighingSchedulerDef.h"
#endif

void MenuWeighingDays( TWeighingPlan *WeighingPlan);
// Menu weighing days

#endif
