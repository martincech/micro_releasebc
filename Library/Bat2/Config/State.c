//******************************************************************************
//
//   State.c     Bat2 state
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "State.h"
#include "Memory\File.h"

//------------------------------------------------------------------------------
//  Remote load
//------------------------------------------------------------------------------

TYesNo StateRemoteLoad( void)
// Load from remote device
{
   TFile File;
   if(!FileOpen(&File, FILE_STATE_REMOTE, FILE_MODE_READ_ONLY)) {
      return NO;
   }
   if(!FileLoad(&File, 0, &State, sizeof(State))) {
      return NO;
   }
   FileClose(&File);
   return NO;
}

//------------------------------------------------------------------------------
//  Remote save
//------------------------------------------------------------------------------

TYesNo StateRemoteSave( void)
// Save to remote device
{
   return NO;
}