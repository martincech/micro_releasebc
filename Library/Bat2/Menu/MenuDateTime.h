//******************************************************************************
//
//   MenuDateTime.h      Date time settings
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuDateTime_H__
   #define __MenuDateTime_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuDateTime( void);
// Menu date time

#endif
