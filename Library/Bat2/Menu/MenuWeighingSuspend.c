//******************************************************************************
//
//   MenuWeighingSuspend.c  Weighing Suspend menu
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingSuspend.h"
#include "Gadget/DMsg.h"          // Message box
#include "Str.h"                  // Strings

#include "Scheduler/WeighingScheduler.h"  // Weighing scheduler executive

//------------------------------------------------------------------------------
//  Menu Weighing Suspend
//------------------------------------------------------------------------------

void MenuWeighingSuspend( void)
// Menu weighing suspend
{
   if( !DMsgYesNo( STR_CONFIRMATION, STR_WEIGHING_SUSPEND_CONFIRM, 0)){
      return;
   }
   DMsgWait();
   WeighingSchedulerSuspend();
} // MenuWeighingSuspend
