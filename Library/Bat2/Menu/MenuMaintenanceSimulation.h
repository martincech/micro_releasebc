//******************************************************************************
//
//   MenuMaintenanceSimulation.h  Maintenance simulation menu
//   Version 1.0                  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuMaintenanceSimulation_H__
   #define __MenuMaintenanceSimulation_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuMaintenanceSimulation( void);
// Menu maintenance simulation

#endif
