//******************************************************************************
//
//   MenuTargetWeights.c  Target weights menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuTargetWeights.h"
#include "Graphic/Graphic.h"      // graphic
#include "Console/conio.h"        // Display
#include "Gadget/DLabel.h"        // Display label
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DEdit.h"         // Display edit value
#include "Weight/DWeight.h"       // Display edit weight
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration


static DefMenu( TargetWeightsMenu)
   STR_MODE,
   STR_GROWTH,
   STR_SEX_DIFFERENTIATION,
   STR_SEX,
   STR_ADJUST_TARGET_WEIGHTS,
EndMenu()

typedef enum {
   MI_MODE,
   MI_GROWTH,
   MI_SEX_DIFFERENTIATION,
   MI_SEX,
   MI_ADJUST_TARGET_WEIGHTS
} ETargetWeightsMenu;

// Local functions :

static void WeighingTargetWeightsParameters( int Index, int y, TWeighingTargetWeights *Parameters);
// Draw target weights parameters

//------------------------------------------------------------------------------
//  Menu TargetWeights
//------------------------------------------------------------------------------

void MenuTargetWeights( TWeighingTargetWeights *TargetWeights)
// Edit target weights parameters
{
TMenuData MData;
int       i;


   DMenuClear( MData);
   forever {
      //>>> item enable
      MData.Mask = 0;
      switch( TargetWeights->SexDifferentiation){
         case SEX_DIFFERENTIATION_NO :
            break;

         case SEX_DIFFERENTIATION_YES :
            MData.Mask |= (1 << MI_SEX);
            break;
#ifdef DEBUG
         case SEX_DIFFERENTIATION_STEP_ONLY :
            MData.Mask |= (1 << MI_MODE) | (1 << MI_GROWTH) | (1 << MI_SEX);
            break;
#endif
      }
      switch (TargetWeights->Mode) {
      case PREDICTION_MODE_GROWTH_CURVE:
         MData.Mask |= (1 << MI_GROWTH);
         break;
      }
      //<<< item enable
      // selection :
      if( !DMenu( STR_TARGET_WEIGHTS, TargetWeightsMenu, (TMenuItemCb *)WeighingTargetWeightsParameters, TargetWeights, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_MODE :
            i = TargetWeights->Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_PREDICTION_MODE, _PREDICTION_MODE_LAST)){
               break;
            }
            TargetWeights->Mode = (byte)i;
            if( TargetWeights->Mode == PREDICTION_MODE_AUTOMATIC && TargetWeights->AdjustTargetWeights >= _ADJUST_TARGET_WEIGHTS_AUTO_LAST){
               TargetWeights->AdjustTargetWeights = _ADJUST_TARGET_WEIGHTS_AUTO_LAST - 1;
            }
            break;

         case MI_GROWTH :
            i = TargetWeights->Growth;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_PREDICTION_GROWTH, _PREDICTION_GROWTH_LAST)){
               break;
            }
            TargetWeights->Growth = (byte)i;
            break;

         case MI_SEX_DIFFERENTIATION :
            i = TargetWeights->SexDifferentiation;
#ifdef DEBUG
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_SEX_DIFFERENTIATION, _SEX_DIFFERENTIATION_LAST)){
               break;
            }
#else
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_SEX_DIFFERENTIATION, SEX_DIFFERENTIATION_STEP_ONLY)){
               break;
            }
#endif
            TargetWeights->SexDifferentiation = (byte)i;
            break;

         case MI_SEX :
            i = TargetWeights->Sex;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_SEX, _SEX_LAST)){
               break;
            }
            TargetWeights->Sex = (byte)i;
            break;

         case MI_ADJUST_TARGET_WEIGHTS :
            i = TargetWeights->AdjustTargetWeights;
            if( TargetWeights->Mode == PREDICTION_MODE_AUTOMATIC){
               if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_ADJUST_TARGET_WEIGHTS, _ADJUST_TARGET_WEIGHTS_AUTO_LAST)){
                  break;
               }
            } else {
               if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_ADJUST_TARGET_WEIGHTS, _ADJUST_TARGET_WEIGHTS_LAST)){
                  break;
               }
            }

            TargetWeights->AdjustTargetWeights = (byte)i;
            break;

      }
   }
} // MenuTargetWeights

//******************************************************************************

//------------------------------------------------------------------------------
//  Parameters
//------------------------------------------------------------------------------

static void WeighingTargetWeightsParameters( int Index, int y, TWeighingTargetWeights *Parameters)
// Draw target weights parameters
{
   switch( Index){
      case MI_MODE :
         DLabelEnum( Parameters->Mode, ENUM_PREDICTION_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_GROWTH :
         DLabelEnum( Parameters->Growth, ENUM_PREDICTION_GROWTH, DMENU_PARAMETERS_X, y);
         break;

      case MI_SEX_DIFFERENTIATION :
         DLabelEnum( Parameters->SexDifferentiation, ENUM_SEX_DIFFERENTIATION, DMENU_PARAMETERS_X, y);
         break;

      case MI_SEX :
         DLabelEnum( Parameters->Sex, ENUM_SEX, DMENU_PARAMETERS_X, y);
         break;

      case MI_ADJUST_TARGET_WEIGHTS :
         DLabelEnum( Parameters->AdjustTargetWeights, ENUM_ADJUST_TARGET_WEIGHTS, DMENU_PARAMETERS_X, y);
         break;

   }
} // WeighingTargetWeightsParameters
