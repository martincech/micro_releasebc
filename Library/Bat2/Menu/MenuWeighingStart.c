//******************************************************************************
//
//   MenuWeighingStart.c  Weighing Start menu
//   Version 1.0          (c) VEIT Electronics
//
//******************************************************************************

#include "MenuWeighingStart.h"
#include "Gadget/DMsg.h"          // Message box
#include "Str.h"                  // Strings
#include "Config/Config.h"        // Project configuration

#include "Weighing/WeighingConfiguration.h"  // Weighing configuration data
#include "Menu/MenuWeighingConfiguration.h"  // Weighing configuration
#include "Predefined/PredefinedList.h"

//------------------------------------------------------------------------------
//  Menu Weighing Start
//------------------------------------------------------------------------------

void MenuWeighingStart( void)
// Menu weighing start
{
TPredefinedList PredefinedList;
   if(!PredefinedListOpen( &PredefinedList)) {
      return;
   }
   // run Weighing configuration menu :
   MenuWeighingConfiguration( STR_WEIGHING, &WeighingConfiguration, NO, &PredefinedList);
   PredefinedListClose(&PredefinedList);
} // MenuWeighingStart
