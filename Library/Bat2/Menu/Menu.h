//******************************************************************************
//                                                                            
//   Menu.h         Bat2 menu
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#ifndef __MenuExit_H__
   #include "Menu/MenuExit.h"
#endif

typedef struct {
   TYesNo SwitchedOff;
} TMenuContext;

extern       TMenuContext    MenuContext;
extern const TMenuContext    MenuContextDefault;

void MenuExecute( void);
// Main menu loop

#endif
