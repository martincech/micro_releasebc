//******************************************************************************
//
//   MenuWeighingTargetAdjust.h     Adjust target weights during weighing menu
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuWeighingTargetAdjust_H__
#define __MenuWeighingTargetAdjust_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuWeighingTargetAdjust( void);
// Menu weighing target adjustment


#endif // __MenuWeighingTargetAdjust_H__


