//******************************************************************************
//
//   MenuStatistics.c  Statistics menu
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#include "MenuStatistics.h"
#include "Gadget/DMenu.h"         // Display menu
#include "Gadget/DMsg.h"          // Message box
#include "Str.h"                  // Strings

#include "Archive/MenuArchive.h"
#include "Storage/MenuSample.h"
#include "Graph/MenuGraph.h"

#ifdef __GSM_SMS_STATISTICS__
#include "MenuSmsStatistics.h"
#endif
static DefMenu( StatisticsMenu)
   STR_ARCHIVE,
   STR_GRAPHS,
#ifdef __GSM_SMS_STATISTICS__
   STR_GSM,
#endif
   STR_TODAYS_WEIGHINGS,
EndMenu()

typedef enum {
  MI_ARCHIVE,
   MI_GRAPHS,
#ifdef __GSM_SMS_STATISTICS__
   MI_GSM,
#endif
   MI_TODAYS_WEIGHINGS
} EStatisticsMenu;

//------------------------------------------------------------------------------
//  Menu Statistics
//------------------------------------------------------------------------------

void MenuStatistics( void)
// Menu statistics
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      MData.Mask |= (1 << MI_GRAPHS);   // graphs always disabled
      // selection :
      if( !DMenu( STR_STATISTICS, StatisticsMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MI_ARCHIVE :
            MenuArchive();
            break;

         case MI_GRAPHS :
            MenuGraph();
            break;
#ifdef __GSM_SMS_STATISTICS__
         case MI_GSM :
            MenuSmsStatistics();
            break;
#endif

         case MI_TODAYS_WEIGHINGS :
            MenuSample();
            break;
      }
   }
} // MenuStatistics
