//******************************************************************************
//
//   MenuRs485Interface.c  Rs485 interface 1 menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "MenuRs485Interface.h"
#include "Rs485/Rs485Config.h"
#include "Rs485/Rs485.h"
#include "Gadget/DMenu.h"                 // Display menu
#include "Gadget/DEdit.h"                 // Edit box
#include "Gadget/DLabel.h"                // Labels
#include "Modbus/ModbusConfig.h"          // Modbus configuration
#include "Megavi/MegaviConfig.h"          // Megavi configuration
#include "Dacs/DacsConfig.h"              // Dacs configuration
#include "Config/Config.h"                // Configuration
#include "mb.h"
#include "Str.h"                          // Strings

static DefMenu( Rs485InterfaceMenu)
   STR_ENABLED,
   STR_MODE,
   STR_ADDRESS,
   STR_MODULE_CODE,
   STR_PROTOCOL,
   STR_ADDRESS,
   STR_BAUD_RATE,
   STR_PARITY,
   STR_REPLY_DELAY,
   STR_DACS_VERSION,
   STR_ADDRESS,
EndMenu()



typedef enum {
   /* RS485 general settings */
   MI_ENABLED,
   MI_MODE,
   /* Megavi settings */
   MI_MODULE_ADDRESS,
   MI_MODULE_CODE,
   /* Modbus settings */
   MI_PROTOCOL,
   MI_ADDRESS,
   MI_BAUD_RATE,
   MI_PARITY,
   /* Dacs settings */
   MI_DACS_VERSION,
   MI_DACS_ADDRESS,
   _MI_LAST
} ERs485InterfaceMenu;

typedef struct STemporaryData
{
   TMegaviModuleOptions *mgOptions;
   TModbusModuleOptions *mbOptions;
   TDacsModuleOptions   *dcOptions;
   TRs485Options        *rsOptions;
} TRS485Data;

// Local functions :

static void Rs485Parameters( int Index, int y, TRS485Data *Interface);
// Draw option parameters
static int ModbusModeToIndex(TRS485Data *Interface);
// Get index to RTU/ASCII text from modbus mode
static eMBMode IndexToModbusMode(TRS485Data *Interface, int index);
// get modbus mode from index
static int GetAvailableModes(ERs485Mode **availableModes, byte currentInterface);
// get modes of operation of RS485 which are not selected on other interfaces
//------------------------------------------------------------------------------
//  Menu Rs485Interface
//------------------------------------------------------------------------------

void MenuRs485Interface( byte Interface)
// Menu rs485 interface
{
TMenuData MData;
dword i, j;
TUniStr EditList[_RS485_MODE_LAST];
ERs485Mode availModes[_RS485_MODE_LAST];
int availModesCount;
TMegaviModuleOptions mgOptions;
TModbusModuleOptions mbOptions;
TDacsModuleOptions   dcOptions;
TRs485Options        rsOptions;
TRS485Data           rsInterface = {
   &mgOptions,
   &mbOptions,
   &dcOptions,
   &rsOptions
};

   memcpy(&mgOptions, &MegaviOptions, sizeof(TMegaviModuleOptions));
   memcpy(&mbOptions, &ModbusOptions, sizeof(TModbusModuleOptions));
   memcpy(&dcOptions, &DacsOptions, sizeof(TDacsModuleOptions));
   memcpy(&rsOptions, &Rs485Options[Interface], sizeof(TRs485Options));

   availModesCount = GetAvailableModes(&availModes, Interface);

   DMenuClear( MData);
   forever {
      //>>> item enable
      MData.Mask = ~(1 << MI_ENABLED);

      if( rsInterface.rsOptions->Enabled){
         MData.Mask &= ~(1 << MI_MODE);
         switch (rsInterface.rsOptions->Mode){
            case RS485_MODE_MEGAVI:
               for(i = MI_MODULE_ADDRESS; i <= MI_MODULE_ADDRESS; i++){
                  MData.Mask &= ~(1 << i);
               }
               break;

            case RS485_MODE_SENSOR_PACK:
            case RS485_MODE_MODBUS:
               for(i = MI_PROTOCOL; i <= MI_PARITY; i++){
                  MData.Mask &= ~(1 << i);
               }
               break;

            case RS485_MODE_DACS :
               for(i = MI_DACS_ADDRESS; i <= MI_DACS_ADDRESS; i++){
                  MData.Mask &= ~(1 << i);
               }
               break;
            default:
               for(i = MI_MODE; i < _MI_LAST; i++){
                  MData.Mask |= (1 << i);
               }
               break;

         }
      }
      //<<< item enable

      // selection :
      int captionIndex = STR_INTERFACE_1 + Interface;
      if ((BAT2_HAS_RS485_I1_MODULE(Bat2Version.Modification) && !BAT2_HAS_RS485_I2_MODULE(Bat2Version.Modification)) ||
          (!BAT2_HAS_RS485_I1_MODULE(Bat2Version.Modification) && BAT2_HAS_RS485_I2_MODULE(Bat2Version.Modification))) {
         captionIndex = STR_RS485;   // change caption index when device has only one rs485 interface
      }

      if( !DMenu( captionIndex, Rs485InterfaceMenu, (TMenuItemCb *) Rs485Parameters, &rsInterface, &MData)){
         TYesNo somethingChanged = NO;
         if(!memequ(rsInterface.mgOptions, &MegaviOptions, sizeof(TMegaviModuleOptions))){
            memcpy(&MegaviOptions, rsInterface.mgOptions, sizeof(TMegaviModuleOptions));
            ConfigMegaviOptionsSave();
            // does the mode changed from or to this option?
            if(rsInterface.rsOptions->Mode == RS485_MODE_MEGAVI || Rs485Options[Interface].Mode == RS485_MODE_MEGAVI){
               somethingChanged = YES;
            }
         }
         if(!memequ(rsInterface.mbOptions, &ModbusOptions, sizeof(TModbusModuleOptions))){
            memcpy(&ModbusOptions, rsInterface.mbOptions, sizeof(TModbusModuleOptions));
            ConfigModbusOptionsSave();
            // does the mode changed from or to this option?
            if(rsInterface.rsOptions->Mode == RS485_MODE_MODBUS|| Rs485Options[Interface].Mode == RS485_MODE_MODBUS ||
               rsInterface.rsOptions->Mode == RS485_MODE_SENSOR_PACK|| Rs485Options[Interface].Mode == RS485_MODE_SENSOR_PACK){
               somethingChanged = YES;
            }
         }
         if(!memequ(rsInterface.dcOptions, &DacsOptions, sizeof(TDacsModuleOptions))){
            memcpy(&DacsOptions, rsInterface.dcOptions, sizeof(TDacsModuleOptions));
            ConfigDacsOptionsSave();
            // does the mode changed from or to this option?
            if(rsInterface.rsOptions->Mode == RS485_MODE_DACS || Rs485Options[Interface].Mode == RS485_MODE_DACS){
               somethingChanged = YES;
            }
         }

         if(!memequ(rsInterface.rsOptions, &Rs485Options[Interface], sizeof(TRs485Options))){
            somethingChanged = YES;
         }
         if(somethingChanged){
            Rs485FreeOne(Interface);
         }
         if(!memequ(rsInterface.rsOptions, &Rs485Options[Interface], sizeof(TRs485Options))){
            memcpy(&Rs485Options[Interface], rsInterface.rsOptions, sizeof(TRs485Options));
            ConfigRs485OptionsSave();
         }
         if(somethingChanged && rsInterface.rsOptions->Enabled){
            Rs485InitOne(Interface);
         }
         return;
      }
      switch( MData.Item){
         case MI_ENABLED :
            if(availModesCount == 0){
               break;
            }
            i = rsInterface.rsOptions->Enabled;
            if( !DEditYesNo( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            if( i == rsInterface.rsOptions->Enabled){
               //same option as before
               break;
            }

            if( i){
               // Yes
               // check whether current selected mode is still available(not selected on other interfaces)
               TYesNo contains = NO;
               for(j = 0; j < availModesCount; j++){
                  if(availModes[j] == rsInterface.rsOptions->Mode){
                     contains = YES;
                     break;
                  }
               }
               if(!contains){
                  rsInterface.rsOptions->Mode = availModes[0];
               }
            }

            rsInterface.rsOptions->Enabled = (TYesNo) i;
            break;

         case MI_MODE :
            EditList[0] = ENUM_RS485_MODE + rsInterface.rsOptions->Mode;
            i = 1;
            for(j = 0; j < availModesCount; j++){
               if(availModes[j] != rsInterface.rsOptions->Mode){
                  EditList[i++] = ENUM_RS485_MODE + availModes[j];
               }
            }
            EditList[availModesCount] = '\0';
            i = 0;
            if(!DEditList(DMENU_EDIT_X, MData.y, &i, EditList)){
                break;
            }
            if(EditList[i] - ENUM_RS485_MODE == rsInterface.rsOptions->Mode){
               break;
            }
            rsInterface.rsOptions->Mode = EditList[i] - (int)ENUM_RS485_MODE;
            if(rsInterface.rsOptions->Mode == RS485_MODE_MODBUS || rsInterface.rsOptions->Mode == RS485_MODE_SENSOR_PACK){
               rsInterface.mbOptions->Mode = IndexToModbusMode(&rsInterface, ModbusModeToIndex(&rsInterface));
            }
            break;

         case MI_MODULE_ADDRESS :
            i = rsInterface.mgOptions->ID >> 4;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, MEGAVI_MEGAVI_MODULE_ADDRESS_MIN, MEGAVI_MEGAVI_MODULE_ADDRESS_MAX, 0)){
               break;
            }
            rsInterface.mgOptions->ID &= 0x0F;
            rsInterface.mgOptions->ID |= (byte)i << 4;
            break;


         case MI_MODULE_CODE :
            i = rsInterface.mgOptions->ID & 0x0F;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, MEGAVI_MEGAVI_MODULE_CODE_MIN, MEGAVI_MEGAVI_MODULE_CODE_MAX, 0)){
               break;
            }
            rsInterface.mgOptions->ID &= 0xF0;
            rsInterface.mgOptions->ID |= (byte)i & 0x0F;
            break;

         case MI_PROTOCOL :
            i = ModbusModeToIndex(&rsInterface);
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_MODBUS_PROTOCOL, 2)){
               break;
            }

            rsInterface.mbOptions->Mode = IndexToModbusMode(&rsInterface, i);
            break;

         case MI_ADDRESS :
            i = rsInterface.mbOptions->Address;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, MB_ADDRESS_MIN, MB_ADDRESS_MAX, 0)){
               break;
            }
            rsInterface.mbOptions->Address = (byte)i;
            break;

         case MI_BAUD_RATE :
            i = rsInterface.mbOptions->BaudRate;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, 1200, 115200, 0)){
               break;
            }
            rsInterface.mbOptions->BaudRate = (dword)i;
            break;

         case MI_PARITY :
            i = rsInterface.mbOptions->Parity;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_MODBUS_PARITY, MB_PAR_EVEN + 1)){
               break;
            }
            rsInterface.mbOptions->Parity = (eMBParity) i;
            break;

         case MI_DACS_VERSION :
            i = rsInterface.dcOptions->Version;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, ENUM_DACS_VERSION, _DACS_VERSION_LAST)){
               break;
            }
            if( rsInterface.dcOptions->Version == (EDacsVersion) i){
               break;
            }
            rsInterface.dcOptions->Version = (EDacsVersion) i;
            break;

         case MI_DACS_ADDRESS :
            i = rsInterface.dcOptions->Address;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, DACS_ADDRESS_MIN, DACS_ADDRESS_MAX, 0)){
               break;
            }
            if(rsInterface.dcOptions->Address == (byte) i){
               break;
            }
            rsInterface.dcOptions->Address = (byte) i;

         default :
            break;
      }
   }
} // MenuRs485Interface


static void Rs485Parameters( int Index, int y, TRS485Data *rsInterface)
// Draw megavi mode options parameters
{
   switch( Index){
      case MI_ENABLED :
         DLabelEnum( rsInterface->rsOptions->Enabled, ENUM_YES_NO, DMENU_PARAMETERS_X, y);
         break;

      case MI_MODE :
         DLabelEnum( rsInterface->rsOptions->Mode, ENUM_RS485_MODE, DMENU_PARAMETERS_X, y);
         break;

      case MI_MODULE_ADDRESS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", rsInterface->mgOptions->ID >> 4);
         break;

      case MI_MODULE_CODE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", rsInterface->mgOptions->ID & 0x0F);
         break;

      case MI_PROTOCOL :
         Index = ModbusModeToIndex(rsInterface);
         DLabelEnum( Index, ENUM_MODBUS_PROTOCOL, DMENU_PARAMETERS_X, y);
         break;

      case MI_ADDRESS :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", rsInterface->mbOptions->Address);
         break;

      case MI_BAUD_RATE :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", rsInterface->mbOptions->BaudRate);
         break;

      case MI_PARITY :
         DLabelEnum( rsInterface->mbOptions->Parity, ENUM_MODBUS_PARITY, DMENU_PARAMETERS_X, y);
         break;

      case MI_DACS_VERSION :
         DLabelEnum( rsInterface->dcOptions->Version, ENUM_DACS_VERSION, DMENU_PARAMETERS_X, y);
         break;

      case MI_DACS_ADDRESS:
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", rsInterface->dcOptions->Address);
         break;

   }
}

static int ModbusModeToIndex(TRS485Data *rsInterface)
{
int Index = 0;

   assert((STR_ASCII - STR_RTU) == 1);
   switch(rsInterface->mbOptions->Mode & ~MB_MASTER){
      case MB_RTU:
         Index = 0;
         break;
      case MB_ASCII:
         Index = 1;
         break;
   }
   return Index;
}

static eMBMode IndexToModbusMode(TRS485Data *rsInterface, int index)
{
   if (index == 1){
      return (rsInterface->rsOptions->Mode == RS485_MODE_SENSOR_PACK) ? MB_ASCII_MASTER : MB_ASCII;
   }
   else{
      return (rsInterface->rsOptions->Mode == RS485_MODE_SENSOR_PACK) ? MB_RTU_MASTER : MB_RTU;
   }
}


static int GetAvailableModes(ERs485Mode **availableModes, byte currentInterface)
// get modes of operation of RS485 which are not selected on other interfaces
{
int i,j,k;
   k = 0;
   for(i = 0; i < _RS485_MODE_LAST; i++){
      availableModes[k++] = i;
      for(j = 0; j < RS485_INTERFACE_COUNT; j++){
         if( j != currentInterface &&        // not current interface
              Rs485Options[j].Enabled &&     // is enabled
             (Rs485Options[j].Mode == i ||   // mode is selected or for modbus only 1 type (master or slave, stack currently doesn't support common functionality)
              Rs485Options[j].Mode == RS485_MODE_MODBUS && i == RS485_MODE_SENSOR_PACK ||
              Rs485Options[j].Mode == RS485_MODE_SENSOR_PACK && i == RS485_MODE_MODBUS)
           ){
            // not available, selected on other RS interface
            k--;
         }
      }
   }
   return k;
}
