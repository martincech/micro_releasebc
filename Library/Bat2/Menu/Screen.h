//******************************************************************************
//
//   Screen.h     Basic screen drawings
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Screen_H__
   #define __Screen_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void ScreenChangeAndRedrawBySex( ESex Sex, TYesNo Force);
// Redraw screen, force to redraw, set sex to show

void ScreenChangeAndRedraw( EKeyboardKey LastKey, TYesNo Force);
// Redraw screen, force to redraw, send last key to switch screen

void ScreenRedraw( TYesNo Force);
// Redraw screen, force to redraw

void ScreenPowerFailure( void);
// Display power failure

void ScreenLogo( void);
// Display logo

void ScreenCharger( void);
// Display charger

#endif
