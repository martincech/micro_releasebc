//******************************************************************************
//
//   MenuConfig.h     Menu permanent storage configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "Menu.h"

TMenuContext MenuContext;

const TMenuContext MenuContextDefault = {
   /* SwitchedOff */    YES
};


