//******************************************************************************
//                                                                            
//   Password.h     Password checking
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Password_H__
   #define __Password_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

TYesNo PasswordCheck( void);
// Password checking, returns YES on passed

TYesNo PasswordInactive( void);
// Password is inactive

void PasswordEdit( void);
// Edit a new password

void PasswordInactivate( void);
// Inactivate password checking

void PasswordClear( void);
// Clear password trials

#endif
