//******************************************************************************
//
//   MenuDef.h  Bat2  menu
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuDef_H__
   #define __MenuDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif


//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define MAIN_MAINTENANCE_SCALE_NAME_SIZE 15
#define MAIN_MAINTENANCE_SCALE_NAME_DEFAULT "BAT2"

#define CONFIGURATION_WEIGHING_DEFAULT_WEIGHING_MIN 0
#define CONFIGURATION_WEIGHING_DEFAULT_WEIGHING_MAX 0
#define CONFIGURATION_WEIGHING_DEFAULT_WEIGHING_DEFAULT 0xFF




//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef byte TWeighingIndex;

//------------------------------------------------------------------------------
//  Configuration weighing
//------------------------------------------------------------------------------

typedef struct {
   TWeighingIndex DefaultWeighing; // Predefined weighing index
} TConfigurationWeighing;



//------------------------------------------------------------------------------
#endif
