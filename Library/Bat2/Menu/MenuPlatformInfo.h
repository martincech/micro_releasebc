//******************************************************************************
//
//   MenuPlatformInfo.h  Platform info screen
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuPlatformInfo_H__
   #define __MenuPlatformInfo_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuPlatformInfo( void);
// Menu platform info

#endif
