//******************************************************************************
//
//   MenuStatistics.h  Statistics menu
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuStatistics_H__
   #define __MenuStatistics_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuStatistics( void);
// Menu statistics

#endif
