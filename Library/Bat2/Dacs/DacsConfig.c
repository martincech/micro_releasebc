//******************************************************************************
//
//   DacsConfig.h     Dacs module configuration
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "DacsConfig.h"
#include "Uart/Uart.h"
#include "Dacs/DacsDef.h"

TDacsModuleOptions DacsOptions;

const TDacsModuleOptions DacsOptionsDefault = {
   /* Version */        DACS_VERSION_6,
   /* Address */        DACS_ADDRESS_DEFAULT
};

