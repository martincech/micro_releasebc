//*****************************************************************************
//
//    BacklightPwm.c Backlight PWM control
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#include "BacklightPwm.h"
#include "Cpu/Pwm.h"                        // PWM control
#include "Display/DisplayConfiguration.h"   // Display configuration

// Local functions :

static unsigned PwmPercent( int Intensity);
// PWM duty cycle by backlight <Intensity>

//------------------------------------------------------------------------------
//  Initialize
//------------------------------------------------------------------------------

void BacklightPwmInit( void)
// Initialize backlight
{
   PwmInit( BACKLIGHT_PWM_FREQUENCY);
   PwmStop( BACKLIGHT_CHANNEL);
} // BacklightPwmInit

//------------------------------------------------------------------------------
//  Backlight on
//------------------------------------------------------------------------------

void BacklightPwmOn( int Intensity)
// Switch backlight on with <Intensity>
{
   if( Intensity == 0){
      BacklightPwmOff();
      return;
   }
   PwmDutySet(BACKLIGHT_CHANNEL, PwmPercent( Intensity));
   PwmStart( BACKLIGHT_CHANNEL);
} // BacklightPwmOn

//------------------------------------------------------------------------------
//  Backlight off
//------------------------------------------------------------------------------

void BacklightPwmOff( void)
// Switch backlight off
{
   PwmStop( BACKLIGHT_CHANNEL);
} // BacklightPwmOff

//------------------------------------------------------------------------------
//  Percent
//------------------------------------------------------------------------------

static const byte Percent[ BACKLIGHT_INTENSITY_MAX + 1] =
{ 0,38,40,43,47,50,60,70,80,100};

static unsigned PwmPercent( int Intensity)
// PWM duty cycle by backlight intensity
{
   return( (unsigned)Percent[ Intensity]);
} // PwmPercent
