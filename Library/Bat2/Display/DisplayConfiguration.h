//*****************************************************************************
//
//    DisplayConfiguration.h  Display configuration
//    Version 1.0             (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __DisplayConfiguration_H__
  #define __DisplayConfiguration_H__

#ifndef __DisplayConfigurationDef_H__
   #include "Display/DisplayConfigurationDef.h"
#endif   

extern       TDisplayConfiguration DisplayConfiguration;
extern const TDisplayConfiguration DisplayConfigurationDefault;

#endif
