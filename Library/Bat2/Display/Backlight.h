//*****************************************************************************
//
//    Backlight.h   Backlight functions
//    Version 1.1  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Backlight_H__
   #define __Backlight_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void BacklightInit( void);
// Initialisation

void BacklightStart( void);
// Set normal mode

void BacklightOn( void);
// Conditionaly on

void BacklightOff( void);
// Conditionaly off

void BacklighStop( void);
// Stop backlight

TYesNo BacklightIsOn( void);
// Check state

void BacklightTest( int Intensity);
// Test backlight intensity

void BacklightTimer( void);
// Backlight timer tick

#endif
