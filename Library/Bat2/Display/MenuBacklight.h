//******************************************************************************
//                                                                            
//   MenuBacklight.h   Backlight menu
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuBacklight_H__
   #define __MenuBacklight_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuBacklight( void);
// Backlight menu

#endif
