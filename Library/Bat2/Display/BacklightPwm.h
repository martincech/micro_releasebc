//*****************************************************************************
//
//    BacklightPwm.h  Backlight PWM control
//    Version 1.0     (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __BacklightPwm_H__
   #define __BacklightPwm_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void BacklightPwmInit( void);
// Initialize backlight

void BacklightPwmOn( int Intensity);
// Switch backlight on with <Intensity>

void BacklightPwmOff( void);
// Switch backlight off

#endif
