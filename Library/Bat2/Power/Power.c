//*****************************************************************************
//
//    Power.c      Power management
//    Version 1.1  (c) VEIT Electronics
//
//*****************************************************************************

#include "Power.h"
#include "System/System.h"
#include "Display/Backlight.h"
#include "Accu/Accu.h"
#include "Charger/Charger.h"
#include "Sleep/Sleep.h"
#include "Kbd/Kbd.h"

TYesNo UsbHostEnabled;

// Internal state
static struct {
   TYesNo SwitchedOnByUser;
   TYesNo PowerExternal;
   TYesNo PowerFailure;
   TYesNo EsdEvent;
} PowerStatus;

// edges/states of power
typedef enum {
   EDGE_RISING,
   EDGE_FALLING,
   LEVEL_LOW,
   LEVEL_HIGH,
   EDGE_NONE
} EEdge;

// internal variables
static TYesNo Init;

// Local functions :
static byte _ExternalVoltage( byte Interrupt);
// External voltage status

static TYesNo UserRequestedOn( void);
// Check if user has switched the device on, value valid only briefly after startup

//------------------------------------------------------------------------------
//  Initialization
//------------------------------------------------------------------------------

void PowerInit( void)
// Initialize
{
   PMC->LVDSC1 = PMC_LVDSC1_LVDV(1); // LVD high range
   PMC->LVDSC2 = PMC_LVDSC2_LVWV(3); // Hgh trip point
   PowerStatus.SwitchedOnByUser = UserRequestedOn();
   PowerPortInit();
   AccuInit();
   ChargerInit();
   SysDelay(100);
   
#warning Pouziti Init - nic moc
   Init = YES;
   PowerExecute();
   Init = NO;

   #warning Slo by realizovat LLWU filtrem (k dispozici jen 2)
   PowerStatus.EsdEvent = NO;
   if(RCM->SRS0 & RCM_SRS0_WAKEUP_MASK) {
      if(!PowerSwitchedOnByUser() && !PowerExternal()) {
         PowerStatus.EsdEvent = YES;
      }
   }
   Power.Accu.Present = YES;
} // PowerInit

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

static void BatteryPresent( TYesNo Present) {
   Power.Accu.Present = Present;
}

void PowerExecute( void)
// Power task
{
   #warning Stacilo by volat mene casteji
   int8 AccuTemp = AccuTemperature();
   int16 AccuVolt = AccuVoltage();

   ChargerTemperatureSet( AccuTemp);
   ChargerExecute();
   Power.Accu.CapacityRemaining = AccuCapacityRemaining();
   PowerStatus.PowerExternal = YES;
   Power.ChargerStatus = ChargerStatus();
} // PowerExecute

//------------------------------------------------------------------------------
//  Switched on by user
//------------------------------------------------------------------------------

TYesNo PowerSwitchedOnByUser( void)
// Power up reason
{
   return PowerStatus.SwitchedOnByUser;
} // PowerSwitchedOnByUser


//------------------------------------------------------------------------------
//  External
//------------------------------------------------------------------------------

TYesNo PowerExternal( void)
// External power present
{
   return PowerStatus.PowerExternal;
} // PowerExternal

//------------------------------------------------------------------------------
//  Failure
//------------------------------------------------------------------------------

TYesNo PowerFailure( void)
// Check for power failure
{
byte Capacity;
#warning PowerFailure cykluje
   PMC->LVDSC2 |= PMC_LVDSC2_LVWACK_MASK;
   if(PMC->LVDSC2 & PMC_LVDSC2_LVWF_MASK) {
      return YES;
   }
   if(PowerExternal()) {
      return NO;
   }
   if(Power.Accu.CapacityRemaining == 0) {
      return YES;
   }
   return NO;
} // PowerFailure

//------------------------------------------------------------------------------
//  Esd event
//------------------------------------------------------------------------------

TYesNo PowerEsdEvent( void)
// Check for esd event
{
   return PowerStatus.EsdEvent;
} // PowerEsdEvent


void PowerOff( void) {
   SleepDeep();
}

void ChargerInputEvent( void)
// Input event callback
{}

//******************************************************************************

//------------------------------------------------------------------------------
//  User requested on
//------------------------------------------------------------------------------

#include "Kbd/Kbx.h"         // Keyboard matrix

static TYesNo UserRequestedOn( void)
// Check if user has switched the device on, value valid only briefly after startup
{
   KbxInit();

   if(KbdPowerUpKey() != K_POWER_ON_OFF) {
      return NO;
   }

   return YES;
} // UserRequestedOn