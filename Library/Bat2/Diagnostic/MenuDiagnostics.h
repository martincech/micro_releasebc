//******************************************************************************
//                                                                            
//   MenuDiagnostics.c      Diagnostics
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#ifndef __MenuDiagnostics_H__
   #define __MenuDiagnostics_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuDiagnostics( void);
// Menu diagnostics

#endif
