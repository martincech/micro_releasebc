//******************************************************************************
//
//   String.c     String
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include <ctype.h>

char *strupr( char *s)
// String to upper
{
   while(*s != '\0') {
      *s = toupper(*s);
      s++;
   }
} // strupr