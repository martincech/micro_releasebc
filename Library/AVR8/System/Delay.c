//******************************************************************************
//
//    Delay.c      System delay loops
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Unisys/Uni.h"
#include "Hardware.h"

//------------------------------------------------------------------------------
//   Delay ms
//------------------------------------------------------------------------------

#define DELAY_COUNT   (word)(F_CPU / 4000 - 1)

void SysDelay( word ms)
// Milisecond delay
{
word cnt;

   __asm__ __volatile__( "\n"
               "L_dl1%=:" "\n\t"
                  "mov %A0, %A2" "\n\t"
                  "mov %B0, %B2" "\n"
               "L_dl2%=:" "\n\t"
                  "sbiw %A0, 1" "\n\t"
                  "brne L_dl2%=" "\n\t"
                  "sbiw %A1, 1" "\n\t"
                  "brne L_dl1%=" "\n\t"
                  : "=&w" (cnt)
                  : "w" (ms), "r" (DELAY_COUNT)
   );
} // SysDelay

//------------------------------------------------------------------------------
//   Delay us
//------------------------------------------------------------------------------

void SysUDelay( word us)
// Microsecond delay
{
   #if ( F_CPU <= 3000000)
	__asm__ __volatile__ (
		"L_ud1%=:\n\t"
         "dec %0" "\n\t"
         "brne L_ud1%="
		: "=r" (us)
		: "0" (us)
	);
   #elif (F_CPU <= 4000000)
	__asm__ __volatile__ (
		"L_ud1%=:\n\t"
         "nop\n\t"
         "dec %0" "\n\t"
         "brne L_ud1%="
		: "=r" (us)
		: "0" (us)
	);
   #elif (F_CPU <= 8000000)
	__asm__ __volatile__ (
		"L_ud1%=:\n\t"
         "nop\n\t"
         "nop\n\t"
         "nop\n\t"
         "nop\n\t"
         "nop\n\t"
         "dec %0" "\n\t"
         "brne L_ud1%="
		: "=r" (us)
		: "0" (us)
	);
   #elif (F_CPU <= 12000000)
   __asm__ __volatile__ (
   "L_ud1%=:\n\t"
   "nop\n\t"
   "nop\n\t"
   "nop\n\t"
   "nop\n\t"
   "nop\n\t"
   "nop\n\t"
   "nop\n\t"
   "nop\n\t"
   "nop\n\t"
   "dec %0" "\n\t"
   "brne L_ud1%="
   : "=r" (us)
   : "0" (us)
   );
   #else // 8-16 MHz
      #error "SysUDelay not implemented yet"
   #endif
} // SysUDelay
