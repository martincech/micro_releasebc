//*****************************************************************************
//
//    Delays.h     AVR timing utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Delays_H__
   #define __Delays_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// <cycles> je pocet cyklu uzivatelskeho kodu uvnitr smycky
// <loop_cycles> pocet cylku je rezie vnitrni smycky
// <in_cycles> je pocet cyklu uzivatelskeho kodu uvnitr smycky
// <out_cycles> je pocet cyklu uzivatelskeho kodu vne smycky
// <us> je zpozdeni v mikrosekundach
// <ms> je zpozdeni v milisekundach
// typ smycky a ridici promenna urcuji rezii smycky <loop_cycles>

// Namet na upgrade : pro zahrnuti vnejsi smycky
// chybi funkce DelayCount( us, in_cycles, out_cycles) =
// ( us * F/1000000 - out_cycles) / (in_cycles + loop_cycles)
// zrejme budou potreba 2 makra :
// 1. pocet vnitrnich cyklu
// 2. doplnujici zpozdeni ve vnejsi smycce pro dorovnani
// poctu cyklu mensiho nez <in_cycles> + <loop_cycles>
// (jedna iterace vnitrni smycky)

//-----------------------------------------------------------------------------
// Mezni hodnoty smycek
//-----------------------------------------------------------------------------

// max. zpozdeni [s] = max.pocet iteraci * (cycles + loop_cycles) / F_CPU

// max. us ve smycce byte preincrement
#define BDELAY_MAX( cycles)    ( (256 * ( (cycles) + 3) * 1000L) / (F_CPU / 1000))
// min. us ve smycce byte preincrement
#define BDELAY_MIN( cycles)    ( (1   * ( (cycles) + 3) * 1000L) / (F_CPU / 1000))

// max. us ve smycce word preincrement
#define WDELAY_MAX( cycles)    ( (65536 * ( (cycles) + 4) * 10L) / (F_CPU / 100000))
// min. us ve smycce byte preincrement
#define WDELAY_MIN( cycles)    ( (1     * ( (cycles) + 4) * 1000L) / (F_CPU / 1000))


//-----------------------------------------------------------------------------
// Makra na vypocet zpozdeni smycky
//-----------------------------------------------------------------------------

// byte preincrement [do while(--i)]
#define BDelayCount( us, cycles)    ( ( (us) * F_CPU) / ( ( (cycles) + 3) * 1000000))

// word preincrement [do while(--i)]
#define WDelayCount( us, cycles)    ( ( (us) * (F_CPU / 1000)) / ( ( (cycles) + 4) * 1000))

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

//void SysDelay( word ms);
// Milisekundove zpozdeni <ms>

#endif
