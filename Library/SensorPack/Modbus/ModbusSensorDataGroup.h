//******************************************************************************
//
//   ModbusSensorDataGroup.h  Modbus Sensor data register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#ifndef __ModbusSensorDataGroup_H__
   #define __ModbusSensorDataGroup_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif
#ifndef __ModbusReg_H__
   #include "ModbusReg.h"
#endif










//------------------------------------------------------------------------------
word ModbusRegReadSensorData( EModbusRegNum R);
// Read Sensor data register group



#endif
