//******************************************************************************
//
//   ModbusWeightDataGroup.c      Modbus Weight data register group read write functions
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "ModbusWeightDataGroup.h"
#include "Sensors/Weight.h"
#include "Weight/WeightDef.h"
#include "ModbusRegRangeCheck.h"




// Locals :


//------------------------------------------------------------------------------
//  Read Weight data register
//------------------------------------------------------------------------------
word ModbusRegReadWeightData( EModbusRegNum R)
// Read Weight data register group
{
word value;
TWeightSample sample;

   switch ( R){
      case MODBUS_REG_WEIGHT_DATA_SAMPLES_RATE :
      {
         value = GetWeightSamplesRate();
         return value;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_1 :
      {
         WeightRead(0, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_1 :
      {
         WeightRead(0, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_1 :
      {
         WeightRead(0, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_2 :
      {
         WeightRead(1, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_2 :
      {
         WeightRead(1, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_2 :
      {
         WeightRead(1, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_3 :
      {
         WeightRead(2, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_3 :
      {
         WeightRead(2, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_3 :
      {
         WeightRead(2, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_4 :
      {
         WeightRead(3, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_4 :
      {
         WeightRead(3, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_4 :
      {
         WeightRead(3, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_5 :
      {
         WeightRead(4, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_5 :
      {
         WeightRead(4, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_5 :
      {
         WeightRead(4, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_6 :
      {
         WeightRead(5, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_6 :
      {
         WeightRead(5, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_6 :
      {
         WeightRead(5, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_7 :
      {
         WeightRead(6, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_7 :
      {
         WeightRead(6, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_7 :
      {
         WeightRead(6, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_8 :
      {
         WeightRead(7, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_8 :
      {
         WeightRead(7, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_8 :
      {
         WeightRead(7, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_9 :
      {
         WeightRead(8, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_9 :
      {
         WeightRead(8, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_9 :
      {
         WeightRead(8, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_10 :
      {
         WeightRead(9, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_10 :
      {
         WeightRead(9, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_10 :
      {
         WeightRead(9, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_11 :
      {
         WeightRead(10, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_11 :
      {
         WeightRead(10, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_11 :
      {
         WeightRead(10, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_12 :
      {
         WeightRead(11, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_12 :
      {
         WeightRead(11, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_12 :
      {
         WeightRead(11, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_13 :
      {
         WeightRead(12, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_13 :
      {
         WeightRead(12, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_13 :
      {
         WeightRead(12, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_14 :
      {
         WeightRead(13, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_14 :
      {
         WeightRead(13, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_14 :
      {
         WeightRead(13, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_15 :
      {
         WeightRead(14, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_15 :
      {
         WeightRead(14, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_15 :
      {
         WeightRead(14, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_16 :
      {
         WeightRead(15, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_16 :
      {
         WeightRead(15, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_16 :
      {
         WeightRead(15, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_17 :
      {
         WeightRead(16, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_17 :
      {
         WeightRead(16, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_17 :
      {
         WeightRead(16, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_18 :
      {
         WeightRead(17, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_18 :
      {
         WeightRead(17, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_18 :
      {
         WeightRead(17, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_19 :
      {
         WeightRead(18, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_19 :
      {
         WeightRead(18, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_19 :
      {
         WeightRead(18, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_20 :
      {
         WeightRead(19, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_20 :
      {
         WeightRead(19, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_20 :
      {
         WeightRead(19, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_21 :
      {
         WeightRead(20, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_21 :
      {
         WeightRead(20, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_21 :
      {
         WeightRead(20, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_22 :
      {
         WeightRead(21, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_22 :
      {
         WeightRead(21, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_22 :
      {
         WeightRead(21, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_23 :
      {
         WeightRead(22, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_23 :
      {
         WeightRead(22, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_23 :
      {
         WeightRead(22, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_24 :
      {
         WeightRead(23, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_24 :
      {
         WeightRead(23, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_24 :
      {
         WeightRead(23, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_25 :
      {
         WeightRead(24, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_25 :
      {
         WeightRead(24, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_25 :
      {
         WeightRead(24, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_26 :
      {
         WeightRead(25, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_26 :
      {
         WeightRead(25, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_26 :
      {
         WeightRead(25, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_27 :
      {
         WeightRead(26, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_27 :
      {
         WeightRead(26, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_27 :
      {
         WeightRead(26, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_28 :
      {
         WeightRead(27, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_28 :
      {
         WeightRead(27, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_28 :
      {
         WeightRead(27, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_29 :
      {
         WeightRead(28, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_29 :
      {
         WeightRead(28, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_29 :
      {
         WeightRead(28, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_30 :
      {
         WeightRead(29, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_30 :
      {
         WeightRead(29, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_30 :
      {
         WeightRead(29, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_31 :
      {
         WeightRead(30, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_31 :
      {
         WeightRead(30, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_31 :
      {
         WeightRead(30, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_32 :
      {
         WeightRead(31, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_32 :
      {
         WeightRead(31, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_32 :
      {
         WeightRead(31, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_33 :
      {
         WeightRead(32, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_33 :
      {
         WeightRead(32, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_33 :
      {
         WeightRead(32, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_34 :
      {
         WeightRead(33, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_34 :
      {
         WeightRead(33, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_34 :
      {
         WeightRead(33, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_35 :
      {
         WeightRead(34, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_35 :
      {
         WeightRead(34, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_35 :
      {
         WeightRead(34, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_36 :
      {
         WeightRead(35, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_36 :
      {
         WeightRead(35, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_36 :
      {
         WeightRead(35, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_37 :
      {
         WeightRead(36, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_37 :
      {
         WeightRead(36, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_37 :
      {
         WeightRead(36, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_38 :
      {
         WeightRead(37, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_38 :
      {
         WeightRead(37, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_38 :
      {
         WeightRead(37, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_39 :
      {
         WeightRead(38, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_39 :
      {
         WeightRead(38, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_39 :
      {
         WeightRead(38, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_40 :
      {
         WeightRead(39, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_40 :
      {
         WeightRead(39, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_40 :
      {
         WeightRead(39, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_41 :
      {
         WeightRead(40, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_41 :
      {
         WeightRead(40, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_41 :
      {
         WeightRead(40, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_42 :
      {
         WeightRead(41, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_42 :
      {
         WeightRead(41, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_42 :
      {
         WeightRead(41, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_43 :
      {
         WeightRead(42, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_43 :
      {
         WeightRead(42, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_43 :
      {
         WeightRead(42, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_44 :
      {
         WeightRead(43, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_44 :
      {
         WeightRead(43, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_44 :
      {
         WeightRead(43, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_45 :
      {
         WeightRead(44, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_45 :
      {
         WeightRead(44, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_45 :
      {
         WeightRead(44, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_46 :
      {
         WeightRead(45, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_46 :
      {
         WeightRead(45, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_46 :
      {
         WeightRead(45, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_47 :
      {
         WeightRead(46, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_47 :
      {
         WeightRead(46, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_47 :
      {
         WeightRead(46, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_48 :
      {
         WeightRead(47, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_48 :
      {
         WeightRead(47, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_48 :
      {
         WeightRead(47, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_49 :
      {
         WeightRead(48, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_49 :
      {
         WeightRead(48, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_49 :
      {
         WeightRead(48, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_50 :
      {
         WeightRead(49, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_50 :
      {
         WeightRead(49, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_50 :
      {
         WeightRead(49, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_51 :
      {
         WeightRead(50, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_51 :
      {
         WeightRead(50, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_51 :
      {
         WeightRead(50, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_52 :
      {
         WeightRead(51, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_52 :
      {
         WeightRead(51, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_52 :
      {
         WeightRead(51, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_53 :
      {
         WeightRead(52, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_53 :
      {
         WeightRead(52, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_53 :
      {
         WeightRead(52, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_54 :
      {
         WeightRead(53, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_54 :
      {
         WeightRead(53, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_54 :
      {
         WeightRead(53, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_55 :
      {
         WeightRead(54, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_55 :
      {
         WeightRead(54, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_55 :
      {
         WeightRead(54, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_56 :
      {
         WeightRead(55, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_56 :
      {
         WeightRead(55, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_56 :
      {
         WeightRead(55, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_57 :
      {
         WeightRead(56, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_57 :
      {
         WeightRead(56, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_57 :
      {
         WeightRead(56, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_58 :
      {
         WeightRead(57, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_58 :
      {
         WeightRead(57, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_58 :
      {
         WeightRead(57, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_59 :
      {
         WeightRead(58, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_59 :
      {
         WeightRead(58, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_59 :
      {
         WeightRead(58, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_60 :
      {
         WeightRead(59, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_60 :
      {
         WeightRead(59, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_60 :
      {
         WeightRead(59, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_61 :
      {
         WeightRead(60, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_61 :
      {
         WeightRead(60, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_61 :
      {
         WeightRead(60, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_62 :
      {
         WeightRead(61, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_62 :
      {
         WeightRead(61, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_62 :
      {
         WeightRead(61, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_63 :
      {
         WeightRead(62, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_63 :
      {
         WeightRead(62, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_63 :
      {
         WeightRead(62, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_64 :
      {
         WeightRead(63, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_64 :
      {
         WeightRead(63, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_64 :
      {
         WeightRead(63, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_65 :
      {
         WeightRead(64, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_65 :
      {
         WeightRead(64, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_65 :
      {
         WeightRead(64, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_66 :
      {
         WeightRead(65, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_66 :
      {
         WeightRead(65, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_66 :
      {
         WeightRead(65, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_67 :
      {
         WeightRead(66, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_67 :
      {
         WeightRead(66, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_67 :
      {
         WeightRead(66, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_68 :
      {
         WeightRead(67, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_68 :
      {
         WeightRead(67, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_68 :
      {
         WeightRead(67, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_69 :
      {
         WeightRead(68, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_69 :
      {
         WeightRead(68, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_69 :
      {
         WeightRead(68, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_70 :
      {
         WeightRead(69, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_70 :
      {
         WeightRead(69, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_70 :
      {
         WeightRead(69, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_71 :
      {
         WeightRead(70, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_71 :
      {
         WeightRead(70, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_71 :
      {
         WeightRead(70, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_72 :
      {
         WeightRead(71, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_72 :
      {
         WeightRead(71, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_72 :
      {
         WeightRead(71, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_73 :
      {
         WeightRead(72, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_73 :
      {
         WeightRead(72, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_73 :
      {
         WeightRead(72, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_74 :
      {
         WeightRead(73, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_74 :
      {
         WeightRead(73, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_74 :
      {
         WeightRead(73, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_75 :
      {
         WeightRead(74, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_75 :
      {
         WeightRead(74, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_75 :
      {
         WeightRead(74, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_76 :
      {
         WeightRead(75, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_76 :
      {
         WeightRead(75, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_76 :
      {
         WeightRead(75, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_77 :
      {
         WeightRead(76, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_77 :
      {
         WeightRead(76, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_77 :
      {
         WeightRead(76, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_78 :
      {
         WeightRead(77, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_78 :
      {
         WeightRead(77, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_78 :
      {
         WeightRead(77, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_79 :
      {
         WeightRead(78, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_79 :
      {
         WeightRead(78, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_79 :
      {
         WeightRead(78, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_80 :
      {
         WeightRead(79, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_80 :
      {
         WeightRead(79, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_80 :
      {
         WeightRead(79, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_81 :
      {
         WeightRead(80, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_81 :
      {
         WeightRead(80, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_81 :
      {
         WeightRead(80, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_82 :
      {
         WeightRead(81, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_82 :
      {
         WeightRead(81, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_82 :
      {
         WeightRead(81, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_83 :
      {
         WeightRead(82, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_83 :
      {
         WeightRead(82, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_83 :
      {
         WeightRead(82, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_84 :
      {
         WeightRead(83, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_84 :
      {
         WeightRead(83, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_84 :
      {
         WeightRead(83, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_85 :
      {
         WeightRead(84, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_85 :
      {
         WeightRead(84, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_85 :
      {
         WeightRead(84, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_86 :
      {
         WeightRead(85, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_86 :
      {
         WeightRead(85, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_86 :
      {
         WeightRead(85, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_87 :
      {
         WeightRead(86, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_87 :
      {
         WeightRead(86, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_87 :
      {
         WeightRead(86, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_88 :
      {
         WeightRead(87, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_88 :
      {
         WeightRead(87, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_88 :
      {
         WeightRead(87, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_89 :
      {
         WeightRead(88, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_89 :
      {
         WeightRead(88, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_89 :
      {
         WeightRead(88, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_90 :
      {
         WeightRead(89, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_90 :
      {
         WeightRead(89, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_90 :
      {
         WeightRead(89, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_91 :
      {
         WeightRead(90, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_91 :
      {
         WeightRead(90, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_91 :
      {
         WeightRead(90, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_92 :
      {
         WeightRead(91, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_92 :
      {
         WeightRead(91, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_92 :
      {
         WeightRead(91, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_93 :
      {
         WeightRead(92, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_93 :
      {
         WeightRead(92, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_93 :
      {
         WeightRead(92, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_94 :
      {
         WeightRead(93, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_94 :
      {
         WeightRead(93, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_94 :
      {
         WeightRead(93, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_95 :
      {
         WeightRead(94, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_95 :
      {
         WeightRead(94, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_95 :
      {
         WeightRead(94, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_96 :
      {
         WeightRead(95, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_96 :
      {
         WeightRead(95, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_96 :
      {
         WeightRead(95, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_97 :
      {
         WeightRead(96, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_97 :
      {
         WeightRead(96, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_97 :
      {
         WeightRead(96, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_98 :
      {
         WeightRead(97, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_98 :
      {
         WeightRead(97, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_98 :
      {
         WeightRead(97, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_99 :
      {
         WeightRead(98, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_99 :
      {
         WeightRead(98, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_99 :
      {
         WeightRead(98, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_100 :
      {
         WeightRead(99, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_100 :
      {
         WeightRead(99, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_100 :
      {
         WeightRead(99, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_101 :
      {
         WeightRead(100, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_101 :
      {
         WeightRead(100, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_101 :
      {
         WeightRead(100, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_102 :
      {
         WeightRead(101, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_102 :
      {
         WeightRead(101, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_102 :
      {
         WeightRead(101, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_103 :
      {
         WeightRead(102, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_103 :
      {
         WeightRead(102, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_103 :
      {
         WeightRead(102, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_104 :
      {
         WeightRead(103, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_104 :
      {
         WeightRead(103, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_104 :
      {
         WeightRead(103, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_105 :
      {
         WeightRead(104, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_105 :
      {
         WeightRead(104, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_105 :
      {
         WeightRead(104, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_106 :
      {
         WeightRead(105, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_106 :
      {
         WeightRead(105, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_106 :
      {
         WeightRead(105, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_107 :
      {
         WeightRead(106, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_107 :
      {
         WeightRead(106, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_107 :
      {
         WeightRead(106, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_108 :
      {
         WeightRead(107, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_108 :
      {
         WeightRead(107, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_108 :
      {
         WeightRead(107, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_109 :
      {
         WeightRead(108, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_109 :
      {
         WeightRead(108, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_109 :
      {
         WeightRead(108, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_110 :
      {
         WeightRead(109, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_110 :
      {
         WeightRead(109, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_110 :
      {
         WeightRead(109, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_111 :
      {
         WeightRead(110, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_111 :
      {
         WeightRead(110, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_111 :
      {
         WeightRead(110, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_112 :
      {
         WeightRead(111, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_112 :
      {
         WeightRead(111, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_112 :
      {
         WeightRead(111, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_113 :
      {
         WeightRead(112, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_113 :
      {
         WeightRead(112, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_113 :
      {
         WeightRead(112, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_114 :
      {
         WeightRead(113, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_114 :
      {
         WeightRead(113, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_114 :
      {
         WeightRead(113, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_115 :
      {
         WeightRead(114, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_115 :
      {
         WeightRead(114, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_115 :
      {
         WeightRead(114, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_116 :
      {
         WeightRead(115, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_116 :
      {
         WeightRead(115, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_116 :
      {
         WeightRead(115, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_117 :
      {
         WeightRead(116, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_117 :
      {
         WeightRead(116, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_117 :
      {
         WeightRead(116, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_118 :
      {
         WeightRead(117, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_118 :
      {
         WeightRead(117, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_118 :
      {
         WeightRead(117, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_119 :
      {
         WeightRead(118, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_119 :
      {
         WeightRead(118, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_119 :
      {
         WeightRead(118, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_120 :
      {
         WeightRead(119, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_120 :
      {
         WeightRead(119, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_120 :
      {
         WeightRead(119, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_121 :
      {
         WeightRead(120, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_121 :
      {
         WeightRead(120, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_121 :
      {
         WeightRead(120, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_122 :
      {
         WeightRead(121, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_122 :
      {
         WeightRead(121, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_122 :
      {
         WeightRead(121, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_123 :
      {
         WeightRead(122, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_123 :
      {
         WeightRead(122, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_123 :
      {
         WeightRead(122, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_124 :
      {
         WeightRead(123, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_124 :
      {
         WeightRead(123, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_124 :
      {
         WeightRead(123, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_125 :
      {
         WeightRead(124, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_125 :
      {
         WeightRead(124, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_125 :
      {
         WeightRead(124, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_126 :
      {
         WeightRead(125, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_126 :
      {
         WeightRead(125, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_126 :
      {
         WeightRead(125, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_127 :
      {
         WeightRead(126, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_127 :
      {
         WeightRead(126, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_127 :
      {
         WeightRead(126, &sample);
         return sample.Weight;
      }
      case MODBUS_REG_WEIGHT_DATA_TIMESTAMP_128 :
      {
         WeightRead(127, &sample);
         return sample.Id;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_HI_128 :
      {
         WeightRead(127, &sample);
         return sample.Weight >> 16;
      }
      case MODBUS_REG_WEIGHT_DATA_WEIGHT_LO_128 :
      {
         WeightRead(127, &sample);
         return sample.Weight;
      }

      default :
         return 0;
   }

   return 0;
}

//------------------------------------------------------------------------------
//  Write Weight data register
//------------------------------------------------------------------------------
TYesNo ModbusRegWriteWeightData( EModbusRegNum R, word D)
// Write Weight data register group
{
   return NO;
}




