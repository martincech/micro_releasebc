//******************************************************************************
//
//   Weight.h           Routines to read Weights
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Weight_H__
   #define __Weight_H__
#include "Weight/WeightDef.h"

typedef struct{
   uint16     Id;
   TRawWeight Weight;
} TWeightSample;

void WeightRead(int i, TWeightSample *sample);
// read last ith sample from samples buffer

uint16 GetWeightSamplesRate();
// get samples rate in Hz


#endif // __Weight_H__


