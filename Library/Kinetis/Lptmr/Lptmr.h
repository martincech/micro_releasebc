//*****************************************************************************
//
//    Lptmr.h           Low power timer
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Lptmr_H__
   #define __Lptmr_H__

#include "Unisys/Uni.h"

void LptmrInit( void);
// Initialize module

void LPTmrEvent( void);
// LPTmr callback

#endif
