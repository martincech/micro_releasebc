//*****************************************************************************
//
//    UsartReg.h   Kinetis USART register control
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __UsartReg_H__
   #include "Cpu/UartTranslation.h"
   #include "Kinetis/Cpu/UsartReg.h"

   #define __UsartReg_H__

#define usartStatusReset( Module)   (Module)->D;(Module)->S1 = 0xFF

#endif