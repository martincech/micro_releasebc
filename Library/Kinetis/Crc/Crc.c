//******************************************************************************
//
//   Crc.cpp            Crc calculation
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "Crc/Crc.h"
#include "Cpu/CrcReg.h"
#include "Hardware.h"

word CalculateCrc16( char *Buf, word BufferLen, word Residue)
// calculate 16 with initial value set to Residue
{
word res;
word *Buffer;

   Buffer = (word *)Buf;
   CrcClockEnable();
   crc16Init(CRC0, CRC16_POLYNOM, Residue);
   do{
      crc16Calc(CRC0, *Buf);
      Buf++;
   }while( --BufferLen);
   res = crc16Result( CRC0);
   CrcClockDisable();
   return res;
}

byte CalculateXorLrc( char *Buf, char BufferLen, TYesNo FinalXor)
// calculate LRC as xor of bytes with additional xoring by 0xFF(if FinalXor set)
{
byte res;
byte *Buffer = Buf;

   res = *Buffer;
   while( --BufferLen){
      res ^= *Buffer++;
   }
   if(FinalXor){
      res ^= 0xFF;
   }
   return res;

}
