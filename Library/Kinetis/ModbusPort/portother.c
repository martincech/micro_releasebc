/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "Multitasking/Multitasking.h"

static TMutex mutex;
static TYesNo inited;
/* ----------------------- Start implementation -----------------------------*/
void
vMBPortEnterCritical( void )
{
   if(inited != YES){
      MultitaskingMutexInit(&mutex);
      inited = YES;
   }
   MultitaskingMutexSet(&mutex);
}

void
vMBPortExitCritical( void )
{
    MultitaskingMutexRelease(&mutex);
}

void
vMBPortClose( void )
{
    extern void vMBPortSerialClose( void );
    extern void vMBPortTimerClose( void );
    vMBPortSerialClose(  );
    vMBPortTimerClose(  );
}