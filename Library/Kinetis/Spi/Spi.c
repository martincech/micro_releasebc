//*****************************************************************************
//
//   Spi.c        Kinetis SPI master interface
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

/*
   - Hierarchy
   The MCU contains MODULE_COUNT Spi modules.
   Each module has its dedicated SCLK, SDI and SDO line.
   
   Each module contains SUBMODULES_PER_MODULE submodules.
   Each submodule can be configured to use distinct clock value and Spi mode.
   
   Each submodule contains SPI_CHANNELS_PER_SUBMODULE channels.
   Each channel has its dedicated CS line.

   - Example
   Given MODULE_COUNT = 3, SUBMODULES_PER_MODULE = 2, SPI_CHANNELS_PER_SUBMODULE = 2,
   Spi channels are assigned as follows:

own com lines  |        Module0               Module1               Module2
own clock value|    Sub0       Sub1       Sub0       Sub1       Sub0       Sub1
own CS pin     |  CH0  CH1   CH2  CH3   CH4  CH5   CH6  CH7   CH8  CH9   CH10 CH11
*/

#include "Spi/Spi.h"
#include "Hardware.h"

#ifdef SPI2
   #define MODULE_COUNT            3
#elif defined(SPI1)
   #define MODULE_COUNT            2
#elif defined(SPI0)
   #define MODULE_COUNT            1
#else
   #error No SPI modules
#endif

#define SUBMODULES_PER_MODULE   2

static const TPin CsPins[];

#include "SpiMacros.h" // fills CsPins array

// Channel decomposition :
#define SpiModuleGet( SpiAddress)      (SpiAddress / (SPI_CHANNELS_PER_SUBMODULE * SUBMODULES_PER_MODULE))
#define SpiSubmoduleGet( SpiAddress)   ((SpiAddress / SPI_CHANNELS_PER_SUBMODULE) % SUBMODULES_PER_MODULE)

// Transceive macros:
#define Send( Val) (SpiDevice->PUSHR = Val | SPI_PUSHR_CTAS(SpiSubmodule))
#define Read( )    (SpiDevice->POPR)

#define TransferComplete( )      (SpiDevice->SR & SPI_SR_TCF_MASK)
#define TransferCompleteClear( ) (SpiDevice->SR = SPI_SR_TCF_MASK)

// Configuration:
#define ResetAndEnableClock() ConfigureModule(0)
#define ConfigureModule( Val) (SpiDevice->MCR = Val)
#define ConfigureChannel( Val) (SpiDevice->CTAR[SpiSubmodule] = Val)
#define ConfigureBaudRate( Half, Prescaler, Scaler)     \
   SpiDevice->CTAR[SpiSubmodule] &= ~(SPI_CTAR_DBR_MASK | SPI_CTAR_PBR_MASK, SPI_CTAR_BR_MASK); \
   SpiDevice->CTAR[SpiSubmodule] |= (Half == 0 ? 0 : SPI_CTAR_DBR_MASK) | SPI_CTAR_PBR(Prescaler) | SPI_CTAR_BR(Scaler);

#define Cpol( Val)      (Val ? SPI_CTAR_CPOL_MASK : 0)
#define Cpha( Val)      (Val ? SPI_CTAR_CPHA_MASK : 0)
#define FrameSize( Val) SPI_CTAR_FMSZ(Val - 1)
#define Master          (SPI_MCR_MSTR_MASK)
#define DisableTxFifo   (SPI_MCR_DIS_TXF_MASK)
#define DisableRxFifo   (SPI_MCR_DIS_RXF_MASK)

// Misc:
#define Average(a, b)   (((a) + (b)) / 2)
#define Abs(a)          (((signed)a) >= 0 ? (a) : -(a))

// Local functions :
static volatile SPI_Type * SpiDeviceGet( TSpiAddress SpiAddress);
// Returns SPI device by <SpiAddress>

static inline void BaudRateSet(TSpiAddress Spi, unsigned BaudRate);
// Sets <BaudRate>

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void SpiInit( TSpiAddress Spi)
// Initialize bus
{
volatile SPI_Type *SpiDevice;
byte SpiSubmodule;
byte SpiModule;
dword Clock;
   SpiDevice    = SpiDeviceGet( Spi);
   SpiModule    = SpiModuleGet( Spi);
   SpiSubmodule = SpiSubmoduleGet( Spi);

   switch(SpiModule) {
      case 0:
         PinFunction( SPI_M0_CLK_PIN, SPI_M0_CLK_PIN_FUNCTION);
         PinFunction( SPI_M0_DIN_PIN, SPI_M0_DIN_PIN_FUNCTION);
         PinFunction( SPI_M0_DOUT_PIN, SPI_M0_DOUT_PIN_FUNCTION);
         Spi0PortInit(); // additional user init
         Spi0ClockEnable();
         switch(SpiSubmodule) {
            case 0:
               Clock = SPI_M0_S0_CLOCK;
               break;
            default:
               Clock = SPI_M0_S1_CLOCK;
               break;
         }
         break;
      case 1:
         PinFunction( SPI_M1_CLK_PIN, SPI_M1_CLK_PIN_FUNCTION);
         PinFunction( SPI_M1_DIN_PIN, SPI_M1_DIN_PIN_FUNCTION);
         PinFunction( SPI_M1_DOUT_PIN, SPI_M1_DOUT_PIN_FUNCTION);
         Spi1PortInit(); // additional user init
         Spi1ClockEnable();
         switch(SpiSubmodule) {
            case 0:
               Clock = SPI_M1_S0_CLOCK;
               break;
            default:
               Clock = SPI_M1_S1_CLOCK;
               break;
         }
         break;
      case 2:
         PinFunction( SPI_M2_CLK_PIN, SPI_M2_CLK_PIN_FUNCTION);
         PinFunction( SPI_M2_DIN_PIN, SPI_M2_DIN_PIN_FUNCTION);
         PinFunction( SPI_M2_DOUT_PIN, SPI_M2_DOUT_PIN_FUNCTION);
         Spi2PortInit(); // additional user init
         Spi2ClockEnable();
         switch(SpiSubmodule) {
            case 0:
               Clock = SPI_M2_S0_CLOCK;
               break;
            default:
               Clock = SPI_M2_S1_CLOCK;
               break;
         }
         break;

      default:
         return;
   }

   ResetAndEnableClock();
   ConfigureModule( Master | DisableTxFifo | DisableRxFifo);
   ConfigureChannel( Cpol(0) | Cpha(0) | FrameSize(8));
   BaudRateSet( Spi, Clock);

   TPin CsPin = CsPins[Spi];
   GpioSet( CsPin);
   GpioOutput( CsPin);
   PinFunction( CsPin, KINETIS_GPIO_FUNCTION);
} // SpiInit

//-----------------------------------------------------------------------------
// Attach
//-----------------------------------------------------------------------------

void SpiAttach( TSpiAddress Spi)
// Attach SPI bus - activate chipselect
{
    GpioClr( CsPins[Spi]);
} // SpiAttach

//-----------------------------------------------------------------------------
// Release
//-----------------------------------------------------------------------------

void SpiRelease( TSpiAddress Spi)
// Release SPI bus - deactivate chipselect
{
   GpioSet( CsPins[Spi]);
} // SpiRelease

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

byte SpiByteRead( TSpiAddress Spi)
// Read byte from SPI
{
volatile SPI_Type *SpiDevice;
byte SpiSubmodule;
word Data;
   SpiDevice    = SpiDeviceGet( Spi);
   SpiSubmodule = SpiSubmoduleGet( Spi);
   Send(0xFF);
   while(!TransferComplete());
   TransferCompleteClear();
   Data = Read();
   return (byte)Data;
} // SpiByteRead

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void SpiByteWrite( TSpiAddress Spi, byte Value)
// Write byte to SPI
{
volatile SPI_Type *SpiDevice;
byte SpiSubmodule;
word Data;
   SpiDevice   = SpiDeviceGet( Spi);
   SpiSubmodule = SpiSubmoduleGet( Spi);
   Send(Value);
   while(!TransferComplete());
   Read(); // clear buffer
   TransferCompleteClear();
} // SpiByteWrite

//-----------------------------------------------------------------------------
// Transceive
//-----------------------------------------------------------------------------

byte SpiByteTransceive( TSpiAddress Spi, byte TxValue)
// Write/read SPI. Returns value read
{
volatile SPI_Type *SpiDevice;
byte SpiSubmodule;
word Data;
   SpiDevice    = SpiDeviceGet( Spi);
   SpiSubmodule = SpiSubmoduleGet( Spi);
   Send(TxValue);
   while(!TransferComplete());
   TransferCompleteClear();
   Data = Read();
   return (byte)Data;
} // SpiByteTransceive

//******************************************************************************

//-----------------------------------------------------------------------------
//  Get device
//-----------------------------------------------------------------------------

static volatile SPI_Type * SpiDeviceGet( TSpiAddress SpiAddress)
// Returns SPI device by <SpiAddress>
{
byte SpiModule = SpiModuleGet( SpiAddress);
   switch(SpiModule) {
      case 0:
         return SPI0;
      case 1:
         return SPI1;
      default:
         return SPI2;
   }
} // _SpiDeviceGet

//-----------------------------------------------------------------------------
//  Baud rate set
//-----------------------------------------------------------------------------

// Baud rate prescalers definition
static const byte Prescalers[] = { 2, 3, 5, 7};
#define PRESCALERS_COUNT sizeof(Prescalers) / sizeof(Prescalers[0])

// Baud rate scalers definition
static const word Scalers[] =    { 2, 4, 6, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768};
#define SCALERS_COUNT sizeof(Scalers) / sizeof(Scalers[0])

static void BaudRateSet(TSpiAddress Spi, unsigned BaudRate)
// Sets <BaudRate>
{
/*
   Funkce tupe projde vsechny kombinace Double Baud Rate, Prescaler, scaler 
   a najde takovou baudrate, ktera je nejblize pozadovane baudrate
*/
byte BestFitHalfBaudRateEncoding = 0;
byte BestFitPrescalerEncoding = 0;
byte BestFitScalerEncoding = 0;
dword BestFitRealBaudRate = 0x7FFFFFFF;
dword RealBaudRate;
byte i;
byte j;
byte k;
volatile SPI_Type *SpiDevice;
byte SpiSubmodule;
   
   for(i = 0 ; i < 2 ; i++) {
      for(j = 0 ; j < PRESCALERS_COUNT ; j++) {
         for(k = 0 ; k < SCALERS_COUNT ; k++) {
            RealBaudRate = F_BUS / Prescalers[j] * (i + 1) / Scalers[k];
            int RealDifference = RealBaudRate - BaudRate;
            int BestDifference = BestFitRealBaudRate - BaudRate;
            if(Abs(RealDifference) < Abs(BestDifference)) {
               BestFitRealBaudRate = RealBaudRate;
               BestFitHalfBaudRateEncoding = i;
               BestFitPrescalerEncoding = j;
               BestFitScalerEncoding = k;
            }
         }
      }
   }
   
   SpiDevice    = SpiDeviceGet( Spi);
   SpiSubmodule = SpiSubmoduleGet( Spi);

   ConfigureBaudRate(BestFitHalfBaudRateEncoding, BestFitPrescalerEncoding, BestFitScalerEncoding);
} // BaudRateSet