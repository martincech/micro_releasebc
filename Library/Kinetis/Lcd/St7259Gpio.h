//*****************************************************************************
//
//    St7259Gpio.h  ST7259 GPIO
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __St7259Gpio_H__
   #define __St7259Gpio_H__

#include "Hardware.h"

#if F_CPU > 48000000
   #define NOPS      Nop(); Nop(); Nop(); Nop(); Nop(); Nop()
#elif F_CPU > 36000000
   #define NOPS      Nop(); Nop(); Nop(); Nop(); Nop()
#elif F_CPU > 24000000
   #define NOPS      Nop(); Nop(); Nop(); Nop()
#elif F_CPU > 12000000
   #define NOPS      Nop(); Nop(); Nop()
#else
   #define NOPS
#endif

// chipselect :
#define GpuSelect()     _GpuSelect(); NOPS
#define GpuDeselect()   _GpuDeselect(); NOPS
// control signals :
#define GpuResSet()     _GpuResSet(); NOPS
#define GpuResClr()     _GpuResClr(); NOPS
#define GpuWrSet()      _GpuWrSet(); NOPS
#define GpuWrClr()      _GpuWrClr(); NOPS
#define GpuRsSet()      _GpuRsSet(); NOPS
#define GpuRsClr()      _GpuRsClr(); NOPS

#endif