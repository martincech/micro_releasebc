//*****************************************************************************
//
//    UartSimple.c  UART basic direct control
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#include "Timer/Timer.h"

//-----------------------------------------------------------------------------
// Tx busy
//-----------------------------------------------------------------------------

TYesNo UartTxBusy( TUartAddress Uart)
// Returns YES if transmitter is busy
{
TUsart *Usart;

   Usart = UsartGet( Uart);
   return( !usartTxReady( Usart));
} // UartTxBusy

//-----------------------------------------------------------------------------
// Tx
//-----------------------------------------------------------------------------

void UartTxChar( TUartAddress Uart, byte Char)
// Transmit <Char> byte
{
TUsart *Usart;

   Usart = UsartGet( Uart);
   while( !usartTxReady( Usart));
   usartTxData( Usart, Char);
} // UartTxChar

//-----------------------------------------------------------------------------
// Rx
//-----------------------------------------------------------------------------

TYesNo UartRxChar( TUartAddress Uart, byte *Char)
// Returns YES and received byte on <Char>,
// or NO on timeout
{
TShortTimer Now;
TUsart *Usart;

   Usart = UsartGet( Uart);
   Now = SysTimer() + _Uart[ Uart].Data->IntercharacterTimeout;
   do {
      if( usartRxError( Usart)){
         usartStatusReset( Usart);     // reset line error
         return( NO);                  // line error
      }
      if( usartRxReady( Usart)){
         *Char = usartRxData( Usart);
         return( YES);
      }
   } while( TimerBefore( SysTimer(), Now));
   return( NO);                        // timeout
} // UartRxChar

//-----------------------------------------------------------------------------
// Rx Wait
//-----------------------------------------------------------------------------

TYesNo UartRxWait( TUartAddress Uart, unsigned Timeout)
// Waits for receive up to <Timeout> miliseconds
{
TShortTimer Now;
TUsart *Usart;
   
   Usart = UsartGet( Uart);
   Now = SysTimer() + Timeout;
   do {
      if( usartRxError( Usart)){
         usartStatusReset( Usart);     // reset line error
         return( NO);                  // break waiting
      }
      if( usartRxReady( Usart)){
         return( YES);
      }
   } while( TimerBefore( SysTimer(), Now));
   return( NO);
} // UartRxWait
