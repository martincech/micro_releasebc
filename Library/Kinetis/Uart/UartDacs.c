//*****************************************************************************
//
//    UartDacs.c  UART Dacs local functions
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************
//------------------------------------------------------------------------------
//   Mode
//------------------------------------------------------------------------------

static void _UartDacsModeSet( TUartAddress Address, TUartMode Mode)
// Set Dacs mode
{
const TUartDescriptor *Uart = UartGet( Address);

   usartInterruptDisable( UsartGet(Address));
   InterruptDisable();
   switch( Address){
#if UART_PORTS_COUNT >= 1
      case UART_UART0 :
         CpuIrqAttach( UART0_RX_TX_IRQn, UART0_PRIORITY, _Usart0DacsHandler);
         CpuIrqAttach( UART0_ERR_IRQn,   UART0_PRIORITY, _Usart0DacsHandler);
         CpuIrqEnable( UART0_RX_TX_IRQn);
         CpuIrqEnable( UART0_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 2
      case UART_UART1 :
         CpuIrqAttach( UART1_RX_TX_IRQn, UART1_PRIORITY, _Usart1DacsHandler);
         CpuIrqAttach( UART1_ERR_IRQn,   UART1_PRIORITY, _Usart1DacsHandler);
         CpuIrqEnable( UART1_RX_TX_IRQn);
         CpuIrqEnable( UART1_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 3
      case UART_UART2 :
         CpuIrqAttach( UART2_RX_TX_IRQn, UART2_PRIORITY, _Usart2DacsHandler);
         CpuIrqAttach( UART2_ERR_IRQn,   UART2_PRIORITY, _Usart2DacsHandler);
         CpuIrqEnable( UART2_RX_TX_IRQn);
         CpuIrqEnable( UART2_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 4
      case UART_UART3 :
         CpuIrqAttach( UART3_RX_TX_IRQn, UART3_PRIORITY, _Usart3DacsHandler);
         CpuIrqAttach( UART3_ERR_IRQn,   UART3_PRIORITY, _Usart3DacsHandler);
         CpuIrqEnable( UART3_RX_TX_IRQn);
         CpuIrqEnable( UART3_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 5
      case UART_UART4 :
         CpuIrqAttach( UART4_RX_TX_IRQn, UART4_PRIORITY, _Usart4DacsHandler);
         CpuIrqAttach( UART4_ERR_IRQn,   UART4_PRIORITY, _Usart4DacsHandler);
         CpuIrqEnable( UART4_RX_TX_IRQn);
         CpuIrqEnable( UART4_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 6
      case UART_UART5 :
         CpuIrqAttach( UART5_RX_TX_IRQn, UART5_PRIORITY, _Usart5DacsHandler);
         CpuIrqAttach( UART5_ERR_IRQn,   UART5_PRIORITY, _Usart5DacsHandler);
         CpuIrqEnable( UART5_RX_TX_IRQn);
         CpuIrqEnable( UART5_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 7
      case UART_UART6 :
         CpuIrqAttach( UART6_RX_TX_IRQn, UART6_PRIORITY, _Usart6DacsHandler);
         CpuIrqAttach( UART6_ERR_IRQn,   UART6_PRIORITY, _Usart6DacsHandler);
         CpuIrqEnable( UART6_RX_TX_IRQn);
         CpuIrqEnable( UART6_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 8
      case UART_UART7 :
         CpuIrqAttach( UART7_RX_TX_IRQn, UART7_PRIORITY, _Usart7DacsHandler);
         CpuIrqAttach( UART7_ERR_IRQn,   UART7_PRIORITY, _Usart7DacsHandler);
         CpuIrqEnable( UART7_RX_TX_IRQn);
         CpuIrqEnable( UART7_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 9
      case UART_UART8 :
         CpuIrqAttach( UART8_RX_TX_IRQn, UART8_PRIORITY, _Usart8DacsHandler);
         CpuIrqAttach( UART8_ERR_IRQn,   UART8_PRIORITY, _Usart8DacsHandler);
         CpuIrqEnable( UART8_RX_TX_IRQn);
         CpuIrqEnable( UART8_ERR_IRQn);
         break;
#endif
#if UART_PORTS_COUNT >= 10
      case UART_UART9 :
         CpuIrqAttach( UART9_RX_TX_IRQn, UART9_PRIORITY, _Usart9DacsHandler);
         CpuIrqAttach( UART9_ERR_IRQn,   UART9_PRIORITY, _Usart9DacsHandler);
         CpuIrqEnable( UART9_RX_TX_IRQn);
         CpuIrqEnable( UART9_ERR_IRQn);
         break;
#endif
      default :
         break;
   }
   InterruptEnable();
} // _UartDacsModeSet

//------------------------------------------------------------------------------
//   IRQ Handler
//------------------------------------------------------------------------------

static void __irq _Usart0DacsHandler( void)
// USART0 Dacs handler
{
   _UsartDacsHandler( UartGet(  UART_UART0));
} // _Usart0DacsHandler

static void __irq _Usart1DacsHandler( void)
// USART1 Dacs handler
{
   _UsartDacsHandler( UartGet(  UART_UART1));
} // _Usart1DacsHandler

static void __irq _Usart2DacsHandler( void)
// USART2 Dacs handler
{
   _UsartDacsHandler( UartGet(  UART_UART2));
} // _Usart2DacsHandler

static void __irq _Usart3DacsHandler( void)
// USART3 Dacs handler
{
   _UsartDacsHandler( UartGet(  UART_UART3));
} // _Usart3DacsHandler

static void __irq _Usart4DacsHandler( void)
// USART4 Dacs handler
{
   _UsartDacsHandler( UartGet(  UART_UART4));
} // _Usart4DacsHandler

static void __irq _Usart5DacsHandler( void)
// USART5 Dacs handler
{
   _UsartDacsHandler( UartGet(  UART_UART5));
} // _Usart5DacsHandler

static void __irq _Usart6DacsHandler( void)
// USART6 Dacs handler
{
   _UsartDacsHandler( UartGet(  UART_UART6));
} // _Usar

static void __irq _Usart7DacsHandler( void)
// USART7 Dacs handler
{
   _UsartDacsHandler( UartGet(  UART_UART7));
} // _Usar

static void __irq _Usart8DacsHandler( void)
// USART8 Dacs handler
{
   _UsartDacsHandler( UartGet(  UART_UART8));
} // _Usar

static void __irq _Usart9DacsHandler( void)
// USART9 Dacs handler
{
   _UsartDacsHandler( UartGet(  UART_UART9));
} // _Usar

//------------------------------------------------------------------------------
//   Common Dacs Handler
//------------------------------------------------------------------------------

static void _UsartDacsHandler( const TUartDescriptor *Uart)
// Common Dacs handler
{
TUartData      *UData;
TUsart         *Usart;
byte           Data;
byte           i;
word           Index;
unsigned       Status;
static byte    TrailerIndex = 0;
static byte    LeaderIndex = 0;
static TYesNo  DoubleDLE = NO;

   Usart = Uart->Usart;
   UData = Uart->Data;
   Status = usartStatus( Usart);
//------------------------------------------------------------------------------
   // check for Rx errors :
   if( usartStatusRxError( Status)){
      if( UData->Status == UART_RECEIVE_ACTIVE){
         if( usartStatusRxFraming( Status)){
            RxSetStatusAndStop( Uart, UART_RECEIVE_FRAMING_ERROR);
         }
         if( usartStatusRxOverrun( Status)){
            RxSetStatusAndStop( Uart, UART_RECEIVE_OVERRUN_ERROR);
         }
         usartStatusReset( Usart);               // clear status
         return;                                 // stop on Rx error
      }
      usartStatusReset( Usart);
   }
//------------------------------------------------------------------------------
      // check for Rx data :
      if( usartStatusRx( Status)){
         Data = usartRxData( Usart);
         if( UData->Status == UART_RECEIVE_ACTIVE){
            //erase parity bit from data when parity enabled
            if( Usart->C1 & UART_C1_PE_MASK && !(Usart->C1 & UART_C1_M_MASK)){
               // 8th bit is parity
               Data &= ~0x80;
            }
            timerRestart( Uart);                    // restart intercharacter timeout
            Index = UData->Index - 1;
            if( UData->Index == 0){
               // waiting for leader
               if( Data == UData->Leader[LeaderIndex]){
                  LeaderIndex++;
                  if(LeaderIndex == UData->LeaderLen){
                     UData->Index++;
                     TrailerIndex = 0;
                     LeaderIndex = 0;
                  }
               } else {
                  LeaderIndex = 0;
               }
            } else if( Index >= UData->ReceiveSizeMax){
               // data size overrun
               RxSetStatusAndStop( Uart, UART_RECEIVE_SIZE_ERROR);
            } else if ( TrailerIndex){
               if( Data == UData->Trailer[TrailerIndex]){
                  if( TrailerIndex == UData->TrailerLen - 1){
                     // frame complete - n trailers
                     UData->Size   = Index;
                     RxSetStatusAndStop( Uart, UART_RECEIVE_FRAME);
                  } else {
                     TrailerIndex++;
                  }
               } else if ( Data == UData->Trailer[0]){
                  // same as first trailer character, just doubling
                  UData->ReceiveBuffer[ Index] = Data;
                  UData->Index++;
                  TrailerIndex = 0;
               } else{
                  // invalid data - error
                  RxSetStatusAndStop( Uart, UART_RECEIVE_SIZE_ERROR);
               }
            } else if( Data == UData->Trailer[0]){
               TrailerIndex = 1;
               if( UData->TrailerLen == TrailerIndex){
                  // frame complete - 1 trailer
                  UData->Size   = Index;
                  RxSetStatusAndStop( Uart, UART_RECEIVE_FRAME);
               }
            } else {
               // data in the frame
               UData->ReceiveBuffer[ Index] = Data;
               UData->Index++;
            }
         }
      }
//------------------------------------------------------------------------------
   // check for Tx data :
   if( usartStatusTx( Status)){
      if( UData->Status == UART_SEND_ACTIVE){
         if( DoubleDLE){
            //send DLE character
            usartTxData( Usart, UData->Trailer[0]);
            DoubleDLE = NO;
         } else {
            Index = UData->Index - 1;
            if( UData->Index == 0){
               //LEADER
               usartTxData( Usart, UData->Leader[LeaderIndex]);
               LeaderIndex++;
               if(LeaderIndex == UData->LeaderLen){
                  UData->Index++;
                  LeaderIndex = 0;
                  TrailerIndex = 0;
               }
            } else if( Index < UData->Size){
               //DATA
               usartTxData( Usart, UData->SendBuffer[ Index]);
               if( UData->SendBuffer[ Index] == UData->Trailer[0]){
                  DoubleDLE = YES;
               }
               UData->Index++;
            } else {
               //TRAILER
               usartTxData( Usart, UData->Trailer[ TrailerIndex]);
               TrailerIndex++;
               if( UData->TrailerLen == TrailerIndex){
                  usartTxInterruptDisable( Usart);     // stop Tx
                  usartTxDoneInterruptEnable( Usart);  // wait for Tx done
               }
            }
         }
      }
   }
//------------------------------------------------------------------------------
   // check for Tx done :
   if( usartStatusTxDone( Status)){
      if( UData->Status == UART_SEND_ACTIVE){
         UData->Status = UART_SEND_DONE;
         UartTxDisable( Uart);                   // RS485 direction to Rx
         usartTxDoneInterruptDisable( Usart);
         _UartReceiveStart( Uart);               // start receiving
      }
   }
} // _UsartDacsHandler
