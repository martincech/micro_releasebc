//*****************************************************************************
//
//    UartMegavi.h   UART megavi local functions
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

static void _UartMegaviModeSet( TUartAddress Uart, TUartMode Mode);
// Set Megavi mode

static void __irq _Usart0MegaviHandler( void);
// USART0 Megavi handler

static void __irq _Usart1MegaviHandler( void);
// USART1 Megavi handler

static void __irq _Usart2MegaviHandler( void);
// USART2 Megavi handler

static void __irq _Usart3MegaviHandler( void);
// USART3 Megavi handler

static void __irq _Usart4MegaviHandler( void);
// USART4 Megavi handler

static void __irq _Usart5MegaviHandler( void);
// USART5 Megavi handler

static void __irq _Usart6MegaviHandler( void);
// USART6 Megavi handler

static void __irq _Usart7MegaviHandler( void);
// USART7 Megavi handler

static void __irq _Usart8MegaviHandler( void);
// USART8 Megavi handler

static void __irq _Usart9MegaviHandler( void);
// USART9 Megavi handler

static void _UsartMegaviHandler( const TUartDescriptor *Uart);
// Common Megavi handler
