//******************************************************************************
//
//   SocketIfUart.h     Uart socket interface
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "Remote/SocketIfUart.h"
#include "SmsGate/SmsGateConfig.h"
#include "Uart/Uart.h"
#include "Remote/Frame.h"
#include <assert.h>

#define SOCKET_UART_INTERCHARACTER_TIMEOUT  0
#define SOCKET_UART_SEND_DELAY 0
#define SOCKET_FRAME_RX_REPLY_TIMEOUT    0
#define SOCKET_FRAME_RX_TIMEOUT          200
#define SOCKET_UART_BUFFER_SIZE         (SMS_GATE_BAUD_RATE_MAX / 60)  // 1 second buffer for 8N1 uart mode

static TFrameState   _FrameState;
static ESocketState  _State;
static byte          _UartBuffer[SOCKET_UART_BUFFER_SIZE];

//------------------------------------------------------------------------------
//   Permission
//------------------------------------------------------------------------------

byte SocketIfUartPermission( TSocket *Socket)
// Permission
{
   return FILE_MODE_READ_WRITE;
} // SocketIfUartPermission

static TYesNo Occupied;

TYesNo SocketIfUartListen( TSocket *Socket) {
   return NO;
   if(Occupied) {
      return NO;
   }
   if(_State == SOCKET_STATE_DISCONNECTED) {
      return NO;
   }
   Occupied = YES;
   return YES;
}

//------------------------------------------------------------------------------
//   Init
//------------------------------------------------------------------------------

void SocketIfUartInit( void)
{
EUartFormat UartFormat;

   UartInit( SmsGateOptions.DeviceAddress);
   switch( SmsGateOptions.Parity){
      case (SMS_GATE_PARITY_NONE):
         UartFormat = UART_7BIT;
         break;
      case (SMS_GATE_PARITY_ODD):
         UartFormat = UART_7BIT_ODD;
         break;
      case (SMS_GATE_PARITY_EVEN):
         UartFormat = UART_7BIT_EVEN;
         break;
   }
   if( SmsGateOptions.DataBits == SMS_GATE_DATA_BITS_8){
       UartFormat += UART_8BIT;
   }
   UartSetup( SmsGateOptions.DeviceAddress, SmsGateOptions.BaudRate, UartFormat);
   UartTimeoutSet( SmsGateOptions.DeviceAddress, UART_TIMEOUT_OFF, SOCKET_UART_INTERCHARACTER_TIMEOUT);
   UartSendDelaySet( SmsGateOptions.DeviceAddress, SOCKET_UART_SEND_DELAY);
   UartModeSet( SmsGateOptions.DeviceAddress, UART_MODE_BINARY_SLAVE);
   UartBufferSet( SmsGateOptions.DeviceAddress, &_UartBuffer, SOCKET_UART_BUFFER_SIZE);

   FrameInit( &_FrameState);
   FrameTimeoutSet( &_FrameState, SOCKET_FRAME_RX_TIMEOUT, SOCKET_FRAME_RX_REPLY_TIMEOUT);
   _State = SOCKET_STATE_DISCONNECTED;

}

//------------------------------------------------------------------------------
//   Free
//------------------------------------------------------------------------------

void SocketIfUartDeInit( void)
{
   UartDeinit( SmsGateOptions.DeviceAddress);
   _State = SOCKET_STATE_DISCONNECTED;
}


//------------------------------------------------------------------------------
//   Execute
//------------------------------------------------------------------------------

void SocketIfUartExecute( void)
{
TUartStatus LinkStatus;
int         SendSize;
byte       *RxBuffer;
int         RxSize;
TYesNo      RxPaused;

   switch( _State){
      case SOCKET_STATE_DISCONNECTED :
         if(   _FrameState.Status == FRAME_STATE_RECEIVE_REPLY_TIMEOUT ||
               _FrameState.Status == FRAME_STATE_RECEIVE_TIMEOUT){
            FrameInit( &_FrameState);
            break;
         }
         _State = SOCKET_STATE_CONNECTED;
         break;

      case SOCKET_STATE_RECEIVE_ACTIVE :
         LinkStatus = UartReceiveStatus( SmsGateOptions.DeviceAddress);
         switch( LinkStatus){
            case UART_IDLE :
               UartReceive( SmsGateOptions.DeviceAddress);
            case UART_RECEIVE_ACTIVE :
            case UART_RECEIVE_TIMEOUT :
            case UART_RECEIVE_FRAME :
               assert( SmsGateOptions.DeviceAddress < UART_UART2 || SmsGateOptions.BaudRate <= 19200);
               RxBuffer = UartReceiveBuffer(SmsGateOptions.DeviceAddress);
               RxSize = UartReceiveSize(SmsGateOptions.DeviceAddress);
               //process data
               FrameReceiveProccess( &_FrameState, RxBuffer, RxSize);
               if( _FrameState.Status == FRAME_STATE_RECEIVE_WAITING_HEADER){
                  break;
               }
               //----------------------------
               // move buffer
               UartBufferMove( SmsGateOptions.DeviceAddress, RxSize);
               //----------------------------
               if( _FrameState.Status == FRAME_STATE_RECEIVE_ACTIVE){
                  break;
               }
               if( _FrameState.Status == FRAME_STATE_RECEIVE_FRAME){
                  _State = SOCKET_STATE_RECEIVE_DONE;
                  break;
               }

            default :
               _State = SOCKET_STATE_RECEIVE_ERROR;
               break;
         }
         break;

      case SOCKET_STATE_SEND_ACTIVE :
         LinkStatus = UartSendStatus( SmsGateOptions.DeviceAddress);
         switch( LinkStatus){
            case UART_SEND_ACTIVE :
               break;

            case UART_SEND_DONE :
               if( _FrameState.Status == FRAME_STATE_SEND_DONE){
                  _State = SOCKET_STATE_SEND_DONE;
                  break;
               }
               if( _FrameState.Status == FRAME_STATE_SEND_TIMEOUT){
                  _State = SOCKET_STATE_SEND_ERROR;
               }
            default :
               if( !FrameSendProccess( &_FrameState,
                                  &_UartBuffer,
                                  SOCKET_UART_BUFFER_SIZE,
                                  &SendSize)){
                  _State = SOCKET_STATE_SEND_ERROR;
               }
               UartSend( SmsGateOptions.DeviceAddress, &_UartBuffer, SendSize);
               break;
         }
      default :
         break;
   }

   switch( _State){
      // check new state and reset uart if needed
      case SOCKET_STATE_SEND_DONE :
      case SOCKET_STATE_RECEIVE_DONE :
      case SOCKET_STATE_RECEIVE_ERROR :
      case SOCKET_STATE_SEND_ERROR :
      case SOCKET_STATE_ERROR :
      case SOCKET_STATE_DISCONNECTED :
         UartStop( SmsGateOptions.DeviceAddress);
         UartBufferSet( SmsGateOptions.DeviceAddress, &_UartBuffer, SOCKET_UART_BUFFER_SIZE);
      break;
   }
}
//------------------------------------------------------------------------------
//   timer
//------------------------------------------------------------------------------

void SocketIfUartTimer( void)
{
   FrameTimer( &_FrameState);
   if(   _FrameState.Status == FRAME_STATE_RECEIVE_REPLY_TIMEOUT ||
         _FrameState.Status == FRAME_STATE_RECEIVE_TIMEOUT){
      _State = SOCKET_STATE_DISCONNECTED;
   }
}

//------------------------------------------------------------------------------
//   Close
//------------------------------------------------------------------------------

void SocketIfUartClose( TSocket *Socket)
// Close socket
{
   _State = SOCKET_STATE_DISCONNECTED;
   Occupied = NO;
}

//------------------------------------------------------------------------------
//   Socket state
//------------------------------------------------------------------------------

byte SocketIfUartState( TSocket *Socket)
{
   return _State;
}

//------------------------------------------------------------------------------
//   RX size
//------------------------------------------------------------------------------

int SocketIfUartReceiveSize( TSocket *Socket)
{
   return _FrameState.SizeProccessed;
}

//------------------------------------------------------------------------------
//   Rx data
//------------------------------------------------------------------------------

TYesNo SocketIfUartReceive( TSocket *Socket, void *Buffer, int Size)
{
   if( !FrameReceiveStart( &_FrameState, Buffer, Size)){
      return NO;
   }
   _State = SOCKET_STATE_RECEIVE_ACTIVE;
   return YES;
}

//------------------------------------------------------------------------------
//   TX
//------------------------------------------------------------------------------

TYesNo SocketIfUartSend( TSocket *Socket, const void *Buffer, int Size)
{
   if(!FrameSendInit(&_FrameState, Buffer, Size)) {
      return NO;
   }
   _State = SOCKET_STATE_SEND_ACTIVE;
   return YES;
}
