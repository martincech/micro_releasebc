//*****************************************************************************
//
//   IRtcTest.c        Kinetis Real Time Clock Tests
//   Version 1.0       (c) VEIT Electronics
//
//*****************************************************************************

#include "Unity/unity_fixture.h"
#include "Unity/User.h"
#include "Rtc/Rtc.h"
#include "System/System.h"
#include "Hardware.h"

TEST_GROUP( Rtc);

TEST_SETUP( Rtc)
{
}

TEST_TEAR_DOWN( Rtc)
{
}

//-----------------------------------------------------------------------------
//  Running
//-----------------------------------------------------------------------------

#define RTC_TEST_TIME         5

TEST( Rtc, Running)
{
UDateTimeGauge Start, Now, Delta;

   RtcInit();

   Start = RtcLoad();
   RtcSave( Start);
   printf("Start: %d\n", Start);
   SysDelay( RTC_TEST_TIME * 1000);
   Now = RtcLoad();
   Delta = Now - Start;
   printf("Stop: %d\n", Now);

   TEST_ASSERT_INT_WITHIN(1, RTC_TEST_TIME, Delta);
}

//-----------------------------------------------------------------------------
//  Overflow
//-----------------------------------------------------------------------------

TEST( Rtc, Overflow)
{
UDateTimeGauge Start, Now, Delta;

   RtcInit();
   RtcSave(0xFFFFFFFF); // near overflow

   SysDelay(RTC_TEST_TIME * 1000);

   Now = RtcLoad(); // should overflow
   Delta = Now - 0xFFFFFFFF;

   TEST_ASSERT_INT_WITHIN(1, RTC_TEST_TIME, Delta);
}

//******************************************************************************

TEST_GROUP_RUNNER( Rtc)
{
   RUN_TEST_CASE( Rtc, Running);
   RUN_TEST_CASE( Rtc, Overflow);
}
