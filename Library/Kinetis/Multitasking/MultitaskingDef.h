//*****************************************************************************
//
//    MultitaskingDef.h   MultitaskingDef
//    Version 1.0        (c) Veit Electronics
//
//*****************************************************************************

#ifndef __MultitaskingDef_H__
   #define __MultitaskingDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#include <ctl.h>

typedef byte TMutex;

typedef struct sTTask{
   CTL_TASK_t Task;
   dword Event;
   dword EventLong;
   TYesNo Idle;
   void (*EntryPoint)(struct sTTask *);
   void *Stack;
   int StackSize;
} TTask;

#endif