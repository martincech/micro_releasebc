//******************************************************************************
//
//    SystemTimer.c  System timer
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Cpu/Cpu.h"
#include "Hardware.h"

// timer interrupt :
#define timerEnable()        InterruptEnable();
#define timerDisable()       InterruptDisable();
/*
   Reading SysTick->CTRL clears COUNTFLAG => pending interrupt lost
   To be sure no interrupt lose, don't enable/disable timer by setting/clearing TICKINT in SysTick->CTRL
*/

#define LPTMR_PRESCALE  2

void __irq LPTimer_IRQHandler( void);

// timer initialization :
static inline void timerInit() {
   SIM->SCGC5 |= SIM_SCGC5_LPTIMER_MASK;
   SIM->SOPT1 |= SIM_SOPT1_OSC32KSEL_MASK; // RTC as ERCLK32K

   LPTMR0->PSR = LPTMR_PSR_PRESCALE(0) | LPTMR_PSR_PCS(2); // ERCLK32K as source for LPTMR

   LPTMR0->CMR = 32678ull * TIMER_PERIOD / LPTMR_PRESCALE / 1000;

   LPTMR0->CSR = LPTMR_CSR_TIE_MASK | LPTMR_CSR_TEN_MASK;
   CpuIrqAttach(LPTimer_IRQn, 3, LPTimer_IRQHandler);
   CpuIrqEnable(LPTimer_IRQn);
}        //timerPeriodSet();SysTick->VAL = 0;SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk;timerStart();timerEnable()

/*#define SysTickCycles() (1LL * TIMER_PERIOD * F_CPU / 1000)

#if SysTickCycles() > 0x00FFFFFF
   #error Too long timer period
#endif*/

// timer period :
//#define timerPeriodSet()     SysTick->LOAD = SysTickCycles()

// timer start :
//#define timerStart()         SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;

// timer handler header :
#define timerHandler()       void __irq LPTimer_IRQHandler( void)

// timer interrupt confirm :
#define timerInterruptConfirm()    (LPTMR0->CSR |= LPTMR_CSR_TCF_MASK)
