//******************************************************************************
//
//    SystemTimer.c  System timer
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Cpu/Cpu.h"
#include "Hardware.h"

// timer interrupt :
#define timerEnable()        MySystickEnable(YES);
#define timerDisable()       MySystickEnable(NO);

// timer initialization :
#define timerInit()

// run user executive
#define timerExecute()       SysTimerExecute()                            

// timer handler header :
#define timerHandler()       void CtlSysTick( void)

// timer interrupt confirm :
#define timerInterruptConfirm()
