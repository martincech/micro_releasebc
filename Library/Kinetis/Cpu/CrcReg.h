//*****************************************************************************
//
//    CrcReg.h      Kinetis CRC register control
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __CrcReg_H__
   #define __CrcReg_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//-----------------------------------------------------------------------------
//  Constants
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//  Functions
//-----------------------------------------------------------------------------

#define crc16Init( Crc, Poly, Residue)       (Crc)->CTRL &= ~CRC_CTRL_TCRC_MASK;(Crc)->GPOLY = (Poly);(Crc)->CTRL |= CRC_CTRL_WAS_MASK;(Crc)->CRC = (Residue);(Crc)->CTRL &= ~CRC_CTRL_WAS_MASK
#define crc16Calc( Crc, Byte)                (Crc)->CRC = (Byte);
#define crc16Result( Crc)                    (Crc)->CRC & 0x0000FFFF

#endif
