//*****************************************************************************
//
//    F.h           Kinetis frequency calculations
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

/*
   F_xxx - real clock speed
   F_xxx_NOMINAL - nominal clock speed
   F_xxx_SET - desired clock speed

   Inputs:
   Define F_CPU_SET or F_CPU to set cpu clock speed.
   Optionally define F_xxx_SET (xxx = {BUS, FLASH, FLEXBUS}) to set respective clock speed.
   If F_xxx_SET not defined, defaults are used.

   Outputs:
   F_xxx_NOMINAL nominal clock speed - doesn't neccesarilly give real clock speed
   If not defined previously, F_CPU is defined as F_CPU_NOMINAL.
*/

#ifndef __F_H__
   #define __F_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif






#if !defined(F_SRC_EXTERNAL) && !defined(F_SRC_PLL) && !defined(F_SRC_FLL)
   #define F_SRC_FLL
#endif


#define FLL 0
#define EXTERNAL 1
#define PLL 2

#if defined(F_SRC_FLL)
   #define F_SRC     FLL
#elif defined(F_SRC_EXTERNAL)
   #define F_SRC     EXTERNAL
#elif defined(F_SRC_PLL)
   #define F_SRC     PLL
#else
   #define F_SRC     FLL
#endif


#ifdef F_OSC
   #define F_EXTERNAL_CLOCK     F_OSC

   #if F_OSC == 32768
      #define RANGE 0
   #elif F_OSC >= 3000000ul && F_OSC <= 8000000ul
      #define RANGE 1
   #elif F_OSC > 8000000ul && F_OSC <= 32000000ul
      #define RANGE 2   
   #else
      #error Unsupported crystal
   #endif
#elif defined F_EXT_CLK
   #define F_EXTERNAL_CLOCK     F_EXT_CLK

   #if F_EXT_CLK == 32768
      #define RANGE 0
   #elif F_EXT_CLK >= 3000000ul && F_EXT_CLK <= 8000000ul
      #define RANGE 1
   #elif F_EXT_CLK > 8000000ul
      #define RANGE 2   
   #else
      #error Unsupported external clock
   #endif
#endif


#define FREQUENCY_OUT_OF_RESET         20970000ull

#if !defined(F_CPU) && !defined(F_CPU_SET)
   #if F_SRC == FLL
      #define F_CPU_SET    FREQUENCY_OUT_OF_RESET
   #elif F_SRC == EXTERNAL
      #define F_CPU_SET    F_EXTERNAL_CLOCK
   #else
      #error F_CPU not defined
   #endif
#endif

#ifndef F_CPU_SET
   #define F_CPU_SET       F_CPU
#endif

// Defaults
#ifndef F_BUS_SET
   #define F_BUS_SET     F_CPU_SET

   #if F_BUS_SET > F_BUS_MAX
      #undef F_BUS_SET
      #define F_BUS_SET  (F_CPU_SET / 2)
   #endif
#endif

#ifndef F_FLASH_SET
   #define F_FLASH_SET     F_BUS_SET

   #if F_FLASH_SET > F_FLASH_MAX
      #undef F_FLASH_SET
      #define F_FLASH_SET  (F_BUS_SET / 2)
   #endif
#endif

#ifndef F_FLEXBUS_SET
   #define F_FLEXBUS_SET   F_BUS_SET
#endif

// Check validity of clocks
#define IsIntegerDivideOf(Lower, Higher)     ((Higher / Lower) * Lower == Higher)

#if F_CPU_SET > F_CPU_MAX
   #error Max F_CPU frequency exceeded
#endif

#if F_BUS_SET > F_BUS_MAX
   #error Max F_BUS frequency exceeded
#endif

#if !IsIntegerDivideOf(F_BUS_SET, F_CPU_SET)
   #error F_BUS is not integer divide of F_CPU
#endif

#if F_FLASH_SET > F_FLASH_MAX || F_FLASH_SET > F_BUS_SET
   #error Max F_FLASH frequency exceeded
#endif

#if !IsIntegerDivideOf(F_FLASH_SET, F_CPU_SET)
   #error F_FLASH is not integer divide of F_CPU_SET
#endif

#if F_FLEXBUS_SET > F_BUS_SET
   #error F_FLEXBUS must be less than or equal to the F_BUS
#endif

// -------------------------- FLL -------------------------------

#define FLL_REFERENCE_CLOCK      32678ul

#if F_SRC == EXTERNAL || F_SRC == PLL
   #define FLL_INPUT_CLOCK          F_EXTERNAL_CLOCK
   #define FLL_REFERENCE_DIVIDER    (FLL_INPUT_CLOCK / FLL_REFERENCE_CLOCK)
   //#define FLL_OUT_CLOCK_NOMINAL  doesnt matter, will use external or pll
#else
   // only FEI mode with default-after-reset settings is implemented
   #define FLL_INPUT_CLOCK          F_SLOW_INTERNAL
   //#define FLL_REFERENCE_DIVIDER  doesnt matter
   #define FLL_OUT_CLOCK_NOMINAL    FREQUENCY_OUT_OF_RESET
#endif

// -------------------------- External -------------------------------

#if F_SRC == EXTERNAL

#define EXTERNAL_OUT_CLOCK_NOMINAL    F_EXTERNAL_CLOCK

#endif

// -------------------------- PLL -------------------------------

#if F_SRC == PLL

#define F_PLL_INPUT_CLOCK  F_EXTERNAL_CLOCK

#define F_PLL_REFERENCE_CLOCK    2000000ul

#if F_PLL_INPUT_CLOCK < F_PLL_REFERENCE_CLOCK
   #error PLL input clock must be >= 2MHz
#endif

// Pll reference divider and multiplier
#define PLL_REFERENCE_DIVIDER    (F_PLL_INPUT_CLOCK / F_PLL_REFERENCE_CLOCK)
#define PLL_MULTIPLIER           (F_CPU_SET / F_PLL_REFERENCE_CLOCK)

#if PLL_MULTIPLIER < PLL_MULTIPLIER_MIN
   #undef  PLL_MULTIPLIER
   #define PLL_MULTIPLIER     PLL_MULTIPLIER_MIN
#endif

#define PLL_OUT_CLOCK_NOMINAL   F_PLL_INPUT_CLOCK / PLL_REFERENCE_DIVIDER * PLL_MULTIPLIER

#endif


// MCG output
#if F_SRC == FLL
   #define MCG_OUT_CLOCK_NOMINAL FLL_OUT_CLOCK_NOMINAL
#elif F_SRC == EXTERNAL
   #define MCG_OUT_CLOCK_NOMINAL EXTERNAL_OUT_CLOCK_NOMINAL
#elif F_SRC == PLL
   #define MCG_OUT_CLOCK_NOMINAL PLL_OUT_CLOCK_NOMINAL
#endif





// Dividers
#define CPU_DIVIDER     ((MCG_OUT_CLOCK_NOMINAL - 1) / F_CPU_SET + 1)
#define BUS_DIVIDER     (F_CPU_SET / (F_BUS_SET / CPU_DIVIDER))
#define FLEXBUS_DIVIDER (F_CPU_SET / (F_FLEXBUS_SET / CPU_DIVIDER))
#define FLASH_DIVIDER   (F_CPU_SET / (F_FLASH_SET / CPU_DIVIDER))

#if CPU_DIVIDER > CLOCK_DIVIDER_MAX || BUS_DIVIDER > CLOCK_DIVIDER_MAX || FLEXBUS_DIVIDER > CLOCK_DIVIDER_MAX || FLASH_DIVIDER > CLOCK_DIVIDER_MAX
   #error Divider too high
#endif

// Clocks
#define F_CPU_NOMINAL           MCG_OUT_CLOCK_NOMINAL / CPU_DIVIDER
#define F_SYSTEM_NOMINAL        F_CPU_NOMINAL
#define F_BUS_NOMINAL           MCG_OUT_CLOCK_NOMINAL / BUS_DIVIDER
#define F_FLASH_NOMINAL         MCG_OUT_CLOCK_NOMINAL / FLASH_DIVIDER
#define F_FLEXBUS_NOMINAL       MCG_OUT_CLOCK_NOMINAL / FLEXBUS_DIVIDER

#ifndef F_CPU
   #define F_CPU    F_CPU_NOMINAL
#endif

#define MCG_OUT_CLOCK   (F_CPU * CPU_DIVIDER)

#define F_SYSTEM     F_CPU
#define F_BUS        MCG_OUT_CLOCK / BUS_DIVIDER
#define F_FLASH      MCG_OUT_CLOCK / FLASH_DIVIDER
#define F_FLEXBUS    MCG_OUT_CLOCK / FLEXBUS_DIVIDER

#endif