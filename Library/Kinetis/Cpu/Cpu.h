//*****************************************************************************
//
//    Cpu.h        Kinetis CPU services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Cpu_H__
   #define __Cpu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#include "Clock/Clock.h"

// interrupt function attribute :
#define __irq    __attribute__((__interrupt__))
#define __abort  __attribute__ ( (interrupt("ABORT")))

// interrupt handler signature :
typedef void TIrqHandler( void);

#ifdef SLEEP_ENABLE
   #define CpuIdleMode()    Wait()//SMC->PMCTRL = SMC_PMCTRL_STOPM(0); Stop()
   // Enter IDLE mode
#else
   #define CpuIdleMode()
#endif

//-----------------------------------------------------------------------------
// Startup
//-----------------------------------------------------------------------------

void CpuInit( void);
// Cpu startup initialisation

#define CpuReset  NVIC_SystemReset

#define CpuIsSoftwareReset()        (RCM->SRS1 & RCM_SRS1_SW_MASK)

//-----------------------------------------------------------------------------
// Interrupt controller
//-----------------------------------------------------------------------------

static inline void CpuIrqAttach( int Irq, int Priority, TIrqHandler *Handler)
// Install IRQ <Handler> at <Irq> with <Priority> level
{
#ifdef VECTORS_IN_RAM
   ((uint32 *)SCB->VTOR)[16+Irq] = (uint32)Handler;
#endif
   NVIC_SetPriority ( Irq, Priority);

}

// ARM Core System exceptions are enabled all the time, can't be disabled

#define CpuIrqEnable( Irq)   NVIC_EnableIRQ( Irq)
// Enable <Irq>

#define CpuIrqDisable( Irq)    NVIC_DisableIRQ( Irq)
// Disable <Irq>

//-----------------------------------------------------------------------------
// Watch dog
//-----------------------------------------------------------------------------

#include "Cpu/WatchDog.h"

//-----------------------------------------------------------------------------
// Intrinsics
//-----------------------------------------------------------------------------

#include <intrinsics.h>

#define Nop()     __nop()
#define Stop()    SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk; __WFI()
#define Wait()    SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk; __WFI()

#endif
