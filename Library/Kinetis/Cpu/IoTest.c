#include "Unity/unity_fixture.h"
#include "Cpu/Kinetis.h"

#define LED_RED    KINETIS_PIN_PORTB04
#define LED_GREEN  KINETIS_PIN_PORTB05

static void Wait( void) {
   int i;
   for(i = 0 ; i < 0x7FFFF ; i++) {}
}

TEST_GROUP(Gpio);

TEST_SETUP(Gpio)
{
}

TEST_TEAR_DOWN(Gpio)
{
}

TEST(Gpio, PinNumbering)
{
   int Gpio = KINETIS_PIN_PORTB04;

   int PortNumber = Gpio / PORT_OFFSET;
   int PinNumber = Gpio % PORT_OFFSET;

   TEST_ASSERT_EQUAL_INT(PortNumber, KINETIS_PORTB);
   TEST_ASSERT_EQUAL_INT(PinNumber, 4);
}

static void TestPtAddress(int Gpio, GPIO_Type *ExpectedAddress) {
   GPIO_Type *Pt = PT(Gpio / PORT_OFFSET);

   TEST_ASSERT_EQUAL_PTR(Pt, ExpectedAddress);
}

TEST(Gpio, PtAddress)
{
   TestPtAddress(KINETIS_PIN_PORTA00, (GPIO_Type *)PTA_BASE);
   TestPtAddress(KINETIS_PIN_PORTA05, (GPIO_Type *)PTA_BASE);
   TestPtAddress(KINETIS_PIN_PORTB00, (GPIO_Type *)PTB_BASE);
   TestPtAddress(KINETIS_PIN_PORTB05, (GPIO_Type *)PTB_BASE);
}

static void TestPortAddress(int Gpio, PORT_Type *ExpectedAddress) {
   PORT_Type *Port = PORT(Gpio / PORT_OFFSET);

   TEST_ASSERT_EQUAL_PTR(Port, ExpectedAddress);
}

TEST(Gpio, PortAddress)
{
   TestPortAddress(KINETIS_PIN_PORTA00, (PORT_Type *)PORTA_BASE);
   TestPortAddress(KINETIS_PIN_PORTA04, (PORT_Type *)PORTA_BASE);
   TestPortAddress(KINETIS_PIN_PORTB00, (PORT_Type *)PORTB_BASE);
   TestPortAddress(KINETIS_PIN_PORTB04, (PORT_Type *)PORTB_BASE);
}

TEST(Gpio, OutputToggleLedsBlinkSlow)
{
   PinFunction( LED_RED, KINETIS_GPIO_FUNCTION);
   PinFunction( LED_GREEN, KINETIS_GPIO_FUNCTION);
   GpioOutput( LED_RED);
   GpioOutput( LED_GREEN);

   GpioToggle( LED_RED);
   Wait();
   Wait();
   Wait();
   GpioToggle( LED_GREEN);
   Wait();
   Wait();
   Wait();
   GpioToggle( LED_RED);
   Wait();
   Wait();
   Wait();
   GpioToggle( LED_GREEN);
   Wait();
   Wait();
   Wait();
}

static void DoBlink( void) {
   GpioSet( LED_RED);
   Wait();
   GpioSet( LED_GREEN);
   Wait();
   GpioClr( LED_RED);
   Wait();
   GpioClr( LED_GREEN);
   Wait();
}

TEST(Gpio, OutputClearSetLedsBlinkFast)
{
   PinFunction( LED_RED, KINETIS_GPIO_FUNCTION);
   PinFunction( LED_GREEN, KINETIS_GPIO_FUNCTION);
   GpioOutput( LED_RED);
   GpioOutput( LED_GREEN);

   DoBlink();
}

TEST(Gpio, WrongPinFunctionOneLedWontBlink)
{
   PinFunction( LED_RED, KINETIS_GPIO_FUNCTION + 1);
   PinFunction( LED_GREEN, KINETIS_GPIO_FUNCTION);
   GpioOutput( LED_RED);
   GpioOutput( LED_GREEN);

   DoBlink();
}

TEST(Gpio, WrongDirectionOneLedWontBlink)
{
   PinFunction( LED_RED, KINETIS_GPIO_FUNCTION);
   PinFunction( LED_GREEN, KINETIS_GPIO_FUNCTION);
   GpioOutput( LED_RED);
   GpioInput( LED_GREEN);

   DoBlink();
}

TEST(Gpio, PinRead)
{
   PinFunction( LED_RED, KINETIS_GPIO_FUNCTION);
   PinFunction( LED_GREEN, KINETIS_GPIO_FUNCTION);
   GpioOutput( LED_RED);
   GpioOutput( LED_GREEN);

   GpioClr( LED_GREEN);
   GpioSet( LED_RED);
   TEST_ASSERT_TRUE(GpioGet(LED_RED));
   TEST_ASSERT_FALSE(GpioGet(LED_GREEN));
   GpioClr( LED_RED);
   TEST_ASSERT_FALSE(GpioGet(LED_RED));
   TEST_ASSERT_FALSE(GpioGet(LED_GREEN));

   GpioSet( LED_RED);
   GpioSet( LED_GREEN);
   TEST_ASSERT_TRUE(GpioGet(LED_GREEN));
   TEST_ASSERT_TRUE(GpioGet(LED_RED));
   GpioClr( LED_GREEN);
   TEST_ASSERT_FALSE(GpioGet(LED_GREEN));
   TEST_ASSERT_TRUE(GpioGet(LED_RED));
}

TEST_GROUP_RUNNER(Gpio)
{
   PortsEnable();
   RUN_TEST_CASE(Gpio, PinNumbering);
   RUN_TEST_CASE(Gpio, PtAddress);
   RUN_TEST_CASE(Gpio, PortAddress);
   RUN_TEST_CASE(Gpio, OutputToggleLedsBlinkSlow);
   RUN_TEST_CASE(Gpio, OutputClearSetLedsBlinkFast);
   RUN_TEST_CASE(Gpio, WrongPinFunctionOneLedWontBlink);
   RUN_TEST_CASE(Gpio, WrongDirectionOneLedWontBlink);
   RUN_TEST_CASE(Gpio, PinRead);
}
