//*****************************************************************************
//
//    WatchDog.h     Kxx WatchDog
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __WatchDog_H__
   #define __WatchDog_H__

#include "Unisys/Uni.h"

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void WatchDogInit( void);
// Initialize watchdog

void WatchDogDisable( void);
// Stop watchdog

void WatchDogStopModeEnable( TYesNo Enable);
// Enable/disable watchdog in stop mode

void WatchDog( void);
// Refresh watchdog

#endif