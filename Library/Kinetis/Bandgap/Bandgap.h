//*****************************************************************************
//
//    Bandgap.h      VREF bandgap
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Bandgap_H__
   #define __Bandgap_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

void BandgapInit( void);
// Initialization

TYesNo BandgapReady( void);
// Check ready

void BandgapShutdown( void);
// Shutdown

#endif
