//*****************************************************************************
//
//    Comparator.h   Comparator
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Comparator_H__
   #define __Comparator_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

void ComparatorInit( void);
// Init

TYesNo ComparatorGet( void);
// Get comparator output

void ComparatorShutdown( void);
// Shutdown

#endif
