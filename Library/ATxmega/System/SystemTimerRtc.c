//******************************************************************************
//
//    SystemTimer.c  System timer
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "Hardware.h"
#include "Rtc/Rtc.h"
#include "System/System.h"

// timer interrupt :
#define timerEnable()        
#define timerDisable()       

// timer initialization :
#define timerInit()           RtcInit()

// run user executive
#define timerExecute()       SysTimerExecute()                            

// timer handler header :
#define timerHandler()       ISR(RTC_OVF_vect)

// timer interrupt confirm :
#define timerInterruptConfirm()
