//******************************************************************************
//
//   ParseLine.cpp Simple line parsing utility
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include "parseline.h"
#include <string.h>
#include <stdio.h>

//------------------------------------------------------------------------------
//  hexadecimal
//------------------------------------------------------------------------------

void ParseLine::hexadecimal( byte *data, const char *line, int *size, int maximumSize)
// Parse hexadecimal <line>, returns <data> with <size>
{
   const char *p = line;
   unsigned   hex;
   int        length = 0;
   forever {
      if( sscanf( p, "%X", &hex) != 1){
         break;
      }
      if( length >= maximumSize - 1){
         break;
      }
      data[ length] = (byte)hex;
      length++;                        // next digit
      p = strchr( p, ' ');             // next field
      if( !p){
         break;                        // end of string
      }
      p++;                             // skip space
   }
   *size = length;
} // hexadecimal

//------------------------------------------------------------------------------
//  Escape string
//------------------------------------------------------------------------------

void ParseLine::escapeString( byte *data, const char *line, int *size)
// Parse <string> with C-like escape characters, returns <data> with <size>
{
   int length = strlen( line);
   memcpy( data, line, length + 1);
   for( int i = 0; i < length; i++){
      if( data[ i] == '\\'){
         // escape sekvence
         switch( data[ i + 1]){
            case '\\' :
               memmove( &data[ i + 1], &data[ i + 2], length - i - 1); // vc. koncove nuly
               length--;
               break;  // jen opacne lomitko
            case 'r' :
               memmove( &data[ i + 1], &data[ i + 2], length - i - 1);
               data[ i] = '\r';
               length--;
               break;
            case 'n' :
               memmove( &data[ i + 1], &data[ i + 2], length - i - 1);
               data[ i] = '\n';
               length--;
               break;
            case 't' :
               memmove( &data[ i + 1], &data[ i + 2], length - i - 1);
               data[ i] = '\t';
               length--;
               break;
            case 'f' :
               memmove( &data[ i + 1], &data[ i + 2], length - i - 1);
               data[ i] = '\f';
               length--;
               break;
            case '0' :
               memmove( &data[ i + 1], &data[ i + 2], length - i - 1);
               data[ i] = '\0';
               length--;
               break;
            case 'x' :
               unsigned hex;
               if( sscanf( (const char *)&data[ i + 2], "%02X", &hex) != 1){
                  break;
               }
               memmove( &data[ i + 1], &data[ i + 4], length - i - 3);
               data[ i] = hex;
               length -= 3;
               break;
            default :
               break;
         } // switch
      }
   }
   *size = length;
} // escapeString
