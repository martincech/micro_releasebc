//******************************************************************************
//
//   WinUart.cpp  Asynchronous serial device for Win32
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "WinUart.h"

#include <string.h>
#include <stdio.h>

// Implementation constants :

#define UART_TX_TIMEOUT  1000          // fixed part of Tx timeout [ms]

#define COM_NUMBER_MIN   1
#define COM_NUMBER_MAX  99

#define portIsOpen()   (_handle != INVALID_HANDLE_VALUE)
#define if_errh( f)     if( (f) == INVALID_HANDLE_VALUE)

#define nameClear()       strcpy( _name, "")
#define nameIsValid()   ( _name[ 0] != '\0')

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------

WinUart::WinUart()
// Constructor
{
   _handle = INVALID_HANDLE_VALUE;
   _name[0]='\0';
   nameClear();
   setDefaultParameters();
} // WinUart

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------

WinUart::~WinUart()
// Destructor
{
   close();
} //~WinUart

//------------------------------------------------------------------------------
// Port Set
//------------------------------------------------------------------------------

TYesNo WinUart::setPort( const char *portName)
// Set <portName>
{
   close();                            // terminate port activity
   nameClear();                        // set name invalid
//   setDefaultParameters();             // prepare defaults
   // check for valid name :
   if( strlen( portName) < 4){
      return( NO);
   }
   // prefix :
   if( portName[ 0] != 'C' ||
       portName[ 1] != 'O' ||
       portName[ 2] != 'M'){
      return( NO);
   }
   // COM number :
   int Number = atoi( &portName[ 3]);
   if( Number < COM_NUMBER_MIN ||
       Number > COM_NUMBER_MAX){
      return( NO);
   }
   // System COM name :
   if( Number <= 9){
      strcpy( _name, portName);         // save port name
   } else {
      // Win32 port name mangling
      sprintf( _name, "\\\\.\\COM%d", Number);
   }
//   // check for port access :
//   HANDLE tmpHandle;
//   tmpHandle = CreateFileA( _name, GENERIC_READ | GENERIC_WRITE,
//                            0, NULL, OPEN_EXISTING, 0, NULL);
//   if_errh( tmpHandle){
//      nameClear();
//      return( NO);                     // unable open port
//   }
//   CloseHandle( tmpHandle);            // close port
   return( YES);
} // setPort

const char *WinUart::getPort()
// Get COM <portName>
{
   int i;
   int len = strlen(_name) - 3; // length of COM word
   for(i = 0; i < len; i++){
      if(strncmp("COM", &_name[i], 3) == 0){
         return &_name[i];
      }
   }
   return NULL;
}


//------------------------------------------------------------------------------
// Baud rate
//------------------------------------------------------------------------------

TYesNo WinUart::setBaudRate( int baudRate)
// set <baudRate>
{
   // the CBR_xxx are identical with <baudRate>
   // but check for acceptable values :
   switch( baudRate){
      case 110 :
         _baudRate = CBR_110;
         break;
      case 300 :
         _baudRate = CBR_300;
         break;
      case 600 :
         _baudRate = CBR_600;
         break;
      case 1200 :
         _baudRate = CBR_1200;
         break;
      case 2400 :
         _baudRate = CBR_2400;
         break;
      case 4800 :
         _baudRate = CBR_4800;
         break;
      case 9600 :
         _baudRate = CBR_9600;
         break;
      case 14400 :
         _baudRate = CBR_14400;
         break;
      case 19200 :
         _baudRate = CBR_19200;
         break;
      case 38400 :
         _baudRate = CBR_38400;
         break;
      case 57600 :
         _baudRate = CBR_57600;
         break;
      case 115200 :
         _baudRate = CBR_115200;
         break;
      default :
         return( NO);                  // invalid baudrate
   }
   safeTransmitterTiming();            // prepare Tx timeouts (depends on baudRate)
   close();                            // reload parameters at next operation
   return( YES);
} // setBaudRate

int WinUart::getBaudRate()
// get <baudRate>
{
   return _baudRate;
}


//------------------------------------------------------------------------------
// Data bits
//------------------------------------------------------------------------------

TYesNo WinUart::setDataBits( int dataBits)
// set <dataBits> count 5..8
{
   if( dataBits < 5 || dataBits > 8){
      return( NO);                     // invalid size
   }
   _dataBits = (BYTE)dataBits;
   safeTransmitterTiming();            // prepare Tx timeouts (depends on dataBits)
   close();                            // reload parameters at next operation
   return( YES);
} // setDataBits

int WinUart::getDataBits()
// get <dataBits>
{
   return _dataBits;
}

//------------------------------------------------------------------------------
// Stop bits
//------------------------------------------------------------------------------

TYesNo WinUart::setStopBits( EStopBits stopBits)
// set <sits>
{
   switch( stopBits){
      case STOPBIT_1 :
         _stopBits = ONESTOPBIT;
         break;
      case STOPBIT_15 :
         _stopBits = ONE5STOPBITS;
         break;
      case STOPBIT_2 :
         _stopBits = TWOSTOPBITS;
         break;
      default :
         return( NO);                  // invalid stop bits count
   }
   close();                            // reload parameters at next operation
   return( YES);
} // setStopBits

WinUart::EStopBits WinUart::getStopBits()
// get <stopBits>
{
   switch( _stopBits){
      default :
         return STOPBIT_1;
      case ONE5STOPBITS :
         return STOPBIT_15;
      case TWOSTOPBITS :
         return STOPBIT_2;
   }
}

//------------------------------------------------------------------------------
// Parity
//------------------------------------------------------------------------------

TYesNo WinUart::setParity( EParity parity)
// set <parity>
{
   switch( parity){
      case PARITY_NONE :
         _parity = NOPARITY;
         break;
      case PARITY_ODD :
         _parity = ODDPARITY;
         break;
      case PARITY_EVEN :
         _parity = EVENPARITY;
         break;
      case PARITY_MARK :
         _parity = MARKPARITY;
         break;
      case PARITY_SPACE :
         _parity = SPACEPARITY;
         break;
      default :
         return( NO);                  // invalid parity
   }
   close();                            // reload parameters at next operation
   return( YES);
} // setParity

WinUart::EParity WinUart::getParity()
// get <parity>
{
   switch( _parity){
      default:
         return PARITY_NONE;
      case ODDPARITY:
         return PARITY_ODD;
      case EVENPARITY:
         return PARITY_EVEN;
      case MARKPARITY:
         return PARITY_MARK;
      case SPACEPARITY:
         return PARITY_SPACE;
   }
}

TYesNo WinUart::setFlowControl( EFlowControl flow)
// set flow control mode
{
   switch( flow){
      case FLOW_RTS_CTS:
         _flowControl = flow;
         break;
      default:
         _flowControl = FLOW_NONE;
         break;
   }
   return YES;
}

WinUart::EFlowControl WinUart::getFlowControl()
// get flow control mode
{
   return _flowControl;
}

//------------------------------------------------------------------------------
// Rx timing
//------------------------------------------------------------------------------

TYesNo WinUart::setReceiveTimeout( int replyTime, int intercharacterTime)
// Set timing of receiver :
// If <intercharacterTime> = 0 waits <replyTime> for whole message
// If <intercharacterTime> > 0 waits <replyTime> for first
// character, next waits <intercharacterTime> * count of characters
{
   if( replyTime == 0 && intercharacterTime == 0){
      return( NO);
   }
   if( replyTime <= intercharacterTime){
      // too short for Win32
      replyTime = 10 * intercharacterTime;  // patch it
   }
   _immediateRead      = NO;                // wait for receive
   _replyTime          = replyTime;
   _intercharacterTime = intercharacterTime;
   safeTransmitterTiming();                 // prepare Tx timeouts
   return( setTimeouts());                  // set port timeouts
} // setReceiveTimeout

//------------------------------------------------------------------------------
//  Rx nowait
//------------------------------------------------------------------------------

TYesNo WinUart::setReceiveNowait()
// Set timing of receiver - returns collected data immediately
{
   _immediateRead      = YES;               // receive without wait
   _replyTime          = 0;
   _intercharacterTime = 0;
   safeTransmitterTiming();                 // prepare Tx timeouts
   return( setTimeouts());                  // set port timeouts
} // setReceiveNowait

//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------

TYesNo WinUart::send( const void *data, int size)
// Send <data> with <size>, returns written size (or 0 if error)
{
   if( !checkOpen()){
      return( NO);
   }
   DWORD w;
   if( !WriteFile( _handle, data, size, &w, NULL)){
      close();                         // unable write, try reopen
      return( NO);                      // timeout or error
   }
   if( w != (DWORD)size){
      close();                         // wrong size, try reopen
      return( NO);
   }
   return( YES);
} // send

//------------------------------------------------------------------------------
// Read
//------------------------------------------------------------------------------

int WinUart::receive( void *data, int size)
// Receive data <data> with <size>, returns received size (or 0 if error)
{
   if( !checkOpen()){
      return( 0);
   }
   if( size == 0){
      return( 0);                      // implementation error
   }
   DWORD r;
   // check for immediate read :
   if( _immediateRead){
      // read without wait
      if( !ReadFile( _handle, data, size, &r, NULL)){
         return( 0);
      }
      return( r);
   }
   // wait for first character :
   if( !ReadFile( _handle, data, 1, &r, NULL)){
      return( 0);
   }
   if( r != 1){
      return( 0);
   }
   if( size == 1){
      return( 1);            // requested 1 byte only
   }
   // read remainder :
   size--;
   byte *p = (byte *)data;
   p++;                      // skip first byte
   if( !ReadFile( _handle, p, size, &r, NULL)){
      return( 0);
   }
   return( r+1);
} // receive

//------------------------------------------------------------------------------
//  Send wait
//------------------------------------------------------------------------------

TYesNo WinUart::waitSend()
// Waits for send done
{
   // COM event system is unreliable, use with care
   if( !checkOpen()){
      return( 0);
   }
   // wait for transmitter empty :
   DWORD commEvent;
   if( !WaitCommEvent( _handle, &commEvent, NULL)){
      return( NO);
   }
   if( !(commEvent & EV_TXEMPTY)){
      return( NO);
   }
   // last character may be transmitted :
   if( _baudRate >= CBR_9600){
      Sleep( 1);                         // fixed 1ms for last character
      return( YES);
   }
   Sleep( (1000 * 10) / _baudRate + 1);  // wait for last character
   return( YES);
} // waitSend

//------------------------------------------------------------------------------
// Flush
//------------------------------------------------------------------------------

void WinUart::flush()
// Make input/output queue empty
{
   if( !checkOpen()){
      return;
   }
   PurgeComm( _handle, PURGE_RXCLEAR | PURGE_TXCLEAR);
} // flush

//------------------------------------------------------------------------------
// Disconnect
//------------------------------------------------------------------------------

void WinUart::disconnect()
// Disconnect/release COM device
{
   close();
} // disconnect

//------------------------------------------------------------------------------
// Break
//------------------------------------------------------------------------------

TYesNo WinUart::setBreak( TYesNo txBreak)
// Set <txBreak> signal at TxD
{
   if( !checkOpen()){
      return( NO);
   }
   if( !EscapeCommFunction( _handle, txBreak ? SETBREAK : CLRBREAK)){
      return( NO);
   }
   return( YES);
} // setBreak

//------------------------------------------------------------------------------
// DTR
//------------------------------------------------------------------------------

TYesNo WinUart::setDtr( TYesNo dtr)
// Set <dtr> signal
{
   if( !checkOpen()){
      return( NO);
   }
   if( !EscapeCommFunction( _handle, dtr ? SETDTR : CLRDTR)){
      return( NO);
   }
   return( YES);
} // setDtr

//------------------------------------------------------------------------------
// RTS
//------------------------------------------------------------------------------

TYesNo WinUart::setRts( TYesNo rts)
// Set <rts> signal
{
   if( !checkOpen()){
      return( NO);
   }
   if( !EscapeCommFunction( _handle, rts ? SETRTS : CLRRTS)){
      return( NO);
   }
   return( YES);
} // setRts

//------------------------------------------------------------------------------
// DSR
//------------------------------------------------------------------------------

TYesNo WinUart::getDtr()
// Returns DSR state
{
   if( !checkOpen()){
      return( NO);
   }
   return( YesNoGet( readModemStatus() & MS_DSR_ON));
} // getDsr

//------------------------------------------------------------------------------
// CTS
//------------------------------------------------------------------------------

TYesNo WinUart::getCts()
// Returns CTS state
{
   if( !checkOpen()){
      return( NO);
   }
   return( YesNoGet( readModemStatus() & MS_CTS_ON));
} // getCts

//------------------------------------------------------------------------------
// DCD
//------------------------------------------------------------------------------

TYesNo WinUart::getDcd()
// Returns DCD state
{
   if( !checkOpen()){
      return( NO);
   }
   return( YesNoGet( readModemStatus() & MS_RLSD_ON));
} // getDcd

//------------------------------------------------------------------------------
// RI
//------------------------------------------------------------------------------

TYesNo WinUart::getRing()
// Returns RI state
{
   if( !checkOpen()){
      return( NO);
   }
   return( YesNoGet( readModemStatus() & MS_RING_ON));
} // getRing

//------------------------------------------------------------------------------
// Modem status
//------------------------------------------------------------------------------

byte WinUart::getModemStatus()
// Returns modem status mask (EModemStatus)
{
   if( !checkOpen()){
      return( NO);
   }
   DWORD m;
   m = readModemStatus();
   byte  status = 0;
   if( m & MS_DSR_ON){
      status |= MODEM_DSR;
   }
   if( m & MS_CTS_ON){
      status |= MODEM_CTS;
   }
   if( m & MS_RLSD_ON){
      status |= MODEM_DCD;
   }
   if( m & MS_RING_ON){
      status |= MODEM_RI;
   }
   return( status);
} // getModemStatus

//******************************************************************************
// Protected
//******************************************************************************

//------------------------------------------------------------------------------
// Close
//------------------------------------------------------------------------------

void WinUart::close()
// Close device
{
   if( portIsOpen()){
      CloseHandle( _handle);
   }
   _handle = INVALID_HANDLE_VALUE;
} // close

//------------------------------------------------------------------------------
// Check Open
//------------------------------------------------------------------------------

TYesNo WinUart::checkOpen()
// Check for device Opened
{
   // check for opened :
   if( portIsOpen()){
      return( YES);
   }
   // check for valid name :
   if( !nameIsValid()){
      return( NO);
   }
   // open device :
   if_errh( _handle = CreateFileA( _name,GENERIC_READ | GENERIC_WRITE,
                                   0, NULL, OPEN_EXISTING, 0, NULL)){
      return( NO);
   }
   // device parameters :
   DCB dcb;
   // get original settings :
   if( !GetCommState( _handle, &dcb)){
      close();
      return( NO);
   }
   // device settings :
   dcb.BaudRate        = _baudRate;
   dcb.Parity          = _parity;
   dcb.ByteSize        = _dataBits;
   dcb.StopBits        = _stopBits;
   dcb.fInX            = FALSE;
   dcb.fOutX           = FALSE;
   dcb.fOutxDsrFlow    = FALSE;
   dcb.fDtrControl     = DTR_CONTROL_ENABLE;
   switch( _flowControl){
      case FLOW_RTS_CTS :
         dcb.fOutxCtsFlow    = TRUE;
         dcb.fRtsControl     = RTS_CONTROL_HANDSHAKE;
         break;
      default:
         dcb.fOutxCtsFlow    = FALSE;
         dcb.fRtsControl     = RTS_CONTROL_ENABLE;
         break;
   }
   dcb.fDsrSensitivity = NO;     // ignore DSR
   dcb.fAbortOnError   = FALSE;  // ignore invalid characters
   // New setup :
   if( !SetCommState( _handle, &dcb)){
      close();
      return( NO);
   }
   // Set timeouts :
   if( !setTimeouts()){
      close();
      return( NO);
   }
   // prepare Tx empty event catching :
   if( !SetCommMask( _handle, EV_TXEMPTY)){
      return( NO);
   }
   return( YES);
} // checkOpen


//------------------------------------------------------------------------------
//  Defaults
//------------------------------------------------------------------------------

void WinUart::setDefaultParameters()
// Set default parameters
{
   setBaudRate( 9600);
   setDataBits( 8);
   setStopBits( STOPBIT_1);
   setParity( PARITY_NONE);
   setReceiveNowait();
   safeTransmitterTiming();
} // setDefaultParameters

//------------------------------------------------------------------------------
//  Timeouts
//------------------------------------------------------------------------------

TYesNo WinUart::setTimeouts()
// Set timeouts
{
   // check for opened :
   if( !portIsOpen()){
      return( YES);                    // port closed
   }
   // Get timeouts :
   COMMTIMEOUTS to;
   GetCommTimeouts( _handle, &to);
   // maximum time for send :
   to.WriteTotalTimeoutMultiplier = _transmitterCharTime;
   to.WriteTotalTimeoutConstant   = UART_TX_TIMEOUT;
   if( _immediateRead){
      // return immediately if no chars present :
      to.ReadIntervalTimeout         =  MAXDWORD;
      to.ReadTotalTimeoutMultiplier  =  0;
      to.ReadTotalTimeoutConstant    =  0;
   } else {
      // intercharacter doesn't work properly, emulate it :
      to.ReadIntervalTimeout         =  _intercharacterTime;
      to.ReadTotalTimeoutMultiplier  =  1;
      to.ReadTotalTimeoutConstant    =  _replyTime;
   }
   // Set timeouts :
   if( !SetCommTimeouts( _handle, &to)){
      close();
      return( NO);
   }
   return( YES);
} // setTimeouts

//------------------------------------------------------------------------------
//  Tx timing
//------------------------------------------------------------------------------

void WinUart::safeTransmitterTiming()
// Set safe timing for transmitter
{
   // Calculate via [us] :
   int charTime = 1;
   if( _baudRate > 0){
      charTime = (1000000L / _baudRate) * _dataBits + 3;
      if( charTime < 1){
         charTime = 1;
      }
   }
   // convert to [ms] :
   charTime /= 1000;
   if( charTime == 0){
      charTime = 1;
   }
   charTime *= 2;                      // security multiplier
   _transmitterCharTime = charTime;    // remember
} // safeTransmitterTiming

//------------------------------------------------------------------------------
//  Modem status
//------------------------------------------------------------------------------

DWORD WinUart::readModemStatus()
// Read modem status
{
   // check for opened :
   if( !portIsOpen()){
      return( 0);
   }
   // get modem status :
   DWORD d = 0;
   if( !GetCommModemStatus( _handle, &d)){
      return( d);
   }
   return( d);
} // readModemStatus
