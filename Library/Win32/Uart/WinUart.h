//******************************************************************************
//
//   WinUart.h    Asynchronous serial device for Win32
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef WINUART_H
   #define WINUART_H

#ifndef _WINDOWS_
   #include <windows.h>
#endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

// hide windows.h superfluous definitions :
#ifdef PARITY_NONE
   #undef PARITY_NONE
#endif
#ifdef PARITY_ODD
   #undef PARITY_ODD
#endif
#ifdef PARITY_EVEN
   #undef PARITY_EVEN
#endif
#ifdef PARITY_MARK
   #undef PARITY_MARK
#endif
#ifdef PARITY_SPACE
   #undef PARITY_SPACE
#endif

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#define UART_PREFIX       "COM"
#define UART_NAME_SIZE    31

//------------------------------------------------------------------------------
//  WinUart
//------------------------------------------------------------------------------

class WinUart
{
public :

   // baud rate :
   typedef enum {
      BAUDRATE_MIN = 110,
      BAUDRATE_MAX = 115200
   } EBaudRate;

   // parity types :
   typedef enum {
      PARITY_NONE,
      PARITY_ODD,
      PARITY_EVEN,
      PARITY_MARK,
      PARITY_SPACE
   } EParity;

   // stop bits types :
   typedef enum {
      STOPBIT_1,
      STOPBIT_15,
      STOPBIT_2
   } EStopBits;

   // modem status :
   typedef enum {
      MODEM_DSR = 0x01,
      MODEM_CTS = 0x02,
      MODEM_DCD = 0x04,
      MODEM_RI  = 0x08
   } EModemStatus;

   typedef enum {
      FLOW_NONE,
      FLOW_RTS_CTS
   } EFlowControl;
   //---------------------------------------------------------------------------

   WinUart();
   // Constructor

   ~WinUart();
   // Destructor

   //---------------------------------------------------------------------------

   TYesNo setPort( const char *portName);
   // Set COM <portName>

   const char *getPort();
   // Get COM <portName>

   TYesNo setBaudRate( int baudRate);
   // set <baudRate>

   int getBaudRate();
   // get <baudRate>

   TYesNo setDataBits( int dataBits);
   // set data <dataBits> count 5..8

   int getDataBits();
   // get <dataBits>

   TYesNo setStopBits( EStopBits stopBits);
   // set <stopBits>

   EStopBits getStopBits();
   // get <stopBits>

   TYesNo setParity( EParity parity);
   // set <parity>

   EParity getParity();
   // get <parity>

   TYesNo setFlowControl( EFlowControl flow);
   // set flow control mode

   EFlowControl getFlowControl();
   // get flow control mode

   TYesNo setReceiveTimeout( int replyTime, int intercharacterTime);
   // Set timing of receiver :
   // If <intercharacterTime> = 0 waits <replyTime> for whole message
   // If <intercharacterTime> > 0 waits <replyTime> for first
   // character, next waits <intercharacterTime> * count of characters

   TYesNo setReceiveNowait();
   // Set timing of receiver - returns collected data immediately

   //---------------------------------------------------------------------------

   TYesNo send( const void *data, int size);
   // Send <data> with <size>

   int receive( void *data, int size);
   // Receive data <data> with <size>, returns received size (or 0 if error)

   TYesNo waitSend();
   // Waits for data sent

   void flush();
   // Make input/output queue empty

   void disconnect();
   // Disconnect/release COM device

   //---------------------------------------------------------------------------

   TYesNo setBreak( TYesNo txBreak);
   // Set <txBreak> signal at TxD

   TYesNo setDtr( TYesNo dtr);
   // Set <dtr> signal

   TYesNo setRts( TYesNo rts);
   // Set <rts> signal

   //---------------------------------------------------------------------------

   TYesNo getDtr();
   // Returns DSR state

   TYesNo getCts();
   // Returns CTS state

   TYesNo getDcd();
   // Returns DCD state

   TYesNo getRing();
   // Returns RI state

   byte getModemStatus();
   // Returns modem status mask (EModemStatus)

   TYesNo checkOpen();
   // Check for device Opened
//******************************************************************************

protected :
   HANDLE _handle;                       // Win32 serial device handle
   char   _name[ UART_NAME_SIZE + 1];    // Win32 serial device name

   DWORD  _baudRate;                     // Win32 baud rate
   BYTE   _dataBits;                     // Win32 byte size
   BYTE   _stopBits;                     // Win32 stopbits
   BYTE   _parity;                       // Win32 parity
   EFlowControl _flowControl;            // flow control settings
   TYesNo _immediateRead;                // ReceiveNowait
   int    _replyTime;                    // reply timeout
   int    _intercharacterTime;           // intercharacter timeout
   int    _transmitterCharTime;          // transmitter character timeout

   void close();
   // Close device

   void setDefaultParameters();
   // Set default parameters

   TYesNo setTimeouts();
   // Set timeouts

   void safeTransmitterTiming();
   // Set safe timing for transmitter

   DWORD readModemStatus();
   // Read modem status
}; // WinUart

#endif
