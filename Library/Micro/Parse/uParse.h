//******************************************************************************
//
//   uParse.h     Text parser services
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __uParse_H__
   #define __uParse_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif
//-----------------------------------------------------------------------------
//  Functions
//-----------------------------------------------------------------------------

int uParseNumber(const char *Text, signed *Number);
// Parse decimal <Number> from <Text>. Returns characters used

int uParseString( const char *Text, char *String, byte Size);
// Parse <String> with maximum <Size>. Returns characters used

int uParseUpDelimiter( const char *Text, char Delimiter, char *Result, char Size);
// Parse <Result> from <Text> up to <Delimiter>. Returns characters used

//------------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
