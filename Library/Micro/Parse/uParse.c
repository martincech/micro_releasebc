//******************************************************************************
//
//   uParse.c     Text parser services
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "uParse.h"
#include "Convert/uBcd.h"
#include <string.h>

//------------------------------------------------------------------------------
//   Number
//------------------------------------------------------------------------------

int uParseNumber( const char *Text, signed *Number)
// Parse decimal <Number> from <Text>. Returns characters used
{
int   Value;
char  ch;
int   Size;
TYesNo negative;

   Value = 0;
   Size  = 0;
   negative = NO;
   forever {
      ch = *Text++;
      if( ch == '\0'){
         break;                // end of string
      }
      if( ch == '-'){
         negative = YES;
         Size++;
         continue;
      }
      if( ch < '0' || ch > '9'){
         break;                // not a digit
      }
      Value *= 10;
      Value += uCharToDec( ch);
      Size++;
   }

   *Number = negative? 0-Value : Value;
   return( Size);                // end of string
} // uParseNumber

//------------------------------------------------------------------------------
//   String
//------------------------------------------------------------------------------

int uParseString( const char *Text, char *String, byte Size)
// Parse <String> with maximum <Size>. Returns characters used
{
   // left delimiter :
   if( *Text != '"'){
      return( 0);
   }
   Text++;                             // skip left delimiter
   // copy up to delimiter :
   return( uParseUpDelimiter( Text, '"', String, Size));
} // uParseString

//------------------------------------------------------------------------------
//   Delimiter
//------------------------------------------------------------------------------

int uParseUpDelimiter( const char *Text, char Delimiter, char *Result, char Size)
// Parse <Result> from <Text> up to <Delimiter>. Returns characters used
{
char ch;
int  CopySize;

   CopySize = 0;
   Size++;                             // add delimiter
   while( Size--){
      ch = *Text++;
      // check for string end :
      if( ch == '\0'){
         return( 0);                   // missing delimiter
      }
      // check for delimiter :
      if( ch == Delimiter){
         *Result = '\0';               // string terminator
         CopySize++;                   // add delimiter
         return( CopySize);
      }
      *Result = ch;                    // copy character
      Result++;
      CopySize++;
   }
   return( 0);                         // string too long
} // uParseUpDelimiter
