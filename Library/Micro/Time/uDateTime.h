//******************************************************************************
//
//   uDateTime.h  Date & Time utilities
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __uDateTime_H__
   #define __uDateTime_H__

#ifndef __uDate_H__
   #include "Time/uDate.h"
#endif

#ifndef __uTime_H__
   #include "Time/uTime.h"
#endif

//------------------------------------------------------------------------------
// Data types
//------------------------------------------------------------------------------

// Linear date & time representation (LSB = 1s)
typedef int32 UDateTimeGauge;

// Date & time structure :
typedef struct
{
   UTime Time;
   UDate Date;
} UDateTime;

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

// Field errors :
typedef enum {
   DATETIME_OK,                       // date & time OK
   DATETIME_WRONG_SEC,                // wrong second
   DATETIME_WRONG_MIN,                // wrong minute
   DATETIME_WRONG_HOUR,               // wrong hour

   DATETIME_WRONG_DAY,                // wrong day
   DATETIME_WRONG_MONTH,              // wrong month
   DATETIME_WRONG_YEAR,               // wrong year

   _DATETIME_WRONG_LAST
} EDateTimeError;

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

UDateTimeGauge uDateTimeGauge( UDateTime *DateTime);
// Encode <DateTime> to internal representation

void uDateTime( UDateTime *DateTime, UDateTimeGauge DateTimeGauge);
// Decode <DateTime> from <DateTimeGauge>

EDateTimeError uDateTimeValid( UDateTime *DateTime);
// Check for date & time validity

//------------------------------------------------------------------------------
// Calculations
//------------------------------------------------------------------------------

UDateGauge uDateTimeDate( UDateTimeGauge DateTimeGauge);
// Returns DateGauge of <DateTimeGauge>

UTimeGauge uDateTimeTime( UDateTimeGauge DateTimeGauge);
// Returns TimeGauge of <DateTimeGauge>

UDateTimeGauge uDateTimeCompose( UDateGauge DateGauge, UTimeGauge TimeGauge);
// Returns DateTimeGauge of <DateGauge> & <TimeGauge>

UDow uDateTimeDow( UDateTimeGauge DateTimeGauge);
// Returns day of week of <DateTimeGauge>

#ifdef __cplusplus
}
#endif

#endif

