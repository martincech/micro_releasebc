//******************************************************************************
//
//   uDateTime.c  Date & Time utilities
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "uDateTime.h"

//------------------------------------------------------------------------------
// Encode
//------------------------------------------------------------------------------

UDateTimeGauge uDateTimeGauge( UDateTime *DateTime)
// Encode <DateTime> to internal representation
{
UDateGauge DateGauge;
UTimeGauge TimeGauge;

   DateGauge = uDateGauge( &DateTime->Date);     // encode date
   TimeGauge = uTimeGauge( &DateTime->Time);     // necode time
   return( uDateTimeCompose( DateGauge, TimeGauge));
} // uDateTimeGauge

//------------------------------------------------------------------------------
// Decode
//------------------------------------------------------------------------------

void uDateTime( UDateTime *DateTime, UDateTimeGauge DateTimeGauge)
// Decode <DateTime> from <DateTimeGauge>
{
UDateGauge DateGauge;
UTimeGauge TimeGauge;

   // split date & time :
   DateGauge = uDateTimeDate( DateTimeGauge);
   TimeGauge = uDateTimeTime( DateTimeGauge);
   // decode date :
   uDate( &DateTime->Date, DateGauge);
   // decode time :
   uTime( &DateTime->Time,  TimeGauge);
} // uDateTime

//------------------------------------------------------------------------------
// Date & Time Validity
//------------------------------------------------------------------------------

EDateTimeError uDateTimeValid( UDateTime *DateTime)
// Check for date & time validity
{
ETimeError TResult;
EDateError DResult;

   // check for time :
   TResult = uTimeValid( &DateTime->Time);
   if( TResult != TIME_OK){
      return( (EDateTimeError)TResult);
   }
   DResult = uDateValid( &DateTime->Date);
   if( DResult != DATE_OK){
      return( (EDateTimeError)(DResult + DATETIME_WRONG_DAY - 1));
   }
   return( DATETIME_OK);
} // uDateTimeValid

//------------------------------------------------------------------------------
// Split date
//------------------------------------------------------------------------------

UDateGauge uDateTimeDate( UDateTimeGauge DateTimeGauge)
// Returns DateGauge of <DateTimeGauge>
{
   return( (UDateGauge)(DateTimeGauge / TIME_DAY));
} // uDateTimeDate

//------------------------------------------------------------------------------
// Split time
//------------------------------------------------------------------------------

UTimeGauge uDateTimeTime( UDateTimeGauge DateTimeGauge)
// Returns TimeGauge of <DateTimeGauge>
{
   return( (UTimeGauge)(DateTimeGauge % TIME_DAY));
} // uDateTimeTime

//------------------------------------------------------------------------------
// Compose date & time
//------------------------------------------------------------------------------

UDateTimeGauge uDateTimeCompose( UDateGauge DateGauge, UTimeGauge TimeGauge)
// Returns DateTimeGauge of <DateGauge> & <TimeGauge>
{
UDateTimeGauge DateTimeGauge;

   DateTimeGauge  = (UDateTimeGauge)TimeGauge;             // inherit time
   DateTimeGauge += (UDateTimeGauge)DateGauge * TIME_DAY;  // multiply date by day seconds
   return( DateTimeGauge);
} // uDateTimeCompose

//------------------------------------------------------------------------------
// Day of week
//------------------------------------------------------------------------------

UDow uDateTimeDow( UDateTimeGauge DateTimeGauge)
// Returns day of week of <DateTimeGauge>
{
UDateGauge DateGauge;

   DateGauge = uDateTimeDate( DateTimeGauge);
   return( uDateDow( DateGauge));
} // uDateTimeDow
