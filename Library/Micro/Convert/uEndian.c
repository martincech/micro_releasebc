//*****************************************************************************
//
//    uEndian.c    Endian conversions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "uEndian.h"

//-----------------------------------------------------------------------------
// Swap word
//-----------------------------------------------------------------------------

word uEndianWordSwap( word Value)
// Swap word <Value>
{
word Result;

   Result = ((Value & 0xFF) << 8) | 
             (Value         >> 8);
   return( Result);
} // uEndianWordSwap

//-----------------------------------------------------------------------------
// Swap dword
//-----------------------------------------------------------------------------

dword uEndianDwordSwap( dword Value)
// Swap dword <Value>
{
dword Result;

   Result = ((Value & 0x000000FF) << 24) | ((Value & 0x0000FF00) << 8) | 
            ((Value & 0x00FF0000) >> 8)  |  (Value >> 24);
   return( Result);
} // uEndianDwordSwap
