//*****************************************************************************
//
//    uBcd.c       BCD utility
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "uBcd.h"

//-----------------------------------------------------------------------------
// Get digit
//-----------------------------------------------------------------------------

byte uBcdDigitGet( dword Value, int Order)
// Returns BCD digit at <Order>
{
   Order <<= 2;         // Order * 4
   return((Value >> Order) & 0x0F);
} // uBcdDigitGet

//-----------------------------------------------------------------------------
// Set digit
//-----------------------------------------------------------------------------

dword uBcdDigitSet( dword Value, int Order, byte Digit)
// Sets BCD <Digit> at <Order>
{
   Order <<= 2;         // Order * 4
   return(( Value & ~(0x0F << Order)) | (Digit << Order));
} // uBcdDigitSet

//-----------------------------------------------------------------------------
// Width binary
//-----------------------------------------------------------------------------

int uBinaryWidth( dword Number)
// Returns digits count of the binary <Number>
{
   if( Number < 10){
      return( 1);
   }
   if( Number < 100){
      return( 2);
   }
   if( Number < 1000){
      return( 3);
   }
   if( Number < 10000){
      return( 4);
   }
   if( Number < 100000){
      return( 5);
   }
   if( Number < 1000000){
      return( 6);
   }
   if( Number < 10000000){
      return( 7);
   }
   return( 8);
} // uBinaryWidth

//-----------------------------------------------------------------------------
// Width BCD/hexadecimal
//-----------------------------------------------------------------------------

int uBcdWidth( dword Number)
// Returns digits count of the BCD/hexadecimal <Number>
{
dword Mask;
int   i;

   Mask   = 0xF0000000;
   for( i = 8; i > 0; i--){
      if( Mask & Number){    // nonzero position
         return( i);
      }
      Mask >>= 4;
   }
   return( 1);               // zero only
} // uBcdWidth

//-----------------------------------------------------------------------------
// Binary to BCD
//-----------------------------------------------------------------------------

dword uBinaryToBcd( dword x)
// Converts binary 0..99 999 999 to BCD
{
byte   i;
dword  y;

   y = 0;
   i = 8;
   do {
      y >>= 4;
      y |= (x % 10) << 28;
      x /= 10;
   } while( --i);
   return( y);
} // uBinaryToBcd

//-----------------------------------------------------------------------------
// BCD to Binary
//-----------------------------------------------------------------------------

dword uBcdToBinary( dword x)
// Converts 0..99 999 999 to binary
{
byte   i;
dword  y;

   y = 0;
   i = 8;
   do {
      y *= 10;
      y += (x & 0xF0000000) >> 28;
      x <<= 4;
   } while( --i);
   return( y);
} // uBcdToBinary

//-----------------------------------------------------------------------------
// Hexadecimal to char
//-----------------------------------------------------------------------------

char uNibbleToHex( byte x)
// Converts low nibble to '0'..'F'
{
   x &= 0x0F;
   if( (x) < 10){
      return( x + '0');
   }
   return( x - 10 + 'A');
} // uNibbleToHex

//-----------------------------------------------------------------------------
// Char to nexadecimal
//-----------------------------------------------------------------------------

byte uCharToHex( char ch)
// Converts ASCII '0'..'9' and 'A'..'F' to digit
{
   if( ch <= '9'){
      return( ch - '0');
   }
   return( ch - 'A' + 10);
} // uCharToHex
