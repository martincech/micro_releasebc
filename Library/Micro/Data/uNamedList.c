//*****************************************************************************
//
//    uNamedList.c  Named list utility
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Data/uNamedList.h"
#include "Data/uDirectory.h"
#include "Memory/Nvm.h"
#include <string.h>

// local functions :

static TNvmAddress _ItemAddress( const UNamedListDescriptor *Descriptor, UNamedListIdentifier Identifier);
// Calculate item address by <Index>

TYesNo uNamedListRemCopy( UNamedListDescriptor *Descriptor, TFileName FilenameDest, TFileName FilenameSrc)
// Load from remote device
{
UNamedList NamedListSrcInt;
UNamedList *NamedListSrc = &NamedListSrcInt;
UNamedList NamedListDestInt;
UNamedList *NamedListDest = &NamedListDestInt;
TFileAddress Address;
dword Size;

   NamedListSrc->Descriptor = Descriptor;
   NamedListDest->Descriptor = Descriptor;

   if(!FileOpen( &NamedListSrc->File, FilenameSrc, FILE_MODE_READ_ONLY)) {
      return NO;
   }

   if(FileIsOffline(&NamedListSrc->File)) {
      Address = 0;
      Size = uNamedListSize(NamedListSrc->Descriptor->Capacity, NamedListSrc->Descriptor->ItemSize);
      FileLoad(&NamedListSrc->File, Address, 0, Size);
      FileClose(&NamedListSrc->File);
      return YES;
   }

   if(!FileOpen( &NamedListDest->File, FilenameDest, FILE_MODE_WRITE_ONLY | FILE_MODE_SANDBOX)) {
      FileClose( &NamedListSrc->File);
      return NO;
   }

   if(!uDirectoryCopy( NamedListDest, NamedListSrc)) {
      FileClose( &NamedListSrc->File);
      FileClose( &NamedListDest->File);
      return NO;
   }

//   #warning Zbytecne se kopiruje cely list. Kopirovat jen obsazene polozky listu.
   Address = _ItemAddress( NamedListSrc->Descriptor, 0);
   Size = uDirectoryCapacity(Descriptor) * NamedListSrc->Descriptor->ItemSize;

   if(!FileClone( &NamedListDest->File, &NamedListSrc->File, Address, Size)) {
      FileClose( &NamedListSrc->File);
      FileClose( &NamedListDest->File);
      return NO;
   }
   FileCommit( &NamedListDest->File);
   FileClose( &NamedListSrc->File);
   FileClose( &NamedListDest->File);
   return YES;
} // uNamedListCopy

//------------------------------------------------------------------------------
//  Initialization
//------------------------------------------------------------------------------

void uNamedListInit( const UNamedListDescriptor *Descriptor, TFileName FileName)
// Initialize <List>
{
UNamedList List;
   uNamedListOpen( &List, Descriptor, FileName);
   uDirectoryInit( &List);
   uNamedListClose( &List);
} // uNamedListInit

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo uNamedListOpen( UNamedList *List, const UNamedListDescriptor *Descriptor, TFileName FileName)
// Open
{
   if(!FileOpen(&List->File, FileName, FILE_MODE_READ_WRITE)) {
      return NO;
   }
   List->Descriptor = (UDirectoryDescriptor *)Descriptor;
   return YES;
} // uNamedListOpen

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void uNamedListClose( UNamedList *List)
// Close
{
   FileClose( &List->File);
} // uNamedListClose

//------------------------------------------------------------------------------
//   Add
//------------------------------------------------------------------------------

UDirectoryIndex uNamedListAdd( UNamedList *List, const char *Name, const void *Data)
// Add item with <Name> and <Data> to <List>
{
UNamedListIdentifier Identifier;

   Identifier = uDirectoryIdentifierFree( List);      // get free identifier/item index
   if( Data){
      uNamedListSave( List, Identifier, Data);        // copy item data
   }
   return( uDirectoryAdd( List, Name, Identifier));   // add directory item
} // uNamedListAdd

//------------------------------------------------------------------------------
//   Copy
//------------------------------------------------------------------------------

UDirectoryIndex uNamedListCopy( UNamedList *List, const char *NewName, UDirectoryIndex FromIndex)
// Copy item <FromIndex> with a <NewName> to <List>
{
UNamedListIdentifier IdentifierFrom, IdentifierTo;
TNvmAddress          AddressFrom, AddressTo;

   // item from :
   IdentifierFrom = uDirectoryItemIdentifier( List, FromIndex);
   AddressFrom    = _ItemAddress( List->Descriptor, IdentifierFrom);
   // item to :
   IdentifierTo   = uDirectoryIdentifierFree( List);       // get free identifier/item index
   AddressTo      = _ItemAddress( List->Descriptor, IdentifierTo);
   FileMove( &List->File, AddressTo, AddressFrom, List->Descriptor->ItemSize);       // copy item data
   return( uDirectoryAdd( List, NewName, IdentifierTo));   // add directory item
} // uNamedListCopy

//------------------------------------------------------------------------------
//   Delete
//------------------------------------------------------------------------------

void uNamedListDelete( UNamedList *List, UDirectoryIndex Index)
// Delete item at <Index> from <List>
{
UNamedListIdentifier Identifier;
TNvmAddress          Address;

   Identifier = uDirectoryItemIdentifier( List, Index);
   uDirectoryDelete( List, Index);
   // clear data at ListIndex :
   Address = _ItemAddress( List->Descriptor, Identifier);
   FileFill( &List->File, Address, 0xFF, List->Descriptor->ItemSize);
} // uNamedListDelete

//------------------------------------------------------------------------------
//   Get
//------------------------------------------------------------------------------

void uNamedListLoad( UNamedList *List, UNamedListIdentifier Identifier, void *Data)
// Load <Data> from <List> by <Identifier>
{
TNvmAddress Address;

   Address = _ItemAddress( List->Descriptor, Identifier);
   FileLoad( &List->File, Address, Data, List->Descriptor->ItemSize);
} // uNamedListLoad

//------------------------------------------------------------------------------
//   Set
//------------------------------------------------------------------------------

void uNamedListSave( UNamedList *List, UNamedListIdentifier Identifier, const void *Data)
// Save <Data> with <Identifier> to <List>
{
TNvmAddress Address;

   Address = _ItemAddress( List->Descriptor, Identifier);
   if( FileMatch( &List->File, Address, Data, List->Descriptor->ItemSize)){
      return;                          // don't save - same data
   }
   FileSave( &List->File, Address, Data, List->Descriptor->ItemSize);
} // uNamedListSave

//------------------------------------------------------------------------------
//   Changed
//------------------------------------------------------------------------------

TYesNo uNamedListChanged( UNamedList *List, UNamedListIdentifier Identifier, const void *Data)
// Returns YES if  <Data> don't match <List> <Identifier> item
{
TNvmAddress Address;

   Address = _ItemAddress( List->Descriptor, Identifier);
   return( !FileMatch( &List->File, Address, Data, List->Descriptor->ItemSize));
} // uNamedListChanged

//******************************************************************************

//------------------------------------------------------------------------------
//   Item address
//------------------------------------------------------------------------------

static TFileAddress _ItemAddress( const UNamedListDescriptor *Descriptor, UNamedListIdentifier Identifier)
// Calculate item address by <Index>
{
   return( uDirectoryAddressNext( Descriptor) + Identifier * Descriptor->ItemSize);
} // _ItemAddress
