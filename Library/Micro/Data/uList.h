//*****************************************************************************
//
//    uList.h       Sequential list utility
//    Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __uList_H__
   #define __uList_H__

#ifndef __uListDef_H__
   #include "Data/uListDef.h"
#endif   

//------------------------------------------------------------------------------
//  Declaration
//------------------------------------------------------------------------------

#define uListAlloc( Name, Capacity, ItemSize) \
   static const UListDescriptor Name = {\
      Capacity,\
      ItemSize\
   }

#define uListSize( Capacity, ItemSize) \
                  (sizeof( UListHeader) + (Capacity) * (ItemSize))

#ifdef __cplusplus
   extern "C" {
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

TYesNo uListCopy(const UListDescriptor *Descriptor, TFileName FilenameDest, TFileName FilenameSrc);
// Load from remote device

void uListInit( const UListDescriptor *Descriptor, TFileName FileName);
// Initialize <List>

TYesNo uListOpen( UList *List, const UListDescriptor *Descriptor, TFileName FileName);
// Open <List> with descriptor <Descriptor>

void uListClose( UList *List);
// Open <List>

UListIndex uListAppend( UList *List, void *Item);
// Append <Item> to <List>

void uListInsert( UList *List, UListIndex Index, void *Item);
// Insert <Item> at <Index> of <List>

void uListDelete( UList *List, UListIndex Index);
// Delete item at <Index> from <List>

void uListItemLoad( UList *List, UListIndex Index, void *Item);
// Load <Item> from <Index> of <List>

void uListItemSave( UList *List, UListIndex Index, void *Item);
// Save <Item> at <Index> of <List>

//------------------------------------------------------------------------------

TYesNo uListHeaderGet( UList *List, UListHeader *Header);
// Returns <List> <Header>

TYesNo uListHeaderSet( UList *List, const UListHeader *Header);
// Sets <List> <Header>

int uListCount( UList *List);
// Returns <List> items count

int uListCapacity( const UListDescriptor *Descriptor);
// Returns <List> capacity


#ifdef __cplusplus
   }
#endif

#endif
