//*****************************************************************************
//
//    uDirectoryDef.h  Directory data description
//    Version 1.0      (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __uDirectoryDef_H__
   #define __uDirectoryDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifndef __File_H__
   #include "Memory/File.h"
#endif

//------------------------------------------------------------------------------
//  Constants
//------------------------------------------------------------------------------

#ifndef UDIRECTORY_NAME_SIZE
   #define UDIRECTORY_NAME_SIZE 15
#endif

#define UDIRECTORY_INDEX_INVALID  0xFF
#define ULIST_IDENTIFIER_INVALID  0xFF

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef byte  UDirectoryIndex;
typedef word  UDirectoryCrc;
typedef dword UDirectoryAddress;

typedef word  UDirectoryItemSize;
typedef byte  UDirectoryIdentifier;

//------------------------------------------------------------------------------
//  Directory header
//------------------------------------------------------------------------------

typedef struct {
   UDirectoryIndex      Capacity;      // total directory items count
   UDirectoryIndex      Count;         // actual directory items count
   UDirectoryIdentifier Last;          // last free list identifier
   byte                 _dummy;
   UDirectoryItemSize   ItemSize;      // list item size
   UDirectoryCrc        Crc;           // checksum
} UDirectoryHeader;

//------------------------------------------------------------------------------
//  Directory item
//------------------------------------------------------------------------------

typedef struct {
   char                 Name[ UDIRECTORY_NAME_SIZE + 1];
   UDirectoryIdentifier Identifier;
} UDirectoryItem;

//------------------------------------------------------------------------------
//  Directory layout
//------------------------------------------------------------------------------

typedef struct {
   UDirectoryHeader Header;            // directory descriptor
   UDirectoryItem   Item[];            // directory items
} UDirectoryLayout;

//------------------------------------------------------------------------------
//  Directory descriptor
//------------------------------------------------------------------------------

typedef struct {
   UDirectoryIndex    Capacity;        // total directory items count
   UDirectoryItemSize ItemSize;        // list item size
} UDirectoryDescriptor;

typedef struct {
   UDirectoryDescriptor *Descriptor;
   TFile File;
} UDirectory;

#endif
