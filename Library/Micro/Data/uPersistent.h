//*****************************************************************************
//
//    uPersistent.h    Persistent data handling
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __uPersistent_H__
   #define __uPersistent_H__

// singleton type :   
#define uPersistent(      Variable, Type)   typedef Type T##Variable; extern T##Variable Variable
#define uPersistentSet(   Variable, Value)               Variable = Value
#define uPersistentGet(   Variable)                      Variable
#define uPersistentAlloc( Variable)                      T##Variable Variable

// embedded type :
#define uPersistentChild(      Parent, Variable, Type) typedef Type T##Variable
#define uPersistentChildSet(   Parent, Variable, Value)             Parent.Variable = Value
#define uPersistentChildGet(   Parent, Variable)                    Parent.Variable
#define uPersistentChildAlloc( Parent, Variable)                    T##Variable Variable

#endif

