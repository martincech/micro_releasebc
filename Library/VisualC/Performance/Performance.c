//******************************************************************************
//
//   Performance.c   Performance time measuring
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <Windows.h>
#pragma hdrstop

#define __DEFINE_BOOL__
#include "Performance.h"

//******************************************************************************
// Timer get
//******************************************************************************

int64 TimerGet( void)
// Get performance timer
{
LARGE_INTEGER Counter;

   QueryPerformanceCounter( &Counter);
   return( Counter.QuadPart);
} // TimerGet

//******************************************************************************
// Timer seconds
//******************************************************************************

double TimerSeconds( int64 Timer)
// Convert to seconds
{
LARGE_INTEGER Frequency;

   if( !QueryPerformanceFrequency( &Frequency)){
      return( 0.0);
   }
   return( (double)Timer / (double)Frequency.QuadPart);
} // TimerSeconds

//******************************************************************************
// Timer miliseconds
//******************************************************************************

double TimerMiliseconds( int64 Timer)
// Convert to miliseconds
{
LARGE_INTEGER Frequency;

   if( !QueryPerformanceFrequency( &Frequency)){
      return( 0.0);
   }
   return( (double)Timer * 1000 / (double)Frequency.QuadPart);
} // TimerMiliseconds

