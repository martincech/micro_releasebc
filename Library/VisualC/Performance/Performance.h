//******************************************************************************
//
//   Performance.h   Performance time measuring
//   Version 1.0  (c) VymOs
//
//******************************************************************************


#ifndef PerformanceH
   #define PerformanceH

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

int64 TimerGet( void);
// Get performance timer

double TimerSeconds( int64 Timer);
// Convert to seconds

double TimerMiliseconds( int64 Timer);
// Convert to miliseconds

#ifdef __cplusplus
   }
#endif

#endif

