//******************************************************************************
//
//   Bootloader.h    Firmware upgrader
//   Version 1.0     (c) Veit Electronics
//
//******************************************************************************

#ifndef __Bootloader_H__
	#define __Bootloader_H__

#include "Usb/Hid/Hid.h"
#include "HidBootloader/HidBootloaderDef.h"
#define DEVICE_NAME_MAX  254

//******************************************************************************
// THid
//******************************************************************************

class TBootloader {
public :
   TBootloader( const char *DeviceName);
   // Constructor

   ~TBootloader();
   // Destructor

   bool Download(const byte *Data, int Size);
   // Run file

   bool StartApplication( void);
   // Exits bootloader, starts application

   bool VersionGet( TDeviceVersion *Version);
   // Checks bootloader version

//---------------------------------------------------------------------------
protected :
   THid *Hid;
   char DeviceName[DEVICE_NAME_MAX + 1];
   bool Open;

   bool SendCmd(TBootloaderCmd *Cmd, TBootloaderReply *Reply);

   bool SendFeatureCmd(TBootloaderFeatureCmd *Cmd);

   bool SendFeatureCmdWithReportReply(TBootloaderFeatureCmd *Cmd, TBootloaderReportReply *Reply);

   bool WaitForIdle( byte Cmd);

   bool CheckOpen( void);
}; // TBootloader

#endif

