//******************************************************************************
//
//   HidBootloader.cpp   HID Bootloader
//   Version 1.0         (c) Veit Electronics
//
//******************************************************************************

#include <windows.h>
#pragma hdrstop

#include "Bootloader.h"
#include "Usb/Hid/Hid.h"

//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------

TBootloader::TBootloader( const char *_DeviceName)
// Constructor
{
int Length = strlen(_DeviceName);

   if(Length > DEVICE_NAME_MAX) {
      Length = DEVICE_NAME_MAX;
   }

   memcpy(DeviceName, _DeviceName, Length);
   DeviceName[Length] = 0;

   Hid = new THid();
   Open = false;
} // TBootloader

//-----------------------------------------------------------------------------
// Destructor
//-----------------------------------------------------------------------------

TBootloader::~TBootloader()
// Destructor
{
   delete Hid;
} // ~TBootloader

//-----------------------------------------------------------------------------
// Download
//-----------------------------------------------------------------------------

bool TBootloader::Download(const byte *Data, int Size)
// Download
{
TBootloaderCmd Cmd;
byte Report[HID_BOOTLOADER_DATA_LENGTH];
int Done = 0;
   if(!CheckOpen()) {
      return NO;
   }

   Cmd.Cmd = BOOTLOADER_CMD_DOWNLOAD;
   if(!SendCmd(&Cmd, NULL)) {
      return false;
   }

   while(Size > 0) {
      memcpy(Report, Data, Size < HID_BOOTLOADER_DATA_LENGTH ? Size : HID_BOOTLOADER_DATA_LENGTH);

      Done += HID_BOOTLOADER_DATA_LENGTH;

      if(!Hid->WriteReport((void *)Report)) {
         return NO;
      }

      Data += HID_BOOTLOADER_DATA_LENGTH;
      Size -= HID_BOOTLOADER_DATA_LENGTH;
   }

   return WaitForIdle(Cmd.Cmd);
} // Download

//-----------------------------------------------------------------------------
// Start application
//-----------------------------------------------------------------------------

bool TBootloader::StartApplication( void)
// Exits bootloader, starts application
{
TBootloaderCmd Cmd;
TBootloaderReply Reply;
   if(!CheckOpen()) {
      return false;
   }

   Cmd.Cmd = BOOTLOADER_CMD_START_APP;
   if(!SendCmd(&Cmd, NULL)) {
      return false;
   }

   if(!WaitForIdle(BOOTLOADER_CMD_START_APP)) {
      return false;
   }
   return true;
} // StartApplication

//-----------------------------------------------------------------------------
// Get version
//-----------------------------------------------------------------------------

bool TBootloader::VersionGet( TDeviceVersion *Version)
// Checks bootloader version
{
TBootloaderCmd Cmd;
TBootloaderReply Reply;
   if(!CheckOpen()) {
      return false;
   }

   Cmd.Cmd = BOOTLOADER_CMD_VERSION;
   if(!SendCmd(&Cmd, &Reply)) {
      return false;
   }

   memcpy(Version, &Reply.Data.Version, sizeof(*Version));

   return true;
} // StartApplication

//******************************************************************************

//-----------------------------------------------------------------------------
// Send cmd
//-----------------------------------------------------------------------------

bool TBootloader::SendCmd(TBootloaderCmd *Cmd, TBootloaderReply *Reply)
{
   Hid->Flush();

   switch(Cmd->Cmd) {
      case BOOTLOADER_CMD_DOWNLOAD:
      case BOOTLOADER_CMD_START_APP:
         return SendFeatureCmd( (TBootloaderFeatureCmd *)Cmd);

      case BOOTLOADER_CMD_VERSION:
         return SendFeatureCmdWithReportReply( (TBootloaderFeatureCmd *)Cmd, (TBootloaderReportReply *)Reply);

      default:
         return false;
   }
}

//-----------------------------------------------------------------------------
// Send feature cmd with report reply
//-----------------------------------------------------------------------------

bool TBootloader::SendFeatureCmdWithReportReply(TBootloaderFeatureCmd *Cmd, TBootloaderReportReply *Reply)
{
byte Report[HID_BOOTLOADER_DATA_LENGTH];
   memcpy(Report, Cmd, sizeof(TBootloaderFeatureCmd));
   if(!Hid->SetFeature((void *)Report)) {
      return false;
   }

   if(!Hid->ReadReport((void *)Report)) {
      return NO;
   }

   memcpy(Reply, Report, sizeof(TBootloaderReportReply));

   if(Reply->Reply != BootloaderReply(Cmd->Cmd, BOOTLOADER_REPLY_OK)) {
      return NO;
   }

   return YES;
} // SendReportCmd

//-----------------------------------------------------------------------------
// Send feature cmd
//-----------------------------------------------------------------------------

bool TBootloader::SendFeatureCmd(TBootloaderFeatureCmd *Cmd)
// Send feature cmd
{
byte Report[HID_BOOTLOADER_DATA_LENGTH];
   Hid->Flush();
   memcpy(Report, Cmd, sizeof(TBootloaderFeatureCmd));
   if(!Hid->SetFeature((void *)Report)) {
      return false;
   }
   return true;
} // SendFeatureCmd

//-----------------------------------------------------------------------------
// Wait for idle
//-----------------------------------------------------------------------------

bool TBootloader::WaitForIdle( byte Cmd) {
byte Report[HID_BOOTLOADER_DATA_LENGTH];
TBootloaderFeatureReply *Reply = (TBootloaderFeatureReply *) Report;
   while(true) {
      if(!Hid->GetFeature((void *)Reply)) {
         return false;
      }
      if(Reply->Reply == BootloaderReply(Cmd, BOOTLOADER_REPLY_BUSY)) {
         // Busy, wait
         continue;
      } else if(Reply->Reply == BootloaderReply(Cmd, BOOTLOADER_REPLY_OK)) {
         // Operation succesfull
         return true;
      } else {
         // Operation failed
         return false;
      }
   }
} // WaitForIdle

//-----------------------------------------------------------------------------
// Check open
//-----------------------------------------------------------------------------

bool TBootloader::CheckOpen( void)
// Check open
{
   if(Open) {
      return true;
   }
   Hid->Open(DeviceName);
   Hid->SetInputBuffers( 0);

   if(Hid->GetOutputReportSize() != HID_BOOTLOADER_DATA_LENGTH) {
      return false;
   }

   if(Hid->GetInputReportSize() != HID_BOOTLOADER_DATA_LENGTH) {
      return false;
   }

   if(Hid->GetFeatureReportSize() != HID_BOOTLOADER_CMD_LENGTH) {
      return false;
   }

   Open = true;

   return true;
} // CheckOpen