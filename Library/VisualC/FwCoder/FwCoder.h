//******************************************************************************
//
//   FwCoder.h           FwCoder
//   Version 1.0         (c) Veit Electronics
//
//******************************************************************************

#ifndef __FwCoder_H__
	#define __FwCoder_H__

#include "Unisys/Uni.h"
#include "Fw/FwDef.h"

typedef enum {
   KEY_AES128,
   KEY_AES192,
   KEY_AES256
} TFwCoderKeyType;

typedef struct {
   char Key[32];
   byte KeyType;
   char InitialVector[32];
} TFwCoderConfig;

//******************************************************************************
// TFwCoder
//******************************************************************************

class TFwCoder {
public :
   TFwCoder();
   // Constructor

   ~TFwCoder();
   // Destructor

   void ConfigSet( TFwCoderConfig *Config);

   bool Encode(const char *InputFile, const char *OutputFile);
   // Encode

   bool Decode(const char *InputFile, const char *OutputFile);

   bool Interpret(const char *InputFile, const char *OutputFile);
//---------------------------------------------------------------------------
protected :
   TFwCrc CrcCompute( TFwCmdCrc *CmdCrc);
   // Compute CRC

   bool CrcCheck( TFwCmdCrc *CmdCrc);
   // Check CRC

   void CrcSet( TFwCmdCrc *CmdCrc);
   // Set CRC

   TFwCoderConfig Config;
}; // TFwUpgrader

#endif

