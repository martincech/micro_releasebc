//******************************************************************************
//
//   FwCoder.cpp         FwCoder
//   Version 1.0         (c) Veit Electronics
//
//******************************************************************************

#include <windows.h>
#pragma hdrstop

#include "FwCoder.h"
#include "Hex/HexFile.h"

#include "Fw/FwDef.h"

#include "Crypt/Crypt.h"

#define HEX_MAX_SIZE       1024 * 1024
#define ENCRYPTED_MAX_SIZE   1024 * 1024
#define ENCODED_MAX_SIZE   ENCRYPTED_MAX_SIZE

//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------

TFwCoder::TFwCoder()
// Constructor
{
   memset(&Config, 0x00, sizeof(Config));
   CryptInit();
} // TBootloader

//-----------------------------------------------------------------------------
// Destructor
//-----------------------------------------------------------------------------

TFwCoder::~TFwCoder()
// Destructor
{
} // ~TBootloader

//-----------------------------------------------------------------------------
// Download
//-----------------------------------------------------------------------------

bool TFwCoder::Encode(const char *InputFile, const char *OutputFile)
// Download <Firmware> of <Size>
{
qword DataCrc;
int i;
   THexFile *Hex = new THexFile(HEX_MAX_SIZE);
   if(!Hex->Load((char *)InputFile)) {
      return false;
   }

   byte *Fw = (byte *)malloc(ENCRYPTED_MAX_SIZE);

   byte *CurrentPtr = Fw;

   TFwCmdCrc *CmdCrc;

   // NONSENSE
   CmdCrc = (TFwCmdCrc *)CurrentPtr;
   CmdCrc->Length = FwSizeOf( NonSense);
   CmdCrc->Cmd.Cmd = FW_NONSENSE;
   CmdCrc->Cmd.Data.NonSense.Random = rand();
   CrcSet(CmdCrc);
   CurrentPtr = (byte *)&CmdCrc->Cmd.Cmd + CmdCrc->Length + sizeof(TFwCrc);

   // VERSION
   CmdCrc = (TFwCmdCrc *)CurrentPtr;
   CmdCrc->Length = FwSizeOf(Version);
   CmdCrc->Cmd.Cmd = FW_VERSION;
   CmdCrc->Cmd.Data.Version.Version.Class = DEVICE_SCALE_COMPACT;
   CmdCrc->Cmd.Data.Version.Version.Hardware = DeviceVersionSet(4, 6, 0);
   CmdCrc->Cmd.Data.Version.Version.Modification = 0;
   CmdCrc->Cmd.Data.Version.Version.Software = DeviceVersionSet(1, 0, 0);
   CmdCrc->Cmd.Data.Version.Version.SerialNumber = 0;
   CrcSet(CmdCrc);
   CurrentPtr = (byte *)&CmdCrc->Cmd.Cmd + CmdCrc->Length + sizeof(TFwCrc);

   // DATA
   dword RemainingCode = Hex->CodeSize;
   byte *CodePtr = &Hex->Code[Hex->CodeMinAddress];
   word Count;
   dword Address = Hex->CodeMinAddress;
   dword Line = 0;

   FwDataCrcStart( &DataCrc);

   while(RemainingCode) {
      if(RemainingCode > FW_DATA_COUNT) {
         Count = FW_DATA_COUNT;
      } else {
         Count = RemainingCode;
      }

      CmdCrc = (TFwCmdCrc *)CurrentPtr;
      CmdCrc->Length = FwSizeOfData( Count);
      CmdCrc->Cmd.Cmd = FW_DATA;
      CmdCrc->Cmd.Data.Data.Address = Address;
      CmdCrc->Cmd.Data.Data.Count = Count;
      memcpy(CmdCrc->Cmd.Data.Data.Data, CodePtr, Count);
      CrcSet(CmdCrc);
      CurrentPtr = (byte *)&CmdCrc->Cmd.Cmd + CmdCrc->Length + sizeof(TFwCrc);

      for(i = 0 ; i < Count ; i++) {
         FwDataCrcProcess( &DataCrc, CmdCrc->Cmd.Data.Data.Data[i]);
      }

      RemainingCode -= Count;
      CodePtr += Count;
      Address += Count;

      if(rand() % 10 == 0) {
         // NONSENSE
         CmdCrc = (TFwCmdCrc *)CurrentPtr;
         CmdCrc->Length = FwSizeOf( NonSense);
         CmdCrc->Cmd.Cmd = FW_NONSENSE;
         CmdCrc->Cmd.Data.NonSense.Random = rand();
         CrcSet(CmdCrc);
         CurrentPtr = (byte *)&CmdCrc->Cmd.Cmd + CmdCrc->Length + sizeof(TFwCrc);
      }
   }

   FwDataCrcEnd( &DataCrc);

   // TERMINATOR
   CmdCrc = (TFwCmdCrc *)CurrentPtr;
   CmdCrc->Length = FwSizeOf(Terminator);
   CmdCrc->Cmd.Cmd = FW_TERMINATOR;
   CmdCrc->Cmd.Data.Terminator.Crc = DataCrc;
   CrcSet(CmdCrc);
   CurrentPtr = (byte *)&CmdCrc->Cmd.Cmd + CmdCrc->Length + sizeof(TFwCrc);

   dword EncodedLength = (dword)(CurrentPtr - Fw);
   dword EncryptedSize = EncryptSize(EncodedLength);

   if(EncryptedSize > ENCODED_MAX_SIZE) {
      return false;
   }

   if(EncryptedSize > EncodedLength) {
      memset(CurrentPtr, 0xFF, EncryptedSize - EncodedLength);
   }

   CryptInit();
   //Encrypt(Fw, EncodedLength);

   FILE *f;
   f = fopen( OutputFile, "wb");
   if( !f){
      return( false);
   }
   if(fwrite(Fw, 1, EncryptedSize, f) != EncryptedSize) {
      return false;
   }

   fclose(f);

   return true;
} // Encode

//-----------------------------------------------------------------------------
// Decode
//-----------------------------------------------------------------------------

bool TFwCoder::Decode(const char *InputFile, const char *OutputFile)
// Decode
{
   byte *Fw = (byte *)malloc(ENCRYPTED_MAX_SIZE);

   FILE *f;
   f = fopen( InputFile, "rb");
   if( !f){
      return( false);
   }

   fseek (f , 0 , SEEK_END);
   dword EnryptedLength = ftell (f);
   rewind (f);

   if(EnryptedLength > ENCRYPTED_MAX_SIZE) {
      return false;
   }

   fread (Fw, 1, EnryptedLength, f);

   fclose(f);

   CryptInit();
   Decrypt(Fw, EnryptedLength);

   dword EncodedLength = EnryptedLength;

   f = fopen( OutputFile, "wb");
   if( !f){
      return( false);
   }

   if(fwrite(Fw, 1, EncodedLength, f) != EncodedLength) {
      return false;
   }

   fclose(f);

   return true;
} // Decode


//-----------------------------------------------------------------------------
// Interpret
//-----------------------------------------------------------------------------

bool TFwCoder::Interpret(const char *InputFile, const char *OutputFile)
// Decode
{
   byte *Fw = (byte *)malloc(ENCODED_MAX_SIZE);

   if(!Decode(InputFile, OutputFile)) {
      return false;
   }

   FILE *f;
   f = fopen( OutputFile, "rb");
   if( !f){
      return( false);
   }

   fseek (f , 0 , SEEK_END);
   dword EncodedLength = ftell(f);
   rewind (f);

   if(EncodedLength > ENCODED_MAX_SIZE) {
      return false;
   }

   fread (Fw, 1, EncodedLength, f);

   fclose(f);

   f = fopen( OutputFile, "w");

   if( !f){
      return( false);
   }

   byte *CurrentPtr = Fw;

   TFwCmdCrc *CmdCrc;

   dword RemainingCode;
   word Count;
   dword Line;

   qword DataCrc;
   int i;

   FwDataCrcStart( &DataCrc);

   while(CurrentPtr != Fw + EncodedLength) {
      CmdCrc = (TFwCmdCrc *)CurrentPtr;
      if(CurrentPtr + CmdCrc->Length >= Fw + EncodedLength) {
         fprintf(f, "Length error\n");
         fclose(f);
         return false;
      }
      if(!CrcCheck( CmdCrc)) {
         fprintf(f, "CRC failed\n");
         fclose(f);
         return false;
      }
      switch(CmdCrc->Cmd.Cmd) {
         case FW_DATA:
            fprintf(f, "DATA\n");
            fprintf(f, "Address: %d\n", CmdCrc->Cmd.Data.Data.Address);
            break;

         case FW_NONSENSE:
            fprintf(f, "NONSENSE\n");
            break;

         case FW_TERMINATOR:
            fprintf(f, "TERMINATOR\n");
            FwDataCrcEnd( &DataCrc);
            if(DataCrc != CmdCrc->Cmd.Data.Terminator.Crc) {
               fprintf(f, "CRC failed\n");
               fclose(f);
               return false;
            }
            fclose(f);
            return true;

         default:
            fprintf(f, "Unknown CMD\n");
            fclose(f);
            return false;
      }

      CurrentPtr = (byte *)&CmdCrc->Cmd.Cmd + CmdCrc->Length + sizeof(TFwCrc);


   }


   fprintf(f, "Termination error\n");
   fclose(f);


   return false;
} // Decode

//-----------------------------------------------------------------------------
// Compute CRC
//-----------------------------------------------------------------------------

TFwCrc TFwCoder::CrcCompute( TFwCmdCrc *CmdCrc)
// Compute CRC
{
TFwCrc Crc;
byte *CrcData = (byte *)&CmdCrc->Cmd;
word Remaining = CmdCrc->Length;
   FwCrcStart(&Crc);
   while(Remaining) {
      FwCrcProcess(&Crc, *CrcData);
      CrcData++;
      Remaining--;
   }
   FwCrcEnd(&Crc);

   return Crc;
}

//-----------------------------------------------------------------------------
// Check CRC
//-----------------------------------------------------------------------------

bool TFwCoder::CrcCheck( TFwCmdCrc *CmdCrc)
// Check CRC
{
TFwCrc MyCrc = CrcCompute( CmdCrc);
TFwCrc *OriginalCrc = (TFwCrc *)&CmdCrc->Cmd + CmdCrc->Length;
   if(MyCrc != *OriginalCrc) {
      return false;
   }

   return true;
}

//-----------------------------------------------------------------------------
// Set Crc
//-----------------------------------------------------------------------------

void TFwCoder::CrcSet( TFwCmdCrc *CmdCrc)
// Set CRC
{
TFwCrc *Crc = (TFwCrc *)&CmdCrc->Cmd + CmdCrc->Length;
   *Crc = CrcCompute(CmdCrc);
}
