﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.ComponentModel;

namespace BatApp
{
   /// <summary>
   /// Interaction logic for DeviceListView.xaml
   /// </summary>
   public partial class DeviceListView
   {
      public DeviceListView()
      {
         InitializeComponent();
      }

      #region Command execution handlers
      void ServerCommand_ExecutionProblem(object sender, Exception e)
      {
         MessageBox.Show(Window.GetWindow(this), e.Message, "Communication Error", MessageBoxButton.OK, MessageBoxImage.Error);
      }
      #endregion

      #region Command handlers initialization routines
      private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
      {
         DeviceManagerViewModel vm = e.OldValue as DeviceManagerViewModel;
         
         if( vm != null)
         {
            vm.LoadDevicesCommand.ExecutionProblem -= ServerCommand_ExecutionProblem;
            vm.ChangeCurrentDeviceCommand.ExecutionProblem -= ServerCommand_ExecutionProblem;
            vm.LoadConfigCommand.ExecutionProblem -= ServerCommand_ExecutionProblem;
         }

         vm = e.NewValue as DeviceManagerViewModel;
         if (vm != null)
         {
            vm.LoadDevicesCommand.ExecutionProblem += ServerCommand_ExecutionProblem;
            vm.ChangeCurrentDeviceCommand.ExecutionProblem += ServerCommand_ExecutionProblem;
            vm.LoadConfigCommand.ExecutionProblem += ServerCommand_ExecutionProblem;
         }        
      }
      #endregion

   }
}
