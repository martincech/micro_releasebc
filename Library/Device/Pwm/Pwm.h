//******************************************************************************
//
//   Pwm.c          PWM controller
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __Pwm_H__
   #define __Pwm_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//-----------------------------------------------------------------------------

// PWM channels :
typedef enum {
   PWM_CHANNEL_0,
   PWM_CHANNEL_1,
   PWM_CHANNEL_2,
   PWM_CHANNEL_3,
   PWM_CHANNEL_4,
   PWM_CHANNEL_5,
   PWM_CHANNEL_6,
   PWM_CHANNEL_7,
   PWM_CHANNEL_8,
   _PWM_CHANNEL_COUNT
} EPwmChannel;

void PwmInit( byte Channel);
// Initialisation

void PwmStart( byte Channel, uint32 PeriodTicks, uint32 Duty);
// Start PWM with <PeriodTicks> per period <Duty> cycle

void PwmDutySet( byte Channel, uint32 Duty);
// Set PWM <Duty> cycle

void PwmStop( byte Channel);
// Stop PWM

#endif
