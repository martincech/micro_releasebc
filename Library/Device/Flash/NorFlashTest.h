//*****************************************************************************
//
//    NorFlashTest.h   NOR Flash memory test
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __NorFlashTest_H__
   #define __NorFlashTest_H__

#ifndef __NorFlash_H__
   #include "Flash/NorFlash.h"
#endif   

TYesNo NorFlashSignature( void);
// Check for device signature

TYesNo NorFlashBlankCheck( void);
// Erase & check

TYesNo NorFlashPageTest( void);
// Run memory page test

TYesNo NorFlashPageTestWrite( void);
// Run memory page test write

TYesNo NorFlashPageTestCheck( void);
// Run memory page test check

TYesNo NorFlashPatternTest( byte Pattern);
// Run memory pattern test

#endif
