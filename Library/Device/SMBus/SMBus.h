//*****************************************************************************
//
//    SMBusMaster.h          SMBus
//    Version 1.0            (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SMBusMaster_H__
   #define __SMBusMaster_H__

#include "Unisys/Uni.h"

TYesNo SMBusWriteWord(byte Cmd, word Data);

TYesNo SMBusReadWord(byte Cmd, word *Data);

#endif