//******************************************************************************
//
//   Aes.h         AES
//   Version 1.0   (c) Veit Electronics
//
//******************************************************************************

#ifndef __Aes_H__
   #define __Aes_H__

#include "Unisys/Uni.h"

#include "Crypt/CryptConfig.h"

#define AES_BLOCK_SIZE     CRYPT_BLOCK_SIZE

void AesInit( void);
// Init

void AesEncrypt( void *Data);
// Encrypt

void AesDecrypt( void *Data);
// Decrypt

#endif 

