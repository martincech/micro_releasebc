//******************************************************************************
//
//   AesDef.h       AES definitions
//   Version 1.0   (c) Veit Electronics
//
//******************************************************************************

#ifndef __AesDef_H__
   #define __AesDef_H__

#include "Unisys/Uni.h"

typedef enum {
   AES_TYPE_128,
   AES_TYPE_192,
   AES_TYPE_256
} EAesType;

#endif 

