//******************************************************************************
//
//   AesCbc.c      AES + CBC crypting
//   Version 1.0   (c) Veit Electronics
//
//******************************************************************************

#include "Crypt/Crypt.h"
#include "Crypt/Aes.h"
#include <string.h>
#include "Crypt/CbcConfig.c"

#if CRYPT_BLOCK_SIZE != AES_BLOCK_SIZE
   #error
#endif

static byte ChainBlock[CRYPT_BLOCK_SIZE];

static void XORBytes( byte * bytes1, byte * bytes2, byte count);

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void CryptInit( void)
// Init
{
   memcpy(ChainBlock, CbcInitialVector, CRYPT_BLOCK_SIZE);
   AesInit();
} // CryptInit

//-----------------------------------------------------------------------------
// Encrypt size
//-----------------------------------------------------------------------------

dword EncryptSize(dword DataSize)
// Length of encrypted data, originally <DataSize>
{
   if(DataSize == 0) {
      return 0;
   }

   return ((DataSize - 1) / CRYPT_BLOCK_SIZE + 1) * CRYPT_BLOCK_SIZE;
} // EncryptSize

//-----------------------------------------------------------------------------
// Encrypt
//-----------------------------------------------------------------------------

void Encrypt(void *_Data, dword Size)
// Encrypt
{
byte *Data = (byte *) _Data;
dword RemainingSize = Size;

   if(Size == 0) {
      return;
   }

   forever {
      XORBytes( Data, ChainBlock, CRYPT_BLOCK_SIZE );
      AesEncrypt(Data);
      memcpy( ChainBlock, Data, CRYPT_BLOCK_SIZE );

      if(RemainingSize <= CRYPT_BLOCK_SIZE) {
         return;
      }

      RemainingSize -= CRYPT_BLOCK_SIZE;
      Data += CRYPT_BLOCK_SIZE;
   }
} // Encrypt

//-----------------------------------------------------------------------------
// Decrypt
//-----------------------------------------------------------------------------

void Decrypt(void *_Data, dword Size)
// Decrypt
{
byte *Data = (byte *) _Data;
byte temp[ CRYPT_BLOCK_SIZE];
dword RemainingSize = Size;

   if(Size == 0) {
      return;
   }

   forever {
      if(RemainingSize < CRYPT_BLOCK_SIZE) {
         memset(Data + CRYPT_BLOCK_SIZE - RemainingSize, 0xFF, CRYPT_BLOCK_SIZE - RemainingSize);
      }

      memcpy( temp, Data, CRYPT_BLOCK_SIZE );
      AesDecrypt( Data );
      XORBytes( Data, ChainBlock, CRYPT_BLOCK_SIZE );
      memcpy( ChainBlock, temp, CRYPT_BLOCK_SIZE );

      if(RemainingSize <= CRYPT_BLOCK_SIZE) {
         return;
      }

      RemainingSize -= CRYPT_BLOCK_SIZE;
      Data += CRYPT_BLOCK_SIZE;
   }
} // Decrypt

//*****************************************************************************

static void XORBytes( byte * bytes1, byte * bytes2, byte count)
{
   do {
      *bytes1 ^= *bytes2; // Add in GF(2), ie. XOR.
      bytes1++;
      bytes2++;
   } while( --count );
}