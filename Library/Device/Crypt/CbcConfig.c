//******************************************************************************
//
//   CbcConfig.c        Bootloader CBC config
//   Version 1.0        (c) Veit Electronics
//
//******************************************************************************

#include "Unisys/Uni.h"
#include "Crypt/CryptConfig.h"

const byte CbcInitialVector[CRYPT_BLOCK_SIZE] = 
{
	0x6e, 0xc2, 0x63, 0xd9, 0xdc, 0x52, 0x5f, 0x17,
	0xc8, 0xdf, 0x0a, 0x65, 0x29, 0x0f, 0xbb, 0xd0
};