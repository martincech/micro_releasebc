//******************************************************************************
//
//   FileNvm.c              File
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#include "FileNvm.h"
#include "Memory/FileInternal.h"
#include "Memory/Nvm.h"
#include "Cpu/Cpu.h"

static int SandboxUsed = NO;

//------------------------------------------------------------------------------
//  Permission
//------------------------------------------------------------------------------

byte FileLocalPermission( void)
// Permission
{
   return FILE_MODE_READ_WRITE | FILE_MODE_SANDBOX;
} // FileLocalPermission

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo FileLocalOpen( TFile *File, TFileName FileName, TFileMode FileMode)
// Open <FileName> with <Mode> access
{
TFileDescriptor *Descriptor;
   if(FileName == FILE_SANDBOX) {
      return NO;
   }
   if(FileMode & FILE_MODE_SANDBOX) {
      if(SandboxUsed) {
         return NO;
      }
      Descriptor = &FileDescriptor[File->Item.Name];
      if(Descriptor->Size > FileDescriptor[FILE_SANDBOX].Size) {
         return NO;
      }
      NvmMove(FileDescriptor[FILE_SANDBOX].Start, Descriptor->Start, Descriptor->Size);
      File->Item = FileItem[FILE_SANDBOX];
      //SandboxUsed = YES;
   }
   return YES;
} // FileOpen

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

TYesNo FileLocalClose( TFile *File)
// Close <File>
{
   if(File->Mode & FILE_MODE_SANDBOX) {
      SandboxUsed = NO;
   }
   return YES;
} // FileClose

//------------------------------------------------------------------------------
//  Commit
//------------------------------------------------------------------------------

TYesNo FileLocalCommit( TFile *File)
// Commit sandboxed <File>
{
TFileDescriptor *Descriptor;
   if(!(File->Mode & FILE_MODE_SANDBOX)) {
      return YES;
   }
   Descriptor = &FileDescriptor[FileItem[File->Name].Name];
   NvmMove(Descriptor->Start, FileDescriptor[FILE_SANDBOX].Start, Descriptor->Size);
   return YES;
} // FileCommit

//------------------------------------------------------------------------------
//  Save
//------------------------------------------------------------------------------

TYesNo FileLocalSave( TFile *File, TFileAddress Address, const void *Data, int Size)
// Save <Data> with <Size> at <Address>
{
TFileDescriptor *Descriptor;
   Descriptor = &FileDescriptor[File->Item.Name];
   if(!NvmSave(Descriptor->Start + Address, Data, Size)) {
      return NO;
   }
   return YES;
} // FileSave

//------------------------------------------------------------------------------
//   Load
//------------------------------------------------------------------------------

TYesNo FileLocalLoad( TFile *File, TFileAddress Address, void *Data, int Size)
// Load <Data> with <Size> from <Address>
{
TFileDescriptor *Descriptor;
   Descriptor = &FileDescriptor[File->Item.Name];
   NvmLoad(Descriptor->Start + Address, Data, Size);
   return YES;
} // FileLoad

//------------------------------------------------------------------------------
//   Fill
//------------------------------------------------------------------------------

TYesNo FileLocalFill( TFile *File, TFileAddress Address, byte Pattern, int Size)
// Write <Pattern> with <Size> at <Address>
{
TFileDescriptor *Descriptor;
   Descriptor = &FileDescriptor[File->Item.Name];
   if(!NvmFill(Descriptor->Start + Address, Pattern, Size)) {
      return NO;
   }
   
   return YES;
} // FileFill

//------------------------------------------------------------------------------
//   Match
//------------------------------------------------------------------------------

TYesNo FileLocalMatch( TFile *File, TFileAddress Address, const void *Data, int Size)
// Compare <Data> with <Size> at <Address>
{
TFileDescriptor *Descriptor;
   Descriptor = &FileDescriptor[File->Item.Name];
   if(!NvmMatch(Descriptor->Start + Address, Data, Size)) {
      return NO;
   }
   
   return YES;
} // FileMatch

//------------------------------------------------------------------------------
//   Sum
//------------------------------------------------------------------------------

TFileSum FileLocalSum( TFile *File, TFileAddress Address, int Size)
// Sum NVM contents at <Address> with <Size>
{
TFileDescriptor *Descriptor;
   Descriptor = &FileDescriptor[File->Item.Name];
   return NvmSum(Descriptor->Start + Address, Size);
} // FileSum

//------------------------------------------------------------------------------
//   Move
//------------------------------------------------------------------------------

TYesNo FileLocalMove( TFile *File, TFileAddress ToAddress, TFileAddress FromAddress, int Size)
// Move data <FromAddress> to <ToAddress> with <Size>. Check for overlapping
{
TFileDescriptor *Descriptor;
   Descriptor = &FileDescriptor[File->Item.Name];
   NvmMove(Descriptor->Start + ToAddress, Descriptor->Start + FromAddress, Size);
   return YES;
} // FileMove
