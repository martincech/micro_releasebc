//*****************************************************************************
//
//    Fram.h       FRAM memory access
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Fram_H__
   #define __Fram_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __FramSpi_H__
   #include "Fram/FramSpi.h"
#endif   

#ifdef __cplusplus
   extern "C" {
#endif
//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void FramInit( void);
// Initialization

TYesNo FramIsPresent( void);
// Returns YES if memory is present

byte FramByteRead( dword Address);
// Returns single byte from <Address>

void FramByteWrite( dword Address, byte Value);
// Write <Value> at <Address>

//------ Block read -----------------------------

void FramBlockReadStart( dword Address);
// Block read start

#define FramBlockReadData()            spiFramByteRead()
// Read byte from block

#define FramBlockReadStop()            spiFramRelease()
// Stop read block

//------ Block write ----------------------------

void FramPageWriteStart( dword Address);
// Start page write at <Address>

#define FramPageWriteData( Value)      spiFramByteWrite( Value)
// Write byte to page

#define FramPageWritePerform()         spiFramRelease()
// Write page

//------ Power save -----------------------------

void FramSleep( void);
// Enter sleep mode

void FramWakeup( void);
// Wakeup from sleep mode

//-----------------------------------------------------------------------------
// Tranparent data access
//-----------------------------------------------------------------------------

void FramSave( dword Address, const void *Data, int Size);
// Save <Data> with <Size> at <Address>

void FramLoad( dword Address, void *Data, int Size);
// Load <Data> with <Size> from <Address>

void FramFill( dword Address, byte Pattern, int Size);
// Write <Pattern> with <Size> at <Address>

TYesNo FramMatch( dword Address, const void *Data, int Size);
// Compare <Data> with <Size> at <Address>

dword FramSum( dword Address, int Size);
// Sum NVM contents at <Address> with <Size>

void FramMove( dword ToAddress, dword FromAddress, int Size);
// Move data <FromAddress> to <ToAddress> with <Size>. Check for overlapping

//-----------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
