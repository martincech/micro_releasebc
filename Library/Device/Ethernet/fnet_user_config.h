//******************************************************************************
//
//   fnet_user_config.h  Fnet stack library configuration file
//   Version 1.0         (c) VEIT Electronics
//
//******************************************************************************

#ifndef _FNET_USER_CONFIG_H_
#define _FNET_USER_CONFIG_H_

// default DNS server address, used when DHCP not enabled( for gsm allways)
#define FNET_CFG_IP4_DEFAULT_DNS        (FNET_IP4_ADDR_INIT(8, 8, 8, 8))

/*****************************************************************************
* Network interfaces
******************************************************************************/
#define FNET_CFG_CPU_ETH0            (1)
#define FNET_CFG_CPU_ETH_RMII        (0)
#define FNET_CFG_CPU_ETH_FULL_DUPLEX (1)
/*****************************************************************************
* IPv4 and/or IPv6 protocol support.
******************************************************************************/
#define FNET_CFG_IP4                (1)
#define FNET_CFG_IP4_FRAGMENTATION  (1)
#define FNET_CFG_IP6                (0)
/*****************************************************************************
* TCP/UDP protocol support.
******************************************************************************/
#define FNET_CFG_SOCKET_MAX         (10)   // maximum number of sockets at the same time
#define FNET_CFG_TCP                (1)
#define FNET_CFG_UDP                (1)
#define FNET_CFG_TCP_GSM            (0)
#define FNET_CFG_UDP_GSM            (0)
#define FNET_CFG_UDP_CHECKSUM       (1)
#define FNET_CFG_DHCP               (1)
#define FNET_CFG_ETH0_IP4_DNS       FNET_CFG_IP4_DEFAULT_DNS

/*****************************************************************************
* Enable proper compiler support, select processor type
******************************************************************************/
#ifndef __WIN32__
#define FNET_CFG_COMP_GNUC          (1)
#else
//#define FNET_CFG_COMP_UV            (1)
#define FNET_CFG_CPU_LITTLE_ENDIAN  (1)
#define FNET_CFG_CPU_ETH_MIB        (0)
#endif

#define FNET_CFG_CPU_MK60N512       (1)
#define FNET_CFG_CPU_CLOCK_HZ       (48000000)
/*****************************************************************************
* Size of the internal static heap buffer.
******************************************************************************/
#define FNET_CFG_HEAP_SIZE          (5 * 1024)

/*****************************************************************************
* Services
******************************************************************************/
#define FNET_CFG_DNS                         (1)
#define FNET_CFG_DNS_RESOLVER                (1)
/*****************************************************************************
* Debugging interface
******************************************************************************/
#define FNET_CFG_DEBUG                       (0)         //allow debug output to serial port
#define FNET_CFG_CPU_SERIAL_PORT_DEFAULT     (5)         //select uart port number
#define FNET_CFG_CPU_SERIAL_PORT_BAUD_RATE   (115200)    //select baud rate
#define FNET_CFG_DEBUG_TRACE                 (1)         //allow trace
#define FNET_CFG_DEBUG_TRACE_TCP             (1)         //trace TCP stack
#define FNET_CFG_DEBUG_TRACE_IP              (0)         //trace IP stack
#define FNET_CFG_DEBUG_TRACE_ETH             (0)
#define FNET_CFG_DEBUG_TRACE_ARP             (0)         //trace IP stack
#define FNET_CFG_DEBUG_TRACE_ARP_TABLE       (1)
#endif

