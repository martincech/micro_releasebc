//******************************************************************************
//
//   fnet_udp_gsm.h     TCP stack on GSM device implementation
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef _FNET_UDP_GSM_H_
#define _FNET_UDP_GSM_H_

#include "fnet_udp.h"


/************************************************************************
*    Protocol structure
*************************************************************************/
extern struct fnet_prot_if fnet_udp_gsm_prot_if;

#endif
