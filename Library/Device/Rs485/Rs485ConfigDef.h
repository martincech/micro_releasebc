//******************************************************************************
//
//   Rs485ConfigDef.h     RS485 module configuration definitions
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Rs485ConfigDef_H__
   #ifndef _MANAGED
      #define __Rs485ConfigDef_H__
   #endif

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//#include "Uart/Uart.h"

//------------------------------------------------------------------------------
//  Rs485 mode
//------------------------------------------------------------------------------

#ifdef _MANAGED
namespace Bat2Library{
   public enum class Rs485ModeE{
#else
typedef enum {
#endif
   RS485_MODE_SENSOR_PACK,
   RS485_MODE_MODBUS,
   RS485_MODE_MEGAVI,
   RS485_MODE_DACS,
#ifndef _MANAGED
   _RS485_MODE_LAST
} ERs485Mode;
#else
   };
}
#endif

#ifndef _MANAGED
//------------------------------------------------------------------------------
//  Rs485 module options
//------------------------------------------------------------------------------

typedef struct {
   TYesNo                  Enabled;
   byte                    Mode;
} TRs485Options;
//------------------------------------------------------------------------------
#endif

#ifdef _MANAGED
   #undef _MANAGED
   #include "Rs485ConfigDef.h"
   #define _MANAGED
#endif

#endif // __Rs485ConfigDef_H__


