//******************************************************************************
//
//   Scheduler.c   Bat2 Scheduler
//   Version 1.0   (c) VEIT Electronics
//
//******************************************************************************

#include "Tasks.h"
#include "Hardware.h"
#include "System/System.h"
#include "Multitasking/Multitasking.h"
#include "Tasks/TasksDef.h"
#ifdef INITIALIZE_STACK
#include <string.h>
#endif

//******************************************************************************
// Init
//******************************************************************************

void TasksInit( void)
// Init tasks
{
#ifdef INITIALIZE_STACK
int i;
   for(i = 0 ; i < TASKS_COUNT ; i++) {
      memset( Tasks[i]->Stack, 0xCC, Tasks[i]->StackSize);
   }
#endif
   MultitaskingInit();
} // TasksInit

//******************************************************************************
// Run
//******************************************************************************

static TYesNo Running = NO;

void TasksRun( void)
// Run tasks
{
int i;
   if(Running) {
      return;
   }
   for(i = 0 ; i < TASKS_COUNT ; i++) {
      MultitaskingTaskRun(Tasks[i]);
   }
   MultitaskingReschedule();
   Running = YES;
} // TasksRun

//******************************************************************************
// Stop
//******************************************************************************

void TasksStop( void)
// Stop tasks
{
int i;
   for(i = 0 ; i < TASKS_COUNT ; i++) {
      MultitaskingTaskExit(Tasks[i]);
   }
   Running = NO;
} // TasksStop

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

void TasksEvent( int Event)
//
{
int i;
   for(i = 0 ; i < TASKS_COUNT ; i++) {
      MultitaskingEventSet(Tasks[i], Event);
   }
} // TasksTimerEventSet

//------------------------------------------------------------------------------
//   Idle
//------------------------------------------------------------------------------

TYesNo TasksIdle( void)
// Checks for all tasks idle
{
int i;
   for(i = 0 ; i < TASKS_COUNT ; i++) {
      if(!MultitaskingTaskIdle(Tasks[i])) {
         return NO;
      }
   }
   return YES;
} // TasksIdle
