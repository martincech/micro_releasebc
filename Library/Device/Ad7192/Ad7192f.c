//*****************************************************************************
//
//    Ad7192f.c    A/D convertor AD7192 services 
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Ad7192f.h"
#include "System/System.h"
#include "Cpu/Cpu.h"
#include "Filter/FilterRelative.h"

#define MCLK 4920000L                            // internal clock [Hz]
#define RS0  3                                   // Register Select bit 0 shift

// Communications register :
#define CR_WEN              (1 << 7)             // Write enable
#define CR_RW               (1 << 6)             // Read/~Write
#define CR_SET_RS( r)       ((r) << 3)           // Register select
#define CR_CREAD            (1 << 2)             // Continuous read

#define CR_READ             CR_RW                // Read data from register
#define CR_WRITE            0x00                 // Write data to register

// Register address :
#define CR_COMMUNICATIONS_REGISTER  CR_SET_RS(0) // Communications register 8bit
#define CR_STATUS_REGISTER          CR_SET_RS(0) // Status register 8bit
#define CR_MODE_REGISTER            CR_SET_RS(1) // Mode register 24bit
#define CR_CONFIGURATION_REGISTER   CR_SET_RS(2) // Configuration register 24bit
#define CR_DATA_REGISTER            CR_SET_RS(3) // Data register 24/32bit
#define CR_ID_REGISTER              CR_SET_RS(4) // ID register 8bit
#define CR_GPOCON_REGISTER          CR_SET_RS(5) // GPOCON register 8bit
#define CR_OFFSET_REGISTER          CR_SET_RS(6) // Offset register 24bit
#define CR_FULL_SCALE_REGISTER      CR_SET_RS(7) // Offset register 24bit


// Status register :
#define SR_RDY              (1 << 7)             // ~Ready
#define SR_ERR              (1 << 6)             // ADC error
#define SR_NOREF            (1 << 5)             // No external reference
#define SR_PARITY           (1 << 4)             // Parity bit
#define SR_GET_CHD(r)       ((r) & 0x07)         // Channel for data 0


// Mode register :
#define MR_SET_MD( n)       (((dword)(n)) << 21) // set mode select bits
#define MR_GET_MD( r)       (((r) >> 21) & 0x07L)// get mode select bits
#define MR_DAT_STA          (1L << 20)           // status after data transmission
#define MR_SET_CLK( n)      (((dword)(n)) << 18) // set clock source
#define MR_GET_CLK( r)      (((r) >> 18) & 0x03L)// get clock source
#define MR_SINC3            (1L << 15)           // sinc3 filter/~sinc4 filter
#define MR_ENPAR            (1L << 13)           // parity bit enable
#define MR_CLK_DIV          (1L << 12)           // clock divide by 2
#define MR_SINGLE           (1L << 11)           // single cycle conversion
#define MR_REJ60            (1L << 10)           // 60Hz filter notch (for 50Hz only)
#define MR_SET_FS( n)       (((dword)(n)) <<  0) // set filter output data rate
#define MR_GET_FS( r)       ((r) & 0x3FFL)       // get filter output data rate

#define MR_EXTERNAL_CRYSTAL     MR_SET_CLK( 0)   // set external crystal clock
#define MR_EXTERNAL_CLOCK       MR_SET_CLK( 1)   // set external clock
#define MR_INTERNAL_CLOCK       MR_SET_CLK( 2)   // set internal clock
#define MR_INTERNAL_CLOCK_OUT   MR_SET_CLK( 3)   // set internal clock with output at MCLK2

#define MR_CONTINUOUS_MODE      MR_SET_MD( 0)    // continuous conversion
#define MR_SINGLE_MODE          MR_SET_MD( 1)    // single conversion mode
#define MR_IDLE                 MR_SET_MD( 2)    // idle mode - no conversions
#define MR_POWER_DOWN           MR_SET_MD( 3)    // power down mode
#define MR_INT_ZERO_CALIBRATION MR_SET_MD( 4)    // internal zero calibration
#define MR_INT_FULL_CALIBRATION MR_SET_MD( 5)    // internal full scale calibration
#define MR_SYS_ZERO_CALIBRATION MR_SET_MD( 6)    // system zero calibration
#define MR_SYS_FULL_CALIBRATION MR_SET_MD( 7)    // system full scale calibration

#define MR_SET_DATA_RATE( f)    MR_SET_FS( MCLK / (1024L * (f)))  // set conversion data rate [Hz]

#define MR_MIN_DATA_RATE        MR_SET_FS( 1023) // FS for 4.7 Hz
#define MR_MAX_DATA_RATE        MR_SET_FS( 1)    // FS for 4.8 kHz
#define MR_CAL_DATA_RATE        MR_SET_FS( 1008) // FS for 4.7 Hz (divisible by 16)


// Configuration register :
#define CR_CHOP          (1L << 23)              // Chop enable bit
#define CR_REFSEL        (1L << 20)              // 0 - REFIN1, 1 - REFIN2
#define CR_SET_CH( n)    (((dword)(n)) << 8)     // set channel select bits
#define CR_GET_CH( r)    (((r) >> 8) & 0xFFL)    // get channel
#define CR_BURN          (1L <<  7)              // set burnout current
#define CR_REFDET        (1L <<  6)              // enable reference detection
#define CR_BUF           (1L <<  4)              // enable buffer amplifier
#define CR_UB            (1L <<  3)              // Unipolar /~Bipolar
#define CR_SET_G( n)     (((dword)(n)) << 0)     // set gain
#define CR_GET_G( r)     (((r) & 0x07L)          // get gain

#define CR_GAIN_1        CR_SET_G(0)             // Set gain 1
#define CR_GAIN_8        CR_SET_G(3)             // Set gain 8
#define CR_GAIN_16       CR_SET_G(4)             // Set gain 16
#define CR_GAIN_32       CR_SET_G(5)             // Set gain 32
#define CR_GAIN_64       CR_SET_G(6)             // Set gain 64
#define CR_GAIN_128      CR_SET_G(7)             // Set gain 128

#define CR_CH_12         0x01                    // Select differential AIN1+2
#define CR_CH_34         0x02                    // Select differential AIN3+4
#define CR_CH_TEMP       0x04                    // Select temperature channel
#define CR_CH_22         0x08                    // Select differential AIN2+2
#define CR_CH_1          0x10                    // Select single AIN1+AINCOM
#define CR_CH_2          0x20                    // Select single AIN2+AINCOM
#define CR_CH_3          0x40                    // Select single AIN3+AINCOM
#define CR_CH_4          0x80                    // Select single AIN4+AINCOM

// GPOCON register :
#define GR_BPDSW         (1 << 6)                // bridge power down switch 1-on / 0-off
#define GR_GP32EN        (1 << 5)                // enable P3+P2
#define GR_GP10EN        (1 << 4)                // enable P1+P0
#define GR_P3DAT         (1 << 3)                // digital output P3
#define GR_P2DAT         (1 << 2)                // digital output P2
#define GR_P1DAT         (1 << 1)                // digital output P1
#define GR_P0DAT         (1 << 0)                // digital output P0


#define ADC_RESET_PULSES 40                      // clock count for reset

//-----------------------------------------------------------------------------
// Implementation constants
//-----------------------------------------------------------------------------

// mode register conversion control :
#define MR_DEFAULT_MODE         (MR_CONTINUOUS_MODE | MR_INTERNAL_CLOCK)
#define MR_IDLE_MODE            (MR_IDLE)
#define MR_POWER_DOWN_MODE      (MR_POWER_DOWN)
// mode register calibration :
#define MR_CALIBRATE_ZERO       (MR_INT_ZERO_CALIBRATION | MR_INTERNAL_CLOCK | MR_CAL_DATA_RATE)
#define MR_CALIBRATE_FULL_SCALE (MR_INT_FULL_CALIBRATION | MR_INTERNAL_CLOCK | MR_CAL_DATA_RATE |\
                                 MR_CLK_DIV)

// configuration register defaults :
#define CR_DEFAULT_CONFIG  (CR_SET_CH( CR_CH_12) | \
                            CR_BUF   | \
                            CR_GAIN_128)

// GPOCON register control :
#define GR_DEFAULT_GPOCON (GR_BPDSW)          // bridge on
#define GR_IDLE_GPOCON     0x00               // bridge off

// Parametric configuration values :
static word   FsValue;
static TYesNo Chop;
static TYesNo Sinc3;
static TYesNo Running;

//-----------------------------------------------------------------------------
// Internal variables
//-----------------------------------------------------------------------------

static TAdcValue _AdcRawValue;
static word      _AdcCount;
static int64     _AdcSum;
static TYesNo    _CallBackEnable;

//-----------------------------------------------------------------------------
// Local functions :
//-----------------------------------------------------------------------------

static void WriteByte( byte Value);
// Write data byte to ADC

static dword ReadWord( void);
// Read 24bit data from ADC

static void WriteWord( dword Value);
// Write 24bit to to ADC

AdcInterruptHandler();
// Adc interrupt

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void AdcInit( void)
// Initialisation
{
   AdcPortInit();
   // Defaults :
   AdcSetDIN();
   AdcSetSCLK();   
   AdcClrCS();                          // select
   AdcReset();                          // reset communication
   AdcIdle();                           // stop conversion
   // prepare default values :
   FsValue = MR_SET_DATA_RATE( ADC_CONVERSION_RATE);
   Chop    = NO;
   Sinc3   = NO;
   // prepare interrupt :
   AdcDisableInt();              // disable ADC external interrupt
   AdcInitInt();
   Running = NO;                        // stopped
   _AdcSum   = 0;
   _AdcCount = 0;
   FilterStop();                        // default filter status
} // AdcInit

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

void AdcReset( void)
// Reset communication
{
byte i;

   AdcDisableInt();
   AdcSetDIN();
   for( i = 0; i < ADC_RESET_PULSES + 2; i++){
      AdcClrSCLK();
      Nop();
      AdcSetSCLK();
   }
   SysDelay( 1);                       // wait at least 500us
} // AdcReset

//-----------------------------------------------------------------------------
// Data rate
//-----------------------------------------------------------------------------

void AdcDataRateSet( word Hertz)
// Set conversion data rate
{
   FsValue = (word)MR_SET_DATA_RATE( Hertz);
   // minimum divisor :
   if( FsValue < MR_MAX_DATA_RATE){
      FsValue = MR_MAX_DATA_RATE;
   }
   // maximum divisor :
   if( FsValue > MR_MIN_DATA_RATE){
      FsValue = MR_MIN_DATA_RATE;
   }
} // AdcDataRateSet

//-----------------------------------------------------------------------------
// Enable SINC3
//-----------------------------------------------------------------------------

void AdcSinc3Set( TYesNo Enable)
// <Enable> SINC3 filtering
{
   Sinc3 = Enable;
} // AdcSinc3Set

//-----------------------------------------------------------------------------
// Enable CHOP
//-----------------------------------------------------------------------------

void AdcChopSet( TYesNo Enable)
// <Enable> CHOP filtering
{
   Chop = Enable;
} // AdcChopSet

//-----------------------------------------------------------------------------
// Idle
//-----------------------------------------------------------------------------

void AdcIdle( void)
// Set idle mode
{
   AdcDisableInt();
   // Mode register :  
   WriteByte( CR_MODE_REGISTER | CR_WRITE);          // comm register
   WriteWord( MR_IDLE_MODE);                         // mode value
   // Bridge power off :
   WriteByte( CR_GPOCON_REGISTER | CR_WRITE);        // comm register
   WriteByte( GR_IDLE_GPOCON);                       // value
} // AdcIdle

//-----------------------------------------------------------------------------
// Power down
//-----------------------------------------------------------------------------

void AdcPowerDown( void)
// Set power down mode
{
   AdcDisableInt();
   // Mode register :  
   WriteByte( CR_MODE_REGISTER | CR_WRITE);          // comm register
   WriteWord( MR_POWER_DOWN_MODE);                   // mode value
   // Bridge power off :
   WriteByte( CR_GPOCON_REGISTER | CR_WRITE);        // comm register
   WriteByte( GR_IDLE_GPOCON);                       // value
} // AdcPoweDown

//-----------------------------------------------------------------------------
// Start
//-----------------------------------------------------------------------------

void AdcStart( void)
// Start continuous conversion
{
   AdcIdle();                                         // stop conversion
   // Configuration register :  
   WriteByte( CR_CONFIGURATION_REGISTER | CR_WRITE);  // comm register
   if( Chop){
      WriteWord( CR_DEFAULT_CONFIG | CR_CHOP);        // config value
   } else {
      WriteWord( CR_DEFAULT_CONFIG);                  // config value
   }
   // GPOCON register :  
   WriteByte( CR_GPOCON_REGISTER | CR_WRITE);         // comm register
   WriteByte( GR_DEFAULT_GPOCON);                     // bridge on
   // Mode register :  
   WriteByte( CR_MODE_REGISTER | CR_WRITE);           // comm register
   if( Sinc3){
      WriteWord( FsValue | MR_DEFAULT_MODE | MR_SINC3);    // mode value
   } else {
      WriteWord( FsValue | MR_DEFAULT_MODE);               // mode value
   }
   // Continuous read :
   WriteByte( CR_DATA_REGISTER | CR_READ | CR_CREAD);      // comm register
   AdcClrDIN();                                            // DIN = H does reset
   // Enable interrupt :
   Running = YES;                                     // conversions started
   FilterStart();                                     // start filtering
   AdcClearInt();                                     // clear old interupt flag
   AdcEnableInt();                                    // enable external interrupt
} // AdcStart

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void AdcStop( void)
// Stop continous conversion
{
   AdcDisableInt();
   if( Running){
      while( !AdcGetRDY());                           // wait for conversion
   }
   WriteByte( CR_DATA_REGISTER | CR_READ);            // stop continuous read
   AdcIdle();                                         // stop conversion
   FilterStop();                                      // stop filtering
   Running = NO;                                      // conversions stopped
} // AdcStop

//-----------------------------------------------------------------------------
// Raw read
//-----------------------------------------------------------------------------

TAdcValue AdcRawRead( void)
// Read actual conversion
{
TAdcValue Value;

   AdcDisableInt();                    // disable external interrupt
   Value = _AdcRawValue;               // get value
   AdcEnableInt();                     // enable external interrupt
   return( Value);
} // AdcRawRead

//-----------------------------------------------------------------------------
// Average read
//-----------------------------------------------------------------------------

TAdcValue AdcAverageRead( void)
// Read averaged value
{
TAdcValue Average;
int64     Sum;
word      Count;

   AdcDisableInt();                    // disable external interrupt
   Sum       = _AdcSum;                // remember value
   Count     = _AdcCount;              // remember value
   _AdcSum   = 0;                      // start new sum
   _AdcCount = 0;                      // start new count
   AdcEnableInt();                     // enable external interrupt
   if( Count == 0){
      return( 0);
   }
   Average = (TAdcValue)(Sum / Count);
   return( Average);
} // AdcAverageRead

//-----------------------------------------------------------------------------
// Low pass read
//-----------------------------------------------------------------------------

TAdcValue AdcLowPassRead( void)
// Read filtered conversion
{
TRawWeight Value;

   AdcDisableInt();                    // disable external interrupt
   Value = FilterRecord.LowPass;       // low pass value
   if( Running){
      AdcEnableInt();
   }
   return( Value);
} // AdcLowPassRead

//-----------------------------------------------------------------------------
// Stable value read
//-----------------------------------------------------------------------------

TYesNo AdcRead( TAdcValue *Weight)
// Read stable value
{
TYesNo Result;

   AdcDisableInt();                    // disable external interrupt
   Result = FilterRead( Weight);
   if( Running){
      AdcEnableInt();
   }
   return( Result);
} // AdcRead

//-----------------------------------------------------------------------------
// Callback enable
//-----------------------------------------------------------------------------

void AdcCallbackEnable( TYesNo Enable)
// Enables/disables ADC callback
{
   _CallBackEnable = Enable;
} // AdcCallBackEnable

//-----------------------------------------------------------------------------
// Write byte
//-----------------------------------------------------------------------------

static void WriteByte( byte Value)
// Write data byte to ADC
{
byte i;

   i = 8;
   do {
      AdcClrSCLK();
      AdcClrDIN();
      if( Value & 0x80){
         AdcSetDIN();
      }
      Nop();
      AdcSetSCLK();
      Value <<= 1;
   } while( --i);
   AdcSetDIN();
} // WriteByte

//-----------------------------------------------------------------------------
// Read word
//-----------------------------------------------------------------------------

static dword ReadWord( void)
// Read 24bit data from ADC
{
dword Value;
byte  i;

   Value  = 0;
   i = 24;
   do {
      Value <<= 1;
      AdcClrSCLK();
      Nop();
      if( AdcGetDOUT()){
         Value |= 1;
      }
      AdcSetSCLK();   
   } while( --i);
   return( Value);
} // ReadWord

//-----------------------------------------------------------------------------
// Write word
//-----------------------------------------------------------------------------

static void WriteWord( dword Value)
// Write 24bit to to ADC
{
byte i;

   i = 24;
   Value <<= 8;                        // bit 23 to MSB
   do {
      AdcClrSCLK();
      AdcClrDIN();
      if( Value & 0x80000000L){
         AdcSetDIN();
      }
      Nop();
      AdcSetSCLK();
      Value <<= 1;
   } while( --i);
   AdcSetDIN();
} // WriteWord

//-----------------------------------------------------------------------------
// Interrupt handler
//-----------------------------------------------------------------------------

#define RAW_ZERO  0x00800000L          // shifted zero

void AdcReadSample( void);

AdcInterruptHandler()
{
   AdcReadSample();
} // ADC_INT_vect

void AdcReadSample( void)
// Read sample
{
   AdcDisableInt();
   _AdcRawValue  = ReadWord();         // get conversion data
   AdcClearInt();                         // clear flag set by data read edges
   AdcEnableInt();
   _AdcRawValue -= RAW_ZERO;           // direct coding with shifted zero
   _AdcSum      += _AdcRawValue;       // averaging sum
   _AdcCount++;                        // averaging count
   FilterNextSample( _AdcRawValue);    // filtering
   if(_CallBackEnable) {
      AdcCallback();
   }
} // AdcReadSample