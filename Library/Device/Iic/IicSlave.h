//*****************************************************************************
//
//   IicSlave.h   I2C slave interface
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __IicSlave_H__
   #define __IicSlave_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

void IicSlaveInit( void);
// Initialize bus

void IicSlaveExecute( void);
// Execute callbacks

TYesNo IicSlaveBusy( void);
// Busy

void IicSlaveAddressed( TYesNo Rw);
// Addressed callback (synchronous)

TYesNo IicSlaveByteGet( byte *Data);
// Byte get (before I2C send) callback (synchronous)

void IicSlaveByteProcess(byte Data);
// Byte proccess (on I2C receive) callback (synchronous)

void IicSlaveStopped( void);
// Stop callback (synchronous)

void IicSlaveEvent( void);
// Event asynchronous callback

#endif