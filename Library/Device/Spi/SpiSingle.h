//*****************************************************************************
//
//   SpiSingle.h  Single SPI interface
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SpiSingle_H__
   #define __SpiSingle_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void SpiInit( void);
// Initialize bus

void SpiAttach( void);
// Attach SPI bus - activate chipselect

void SpiRelease( void);
// Release SPI bus - deactivate chipselect

byte SpiByteRead( void);
// Read byte from SPI

void SpiByteWrite( byte Value);
// Write byte to SPI

byte SpiByteTransceive( byte TxValue);
// Write/read SPI. Returns value read

#endif
