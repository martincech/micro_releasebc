//*****************************************************************************
//
//    Kbd.c        Keyboard services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Kbd/Kbd.h"
#include "Kbd/Kbx.h"              // Keyboard matrix
#include "System/System.h"        // Operating system
#include "Cpu/Cpu.h"              // WatchDog

word _KbdCounter;                      // timer counter
word _KbdStatus;                       // context
word _LastKey;                         // last pressed key

// kbd counter context info :
#define KBD_IDLE         0x0000        // wait for first press
#define KBD_FIRST_PRESS  0x0001        // wait for stabilisation after first press
#define KBD_WAIT_REPEAT  0x0002        // wait for autorepeat
#define KBD_WAIT_RELEASE 0x0003        // stop autorepeat, wait for release

#define StatusGet()               _KbdStatus
#define CounterExpired()         (_KbdCounter == 0)
#define StatusSet( Code, Timeout) _KbdStatus  = (Code);_KbdCounter = TimerFastCount(Timeout)
#define StatusIdleSet()           _KbdStatus  = KBD_IDLE

// Local functions :

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void KbdInit( void)
// Initialization
{
   _KbdCounter = 0;                 // timer counter
   _KbdStatus  = 0;                 // context
   _LastKey     = K_RELEASED;        // context key (getch)
   KbxInit();
} // KbdInit

//-----------------------------------------------------------------------------
// Power-up key
//-----------------------------------------------------------------------------

int KbdPowerUpKey( void)
// Returns key hold after power up, or K_RELEASED
{
int Key;

   Key = KbxRead();
   SysDelay( 10);
   return( KbxRead());
} // KbdPowerUpKey

//-----------------------------------------------------------------------------
// Power-up key release
//-----------------------------------------------------------------------------

void KbdPowerUpRelease( void)
// Wait for power up key release
{
   while( KbxRead() != K_RELEASED){
      WatchDog();            // wait for release
   }
} // KbdPowerUpRelease

//-----------------------------------------------------------------------------
// Test
//-----------------------------------------------------------------------------

TYesNo kbhit( void)
// Returns YES on key hit
{
   _LastKey = KbdGet();
   if( _LastKey == K_IDLE){
      return( NO);            // nothing pressed
   }
   return( YES);
} // kbhit

//-----------------------------------------------------------------------------
// Wait key
//-----------------------------------------------------------------------------

int getch( void)
// Waits for key, returns it
{
int Key;

   if( _LastKey == K_IDLE){
      // wait for press
      while( !kbhit()){
         WatchDog();
      }
   }
   Key = _LastKey;                     // remember
   // wait for release :
   while( KbdGet() != K_RELEASED){
       WatchDog();
   }
   _LastKey = K_IDLE;                  // release
   return( Key);
} // getch

//-----------------------------------------------------------------------------
// Wait release
//-----------------------------------------------------------------------------

void KbdDisable( void)
// Disable key, wait for release
{
   StatusSet( KBD_WAIT_RELEASE, 0);     // new context
} // KbdDisable

//-----------------------------------------------------------------------------
// Get key
//-----------------------------------------------------------------------------

int KbdGet( void)
// Test for key, returns K_IDLE for no key pressed
{
int Key;

   Key = KbxRead();
   switch( StatusGet()){
      case KBD_IDLE :
         if( Key == K_RELEASED){
            return( K_IDLE);           // nothing pressed
         }
         StatusSet( KBD_FIRST_PRESS, KBD_DEBOUNCE);         // wait for key stabilisation
         return( K_IDLE);

      case KBD_FIRST_PRESS :
         if( !CounterExpired()){
            return( K_IDLE);           // wait after first touch
         }
         if( Key == K_RELEASED){
            StatusIdleSet();           // wait for first touch
            return( K_IDLE);           // stabilisation not reached
         }
         StatusSet( KBD_WAIT_REPEAT, KBD_AUTOREPEAT_START); // wait for autorepeat
         _LastKey = Key;
         return( Key);                 // valid key

      case KBD_WAIT_REPEAT :
         if( Key == K_RELEASED){
            StatusIdleSet();           // wait for first touch
            return( _LastKey | K_RELEASED);       // key release
         }
         if( !CounterExpired()){
            return( K_IDLE);           // wait for autorepeat
         }
         // autorepeat start :
         StatusSet( KBD_WAIT_REPEAT, KBD_AUTOREPEAT_SPEED); // wait for next autorepeat
         return( _LastKey | K_REPEAT); // repeat key

      case KBD_WAIT_RELEASE :
         if( Key == K_RELEASED){
            StatusIdleSet();           // wait for first touch
            return( _LastKey | K_RELEASED);       // key release
         }
         return( K_IDLE);
   }
   return( K_IDLE);                    // let be compiler happy
} // KbdGet

//------------------------------------------------------------------------------
//  Timer
//------------------------------------------------------------------------------

void KbdTimer( void)
// Keyboard timer handler
{
   if( _KbdCounter){
      --_KbdCounter;
   }
} // KbdTimer
