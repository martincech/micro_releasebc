//*****************************************************************************
//
//    Dacs.h        Dacs communication throug serial port
//    Version 1.0    (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __DACS_H__
   #define __DACS_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __DacsDef_H__
   #include "Dacs/DacsDef.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef enum {
   DACS_IDLE,
   DACS_RECEIVE_LISTENING,           // Listening any incoming communication
   DACS_RECEIVE_ERROR,               // Error while processing incoming message
   DACS_RECEIVE_DATA_WAITING,        // Incoming command successfully received
   DACS_SEND_ACTIVE,                 // Sending of reply active
   DACS_SEND_DONE,                   // Sending of reply finished
   _DACS_LAST
} EDacsStatus;

typedef byte TDacsStatus;

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------
void DacsInit( int uartPort);
// Initialize

void DacsDeinit( void);
// Deinitialize

TDacsStatus DacsStatus( void);
// Check for last operation status

void DacsListenStart( void);
// Starts listening on port

void DacsListenStop( void);
// Stops listening on port

TDacsRequest *DacsReceive( void);
// Reads incoming data from receive buffer, succeed only when DACS_RECEIVE_DATA_WAITING
// return NULL pointer when no data avaible or data error

TYesNo DacsSend( void);
// Sends reply, reply buffer should be set

TDacsReply *DacsGetReplyBuffer();
// Returns buffer for outgoing message, should be filled before calling DacsSend

#ifdef __cplusplus
   }
#endif

#endif // __DACS_H__
