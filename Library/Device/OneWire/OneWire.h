//*****************************************************************************
//
//   OneWire.h   One wire bus
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#ifndef __OneWire_H__
   #define __OneWire_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#define ONE_WIRE_CMD_SEARCH_ROM       0xF0
#define ONE_WIRE_CMD_READ_ROM         0x33
#define ONE_WIRE_CMD_MATCH_ROM        0x55
#define ONE_WIRE_CMD_SKIP_ROM         0xCC
#define ONE_WIRE_CMD_ALARM_SEARCH     0xEC

typedef enum {
   ONE_WIRE_CH0,
   ONE_WIRE_CH1,
   ONE_WIRE_CH2,
   ONE_WIRE_CH3,
   ONE_WIRE_CH4,
   ONE_WIRE_CH5,
   ONE_WIRE_CH6,
   ONE_WIRE_CH7
} EOneWireChannel;

typedef byte TOneWireChannel;

void OneWireInit( void);
// Initialization

TYesNo OneWireReset( TOneWireChannel Channel);
// Resets bus, returns YES if device present

void OneWireByteWrite( TOneWireChannel Channel, byte Value);
// Write byte

byte OneWireByteRead( TOneWireChannel Channel);
// read byte

void OneWireStrongPullUpEnable( TOneWireChannel Channel, TYesNo Enable);
// Strong pull enable

#endif
