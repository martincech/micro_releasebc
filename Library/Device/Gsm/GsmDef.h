//******************************************************************************
//
//   GsmDef.h     GSM data types
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __GsmDef_H__
   #define __GsmDef_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

//-----------------------------------------------------------------------------
//  Data types
//-----------------------------------------------------------------------------

#define GSM_OPERATOR_SIZE              16        // max. operator name size
#define GSM_PHONE_NUMBER_SIZE          21        // max. Phone number size (1 char for + sign)
#define GSM_SMS_SIZE                   160       // max. SMS message text size(7bit data)
#define GSM_SMS_BYTE_SIZE              140       // max. SMS message byte size(8bit data)
#define GSM_BUFFER_SIZE                1600      // communication buffer size

#define GSM_SMS_CHANNEL_DATA_MAX_SIZE  (20*GSM_SMS_BYTE_SIZE) // max 2800 byte per channel
#define GSM_SMS_CHANNEL_STACK_SIZE     6*GSM_SMS_CHANNEL_DATA_MAX_SIZE    // size of stack for gsm channel

#define GSM_PIN_SIZE_MIN               4         // min. PIN characters count
#define GSM_PIN_SIZE_MAX               8         // max. PIN characters count

#define GSM_SIGNAL_MIN                 0         // min. signal strength
#define GSM_SIGNAL_MAX                 31        // max. signal strength
#define GSM_SIGNAL_UNDEFINED           99        // undefined signal strength

//-----------------------------------------------------------------------------
//  Socket related types
//-----------------------------------------------------------------------------

typedef enum{
   STATE_UNDEFINED,
   STATE_ALOCATED = 2,
   STATE_CONNECTING,
   STATE_UP,
   STATE_CLOSING,
   STATE_DOWN,
   STATE_ALERTING,
   STATE_CLIENT_CONNECTED,
   STATE_RELEASED,
   _STATE_LAST
} TGsmNetSocketState;

//-----------------------------------------------------------------------------
//  SMS related types
//-----------------------------------------------------------------------------

// SMS read return codes :
typedef enum {
   GSM_SMS_READ_OK,                         // SMS read O.K.
   GSM_SMS_READ_EMPTY,                      // Empty memory position
   GSM_SMS_READ_ERROR,                      // SMS read error
   _GSM_SMS_READ_LAST
} ESmsReadStatus;

typedef byte TSmsReadStatus;                // ESmsReadStatus

//-----------------------------------------------------------------------------
//  SMS socket(channel) related types
//-----------------------------------------------------------------------------

// SMS send return codes :
typedef enum {
   GSM_SMS_SEND_OK,                 // delivery O.K., data delivered to destination
   GSM_SMS_SEND_WAIT_START,         // waiting for start, no sms sended yet

   GSM_SMS_SEND_WAIT_FIRST_ACK,     // waiting for first sms acknowledgement, one sms sent allready
   GSM_SMS_SEND_RESEND_FIRST,       // resending first message
   GSM_SMS_SEND_SENDING,            // sending in progress, not all sms's sent
   GSM_SMS_SEND_RESENDING,          // resending some non-acked data
   GSM_SMS_SEND_WAIT_SOME_ACK,      // sending done, just waiting for acknowledgement

   GSM_SMS_SEND_ERROR,              // delivery error, can't deliver to destination(unreachable or unknown destination)
   GSM_SMS_SEND_ERROR_INTERNAL,     // error due to gsm modem send problem
   GSM_SMS_SEND_TIMEOUT,            // delivery error, destination response timeout during sending (although some sms's was delivered)
   GSM_SMS_SEND_STOPPED,            // sending stopped by user action
   GSM_SMS_SEND_WRONG_CHANNEL_DESC, // supplied channel is not valid
   _GSM_SMS_SEND_LAST
} EChannelStatus;


typedef unsigned long TSmsChannel;
typedef byte TSmsChannelSendStatus;

#define SMS_INVALID_CHANNEL   ((TSmsChannel) - 1)
#endif
