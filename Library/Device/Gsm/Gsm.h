//******************************************************************************
//
//   Gsm.h        GSM services
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Gsm_H__
   #define __Gsm_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __GsmDef_H__
   #include "Gsm/GsmDef.h"
#endif

#ifndef __uClock_H__
   #include "Time/uClock.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

#ifdef __GSM_SMS_STATISTICS__
// Gsm module statistics
typedef struct {
      // sends
   unsigned SmsBSendCount;     // binary data sms send count(channel)
   unsigned SmsBLostCount;     // binary data sms lost count(channel)
   unsigned SmsASendCount;     // channel ack sms send count
   unsigned SmsALostCount;     // channel ack sms lost count
   unsigned SmsDeliveryCount;  // delivery receive count
   unsigned SmsTSendCount;     // text sms send count
      //receives
   unsigned SmsBRcvCount;      // binary data sms receive count(channel)
   unsigned SmsARcvCount;      // binary ack sms receive count(channel)
   unsigned SmsTRcvCount;      // text sms receive count
      //times
   unsigned AverageDeliverTime;  // average time between sms send and it's delivery acknowledge
   unsigned AverageDeliverCount; // number of values in AverageDeliverTime calculation
} TGsmSmsStatistics;

extern TGsmSmsStatistics GsmStatistics;
#endif
//-----------------------------------------------------------------------------
//  Functions
//-----------------------------------------------------------------------------

void GsmInit( void);
// Initialize

void GsmDeinit( void);
// Deinitialize

TYesNo GsmShutdown( void);
// Shutdown modem by AT command

TYesNo GsmReset( void);
// Initialize modem (no PIN needed)

TYesNo GsmRegistered( void);
// Returns YES on modem registered in network

TYesNo GsmOperator( char *Name);
// Returns operator <Name>

TYesNo GsmPhoneNumber( char *PhoneNumber);
// Returns own <PhoneNumber>

byte GsmSignalStrength( void);
// Returns relative signal strength

TYesNo GsmPinReady( void);
// Returns YES on valid PIN

TYesNo GsmPinEnter( char *Pin);
// Enters <Pin>

byte GsmError( void);
// Returns last operation error code

//-- SMS ----------------------------------------------------------------------

TYesNo GsmSmsInit( void);
// Initialize SMS service (with valid PIN only)

TYesNo GsmSmsSend( char *PhoneNumber, const char *Format, ...);
// Send single SMS message by <Format> to <PhoneNumber>

TYesNo GsmSmsSendBinary(char *PhoneNumber, char *Data, int size);
// Send single SMS message with binary data to <PhoneNumber>

TSmsReadStatus GsmSmsRead(char *PhoneNumber, char *Text, UDateTimeGauge *ReceivedTime);
// Gets single SMS if some available

#ifdef _SOCKET_IF_SMS_
//-- SMS channel --------------------------------------------------------------
#include "GsmChannel.h"
#endif

//--Data modem -----------------------------------------------------------------

TYesNo GsmNetInit( void);
// Initialize internet underlying connection_Id stack)

TYesNo GsmNetSocketSetup(byte SockAddr, const char* address, const char *port, TYesNo Tcp, unsigned short Tcp_KeepIdle, byte Tcp_KeepCnt, byte Tcp_KeepIntvl);
// Initialize socket connection to remote address:port, when <Tcp> select Tcp socket else Udp

TYesNo GsmNetSocketOpen(byte SockAddr);
// open previously configured socket

TYesNo GsmNetSocketClose(byte SockAddr);
// close socket

TGsmNetSocketState GsmNetSocketState( byte SockAddr);
// read socket state

TYesNo GsmNetGetSocketAddress( byte SockAddr, char* const address, char* const port);
// return local socket address and port

int GsmNetWrite( byte SockAddr, byte *data, int size);
// write data of size, return actual writen size
int GsmNetRead( byte SockAddr, byte *data, int MaxSize);
// read data up to maxsize, retur actual read size
//------------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
