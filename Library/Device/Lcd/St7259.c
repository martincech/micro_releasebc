//*****************************************************************************
//
//    St7259.c      ST7259 generic graphic controller services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "St7259.h"
#include "Lcd/St7259Gpio.h"
#include "System/System.h"
#include "Cpu/Cpu.h"

#define GPU_BUS_MASK       0xFF                            // D0..D7 bus mask
#define GPU_MAX_RAM_COLUMN 85                              // 255 pixels RAM width

// ST7529 commands ----------------------------------------------------------

#define GPU_CMD_EXTIN         0x30               // Ext In  (Ext = 0)
#define GPU_CMD_EXTOUT        0x31               // Ext Out (Ext = 1)
// Ext = 0
#define GPU_CMD_DISON         0xAF               // Display On
#define GPU_CMD_DISOFF        0xAE               // Display Off
#define GPU_CMD_DISNOR        0xA6               // Normal display
#define GPU_CMD_DISINV        0xA7               // Inverse display
#define GPU_CMD_COMSCN        0xBB               // COM scan direction
#define GPU_CMD_DISCTRL       0xCA               // Display control
#define GPU_CMD_SLPIN         0x95               // Sleep in
#define GPU_CMD_SLPOUT        0x94               // Sleep out
#define GPU_CMD_LASET         0x75               // Line Address Set
#define GPU_CMD_CASET         0x15               // Column Address Set
#define GPU_CMD_DATSDR        0xBC               // Data Scan Direction
#define GPU_CMD_RAMWR         0x5C               // Writing to Memory
#define GPU_CMD_RAMRD         0x5D               // Reading from Memory
#define GPU_CMD_PTLIN         0xA8               // Partial Display in
#define GPU_CMD_PTLOUT        0xA9               // Partial Display out
#define GPU_CMD_RMWIN         0xE0               // Read & Modify in
#define GPU_CMD_RMWOUT        0xEE               // Read & Modiry end
#define GPU_CMD_ASCSET        0xAA               // Area Scroll Set
#define GPU_CMD_SCSTART       0xAB               // Scroll Start Set
#define GPU_CMD_OSCON         0xD1               // Internal OSC on
#define GPU_CMD_OSCOFF        0xD2               // Internal OSC off
#define GPU_CMD_PWRCTRL       0x20               // Power control
#define GPU_CMD_VOLCTRL       0x81               // EC control
#define GPU_CMD_VOLUP         0xD6               // EC increase
#define GPU_CMD_VOLDOWN       0xD7               // EC decrease
#define GPU_CMD_RESERVED      0x82               // don't use
#define GPU_CMD_EPSRRD1       0x7C               // Read register 1
#define GPU_CMD_EPSRRD2       0x7D               // Read register 2
#define GPU_CMD_NOP           0x25               // NOP instruction
#define GPU_CMD_STREAD                           // Status read
#define GPU_CMD_EPINT         0x07               // Initial code
// Ext = 1
#define GPU_CMD_GRAY1SET      0x20               // FRAME1 gray PWM set
#define GPU_CMD_GRAY2SET      0x21               // FRAME2 gray PWM set
#define GPU_CMD_WTSET         0x22               // Weight set
#define GPU_CMD_ANASET        0x32               // Analog circuit set
//#define GPU_CMD_SWINT         0x34               // Software Initial
#define GPU_CMD_DITHOFF       0x34               // Dither off
#define GPU_CMD_DITHON        0x35               // Dither on
#define GPU_CMD_EPCTIN        0xCD               // Control EEPROM
#define GPU_CMD_EPCOUT        0xCC               // Cancel EEPROM
#define GPU_CMD_EPMWR         0xFC               // Write to EEPROM
#define GPU_CMD_EPMRD         0xFD               // Read from EEPROM

// Parameters -----------------------------------------------------------------

// COMSCN
#define GPU_COMSCN_NORMAL     0x00               // scan direction 0..79, 80..159
#define GPU_COMSCN_DINVERT    0x01               // scan direction 0..79, 159..80
#define GPU_COMSCN_UINVERT    0x02               // scan direction 79..0, 80..159
#define GPU_COMSCN_INVERT     0x03               // scan direction 79..0, 159..80

// DISCTRL
#define GPU_DISCTRL1_NOCLD    0x00               // no clock division
#define GPU_DISCTRL1_CLD      0x04               // clock / 2
#define GPU_DISCTRL2_DT       0x3F               // drive duty mask
#define GPU_DISCTRL3_LF       0x0F               // line cycles (2..16) mask
#define GPU_DISCTRL3_FI       0x10               // inversion frame

// DATSDR
#define GPU_DATSDR1_NORMAL    0x00               // without inversions
#define GPU_DATSDR1_LI        0x01               // line address inversion
#define GPU_DATSDR1_CI        0x02               // column address inversion
#define GPU_DATSDR1_CL        0x04               // address scan in the line direction
#define GPU_DATSDR2_NORMAL    0x00               // segment P1, P2, P3 arrangement
#define GPU_DATSDR2_CLR       0x01               // segment P3, P2, P1 arrangement
#define GPU_DATSDR3_2B3P      0x01               // 2 byte / 3 pixel
#define GPU_DATSDR3_3B3P      0x02               // 3 byte / 3 pixel


// PWRCTRL
#define GPU_PWRCTRL_VR        0x01               // reference voltage
#define GPU_PWRCTRL_VF        0x02               // voltage follower
#define GPU_PWRCTRL_VB        0x08               // booster
#define GPU_PWRCTRL_ALL       (GPU_PWRCTRL_VR | GPU_PWRCTRL_VF | GPU_PWRCTRL_VB)

// VOLCTRL
#define GPU_VOLCTRL1_MASK     0x3F               // LSB
#define GPU_VOLCTRL2_SHIFT    6                  // upper byte shift
#define GPU_VOLCTRL2_MASK     0x07               // MSB

// ANASET
#define GPU_ANASET1_127       0x00               // Frequency 12.7 kHz
#define GPU_ANASET1_132       0x04               // Frequency 13.2 kHz
#define GPU_ANASET1_143       0x02               // Frequency 14.3 kHz
#define GPU_ANASET1_157       0x06               // Frequency 15.7 kHz
#define GPU_ANASET1_173       0x01               // Frequency 17.3 kHz
#define GPU_ANASET1_193       0x05               // Frequency 19.3 kHz
#define GPU_ANASET1_219       0x03               // Frequency 21.9 kHz
#define GPU_ANASET1_254       0x07               // Frequency 25.4 kHz
#define GPU_ANASET2_3K        0x00               // Booster 3kHz
#define GPU_ANASET2_6K        0x01               // Booster 6kHz
#define GPU_ANASET2_12K       0x02               // Booster 12kHz
#define GPU_ANASET2_24K       0x03               // Booster 24kHz
#define GPU_ANASET3_14        0x00               // LCD Bias 1/14
#define GPU_ANASET3_13        0x01               // LCD Bias 1/13
#define GPU_ANASET3_12        0x02               // LCD Bias 1/12
#define GPU_ANASET3_11        0x03               // LCD Bias 1/11
#define GPU_ANASET3_10        0x04               // LCD Bias 1/10
#define GPU_ANASET3_9         0x05               // LCD Bias 1/9
#define GPU_ANASET3_7         0x06               // LCD Bias 1/7
#define GPU_ANASET3_5         0x07               // LCD Bias 1/5

// EPINT
#define GPU_EPINT1            0x19               // EEPROM parameters

// EPCTIN
#define GPU_EPCTIN1_EERD      0x00               // EEPROM read
#define GPU_EPCTIN1_EEWR      0x20               // EEPROM write

//-----------------------------------------------------------------------------

#ifndef GPU_DISABLE_BUFFER
   TGraphicBuffer GBuffer;             // working buffer
#endif

// Local functions :

#define WriteData( Data)      GpuWrite( Data)
// Write data

static void WriteCommand( byte Cmd);
// Write command

static void SetData( unsigned Value);
// Write data to bus D0..D7

static void ClearRam( void);
// Clear all RAM

static void SetArea( int RowLo, int RowHi, int ColLo, int ColHi);
// Set read/write area

#if G_PLANES == 2
static void SetGrayscale( void);
// Set gray scale palette
#endif

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void GpuInit( void)
// Initialization
{
   GpuClearAll();                      // all controls to inactive, A0 = L
   GpuPortInit();                     // port directions
   GpuRsSet();                         // data mode default
   GpioBusEnable( GPU_DATA_OFFSET, GPU_BUS_MASK);        // enable GPIO
   GpioBusOutput( GPU_DATA_OFFSET, GPU_BUS_MASK);        // output direction

   GpuResClr();                        // activate /RES
   SysUDelay( 5);                      // reset pulse
   GpuResSet();                        // deactivate /RES
   SysUDelay( 5);                      // wait for ready
   GpuSelect();                        // activate /CS

   WriteCommand( GPU_CMD_EXTIN);
   WriteCommand( GPU_CMD_SLPOUT);
   WriteCommand( GPU_CMD_OSCON);

   WriteCommand( GPU_CMD_PWRCTRL);
   WriteData( GPU_PWRCTRL_VB);

   SysDelay( 2);

   WriteCommand( GPU_CMD_PWRCTRL);
   WriteData( GPU_PWRCTRL_ALL);

   WriteCommand( GPU_CMD_VOLCTRL);
   WriteData( GPU_VOLCTRL1_MASK &  GPU_EC_BASE);
   WriteData( GPU_VOLCTRL2_MASK & (GPU_EC_BASE >> GPU_VOLCTRL2_SHIFT));

   WriteCommand( GPU_CMD_DISCTRL);
   WriteData( GPU_DISCTRL1_CLD);
   WriteData( GPU_DISCTRL2_DT & (GPU_MAX_ROW / 4 - 1));
   WriteData( GPU_DISCTRL3_LF &  GPU_LF);

   WriteCommand( GPU_CMD_DISNOR);

   WriteCommand( GPU_CMD_COMSCN);
   WriteData( GPU_COMSCN_DINVERT);

   WriteCommand( GPU_CMD_DATSDR);
   WriteData( GPU_DATSDR1);
   WriteData( GPU_DATSDR2);
   WriteData( GPU_DATSDR3_3B3P);

   WriteCommand( GPU_CMD_EXTOUT);
   WriteCommand( GPU_CMD_ANASET);
   WriteData( GPU_ANASET1);
   WriteData( GPU_ANASET2);
   WriteData( GPU_ANASET3);
   WriteCommand( GPU_CMD_DITHOFF);
   WriteCommand( GPU_CMD_EXTIN);
#if G_PLANES == 2
   SetGrayscale();
#endif
   ClearRam();
   WriteCommand( GPU_CMD_DISON);

   GpuDeselect();    // deselect /CS
#ifndef GPU_DISABLE_BUFFER
   GpuInitBuffer();
#endif
} // GpuInit

//-----------------------------------------------------------------------------
// On
//-----------------------------------------------------------------------------

void GpuOn( void)
// Display On
{
   GpuSelect();
   WriteCommand( GPU_CMD_DISON);
   GpuDeselect();
} // GpuOn

//-----------------------------------------------------------------------------
// Off
//-----------------------------------------------------------------------------

void GpuOff( void)
// Display Off
{
   GpuSelect();
   WriteCommand( GPU_CMD_DISOFF);
   GpuDeselect();
} // GpuOff

//-----------------------------------------------------------------------------
// Clear
//-----------------------------------------------------------------------------

void GpuClear( void)
// Clear display
{
   GpuSelect();
   ClearRam();
   GpuDeselect();
} // GpuClear

//-----------------------------------------------------------------------------
// Goto
//-----------------------------------------------------------------------------

void GpuGoto( int Row, int Column)
// Set coordinate to <Row>,<Column>
{
   GpuSelect();
   SetArea( Row, GPU_MAX_ROW - 1, Column + CPU_COLUMN_OFFSSET, GPU_MAX_COLUMN - 1  + CPU_COLUMN_OFFSSET);
   WriteCommand( GPU_CMD_RAMWR);
} // GpuGoto

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void GpuWrite( unsigned Pattern)
// Write <Pattern> to current coordinate (increment <Column>)
{
   GpuWrClr();
   SetData( Pattern);
   GpuWrSet();
} // GpuWrite

//-----------------------------------------------------------------------------
// Done
//-----------------------------------------------------------------------------

void GpuDone( void)
// Write done
{
   GpuDeselect();
} // GpuDone

//-----------------------------------------------------------------------------
// Contrast
//-----------------------------------------------------------------------------

void GpuContrast( unsigned Value)
// Set display contrast
{
   GpuSelect();
   WriteCommand( GPU_CMD_VOLCTRL);
   WriteData( GPU_VOLCTRL1_MASK &  Value);
   WriteData( GPU_VOLCTRL2_MASK & (Value >> GPU_VOLCTRL2_SHIFT));
   GpuDeselect();
} // GpuContrast

//-----------------------------------------------------------------------------
// Shutdown
//-----------------------------------------------------------------------------

void GpuShutdown( void)
// Power off sequence
{
   GpuSelect();
   WriteCommand( GPU_CMD_SLPIN);
   GpuDeselect();
   GpuResClr();                        // activate /RES
   SysDelay( 15);                      // wait min. 12ms
} // GpuShutdown

//-----------------------------------------------------------------------------
#ifndef GPU_DISABLE_BUFFER

#define GPU_COLUMN_WIDTH 3             // minimal column resolution

// calculation Y -> Row
#define GetRow( y)       ((y) >> 3)    // vertical byte address
#define GetLine( y)      ((y) & 0x07)  // vertical byte bit address

#if G_PLANES == 1
//-----------------------------------------------------------------------------
// Flush
//-----------------------------------------------------------------------------

void GpuFlush( void)
// Copy active buffer to controller
{
int      x, y;
int      minX, maxX, minCol;
unsigned Mask;
byte    *Buffer;

   // round X coordinate to resolution :
   minCol = (GBuffer.MinX / GPU_COLUMN_WIDTH);
   minX   = minCol * GPU_COLUMN_WIDTH;
   maxX   = GBuffer.MaxX / GPU_COLUMN_WIDTH + 1;       // max index to strips count
   maxX  *= GPU_COLUMN_WIDTH;
   for( y = GBuffer.MinY; y <= GBuffer.MaxY; y++){
      GpuGoto( y, minCol);
      Mask = (1 << GetLine( y));                      // bit in the row byte
      Buffer = &GBuffer.Buffer[ GetRow( y)][ minX];   // byte start of the area
//    for( x = minX; x < maxX; x++){
      x = maxX - minX;
      do {
         GpuWrClr();
#ifdef GPU_STRICT_COLOR
         GpioBusClr( GPU_DATA_OFFSET, GPU_BUS_MASK);
         if( *Buffer++ & Mask){
            GpioBusSet( GPU_DATA_OFFSET, GPU_COLOR_BLACK);
         } else {
            GpioBusSet( GPU_DATA_OFFSET, GPU_COLOR_WHITE);
         }
#else
         if( *Buffer++ & Mask){
            GpioBusClr( GPU_DATA_OFFSET, GPU_BUS_MASK);
         } else {
            GpioBusSet( GPU_DATA_OFFSET, GPU_BUS_MASK);
         }
#endif
         GpuWrSet();
      } while( --x);
   }
   GpuDone();
   GpuInitBuffer();
} // GpuFlush

#elif G_PLANES == 2
//-----------------------------------------------------------------------------
// Flush
//-----------------------------------------------------------------------------

void GpuFlush( void)
// Copy active buffer to controller
{
int      x, y;
int      minX, maxX, minCol;
unsigned Mask;
byte    *Buffer;

   // round X coordinate to resolution :
   minCol = (GBuffer.MinX / GPU_COLUMN_WIDTH);
   minX   = minCol * GPU_COLUMN_WIDTH;
   maxX   = GBuffer.MaxX / GPU_COLUMN_WIDTH + 1;       // max index to strips count
   maxX  *= GPU_COLUMN_WIDTH;
   for( y = GBuffer.MinY; y <= GBuffer.MaxY; y++){
      GpuGoto( y, minCol);
      Mask = (1 << GetLine( y));                          // bit in the row byte
      Buffer = &GBuffer.Buffer[ 0][ GetRow( y)][ minX];   // byte start of the plane 0 area
//    for( x = minX; x < maxX; x++){
      x = maxX - minX;
      do {
         GpuWrClr();
         GpioBusSet( GPU_DATA_OFFSET, GPU_BUS_MASK);      // white
         // plane 0 :
         if( *Buffer & Mask){                             // light gray
            GpioBusClr( GPU_DATA_OFFSET, GPU_COLOR_DARKGRAY);
         }
         // plane 1 :
         if( *(Buffer + (GB_WIDTH * GB_HEIGHT)) & Mask){  // dark gray
            GpioBusClr( GPU_DATA_OFFSET, GPU_COLOR_LIGHTGRAY);
         }                                                // both planes = black
         Buffer++;
         GpuWrSet();
      } while( --x);
   }
   GpuDone();
   GpuInitBuffer();
} // GpuFlush
#endif // G_PLANES

//-----------------------------------------------------------------------------
// Buffer initialization
//-----------------------------------------------------------------------------

void GpuInitBuffer( void)
// Buffer initialization
{
   GBuffer.MinX = G_WIDTH;
   GBuffer.MaxX = 0;
   GBuffer.MinY = G_HEIGHT;
   GBuffer.MaxY = 0;
} // GpuInitBuffer
#endif // GPU_DISABLE_BUFFER

//*****************************************************************************

//-----------------------------------------------------------------------------
// Write command
//-----------------------------------------------------------------------------

static void WriteCommand( byte Cmd)
// Write command
{
   GpuRsClr();
   GpuWrClr();
   SetData( Cmd);
   GpuWrSet();
   GpuRsSet();
} // WriteCommand

//-----------------------------------------------------------------------------
// Bus out
//-----------------------------------------------------------------------------

static void SetData( unsigned Value)
// Write data to bus D0..D7
{
   Value &= GPU_BUS_MASK;
   GpioBusClr( GPU_DATA_OFFSET, GPU_BUS_MASK);
   GpioBusSet( GPU_DATA_OFFSET, Value);
} // SetData

//-----------------------------------------------------------------------------
// Clear RAM
//-----------------------------------------------------------------------------

static void ClearRam( void)
// Clear all RAM
{
int r, c;

   SetArea( 0, GPU_MAX_ROW - 1, 0, GPU_MAX_RAM_COLUMN - 1);
   WriteCommand( GPU_CMD_RAMWR);
   for( r = 0; r < GPU_MAX_ROW; r++){
      for( c = 0; c < GPU_MAX_RAM_COLUMN; c++){
         WriteData( GPU_COLOR_WHITE);
         WriteData( GPU_COLOR_WHITE);
         WriteData( GPU_COLOR_WHITE);
      }
   }
} // ClearRam

//-----------------------------------------------------------------------------
// Set area
//-----------------------------------------------------------------------------

static void SetArea( int RowLo, int RowHi, int ColLo, int ColHi)
// Set read/write area
{
   WriteCommand( GPU_CMD_LASET);
   WriteData( RowLo);
   WriteData( RowHi);

   WriteCommand( GPU_CMD_CASET);
   WriteData( ColLo);
   WriteData( ColHi);
} // SetCursor

//-----------------------------------------------------------------------------
// Gray Scale
//-----------------------------------------------------------------------------

#if G_PLANES == 2
static void WritePalette( void);
// Write palette

static void SetGrayscale( void)
// Set gray scale palette
{
   WriteCommand( GPU_CMD_EXTOUT);
   WriteCommand( GPU_CMD_GRAY1SET);
   WritePalette();
   WriteCommand( GPU_CMD_GRAY2SET);
   WritePalette();
   WriteCommand( GPU_CMD_EXTIN);
} // SetGrayscale

static void WritePalette( void)
// Write palette
{
   WriteData( G_INTENSITY_WHITE);
   WriteData( G_INTENSITY_WHITE);
   WriteData( G_INTENSITY_WHITE);
   WriteData( G_INTENSITY_WHITE);

   WriteData( G_INTENSITY_LIGHTGRAY);
   WriteData( G_INTENSITY_LIGHTGRAY);
   WriteData( G_INTENSITY_LIGHTGRAY);
   WriteData( G_INTENSITY_LIGHTGRAY);

   WriteData( G_INTENSITY_DARKGRAY);
   WriteData( G_INTENSITY_DARKGRAY);
   WriteData( G_INTENSITY_DARKGRAY);
   WriteData( G_INTENSITY_DARKGRAY);

   WriteData( G_INTENSITY_BLACK);
   WriteData( G_INTENSITY_BLACK);
   WriteData( G_INTENSITY_BLACK);
   WriteData( G_INTENSITY_BLACK);
} // WritePalette
#endif
