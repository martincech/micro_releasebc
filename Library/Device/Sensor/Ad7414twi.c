//*****************************************************************************
//
//   Ad7414twi.c  AD7414 thermometer services
//   Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Ad7414.h"
#include "Iic/Twi.h"
#include "System/System.h"          // SysUDelay

// Adresy registru :

#define TEMP_VALUE_REGISTER  0	// registr teploty
#define TEMP_CFG_REGISTER    1	// konfiguracni registr
#define TEMP_THIGH_REGISTER  2	// horni alarm
#define TEMP_TLOW_REGISTER   3	// dolni alarm

// Configuration register :

#define TEMP_TEST_MODE0     0	// testovaci rezim LSB
#define TEMP_TEST_MODE1     1	// testovaci rezim MSB
#define TEMP_ONE_SHOT       2	// jednorazove spusteni
#define TEMP_ALERT_RESET    3	// zruseni alarmu
#define TEMP_ALERT_POLARITY 4	// polarita alarmu
#define TEMP_ALERT_DISABLE  5	// zakazani alarmu, 0=enable
#define TEMP_FLTR           6	// filtrace I2C signalu
#define TEMP_PD             7	// uspavaci rezim


#define TEMP_DFLT_CONFIG ((1<<TEMP_PD)|(1<<TEMP_FLTR)|(1<<TEMP_ALERT_DISABLE)|\
                          (1<<TEMP_ALERT_RESET)|(1<<TEMP_ONE_SHOT))

// Temperature value register (2nd read) :

#define TEMP_B1         7 	// teplota B1
#define TEMP_B0         6       // teplota B0
#define TEMP_ALERT_FLAG 5	// alarm aktivni
#define TEMP_THIGH_FLAG 4	// horni alarm aktivni
#define TEMP_TLOW_FLAG  3	// dolni alarm aktivni

#define TEMP_LVALUE_MASK ((1<<TEMP_B1) | (1<<TEMP_B0))


//------------------------------------------------------------------------------
// Start konverze
//------------------------------------------------------------------------------

void TempStartConversion( void)
// Vysle prikaz na provedeni konverze
{
byte Data[ 2];

   Data[ 0] = TEMP_CFG_REGISTER;
   Data[ 1] = TEMP_DFLT_CONFIG;
   TwiWrite( TEMP_ADDRESS, Data, sizeof( Data));
} // TempStartConversion

//------------------------------------------------------------------------------
// Cteni
//------------------------------------------------------------------------------

int16 TempRead( void)
// Vrati prectenou teplotu
{
int16 Value;
byte  Data[ 2];

   TwiWriteByte( TEMP_ADDRESS, TEMP_VALUE_REGISTER);
   TwiRead( TEMP_ADDRESS, Data, sizeof( Data));
   Value  = ((int16)Data[ 0] << 8) | Data[ 1];
   return( Value >> 6);
} // TempRead

#ifndef TEMP_AVERAGING
//------------------------------------------------------------------------------
// Mereni
//------------------------------------------------------------------------------

int16 TempMeasure( void)
// Vrati teplotu * 10 C
{
int16 RawValue;

   TempStartConversion();
   SysUDelay( TEMP_WAIT_CONVERSION);
   RawValue = TempRead();
   RawValue *= 10;
   return( RawValue >> 2);
} // TempMeasure

#else // TEMP_AVERAGING
//------------------------------------------------------------------------------
// Mereni
//------------------------------------------------------------------------------

int16 TempMeasure( void)
// Vrati teplotu * 10 C
{
int16 RawValue;
byte  i;

   i = TEMP_AVERAGE_COUNT;
   RawValue = 0;
   do {
      TempStartConversion();
      SysUDelay( TEMP_WAIT_CONVERSION);
      RawValue += TempRead();
   } while( --i);
   RawValue *= 10;
   return( RawValue >> (TEMP_AVERAGE_SHIFT + 2));
} // TempMeasure
#endif // TEMP_AVERAGING
