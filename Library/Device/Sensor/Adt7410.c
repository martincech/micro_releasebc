//*****************************************************************************
//
//   Adt7410.c    ADT7410 I2C thermometer services
//   Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Adt7410.h"
#include "Iic/Iic.h"
#include "System/System.h"  // Operating System

// I2C bus address (A0,1 = L) :
#define TEMP_ADDRESS_R  ((0x48 << 1) | 0x01)
#define TEMP_ADDRESS_W  ((0x48 << 1) | 0x00)

// Register address :
#define TEMP_REGISTER_TEMPERATURE_MSB   0x00
#define TEMP_REGISTER_TEMPERATURE_LSB   0x01
#define TEMP_REGISTER_STATUS            0x02
#define TEMP_REGISTER_CONFIGURATION     0x03
#define TEMP_REGISTER_THIGH_MSB         0x04
#define TEMP_REGISTER_THIGH_LSB         0x05
#define TEMP_REGISTER_TLOW_MSB          0x06
#define TEMP_REGISTER_TLOW_LSB          0x07
#define TEMP_REGISTER_TCRIT_MSB         0x08
#define TEMP_REGISTER_TCRIT_LSB         0x09
#define TEMP_REGISTER_THYST_LSB         0x0A
#define TEMP_REGISTER_ID                0x0B
#define TEMP_REGISTER_RESET             0x2F

// Status register :
// see header file

// Configuration register :
#define TEMP_CFG_FAULT_MASK      (2 << 0)
#define TEMP_CFG_CT_POLARITY     (1 << 2)
#define TEMP_CFG_INT_POLARITY    (1 << 3)

#define TEMP_CFG_COMPARATOR_MODE (1 << 4)
#define TEMP_CFG_INTERRUPT_MODE  (0 << 4)

#define TEMP_CFG_CONTINUOUS_MODE (0 << 5)
#define TEMP_CFG_ONE_SHOT_MODE   (1 << 5)
#define TEMP_CFG_SPS_MODE        (2 << 5)
#define TEMP_CFG_SHUTDOWN_MODE   (3 << 5)

#define TEMP_CFG_16BITS          (1 << 7)

// Used configurations :
#define TEMP_CONFIGURATION_SINGLE   (TEMP_CFG_ONE_SHOT_MODE)
#define TEMP_CONFIGURATION_PERIODIC (TEMP_CFG_SPS_MODE)
#define TEMP_CONFIGURATION_SHUTDOWN (TEMP_CFG_SHUTDOWN_MODE)

//------------------------------------------------------------------------------
//  Identification
//------------------------------------------------------------------------------

byte TempIdentification( void)
// Returns chip ID
{
byte Value;
   
   IicStart();
   IicSend( TEMP_ADDRESS_W);
   IicSend( TEMP_REGISTER_ID);
   IicStart();
   IicSend( TEMP_ADDRESS_R);
   Value  = IicReceive( NO);
   IicStop();
   return( Value);	
} // TempIdentification	

//------------------------------------------------------------------------------
//  Status
//------------------------------------------------------------------------------

byte TempStatus( void)
// Returns status
{
byte Value;
   
   IicStart();
   IicSend( TEMP_ADDRESS_W);
   IicSend( TEMP_REGISTER_STATUS);
   IicStart();
   IicSend( TEMP_ADDRESS_R);
   Value  = IicReceive( NO);
   IicStop();
   return( Value);		
} // TempStatus

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void TempStartConversion( void)
// Start conversion
{  
   IicStart();
   IicSend( TEMP_ADDRESS_W);
   IicSend( TEMP_REGISTER_CONFIGURATION);
   IicSend( TEMP_CONFIGURATION_SINGLE);
   IicStop();
} // TempStartConversion

//------------------------------------------------------------------------------
//  Start periodic
//------------------------------------------------------------------------------

void TempStartPeriodic( void)
// Start periodic conversion
{
   IicStart();
   IicSend( TEMP_ADDRESS_W);
   IicSend( TEMP_REGISTER_CONFIGURATION);
   IicSend( TEMP_CONFIGURATION_PERIODIC);
   IicStop();	 
} // TempStartPeriodic

//------------------------------------------------------------------------------
//  Shutdown
//------------------------------------------------------------------------------

void TempShutdown( void)
// Stop conversions
{
   IicStart();
   IicSend( TEMP_ADDRESS_W);
   IicSend( TEMP_REGISTER_CONFIGURATION);
   IicSend( TEMP_CONFIGURATION_SHUTDOWN);
   IicStop();	 	
} // TempShutdown

//------------------------------------------------------------------------------
//  Read
//------------------------------------------------------------------------------

int16 TempRead( void)
// Read raw temperature
{
int16 Value;

   IicStart();
   IicSend( TEMP_ADDRESS_W);
   IicSend( TEMP_REGISTER_TEMPERATURE_MSB);
   IicStart();
   IicSend( TEMP_ADDRESS_R);
   Value  = (int16)IicReceive( YES) << 8;
   Value |= IicReceive( NO);
   IicStop();
   return( Value >> 3);
} // TempRead

//------------------------------------------------------------------------------
// Convert
//------------------------------------------------------------------------------

int16 TempConvert( int16 TempRaw)
// Convert <TempRaw> to [0.1C]
{
   TempRaw *= 10;
   // round :
   if( TempRaw >= 0){
      TempRaw += (1 << 3);
   } else {
      TempRaw -= (1 << 3);
   }
   return( TempRaw >> 4);	
} // TempConvert

//------------------------------------------------------------------------------
// Convert raw
//------------------------------------------------------------------------------

int16 TempConvertRaw( int16 Temp)
// Convert <Temp> [0.1C] to raw temperature
{
	Temp <<= 4;                           // convert to LSB
	Temp  /= 10;                          // move [0.1C] to 1C
	return( Temp);
} // TempConvertRaw

//------------------------------------------------------------------------------
// Measure
//------------------------------------------------------------------------------

int16 TempMeasure( void)
// Measure temperature * 10 C
{
   TempStartConversion();
   SysDelay( TEMP_WAIT_CONVERSION);
   return( TempConvert( TempRead()));
} // TempMeasure
