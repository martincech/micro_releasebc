//*****************************************************************************
//
//   Max17047.c       Max17047 fuel gauge
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#include "Max17047.h"
#include "max17047Iic.h"
#include "Cpu/Cpu.h"
#include "Eeprom/Eep.h"
#include "System/System.h"

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void Max17047Init( void)
// Initialisation
{
   max17047IicInit();
} // Max17047Init

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

TYesNo Max17047ReadMultiple( word *Data, byte Address, byte Count)
// Read <Data> from <Address> of size <Count>, returns data read
{
byte Index = 0;
byte *DataB = (byte *) Data;
   max17047IicStart();

   if(!max17047IicSend( IicStartByte(MAX17047_IIC_ADDRESS, IIC_WRITE))) {
      max17047IicStop();
      return NO;
   }

   if(!max17047IicSend( Address)) {
      max17047IicStop();
      return NO;
   }

   max17047IicStart();
   if(!max17047IicSend( IicStartByte(MAX17047_IIC_ADDRESS, IIC_READ))) {
      max17047IicStop();
      return NO;
   }
   
   while(Count--) {
      *DataB++ = max17047IicReceive( YES);
      if(Count) {
         *DataB++ = max17047IicReceive( YES);
      } else {
         *DataB++ = max17047IicReceive( NO);
      }
      Index++;
   }
   

   max17047IicStop();
   
   return YES;
} // Max17047Read

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

TYesNo Max17047WriteMultiple( word *Data, byte Address, byte Count)
// Write <Data> from <Address> of size <Count>, returns data written
{
byte Index = 0;
byte *DataB = (byte *) Data;
   max17047IicStart();

   if(!max17047IicSend( IicStartByte(MAX17047_IIC_ADDRESS, IIC_WRITE))) {
      max17047IicStop();
      return NO;
   }

   if(!max17047IicSend( Address)) {
      max17047IicStop();
      return NO;
   }
   
   while(Count--) {
      if(!max17047IicSend( *DataB++)) {
         max17047IicStop();
         return NO;
      }
      if(!max17047IicSend( *DataB++)) {
         max17047IicStop();
         return NO;
      }
      Index++;
   }

   max17047IicStop();
   
   return YES;
} // Max17047Write

//-----------------------------------------------------------------------------
// Read word
//-----------------------------------------------------------------------------

TYesNo Max17047Read( byte Address, word *Data)
// Read register
{
   max17047IicStart();

   if(!max17047IicSend( IicStartByte(MAX17047_IIC_ADDRESS, IIC_WRITE))) {
      max17047IicStop();
      return NO;
   }

   if(!max17047IicSend( Address)) {
      max17047IicStop();
      return NO;
   }

   max17047IicStart();
   if(!max17047IicSend( IicStartByte(MAX17047_IIC_ADDRESS, IIC_READ))) {
      max17047IicStop();
      return NO;
   }
   
   *Data = max17047IicReceive( YES);
   *Data |= max17047IicReceive( NO) << 8;

   max17047IicStop();
   
   return YES;
} // Max17047Read

//-----------------------------------------------------------------------------
// Write word
//-----------------------------------------------------------------------------

TYesNo Max17047Write( byte Address, word Data)
// Write register
{
   max17047IicStart();

   if(!max17047IicSend( IicStartByte(MAX17047_IIC_ADDRESS, IIC_WRITE))) {
      max17047IicStop();
      return NO;
   }

   if(!max17047IicSend( Address)) {
      max17047IicStop();
      return NO;
   }

   if(!max17047IicSend( Data)) {
      max17047IicStop();
      return NO;
   }
   
   if(!max17047IicSend( Data >> 8)) {
      max17047IicStop();
      return NO;
   }
   max17047IicStop();
   
   return YES;
} // Max17047Write