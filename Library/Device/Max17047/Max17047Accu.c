//*****************************************************************************
//
//   Max17047Accu.c    Max17047 fuel gauge
//   Version 1.0       (c) Veit Electronics
//
//*****************************************************************************

#include "Accu/Accu.h"
#include "Max17047.h"
#include "Cpu/Cpu.h"
#include "Eeprom/Eep.h"
#include "System/System.h"

#define MAX17047_CURRENT_RESOLUTION_PER_MOHM      15625 // 100 nA
#define MAX17047_VOLTAGE_RESOLUTION               625 // uV
#define MAX17047_CURRENT_RESOLUTION               MAX17047_CURRENT_RESOLUTION_PER_MOHM / MAX17047_CURRENT_SHUNT

#define MAX17047_CURRENT_SHUNT     20 // mOhms

typedef struct {
   struct {
      word FullCap;
      word Cycles;
      word RCOMP0;
      word TempCo;
      word QResidual00;
      word QResidual10;
      word QResidual20;
      word QResidual30;
      word dQacc;
      word dPacc;
   } Data;
   word Crc;
} TMax17047Backup;

#define BACKUP_EEPROM_ADDRESS     0

word CrcCompute(void *Data, int Length) {
   word Crc = 0;
   byte *DataB = Data;
   while(Length--) {
      Crc += *DataB++;
   }
   return -Crc;
}

void Max17047Backup( void) {
TMax17047Backup Backup;
   if(!Max17047Read(Max17047AddressOf( FullCAP), &Backup.Data.FullCap)) {
      return;
   }
   if(!Max17047Read(Max17047AddressOf( Cycles), &Backup.Data.Cycles)) {
      return;
   }
   if(!Max17047Read(Max17047AddressOf( RCOMP0), &Backup.Data.RCOMP0)) {
      return;
   }
   if(!Max17047Read(Max17047AddressOf( TempCo), &Backup.Data.TempCo)) {
      return;
   }
   if(!Max17047Read(Max17047AddressOf( QResidual00), &Backup.Data.QResidual00)) {
      return;
   }
   if(!Max17047Read(Max17047AddressOf( QResidual10), &Backup.Data.QResidual10)) {
      return;
   }
   if(!Max17047Read(Max17047AddressOf( QResidual20), &Backup.Data.QResidual20)) {
      return;
   }
   if(!Max17047Read(Max17047AddressOf( QResidual30), &Backup.Data.QResidual30)) {
      return;
   }
   if(!Max17047Read(Max17047AddressOf( dQacc), &Backup.Data.dQacc)) {
      return;
   }
   if(!Max17047Read(Max17047AddressOf( dPacc), &Backup.Data.dPacc)) {
      return;
   }
   Backup.Crc = CrcCompute(&Backup.Data, sizeof(Backup.Data));
   EepDataLoad(BACKUP_EEPROM_ADDRESS, &Backup, sizeof( TMax17047Backup));
}

void Max17047Restore( void) {
TMax17047Backup Backup;
   //EepDataLoad(BACKUP_EEPROM_ADDRESS, &Backup, sizeof( TMax17047Backup));
   if(CrcCompute(&Backup.Data, sizeof(Backup.Data)) != Backup.Crc) {
      return;
   }
   // Restore
   Max17047Write(Max17047AddressOf( FullCAP), Backup.Data.FullCap);
   Max17047Write(Max17047AddressOf( Cycles), Backup.Data.Cycles);
   Max17047Write(Max17047AddressOf( RCOMP0), Backup.Data.RCOMP0);
   Max17047Write(Max17047AddressOf( TempCo), Backup.Data.TempCo);
   Max17047Write(Max17047AddressOf( QResidual00), Backup.Data.QResidual00);
   Max17047Write(Max17047AddressOf( QResidual10), Backup.Data.QResidual10);
   Max17047Write(Max17047AddressOf( QResidual20), Backup.Data.QResidual20);
   Max17047Write(Max17047AddressOf( QResidual30), Backup.Data.QResidual30);
   Max17047Write(Max17047AddressOf( dQacc), Backup.Data.dQacc);
   Max17047Write(Max17047AddressOf( dPacc), Backup.Data.dPacc);
}
//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

#define POR_DELAY    600

void AccuInit( void)
// Initialization
{
   word Value;
   TMax17047Config *Config;
   TMax17047Status *Status;
   
   Max17047Init();
   
   Status = (TMax17047Status *) &Value;
   
   Max17047Read(Max17047AddressOf( Status), &Value);
   
   if(!Status->POR) {
      // No POR, return
      return;
   }
   // Delay
   SysDelay(POR_DELAY);

   // clear POR
   Status->POR = NO;
   Max17047Write(Max17047AddressOf( Status), Value);


   Config = (TMax17047Config *) &Value;
   Config->Ber = NO;
   Config->Bei = NO;
   Config->Aen = NO;
   Config->FTHRM = NO;
   Config->ETHRM = YES;
   Config->ALSH = NO;
   Config->I2CSH = NO;
   Config->SHDN = NO;
   Config->Tex = NO;
   Config->Ten = YES;
   
   Max17047Write(Max17047AddressOf( CONFIG), Value);
   
   //Max17047Restore();
} // AccuInit

#define BACKUP_PERIOD   1 // min

#define BACKUP_TICKS      ((BACKUP_PERIOD * 60 * 1000ull) / ACCU_BACKUP_PERIOD)
static int Ticks = 0;

void AccuExecute( void) {
   Ticks++;
   if(Ticks < BACKUP_TICKS) {
      return;
   }
   Ticks = 0;
   
   //Max17047Backup();
}

//-----------------------------------------------------------------------------
// Ready
//-----------------------------------------------------------------------------

TYesNo AccuReady( void)
// Ready
{
word Value;
   if(Max17047Read(Max17047AddressOf( Status), &Value)) {
      return YES;
   }
   return NO;
} // AccuReady

//-----------------------------------------------------------------------------
// Capacity
//-----------------------------------------------------------------------------

byte AccuCapacityRemaining( void)
// Remaining capacity %
{
   word Voltage = AccuVoltage();
   if(Voltage > 3900) {
      return 100;
   } else if(Voltage > 3800) {
      return 80;
   } else if(Voltage > 3650) {
      return 60;
   } else if(Voltage > 3600) {
      return 40;
   } else if(Voltage > 3550) {
      return 20;
   } else {
      return 0;
   }

   int16 CapNative;
   int8 Cap;
   if(!Max17047Read( Max17047AddressOf( SOCREP), &CapNative)) {
      return ACCU_CAPACITY_INVALID;
   }
   Cap = CapNative >> 8;
   return Cap;
} // AccuCapacityRemaining

//-----------------------------------------------------------------------------
// Voltage
//-----------------------------------------------------------------------------

word AccuVoltage( void)
// Voltage mV
{
   word VoltageNative;
   word Voltage;
   if(!Max17047Read( Max17047AddressOf( AverageVCELL), &VoltageNative)) {
      return 0;
   }
   Voltage = ((dword) (VoltageNative >> 3) * MAX17047_VOLTAGE_RESOLUTION) / 1000;
   return Voltage;
} // AccuVoltage

//-----------------------------------------------------------------------------
// Current
//-----------------------------------------------------------------------------

int16 AccuCurrent( void)
// Current mA
{
   int16 CurrentNative;
   int16 Current;
   if(!Max17047Read( Max17047AddressOf( Current), &CurrentNative)) {
      return 0;
   }
   Current = ((int32)CurrentNative * MAX17047_CURRENT_RESOLUTION) / 10000;
   return Current;
} // AccuCurrent

//-----------------------------------------------------------------------------
// Temperature
//-----------------------------------------------------------------------------

int8 AccuTemperature( void)
// Temperature �C
{
   int16 TemperatureNative;
   int8 Temperature;
   if(!Max17047Read( Max17047AddressOf( Temperature), &TemperatureNative)) {
      return 0;
   }
   Temperature = TemperatureNative >> 8;
   return Temperature;
} // AccuTemperature