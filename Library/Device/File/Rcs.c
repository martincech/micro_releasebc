//*****************************************************************************
//
//    Rcs.c        Remote control server
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Rcs.h"
#include "Spi/SpiCrc.h"
#include "EfsRpc.h" 
#include "File/Efs.h"
#include "Hardware.h"

#include "usb_task.h"               // asf
#include "host_mass_storage_task.h" // asf

#define BUFFER_SIZE sizeof(TEfsCommand)

static byte _Buffer[ BUFFER_SIZE];
static byte _Context;
static TEfsCommand *Command = (TEfsCommand *) _Buffer; 
static TEfsReply   *Reply   = (TEfsReply   *) _Buffer; 

typedef enum {
   RCS_START,
   RCS_RECEIVE,
   RCS_SEND,
} ERcsContext;

// Local functions :

static TYesNo CommandExecute( TEfsCommand *Command, word Size);
// Execute received command

static TYesNo CmdFileWrite(word Size);
// Execute file write commnand

static TYesNo CmdFileRead(word Size);
// Execute file read command

static TYesNo CmdDirectoryRename(const char *Path);
// Execute directory rename command

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void RcsInit( void)
// Initialize
{
   SpiCrcInit();
   EfsInit();
   _Context = RCS_START;
} // RcsInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void RcsExecute( void)
// Server executive
{
byte Status;

   switch( _Context) {
      //-----------------------------------------------------------------------
      case RCS_START :
         SpiCrcReceive( Command, BUFFER_SIZE);
         _Context = RCS_RECEIVE;
         break;

      //-----------------------------------------------------------------------
      case RCS_RECEIVE :
         switch( SpiCrcStatus()){
            case SPI_RECEIVE_ACTIVE :
               // receive in progress               
               break;                  

            case SPI_RECEIVE_DONE :
               // frame received, send reply :
               if( !CommandExecute( Command, SpiCrcReceiveSize())){
                  Reply->Reply = Command->Command;
                  SpiCrcSend( Reply, 1);
               }
               _Context = RCS_SEND;
               break;

            default :
			      SpiCrcReceive( Command, BUFFER_SIZE);     // no reply, receive again
               _Context = RCS_RECEIVE;
               break;
         }
         break;

      //-----------------------------------------------------------------------
      case RCS_SEND :
         if( SpiCrcStatus() == SPI_SEND_ACTIVE){
            break;
         }
         SpiCrcReceive( Command, BUFFER_SIZE);
         _Context = RCS_RECEIVE;
         break;

      //-----------------------------------------------------------------------
      default :
         _Context = RCS_START;
         break;
   }
   
   usb_task();
   host_mass_storage_task();
} // RcsExecute

//******************************************************************************

//-----------------------------------------------------------------------------
// Command
//-----------------------------------------------------------------------------

static TYesNo CommandExecute( TEfsCommand *Command, word Size)
// Execute received command
{
   word ReplySize;

   ReplySize = EfsSimpleReplySize();        // simple reply

   switch( Command->Command) {
      case EFS_CMD_INIT :
         if( Size != EfsSimpleCommandSize()) {
            return( NO);
         }
         EfsInit();
         break;

      case EFS_CMD_DIRECTORY_CREATE :
         if( Size != EfsPathCommandSize(Command->Data.DirectoryCreate)) {
            return( NO);
         }
         if(!EfsDirectoryCreate( Command->Data.DirectoryCreate.Path)) {
            return NO;
         }
         break;
		 
      case EFS_CMD_DIRECTORY_CHANGE:
         if( Size != EfsPathCommandSize(Command->Data.DirectoryChange)) {
            return( NO);
         }
         if(!EfsDirectoryChange( Command->Data.DirectoryChange.Path)) {
            return NO;
         }
	      break;
		 
      case EFS_CMD_DIRECTORY_DELETE:
         if( Size != EfsPathCommandSize(Command->Data.DirectoryChange)) {
            return( NO);
         }
         if(!EfsDirectoryDelete( Command->Data.DirectoryChange.Path)) {
            return NO;
         }
	      break;

      case EFS_CMD_DIRECTORY_RENAME:
         if( Size != EfsPathCommandSize(Command->Data.DirectoryRename)) {
            return( NO);
         }

         if(!CmdDirectoryRename(Command->Data.DirectoryRename.Path)) {
            return NO;
         }

         return YES;

      case EFS_CMD_FILE_CREATE:
         if( Size != EfsNameCommandSize(Command->Data.FileCreate)) {
            return( NO);
         }
         if(!EfsFileCreate( Command->Data.FileCreate.Name, Command->Data.FileCreate.Size)) {
            return NO;
         }
	      break;

      case EFS_CMD_FILE_DELETE:
         if( Size != EfsNameCommandSize(Command->Data.FileDelete)) {
            return( NO);
         }
         if(!EfsFileDelete( Command->Data.FileDelete.Name)) {
            return NO;
         }
         break;

      case EFS_CMD_FILE_OPEN:
         if( Size != EfsNameCommandSize(Command->Data.FileOpen)) {
            return( NO);
         }
         if(!EfsFileOpen( Command->Data.FileOpen.Name)) {
            return NO;
         }
         break;

      case EFS_CMD_FILE_CLOSE:
         if( Size != EfsSimpleCommandSize()) {
            return( NO);
         }
         EfsFileClose();
         break;

      case EFS_CMD_FILE_WRITE:
         if( Size != EfsCommandSize( FileWrite)) {
            return( NO);
         }
         if(!CmdFileWrite( Command->Data.FileWrite.Count)) {
            return NO;
         }
         return YES;

      case EFS_CMD_FILE_READ:
         if( Size != EfsCommandSize( FileRead)) {
            return( NO);
         }
         if(!CmdFileRead( Command->Data.FileRead.Count)) {
            return NO;
         }
         return YES;

      case EFS_CMD_FILE_SEEK:
         if( Size != EfsCommandSize( FileSeek)) {
            return( NO);
         }
         if(!EfsFileSeek( Command->Data.FileSeek.Pos, Command->Data.FileSeek.Whence)) {
            return NO;
         }
	      break;

      case EFS_CMD_CLOCK_SET:
         if( Size != EfsCommandSize(ClockSet)) {
            return( NO);
         }
         if(!EfsClockSet( Command->Data.ClockSet.Clock)) {
            return NO;
         }
         break;

      case EFS_CMD_UNMOUNT:
         if( Size != EfsSimpleCommandSize()) {
            return( NO);
         }
         
         EfsUnmount();
         break;

      case EFS_CMD_FILE_EXISTS:
         if( Size != EfsNameCommandSize(Command->Data.FileExists)) {
            return( NO);
         }
         Reply->Data.FileExists.Exists = EfsFileExists(Command->Data.FileExists.Name);
         ReplySize = EfsExistsReplySize();
         break;

      case EFS_CMD_DIRECTORY_EXISTS:
         if( Size != EfsPathCommandSize(Command->Data.DirectoryExists)) {
            return( NO);
         }
         Reply->Data.DirectoryExists.Exists = EfsDirectoryExists(Command->Data.DirectoryExists.Path);
         ReplySize = EfsExistsReplySize();
         break;

      default :
         return( NO);
   }

   Reply->Reply = EfsReply( Command->Command);
   SpiCrcSend( Reply, ReplySize);
   return( YES);
} // CommandExecute

//-----------------------------------------------------------------------------
// File write
//-----------------------------------------------------------------------------

static TYesNo CmdFileWrite(word Count)
// Execute file write commnand
{
   word PieceNumber = 0;
   word CountRemaining = Count;
   word CountToBeReceived;

   while(CountRemaining) {
      CountToBeReceived = CountRemaining > EFS_DATA_CHUNK_SIZE ? EFS_DATA_CHUNK_SIZE : CountRemaining;

      if(!SpiCrcPacketReceive(_Buffer, BUFFER_SIZE)) {
         return NO;
      }
	   if(SpiCrcReceiveSize() != CountToBeReceived) {
	      return NO;
	   }
	   if(EfsFileWrite(_Buffer, CountToBeReceived) != CountToBeReceived) {
         return NO;
      }

      Reply->Reply = EfsReply( EFS_CMD_FILE_WRITE);
      Reply->Data.FileWriteConfirmation.PieceNumber = PieceNumber;
      Reply->Data.FileWriteConfirmation.Repeat = NO;

      if(!SpiCrcPacketSend(Reply, EfsReplySize(FileWriteConfirmation))) {
         return NO;
      }

      CountRemaining -= CountToBeReceived;
      PieceNumber++;

      usb_task();
      host_mass_storage_task();
   }
   
   return YES;
} // CmdFileWrite

//-----------------------------------------------------------------------------
// File read
//-----------------------------------------------------------------------------

static TYesNo CmdFileRead(word Count)
// Execute file read commnand
{
   word PieceNumber = 0;
   word CountRemaining = Count;
   word CountToBeSent;

   while(CountRemaining) {
      CountToBeSent = CountRemaining > EFS_DATA_CHUNK_SIZE ? EFS_DATA_CHUNK_SIZE : CountRemaining;
      CountToBeSent = EfsFileRead( _Buffer, CountToBeSent);

      if(!SpiCrcPacketSend( _Buffer, CountToBeSent)) {
         return NO;
      }
      if(!SpiCrcPacketReceive( Command, sizeof( *Command))) {
         return NO;
      }
      if(SpiCrcReceiveSize() != EfsCommandSize( FileReadConfirmation)) {
         return NO;
      }
      if(Command->Command != EFS_CMD_FILE_READ) {
         return NO;
      }
      if(Command->Data.FileReadConfirmation.PieceNumber != PieceNumber) {
         return NO;
      }

      CountRemaining -= CountToBeSent;
      PieceNumber++;

      if(CountToBeSent != EFS_DATA_CHUNK_SIZE) {
         break;
      }

      usb_task();
      host_mass_storage_task();
   }

   return YES;
} // CmdFileRead

//-----------------------------------------------------------------------------
// Directory rename
//-----------------------------------------------------------------------------

static TYesNo CmdDirectoryRename(const char *_Path)
// Execute directory rename command
{
   char Path[EFS_PATH_MAX + 1];
   strcpy(Path, _Path);

   Reply->Reply = EfsReply( EFS_CMD_DIRECTORY_RENAME);

   if(!SpiCrcPacketSend(Reply, EfsSimpleReplySize())) {
      return NO;
   }
   if(!SpiCrcPacketReceive( Command, sizeof( *Command))) {
      return NO;
   }
   if(SpiCrcReceiveSize() != EfsPathCommandSize( Command->Data.DirectoryRename)) {
      return NO;
   }
   if(Command->Command != EFS_CMD_DIRECTORY_RENAME) {
      return NO;
   }
   if(!EfsDirectoryRename(Path, Command->Data.DirectoryRename.NewName)) {
      return NO;
   }

   Reply->Reply = EfsReply( EFS_CMD_DIRECTORY_RENAME);

   if(!SpiCrcPacketSend(Reply, EfsSimpleReplySize())) {
      return NO;
   }

   return YES;
}