//******************************************************************************
//
//   UartModem.h  Serial port modem support
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __UartModem_H__
   #define __UartModem_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

// Rem : receive function has parameter Size - expected reply size
//       because of Win32 API UART implementation
// Rem : send function has additiona parameters Size. Standard
//       strings are terminated with '\0' character. The Size
//       may be 0. GSM alphabet used by SMS interprets character
//       code 0 as '@' There must be expicitly entered Size in this case

//------------------------------------------------------------------------------
//  Data types
//------------------------------------------------------------------------------

typedef enum {
   UART_MODEM_IDLE,
   UART_MODEM_SEND_ACTIVE,
   UART_MODEM_SEND_DONE,
   UART_MODEM_RECEIVE_ACTIVE,
   UART_MODEM_RECEIVE_DONE,
   UART_MODEM_RECEIVE_ERROR,
   _UART_MODEM_LAST
} EUartModemStatus;

typedef byte TUartModemStatus;

//------------------------------------------------------------------------------
//  Setup
//------------------------------------------------------------------------------
#ifdef __cplusplus
   #include "Uart/WinUart.h"
   #include "Crt/CrtDump.h"

   void UartModemSetup( WinUart *Uart, CrtDump *Dump);
   // Initialize with <Uart> and optional <Dump>
#endif

#ifdef __cplusplus
   extern "C" {
#endif
//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void UartModemInit( void);
// Initialize

void UartModemDeinit( void);
// Deinitialiize

void UartModemBaudRateSet( int BaudRate);
// Set modem <BaudRate>

void UartModemTimeoutSet( int Timeout);
// Set receive <Timeout>

void UartModemBufferSet( void *Buffer, int Size);
// Set Rx <Buffer> with <Size>

void UartModemSend( char *String, int Size);
// Send <String> with optional <Size>

TYesNo UartModemReceive( int Size);
// Receive buffer with <Size> (may be zero - receive up to timeout)

TUartModemStatus UartModemStatus( void);
// Check for last operation status

//------------------------------------------------------------------------------
#ifdef __cplusplus
   }
#endif

#endif
