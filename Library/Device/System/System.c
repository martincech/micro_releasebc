//******************************************************************************
//
//    System.c     Operating system
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Hardware.h"
#include "System/System.h"
#include "Sleep/Sleep.h"
#ifdef SYSTEM_KEYBOARD
   #include "Kbd/Kbd.h"
#endif

#include "Rtc/Rtc.h"
#include "Time/uClock.h"

//------------------------------------------------------------------------------

#include "System/SystemTimer.c"           // system timer services

//------------------------------------------------------------------------------
// Timer counters :
//------------------------------------------------------------------------------

// timer counters :
#define CounterFastSet()         _CounterFast     =  TIMER_FAST_PERIOD / TIMER_PERIOD
#define CounterSlowSet()         _CounterSlow     =  TIMER_SLOW_PERIOD / TIMER_PERIOD
#define CounterFlashSet()        _CounterFlash    = (TIMER_FLASH1 + TIMER_FLASH2) / TIMER_PERIOD
#define CounterTimeoutSet()      _CounterTimeout  = TimerSlowCount(TIMER_TIMEOUT)
#define CounterAlarmSet( ms)     _CounterAlarm    =  (ms) / TIMER_PERIOD

// timer flags :
#define TIMER_FLAG_FAST     0x01
#define TIMER_FLAG_SLOW     0x02
#define TIMER_FLAG_FLASH1   0x04
#define TIMER_FLAG_FLASH2   0x08
#define TIMER_FLAG_ALARM    0x10


#define FlagDefault()      _TimerFlags  = 0;

#define FlagFastSet()      _TimerFlags |=  TIMER_FLAG_FAST
#define FlagFastClear()    _TimerFlags &= ~TIMER_FLAG_FAST
#define FlagFast()        (_TimerFlags &   TIMER_FLAG_FAST)

#define FlagSlowSet()      _TimerFlags |=  TIMER_FLAG_SLOW
#define FlagSlowClear()    _TimerFlags &= ~TIMER_FLAG_SLOW
#define FlagSlow()        (_TimerFlags &   TIMER_FLAG_SLOW)

#define FlagFlash1Set()    _TimerFlags |=  TIMER_FLAG_FLASH1
#define FlagFlash1Clear()  _TimerFlags &= ~TIMER_FLAG_FLASH1
#define FlagFlash1()      (_TimerFlags &   TIMER_FLAG_FLASH1)

#define FlagFlash2Set()    _TimerFlags |=  TIMER_FLAG_FLASH2
#define FlagFlash2Clear()  _TimerFlags &= ~TIMER_FLAG_FLASH2
#define FlagFlash2()      (_TimerFlags &   TIMER_FLAG_FLASH2)

#define FlagAlarmSet()     _TimerFlags |=  TIMER_FLAG_ALARM
#define FlagAlarmClear()   _TimerFlags &= ~TIMER_FLAG_ALARM
#define FlagAlarm()       (_TimerFlags &   TIMER_FLAG_ALARM)

//------------------------------------------------------------------------------
// Local variables :
//------------------------------------------------------------------------------

static volatile dword      _TimerTick;            // milisecond timer ticks
static volatile byte      _CounterFast;          // fast countdown
static volatile word      _CounterSlow;          // slow countdown
static volatile word      _CounterFlash;         // flash countdown
static volatile word      _CounterAlarm;         // alarm timer

static volatile word      _CounterTimeout;       // inactivity countdown

static volatile byte      _TimerFlags;           // timer flags
static int                _AlarmEvent;           // alarm event code

//------------------------------------------------------------------------------
// Local functions :
//------------------------------------------------------------------------------

static byte TickFast( void);
// Returns YES if fast tick occured

static byte TickSlow( void);
// Returns YES if slow tick occured

static byte TickFlash1( void);
// Returns YES if flash 1 tick occured

static byte TickFlash2( void);
// Returns YES if flash 2 tick occured

static byte TickAlarm( void);
// Returns YES if alarm tick occured

//------------------------------------------------------------------------------
//   Initialization
//------------------------------------------------------------------------------

void SysInit( void)
// Initialization
{
   // set counters :
   CounterFastSet();                   // set fast countdown
   CounterSlowSet();                   // set slow countdown
   CounterFlashSet();                  // set flash countdown
   CounterTimeoutSet();                // set keyboard timeout
   CounterAlarmSet( 0);                // clear alarm

   _AlarmEvent = K_IDLE;               // no alarm event
   FlagDefault();                      // clear flags
#ifdef SLEEP_ENABLE
   SleepInit();
#endif
   // run system timer :
   timerInit();                        // initialize timer
   InterruptEnable();                  // enable interrupts
} // SysInit

//------------------------------------------------------------------------------
//  Scheduler
//------------------------------------------------------------------------------

int SysSchedulerDefault( void)
// Default scheduler cycle, returns any event
{
int Key = K_IDLE;

   forever {
      if( TickFast()){
#ifdef SYSTEM_KEYBOARD
         KbdTimer();                      // keyboard timer tick
         Key = KbdGet();                  // keyboard read
         if( Key != K_IDLE){
            FlagFastSet();                // don't clear FlagFast
            return( Key);
         }
#endif
         return( K_TIMER_FAST);
      }
      if( TickSlow()){
         // check for timeout :
         if( _CounterTimeout){
            if( --_CounterTimeout == 0){
               CounterTimeoutSet();
               FlagSlowSet();             // don't clear FlagSlow
               return( K_TIMEOUT);
            }
         }
         return( K_TIMER_SLOW);
      }
      if( TickFlash1()){
         return( K_FLASH1);
      }
      if( TickFlash2()){
         return( K_FLASH2);
      }
      if( TickAlarm()){
         return( _AlarmEvent);
      }

#ifdef SLEEP_ENABLE
      Key = SleepScheduler(); 
      if(Key == K_TIMER_SLOW) { // emulate other timers
         FlagSlowSet();
         FlagFastSet();
         FlagFlash1Set();
         FlagFlash2Set();
         continue;
      }
#endif
      return( Key);
   }
} // SysSchedulerDefault

//------------------------------------------------------------------------------
//  Event wait
//------------------------------------------------------------------------------

int SysEventWait( void)
// Scheduler loop, returns nonempty event
{
int Key;

   forever {
      Key = SysScheduler();            // scheduler step
      if( Key != K_IDLE) {
          return( Key);                // nonempty event
      }
   }
} // SysEventWait

//------------------------------------------------------------------------------
//   Timeout
//------------------------------------------------------------------------------

void SysTimeoutDisable( void)
// Disable timeout
{
   timerDisable();
   _CounterTimeout = 0;
   timerEnable();
} // SysTimeoutDisable

void SysTimeoutEnable( void)
// Enable timeout
{
   timerDisable();
   CounterTimeoutSet();
   timerEnable();
} // SysTimeoutEnable

void SysTimeoutReset( void)
// Reset timeout
{
   timerDisable();
   CounterTimeoutSet();
   timerEnable();
} // SysTimeoutReset

//------------------------------------------------------------------------------
//  Flash
//------------------------------------------------------------------------------

void SysFlashReset( void)
// Restart flashing
{
   timerDisable();
   CounterFlashSet();
   FlagFlash1Clear();
   FlagFlash2Clear();
   timerEnable();
} // SysFlashReset

//-----------------------------------------------------------------------------
// Alarm processing
//-----------------------------------------------------------------------------

void SysAlarm( word ms, int Event)
// Send alarm <Event> after <ms>
{
   _AlarmEvent = Event;                // remember event
   timerDisable();
   CounterAlarmSet( ms);               // set alarm countdown
   FlagAlarmClear();                   // clear flag
   timerEnable();
} // SysAlarm

void SysAlarmDisable( void)
// Deactivate alarm
{
   timerDisable();
   CounterAlarmSet( 0);                // don't interrupt
   FlagAlarmClear();                   // clear flag
   timerEnable();
} // SysResetAlarm

#include "System/Delay.c"              // delay loops

//-----------------------------------------------------------------------------
// System timer
//-----------------------------------------------------------------------------

word SysTimer( void)
// Returns milisecond timer
{
word Timer;

   timerDisable();
   Timer = _TimerTick;
   timerEnable();
   return( Timer);
} // SysTimer

//-----------------------------------------------------------------------------
// System time timer
//-----------------------------------------------------------------------------

dword SysTime( void)
// return second timer
{
dword Timer;

   timerDisable();
   Timer = _TimerTick;
   timerEnable();
   return( Timer/(TIMER_SEC_PERIOD/TIMER_PERIOD));
}

word SysTimerNaked( void)
// Get milisecond timer from interrupt
{
   return( _TimerTick);
} // SysTimerNaked

//-----------------------------------------------------------------------------
// System clock
//-----------------------------------------------------------------------------

dword SysClock( void)
// Returns actual date/time
{
dword Clock;

   timerDisable();
   Clock = SysClockNaked();
   timerEnable();
   return( Clock);
} // SysClock

dword SysClockNaked( void)
// Get actual date/time from interrupt
{
   return( RtcLoad());
} // SysClockNaked

void SysClockSet( dword Clock)
// Set actual date/time
{
   timerDisable();
   RtcSave(Clock);
   timerEnable();
} // SysClockSet

//------------------------------------------------------------------------------
//  Date time
//------------------------------------------------------------------------------

dword SysDateTime( void)
// Returns wall clock date/time
{
UClockGauge Clock;

   Clock = SysClock();
   return( uClockDateTime( Clock));
} // SysDateTime

void SysDateTimeSet( dword DateTime)
// Set system clock by wall clock <DateTime>
{
UClockGauge Clock;

   Clock = uDateTimeClock( DateTime);
   SysClockSet( Clock);
} // SysDateTimeSet

TYesNo SysDateTimeInvalid( void)
// Has system invalid time?
{
   return RtcInvalid();
} // SysDateTimeInvalid

//------------------------------------------------------------------------------
// Timer interrupt
//------------------------------------------------------------------------------

void _SysTimerDisable( void)
// Stop timer interrupts
{
   timerDisable();   
} // _SysTimerDisable

void _SysTimerEnable( void)
// Start timer interrupts
{
   timerEnable();
} // _SysTimerEnable

//------------------------------------------------------------------------------
// Tick test
//------------------------------------------------------------------------------

static byte TickFast( void)
// Returns YES if fast tick occured
{
byte Is;

   timerDisable();
   Is = FlagFast();                    // read flag
   FlagFastClear();                    // clear flag
   timerEnable();
   return( Is);
} // TickFast

static byte TickSlow( void)
// Returns YES if slow tick occured
{
byte Is;

   timerDisable();
   Is = FlagSlow();                    // read flag
   FlagSlowClear();                    // clear flag
   timerEnable();
   return( Is);
} // TickSlow

static byte TickFlash1( void)
// Returns YES if flash 1 tick occured
{
byte Is;

   timerDisable();
   Is = FlagFlash1();                  // read flag
   FlagFlash1Clear();                  // clear flag
   timerEnable();
   return( Is);
} // TickFlash1

static byte TickFlash2( void)
// Returns YES if flash 2 tick occured
{
byte Is;

   timerDisable();
   Is = FlagFlash2();                  // read flag
   FlagFlash2Clear();                  // clear flag
   timerEnable();
   return( Is);
} // TickFlash2

static byte TickAlarm( void)
// Returns YES if alarm tick occured
{
byte Is;

   timerDisable();
   Is = FlagAlarm();                   // read flag
   FlagAlarmClear();                   // clear flag
   timerEnable();
   return( Is);
} // TickAlarm

//------------------------------------------------------------------------------
// Timer handler
//------------------------------------------------------------------------------

timerHandler()
{
   timerInterruptConfirm();            // confirm timer interrupt
   _TimerTick += TIMER_PERIOD;         // system timer
   // fast timer :
   _CounterFast--;
   if( _CounterFast == 0){
      CounterFastSet();                // count again
      FlagFastSet();                   // set flag
   }
   // slow timer :
   _CounterSlow--;
   if( _CounterSlow == 0){             
      CounterSlowSet();                // count again
      FlagSlowSet();                   // set flag
   }
   // flash timer :
   _CounterFlash--;
   if( _CounterFlash == TIMER_FLASH1){
      FlagFlash1Set();                 // flash 1 reached, set flag
   }
   if( _CounterFlash == 0){
      CounterFlashSet();               // count again
      FlagFlash2Set();                 // set flag
   }
   // alarm timer :
   if( _CounterAlarm){
      _CounterAlarm--;
      if( _CounterAlarm == 0){
         FlagAlarmSet();
      }
   }

   timerExecute();
} // timerHandler
