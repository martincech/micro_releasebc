//******************************************************************************
//
//   SysTimer.c     Simple system timer
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "System/System.h"
#include "System/SystemTimer.c"

static volatile word _TimerTick;

//------------------------------------------------------------------------------
//   Start timer
//------------------------------------------------------------------------------

void SysTimerStart( void)
// Start system timer
{
   timerInit();
   timerEnable();
} // SysTimerStart

//------------------------------------------------------------------------------
// Timer interrupt
//------------------------------------------------------------------------------

void _SysTimerDisable( void)
// Stop timer interrupts
{
   timerDisable();   
} // _SysTimerDisable

void _SysTimerEnable( void)
// Start timer interrupts
{
   timerEnable();
} // _SysTimerEnable

//------------------------------------------------------------------------------
//   Timer handler
//------------------------------------------------------------------------------

timerHandler()
{
   timerInterruptConfirm();
   _TimerTick += TIMER_PERIOD;         // system timer
   SysTimerExecute();                  // user actions
} // timerHandler

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

word SysTimer( void)
// Returns milisecond timer
{
   return( _TimerTick);
} // SysTimer