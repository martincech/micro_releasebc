//******************************************************************************
//
//    Delay.h      System delay loops
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __Delay_H__
   #define __Delay_H__

#ifndef __Uni_H__
   #include "Unisys/Uni.h"
#endif

#ifdef __cplusplus
   extern "C" {
#endif

void SysDelay( word ms);
// Milisecond delay

void SysUDelay( word us);
// Microsecond delay

#ifdef __cplusplus
   }
#endif

#endif
