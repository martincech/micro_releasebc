//******************************************************************************
//
//    SystemTimer.c  System timer
//    Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#error "Timer.c code template only"

// timer interrupt :
#define timerEnable()        // enable timer interrupt
#define timerDisable()       // disable timer interrupt

// timer initialization :
#define timerInit()          // prepare timer for run

// timer period :
#define timerPeriodSet()     // set time period

// timer start :
#define timerStart()         // start timer

// timer handler header :
#define timerHandler()       // timer ISR header

// timer interrupt confirm :
#define timerInterruptConfirm()    // confirm timer interrupt
