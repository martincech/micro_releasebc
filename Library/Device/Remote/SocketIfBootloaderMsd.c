//*****************************************************************************
//
//    SocketIfBootloaderMsd.c     Mass storage device socket interface
//    Version 1.0       (c) VEIT Electronics
//
//*****************************************************************************

#include "Remote/SocketIfBootloaderMsd.h"
#include "Remote/SocketIfMsdDef.h"
#include "File/Efs.h"
#include "Remote/Frame.h"
#include "Console/conio.h"
#include "Config/Configuration.h"
#include <string.h>
#define CHUNK_LENGTH   32

static TFrameState _State;
static byte         Status;
static TEfsFile File;
static EFileMode Permission;
//------------------------------------------------------------------------------
//   Permission
//------------------------------------------------------------------------------

byte SocketIfBootloaderMsdPermission( TSocket *Socket)
// Write only
{  
   return Permission;
} // SocketIfBootloaderMsdPermission

//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo SocketIfBootloaderMsdOpenForReceiveCmd(void)
// Open socket for cmd receive
{
   if (!EfsInit()) {
      return NO;
   }

   if(!EfsFileExists( FIRMWARE_FILENAME)) {
      EfsSwitchOff();
   }    

   if (!EfsFileOpen(&File, FIRMWARE_FILENAME)) {
      EfsSwitchOff();
      return NO;
   }
   Status = SOCKET_STATE_CONNECTED;
   Permission = FILE_MODE_READ_ONLY;
   return YES;
} // SocketIfBootloaderMsdOpen


//------------------------------------------------------------------------------
//  Open
//------------------------------------------------------------------------------

TYesNo SocketIfBootloaderMsdOpenForSendCmd( TSocket *Socket)
// Open socket for send cmd
{
   if(!EfsInit()) {
      return NO;
   }
   
   EfsFileDelete( FIRMWARE_FILENAME);

   if (!EfsFileCreate(&File, FIRMWARE_FILENAME, 0)) {
      EfsSwitchOff();
      return NO;
   }

   Status = SOCKET_STATE_CONNECTED;
   Permission = FILE_MODE_WRITE_ONLY;
   return YES;
} // SocketIfBootloaderMsdOpenForCmd

//------------------------------------------------------------------------------
//  State
//------------------------------------------------------------------------------

byte SocketIfBootloaderMsdState( TSocket *Socket)
// Gets state of socket
{
   return Status;
} // SocketIfBootloaderMsdState

//------------------------------------------------------------------------------
//  Receive
//------------------------------------------------------------------------------

TYesNo SocketIfBootloaderMsdReceive( TSocket *Socket, void *Buffer, int Size)
// Receive into <Buffer> with <Size>
{
byte Chunk[CHUNK_LENGTH];
int ProccessedLength;
int ChunkLength;

   if(!FrameReceiveStart(&_State, Buffer, Size)) {
      Status = SOCKET_STATE_ERROR;
      return NO;
   }
   forever {
      ChunkLength = EfsFileRead( &File, Chunk, CHUNK_LENGTH);
      if(ChunkLength == 0) {
         return YES;
      }
      ProccessedLength = FrameReceiveProccess( &_State, Chunk, ChunkLength);
      if(ProccessedLength < ChunkLength) {
         if(!EfsFileSeek( &File, ProccessedLength - ChunkLength, EFS_SEEK_CURRENT)) {
            Status = SOCKET_STATE_ERROR;
            return NO;
         }
      }
      switch(_State.Status) {
         case FRAME_STATE_RECEIVE_ACTIVE:
         case FRAME_STATE_RECEIVE_WAITING_HEADER :
            break;

         case FRAME_STATE_RECEIVE_FRAME:
            Status = SOCKET_STATE_RECEIVE_DONE;
            return YES;

         default:
            Status = SOCKET_STATE_ERROR;
            return NO;
      }
   }
} // SocketIfBootloaderMsdReceive

//------------------------------------------------------------------------------
//  Send
//------------------------------------------------------------------------------

TYesNo SocketIfBootloaderMsdSend( TSocket *Socket, const void *Buffer, int Size)
// Send <Buffer> with <Size>
{
byte Chunk[CHUNK_LENGTH];
int ProccessedLength;

   if(!FrameSendInit(&_State, Buffer, Size)) {
      Status = SOCKET_STATE_ERROR;
      return NO;
   }
   forever {
      if(!FrameSendProccess( &_State, Chunk, CHUNK_LENGTH, &ProccessedLength)) {
         Status = SOCKET_STATE_ERROR;
         return NO;
      }
      if(!EfsFileWrite(&File, Chunk, ProccessedLength)) {
         Status = SOCKET_STATE_ERROR;
         return NO;
      }
      switch(_State.Status) {
         case FRAME_STATE_SEND_ACTIVE:
            break;

         case FRAME_STATE_SEND_DONE:
            Status = SOCKET_STATE_SEND_DONE;
            return YES;

         default:
            return NO;
      }
   }
} // SocketIfBootloaderMsdSend

//------------------------------------------------------------------------------
//  Receive size
//------------------------------------------------------------------------------

int SocketIfBootloaderMsdReceiveSize( TSocket *Socket)
// Gets number of received bytes
{
   return _State.SizeProccessed;
} // SocketIfBootloaderMsdReceiveSize

//------------------------------------------------------------------------------
//  Close
//------------------------------------------------------------------------------

void SocketIfBootloaderMsdClose( TSocket *Socket)
// Close socket
{
   EfsFileClose( &File);
   EfsSwitchOff();
   Status = SOCKET_STATE_DISCONNECTED;
   Permission = 0;
} // SocketIfBootloaderMsdClose
