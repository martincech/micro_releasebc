//*****************************************************************************
//
//    SocketIfEthernet.h    Ethernet socket interface
//    Version 1.0           (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __SocketIfEthernet_H__
   #define __SocketIfEthernet_H__

#include "Socket.h"

byte SocketIfEthernetPermission( TSocket *Socket);
// Permission

TYesNo SocketIfEthernetListen( TSocket *Socket);
// Listen

void SocketIfEthernetExecute( void);
// Executive

void SocketIfEthernetTimer( void);
// Timer

byte SocketIfEthernetState( TSocket *Socket);
// Gets state of socket

TYesNo SocketIfEthernetReceive( TSocket *Socket, void *Buffer, int Size);
// Receive into <Buffer> with <Size>

int SocketIfEthernetReceiveSize( TSocket *Socket);
// Gets number of received bytes

TYesNo SocketIfEthernetSend( TSocket *Socket, const void *Buffer, int Size);
// Send <Buffer> with <Size>

void SocketIfEthernetClose( TSocket *Socket);
// Close socket

#endif