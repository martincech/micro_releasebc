//******************************************************************************
//
//   SocketIfUart.h     Uart socket interface
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __SocketIfUart_H__
#define __SocketIfUart_H__

#include "Socket.h"

byte SocketIfUartPermission( TSocket *Socket);
// Permission

TYesNo SocketIfUartListen( TSocket *Socket);
// Listen

void SocketIfUartInit( void);
// init

void SocketIfUartDeInit( void);
// free

void SocketIfUartExecute( void);
// Executive

void SocketIfUartTimer( void);
// Timer

byte SocketIfUartState( TSocket *Socket);
// Gets state of socket

TYesNo SocketIfUartReceive( TSocket *Socket, void *Buffer, int Size);
// Receive into <Buffer> with <Size>

int SocketIfUartReceiveSize( TSocket *Socket);
// Gets number of received bytes

TYesNo SocketIfUartSend( TSocket *Socket, const void *Buffer, int Size);
// Send <Buffer> with <Size>

void SocketIfUartClose( TSocket *Socket);
// Close socket

#endif // __SocketIfUart_H__


