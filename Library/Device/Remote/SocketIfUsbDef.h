//*****************************************************************************
//
//    SocketIfUsbDef.h  Usb socket interface
//    Version 1.0       (c) VEIT Electronics
//
//*****************************************************************************

#ifndef _SocketIfUsbDef_H_
   #define _SocketIfUsbDef_H_

#include "Unisys/Uni.h"

typedef struct {
   TYesNo Open  :  1;
   dword  Dummy : 31;
} TSocketIfUsbCmd;

typedef struct {
   TYesNo ReadyReceive :  1;
   dword  Dummy        : 31;
} TSocketIfUsbReply;

#endif
