//******************************************************************************
//
//    FwStorage.c  Firmware updater storage
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "FwStorage.h"
#include "Memory/File.h"
#include "Memory/NvmLayout.h"

#define CONFIRM_BYTE    0x55

static TFile File;




TYesNo FwStorageRead(int Address, void *Buffer, int Size) {
   return FileLoad(&File, Address, Buffer, Size);
}



TYesNo FwStorageReady( void) {
   if(FileByteRead(&File, NVM_FIRMWARE_SIZE - 1) == CONFIRM_BYTE) {
      return YES;
   }

   return NO;
}

void FwStorageInvalidate( void) {
   FileByteWrite(&File, NVM_FIRMWARE_SIZE - 1, 0x00);
}


void FwStorageInit( void) {
}


TYesNo FwStorageOpen( void) {
   if(!FileOpen(&File, FILE_FIRMWARE_REMOTE, FILE_MODE_WRITE_ONLY)) {
      return NO;
   }
   return YES;
}

TYesNo FwStorageWrite(int Address, void *Buffer, int Size) {
   return FileSave(&File, Address, Buffer, Size);
}

void FwStorageConfirm( void) {
   FileByteWrite(&File, NVM_FIRMWARE_SIZE - 1, CONFIRM_BYTE);
}

void FwStorageClose() {
   FileClose(&File);
}
