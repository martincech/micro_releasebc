//******************************************************************************
//
//    FwStorage.h  Firmware updater storage
//    Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef __FwStorage_H__
   #define __FwStorage_H__

#include "Unisys/Uni.h"

#ifdef __cplusplus
   extern "C" {
#endif

void FwStorageInit( void);

TYesNo FwStorageWrite(int Address, void *Buffer, int Size);

TYesNo FwStorageRead(int Address, void *Buffer, int Size);

TYesNo FwStorageOpen( void);
void FwStorageConfirm( void);
void FwStorageClose();

TYesNo FwStorageReady( void);

void FwStorageInvalidate( void);

#ifdef __cplusplus
   }
#endif

#endif
