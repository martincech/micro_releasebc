//*****************************************************************************
//
//    FwDef.h        Firmware definitions
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#ifndef __FwDef_H__
#ifndef _MANAGED
   #define __FwDef_H__
#endif

#ifndef _MANAGED

#include "Unisys/Uni.h"
#include "Device/VersionDef.h"



#define BOOTLOADER_SIZE          0x8000
#define MCU_FLASH_SIZE			 (512 * 1024)
//#include "Hardware.h"
#include <stddef.h>
#define OFFSETOF_BOOTLOADER            0
#define OFFSETOF_APPLICATION           (OFFSETOF_BOOTLOADER + sizeof(TBootloader))
#define OFFSETOF_APPLICATION_HEADER    OFFSETOF_APPLICATION
#define OFFSETOF_APPLICATION_CODE      (OFFSETOF_APPLICATION_HEADER + sizeof(TApplicationHeader))

#define APPLICATION_SIZE         MCU_FLASH_SIZE - sizeof(TBootloader)

typedef struct {
   byte Bootloader[BOOTLOADER_SIZE];
} TBootloader;

typedef struct {
   dword StackPointer;
   dword ResetVector;
} TApplicationHeader;

typedef union {
   TApplicationHeader Header;
   byte ApplicationCode[APPLICATION_SIZE];
} TApplication;

typedef struct {
   TBootloader Bootloader;
   TApplication Application;
} TFlashMap;






#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

typedef byte TFwCrc;

typedef enum {
   FW_NONSENSE,
   FW_VERSION,
   FW_DATA,
   FW_TERMINATOR,
} EFwCmd;

typedef struct {
   byte Random;
} __packed TFwNonSense;

typedef struct {
   TDeviceVersion Version;
} __packed TFwVersion;

#define FW_DATA_COUNT      1024

typedef struct {
   dword Address;
   word Count;
   byte Data[FW_DATA_COUNT];
} __packed TFwData;

typedef qword TFwDataCrc;

typedef struct {
   TFwDataCrc Crc;
} __packed TFwTerminator;

typedef union {
   TFwNonSense NonSense;
   TFwVersion Version;
   TFwTerminator Terminator;
   TFwData Data;
} UFwCmd;



typedef struct {
   
   byte Cmd;
   UFwCmd Data;
} __packed TFwCmd;


typedef struct {
   word Length;
   TFwCmd Cmd;
   TFwCrc Crc;
} __packed TFwCmdCrc;



#define FwCrcStart( Crc)               *(Crc) = 0
#define FwCrcProcess( Crc, NewChar)   *(Crc) += NewChar
#define FwCrcEnd( Crc)




#define FwDataCrcStart( Crc)               *(Crc) = 0
#define FwDataCrcProcess( Crc, NewChar)   *(Crc) += NewChar
#define FwDataCrcEnd( Crc)       




#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif




#define FwCrc(CmdCrc)    (*(TFwCrc *)((byte *)&(CmdCrc) + FwCrcSize( (CmdCrc).Length) - sizeof(TFwCrc)))

#define FwSizeOfSimpleCmd()    (1)

#define FwSizeOf( Name)    (FwSizeOfSimpleCmd() + sizeof(TFw##Name))

#define FwSizeOfData( Count)    (FwSizeOf(Data) - FW_DATA_COUNT + Count) 


#define FwCrcSize( Count)    (sizeof(word) + sizeof(TFwCrc) + Count)

#endif

#ifdef _MANAGED
#undef _MANAGED
#include "FwDef.h"
#define _MANAGED
#endif
#endif
