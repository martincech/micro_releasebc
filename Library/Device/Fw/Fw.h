//*****************************************************************************
//
//    Fw.h           Firmware
//    Version 1.0    (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Fw_H__
   #define __Fw_H__
   
#include "Unisys/Uni.h"
#include "Fw/FwDef.h"

typedef enum {
   FW_STATUS_ERROR = NO,
   FW_STATUS_PROCESSING,
   FW_STATUS_DONE
} EFwStatus;

void FwInit( TYesNo TestRun);
// Inits processing

byte FwExecute( void *Buffer, int Length);
// Exexute with new data, returns status

TYesNo FwValidate( void);
// Validate Fw 

void FwApplicationRun( void);

TYesNo FwVersionCheck( TFwVersion *_Version);

#endif