//*****************************************************************************
//
//    Rx8025.c    Real time clock RX-8025 SA/NB services
//    Version 1.0 (c) VEIT Electronics
//
//*****************************************************************************

#include "Rtc/Rtc.h"
#include "System/System.h"   // SysUDelay
#include "Convert/uBcd.h"    // BCD arihmetic

#include "Rtc/RtcIic.h"      // I2C interface

// I2C Address :
#define RTC_BASE_ADDRESS  0x64                        // RTC base address
#define RTC_READ         (RTC_BASE_ADDRESS | 0x01)    // RTC read
#define RTC_WRITE         RTC_BASE_ADDRESS            // RTC write

//-----------------------------------------------------------------------------
// Register addresses
//-----------------------------------------------------------------------------

// upper nibble :
#define RTC_SEC_ADDRESS           0x00       // second BCD
#define RTC_MIN_ADDRESS           0x01       // minutes BCD
#define RTC_HOUR_ADDRESS          0x02       // hours BCD
#define RTC_WDAY_ADDRESS          0x03       // day of week BCD
#define RTC_DAY_ADDRESS           0x04       // day of month BCD
#define RTC_MONTH_ADDRESS         0x05       // month BCD
#define RTC_YEAR_ADDRESS          0x06       // year BCD

#define RTC_CLOCK_ADDRESS         RTC_SEC_ADDRESS     // first address of clock information
#define RTC_CLOCK_SIZE            7                   // size of clock information

#define RTC_OFFSET_ADDRESS        0x07       // digital offset - XTAL correction
#define RTC_ALARMW_MIN_ADDRESS    0x08       // Alarm W - minute
#define RTC_ALARMW_HOUR_ADDRESS   0x09       // Alarm W - hour
#define RTC_ALARMW_WDAY_ADDRESS   0x0A       // Alarm W - weekday
#define RTC_ALARMD_MIN_ADDRESS    0x0B       // Alarm D - minute
#define RTC_ALARMD_HOUR_ADDRESS   0x0C       // Alarm D - hour

#define RTC_CONTROL1_ADDRESS      0x0E       // Control 1
#define RTC_CONTROL2_ADDRESS      0x0F       // Control 2

#define RTC_SIMPLE_READ           0x04       // simple read (OR with address)

//-----------------------------------------------------------------------------
// Register masks
//-----------------------------------------------------------------------------

// Register CONTROL 1 :
#define RTC_INTA_OFF              0x00       // CT0-2 /INTA = HiZ
#define RTC_INTA_LOW              0x01       // CT0-2 /INTA = Fixed Low
#define RTC_INTA_PULSE_2HZ        0x02       // CT0-2 /INTA = Pulse mode 2 Hz
#define RTC_INTA_PULSE_1HZ        0x03       // CT0-2 /INTA = Pulse mode 1 Hz
#define RTC_INTA_LEVEL_SEC        0x04       // CT0-2 /INTA = Level mode 1 sec
#define RTC_INTA_LEVEL_MIN        0x05       // CT0-2 /INTA = Level mode 1 min
#define RTC_INTA_LEVEL_HOUR       0x06       // CT0-2 /INTA = Level mode 1 hour
#define RTC_INTA_LEVEL_MONTH      0x07       // CT0-2 /INTA = Level mode 1 month
#define RTC_CLEN2                 0x10       // /CLEN2 FOUT control
#define RTC_24                    0x20       // /12,24 24-hour clock
#define RTC_DALE                  0x40       // DALE alarm D enable
#define RTC_WALE                  0x80       // WALE alarm W enable

// Register CONTROL 2 :
#define RTC_DAFG                  0x01       // DAFG alarm D flag
#define RTC_WAFG                  0x02       // WAFG alarm W flag
#define RTC_CTFG                  0x04       // CTFG periodic interrupt flag
#define RTC_CLEN1                 0x08       // /CLEN1 FOUT control
#define RTC_PON                   0x10       // PON  1 - power on reset detected
#define RTC_XST                   0x20       // /XST 0 - oscillator stop detected
#define RTC_VDET                  0x40       // VDET 1 - power drop detected
#define RTC_VDSL_13               0x80       // VDSL set power drop treshold 1.3V (0 - 2.1V)

// Default values :
#define RTC_CONTROL1_DEFAULT     (RTC_INTA_OFF | RTC_24)
#define RTC_CONTROL2_DEFAULT     (RTC_XST)

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void RtcInit( void)
// Initialize bus and RTC
{
TYesNo Ack;

   iicRtcInit();
   iicRtcStart();
   Ack = iicRtcSend( RTC_WRITE);
   // write control registers :
   Ack = iicRtcSend( RTC_CONTROL1_ADDRESS << 4);
   Ack = iicRtcSend( RTC_CONTROL1_DEFAULT);      // address CONTROL1
   Ack = iicRtcSend( RTC_CONTROL2_DEFAULT);      // address CONTROL2
   iicRtcStop();
   SysUDelay( 65);                               // stop delay
} // RtcInit

//-----------------------------------------------------------------------------
// Load
//-----------------------------------------------------------------------------

UDateTimeGauge RtcLoad( void)
// Returns RTC time
{
byte      Clock[ RTC_CLOCK_SIZE];
UDateTime TmpTime;
TYesNo    Ack;

   iicRtcStart();
   Ack = iicRtcSend( RTC_WRITE);
   Ack = iicRtcSend( RTC_CLOCK_ADDRESS << 4);
   // Read data :
   iicRtcStart();
   Ack = iicRtcSend( RTC_READ);
   iicRtcRead( Clock, RTC_CLOCK_SIZE);
   iicRtcStop();
   SysUDelay( 65);                     // stop delay
   // Decode data :
   TmpTime.Time.Sec   = uBcdToBinary( Clock[ RTC_SEC_ADDRESS]);
   TmpTime.Time.Min   = uBcdToBinary( Clock[ RTC_MIN_ADDRESS]);
   TmpTime.Time.Hour  = uBcdToBinary( Clock[ RTC_HOUR_ADDRESS]);
   TmpTime.Date.Day   = uBcdToBinary( Clock[ RTC_DAY_ADDRESS]);
   TmpTime.Date.Month = uBcdToBinary( Clock[ RTC_MONTH_ADDRESS]);
   TmpTime.Date.Year  = uBcdToBinary( Clock[ RTC_YEAR_ADDRESS]);
   return( uDateTimeGauge( &TmpTime));
} // RtcLoad

//-----------------------------------------------------------------------------
// Save
//-----------------------------------------------------------------------------

void RtcSave( UDateTimeGauge DateTime)
// Set RTC time by <Time>
{
byte      Clock[ RTC_CLOCK_SIZE];
UDateTime TmpTime;

   // Encode data :
   uDateTime( &TmpTime, DateTime);
   Clock[ RTC_SEC_ADDRESS]   = uBinaryToBcd( TmpTime.Time.Sec);
   Clock[ RTC_MIN_ADDRESS]   = uBinaryToBcd( TmpTime.Time.Min);
   Clock[ RTC_HOUR_ADDRESS]  = uBinaryToBcd( TmpTime.Time.Hour);
   Clock[ RTC_WDAY_ADDRESS]  = uBinaryToBcd( (uDateTimeDow( DateTime) + 1) % 7);
   Clock[ RTC_DAY_ADDRESS]   = uBinaryToBcd( TmpTime.Date.Day);
   Clock[ RTC_MONTH_ADDRESS] = uBinaryToBcd( TmpTime.Date.Month);
   Clock[ RTC_YEAR_ADDRESS]  = uBinaryToBcd( TmpTime.Date.Year);
   // Write data :
   iicRtcStart();
   iicRtcSend( RTC_WRITE);
   iicRtcSend( RTC_CLOCK_ADDRESS << 4);
   iicRtcWrite( Clock, RTC_CLOCK_SIZE);
   iicRtcStop();
   SysUDelay( 65);                     // stop delay
} // RtcSave
