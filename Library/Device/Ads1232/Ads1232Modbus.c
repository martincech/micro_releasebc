//*****************************************************************************
//
//    Ads1232.c    A/D convertor
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Ads1232.h"
#include "Filter/FilterRelative.h"
#include "System/System.h"
#include <string.h>


#define INVALID_TIMESTAMP     0xFFFF

static uint16 ExpectedTimestamp;
static TYesNo Running;
static dword NextTick;
//-----------------------------------------------------------------------------
// Internal variables
//-----------------------------------------------------------------------------

static TAdcValue _AdcRawValue;
static word      _AdcCount;
static int64     _AdcSum;
static TYesNo    _CallBackEnable;

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void AdcInit( void)
// Initialisation
{
   Running = NO;
   _AdcSum   = 0;
   _AdcCount = 0;
   FilterStop();                        // default filter status
} // AdcInit

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

void AdcReset( void)
// Reset communication
{
} // AdcReset

//-----------------------------------------------------------------------------
// Data rate
//-----------------------------------------------------------------------------

void AdcDataRateSet( word Hertz)
// Set conversion data rate
{
} // AdcDataRateSet

//-----------------------------------------------------------------------------
// Enable SINC3
//-----------------------------------------------------------------------------

void AdcSinc3Set( TYesNo Enable)
// <Enable> SINC3 filtering
{
} // AdcSinc3Set

//-----------------------------------------------------------------------------
// Enable CHOP
//-----------------------------------------------------------------------------

void AdcChopSet( TYesNo Enable)
// <Enable> CHOP filtering
{
} // AdcChopSet

//-----------------------------------------------------------------------------
// Idle
//-----------------------------------------------------------------------------

void AdcIdle( void)
// Set idle mode
{
} // AdcIdle

//-----------------------------------------------------------------------------
// Power down
//-----------------------------------------------------------------------------

void AdcPowerDown( void)
// Set power down mode
{
} // AdcPoweDown

//-----------------------------------------------------------------------------
// Start
//-----------------------------------------------------------------------------

void AdcStart( void)
// Start continuous conversion
{
   FilterStart();                                     // start filtering
   Running = YES;
   ExpectedTimestamp = INVALID_TIMESTAMP;
   NextTick = SysTimer();
} // AdcStart

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void AdcStop( void)
// Stop continous conversion
{
   FilterStop();                                      // stop filtering
   Running = NO;
} // AdcStop

//-----------------------------------------------------------------------------
// Raw read
//-----------------------------------------------------------------------------

TAdcValue AdcRawRead( void)
// Read actual conversion
{
   return( _AdcRawValue);
} // AdcRawRead

//-----------------------------------------------------------------------------
// Average read
//-----------------------------------------------------------------------------

TAdcValue AdcAverageRead( void)
// Read averaged value
{
TAdcValue Average;
int64     Sum;
word      Count;

   Sum       = _AdcSum;                // remember value
   Count     = _AdcCount;              // remember value
   _AdcSum   = 0;                      // start new sum
   _AdcCount = 0;                      // start new count
   if( Count == 0){
      return( 0);
   }
   Average = (TAdcValue)(Sum / Count);
   return( Average);
} // AdcAverageRead

//-----------------------------------------------------------------------------
// Low pass read
//-----------------------------------------------------------------------------

TAdcValue AdcLowPassRead( void)
// Read filtered conversion
{
TRawWeight Value;

   Value = FilterRecord.LowPass;       // low pass value
   return( Value);
} // AdcLowPassRead

//-----------------------------------------------------------------------------
// Stable value read
//-----------------------------------------------------------------------------

TYesNo AdcRead( TAdcValue *Weight)
// Read stable value
{
TYesNo Result;

   Result = FilterRead( Weight);
   return( Result);
} // AdcRead

//-----------------------------------------------------------------------------
// Callback enable
//-----------------------------------------------------------------------------

void AdcCallbackEnable( TYesNo Enable)
// Enables/disables ADC callback
{
   _CallBackEnable = Enable;
} // AdcCallBackEnable

void AdcSetFromModbus(TAdcModbusSample *Samples, byte Count) {
   if(Count == 0) {
      return;
   }

   Count--;

   // rewind invalid samples
   while(Samples[Count].Timestamp == INVALID_TIMESTAMP) {
      if(Count == 0) {
         return;
      }
      Count--;
   }

   // rewind already processed samples
   if(ExpectedTimestamp != INVALID_TIMESTAMP) {
      while(Samples[Count].Timestamp != ExpectedTimestamp) {
         if(Count == 0) {
            AdcStop();
            AdcStart();
            return;
         }
         Count--;
      }
   }
   // process samples
   for(int i = Count ; ; ) {
      if(Samples[i].Timestamp == INVALID_TIMESTAMP) {
         return;
      }

      if(ExpectedTimestamp != INVALID_TIMESTAMP) {
         if(ExpectedTimestamp != Samples[i].Timestamp) {
            AdcStop();
            AdcStart();
            return;
         }
      } else {
         ExpectedTimestamp = Samples[i].Timestamp; // Catch first timestamp
      }

      Samples[i].Value = (Samples[i].Value << 16) | (Samples[i].Value >> 16);

      _AdcSum      += Samples[i].Value;       // averaging sum
      _AdcCount++;                        // averaging count
      FilterNextSample( Samples[i].Value);    // filtering

      if(_CallBackEnable) {
         AdcCallback();
      }

      ExpectedTimestamp = Samples[i].Timestamp + 1;

      if(ExpectedTimestamp == INVALID_TIMESTAMP) {
         ExpectedTimestamp = 0;
      }

      if(i == 0) {
         break;
      }
      i--;
   }
}

TYesNo AdcRunning( void) {
   return Running;
}