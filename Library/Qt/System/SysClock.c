//*****************************************************************************
//
//   SysClock.c   System date/time utility
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "System/System.h"
#include "Time/uClock.h"
#include <windows.h>               // Sleep only
#include <time.h>
#include "Utility/Performance.h"
//-----------------------------------------------------------------------------
// Timer
//-----------------------------------------------------------------------------

word SysTimer( void)
// Returns milisecond timer
{
double CounterSeconds;

   CounterSeconds = TimerSeconds( TimerGet());  // x86 performance timer
   return( (word)(CounterSeconds * 1000.0));
} // SysTimer

dword SysTime( void)
// return second timer
{
double CounterSeconds;

   CounterSeconds = TimerSeconds( TimerGet());  // x86 performance timer
   return((dword)CounterSeconds);
}

//-----------------------------------------------------------------------------
// Clock
//-----------------------------------------------------------------------------

int Bias = 0;

dword SysClock( void)
// Returns actual date/time
{
UDateTime   DateTime;
time_t      now;
struct tm  *tm;

   time( &now);
   tm = localtime( &now);
   DateTime.Time.Sec   = tm->tm_sec;
   DateTime.Time.Min   = tm->tm_min;
   DateTime.Time.Hour  = tm->tm_hour;
   DateTime.Date.Day   = tm->tm_mday;
   DateTime.Date.Month = tm->tm_mon  + MONTH_JANUARY;
   DateTime.Date.Year  = tm->tm_year + 1900 - 2000;
   return( uClockGauge( &DateTime) + Bias);
} // SysClock

void SysClockSet( dword DateTime)
// Set actual date/time
{
UDateTime dt;
struct tm tm;
time_t    now;
time_t    set;

   uClock( &dt, DateTime);    // parse to items
   tm.tm_sec   = dt.Time.Sec;
   tm.tm_min   = dt.Time.Min;
   tm.tm_hour  = dt.Time.Hour;
   tm.tm_mday  = dt.Date.Day;
   tm.tm_mon   = dt.Date.Month - MONTH_JANUARY;
   tm.tm_year  = dt.Date.Year - 1900 + 2000;
   tm.tm_isdst = -1;
   set = mktime( &tm);
   time( &now);
   Bias = (int)(set - now);
} // SysClockSet

//-----------------------------------------------------------------------------
// Date time
//-----------------------------------------------------------------------------

dword SysDateTime( void)
// Returns wall clock date/time
{
UClockGauge Clock;

   Clock = SysClock();
   return( uClockDateTime( Clock));
} // SysDateTime

void SysDateTimeSet( dword DateTime)
// Set wall clock <DataTime>
{
UClockGauge Clock;

   Clock = uDateTimeClock( DateTime);
   SysClockSet( Clock);
} // SysDateTimeSet

TYesNo SysDateTimeInvalid( void)
// Has system invalid time?
{
   return NO;
}

//-----------------------------------------------------------------------------
// Delay
//-----------------------------------------------------------------------------

void SysDelay( word ms)
// Delay loop [ms]
{
   Sleep( ms);             // Win32 API
} // SysDelay

void SysUDelay( word us)
// Delay loop [us]
{
   Sleep( 1);              // short delay
} // SysUDelay
