//******************************************************************************
//
//   CodeSource.h     Source code generator
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#ifndef __CodeSource_H__
   #define __CodeSource_H__

#include <QString>
#include <QStringList>

namespace CodeSource
{

   //---------------------------------------------------------------------------
   QString includeFileLocal( QString filePath, QString fileName);
   // generate local include statement for <filePath> and <fileName>
   QString includeFileGlobal( QString filePath, QString fileName);
   // generate global include statement for <filePath> and <fileName>
   //---------------------------------------------------------------------------
   QString functionDefinition( QString returnType, QString name, QStringList parameters, QString comment, QString body);
   QString functionDefinition( QString returnType, QString name, QString parameters, QString comment, QString body);
   QString functionDefinition( QString returnType, QString name, const char * parameters, QString comment, QString body);
   // generate function with body
   QString functionDeclaration(QString returnType, QString name, QStringList parameters, QString comment);
   QString functionDeclaration(QString returnType, QString name, QString parameters, QString comment);
   QString functionDeclaration(QString returnType, QString name, const char * parameters, QString comment);
   // generate function declaration
   QString functionCall( QString name, QStringList parameters);
   QString functionCall( QString name, QString     parameters);
   QString functionCall( QString name, const char *parameters);
   // generate function call
   //---------------------------------------------------------------------------
   QString globalVariableDefinition( QString type, QString name, QString defaultValue = "0", QString description = "");
   // generate global variable definition:  type _CamelCaseName; //description
   QString globalVariable( QString name);
   // generate access string to global variable
} // CodeSource


#endif // __CodeSource_H__


