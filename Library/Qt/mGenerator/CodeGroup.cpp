//******************************************************************************
//
//   CodeGroup.c        Source/header code generator for every register group
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "CodeGroup.h"
#include "uGenerator/SourceTemplate.h"
#include "Parse/nameTransformation.h"
#include "uGenerator/CodeHeader.h"
#include "mGenerator/CodeSource.h"
#include "uGenerator/CodeSeparator.h"
#include <QMessageBox>
//------------------------------------------------------------------------------
//  Code for source
//------------------------------------------------------------------------------

QString CodeGroup::sourceCode( RegisterGroup *group)
//generate full source code for this <group>
{
int                  regCount;
Register             reg;
Register             firstReg;
SourceTemplate       sourceTemplate;
QString              SaveExecutiveName;
QStringList          slist;
QString              tmpString;
QString              ConcatString;
QStringList          LGroups;
bool                 inti;
QString functionBody;
QString functionDefinitions;
QString functionDeclarations;
Registers stringRegisters;
Registers commandRegisters;
Registers sbRegs;
Registers regs;
Registers globalRegisters;

   regCount = group->item.count();

   sourceTemplate.setFileName( SOURCE_FILE_NAME);
   sourceTemplate.setParameter( "NAME", "Modbus" + OName::toCamelCase(group->name) + "Group");
   sourceTemplate.setParameter( "DESCRIPTION", "Modbus " + group->name +" register group read write functions");
   //-------------------------------------------------------------------------------------
   //includes
   QStringList includes = group->Includes();
   for( int j = 0; j < includes.count(); j++){
      includes[ j] = CodeSource::includeFileLocal("", includes[ j]);
   }
   //-------------------------------------------------------------------------------------
   // index replacement
   group->IndexReplacement();
   //-------------------------------------------------------------------------------------

   //save executives for buffered registers
   if(!group->BufferedSubgroups()->isEmpty()){
      QHash<QString, RegisterGroup>::const_iterator it = group->BufferedSubgroups()->begin();
      // save executives
      int saveGroupOffset = 0;
      while (it != group->BufferedSubgroups()->end()) {
         SaveExecutiveName = OName::toCamelCase("_" + OName::fromUpperCase(it.key()) + " command executive");
         functionDefinitions += CodeSeparator::section("COMMAND " + it.key() + " executive");
         functionDefinitions += CodeSource::functionDefinition("static void", SaveExecutiveName, "", "execute "+ reg.name + " command - apply all parameteres", saveExecutive( it.value(), saveGroupOffset));
         functionDeclarations += CodeSource::functionDeclaration("static void", SaveExecutiveName, "", "execute "+ reg.name + " command - apply all parameteres");
         saveGroupOffset += it.value().Count();
         it++;
      }
      //group mapping
      functionDefinitions += CodeSeparator::section("Register mapping");
      functionDefinitions += groupMapping( *group->BufferedSubgroups(), group->name);
      functionDeclarations += CodeSource::functionDeclaration("static int", "_GetRegisterMapping", "EModbusRegNum R", "return mapping of discontinuous register number to contiuous array");
   }


   QString globalsText;
   for(int j = 0; j < regCount; j++){
      reg = group->item.at(j);
      if( reg.dataName.toLower() == KEYWORD_GLOBAL){
         globalsText += CodeSource::globalVariableDefinition( reg.dataGSLocals, group->name + " " + reg.name, reg.defaultValue, reg.description);
         tmpString = CodeSource::globalVariable( group->name + " " + reg.name);
         for (int i = 0; i < regCount; i++){
            firstReg = group->item.at(i);
            firstReg.dataName.replace(reg.name, tmpString);
            firstReg.dataGetter.replace(reg.name, tmpString);
            if( firstReg.dataName.toLower() == KEYWORD_GLOBAL ){
               firstReg.dataSetter.replace( reg.name, "D");
            }else{
               firstReg.dataSetter.replace(reg.name, tmpString);
            }
            firstReg.dataGSLocals.replace(reg.name, tmpString);
            group->item.replace(i, firstReg);
         }
      }
   }
   for(int j = 0; j < group->variables.count(); j++){
      globalsText += "static " + group->variableTypes[j]+ " " + group->variables[j] + ";\n";
   }


   //-------------------------------------------------------------------------------------
   //local functions declarations
   sourceTemplate.setParameter("LOCAL_DECLARATIONS", functionDeclarations);
   //-------------------------------------------------------------------------------------
   //Global static variables declaration
   sourceTemplate.setParameter( "GLOBALS", globalsText);
   //-------------------------------------------------------------------------------------
   //local functions definitions
   sourceTemplate.setParameter("LOCAL_DEFINITIONS", functionDefinitions);

   //-------------------------------------------------------------------------------------
   //functions
   //-------------------------------------------------------------------------------------
   //read
   //first fill GSGroups
   for( int j = 0; j < regCount; j++){
      reg = group->item.at(j);
      if( !(reg.read && (reg.registerType == REGISTER_REGISTER || reg.registerType == REGISTER_STRING || reg.registerType == REGISTER_STRING_BUFFER) && reg.dataName.toLower() != KEYWORD_GLOBAL)){
         continue;
      }
      //locals
      if( !reg.dataGSLocals.isEmpty()){
         slist = reg.dataGSLocals.split(";");
         for( int k = 0; k < slist.count(); k++){
            if( !LGroups.contains(slist[ k])){
               LGroups       << slist[ k];
            }
         }
      }
   }

   QString functions;
   functionBody = "";
   functionBody = LGroups.join(";\n");
   if( !functionBody.isEmpty()){
      functionBody += ";\n\n";
   }
   inti = false;
   //array types
   QList<Registers> arrayRegisters = group->ArrayTypeRegistersOfType(REGISTER_REGISTER, REGISTER_R , true) + group->ArrayTypeRegistersOfType(REGISTER_STRING, REGISTER_R , true) + group->ArrayTypeRegistersOfType(REGISTER_STRING_BUFFER, REGISTER_R , true);
   functionBody += arrayTypeRegistersRead(arrayRegisters, group->name);

   if( !inti && !arrayRegisters.isEmpty()){
      functionBody.insert(0, "int i;\n");
      inti = true;
   }

   //non array types
   regs = group->RegistersOfType(REGISTER_REGISTER, REGISTER_R , false) + group->RegistersOfType(REGISTER_STRING, REGISTER_R , false) + group->RegistersOfType(REGISTER_STRING_BUFFER, REGISTER_R , false);
   functionBody += nonArrayTypeRegistersRead(regs, group->name);
   functionBody += "   return 0;\n";

   functions += CodeSeparator::section("Read " + group->name + " register");
   functions += CodeSource::functionDefinition("word", OName::toCamelCase( QString("modbus reg read ") + group->name), "EModbusRegNum R", "Read "+ group->name +" register group", functionBody);
   //-------------------------------------------------------------------------------------
   //write
   //first fill GSGroups
   LGroups.clear();
   for( int j = 0; j < regCount; j++){
      reg = group->item.at(j);
        if( !(reg.write && (reg.registerType == REGISTER_REGISTER || reg.registerType == REGISTER_COMMAND || reg.registerType == REGISTER_STRING || reg.registerType == REGISTER_STRING_BUFFER) && reg.dataName.toLower() != KEYWORD_GLOBAL)){
         continue;
      }
      //locals
      if( !reg.dataGSLocals.isEmpty()){
         slist = reg.dataGSLocals.split(";");
         for( int k = 0; k < slist.count(); k++){
            if( !LGroups.contains(slist[ k])){
               LGroups       << slist[ k];
            }
         }
      }
   }
   functionBody = "";
   functionBody = LGroups.join(";\n");
   if( !functionBody.isEmpty()){
      functionBody += ";\n\n";
   }
   SourceTemplate groupWriteTemplate;
   inti = false;

   //string registers
   stringRegisters = group->RegistersOfType(REGISTER_STRING, REGISTER_W);
   commandRegisters = group->RegistersOfType(REGISTER_COMMAND, REGISTER_W);
   globalRegisters = group->GlobalRegisters().OfType(REGISTER_W, true, true);
   regs = group->RegistersOfType(REGISTER_REGISTER, REGISTER_W, false);
   arrayRegisters = group->ArrayTypeRegistersOfType(REGISTER_REGISTER, REGISTER_W, false);
   sbRegs = group->RegistersOfType(REGISTER_STRING_BUFFER, REGISTER_W);

   if(!group->RegistersOfType(REGISTER_W,true, true).isEmpty()){
      functionBody += "   // Check value correctness\n"
                      "   if( !ModbusRegRangeCheck"+OName::toCamelCase(group->name)+"( R, D)){\n"
                      "      return NO;\n"
                      "   }\n";
   }
   for(int j = 0; j < stringRegisters.count(); j++){
      reg = stringRegisters[j];
      groupWriteTemplate.setFileName( GROUP_WRITE_STRING);
      groupWriteTemplate.setParameter("NAME", CodeHeader::constantIdentifier("modbus reg", group->name, reg.name, ""));
      groupWriteTemplate.setParameter("SHORT_NAME", reg.name);
      groupWriteTemplate.setParameter("VALUE", reg.dataName);
      groupWriteTemplate.setParameter("VALUE_LEN", reg.stringLen.isEmpty()? "/*TODO*/1" : reg.stringLen);
      tmpString = reg.dataGSLocals;
      groupWriteTemplate.setParameter("GS_LOCALS", tmpString.replace(QRegExp("\n[ ]*"), "\n      "));
      ConcatString = "";
      if( !reg.dataGetter.isEmpty()){
         slist = reg.dataGetter.split(";");
         for( int k = 0; k < slist.count(); k++){
            ConcatString += "      " + slist[ k] + ";\n";
         }
      }
      tmpString = ConcatString;
      groupWriteTemplate.setParameter("GETTER", tmpString);
      ConcatString = "";
      if( !reg.dataSetter.isEmpty()){
         slist = reg.dataSetter.split(";");
         for( int k = 0; k < slist.count(); k++){
            ConcatString += "      " + slist[ k] + ";\n";
         }
      }
      tmpString = ConcatString;
      groupWriteTemplate.setParameter("SETTER", tmpString);
      functionBody += groupWriteTemplate.toString();
   }
   //command registers
   for(int j = 0; j < commandRegisters.count(); j++){
      reg = commandRegisters[j];
      groupWriteTemplate.setFileName( GROUP_WRITE_COMMAND);
      groupWriteTemplate.setParameter("NAME", CodeHeader::constantIdentifier("modbus reg", group->name, reg.name, ""));
      groupWriteTemplate.setParameter("SHORT_NAME", reg.name);
      SaveExecutiveName = "";
      if( !reg.dataSetter.isEmpty()){
         groupWriteTemplate.setParameter("CHECKER", "      if( !("+ reg.dataSetter + ")){\n         return NO;\n      }\n");
      }
      if( !reg.dataGetter.isEmpty()){
         slist = reg.dataGetter.split(";");
         slist.replaceInStrings(QRegExp("^"), "      ");
         slist.append("");
         SaveExecutiveName = slist.join(";\n");
         groupWriteTemplate.setParameter("COMMAND", SaveExecutiveName);
      }
      if(!group->BufferedSubgroups()->isEmpty() && group->BufferedSubgroups()->contains(reg.name)){
         tmpString = "      " + OName::toCamelCase("_" + OName::fromUpperCase(reg.name) + " command executive");
         SaveExecutiveName = CodeSource::functionCall(tmpString, "");
         groupWriteTemplate.setParameter("BODY", SaveExecutiveName);
      }
      functionBody += groupWriteTemplate.toString();
   }
   //global registers
   for(int j = 0; j < globalRegisters.count(); j++){
      reg = globalRegisters[j];
      groupWriteTemplate.setFileName( GROUP_WRITE_GLOBAL);
      groupWriteTemplate.setParameter("NAME", CodeHeader::constantIdentifier("modbus reg", group->name, reg.name, ""));
      groupWriteTemplate.setParameter("REG_NAME",  CodeSource::globalVariable( group->name + " " + reg.name));
      tmpString = reg.dataSetter;
      tmpString.replace( reg.name, " D");
      groupWriteTemplate.setParameter("CHECKER",  (tmpString.isEmpty()? "/*TODO*/0" : tmpString));
      ConcatString = "";
      if( !reg.dataGetter.isEmpty()){
         slist = reg.dataGetter.split(";");
         for( int k = 0; k < slist.count(); k++){
            ConcatString += "\n      " + slist[ k] + ";";
         }
      }
      groupWriteTemplate.setParameter("GETTER", ConcatString);
      functionBody += groupWriteTemplate.toString();
   }

   // R/W registers
   if(!regs.isEmpty() || !arrayRegisters.isEmpty()){
      functionBody += "   // R/W registers \n";
   }
   int j;
   // array type registers
   for(j = 0; j < arrayRegisters.count(); j++){
      Registers r = arrayRegisters[j];
      regs = regs.Exclude(r);
      if(group->BufferedSubgroupsContainsReg(r.first())){
         // exclude buffered
         continue;
      }
      if(!inti){
         inti = true;
         functionBody.insert(0, "int i;\n");
      }
      groupWriteTemplate.setFileName( GROUP_WRITE_ARRAY);
      groupWriteTemplate.setParameter("SHORT_NAME", r.first().ArrayName());
      groupWriteTemplate.setParameter("FIRST", CodeHeader::constantIdentifier("modbus reg", group->name, r.first().name, ""));
      groupWriteTemplate.setParameter("LAST", CodeHeader::constantIdentifier("modbus reg", group->name, r.last().name, ""));
      groupWriteTemplate.setParameter("GETTER", (r.first().dataGetter.isEmpty()? "": r.first().dataGetter + ";"));
      groupWriteTemplate.setParameter("BODY", r.first().dataName + " = D;");
      groupWriteTemplate.setParameter("SETTER",  (r.first().dataSetter.isEmpty()? "": r.first().dataSetter + ";"));
      functionBody += groupWriteTemplate.toString();
   }

   for(j = 0; j < regs.count(); j++){
      reg = regs[j];
      if(group->BufferedSubgroupsContainsReg(reg)){
         // exclude buffered
         continue;
      }

      groupWriteTemplate.setFileName( GROUP_WRITE);
      groupWriteTemplate.setParameter("NAME", CodeHeader::constantIdentifier("modbus reg", group->name, reg.name, ""));
      groupWriteTemplate.setParameter("SHORT_NAME", reg.name);
      groupWriteTemplate.setParameter("GETTER", (reg.dataGetter.isEmpty()? "": reg.dataGetter + ";"));
      groupWriteTemplate.setParameter("BODY", reg.dataName + " = D;");
      groupWriteTemplate.setParameter("SETTER",  (reg.dataSetter.isEmpty()? "": reg.dataSetter + ";"));
      functionBody += groupWriteTemplate.toString();
   }

   if(!sbRegs.isEmpty() && !group->BufferedSubgroups()->isEmpty()){
      QMessageBox::warning(NULL, "String buffer problem", "String buffer and SAVE method cannot be together in one group! Please see documentation.");
   }

   if(!sbRegs.isEmpty()){
      if( !inti){
         functionBody.insert(0, "int i;\n");
         inti = true;
      }
      groupWriteTemplate.setFileName( GROUP_WRITE_STRING_BUFFER);
      groupWriteTemplate.setParameter("FIRST", CodeHeader::constantIdentifier("modbus reg", group->name, sbRegs.first().name, ""));
      groupWriteTemplate.setParameter("LAST", CodeHeader::constantIdentifier("modbus reg", group->name, sbRegs.last().name, ""));
      groupWriteTemplate.setParameter("GROUP_CCNAME", OName::toCamelCase(group->name));
      functionBody += groupWriteTemplate.toString();
   }

   if(!group->BufferedSubgroups()->isEmpty()){
      if( !inti){
         functionBody.insert(0, "int i;\n");
         inti = true;
      }
      groupWriteTemplate.setFileName( GROUP_WRITE_BUFFERED);
      groupWriteTemplate.setParameter("FIRST", CodeHeader::constantIdentifier("modbus reg", group->name, group->BufferedFirst().name, ""));
      groupWriteTemplate.setParameter("LAST", CodeHeader::constantIdentifier("modbus reg", group->name, group->BufferedLast().name, ""));
      groupWriteTemplate.setParameter("GROUP_CCNAME", OName::toCamelCase(group->name));
      functionBody += groupWriteTemplate.toString();
   }

   functionBody += "   return NO;\n";
   functions += CodeSeparator::section("Write " + group->name + " register");
   QStringList params;params.append("EModbusRegNum R");params.append("word D");
   functions += CodeSource::functionDefinition("TYesNo", OName::toCamelCase( QString("modbus reg write ") + group->name), params, "Write "+ group->name +" register group", functionBody);
   includes += CodeSource::includeFileLocal("", MODBUS_RANGE_CHECK_FILENAME);


   for(int j = 0; j < regCount; j++){
      // include string.h if some string register present
      reg = group->item.at(j);
      if( reg.registerType == REGISTER_STRING){
         includes << CodeSource::includeFileGlobal("", "string");
         break;
      }
   }
   sourceTemplate.setParameter( "INCLUDE", includes.join(""));
   sourceTemplate.setParameter( "FUNCTIONS", functions);

   return sourceTemplate.toString();
}


//------------------------------------------------------------------------------
//  Code for header
//------------------------------------------------------------------------------

QString CodeGroup::headerCode( const RegisterGroup *group)
// generate header for this <group>
{
SourceTemplate       headerTemplate;

   headerTemplate.setFileName( HEADER_FILE_NAME);
   headerTemplate.setParameter( "NAME", "Modbus" + OName::toCamelCase(group->name) + "Group");
   headerTemplate.setParameter( "DESCRIPTION", "Modbus " + group->name +" register group read write functions");
   //includes
   QString includes;
   includes +=  CodeHeader::includeFile("Unisys", "Uni");
   includes +=  CodeHeader::includeFile("", MODBUS_REG_FILENAME);
   headerTemplate.setParameter( "INCLUDE", includes);
   Registers readRegs = group->RegistersOfType(REGISTER_R, true, true);
   Registers writeRegs = group->RegistersOfType(REGISTER_W, true, true);
   //functions
   QString functions;
   if(!readRegs.isEmpty()){
      functions += CodeSource::functionDeclaration("word", OName::toCamelCase( QString("modbus reg read ") + group->name), "EModbusRegNum R", "Read "+ group->name +" register group\n");
   }
   if(!writeRegs.isEmpty()){
      QStringList params;params.append("EModbusRegNum R");params.append("word D");
      functions += CodeSource::functionDeclaration("TYesNo", OName::toCamelCase( QString("modbus reg write ") + group->name), params, "Write "+ group->name +" register group\n");
   }
   headerTemplate.setParameter( "FUNCTIONS", functions);

   return( headerTemplate.toString());
}


//------------------------------------------------------------------------------
//  FileName
//------------------------------------------------------------------------------

QString CodeGroup::fileName(  const RegisterGroup *group)
// return file name for this group (without .c or .h)
{
   return "Modbus" + OName::toCamelCase(group->name) + "Group";
}


//------------------------------------------------------------------------------
//  Helper function groupMapping
//------------------------------------------------------------------------------

QString CodeGroup::groupMapping(const QHash<QString, RegisterGroup> bufferedGroup, QString groupName)
// helper function for group generation - create group mapping definition function
{
QString                    functionBody;
QString                    cases;
SourceTemplate             ifTemplate;
SourceTemplate             caseTemplate;
SourceTemplate             switchTemplate;
int                        i1;
bool                       inti = false;
   if(bufferedGroup.isEmpty()){
      return "";
   }
   i1 = 0;

   foreach (RegisterGroup group, bufferedGroup) {
      QList<Registers> arrayGroups = group.item.OfArrayType();
      Registers nonArrays = group.item.OfNonArrayType();
      foreach (Registers regs, arrayGroups) {
         ifTemplate.setFileName( GROUP_READ_ARRAY);
         ifTemplate.setParameter("FIRST", CodeHeader::constantIdentifier("modbus reg", groupName, regs.first().name, ""));
         ifTemplate.setParameter("LAST", CodeHeader::constantIdentifier("modbus reg", groupName, regs.last().name, ""));
         ifTemplate.setParameter("VALUE", CodeHeader::constantIdentifier("modbus reg", groupName, regs.first().name, "") + " + " + QString::number(i1));
         functionBody += ifTemplate.toString();
         if(!inti){
            functionBody.insert(0, "int i;\n");
            inti = true;
         }
         i1 += regs.count();
      }

      foreach (Register reg, nonArrays){
         caseTemplate.setFileName( GROUP_READ_CASE);
         caseTemplate.setParameter("REG_NAME", CodeHeader::constantIdentifier("modbus reg", groupName, reg.name, ""));
         caseTemplate.setParameter("REG_VALUE", QString::number(i1));
         cases += caseTemplate.toString();
         i1++;
      }
   }

   if( !cases.isEmpty()){
      switchTemplate.setFileName( GROUP_READ_SWITCH);
      switchTemplate.setParameter("CASE", cases);
      switchTemplate.setParameter("DEFAULT", "-1");
   }

   functionBody += switchTemplate.toString() + "\n   return -1;\n";
   return CodeSource::functionDefinition("static int", "_GetRegisterMapping", "EModbusRegNum R", "return mapping of discontinuous register number to contiuous array", functionBody);
}

//------------------------------------------------------------------------------
//  Helper function save executive
//------------------------------------------------------------------------------

QString CodeGroup::saveExecutive(RegisterGroup group, const int groupOffset)
// helper function for group generation - create save executive
{
QString           functionBody;
int               regCount;
Register          reg;
SourceTemplate    sourceTemplate;
QStringList       slist;
QStringList       LGroups;
QStringList       GGroups;       //getter groups - same getters
QVector<int>      GGroupsCount; //count of same getter/setter groups
QStringList       SGroups;       //getter groups - same getters
QVector<int>      SGroupsCount;       //count of same getter
QString           ConcatString;
int               i1;
bool              inti;
int               i;

   i1 = groupOffset;
   regCount = group.Count();
   //first fill GSGroups
   for( int j = 0; j < regCount; j++){
      reg = group.item.at(j);
      if( !(reg.read && reg.write && reg.registerType == REGISTER_REGISTER && reg.dataName.toLower() != KEYWORD_GLOBAL)){
         continue;
      }
      //locals
      if( !reg.dataGSLocals.isEmpty()){
         slist = reg.dataGSLocals.split(";");
         for( int k = 0; k < slist.count(); k++){
            if( !LGroups.contains(slist[ k])){
               LGroups       << slist[ k];
            }
         }
      }
      //getters
      if( !reg.dataGetter.isEmpty()){
         slist = reg.dataGetter.split(";");
         for( int k = 0; k < slist.count(); k++){
            if( slist[k].contains(QRegExp(" i[^\\w]"))){
               break;
            }
            if( GGroups.contains( slist[ k])){
               i = GGroups.indexOf( slist[ k]);
               GGroupsCount.replace( i, GGroupsCount[ i] + 1);
            } else {
               GGroups       << slist[ k];
               GGroupsCount  << 1;
            }
         }
      }
      //setters
      if( !reg.dataSetter.isEmpty()){
         slist = reg.dataSetter.split(";");
         for( int k = 0; k < slist.count(); k++){
            if( slist[k].contains(QRegExp(" i[^\\w]"))){
               for( int m = k-1; m >= 0; m--){
                  if( (i = SGroups.indexOf(slist[ m]))>=0){
                     SGroups.removeAt(i);
                     SGroupsCount.remove(i);
                  }
               }
               continue;
            }
            if( SGroups.contains( slist[ k])){
               i = SGroups.indexOf( slist[ k]);
               SGroupsCount.replace( i, SGroupsCount[ i] + 1);
            } else {
               SGroups       << slist[ k];
               SGroupsCount  << 1;
            }
         }
      }
   }

   //next create source code
   functionBody = LGroups.join(";\n");
   if( !functionBody.isEmpty()){
      functionBody += ";\n\n";
   }
   inti = false;
   QList<Registers> arrayGroups = group.item.OfArrayType();
   Registers nonArrays = group.item.OfNonArrayType();

   foreach (Registers regs, arrayGroups) {
      if( !inti){
         inti = true;
         functionBody.insert(0, "int i;\n");
      }
      sourceTemplate.setFileName( GROUP_SAVE_ARRAY);
      sourceTemplate.setParameter( "INDEX1", QString::number(i1/8));
      sourceTemplate.setParameter( "INDEX2", QString::number(i1));
      sourceTemplate.setParameter( "INDEX3", QString::number(i1%8));
      sourceTemplate.setParameter( "INDEX4", QString::number(8-(i1%8)));
      reg = regs.first();
      if( !reg.dataGetter.isEmpty() || !reg.dataSetter.isEmpty()){
         int               i;
         bool              localI;
         slist = reg.dataGetter.split(";");
         ConcatString = "";
         for( int k = 0; k < slist.count(); k++){
            i = GGroups.indexOf( slist[ k]);
            if( i < 0){
               continue;
            }
            if( GGroupsCount[ i] > 0){
               ConcatString += "   " + slist[ k] + ";\n";
               GGroupsCount.replace(i, -1);
            }
         }
         sourceTemplate.setParameter( "GETTER", ConcatString);
         slist = reg.dataGetter.split(";");
         ConcatString = "";
         localI = false;
         for( int k = 0; k < slist.count(); k++){
            if( !slist[ k].contains(QRegExp(" i[^\\w]")) && !localI){
               continue;
            }
            localI = true;
            ConcatString += "        " + slist[ k] + ";\n";
         }
         sourceTemplate.setParameter( "LOCAL_GETTER", ConcatString);

         slist = reg.dataSetter.split(";");
         ConcatString = "";
         for( int k = 0; k < slist.count(); k++){
            i = SGroups.indexOf( slist[ k]);
            if( i < 0){
               continue;
            }
            if( SGroupsCount[ i] > 0){
               SGroupsCount.replace(i, SGroupsCount[ i] - regs.count()-1 );
            }
            if( SGroupsCount[ i] == 0){
               ConcatString += "   " + slist[ k] + ";\n";
               SGroupsCount.replace(i, -1);
            }
         }
         sourceTemplate.setParameter( "SETTER", ConcatString);
         slist = reg.dataSetter.split(";");
         ConcatString = "";
         localI = false;
         for( int k = 0; k < slist.count(); k++){
            ConcatString += "        " + slist[ k] + ";\n";
            if( slist[ k].contains(QRegExp(" i[^\\w]"))){
               localI = true;
               break;
            }
         }
         if( localI){
            sourceTemplate.setParameter( "LOCAL_SETTER", ConcatString);
         }
      }

      sourceTemplate.setParameter( "COUNT", QString::number(regs.count()));
      i1 += regs.count();

      sourceTemplate.setParameter( "REG_NAME", reg.ArrayName());
      sourceTemplate.setParameter( "GROUP_CCNAME", OName::toCamelCase(group.name));
      if( reg.dataName.toLower() == KEYWORD_GLOBAL){
         sourceTemplate.setParameter( "VALUE", CodeSource::globalVariable(group.name + " " + reg.name));
      } else if( reg.dataName.isEmpty()){
         sourceTemplate.setParameter( "VALUE", "// TODO");
      } else {
         sourceTemplate.setParameter( "VALUE", reg.dataName);
      }
      functionBody += sourceTemplate.toString();
   }

   foreach (Register reg, nonArrays){
      sourceTemplate.setFileName( GROUP_SAVE);
      sourceTemplate.setParameter( "INDEX1", QString::number(i1/8));
      sourceTemplate.setParameter( "INDEX2", QString::number(i1));
      sourceTemplate.setParameter( "INDEX3", QString::number(7-i1%8));
      if( !reg.dataGetter.isEmpty() || !reg.dataSetter.isEmpty()){
         slist = reg.dataGetter.split(";");
         ConcatString = "";
         for( int k = 0; k < slist.count(); k++){
            i = GGroups.indexOf( slist[ k]);
            if( GGroupsCount[ i] > 0){
               ConcatString += "   " + slist[ k] + ";\n";
               GGroupsCount.replace(i, -1);
            }
         }
         sourceTemplate.setParameter( "GETTER", ConcatString);

         slist = reg.dataSetter.split(";");
         ConcatString = "";
         for( int k = 0; k < slist.count(); k++){
            i = SGroups.indexOf( slist[ k]);
            if( SGroupsCount[ i] <= 1 && SGroupsCount[ i] >= 0){
               ConcatString += "   " + slist[ k] + ";\n";
               SGroupsCount.replace(i, -1);
            } else {
               SGroupsCount.replace(i, SGroupsCount[ i] - 1);
            }
         }
         sourceTemplate.setParameter( "SETTER", ConcatString);
      }
      i1++;

      sourceTemplate.setParameter( "REG_NAME", reg.name);
      sourceTemplate.setParameter( "GROUP_CCNAME", OName::toCamelCase(group.name));
      if( reg.dataName.toLower() == KEYWORD_GLOBAL){
         sourceTemplate.setParameter( "VALUE", CodeSource::globalVariable(group.name + " " + reg.name));
      } else if( reg.dataName.isEmpty()){
         sourceTemplate.setParameter( "VALUE", "// TODO");
      } else {
         sourceTemplate.setParameter( "VALUE", reg.dataName);
      }
      functionBody += sourceTemplate.toString();
   }
   return functionBody;
}

QString CodeGroup::arrayTypeRegistersRead(QList<Registers>  regs, QString groupName)
// get read for array type registers
{
SourceTemplate groupIfTemplate;
Registers tmp;
Register first;
Register last;
QString tmpString;
QString ConcatString;
QString body;
   for(int i = 0; i < regs.count(); i++){
      tmp = regs[i];
      first = tmp.first();
      last = tmp.last();

      groupIfTemplate.setFileName( GROUP_READ_ARRAY);
      groupIfTemplate.setParameter("FIRST", CodeHeader::constantIdentifier("modbus reg", groupName, first.name, ""));
      groupIfTemplate.setParameter("LAST", CodeHeader::constantIdentifier("modbus reg", groupName, last.name, ""));
      if( first.registerType == REGISTER_STRING_BUFFER){
         groupIfTemplate.setParameter("VALUE", "WriteValuesBuffer.CharacterBuffer[ i]");
      } else if( first.dataName.toLower() == KEYWORD_GLOBAL){
         groupIfTemplate.setParameter("VALUE", CodeSource::globalVariable(groupName + " " + first.name));
      } else if( first.dataName.isEmpty()){
         groupIfTemplate.setParameter("VALUE", "/* TODO */0");
      } else {
         tmpString = first.dataName;
         if( first.registerType == REGISTER_STRING){
            groupIfTemplate.setParameter("VALUE", "1");
            tmpString = "      strncpy( (void *)WriteValuesBuffer.CharacterBuffer, " + tmpString + ", " + (first.stringLen.isEmpty()? "/*TODO*/1" : first.stringLen) + ");\n";
         } else {
            groupIfTemplate.setParameter("VALUE", tmpString);
            tmpString = "";
         }
      }
      ConcatString = "";
      if( !first.dataGetter.isEmpty()){
         QStringList slist = first.dataGetter.split(";");
         for( int k = 0; k < slist.count(); k++){
            ConcatString += "      " + slist[ k] + ";\n";
         }
      }
      tmpString = ConcatString + tmpString;
      groupIfTemplate.setParameter("GETTER", tmpString);
      body += groupIfTemplate.toString();
   }

   return body;
}


QString CodeGroup::nonArrayTypeRegistersRead(Registers regs, QString groupName)
// get read for non array type registers
{

SourceTemplate groupCaseTemplate;
SourceTemplate groupSwitchTemplate;
QString groupCases;
Register reg;
QString tmpString;
QString ConcatString;

   for(int i = 0; i < regs.count(); i++){
      reg = regs[i];

      groupCaseTemplate.setFileName( GROUP_READ_CASE);
      groupCaseTemplate.setParameter("REG_NAME", CodeHeader::constantIdentifier("modbus reg", groupName, reg.name, ""));
      tmpString = "";
      if( reg.registerType == REGISTER_STRING_BUFFER){
         groupCaseTemplate.setParameter("VALUE", "WriteValuesBuffer.CharacterBuffer[ i]");
      } else if( reg.dataName.toLower() == KEYWORD_GLOBAL){
         groupCaseTemplate.setParameter("REG_VALUE", CodeSource::globalVariable(groupName + " " + reg.name));
      } else if( reg.dataName.isEmpty()){
         groupCaseTemplate.setParameter("REG_VALUE", "/* TODO */0");
      } else {
         if( reg.registerType == REGISTER_STRING){
            groupCaseTemplate.setParameter("REG_VALUE", "1");
            tmpString = "         strncpy( (void *)WriteValuesBuffer.CharacterBuffer, " + reg.dataName  + ", " + (reg.stringLen.isEmpty()? "/*TODO*/1" : reg.stringLen) + ");\n";
         } else {
            groupCaseTemplate.setParameter("REG_VALUE", reg.dataName);
            tmpString = "";
         }
      }
      ConcatString = "";
      if( !reg.dataGetter.isEmpty() && reg.dataName.toLower() != KEYWORD_GLOBAL){
         QStringList slist = reg.dataGetter.split(";");
         for( int k = 0; k < slist.count(); k++){
            ConcatString += "         " + slist[ k] + ";\n";
         }
      }
      tmpString = ConcatString + tmpString;
      groupCaseTemplate.setParameter("GETTER", tmpString);
      groupCases += groupCaseTemplate.toString();
   }

   if( !groupCases.isEmpty()){
      groupSwitchTemplate.setFileName( GROUP_READ_SWITCH);
      groupSwitchTemplate.setParameter("CASE", groupCases);
      groupSwitchTemplate.setParameter("DEFAULT", QString::number(0));
   }
   return groupSwitchTemplate.toString();
}
