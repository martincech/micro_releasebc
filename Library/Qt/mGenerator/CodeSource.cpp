//******************************************************************************
//
//   CodeSource.c     Source code generator
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "CodeSource.h"
#include "Parse/nameTransformation.h"

//------------------------------------------------------------------------------
//  Include local file
//------------------------------------------------------------------------------

QString CodeSource::includeFileLocal( QString filePath, QString fileName)
// generate local include statement for <filePath> and <fileName>
{
QString text;

   if( filePath.isEmpty()){
      text +=  QString( "#include \"%1.h\"\n").arg( fileName);
   } else {
      text +=  QString( "#include \"%1/%2.h\"\n").arg( filePath).arg( fileName);
   }
   return( text);
}

//------------------------------------------------------------------------------
//  Include global file
//------------------------------------------------------------------------------

QString CodeSource::includeFileGlobal( QString filePath, QString fileName)
// generate global include statement for <filePath> and <fileName>
{
QString text;

   if( filePath.isEmpty()){
      text +=  QString( "#include <%1.h>\n").arg( fileName);
   } else {
      text +=  QString( "#include <%1/%2.h>\n").arg( filePath).arg( fileName);
   }
   return( text);
}

//------------------------------------------------------------------------------
//  Function definition
//------------------------------------------------------------------------------
QString CodeSource::functionDefinition( QString returnType, QString name, QString parameters, QString comment, QString body)
{
   return functionDefinition(returnType, name, QStringList(parameters), comment, body);
}

QString CodeSource::functionDefinition( QString returnType, QString name, const char * parameters, QString comment, QString body)
{
   return functionDefinition(returnType, name, QString(parameters), comment, body);
}

QString CodeSource::functionDefinition( QString returnType, QString name, QStringList parameters, QString comment, QString body)
// generate function with body
{
QString text;

   text = returnType + " " + name + "( ";
   if( parameters.count() == 0 || (parameters.count() == 1 && parameters.first() == "")){
      text += "void";
   } else {
      for( int i = 0; i < parameters.count() - 1; i++){
         text += parameters[ i] + ", ";
      }
      text += parameters.last();
   }
   text += ")\n// " + comment + "\n{\n";
   text += body;
   text += "}\n\n";
   return text;
}

//------------------------------------------------------------------------------
//  Function declaration
//------------------------------------------------------------------------------
QString CodeSource::functionDeclaration(QString returnType, QString name, QString parameters, QString comment)
{
   return functionDeclaration(returnType, name, QStringList(parameters), comment);
}

QString CodeSource::functionDeclaration(QString returnType, QString name, const char * parameters, QString comment)
{
   return functionDeclaration(returnType, name, QString(parameters), comment);
}

QString CodeSource::functionDeclaration(QString returnType, QString name, QStringList parameters, QString comment)
// generate function declaration
{
QString text;

   text = returnType + " " + name + "( ";
   if( parameters.count() == 0 || (parameters.count() == 1 && parameters.first() == "")){
      text += "void";
   } else {
      for( int i = 0; i < parameters.count() - 1; i++){
         text += parameters[ i] + ", ";
      }
      text += parameters.last();
   }
   text += ");\n// " + comment + "\n\n";
   return text;
}

//------------------------------------------------------------------------------
//  Function call
//------------------------------------------------------------------------------
QString CodeSource::functionCall( QString name, QString     parameters)
{
   return functionCall( name, QStringList(parameters));
}

QString CodeSource::functionCall( QString name, const char *parameters)
{
   return functionCall( name, QString(parameters));
}

QString CodeSource::functionCall( QString name, QStringList parameters)
// generate function call
{
QString text;

   text = name;
   text += "(";

   if( parameters.count() != 0 && parameters.first() != ""){
      text += " ";
      for( int i = 0; i < parameters.count() - 1; i++){
         text += parameters[ i] + ", ";
      }
      text += parameters.last();
   }
   text += ");";
   return text;
}

//------------------------------------------------------------------------------
//  Global variable
//------------------------------------------------------------------------------
QString CodeSource::globalVariableDefinition( QString type, QString name, QString defaultValue, QString description)
// generate global variable definition:  type _CamelCaseName; //description
{
QString text;

   text = "static ";
   if( type.isEmpty()){
      text += "int";
   } else {
      text += type;
   }
   text += "   _" + OName::toCamelCase(OName::fromUpperCase(name));
   text += " = "+defaultValue+";";
   if( description.isEmpty()){
      text += "\n";
   } else {
      text += " // "+ description + "\n";
   }
   return text;
}

QString CodeSource::globalVariable( QString name)
// generate access string to global variable
{
QString text;
   text = "_" + OName::toCamelCase(OName::fromUpperCase(name));
   return text;
}
