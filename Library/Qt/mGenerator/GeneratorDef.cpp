#include "GeneratorDef.h"


void Register::IndexReplacement()
{
   dataGetter.replace(QRegExp("\\[.*\\]"), "[ i]");
   dataGetter.replace(QRegExp("[ ]+\\[ i\\]"), " i");
   dataSetter.replace(QRegExp("\\[.*\\]"), "[ i]");
   dataSetter.replace(QRegExp("[ ]+\\[ i\\]"), " i");
   dataName.replace(QRegExp("\\[.*\\]"), "[ i]");
   dataName.replace(QRegExp("[ ]+\\[ i\\]"), " i");
}

ERegisterRWType Register::Type()
{
   if(read && !write){
      return REGISTER_R;
   }
   if(write && !read){
      return REGISTER_W;
   }
   return REGISTER_RW;
}

QStringList Register::Includes()
{
   if(dataFile.isEmpty()){
      return QStringList();
   }
   return dataFile.split(";");
}

Registers Registers::Exclude(Registers other) const
{
Registers regList;

   foreach (Register reg, *this) {
      if(!other.contains(reg)){
         regList.append(reg);
      }
   }
   return regList;
}

QString Register::ArrayName()
{
QStringList stringList;
bool        TwoNumbers;

   if(!this->arrayType){
      return name;
   }

   stringList = name.split("_");
   stringList.removeLast();
   stringList.last().toInt(&TwoNumbers);
   if( TwoNumbers){
      stringList.removeLast();
   }
   return stringList.join("_");
}

RegisterGroup::RegisterGroup()
{
   bufferedSubgroupsCreated = false;
}

RegisterGroup::~RegisterGroup()
{
}

int RegisterGroup::Count() const
{
   return item.count();
}


const QHash<QString, RegisterGroup> *RegisterGroup::BufferedSubgroups()
{
int                  regCount;
Register             reg;
RegisterGroup        subGroup;
QString              subGroupName;
QString              commandName;

   if(bufferedSubgroupsCreated){
      return &bufferedSubgroups;
   }

   regCount = item.count();
   for( int j = 0; j < regCount; j++){
      reg = item[j];
      if( (reg.registerType == REGISTER_COMMAND) && (reg.name.toLower().endsWith("save") || reg.name.toLower().endsWith("start"))){
         commandName = reg.name;
         subGroup = *this;
         subGroup.item.clear();
         if( reg.name.indexOf("_") != -1){
            QStringList slist = reg.name.split("_");
            slist.removeLast();
            subGroupName = slist.join("_");
            for(int i = 0; i < regCount; i++){
               reg = item[i];
               if( (reg.write && reg.registerType == REGISTER_REGISTER && reg.name.startsWith(subGroupName))){
                  subGroup.item.append( reg);
               }
            }
         }
         if(!subGroup.item.isEmpty()){
            bufferedSubgroups.insert(commandName, subGroup);
         }
      }
   }

   bufferedSubgroupsCreated = true;
   return &bufferedSubgroups;
}

bool RegisterGroup::BufferedSubgroupsContainsReg(Register &reg)
{
QString name;

   if(!BufferedSubgroups()->isEmpty()){
      QList<QString> keys = BufferedSubgroups()->keys();
      for(int i = 0; i < keys.count(); i++){
         QStringList slist = keys[i].split("_");
         slist.removeLast();
         name = slist.join("_");

         if(reg.name.contains(name) && BufferedSubgroups()->value(keys[i]).item.contains(reg)){
            return true;
         }
      }
   }
   return false;
}

void RegisterGroup::IndexReplacement()
{
int         regCount;
Register    reg;

   regCount = item.count();
   for(int j = 0; j < regCount; j++){
      reg = item.at(j);
      reg.IndexReplacement();
      item.replace(j, reg);
   }
}

QStringList RegisterGroup::Includes() const
{
int         regCount;
Register    reg;
QStringList includes;

   regCount = item.count();
   for(int j = 0; j < regCount; j++){
      reg = item.at(j);
      includes  << reg.Includes();
   }

   includes.removeDuplicates();
   includes.replaceInStrings(".h", "");
   includes.replaceInStrings(QRegExp("[ ]*"), "");
   includes.removeAll("");
   return includes;
}

Register RegisterGroup::BufferedFirst()
{
Register reg;
int num = INT_MAX;
QHash<QString, RegisterGroup>::const_iterator it;

   it = BufferedSubgroups()->begin();
   while (it != BufferedSubgroups()->end()) {
      foreach (Register r, it.value().item) {
         if(num > r.num){
            num = r.num;
            reg = r;
         }
      }
      it++;
   }
   return reg;
}

Register RegisterGroup::BufferedLast()
{
Register reg;
int num = INT_MIN;
QHash<QString, RegisterGroup>::const_iterator it;

   it = BufferedSubgroups()->begin();
   while (it != BufferedSubgroups()->end()) {
      foreach (Register r, it.value().item) {
         if(num < r.num){
            num = r.num;
            reg = r;
         }
      }
      it++;
   }
   return reg;
}

Registers Registers::OfType(int rwType, bool includeArrayTypes, bool includeGlobal) const
{
Registers typeList;

   foreach (Register reg, *this) {
      if((reg.Type() & rwType) && (includeArrayTypes? true : !reg.arrayType) && (includeGlobal? true : reg.dataName.toLower() != KEYWORD_GLOBAL)){
         typeList.append(reg);
      }
   }
   return typeList;
}

Registers Registers::OfType(ERegisterType type, bool includeArrayTypes, bool includeGlobal) const
{
Registers typeList;

   foreach (Register reg, *this) {
      if(reg.registerType == type && (includeArrayTypes? true : !reg.arrayType) && (includeGlobal? true : reg.dataName.toLower() != KEYWORD_GLOBAL)){
         typeList.append(reg);
      }
   }
   return typeList;
}

Registers Registers::OfType(ERegisterType type, int rwType, bool includeArrayTypes, bool includeGlobal ) const
{
Registers typeList;

   foreach (Register reg, *this) {
      if(reg.registerType == type && (reg.Type() & rwType) && (includeArrayTypes? true : !reg.arrayType) && (includeGlobal? true : reg.dataName.toLower() != KEYWORD_GLOBAL)){
         typeList.append(reg);
      }
   }
   return typeList;
}

QList<Registers> Registers::ArrayTypeOfType(ERegisterType type, int rwType, bool includeGlobal ) const
{
Registers all = OfType(type, rwType, true, includeGlobal);

   return all.OfArrayType();
}

Registers Registers::OfNonArrayType() const
{
Registers ret;

   foreach (Register reg, *this) {
      if(!reg.arrayType){
         ret.append(reg);
      }
   }
   return ret;
}

QList<Registers> Registers::OfArrayType() const
{
Registers arrayTypes;
Registers ret;
QList<Registers> retList;

   foreach (Register reg, *this) {
      if(reg.arrayType){
         arrayTypes.append(reg);
      }
   }

   if(arrayTypes.isEmpty()){
      return retList;
   }
   int type = arrayTypes.first().arrayType;
   foreach (Register reg, arrayTypes) {
      if(type == reg.arrayType){
         ret.append(reg);
         continue;
      }
      retList.append(ret);
      ret.clear();
      ret.append(reg);
      type = reg.arrayType;
   }
   if(!ret.isEmpty()){
      retList.append(ret);
   }
   return retList;
}

Registers Registers::OfTheSameArrayType(Register iReg) const
{
Registers typeList;

   if(!iReg.arrayType){return typeList;}

   foreach (Register reg, *this) {
      if(reg.arrayType && reg.arrayType == iReg.arrayType){
         typeList.append(reg);
      }
   }
   return typeList;
}

Registers Registers::GlobalRegisters() const
{
Registers typeList;

   foreach (Register reg, *this) {
      if(reg.dataName.toLower() == KEYWORD_GLOBAL){
         typeList.append(reg);
      }
   }
   return typeList;
}

Registers RegisterGroup::RegistersOfType(int rwType, bool includeArrayTypes, bool includeGlobal) const
{
   return item.OfType(rwType, includeArrayTypes, includeGlobal);
}
Registers RegisterGroup::RegistersOfType(ERegisterType type, bool includeArrayTypes, bool includeGlobal) const
{
   return item.OfType(type, includeArrayTypes, includeGlobal);
}

Registers RegisterGroup::RegistersOfType(ERegisterType type, int rwType,  bool includeArrayTypes, bool includeGlobal) const
{
   return item.OfType(type, rwType, includeArrayTypes, includeGlobal);
}

Registers RegisterGroup::RegistersOfTheSameArrayType(Register iReg) const
{
   return item.OfTheSameArrayType(iReg);
}

QList<Registers> RegisterGroup::ArrayTypeRegistersOfType(ERegisterType type, int rwType, bool includeGlobal ) const
{
   return item.ArrayTypeOfType(type, rwType, includeGlobal);
}

Registers RegisterGroup::GlobalRegisters() const
{
   return item.GlobalRegisters();
}
