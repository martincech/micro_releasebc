//******************************************************************************
//
//   Bat2SmsRemote.h   Bat2 remote access through COM port
//   Version 1.0        (c) Veit Electronics
//
//******************************************************************************

#ifndef __BAT2_SMS_REMOTE_H__
#define __BAT2_SMS_REMOTE_H__

#include "Crt/crtdump.h"
#include "Uart/WinUart.h"
#include "Bat2RemoteInterface.h"
#include "Bat2/Remote/Threads.h"
#include <QThread>
#include <QRunnable>
#include <QTimer>

class Bat2SmsRemote : public QObject, public TBat2RemoteInterface, public WinUart
{
   Q_OBJECT
public:
   Bat2SmsRemote(char *Phone, CrtDump *log, QObject *parent = 0);
   Bat2SmsRemote(char *Phone, QObject *parent = 0);
   // Constructor
   virtual ~Bat2SmsRemote();
   // destructor

   bool connected();
   bool inited();
   QAbstractItemModel *channels();

signals:
   void start();
private slots:
   void initDone();

protected :
   CrtDump *_logger;
   QThread *workerThread;
   SmsChannelSimpleExecutive  *smsChannelThread;
   InitThread *init;
   TYesNo SendCommandAndReceiveReply( TRcCmd *Cmd, int CmdSize, TRcReply *Reply, int *ReplySize, int DesiredReplySize);
   // Send command and receive reply

   TYesNo SendCommand( TRcCmd *Cmd, int CmdSize);
   // Send command

   TYesNo ReceiveReply( TRcReply *Reply, int *ReplySize);
   // Receive reply

private:
   Bat2SmsRemote( const Bat2SmsRemote &){}
   char phone[20];
   TRcCmd *Cmd;
   int CmdSize;
   TRcReply *Reply;
   int *ReplySize;
   TYesNo result;
   int DesiredReplySize;

friend class SendAndReceiveEx;
friend class SendCommandEx;
friend class ReceiveReplyEx;

};

#include "System/System.h"
#include "System/SysTimer.h"
class SendAndReceiveEx : public QRunnable
{
public:
   SendAndReceiveEx( Bat2SmsRemote *master);
   virtual void run();
private:
   Bat2SmsRemote *master;
   CrtDump *_logger;
};

class SendCommandEx : public QRunnable
{
public:
   SendCommandEx( Bat2SmsRemote *master);
   virtual void run();
private:
   Bat2SmsRemote *master;
};

class ReceiveReplyEx : public QRunnable
{
public:
   ReceiveReplyEx( Bat2SmsRemote *master);
   virtual void run();
private:
   Bat2SmsRemote *master;
   dword timer;
};
#endif // __BAT2_SMS_REMOTE_H__
