//******************************************************************************
//
//   AppMain.c    Bat2 PC terminal main
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Hardware.h"
#include "System/System.h"
#include "uSimulator/AppMain.h"
#include "Graphic/Graphic.h"
#include "Fonts.h"
#include "Console/conio.h"
#include "Config/Config.h"                // Project configuration
#include "Scheduler/WeighingScheduler.h"  // Weighing scheduler executive
#include "Weighing/pWeighing.h"           // PlatformWeighing
#include "Platform/Platform.h"            // Platform
#include "Menu/Menu.h"                    // Main menu
#include "Menu/MenuExit.h"                // Main menu
#include "Menu/Screen.h"                  // Display screen
#include "Power/Power.h"                  // Power management
#include "Memory/Nvm.h"
#include "Str.h"                          // Project strings
#include "Multitasking/Tasks.h"
#include "Multitasking/Multitasking.h"
#include "Action/ActionRemote.h"
#include "Config/State.h"
#include "Log/Log.h"
#include "Debug/uDebug.h"
#include <QThread>
//******************************************************************************
// Main
//******************************************************************************

void AppMain( void)
{
#ifdef OPTION_REMOTE
UDateTimeGauge DateTime;
   if(ActionRemoteTimeGet( &DateTime)) {
      SysDateTimeSet( DateTime);
   }

   ArchiveRemoteLoad();
   SampleRemoteLoad();
   ContextRemoteLoad();

   ConfigRemoteLoad();
   WeighingPlanListRemoteLoad();
   ContactListRemoteLoad();
   PredefinedListRemoteLoad();
   CurveListRemoteLoad();
   CorrectionListRemoteLoad();
   StateRemoteLoad();
#endif

   MenuExitClear();
   AppInit();
   // main menu :

   while(SysScheduler() != K_SHUTDOWN)
   {
   }

#ifdef OPTION_REMOTE
      ConfigRemoteSave();
      ContactListRemoteSave();
      WeighingPlanListRemoteSave();

      PredefinedListRemoteSave();
      CurveListRemoteSave();
      CorrectionListRemoteSave();
#endif
   SysScheduler();
} // AppMain
