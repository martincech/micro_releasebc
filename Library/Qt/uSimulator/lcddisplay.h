//*****************************************************************************
//
//   LcdDisplay.h    LCD display simulator
//   Version 1.0     (c) VymOs
//
//*****************************************************************************

#ifndef LCDDISPLAY_H
#define LCDDISPLAY_H

#include <QWidget>
#include <QPixmap>

#include "Graphic/Gpu.h"

//-----------------------------------------------------------------------------
// LCD display
//-----------------------------------------------------------------------------

class LcdDisplay : public QWidget
{
   Q_OBJECT

public:
   explicit LcdDisplay( QWidget *parent = 0);
   // constructor

   ~LcdDisplay();
   // destructor

   void initializeBuffer();
   // Initialize buffer

   void clearBuffer();
   // Clear buffer

   void updateBuffer();
   // Update buffer

   QSize sizeHint() const;
   // Widget size

public slots :
   void copy();
   // Copy display to clipboard

protected :
   QPixmap *mPixmap;

    void paintEvent( QPaintEvent *event);
signals:
   void repaintSignal();

    // Redraw
}; // LcdDisplay

#endif // LCDDISPLAY_H
