//*****************************************************************************
//
//   Nvm.cpp       Nonvolatile memory simulator for Qt
//   Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#include "Memory/Nvm.h"
#include <fstream>
#include <string.h>

#define FILE_NAME_MAX         1024

// memory array :
static byte _NvmMemory[ NVM_SIZE];
static char FileName[FILE_NAME_MAX + 1];

using namespace std;
//------------------------------------------------------------------------------
//  Setup
//------------------------------------------------------------------------------

TYesNo NvmSetup(char *Ptr)
// Setup NVM from <FileName>
{
   if(strlen(Ptr) > FILE_NAME_MAX) {
      FileName[0] = '\0';
      return NO;
   }
   strcpy(FileName, Ptr);
   memset( _NvmMemory, 0xFF, NVM_SIZE);          // default contents
   ifstream file( FileName, fstream::in | fstream::binary);
   if( !file.is_open()){
      return( NO);
   }

   file.read((char *)_NvmMemory, NVM_SIZE);
   int size = (int)file.gcount();
   file.close();
   return( size == NVM_SIZE);
} // NvmSetup

//------------------------------------------------------------------------------
//  Shutdown
//------------------------------------------------------------------------------

void NvmShutdown()
// Save NVM to <FileName>
{
   ofstream file( FileName, fstream::out | fstream::trunc | fstream::binary);
   if( !file.is_open()){
      return;
   }
   file.write( (const char *)_NvmMemory, NVM_SIZE);
   file.close();
   return;
} // NvmShutdown


//------------------------------------------------------------------------------
//  format nvm
//------------------------------------------------------------------------------

void NvmFormat(void)
{
   NvmFill(0, 0xff, NVM_SIZE);
}

//------------------------------------------------------------------------------
//  Initialization
//------------------------------------------------------------------------------

void NvmInit( void)
// Initialize
{
} // NvmInit

//------------------------------------------------------------------------------
//  Write byte
//------------------------------------------------------------------------------

void NvmByteWrite( TNvmAddress Address, byte Value)
// Write <Value> at <Address>
{
   if( Address > NVM_SIZE){
      return;
   }
   _NvmMemory[ Address] = Value;
} // NvmByteWrite

//------------------------------------------------------------------------------
//  Write word
//------------------------------------------------------------------------------

void NvmWordWrite( TNvmAddress Address, word Value)
// Write <Value> at <Address>
{
   if( Address > NVM_SIZE){
      return;
   }
   *((word *)&_NvmMemory[ Address]) = Value;
} // NvmWordWrite

//------------------------------------------------------------------------------
//  Write dword
//------------------------------------------------------------------------------

void NvmDwordWrite( TNvmAddress Address, dword Value)
// Write <Value> at <Address>
{
   if( Address > NVM_SIZE){
      return;
   }
   *((dword *)&_NvmMemory[ Address]) = Value;
} // NvmDwordWrite

//------------------------------------------------------------------------------
//  Read byte
//------------------------------------------------------------------------------

byte NvmByteRead( TNvmAddress Address)
// Returns value from <Address>
{
   if( Address > NVM_SIZE){
      return( 0);
   }
   return( _NvmMemory[ Address]);
} // NvmByteRead

//------------------------------------------------------------------------------
//  Read word
//------------------------------------------------------------------------------

word NvmWordRead( TNvmAddress Address)
// Returns value from <Address>
{
   if( Address > NVM_SIZE){
      return( 0);
   }
   return( *((word *)&_NvmMemory[ Address]));
} // NvmWordRead

//------------------------------------------------------------------------------
//  Read dword
//------------------------------------------------------------------------------

dword NvmDwordRead( TNvmAddress Address)
// Returns value from <Address>
{
   if( Address > NVM_SIZE){
      return( 0);
   }
   return( *((dword *)&_NvmMemory[ Address]));
} // NvmDwordRead

//------------------------------------------------------------------------------
//  Save
//------------------------------------------------------------------------------

TYesNo NvmSave( TNvmAddress Address, const void *Data, int Size)
// Save <Data> with <Size> at <Address>
{
   if( Size + Address > NVM_SIZE){
      return( NO);
   }
   memcpy( &_NvmMemory[ Address], Data, Size);
   return( YES);
} // NvmSave

//------------------------------------------------------------------------------
//  Load
//------------------------------------------------------------------------------

void NvmLoad( TNvmAddress Address, void *Data, int Size)
// Load <Data> with <Size> from <Address>
{
   if( Size + Address > NVM_SIZE){
      return;
   }
   memcpy( Data, &_NvmMemory[ Address], Size);
} // NvmLoad

//------------------------------------------------------------------------------
//  Fill
//------------------------------------------------------------------------------

TYesNo NvmFill( TNvmAddress Address, byte Pattern, int Size)
// Write <Pattern> with <Size> at <Address>
{
   if( Size + Address > NVM_SIZE){
      return( NO);
   }
   memset( &_NvmMemory[ Address], Pattern, Size);
   return( YES);
} // NvmFill

//------------------------------------------------------------------------------
//  Match
//------------------------------------------------------------------------------

TYesNo NvmMatch( TNvmAddress Address, const void *Data, int Size)
// Compare <Data> with <Size> at <Address>
{
   if( Size + Address > NVM_SIZE){
      return( NO);
   }
   return( memequ( &_NvmMemory[ Address], Data, Size));
} // NvmMatch

//------------------------------------------------------------------------------
//  Sum
//------------------------------------------------------------------------------

TNvmSum NvmSum( TNvmAddress Address, int Size)
// Sum NVM contents at <Address> with <Size>
{
TNvmSum Sum;

   if( Size + Address > NVM_SIZE){
      return( 0);
   }
   Sum = 0;
   while( Size--){
      Sum += _NvmMemory[ Address];
      Address++;
   }
   return( Sum);
} // NvmSum

//------------------------------------------------------------------------------
//  Move
//------------------------------------------------------------------------------

void NvmMove( TNvmAddress ToAddress, TNvmAddress FromAddress, int Size)
// Move data <FromAddress> to <ToAddress> with <Size>. Check for overlapping
{
   memmove( &_NvmMemory[ ToAddress], &_NvmMemory[ FromAddress], Size);
} // NvmMove

//------------------------------------------------------------------------------
//  Commit
//------------------------------------------------------------------------------

void NvmCommit( void)
// Permanently save data
{
} // NvmCommit
