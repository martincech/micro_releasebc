//*****************************************************************************
//
//    port.h             Modbus port to Kinetis CortexMx target
//    Version 1.0        (c) Veit Electronics
//
//*****************************************************************************

#ifndef _PORT_H
   #ifndef _MANAGED
      #define _PORT_H
   #endif
   
#ifndef _MANAGED
/* ----------------------- Platform includes --------------------------------*/
#include "Unisys/Uni.h"
/* ----------------------- Defines ------------------------------------------*/
#define	INLINE                      inline
#define PR_BEGIN_EXTERN_C           extern "C" {
#define	PR_END_EXTERN_C             }

#define ENTER_CRITICAL_SECTION( )   vMBPortEnterCritical()
#define EXIT_CRITICAL_SECTION( )    vMBPortExitCritical()

typedef TYesNoEnum BOOL;

typedef byte UCHAR;
typedef char CHAR;

typedef uint16 USHORT;
typedef int16   SHORT;

typedef uint32 ULONG;
typedef int32    LONG;

#ifndef TRUE
#define TRUE                                    YES
#endif

#ifndef FALSE
#define FALSE                                   NO
#endif

#define MB_PORT_HAS_CLOSE	                1
#define MB_ASCII_TIMEOUT_WAIT_BEFORE_SEND_MS    2

#define assert(x)
/* ----------------------- Prototypes ---------------------------------------*/
void            vMBPortEnterCritical( void );
void            vMBPortExitCritical( void );

#endif

#ifdef _MANAGED
#undef _MANAGED
#include "port.h"
#define _MANAGED
#endif

#endif
