/* ----------------------- System includes ----------------------------------*/
#include "System/Delay.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "port.h"
#include "mb.h"
#include "mbport.h"
#define MB_FUNC_TIMERS_MAX 3

typedef struct {
   Timer timer;
   int timeout;
   int counter;
   pxMBTimerExpired expiredFn;
} TimerInfo;

static TimerInfo Timers[MB_FUNC_TIMERS_MAX];;

/* ----------------------- Start implementation -----------------------------*/

/* ----------------------- Start implementation -----------------------------*/
BOOL xMBPortTimerInit(Timer *timer, USHORT usTim1Timerout50us, pxMBTimerExpired expirationFn )
{   
TimerInfo *timerPtr = NULL;
Timer i;
  
   for(i = 0; i < MB_FUNC_TIMERS_MAX; i++){
      if(Timers[i].timer == 0){
         timerPtr = &Timers[i];
         break;
      }
   }
   if(timerPtr == NULL || usTim1Timerout50us == 0 || expirationFn == NULL){
      return FALSE;
   }
   timerPtr->timer = i+1;
   timerPtr->timeout = usTim1Timerout50us*50;
   if(timerPtr->timeout > 1000){
      timerPtr->timeout = timerPtr->timeout/1000;
   }else{
      timerPtr->timeout = 1;
   }
   timerPtr->counter = -1;
   timerPtr->expiredFn = expirationFn;
   *timer = timerPtr->timer;
   return TRUE;
}

void vMBPortTimerClose( Timer timer )
{
   if(timer >0 && timer < MB_FUNC_TIMERS_MAX){
      Timers[timer-1].timer = 0;
   }
}

void vMBPortTimerEnable( Timer timer )
{
   if(timer >0 && timer < MB_FUNC_TIMERS_MAX){
      Timers[timer-1].counter = Timers[timer-1].timeout;
   }
}

void vMBPortTimerDisable( Timer timer )
{
   if(timer >0 && timer < MB_FUNC_TIMERS_MAX){
      Timers[timer-1].counter = -1;
   }
}

void vMBPortDelay( USHORT usTimeOutMS )
{
   SysDelay(usTimeOutMS);
}


void vMBPortTimerExecute( void )
{
   for(Timer i = 0; i < MB_FUNC_TIMERS_MAX; i++){
      if(Timers[i].timer == 0){
         continue;
      }
      if(Timers[i].counter > 0){
         Timers[i].counter--;
      }

      if(Timers[i].counter == 0){
         Timers[i].expiredFn();
      }  
   }
 
}
