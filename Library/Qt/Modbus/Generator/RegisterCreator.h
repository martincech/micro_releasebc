#ifndef REGISTERCREATOR_H
#define REGISTERCREATOR_H

#include "ARegister.h"
#include "Parse/csv.h"
#include "ModelParser.h"
#include <QString>
//------------------------------------------------------------------------------
//  Abstract factory creator
//------------------------------------------------------------------------------

class RegisterCreator
{
protected:
   ModelParser *parser;
public:
   RegisterCreator(){parser = NULL;}
   virtual ARegister *CreateRegisters() = 0;
   const ArrayModel *GetModel();
   ArrayModel *CreateModel(ARegister *reg);
};

//------------------------------------------------------------------------------
//  Concrete Csv factor
//------------------------------------------------------------------------------

class CsvCreator : public RegisterCreator
{
public:
   CsvCreator( QString FileName);
   ~CsvCreator();
   ARegister *CreateRegisters();
private:
   Csv *reader;
   ARegister *registers;
   QString iFile;
};

#endif // REGISTERCREATOR_H
