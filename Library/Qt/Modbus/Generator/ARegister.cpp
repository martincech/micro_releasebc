#include "ARegister.h"
#include <QVariant>
#include "GeneratorDef.h"
#include <typeinfo>       // std::bad_cast
//------------------------------------------------------------------------------
//  Abstract register class
//------------------------------------------------------------------------------

ARegister::ERegisterType ARegister::decodeType( QString &typeString)
{
   typeString.remove(" ");
   typeString = typeString.toLower();

   if( typeString == "stringbuffer"){
      return REGISTER_STRING_BUFFER;
   }
   if( typeString == "command"){
      return REGISTER_COMMAND;
   }
   if( typeString == "string"){
      return REGISTER_STRING;
   }

   return REGISTER_REGISTER;
}

QString ARegister::encodeType( int type)
{
QString stringRep;
   if( type & REGISTER_REGISTER){
      stringRep += "Register";
   }
   if( type & REGISTER_STRING_BUFFER){
      if(!stringRep.isEmpty()){
         stringRep += ", ";
      }
      stringRep += "String buffer";
   }
   if( type & REGISTER_STRING){
      if(!stringRep.isEmpty()){
         stringRep += ", ";
      }
      stringRep += "String";
   }
   if( type & REGISTER_COMMAND){
      if(!stringRep.isEmpty()){
         stringRep += ", ";
      }
      stringRep += "Command";
   }
   return stringRep;
}

Iterator *ARegister::CreateIterator()
{
   return new NullIterator();
}

//------------------------------------------------------------------------------
//  Leaf - concrete one register
//------------------------------------------------------------------------------

Register::Register( QString &Name, ERegisterType Type, int Number, const QStringList &Header, bool Readable, bool Writable)
{
   this->name = Name;
   this->registerType = Type;
   this->num = Number;
   this->read = Readable;
   this->write = Writable;
   this->headerFile = Header;
}

// Own methods
void Register::SetRange(int From, int To)
{
   from = QString::number(From);
   to = QString::number(To);
}

void Register::SetFrom(int From)
{
   from = QString::number(From);
}

void Register::SetTo(int To)
{
   to = QString::number(To);
}

//------------------------------------------------------------------------------
//  Composite - group of ARegister
//------------------------------------------------------------------------------
RegisterGroup::RegisterGroup( QString Name, QStringList &GroupVariables, int Base)
{
QStringList slist;

   this->name = Name;
   this->base = Base;
   for( int i = 0; i < GroupVariables.count(); i++){
      slist = GroupVariables.at(i).split(QRegExp("\\s+"));
      if(slist.count()< 2){
         continue;
      }
      this->variableTypes << slist[0];
      this->variableNames << slist[1];
   }
}

RegisterGroup::~RegisterGroup()
{
ARegister *reg;

   foreach(reg, Registers){
      delete( reg);
   }
   Registers.clear();
}

// ARegister methods
bool RegisterGroup::Readable(ERegisterType Type)
{
ARegister *reg;

   foreach(reg, Registers){
      if( reg->Readable(Type)){
         return true;
      }
   }
   return false;
}

bool RegisterGroup::Writable(ERegisterType Type)
{
ARegister *reg;

   foreach(reg, Registers){
      if( reg->Writable(Type)){
         return true;
      }
   }
   return false;
}

QStringList RegisterGroup::Header()
{
QStringList slist;
ARegister *reg;

   foreach(reg, Registers){
      slist << reg->Header();
      slist.removeDuplicates();
   }
   slist.replaceInStrings(QRegExp("[ ]*"), "");
   slist.removeAll("");
   return slist;
}

// Own methods
void RegisterGroup::Add(ARegister *Reg)
{
   Registers.append(Reg);
   registerType |= Reg->Type();
}

void RegisterGroup::Remove(ARegister *Reg)
{
int i;
ARegister *reg;

   i = Registers.indexOf(Reg);
   if( i >= 0){
      Registers.remove(i);
   }

   foreach(reg, Registers){
      registerType |= reg->Type();
   }
}

Iterator *RegisterGroup::CreateIterator()
{
   return new GroupIterator(this);
}

bool RegisterGroup::Contains(ARegister *Reg)
{
   return Registers.contains(Reg);

}

void RegisterGroup::SetName( QString Name)
{
   this->name = Name;
}

void RegisterGroup::SetNumber( int Number)
{
   this->base = Number;
}

//------------------------------------------------------------------------------
//  Group iter�tor
//------------------------------------------------------------------------------
GroupIterator::GroupIterator(RegisterGroup *reg)
   : Iterator()
{
   _it = new QVectorIterator<ARegister*>(reg->Registers);
}
GroupIterator::GroupIterator(const GroupIterator &copy)
   : Iterator(copy)
{
}

GroupIterator::~GroupIterator()
{
   delete _it;
}

bool GroupIterator::hasNext()
{
   return _it->hasNext();
}

ARegister *GroupIterator::Next()
{
   return _it->next();
}

void GroupIterator::SetItToNewLocation( RegisterGroup *reg)
{
   _it = new QVectorIterator<ARegister*>(reg->Registers);
}

//------------------------------------------------------------------------------
//  Group read iterator
//------------------------------------------------------------------------------
bool RGroupIterator::hasNext()
{
ARegister *reg;

   reg = Next();
   if(reg){
      _it->previous();
      return true;
   }
   return false;
}

ARegister *RGroupIterator::Next()
{
ARegister *reg;

   while(GroupIterator::hasNext()){
      reg = GroupIterator::Next();
      if(reg->Readable()){
         return reg;
      }
   }
   return NULL;
}

//------------------------------------------------------------------------------
//  Group write iterator
//------------------------------------------------------------------------------

ARegister *WGroupIterator::Next()
{
ARegister *reg;

   while(GroupIterator::hasNext()){
      reg = GroupIterator::Next();
      if(reg->Writable()){
         return reg;
      }
   }
   return NULL;
}

//------------------------------------------------------------------------------
//  Group R/W iterator
//------------------------------------------------------------------------------

ARegister *RWGroupIterator::Next()
{
ARegister *reg;

   while(GroupIterator::hasNext()){
      reg = GroupIterator::Next();
      if(reg->Readable() || reg->Writable()){
         return reg;
      }
   }
   return NULL;
}

//------------------------------------------------------------------------------
//  Group Type iterator
//------------------------------------------------------------------------------

ARegister *TypeGroupIterator::Next()
{
ARegister *reg;

   while(GroupIterator::hasNext()){
      reg = GroupIterator::Next();
      if(reg->Type() & itType){
         return reg;
      }
   }
   return NULL;
}

//------------------------------------------------------------------------------
//  Group Global keyword iterator
//------------------------------------------------------------------------------
GlobalRegsGroupIterator::GlobalRegsGroupIterator(RegisterGroup *reg)
   : GroupIterator(reg)
{
}

GlobalRegsGroupIterator::GlobalRegsGroupIterator(const GlobalRegsGroupIterator &copy)
   : GroupIterator(copy)
{
   _itStack = copy._itStack;
}

GlobalRegsGroupIterator::~GlobalRegsGroupIterator()
{
   for(int i = 0; i < _itStack.count(); i++){
      delete(_itStack.at(i));
   }
}

bool GlobalRegsGroupIterator::hasNext()
{
ARegister *reg;
QVector<QVectorIterator<ARegister*> *> stackBack;
QVectorIterator<ARegister*> *itBack;

   stackBack = _itStack;
   itBack = _it;
   reg = this->Next();
   if(itBack != _it){
      delete(_it);
      while(_itStack.back()!= itBack){
         delete(_itStack.back());
         _itStack.pop_back();
      }
   }

   _itStack = stackBack;
   _it = itBack;
   _it->previous();
   return reg != NULL;
}

ARegister *GlobalRegsGroupIterator::Next()
{
ARegister *reg;

   while(GroupIterator::hasNext()){
      reg = GroupIterator::Next();
      if(reg->ChildCount() != 0){
         _itStack.push_back(_it);
         GroupIterator::SetItToNewLocation((RegisterGroup*) reg);
         if( !this->Next()){
            continue;
         }
      }
      if(((Register*)reg)->DataName().toLower() == KEYWORD_GLOBAL){
         return reg;
      }
   }
   if(!_itStack.isEmpty()){
      delete(_it);
      _it = _itStack.back();
      _itStack.pop_back();
      return this->Next();
   }
   return NULL;
}

//------------------------------------------------------------------------------
//  Group Global keyword iterator
//------------------------------------------------------------------------------

ConditionalIterator::ConditionalIterator(RegisterGroup *reg)
   : RGroupIterator(reg)
{
   setCondition(true, true);
}

ConditionalIterator::ConditionalIterator(RegisterGroup *reg, bool r, bool w, int type)
   : RGroupIterator(reg)
{
   setCondition(r, w, type);
}

ConditionalIterator::ConditionalIterator(const ConditionalIterator &copy)
   : RGroupIterator(copy)
{
   R = copy.R;
   W = copy.W;
   type = copy.type;
}


void ConditionalIterator::setCondition(bool r, bool w, int type)
{
   R = r;
   W = w;
   this->type = type;
   _it->toFront();
}

ARegister *ConditionalIterator::Next()
{
ARegister *reg;

   while(GroupIterator::hasNext()){
      reg = GroupIterator::Next();
      if((reg->Type() & type) &&
         reg->Readable() == R &&
         reg->Writable() == W){
         return reg;
      }
   }
   return NULL;
}
