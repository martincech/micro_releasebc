//*****************************************************************************
//
//   Nvm.cpp       Nonvolatile memory simulator for Qt
//   Version 1.0   (c) VEIT Electronics
//
//*****************************************************************************

#include "Memory/Nvm.h"
#include "Fram/FramFake.h"        // FRAM simulator
#include "Flash/FlashFake.h"      // FLASH simulator
#include <QFile>
#include <string.h>

//------------------------------------------------------------------------------
//  Setup
//------------------------------------------------------------------------------

TYesNo NvmSetup( const char *FileName)
// Setup NVM from <FileName>
{
TYesNo Ok;

   memset( _FlashArray(), 0xFF, FLASH_SIZE);         // FLASH default contents
   memset( _FramArray(),  0xFF, FRAM_SIZE);          // FRAM default contents
   QFile file( FileName);
   if( !file.open( QIODevice::ReadOnly)){
      return( NO);
   }
   int size;
   Ok = YES;
   size = file.read( (char *)_FlashArray(), FLASH_SIZE);
   if( size != FLASH_SIZE){
      Ok = NO;
   }
   size = file.read( (char *)_FramArray(),  FRAM_SIZE);
   if( size != FRAM_SIZE){
      Ok = NO;
   }
   file.close();
   return( Ok);
} // NvmSetup

//------------------------------------------------------------------------------
//  Shutdown
//------------------------------------------------------------------------------

TYesNo NvmShutdown( const char *FileName)
// Save NVM to <FileName>
{
   QFile file( FileName);
   if( !file.open( QIODevice::WriteOnly | QIODevice::Truncate)){
      return( NO);
   }
   file.write( (const char *)_FlashArray(), FLASH_SIZE);
   file.write( (const char *)_FramArray(),  FRAM_SIZE);
   file.close();
   return( YES);
} // NvmShutdown
