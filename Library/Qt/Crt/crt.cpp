//******************************************************************************
//
//   Crt.cpp      CRT simple terminal window
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "crt.h"
#include <stdio.h>
#include <QThread>

#define CRT_STRING_SIZE  1023  // maximum printf string

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

Crt::Crt(QWidget *parent) :
   QPlainTextEdit(parent)
{
   _maximumRows = 300;
   setReadOnly( true);
   setLineWrapMode( QPlainTextEdit::NoWrap);
   setWordWrapMode( QTextOption::NoWrap);
   setMaximumBlockCount( _maximumRows);
   QFont consoleFont( "Courier New", 8);
   setFont( consoleFont);
#ifdef __UDEBUG__
   ostream = NULL;
#endif
} // Crt

Crt::~Crt()
{
#ifdef __UDEBUG__
   if(ostream){
      ofile.close();
   }
#endif
}

//------------------------------------------------------------------------------
//  Maximum rows
//------------------------------------------------------------------------------

void Crt::setMaximumRows( int Rows)
{
   _maximumRows = Rows;
   setMaximumBlockCount( _maximumRows);
} // setMaximumRows

int Crt::maximumRows()
{
   return( _maximumRows);
} // maximumRows

//------------------------------------------------------------------------------
//  clear
//------------------------------------------------------------------------------

void Crt::clear()
{
   QPlainTextEdit::clear();
} // clear

//------------------------------------------------------------------------------
//  putch
//------------------------------------------------------------------------------

void Crt::putch( const char ch)
{
   putch( QChar( ch));
} // putch

//------------------------------------------------------------------------------
//  puts
//------------------------------------------------------------------------------

void Crt::puts( const char *string)
{
   puts(QString(string));
//   appendPlainText( QString( string));
} // puts

#ifdef __UDEBUG__
void Crt::appendToFile(QString s){
   if( !ostream){
      ofile.setFileName(this->objectName() + "_" + QString::number((int)this->thread()) + ".txt");
      if(ofile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)){
         ostream = new QTextStream(&ofile);
      } else {
         return;
      }
   }
   *ostream << s;
   ostream->flush();
}
#endif
//------------------------------------------------------------------------------
//  printf
//------------------------------------------------------------------------------

void Crt::printf( const char *format, ...)
{
va_list arg;

   va_start( arg, format);
   vprintf( format, arg);
   va_end( arg);
} // printf

//------------------------------------------------------------------------------
//  vprintf
//------------------------------------------------------------------------------

void Crt::vprintf( const char *format, va_list arg)
{
char buffer[ CRT_STRING_SIZE + 1];

   vsnprintf( buffer, CRT_STRING_SIZE, format, arg);
   puts( buffer);
} // vprintf

//------------------------------------------------------------------------------
//  Qt putch
//------------------------------------------------------------------------------

void Crt::putch( QChar ch)
{
   puts( QString( ch));
} // putch

//------------------------------------------------------------------------------
//  Qt puts
//------------------------------------------------------------------------------

void Crt::puts( QString string)
{
   QObject::connect(this, SIGNAL(addText(QString)), this, SLOT(insertPlainText(QString)));
#ifdef __UDEBUG__
   QObject::connect(this, SIGNAL(addText(QString)), this, SLOT(appendToFile(QString)));
#endif
   emit(addText(string));
#ifdef __UDEBUG__
   QObject::disconnect(this, SIGNAL(addText(QString)), this, SLOT(appendToFile(QString)));
#endif
   QObject::disconnect(this, SIGNAL(addText(QString)), this, SLOT(insertPlainText(QString)));
} // puts
