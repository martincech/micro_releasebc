//*****************************************************************************
//
//    Rcs.cpp      Remote control server
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "Rcs.h"
#include "System/System.h"             // Operating system
#include "Remote/RcsCommand.h"         // Remote command processing
#include "Platform/PlatformRpc.h"      // Platform remote calls
#include "Platform/Wepl.h"             // Weighing platform utility

#define PLATFORM_NAME        "Wepl"

#define BUFFER_SIZE SOCKET_MESSAGE_SIZE_MAX

static byte         _buffer[ BUFFER_SIZE];
static LocalServer *_server;

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void RcsInit( LocalServer *server)
// Initialize
{
   WeplInit();
   _server = server;
   _server->start( PLATFORM_NAME);     // start listen
} // RcsInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void RcsExecute( void)
// Server executive
{
   int  rxSize;
   rxSize = _server->receive( _buffer, BUFFER_SIZE);
   if( !rxSize){
      return;
   }
   word ReplySize;
   if( !RcsCommand( (TPlatformCommand *)_buffer, (word)rxSize, (TPlatformReply *)_buffer, &ReplySize)){
      return;
   }
   _server->send( _buffer, ReplySize);
} // RcsExecute
