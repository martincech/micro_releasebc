//******************************************************************************
//
//   WeplClient.h   Weighing platform communication
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#ifndef __WeplClient_H__
   #define __WeplClient_H__

#include "Socket/LocalClient.h"
#include "Crt/Crt.h"
#include "Platform/Wepl.h"

void WeplClientInit( LocalClient *client, Crt *crt);
// Initialize client
   
#endif
