//*****************************************************************************
//
//   System.c     Operating system
//   Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#include "System/System.h"
#include "Time/uClock.h"
#include <time.h>

//-----------------------------------------------------------------------------
// Clock
//-----------------------------------------------------------------------------

dword SysClock( void)
// Returns actual date/time
{
UDateTime   DateTime;
time_t      now;
struct tm  *tm;

   time( &now);
   tm = localtime( &now);
   DateTime.Time.Sec   = tm->tm_sec;
   DateTime.Time.Min   = tm->tm_min;
   DateTime.Time.Hour  = tm->tm_hour;
   DateTime.Date.Day   = tm->tm_mday;
   DateTime.Date.Month = tm->tm_mon  + MONTH_JANUARY;
   DateTime.Date.Year  = tm->tm_year + 1900 - 2000;
   return( uClockGauge( &DateTime));  // internal with respect of DST
} // SysClock

dword SysDateTime( void)
// Returns wall clock date/time
{
UDateTime   DateTime;
time_t      now;
struct tm  *tm;

   time( &now);
   tm = localtime( &now);
   DateTime.Time.Sec   = tm->tm_sec;
   DateTime.Time.Min   = tm->tm_min;
   DateTime.Time.Hour  = tm->tm_hour;
   DateTime.Date.Day   = tm->tm_mday;
   DateTime.Date.Month = tm->tm_mon  + MONTH_JANUARY;
   DateTime.Date.Year  = tm->tm_year + 1900 - 2000;
   return( uDateTimeGauge( &DateTime));  // wall clock time without DST
} // SysDateTime

void SysClockSet( dword Clock)
// Set actual date/time to <DateTime> internal representation
{
} // SysClockSet
