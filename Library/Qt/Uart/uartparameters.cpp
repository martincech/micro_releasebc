//******************************************************************************
//
//   UartParameters.cpp Uart parameters dialog
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "UartParameters.h"
#include "ui_uartparameters.h"

#define UART_MAX  99

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

UartParameters::UartParameters(WinUart * uart, QWidget *parent) :
   UartParameters(parent)
{
   fromUart(uart);
}

UartParameters::UartParameters(QWidget *parent) :
   QDialog(parent),
   ui(new Ui::UartParameters)
{
   ui->setupUi(this);
   // ports list :
   QString number;
   for( int i = 1; i <= UART_MAX; i++){
      number.setNum(i);
      ui->cbxPort->addItem( QString( UART_PREFIX) + number);
   }
   // baud rate list :
   ui->cbxBaudRate->addItem( "110",   110);
   ui->cbxBaudRate->addItem( "300",   300);
   ui->cbxBaudRate->addItem( "600",   600);
   ui->cbxBaudRate->addItem( "1200",  1200);
   ui->cbxBaudRate->addItem( "2400",  2400);
   ui->cbxBaudRate->addItem( "4800",  4800);
   ui->cbxBaudRate->addItem( "9600",  9600);
   ui->cbxBaudRate->addItem( "14400", 14400);
   ui->cbxBaudRate->addItem( "19200", 19200);
   ui->cbxBaudRate->addItem( "38400", 38400);
   ui->cbxBaudRate->addItem( "57600", 57600);
   ui->cbxBaudRate->addItem( "115200",115200);
   // data bits :
   ui->cbxDataBits->addItem( "5", 5);
   ui->cbxDataBits->addItem( "6", 6);
   ui->cbxDataBits->addItem( "7", 7);
   ui->cbxDataBits->addItem( "8", 8);
   // parity :
   ui->cbxParity->addItem( "None");
   ui->cbxParity->addItem( "Odd");
   ui->cbxParity->addItem( "Even");
   ui->cbxParity->addItem( "Mark");
   ui->cbxParity->addItem( "Space");
   // stop bits :
   ui->cbxStopBits->addItem( "1 bit");
   ui->cbxStopBits->addItem( "1.5 bits");
   ui->cbxStopBits->addItem( "2 bits");
   // flow control:
   ui->cbxFlowControl->addItem( "none");
   ui->cbxFlowControl->addItem( "RTS/CTS");

   // default values :
   _portName = QString( "COM1");
   _baudRate = 9600;
   _dataBits = 8;
   _parity   = WinUart::PARITY_NONE;
   _stopBits = WinUart::STOPBIT_1;
   _flowControl = WinUart::FLOW_NONE;
   // setup dialog items :
   setup();
} // UartParameters

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

UartParameters::~UartParameters()
{
   delete ui;
} // ~UartParameters

//------------------------------------------------------------------------------
//  Getters
//------------------------------------------------------------------------------

QString UartParameters::getPortName()
{
   return( _portName);
} // getPortName

int UartParameters::getBaudRate()
{
   return( _baudRate);
} // getBaudRate

int UartParameters::getDataBits()
{
   return( _dataBits);
} // getDataBits

WinUart::EParity UartParameters::getParity()
{
   return( _parity);
} // getParity

WinUart::EStopBits UartParameters::getStopBits()
{
   return( _stopBits);
} // getStopBits

WinUart::EFlowControl UartParameters::getFlowControl()
{
   return( _flowControl);
} // getFlowControl

//------------------------------------------------------------------------------
//  Setters
//------------------------------------------------------------------------------

void UartParameters::setPortName( QString portName)
{
   _portName = portName;
   setup();
} // setPortName

void UartParameters::setBaudRate( int baudRate)
{
   _baudRate = baudRate;
   setup();
} // setBaudRate

void UartParameters::setDataBits( int dataBits)
{
   _dataBits = dataBits;
   setup();
} // setDataBits

void UartParameters::setParity( WinUart::EParity parity)
{
   _parity = parity;
   setup();
} // setParity

void UartParameters::setStopBits( WinUart::EStopBits stopBits)
{
   _stopBits = stopBits;
   setup();
} // setStopBits

void UartParameters::setFlowControl( WinUart::EFlowControl flowControl)
{
   _flowControl = flowControl;
   setup();
}

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

TYesNo UartParameters::execute()
{
   setup();
   if( !exec()){
      return( NO);
   }
   update();
   return( YES);
} // execute

//------------------------------------------------------------------------------
//  toString
//------------------------------------------------------------------------------

QString UartParameters::toString()
// Returns parameters as string
{
   QString result;
   // decode parity :
   char parity;
   switch( _parity){
      case WinUart::PARITY_NONE :
         parity = 'n';
         break;
      case WinUart::PARITY_ODD :
         parity = 'o';
         break;
      case WinUart::PARITY_EVEN :
         parity = 'e';
         break;
      case WinUart::PARITY_MARK :
         parity = 'm';
         break;
      case WinUart::PARITY_SPACE :
         parity = 's';
         break;
      default :
         parity = '?';
         break;
   }
   // decode stop bits :
   const char *stopBits;
   switch( _stopBits){
      case WinUart::STOPBIT_1 :
         stopBits = "1";
         break;
      case WinUart::STOPBIT_15 :
         stopBits = "1.5";
         break;
      case WinUart::STOPBIT_2 :
         stopBits = "2";
         break;
      default :
         stopBits = "?";
         break;
   }
   const char *flow;
   switch( _flowControl){
      case WinUart::FLOW_RTS_CTS :
         flow = "Harware RTS/CTS";
         break;
      default:
         flow = "none";
         break;
   }

   result.sprintf( "%s:%d,%d,%c,%s, %s", qPrintable( _portName), _baudRate, _dataBits, parity, stopBits, flow);
   return( result);
} // toString

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

TYesNo UartParameters::toUart( WinUart *uart)
// Set parameters to <uart>
{
   update();
   if( !uart->setPort( qPrintable( _portName))){
      return( NO);
   }
   if( !uart->setBaudRate( _baudRate)){
      return( NO);
   }
   if( !uart->setDataBits( _dataBits)){
      return( NO);
   }
   if( !uart->setParity( _parity)){
      return( NO);
   }
   if( !uart->setStopBits( _stopBits)){
      return( NO);
   }
   if( !uart->setFlowControl( _flowControl)){
      return( NO);
   }
   return( YES);
} // toUart

void UartParameters::fromUart( WinUart *uart)
// Set parameters from <uart>
{
   setPortName(uart->getPort());
   setBaudRate(uart->getBaudRate());
   setDataBits(uart->getDataBits());
   setParity(uart->getParity());
   setStopBits(uart->getStopBits());
   setFlowControl(uart->getFlowControl());
}

//------------------------------------------------------------------------------
//  Setup
//------------------------------------------------------------------------------

void UartParameters::setup()
{
   QString index = _portName.mid( sizeof( UART_PREFIX) - 1);
   int portIndex = index.toInt();
   ui->cbxPort->setCurrentIndex( portIndex - 1);
   ui->cbxBaudRate->setCurrentIndex( ui->cbxBaudRate->findData( _baudRate));
   ui->cbxDataBits->setCurrentIndex( _dataBits - 5);
   ui->cbxParity->setCurrentIndex( (int)_parity);
   ui->cbxStopBits->setCurrentIndex( (int)_stopBits);
   ui->cbxFlowControl->setCurrentIndex( (int)_flowControl);
} // setup

//------------------------------------------------------------------------------
//  Update
//------------------------------------------------------------------------------

void UartParameters::update()
{
   // port name :
   int index = ui->cbxPort->currentIndex();
   QString number;
   number.setNum( index + 1);
   _portName = QString( UART_PREFIX) + number;
   // baud rate :
   index = ui->cbxBaudRate->currentIndex();
   _baudRate = ui->cbxBaudRate->itemData( index).toInt();
   // other enums :
   _dataBits = ui->cbxDataBits->currentIndex() + 5;
   _parity   = (WinUart::EParity)ui->cbxParity->currentIndex();
   _stopBits = (WinUart::EStopBits)ui->cbxStopBits->currentIndex();
   _flowControl = (WinUart::EFlowControl) ui->cbxFlowControl->currentIndex();
} // update

QDataStream &operator<<(QDataStream &out, WinUart *uart)
{
   const char *port = uart->getPort();
   qint64  baudRate = (qint64) uart->getBaudRate();
   qint64  dataBits = (qint64)uart->getDataBits();
   qint64  stopBits = (qint64)uart->getStopBits();
   qint64  parity = (qint64)uart->getParity();
   qint64  flowControl = (qint64)uart->getFlowControl();

   out << port << baudRate << dataBits <<
           stopBits << parity << flowControl;
   return out;
}

QDataStream &operator>>(QDataStream &in, WinUart *uart)
{
   char * port;
   qint64  baudRate;
   qint64  dataBits;
   qint64  stopBits;
   qint64  parity;
   qint64  flowControl;

   in >> port >> baudRate >> dataBits >> stopBits >> parity >> flowControl;
   uart->setPort(port);
   uart->setBaudRate(baudRate);
   uart->setDataBits(dataBits);
   uart->setStopBits((WinUart::EStopBits) stopBits);
   uart->setParity((WinUart::EParity)parity);
   uart->setFlowControl((WinUart::EFlowControl)flowControl);
   delete port;
   return in;
}
