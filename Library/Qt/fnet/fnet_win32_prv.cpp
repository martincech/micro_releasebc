#include "fnet_win32_prv.h"
#define HAVE_REMOTE
#include <pcap.h>
#include <stdio.h>
#include <QStringList>
#include <QRegExp>
#include <QtConcurrentRun>

static eth_if_t *winApiGetInterface(int num, isr_handler interruptHandler);
static int winsockapi_inited = 0;
static pcap_if_t *alldevs;
static char errbuf[PCAP_ERRBUF_SIZE];
static void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data);
static void pcap_listener(eth_if_t *ethif);

eth_if_t *winApiInit(isr_handler interruptHandler)
{

   if(winsockapi_inited == 0){
      if (pcap_findalldevs_ex(PCAP_SRC_IF_STRING, NULL, &alldevs, errbuf) == -1)
      {
         return NULL;
      }
   }
   eth_if_t *d = winApiGetInterface(winsockapi_inited, interruptHandler);
   if(d != NULL){
      winsockapi_inited++;
   }
   return d;
}

void winApiFree(eth_if_t *ethif)
{
   if(winsockapi_inited == 0){
      return;
   }
   pcap_breakloop((pcap_t *)ethif->pcapdev);
   pcap_close((pcap_t *)ethif->pcapdev);
   delete (ethif);
   winsockapi_inited--;
   if(winsockapi_inited == 0){
       pcap_freealldevs(alldevs);
   }
}

eth_if_t *winApiGetInterface(int num, isr_handler interruptHandler)
{
eth_if_t *ethif;
pcap_if_t *d;
pcap_addr *pcapAddr;
QString a1, a2;
bool addressMatch;

   for(d = alldevs; d != NULL; d= d->next)
   {
      ethif = new eth_if_t;
      QList<QNetworkInterface> ifaces = QNetworkInterface::allInterfaces();
      foreach (QNetworkInterface iface, QNetworkInterface::allInterfaces()) {
         addressMatch = false;
         foreach (QNetworkAddressEntry address, iface.addressEntries()) {
            for(pcapAddr = d->addresses; pcapAddr != NULL; pcapAddr = pcapAddr->next){
               if(address.ip().protocol() != QAbstractSocket::IPv4Protocol ||
                     pcapAddr->addr->sa_family != AF_INET){
                  // address type match
                  break;
               }

               a1 = QString(inet_ntoa(((struct sockaddr_in *)pcapAddr->netmask)->sin_addr));
               a2 = address.netmask().toString();

               if( a1 != a2){
                  // netmask match
                  break;
               }
               a1 = QString(inet_ntoa(((struct sockaddr_in *)pcapAddr->addr)->sin_addr));
               a2 = address.ip().toString();

               if( a1 != a2){
                  // address match
                  break;
               }
               addressMatch = true;
               break;
            }
            if(addressMatch){
               break;
            }
         }
         if(addressMatch){
            ethif->networkdesc = iface;
            break;
         }
      }

      if(addressMatch){
         if(num != 0){
            num--;
            continue;
         }
         // open pcap interface
         if ((ethif->pcapdev = pcap_open(((pcap_if_t *)d)->name,// name of the device
                             65536,                // portion of the packet to capture
                             PCAP_OPENFLAG_PROMISCUOUS,                    // nonpromiscuous mode
                             1000,                 // read timeout
                             NULL,                 // authentication on the remote machine
                             errbuf                // error buffer
                             )) == NULL){
            return NULL;
         }
         ethif->rxhandler = interruptHandler;
         QtConcurrent::run(&pcap_listener, (eth_if_t *)ethif);
         return ethif;
      }
   }
   return NULL;
}

void winApiGetIpAddr(eth_if_t *ethif, char *ip_addr)
{
QNetworkAddressEntry address;

   foreach (address, ethif->networkdesc.addressEntries()){
      if(address.ip().protocol() != QAbstractSocket::IPv4Protocol){
         // address type match
         continue;
      }

      strcpy(ip_addr, address.ip().toString().toStdString().c_str());
      return;
   }
   ip_addr[0] = '\0';
}


void winApiGetMacAddr(eth_if_t *ethif, char *mac_addr)
{
   QStringList macList = ethif->networkdesc.hardwareAddress().split(QRegExp("[:]+"));
   for(int i = 0; i < macList.length(); i++){
      mac_addr[i] = macList[i].toInt(0, 16);
   }
}

bool winApiIsConnected(eth_if_t *ethif)
{
   return (ethif->networkdesc.isValid() && ethif->networkdesc.IsRunning && ethif->networkdesc.IsUp);
}

bool winApiOutput(eth_if_t *ethif, const unsigned char *data, int length)
{
   return pcap_sendpacket(((pcap_t *)ethif->pcapdev), data, length) == 0;
}

static void pcap_listener(eth_if_t *ethif)
{
   pcap_loop((pcap_t *)ethif->pcapdev, -1, &packet_handler, (u_char *)ethif);
}

/* Callback function invoked by libpcap for every incoming packet */
static void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data)
{
eth_if_t *ethif = (eth_if_t *) param;

   ethif->rxhandler(ethif, (const unsigned char *)pkt_data, header->len);
}
