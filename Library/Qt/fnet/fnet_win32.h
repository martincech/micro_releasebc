#ifndef __FNET_WIN32_H__
   #define __FNET_WIN32_H__



extern "C"{
#include "fnet.h"
#include "fnet_netif_prv.h"

   int fnet_pcap_get_ip_addr(fnet_netif_t *netif, fnet_ip4_addr_t * ip_addr);
   extern void fnet_log(int c);
}

#endif // __FNET_WIN32_H__
