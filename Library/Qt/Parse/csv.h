//******************************************************************************
//
//   Csv.h        CSV parser
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#ifndef CSV_H
   #define CSV_H

#include "Model/ArrayModel.h"

class Csv
{
public:
   Csv( int columns);
   ~Csv();

   bool load( QString fileName);
   // load CSV <fileName> to current array

   bool save( QString fileName);
   // save current array as <fileName>

   void setUnicode( bool isUnicode);
   // set current <isUnicode> encoding

   void setItemSeparator( QChar itemSeparator);
   // set items separator character

   void setTextDelimiter( QChar textDelimiter);
   // set text delimiter character

   ArrayModel *array();
   // returns current array of strings

private :
   ArrayModel *_array;
   QChar       _itemSeparator;
   QChar       _textDelimiter;
   bool        _isUnicode;

   void parseLine( QString line, QStringList &items);
   // parse <line>, returns <items> list
}; // Csv

#endif // CSV_H
