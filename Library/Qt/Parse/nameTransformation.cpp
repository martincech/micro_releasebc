//******************************************************************************
//
//   NameTransformation.cpp Object name transformation
//   Version 1.0            (c) VEIT Electronics
//
//******************************************************************************

#include "NameTransformation.h"
#include <QStringList>

//------------------------------------------------------------------------------
//  To upper case
//------------------------------------------------------------------------------

QString OName::toUpperCase( QString name)
// remove spaces, all letters in upper case
{
   name = name.simplified();                // to single space
   name.replace( QChar( ' '), QChar( '_')); // space as underscore
   name = name.toUpper();                   // upper case
   return( name);
} // toUpperCase

//------------------------------------------------------------------------------
//  To lower case
//------------------------------------------------------------------------------

QString OName::toLowerCase( QString name)
// single space only, all letters lowcase
{
   name = name.simplified();                // to single space
   name = name.toLower();                   // low case
   return( name);
} // toLowerCase

//------------------------------------------------------------------------------
//  Camel case
//------------------------------------------------------------------------------

QString OName::toCamelCase( QString name)
// remove spaces, first word letter in upper case
{
   name = name.simplified();                // to single space
   // split to words :
   QStringList words;
   words = name.split( " ", QString::SkipEmptyParts);
   // capitalize :
   QString word;
   for( int i = 0; i < words.size(); i++){
      word = words.at( i);
      word = OName::toText( word);          // first letter upcase, other lowcase
      words.replace( i, word);
   }
   // joint without spaces :
   name = words.join( QString());
   return( name);
} // toCamelCase

//------------------------------------------------------------------------------
//  Object case (Qt)
//------------------------------------------------------------------------------

QString OName::toObjectCase( QString name)
// remove spaces, first word in lower case other as camel case
{
   name     = OName::toCamelCase( name);
   name[ 0] = name[ 0].toLower();
   return( name);
} // toObjectCase

//------------------------------------------------------------------------------
//  Text
//------------------------------------------------------------------------------

QString OName::toText( QString name)
// name as text string
{
   name = name.simplified();                // to single space
   name = name.toLower();                   // all lowcase
   if( name.size() > 0){
      if( name[ 0] != QChar('_')){
         name[ 0] = name[ 0].toUpper();     // first letter upcase
      } else {
         // first letter is underscore
         if( name.size() > 1){
            name[ 1] = name[ 1].toUpper();  // second letter upcase
         }
      }
   }
   return( name);
} // toText

//------------------------------------------------------------------------------
//  From upcase
//------------------------------------------------------------------------------

QString OName::fromUpperCase( QString name)
// replace underscore with space, first letter upcase, remainder lowcase
{
   name.replace( QChar( '_'), QChar( ' '));  // underscore as space
   return( OName::toText( name));
} // fromUpperCase

//------------------------------------------------------------------------------
//  From camel
//------------------------------------------------------------------------------

QString OName::fromCamelCase( QString name)
// insert space before upcase letter, first letter upcase, remainder lowcase
{
   QString newName;
   for( int i = 0; i < name.size(); i++){
      if( name[ i].isUpper()){
         newName.append( ' ');
      }
      newName.append( name[ i]);
   }
   return( OName::toText( newName));
} // fromCamelCase
