//******************************************************************************
//
//   Lexer.cpp    Lexical analyser
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "Lexer.h"

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

Lexer::Lexer() : _units()
{
   _text  = QString();
} // Lexer

Lexer::Lexer( QString text) : _units()
{
   setText( text);
} // Lexer

//------------------------------------------------------------------------------
//   Set text
//------------------------------------------------------------------------------

void Lexer::setText( QString text)
// set current <text>
{
   _text = text;
   _units.clear();
   _line   = 0;
   _column = 0;
   _errorUnit.type   = LEX_ERROR;
   _errorUnit.value  = QString();
   _errorUnit.line   = 0;
   _errorUnit.column = 0;
} // setText

//------------------------------------------------------------------------------
//   Count
//------------------------------------------------------------------------------

int Lexer::count()
// returns lexical units count
{
   return( _units.count());
} // count

//------------------------------------------------------------------------------
//   Get item
//------------------------------------------------------------------------------

const LexicalUnit *Lexer::at( int index)
// returns units at <index>
{
   if( index < 0){
      _errorUnit.type   = LEX_ERROR;
      _errorUnit.line   = 0;
      _errorUnit.column = 0;
      return( &_errorUnit);
   }
   if( index >= _units.count()){
      _errorUnit.type   = LEX_END;
      _errorUnit.line   = _units.at( _units.count() - 1).line;
      _errorUnit.column = _units.at( _units.count() - 1).column;
      return( &_errorUnit);
   }
   return( &_units.at( index));
} // at

//------------------------------------------------------------------------------
//   View
//------------------------------------------------------------------------------

QString Lexer::view( LexicalUnit &unit)
// convert <unit> to diagnostic text
{
   switch( unit.type){
      case LEX_UNDEFINED :
         return( QString( "(%1)UNDEF").arg( unit.column));

      case LEX_SPACE :
         return( QString( "(%1)SPACE : '%2'").arg( unit.column).arg( unit.value));

      case LEX_NEWLINE :
         return( QString( "(%1)NEWLINE %2").arg( unit.column).arg( unit.line));

      case LEX_PUNCTUATION :
         return( QString( "(%1)PUNCT : '%2'").arg( unit.column).arg( unit.value));

      case LEX_SYMBOL :
         return( QString( "(%1)SYM : '%2'").arg( unit.column).arg( unit.value));

      case LEX_WORD :
         return( QString( "(%1)WORD : '%2'").arg( unit.column).arg( unit.value));

      case LEX_NUMBER :
         return( QString( "(%1)NUM : '%2'").arg( unit.column).arg( unit.value));

      case LEX_TEXT :
         return( QString( "(%1)TEXT : \"%2\"").arg( unit.column).arg( unit.value));

      case LEX_COMMENT :
         return( QString( "(%1)COMMENT : '%2'").arg( unit.column).arg( unit.value));

      case LEX_END :
         return( QString( "**END**"));

      case LEX_ERROR :
         return( QString( "(%1)ERROR '%2'").arg( unit.column).arg( unit.value));

      default :
         return( QString( "(%1)UKNOWN %2").arg( unit.column).arg( unit.type));
   }
} // view

//------------------------------------------------------------------------------
//   Parse
//------------------------------------------------------------------------------

void Lexer::parse()
// parse current text
{
   // clear list :
   _units.clear();
   // initialize position :
   _line    = 1;
   _column  = 1;
   int size = _text.size();
   int startIndex;
   QChar       ch;
   LexicalUnit unit;   
   for( int i = 0; i < size; i++){
      ch = _text[ i];
      startIndex = i;
      unit.line   = _line;
      unit.column = _column;
      if( ch.isLetter() || ch == QChar('_')){
         unit = parseWord( i);
      } else if( ch.isNumber()){
         unit = parseNumber( i);
      } else if( ch.isSpace()){
         if( ch == QChar( '\n')){
            unit.type  = LEX_NEWLINE;
            unit.value = QString( "\\n");
            _column = 0;
            _line++;
         } else {
            unit = parseSpace( i);
         }
      } else if( ch == QChar( '"')){
         unit = parseText( i);
      } else if( ch == QChar( '/')){
         unit.value = ch;
         unit.type  = LEX_SYMBOL;
         if( i < size - 1){
            ch = _text[ i + 1];
            if( ch == QChar( '/')){
               unit = parseComment( i);
            }
         }
      } else {
         unit = parseOther( i);
      }
      _column += i - startIndex + 1;
      _units.append( unit);
   }
} // parse

//------------------------------------------------------------------------------
//   Parse word
//------------------------------------------------------------------------------

LexicalUnit Lexer::parseWord( int &index)
// parse text word at <index>
{
   LexicalUnit unit;
   unit.line   = _line;
   unit.column = _column;
   unit.type   = LEX_WORD;
   unit.value  = QString();
   QChar ch;
   int   size = _text.size();
   for( int i = index; i < size; i++){
      ch = _text[ i];
      if( !(ch.isLetter() || ch.isNumber() || ch == QChar( '_'))){
         index = i - 1;
         return( unit);
      }
      unit.value.append( ch);
   }
   index     = size;
   unit.type = LEX_ERROR;              // unable complete
   return( unit);
} // parseWord

//------------------------------------------------------------------------------
//   Parse number
//------------------------------------------------------------------------------

LexicalUnit Lexer::parseNumber( int &index)
// parse number at <index>
{
   LexicalUnit unit;
   unit.line   = _line;
   unit.column = _column;
   unit.type   = LEX_NUMBER;
   unit.value  = QString();
   QChar ch;
   int   size = _text.size();
   for( int i = index; i < size; i++){
      ch = _text[ i];
      if( !ch.isNumber()){
         index = i - 1;
         return( unit);
      }
      unit.value.append( ch);
   }
   index     = size;
   unit.type = LEX_ERROR;              // unable complete
   return( unit);
} // parseNumber

//------------------------------------------------------------------------------
//   Parse space
//------------------------------------------------------------------------------

LexicalUnit Lexer::parseSpace( int &index)
// parse spaces
{
   LexicalUnit unit;
   unit.line   = _line;
   unit.column = _column;
   unit.type   = LEX_SPACE;
   unit.value  = QString();
   QChar ch;
   int   size = _text.size();
   for( int i = index; i < size; i++){
      ch = _text[ i];
      if( ch == QChar( '\n')){
         index = i - 1;
         return( unit);
      }
      if( !ch.isSpace()){
         index = i - 1;
         return( unit);
      }
      unit.value.append( ch);
   }
   index     = size;
   unit.type = LEX_ERROR;              // unable complete
   return( unit);
} // parseSpace

//------------------------------------------------------------------------------
//   Parse comment
//------------------------------------------------------------------------------

LexicalUnit Lexer::parseText( int &index)
// parse text string
{
   LexicalUnit unit;
   unit.line   = _line;
   unit.column = _column;
   unit.type   = LEX_TEXT;
   unit.value  = QString();
   QChar ch;
   int   size = _text.size();
   for( int i = index + 1; i < size; i++){
      ch = _text[ i];
      if( ch == QChar( '\n')){
         unit.type = LEX_ERROR;        // line feed inside string
         index     = i - 1;
         return( unit);
      }
      if( ch == QChar( '"')){
         index = i;                    // skip quote
         return( unit);
      }
      unit.value.append( ch);
   }
   index     = size;
   unit.type = LEX_ERROR;              // unable complete
   return( unit);
} // parseText

//------------------------------------------------------------------------------
//   Parse comment
//------------------------------------------------------------------------------

LexicalUnit Lexer::parseComment( int &index)
// parse comment
{
   LexicalUnit unit;
   unit.line   = _line;
   unit.column = _column;
   unit.type   = LEX_COMMENT;
   unit.value  = QString();
   QChar ch;
   int   size = _text.size();
   for( int i = index + 2; i < size; i++){
      ch = _text[ i];
      if( ch == QChar( '\n')){
         index = i - 1;
         return( unit);
      }
      unit.value.append( ch);
   }
   index     = size;
   unit.type = LEX_ERROR;              // unable complete
   return( unit);
} // parseComment

//------------------------------------------------------------------------------
//   Parse other
//------------------------------------------------------------------------------

LexicalUnit Lexer::parseOther( int &index)
// parse punctuation and symbols
{
   LexicalUnit unit;
   unit.line   = _line;
   unit.column = _column;
   unit.type   = LEX_UNDEFINED;
   QChar ch;
   ch = _text[ index];
   unit.value  = ch;
   switch( ch.toAscii()){
      case '!' :
         unit.type = LEX_PUNCTUATION;
         break;
      case '\"' :
         unit.type = LEX_SYMBOL;
         break;
      case '#' :
         unit.type = LEX_SYMBOL;
         break;
      case '$' :
         unit.type = LEX_SYMBOL;
         break;
      case '%' :
         unit.type = LEX_SYMBOL;
         break;
      case '&' :
         unit.type = LEX_SYMBOL;
         break;
      case '\'' :
         unit.type = LEX_SYMBOL;
         break;
      case '(' :
         unit.type = LEX_SYMBOL;
         break;
      case ')' :
         unit.type = LEX_SYMBOL;
         break;
      case '*' :
         unit.type = LEX_SYMBOL;
         break;
      case '+' :
         unit.type = LEX_SYMBOL;
         break;
      case ',' :
         unit.type = LEX_PUNCTUATION;
         break;
      case '-' :
         unit.type = LEX_SYMBOL;
         break;
      case '.' :
         unit.type = LEX_PUNCTUATION;
         break;
      case '/' :
         unit.type = LEX_SYMBOL;
         break;
      case ':' :
         unit.type = LEX_PUNCTUATION;
         break;
      case ';' :
         unit.type = LEX_PUNCTUATION;
         break;
      case '<' :
         unit.type = LEX_SYMBOL;
         break;
      case '=' :
         unit.type = LEX_SYMBOL;
         break;
      case '>' :
         unit.type = LEX_SYMBOL;
         break;
      case '?' :
         unit.type = LEX_PUNCTUATION;
         break;
      case '@' :
         unit.type = LEX_SYMBOL;
         break;
      case '[' :
         unit.type = LEX_SYMBOL;
         break;
      case '\\' :
         unit.type = LEX_SYMBOL;
         break;
      case ']' :
         unit.type = LEX_SYMBOL;
         break;
      case '^' :
         unit.type = LEX_SYMBOL;
         break;
      case '_' :
         unit.type = LEX_SYMBOL;
         break;
      case '`' :
         unit.type = LEX_SYMBOL;
         break;
      case '{' :
         unit.type = LEX_SYMBOL;
         break;
      case '|' :
         unit.type = LEX_SYMBOL;
         break;
      case '}' :
         unit.type = LEX_SYMBOL;
         break;
      case '~' :
         unit.type = LEX_SYMBOL;
         break;
      default :
         break;
   }
   return( unit);
} // parseOther
