//******************************************************************************
//
//   TextFile.cpp Text file utility
//   Version 1.0  (c) VEIT Electronics
//
//******************************************************************************

#include "TextFile.h"
#include <QFile>
#include <QTextStream>
#include <QTextCodec>

#define UNICODE_CODEC_NAME "UTF-16"

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

TextFile::TextFile()
{
   _fileName  = QString();
   _isUnicode = false;
} // TextFile

TextFile::TextFile( QString fileName)
{
   _fileName  = fileName;
   _isUnicode = false;
} // TextFile

//------------------------------------------------------------------------------
//  File name
//------------------------------------------------------------------------------

void TextFile::setFileName( QString fileName)
// set current <fileName>
{
   _fileName = fileName;
} // setFileName

//------------------------------------------------------------------------------
//  Unicode
//------------------------------------------------------------------------------

void TextFile::setUnicode( bool isUnicode)
// set current <isUnicode> encoding
{
   _isUnicode = isUnicode;
} // setUnicode


//------------------------------------------------------------------------------
//  Load
//------------------------------------------------------------------------------

QString TextFile::load()
// load current file (auto detect unicode encoding)
{
   if( _fileName.isEmpty()){
      return( QString());
   }
   QFile file( _fileName);
   if( !file.open( QIODevice::ReadOnly | QIODevice::Text)){
      return( QString());
   }
   QTextStream stream( &file);
   stream.setAutoDetectUnicode( false);        // disable auto detection
   if( _isUnicode){
      stream.setCodec( UNICODE_CODEC_NAME);
   }
   return( stream.readAll());
} // load

//------------------------------------------------------------------------------
//  Save
//------------------------------------------------------------------------------

bool TextFile::save( QString text)
// save current file (by current unicode encoding)
{
   if( _fileName.isEmpty()){
      return( false);
   }
   QFile file( _fileName);
   if( !file.open( QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)){
      return( false);
   }
   QTextStream stream( &file);
   if( _isUnicode){
      stream.setCodec( UNICODE_CODEC_NAME);
      stream << QChar( QChar::ByteOrderMark);
   }
   stream << text;
   return( true);
} // save
