//******************************************************************************
//
//   Efs.cpp         External File System
//   Version 1.0     (c) VEIT Electronics
//
//******************************************************************************

#include "File/Efs.h"
#include <string.h>
#include <QDir>
#include <QFile>

static char _EfsRoot[ EFS_PATH_MAX + 1];
static char _EfsDirectory[ EFS_PATH_MAX + 1];
static char _EfsFile[ EFS_PATH_MAX + 1];

static QDir  _Directory;
static QFile _File;

static bool RemoveDir(const QString & DirName);
// Remove dir

//------------------------------------------------------------------------------
//  Simulator
//------------------------------------------------------------------------------

void EfsRoot( const char *Directory)
// Setup external filesystem root to <Directory>
{
   strcpy( _EfsRoot, Directory);
} // EfsRoot

//-----------------------------------------------------------------------------
//   Initialize
//-----------------------------------------------------------------------------

TYesNo EfsInit( void)
// Initialisation
{
   strcpy( _EfsDirectory, _EfsRoot);
   if( !_Directory.mkpath( QString( _EfsRoot))){
      return NO;
   }
   strcpy( _EfsFile, "");

   return YES;
} // EfsInit

//-----------------------------------------------------------------------------
//   Format
//-----------------------------------------------------------------------------

TYesNo EfsFormat( void)
// Format
{
   strcpy( _EfsDirectory, _EfsRoot);
   strcat( _EfsDirectory, "/");

   return RemoveDir(QString(_EfsDirectory));
}

//-----------------------------------------------------------------------------
//   Directory create
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryCreate( const char *Name)
// Create new directory with <Name>
{
   strcpy( _EfsDirectory, _EfsRoot);
   strcat( _EfsDirectory, "/");
   strcat( _EfsDirectory, Name);
   return( _Directory.mkpath( QString( _EfsDirectory)));
} // EfsDirectoryCreate

//-----------------------------------------------------------------------------
//   Directory exists
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryExists( const char *Path)
// Check for directory <Name>
{
   strcpy( _EfsDirectory, _EfsRoot);
   strcat( _EfsDirectory, "/");
   strcat( _EfsDirectory, Path);
   if( !_Directory.cd( QString(_EfsDirectory))){
      return( NO);
   }

   return YES;
} // EfsDirectoryExists

//-----------------------------------------------------------------------------
//   Directory rename
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryRename( const char *Path, const char *NewName)
// Renames selected directory
{
   strcpy( _EfsDirectory, _EfsRoot);
   strcat( _EfsDirectory, "/");
   strcat( _EfsDirectory, Path);
   if( !_Directory.cd( QString(_EfsDirectory))){
      return( NO);
   }
   QString OldName = _Directory.dirName();

   _Directory.cd("..");

   if(!_Directory.rename(OldName, QString(NewName))) {
      return NO;
   }

   return YES;
} // EfsDirectoryRename

//-----------------------------------------------------------------------------
//   Directory change
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryChange( const char *Name)
// Change current directory to <Name>
{
   strcpy( _EfsDirectory, _EfsRoot);
   strcat( _EfsDirectory, "/");
   strcat( _EfsDirectory, Name);
   return( YES);
} // EfsDirectoryChange

//-----------------------------------------------------------------------------
//   Directory delete
//-----------------------------------------------------------------------------

TYesNo EfsDirectoryDelete( const char *Name)
// Delete directory <Name>
{
   strcpy( _EfsDirectory, _EfsRoot);
   strcat( _EfsDirectory, "/");
   if( !_Directory.cd( QString(_EfsDirectory))){
      return( NO);
   }
   return( _Directory.rmdir( QString( Name)));
} // EfsDirectoryDelete

//-----------------------------------------------------------------------------
//   File exists
//-----------------------------------------------------------------------------

TYesNo EfsFileExists( const char *Name)
// Check for file <Name>
{
   return QFile::exists( QString(Name));
} // EfsFileExists

//-----------------------------------------------------------------------------
//   File create
//-----------------------------------------------------------------------------

TYesNo EfsFileCreate( TEfsFile *File, const char *FileName, dword Size)
// Create <FileName>
{
   strcpy( _EfsFile, _EfsDirectory);
   strcat( _EfsFile, "/");
   strcat( _EfsFile, FileName);
   _File.setFileName( QString( _EfsFile));
   return( _File.open( QIODevice::ReadWrite | QIODevice::Truncate));
} // EfsFileCreate

//-----------------------------------------------------------------------------
//   File open
//-----------------------------------------------------------------------------

TYesNo EfsFileOpen( TEfsFile *File, const char *FileName)
// Open <FileName> for read/write
{
   strcpy( _EfsFile, _EfsDirectory);
   strcat( _EfsFile, "/");
   strcat( _EfsFile, FileName);
   _File.setFileName( QString( _EfsFile));
   return( _File.open( QIODevice::ReadWrite));
} // EfsFileOpen

//-----------------------------------------------------------------------------
//   File delete
//-----------------------------------------------------------------------------

TYesNo EfsFileDelete( const char *FileName)
// Delete <FileName>
{
   strcpy( _EfsFile, _EfsDirectory);
   strcat( _EfsFile, "/");
   strcat( _EfsFile, FileName);
   return( _Directory.remove( QString( _EfsFile)));
} // EfsFileDelete

//-----------------------------------------------------------------------------
//   File close
//-----------------------------------------------------------------------------

void EfsFileClose( TEfsFile *File)
// Close opened file
{
   _File.close();
} // EfsFileClose

//-----------------------------------------------------------------------------
//   File write
//-----------------------------------------------------------------------------

word EfsFileWrite( TEfsFile *File, const void *Data, word Count)
// Write <Data> with <Count> bytes, returns number of bytes written
{
qint64 Size;

   Size = _File.write( (const char *)Data, (qint64)Count);
   if( Size < 0){
      return( 0);
   }
   return( (word)Size);
} // EfsFileWrite

//-----------------------------------------------------------------------------
//   File read
//-----------------------------------------------------------------------------

word EfsFileRead( TEfsFile *File, void *Data, word Count)
// Read <Data> with <Count> bytes, returns number of bytes read
{
qint64 Size;

   Size = _File.read( (char *)Data, (qint64)Count);
   if( Size < 0){
      return( 0);
   }
   return( (word)Size);
} // EfsFileRead

//-----------------------------------------------------------------------------
//   File seek
//-----------------------------------------------------------------------------

TYesNo EfsFileSeek( TEfsFile *File, int32 Pos, EEfsSeekMode Whence)
// Seek at <Pos> starting from <Whence>
{
   switch( Whence){
      case EFS_SEEK_START :
         return( _File.seek( (qint64)Pos));

      case EFS_SEEK_CURRENT :
         return( _File.seek( _File.pos() + Pos));

      case EFS_SEEK_END :
         return( _File.seek( _File.size() - 1 + Pos));
   }
   return( NO);
} // EfsFileSeek

//-----------------------------------------------------------------------------
//   Unmount
//-----------------------------------------------------------------------------

void EfsSwitchOff( void)
// Safely unmount drive
{
} // EfsUnmount

//******************************************************************************

//-----------------------------------------------------------------------------
//   Remove dir
//-----------------------------------------------------------------------------

static bool RemoveDir(const QString & DirName)
// Remove dir
{
    bool result;
    QDir dir(DirName);

    if (dir.exists(DirName)) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                result = RemoveDir(info.absoluteFilePath());
            }
            else {
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result) {
                return result;
            }
        }
        result = dir.rmdir(DirName);
    }
    return result;
}
