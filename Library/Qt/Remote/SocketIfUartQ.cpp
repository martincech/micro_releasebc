#include "SocketIfUartQ.h"
#include "Remote/Frame.h"

#define CHUNK_SIZE   1024

#define LoggerTx( frame, size)       if( _logger) _logger->show( CrtDump::TX, frame, size)
#define LoggerRx( frame, size)       if( _logger) _logger->show( CrtDump::RX, frame, size)
#define LoggerGarbage( frame, size)  if( _logger) _logger->show( CrtDump::GARBAGE, frame, size)
#define LoggerReport( txt, ...)      if( _logger) _logger->printf( txt, ##__VA_ARGS__)

//------------------------------------------------------------------------------
//  Constructors
//------------------------------------------------------------------------------

SocketIfUart::SocketIfUart( ) :
   WinUart()
{
   _State = SOCKET_STATE_DISCONNECTED;
   this->setReceiveTimeout(1000, 0);
}

bool SocketIfUart::connected(){
   return checkOpen();
}

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

SocketIfUart::~SocketIfUart()
// destructor
{

}

byte SocketIfUart::State( void)
// Gets state of socket
{
   return _State;
}


TYesNo SocketIfUart::Receive( void *Buffer, int Size)
// Receive into <Buffer> with <Size>
{
byte ChunkBuffer[CHUNK_SIZE];
int ReceiveSize;
TFrameState FrameState;
   FrameInit( &FrameState);
   FrameReceiveStart( &FrameState, Buffer, Size);

   forever {
      ReceiveSize = receive( ChunkBuffer, CHUNK_SIZE);
      if(!ReceiveSize) {
         return NO;
      }
      if(!FrameReceiveProccess( &FrameState, ChunkBuffer, ReceiveSize)) {
         return NO;
      }
      switch(FrameState.Status) {
         case FRAME_STATE_RECEIVE_ACTIVE:
            break;
         case FRAME_STATE_RECEIVE_FRAME:
            ReplySize = FrameState.SizeProccessed;
            _State = SOCKET_STATE_RECEIVE_DONE;
            return YES;
         default:
            _State = SOCKET_STATE_RECEIVE_ERROR;
            return NO;
      }
   }
}

int SocketIfUart::ReceiveSize( void)
// Gets number of received bytes
{
   return ReplySize;
}

TYesNo SocketIfUart::Send( const void *Buffer, int Size)
// Send <Buffer> with <Size>
{
int ChunkSize;
byte ChunkBuffer[CHUNK_SIZE];
TFrameState FrameState;
   FrameInit( &FrameState);
   FrameSendInit( &FrameState, Buffer, Size);

   forever {
      if(!FrameSendProccess( &FrameState, ChunkBuffer, CHUNK_SIZE, &ChunkSize)) {
         return NO;
      }
      if(!send( ChunkBuffer, ChunkSize)) {
         return NO;
      }
      switch(FrameState.Status) {
         case FRAME_STATE_SEND_ACTIVE:
            break;

         case FRAME_STATE_SEND_DONE:
            _State = SOCKET_STATE_SEND_DONE;
            return YES;

         default:
            _State = SOCKET_STATE_SEND_ERROR;
            return NO;
      }
   }
}

void SocketIfUart::Close( void)
// Close socket
{
   WinUart::close();
}

TYesNo SocketIfUart::WriteOnly( void)
//
{
   return NO;
}
