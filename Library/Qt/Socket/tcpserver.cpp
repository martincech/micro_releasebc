//******************************************************************************
//
//   TcpServer.cpp  TCP socket server
//   Version 1.0    (c) VEIT Electronics
//
//******************************************************************************

#include "tcpserver.h"
#include <string.h>

#define LoggerTx( frame, size)       if( _logger) _logger->show( CrtDump::TX, frame, size)
#define LoggerRx( frame, size)       if( _logger) _logger->show( CrtDump::RX, frame, size)
#define LoggerGarbage( frame, size)  if( _logger) _logger->show( CrtDump::GARBAGE, frame, size)
#define LoggerReport( txt, ...)      if( _logger) _logger->printf( txt, ##__VA_ARGS__)

//------------------------------------------------------------------------------
//   Constructor
//------------------------------------------------------------------------------

TcpServer::TcpServer()
{
   this->_socket       = NULL;
   _logger       = NULL;
   _receivedSize = 0;
} // TcpServer


TcpServer::TcpServer( QObject *parent) :
   QTcpServer( parent)
{
   _socket       = 0;
   _logger       = 0;
   _receivedSize = 0;
} // TcpServer

//------------------------------------------------------------------------------
//   Destructor
//------------------------------------------------------------------------------

TcpServer::~TcpServer()
{
   LoggerReport( "Server stopped\n");
   if( !_socket){
      return;
   }
   _socket->abort();
   _socket->deleteLater();
} // ~TcpServer

//------------------------------------------------------------------------------
//   Start
//------------------------------------------------------------------------------

bool TcpServer::start( QString name)
{
   return start(name, SOCKET_PORT);
}

bool TcpServer::start( QString name, int port)
// Start server with <name>
{

   if( !listen( QHostAddress::Any, port)){
      LoggerReport( "Server unable listen\n");
      return( false);
   }
   connect( this, SIGNAL( newConnection()),
            this, SLOT( connectClient()));
   LoggerReport( "Server start\n");
   return( true);
} // start

//------------------------------------------------------------------------------
//  Set logger
//------------------------------------------------------------------------------

void TcpServer::setLogger( CrtDump *logger)
// Set data visualisation <logger>
{
   _logger = logger;
} // setLogger

//------------------------------------------------------------------------------
//   Send
//------------------------------------------------------------------------------

bool TcpServer::send( void *data, int size)
// Send data to client
{
   if( size > SOCKET_MESSAGE_SIZE_MAX){
      return( false);                  // too long message
   }
   if( !_socket){
      return( false);                  // disconnected
   }
   _message[ 0] = size & 0xFF;
   _message[ 1] = (size >> 8) & 0xFF;
   memcpy( &_message[ 2], data, size);
   qint64 sizeSend = SocketMessageLength( size);
   qint64 sizeWritten;
   sizeWritten = _socket->write( (const char *)_message, sizeSend);
   if( sizeWritten != sizeSend){
      LoggerReport( "Send size mismatch\n");
      return( false);
   }
   _socket->flush();
   _receivedSize = 0;                  // wait for next command
   LoggerTx( _message, sizeWritten);
   return( true);
} // send

//------------------------------------------------------------------------------
//   Receive
//------------------------------------------------------------------------------

int TcpServer::receive( void *data, int size)
// Receive <data> with total <size>, returns size received
{
   if( !_socket){
      return( 0);                      // disconnected
   }
   qint64 availableSize;
   availableSize = _socket->bytesAvailable();
   if( availableSize == 0){
      return( 0);                      // no data received
   }
   qint64  readRequest;
   qint64  readSize;
   // read header :
   if( _receivedSize == 0){
      if( availableSize < (qint64)SocketHeaderLenght()){
         LoggerReport( "Short header\n");
         return( 0);                   // too short for header
      }
      readRequest = SocketHeaderLenght();
      readSize    = _socket->read( (char *)_message, readRequest);
      if( readSize != readRequest){
         LoggerReport( "Header read failed\n");
         return( 0);                   // read failed
      }
      availableSize -= readSize;
      _receivedSize  = (SocketMessageSize)_message[ 0] | ((SocketMessageSize)_message[ 1] << 8);
      LoggerReport( "Header read ok\n");
   }
   // read message body :
   if( _receivedSize > availableSize){
      LoggerReport( "Message data short\n");
      return( 0);                      // incomplete message
   }
   readRequest = _receivedSize;
   readSize = _socket->read( (char *)&_message[ 2], readRequest);
   if( readSize != readRequest){
      LoggerReport( "Message body size mismatch\n");
      _receivedSize = 0;               // start new reception
      return( 0);                      // read failed
   }
   if( _receivedSize > size){
      LoggerReport( "Message too large\n");
      _receivedSize = 0;               // start new reception
      return( 0);                      // message too long
   }
   memcpy( data, &_message[ 2], (size_t)readSize);
   LoggerRx( _message, SocketMessageLength( readSize));
   _receivedSize = 0;                  // start new reception
   return( (int)readSize);             // return size read
} // receive

//******************************************************************************

//------------------------------------------------------------------------------
//   Connect
//------------------------------------------------------------------------------

void TcpServer::connectClient()
// New client connected
{
   QTcpSocket *clientConnection = nextPendingConnection();
   LoggerReport( "New client\n");
   if( _socket){
      // already connected
      LoggerReport( "Already connected\n");
      clientConnection->disconnectFromHost();
      return;
   }
   connect( clientConnection,SIGNAL( disconnected()),
            this, SIGNAL(clientDisconnected()));
   connect( clientConnection, SIGNAL( disconnected()),
            this, SLOT( disconnectClient()));
   connect( clientConnection, SIGNAL( readyRead()), 
            this, SLOT( dataReady()));
   _receivedSize = 0;                  // wait for a command
   _socket       = clientConnection;
   this->_socket->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
   LoggerReport( "Connected to client: %s:%d\n", _socket->peerAddress().toString().toStdString().c_str(),
                 _socket->peerPort());
} // connectClient

//------------------------------------------------------------------------------
//   Data ready
//------------------------------------------------------------------------------

void TcpServer::dataReady()
// Data received
{
   emit dataReceived();
} // dataReady

//------------------------------------------------------------------------------
//   Disconnect
//------------------------------------------------------------------------------

void TcpServer::disconnectClient()
// Client disconnected
{
   LoggerReport( "Disconnected from client\n");
   _socket->flush();
   _socket->disconnectFromHost();
   _socket->deleteLater();
   _socket       = 0;
   _receivedSize = 0;
} // disconnectClient
