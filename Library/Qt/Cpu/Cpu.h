//*****************************************************************************
//
//    Cpu.h        Fake CPU services
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Cpu_H__
   #define __Cpu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// interrupt function attribute :
#define __irq

// interrupt handler signature :
typedef void TIrqHandler( void);

#define CheckTrigger( counter, expression)  \
                     if( counter){          \
                        if( !--(counter)){  \
                            expression;     \
                        }                   \
                     }                      \

#define CpuIdleMode()

//-----------------------------------------------------------------------------
// Startup
//-----------------------------------------------------------------------------

#define CpuInit()
// Cpu startup initialisation
#define CpuReset()
// Cpu reset

//-----------------------------------------------------------------------------
// Interrupt controller
//-----------------------------------------------------------------------------

#define CpuIrqAttach( Irq, Priority, Handler)
// Install IRQ <Handler> at <Irq> with <Priority> level

//-----------------------------------------------------------------------------
// Watch dog
//-----------------------------------------------------------------------------

#define  WatchDogInit()
// Initialize watchdog

#define WatchDog()
// Refresh watchdog

//-----------------------------------------------------------------------------
// Interrupts
//-----------------------------------------------------------------------------

#define InterruptDisable()
#define InterruptEnable()

//-----------------------------------------------------------------------------
// Intrinsics
//-----------------------------------------------------------------------------

#define Nop()


#endif
