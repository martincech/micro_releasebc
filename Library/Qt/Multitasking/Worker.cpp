#include "Worker.h"

extern Worker *MoveToNextWorker();

Worker::Worker(TTask *Task) :
   QObject()
{
   this->Task = Task;
}

Worker::~Worker()
{
   AllWorkers.removeAll(this);
}

void Worker::doWork(){
   Task->EntryPoint(Task);
   MoveToNextWorker();
}

QList<Worker*> Worker::AllWorkers = QList<Worker*>();
QMutex Worker::RunMutex;
