//******************************************************************************
//
//   CodeSeparator.cpp  Source code separators
//   Version 1.0        (c) VEIT Electronics
//
//******************************************************************************

#include "CodeSeparator.h"
#include "uGenerator/SourceTemplate.h"
#include "uGenerator/GeneratorDef.h"

#define TITLE_FILE_NAME        GENERATOR_TEMPLATE_PATH "Title.h"
#define SECTION_FILE_NAME      GENERATOR_TEMPLATE_PATH "Section.h"

//------------------------------------------------------------------------------
//  Title
//------------------------------------------------------------------------------

QString CodeSeparator::title( QString fileName, QString description)
// generate source file header with <fileName> and <description>
{
   SourceTemplate titleTemplate( TITLE_FILE_NAME);
   titleTemplate.setParameter( "NAME", fileName);
   titleTemplate.setParameter( "DESCRIPTION", description);
   return( titleTemplate.toString());
} // title

//------------------------------------------------------------------------------
//  Separator
//------------------------------------------------------------------------------

QString CodeSeparator::section( QString description)
// generate section box with <description>
{
   SourceTemplate sectionTemplate( SECTION_FILE_NAME);
   sectionTemplate.setParameter( "DESCRIPTION", description);
   return( sectionTemplate.toString());
} // section
