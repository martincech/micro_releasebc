//******************************************************************************
//
//   CodeSeparator.h  Source code separators
//   Version 1.0      (c) VEIT Electronics
//
//******************************************************************************

#ifndef CODESEPARATOR_H
   #define CODESEPARATOR_H

#include <QString>

class CodeSeparator
{
public:
   static QString title( QString fileName, QString description);
   // generate source file header with <fileName> and <description>

   static QString section( QString description);
   // generate section box with <description>
}; // CodeSeparator

#endif // CODESEPARATOR_H
