//******************************************************************************
//
//   SourceTemplate.h  Source code template
//   Version 1.0       (c) VEIT Electronics
//
//******************************************************************************

#ifndef SOURCETEMPLATE_H
   #define SOURCETEMPLATE_H

#include <QString>

class SourceTemplate
{
public:
   SourceTemplate();
   SourceTemplate( QString fileName);

   void setFileName( QString fileName);
   // set new <fileName>

   bool setParameter( QString name, QString value);
   // search for <Name>, replace it with <Value>

   const QString toString();
   // returns actual text


private :
   QString _text;
}; // SourceTemplate

#endif // SOURCETEMPLATE_H
