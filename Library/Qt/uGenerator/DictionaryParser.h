//******************************************************************************
//
//   DictionaryParser.h  Dictionary parser
//   Version 1.0  (c)    VEIT Electronics
//
//******************************************************************************

#ifndef DICTIONARYPARSER_H
   #define DICTIONARYPARSER_H

#include <QString>
#include <QVector>
#include "Model/ArrayModel.h"
#include "uGenerator/GeneratorDef.h"

typedef QVector<DictionaryDefinition> DictionaryData;

//------------------------------------------------------------------------------

class DictionaryParser
{
public:
   DictionaryParser( ArrayModel *dictionaryArray);
   // create dictionary from <dictionaryArray>

   ~DictionaryParser();
   // destructor

   int itemsCount();
   // returns number of items

   const DictionaryDefinition *getDefinition( QString name);
   // returns enumeration definition by <name>

   const DictionaryDefinition *definitionAt( int index);
   // returns enumeration definition at <index> position

   bool hasName( QString name);
   // check for <name>

private :
   DictionaryData _dictionary;
   Enum           _dictionaryTypeEnum;
}; // DictionaryParser

#endif // DICTIONARYPARSER_H
