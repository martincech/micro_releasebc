//******************************************************************************
//
//   DataParser.h  Data definitions parser
//   Version 1.0   (c)    VEIT Electronics
//
//******************************************************************************

#ifndef DATAPARSER_H
   #define DATAPARSER_H

#include <QString>
#include <QVector>
#include "Model/ArrayModel.h"
#include "uGenerator/GeneratorDef.h"
#include "uGenerator/DictionaryParser.h"
#include "uGenerator/EmbeddedParser.h"

typedef QVector<DataDefinition> DataDefinitionList;

class DataParser
{
public:
   DataParser( ArrayModel *dataArray, DictionaryParser *dictionary, EmbeddedParser *embeddedTypes);
   // create data definitions from <dataArray> and <dictionary>

   ~DataParser();
   // destructor

   int itemsCount();
   // returns number of items

   const DataDefinition *getDefinition( QString name);
   // returns definition by <name>

   const DataDefinition *definitionAt( int index);
   // returns definition at <index> position

   bool hasName( QString name);
   // check for <name>

   QString title();
   // returns definition title

   QString fileName();
   // returns definition file name

   DictionaryParser *dictionary();
   // returns current dictionary

private :
   DataDefinitionList  _dataList;
   DictionaryParser   *_dictionary;
   EmbeddedParser     *_embeddedTypes;
   QString             _title;
   QString             _fileName;

   void updateDataType( DataItem &item);
   // update <item> by dictionary and embedded types
}; // DataParser

#endif // DATAPARSER_H
