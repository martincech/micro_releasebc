//******************************************************************************
//
//   DictionaryParser.cpp  Dictionary parser
//   Version 1.0  (c)      VEIT Electronics
//
//******************************************************************************

#include "DictionaryParser.h"
#include "uGenerator/GeneratorDef.h"

// Rem : maybe - povolit klicove slovo 'Prefix' a pridavat prefix
// ke jmenu enumu (analogicky viz DataParse.cpp)

// fixed column numbers :
#define NAME_COLUMN  0
#define ITEM_COLUMN  1

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------

DictionaryParser::DictionaryParser( ArrayModel *dictionaryArray) :
                                   _dictionaryTypeEnum( ENUM_DICTIONARY_TYPE)
// create dictionary from <dictionaryArray>
{
   int rows;
   QString name;
   QString item;
   QString items;
   QString type;
   rows = dictionaryArray->getRowsCount();
   DictionaryDefinition definition;
   dictionaryArray->setTitle( NAME_COLUMN, "Name");
   dictionaryArray->setTitle( ITEM_COLUMN, "Items");
   for( int row = 0; row < rows; row++){
      name = dictionaryArray->getItem( row, NAME_COLUMN);
      if( name.isEmpty()){
         continue;
      }
      definition.name = name;
      // check for item type definition :
      type = dictionaryArray->getItem( row, ITEM_COLUMN);
      if( !type.isEmpty()){
         definition.itemsList = 0;     // disable enum
         if( _dictionaryTypeEnum.hasString( type.toAscii())){
            definition.type = _dictionaryTypeEnum.toCode( type.toAscii());
         } else {
            definition.type = DICTIONARY_TYPE_UNDEFINED;
         }
         _dictionary.append( definition);
         continue;                     // skip empty definition
      } else {
         definition.type = DICTIONARY_TYPE_ENUM;
      }
      row++;                           // skip name
      items.clear();                   // clear enum items
      while( row < rows){
         item = dictionaryArray->getItem( row, ITEM_COLUMN);
         if( item.isEmpty()){
            break;
         }
         items += item;           // add item to list
         items += QChar( '\0');   // item terminator
         row++;
      }
      definition.itemsList = new Enum( items.toAscii());
      _dictionary.append( definition);
   }
} // DictionaryParser

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

DictionaryParser::~DictionaryParser()
// destructor
{
   foreach( DictionaryDefinition definition, _dictionary){
      delete definition.itemsList;
   }
} // DictionaryParser

//------------------------------------------------------------------------------
//  Count
//------------------------------------------------------------------------------

int DictionaryParser::itemsCount()
// returns number of items
{
   return( _dictionary.count());
} // itemsCount

//------------------------------------------------------------------------------
//  Definition by name
//------------------------------------------------------------------------------

const DictionaryDefinition *DictionaryParser::getDefinition( QString name)
// returns enumeration definition by <name>
{
   if( !hasName( name)){
      return( 0);
   }
   for( int i = 0; i < _dictionary.count(); i++){
      if( _dictionary.at( i).name == name){
         return( &_dictionary.at( i));
      }
   }
   return( 0);
} // getDefinition

//------------------------------------------------------------------------------
//  Definition by index
//------------------------------------------------------------------------------

const DictionaryDefinition *DictionaryParser::definitionAt( int index)
// returns enumeration definition at <index> position
{
   if( index >= _dictionary.count()){
      return( 0);
   }
   return( &_dictionary.at( index));
} // definitionAt

//------------------------------------------------------------------------------
//  Has name
//------------------------------------------------------------------------------

bool DictionaryParser::hasName( QString name)
// check for <name>
{
   for( int i = 0; i < _dictionary.count(); i++){
      if( _dictionary.at( i).name == name){
         return( true);
      }
   }
   return( false);
} // hasName
