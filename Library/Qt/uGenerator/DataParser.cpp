//******************************************************************************
//
//   DataParser.cpp    Data definitions parser
//   Version 1.0  (c)  VEIT Electronics
//
//******************************************************************************

#include "DataParser.h"
#include "uGenerator/CodeNumber.h"
#include "uGenerator/CodeHeader.h"
#include "Parse/nameTransformation.h"

// rem : maybe - v radku s titulkem 'Object' nacist zahlavi sloupcu.
// Konstanty xxx_COLUMN priradit dynamicky podle nazvu sloupcu

// fixed column numbers :
#define OBJECT_COLUMN   0
#define NAME_COLUMN     1
#define TYPE_COLUMN     2
#define CLASS_COLUMN    3
#define UNITS_COLUMN    4
#define DECIMALS_COLUMN 5
#define LOW_COLUMN      6
#define HIGH_COLUMN     7
#define DEFAULT_COLUMN  8
#define COMMENT_COLUMN  9

//------------------------------------------------------------------------------
//  Constructor
//------------------------------------------------------------------------------


DataParser::DataParser( ArrayModel *dataArray, DictionaryParser *dictionary, EmbeddedParser *embeddedTypes)
 // create data definitions from <dataArray> and <dictionary>
{
   _dictionary    = dictionary;
   _embeddedTypes = embeddedTypes;
   int rows;
   QString objectName;
   QString name;
   QString prefix;
   QString menuPrefix;
   DataDefinition definition;
   rows = dataArray->getRowsCount();
   for( int row = 0; row < rows; row++){
      objectName = dataArray->getItem( row, OBJECT_COLUMN);
      if( objectName.isEmpty()){
         continue;
      }
      // check for keywords :
      if( objectName == QString( KEYWORD_TITLE)){
         _title = dataArray->getItem( row, NAME_COLUMN);
         continue;
      }
      if( objectName == QString( KEYWORD_FILE)){
         _fileName = dataArray->getItem( row, NAME_COLUMN);
         continue;
      }
      if( objectName == QString( KEYWORD_PREFIX)){
         prefix = dataArray->getItem( row, NAME_COLUMN);
         continue;
      }
      if( objectName == QString( KEYWORD_MENU_PREFIX)){
         menuPrefix = dataArray->getItem( row, NAME_COLUMN);
         continue;
      }
      if( objectName == QString( KEYWORD_OBJECT)){
         // copy title row to column header :
         for( int col = 0; col < OBJECT_COLUMNS_MAX; col++){
            dataArray->setTitle( col, dataArray->getItem( row, col));
         }
         // skip column header
         continue;
      }
      // data/menu definition :
      definition.prefix     = prefix;
      definition.menuPrefix = menuPrefix;
      definition.name       = objectName;
      definition.menu       = false;             // data & menu definition
      if( dataArray->getItem( row, NAME_COLUMN) == KEYWORD_MENU){
         definition.menu = true;                 // menu definition only
      }
      definition.data    = false;                // data & menu definition
      if( dataArray->getItem( row, NAME_COLUMN) == KEYWORD_DATA){
         definition.data = true;                 // data definition only
      }
      definition.item.clear();
      row++;
      while( row < rows){
         name = dataArray->getItem( row, NAME_COLUMN);
         if( name.isEmpty()){
            break;
         }
         DataItem item;
         item.name         = name;
         item.dataName     = dataArray->getItem( row, TYPE_COLUMN);
         item.dataType     = _embeddedTypes->dataType( item.dataName);
         item.className    = dataArray->getItem( row, CLASS_COLUMN);
         item.classType    = _embeddedTypes->classType( item.className);
         item.units        = dataArray->getItem( row, UNITS_COLUMN);
         item.decimals     = dataArray->getItem( row, DECIMALS_COLUMN).toInt();
         item.lowLimit     = dataArray->getItem( row, LOW_COLUMN).toDouble();
         item.highLimit    = dataArray->getItem( row, HIGH_COLUMN).toDouble();
         item.defaultValue = dataArray->getItem( row, DEFAULT_COLUMN);
         item.comment      = dataArray->getItem( row, COMMENT_COLUMN);
         updateDataType( item);
         definition.item.append( item);
         row++;
      }
      _dataList.append( definition);
   }
} // DataParser

//------------------------------------------------------------------------------
//  Destructor
//------------------------------------------------------------------------------

DataParser::~DataParser()
// destructor
{
} // DataParser

//------------------------------------------------------------------------------
//  Count
//------------------------------------------------------------------------------

int DataParser::itemsCount()
// returns number of items
{
   return( _dataList.count());
} // itemsCount

//------------------------------------------------------------------------------
//  Item by name
//------------------------------------------------------------------------------

const DataDefinition *DataParser::getDefinition( QString name)
// returns definition by <name>
{
   for( int i = 0; i < _dataList.count(); i++){
      if( _dataList.at( i).name == name){
         return( &_dataList.at( i));
      }
   }
   return( 0);
} // getDefinition

//------------------------------------------------------------------------------
//  Name by index
//------------------------------------------------------------------------------

const DataDefinition *DataParser::definitionAt( int index)
// returns definition at <index> position
{
   if( index >= _dataList.count()){
      return( 0);
   }
   return( &_dataList.at( index));
} // definitionAt

//------------------------------------------------------------------------------
//  Has name
//------------------------------------------------------------------------------

bool DataParser::hasName( QString name)
// check for <name>
{
   for( int i = 0; i < _dataList.count(); i++){
      if( _dataList.at( i).name == name){
         return( true);
      }
   }
   return( false);
} // hasName

//------------------------------------------------------------------------------
//  Title
//------------------------------------------------------------------------------

QString DataParser::title()
// returns definition title
{
   return( _title);
} // title

//------------------------------------------------------------------------------
//  File name
//------------------------------------------------------------------------------

QString DataParser::fileName()
// returns definition file name
{
   return( _fileName);
} // fileName

//------------------------------------------------------------------------------
//  Dictionary
//------------------------------------------------------------------------------

DictionaryParser *DataParser::dictionary()
// returns current dictionary
{
   return( _dictionary);
} // dictionary

//******************************************************************************

//------------------------------------------------------------------------------
//  Update type
//------------------------------------------------------------------------------

void DataParser::updateDataType( DataItem &item)
// update <item> by dictionary and embedded types
{
   // custom struct definition :
   if( item.dataType == DATA_TYPE_STRUCT){
      item.classType = CLASS_TYPE_CUSTOM;
      item.editor    = OName::toCamelCase( item.className);
      return;
   }
   // custom data definition :
   if( item.dataType != DATA_TYPE_CUSTOM){
      return;                          // don't update : alias, menu, method
   }
   // check for predefined types :
   const EmbeddedDefinition *embeddedDefinition;
   if( _embeddedTypes->hasName( item.dataName)){
      embeddedDefinition = _embeddedTypes->getDefinition( item.dataName);
   } else {
      // embedded type not found, try dictionary :
      if( !_dictionary->hasName( item.dataName)){
         if( item.classType == CLASS_TYPE_ARRAY){
            // anonymous structure array
            item.dataType     = DATA_TYPE_STRUCT;
            item.defaultValue = QString::number( 0);
            item.editor       = "Array";
            return;
         }
         item.classType = CLASS_TYPE_UNDEFINED;
         item.editor    = "Unknown";
         return;                    // unknown data type
      }
      QString dataName;
      const DictionaryDefinition *dictionaryItem;
      dictionaryItem = _dictionary->getDefinition( item.dataName);
      switch( dictionaryItem->type){
         case DICTIONARY_TYPE_BYTE :
            dataName = "byte";
            break;

         case DICTIONARY_TYPE_WORD :
            dataName = "word";
            break;

         case DICTIONARY_TYPE_DWORD :
            dataName = "dword";
            break;

         default :
            item.classType = CLASS_TYPE_UNDEFINED;
            item.editor    = "Unknown";
            return;                    // unknown data type
      }
      embeddedDefinition = _embeddedTypes->getDefinition( dataName);
      if( !embeddedDefinition){
         item.classType = CLASS_TYPE_UNDEFINED;
         item.editor    = "Unknown";
         return;                       // unknown data type
      }
      item.dataName = CodeHeader::dataTypeName( item.dataName);
   }
   switch( embeddedDefinition->classType){
      case CLASS_TYPE_UINT :
         // check for YesNo type :
         if( item.classType == CLASS_TYPE_YESNO){
            item.classType = CLASS_TYPE_YESNO;
            item.editor    = "YesNo";
            return;
         }
         if( item.className.isEmpty()){
            // standard uint type
            item.classType = embeddedDefinition->classType;
            item.className = embeddedDefinition->className;
         } else {
            // check for enumeration :
            if( item.classType == CLASS_TYPE_UNDEFINED){
               if( !_dictionary->hasName( item.className.toAscii())){
                  item.classType = CLASS_TYPE_UNDEFINED;
                  item.editor    = "Unknown";
                  return;              // unknown data type
               }
               item.classType = CLASS_TYPE_ENUM;
               item.editor    = "Enum";
               return;
            } // else class type keyword
         }
         // check for class column keywords :
         if( item.classType == CLASS_TYPE_FIXED){
            // fixed type, update limits
            item.lowLimit     = CodeNumber::fixedNumber( item.lowLimit,  item.decimals);
            item.highLimit    = CodeNumber::fixedNumber( item.highLimit, item.decimals);
            item.defaultValue = QString::number( CodeNumber::fixedNumber( item.defaultValue.toDouble(), item.decimals));
         } else if( item.classType == CLASS_TYPE_SPARE){
            // spare item
            item.lowLimit     = 0;
            item.highLimit    = 0;
            item.defaultValue = QString::number( 0);
         } else if( item.classType == CLASS_TYPE_ARRAY){
            item.defaultValue = QString::number( 0);
            item.editor       = "Array";
            item.width        = 0;
            return;
         } // else standard UINT
         item.editor = embeddedDefinition->editor;
         item.width  = CodeNumber::fixedRangeWidth( item.lowLimit, item.highLimit, item.decimals);
         return;

      case CLASS_TYPE_INT :
      case CLASS_TYPE_FLOAT :
         item.classType = embeddedDefinition->classType;
         item.editor    = embeddedDefinition->editor;
         item.width     = CodeNumber::fixedRangeWidth( item.lowLimit, item.highLimit, item.decimals);
         return;

      case CLASS_TYPE_STRING :
       case CLASS_TYPE_PASSWORD :
         if( item.className.isEmpty()){
            // single character
            item.classType = CLASS_TYPE_CHAR;
            item.editor    = "Letter";
            return;
         }
         if( item.classType != CLASS_TYPE_STRING && item.classType != CLASS_TYPE_PASSWORD){
            item.classType = CLASS_TYPE_UNDEFINED;
            item.editor    = "Unknown";
            return;                    // unknown data type
         }
         item.editor    = embeddedDefinition->editor;
         return;

      case CLASS_TYPE_CUSTOM :
         item.dataName  = embeddedDefinition->dataName;
         item.classType = CLASS_TYPE_EMBEDDED;
         item.editor    = embeddedDefinition->editor;
         if( item.defaultValue.isEmpty()){
            item.defaultValue = QString( "?");      // fake value
         }
         return;

      default :
         item.classType = CLASS_TYPE_UNDEFINED;
         item.editor    = "Unknown";
         return;                    // unknown data type
   }
} // updateDataType
