//*****************************************************************************
//
//    Fonts.h      project fonts
//    Version 1.0  (c) VEIT Electronics
//
//*****************************************************************************

#ifndef __Fonts_H__
   #define __Fonts_H__

#ifndef __Font_H__   
   #include "Font/Font.h"
#endif   

// Font descriptor :

extern TFontDescriptor const *Fonts[];

void SetFont( int FontNumber);
// Set font

#endif
